import React from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";

const MUIBox = () => {
  return (
    <React.Fragment>
      <Box component="span" m={1}>
        <Button>Hello World</Button>
      </Box>
      <Box color="text.primary" clone>
        <Button>Hello World</Button>
      </Box>
    </React.Fragment>
  );
};

export default MUIBox;
