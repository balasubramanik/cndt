import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Help from "./components/Help";
import Slider from "./components/Slider";
import Header from "./components/Header";
import HeaderBanner1 from "./components/Banner/HeaderBanner1.tsx";
import Gallery from './components/Gallery/Gallery.tsx';
import CloseIcon from '@material-ui/icons/Close';
import { Grid, Typography, Button, Backdrop } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  }),
);
function App() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };
  const renderImage = (item) => {
    return(
      <React.Fragment>
        <img className='image-gallery-image' style={{maxHeight: '450px'}} src={item.original} alt={item.description} />
        <Grid container>
          <Grid item xs={12} sm={3} md={3}></Grid>
          <Grid item xs={12} sm={6} md={6} style={{ borderBottom: '1px solid #fff', whiteSpace: 'normal'}}>
            <Typography variant="body1">
              {item.description}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={3} md={3}></Grid>
          
        </Grid>
      </React.Fragment>
    ); 
      
  }
  return (
    <React.Fragment>
      
      <Header />
      
      <main>
        {/* <!-- Banner --> */}
        <section className="banner">
          <div className="content">
            {/* <HeaderBanner data={{
              image: "/assets/img/harley-davidson.jpg", 
              title: "Residential",
              content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos laborum fugiat deleniti? Eum quasi quidem quibusdam.",
              link: {text: "Lorem ipsum"}
              }} 
              
            /> */}
            <HeaderBanner1 data={{
              image: "/assets/img/image-1.jpg", 
              title: "Billing & Payment Options",
              subTitle: "Pay online quickly and securely",
              content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vestibulum et curcus mauris, Quisque varius ultricies pretium. Sed consequat neque dolor, vel fringilla augue ornare pellentesque.",
              link: {text: "Pay Online Now", url: '/billing-payment-options'}
              }} 
              
            />
            
          </div>
        </section>
        {/* <!-- End Banner -->
        <!-- Breadcrumb --> */}
        <div className="container">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb">
              <li className="breadcrumb-item">
                <a href="/#">Home</a>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                Residential
              </li>
            </ol>
          </nav>
        </div>
        {/* <!-- End Breadcrumb -->
        <!-- Grid Section --> */}
        <section>
        <Backdrop className={classes.backdrop} open={open}>
        <Grid container justify="flex-end">
          <Grid xs={12} style={{textAlign: 'right'}}>
            <Button onClick={handleClose}><CloseIcon style={{fontSize: '42px'}} /></Button>
          </Grid>
          <Grid xs={12}>
          <Gallery items={[
            {
              original: '/assets/gallery/1.jpg',
              thumbnail: '/assets/gallery/1t.jpg',
              description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, ',
              renderItem: renderImage
            },
            {
              original: '/assets/gallery/2.jpg',
              thumbnail: '/assets/gallery/2t.jpg',
              description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, ',
              renderItem: renderImage
            },
            {
              original: '/assets/gallery/3.jpg',
              thumbnail: '/assets/gallery/3t.jpg',
              description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, ',
              renderItem: renderImage
            }]} 
            showFullscreenButton={false}
            showPlayButton={false}
            
          />
          </Grid>
          </Grid>
        </Backdrop >
          <div className="container">
            <header>
              <div>
                <h2 className="h4">We are here to help...</h2>
              </div>
              <p>
                The smart, modern electric grid gives you the highly reliable
                energy you expect.It also offers you new ways to take advantages
                of innovations that make your home or business more cost
                effective, comfortable and productive.
              </p>
            </header>

            <div className="row">
              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div className="card">
                  <img
                    className="card-img-top"
                    src="/assets/img/image-1.jpg"
                    alt="Card cap"
                  />
                  <div className="card-body">
                    <h5 className="card-title h5">
                      Billing &amp; Payment options
                    </h5>
                    <p className="card-text">
                      This is a wider card with supporting text below as a
                      natural lead-in to additional content. This content is a
                      little bit longer.
                    </p>
                    <a href="/#">Link</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div className="card">
                  <img
                    className="card-img-top"
                    src="/assets/img/image-2.jpg"
                    alt="Card cap"
                  />
                  <div className="card-body">
                    <h5 className="card-title h5">Start, Stop or Move</h5>
                    <p className="card-text">
                      This is a wider card with supporting text below as a
                      natural lead-in to additional content. This content is a
                      little bit longer.
                    </p>
                    <a href="/#">Link</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- End Grid --> 
        Gallery */}
        <section className="section">
          <div className="container">
            <Gallery items={[
              {
                original: '/assets/gallery/1.jpg',
                thumbnail: '/assets/gallery/1t.jpg',
                description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, ',
                renderItem: renderImage
              },
              {
                original: '/assets/gallery/2.jpg',
                thumbnail: '/assets/gallery/2t.jpg',
                description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, ',
                renderItem: renderImage
              },
              {
                original: '/assets/gallery/3.jpg',
                thumbnail: '/assets/gallery/3t.jpg',
                description: 'Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, ',
                renderItem: renderImage
              }]} 
              showFullscreenButton={false}
              showPlayButton={false}
              showBullets={true}              
              showThumbnails={false}
              onClick={handleToggle}
            />
          </div>
        </section>
        {/* End Gallery
        <!-- Icon Section --> */}
        <section className="section">
          <div className="container">
            <header>
              <div className="mb-0">
                <h2 className="h5">Future Thinking</h2>
              </div>
            </header>
            <div className="row">
              <div className="col-md-4 border-right">
                <span className="icon">
                  <FontAwesomeIcon icon="anchor" size="5x" />
                </span>
                <h3 className="h6">Power Choices</h3>
                <p className="small">
                  We strive to embrace and drive change in our industry.
                </p>
              </div>

              <div className="col-md-4 border-right">
                <span className="icon">
                  <FontAwesomeIcon icon={["fab", "angellist"]} size="5x" />
                </span>
                <h3 className="h6">Energy Savings</h3>
                <p className="small">
                  We strive to embrace and drive change in our industry.
                </p>
              </div>

              <div className="col-md-4">
                <span className="icon">
                  <FontAwesomeIcon
                    icon={["fab", "accessible-icon"]}
                    size="5x"
                  />
                </span>
                <h3 className="h6">Choose Renewable</h3>
                <p className="small">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit,{" "}
                </p>
              </div>
            </div>
          </div>
        </section>
        {/* <!-- End Icon Section -->
        <!-- Image  --> */}
        <section className="section banner">
          <div className="content">
            <img
              src="/assets/img/evgeny.jpg"
              className="img-fluid"
              alt="Banner"
            />
          </div>
        </section>
        {/* <!-- End Image  --> */}
        <Help />
        <Slider />
      </main>
      {/* <!-- Footer --> */}
      <footer className="bg-primary footer-navigation-1">
        {/* <!-- Banner --> */}
        <section className="banner">
          <div className="content">
            <img
              src="/assets/img/wind-mill.png"
              alt="banner"
              className="img-fluid"
            />
          </div>
        </section>
        {/* <!-- End Banner --> */}
        <div className="container">
          <div className="row">
            <div className="col-sm-7">
              <div className="row">
                <div className="col-md-6">
                  <h2 className="h6 text-white text-uppercase">company</h2>
                  <div className="row">
                    <div className="col-md-6">
                      <nav>
                        <ul className="list-unstyled">
                          <li>
                            <a href="/#">About PGE</a>
                          </li>
                          <li>
                            <a href="/#">Energy Strategy</a>
                          </li>
                          <li>
                            <a href="/#">Careers</a>
                          </li>
                          <li>
                            <a href="/#">News Center</a>
                          </li>
                          <li>
                            <a href="/#">Regulatory</a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                    <div className="col-md-6">
                      <nav>
                        <ul className="list-unstyled">
                          <li>
                            <a href="/#">Environment</a>
                          </li>
                          <li>
                            <a href="/#">Sustainability</a>
                          </li>
                          <li>
                            <a href="/#">Communities</a>
                          </li>
                          <li>
                            <a href="/#">Investors</a>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
                <div className="col-md-6">
                  <h2 className="h6 text-white text-uppercase">
                    work with pge
                  </h2>
                  <nav className="border-right">
                    <ul className="list-unstyled">
                      <li>
                        <a href="/#">Commercial</a>
                      </li>
                      <li>
                        <a href="/#">
                          Property <abbr title="Management">Mgmt</abbr>
                        </a>
                      </li>
                      <li>
                        <a href="/#">Construction</a>
                      </li>
                      <li>
                        <a href="/#">New Business</a>
                      </li>
                      <li>
                        <a href="/#">Suppliers</a>
                      </li>
                      <li>
                        <a href="/#">Wholesale</a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
            <div className="col-sm-5">
              <div className="row float-right">
                <div className="col-md-6">
                  <h2 className="h6 text-white text-uppercase">
                    customer service
                  </h2>
                  <address>
                    <div className="text-white">
                      <p>888-542-8818</p>
                    </div>
                    <div className="text-white">
                      <p>7am-7pm, Monday - Friday</p>
                    </div>
                  </address>
                  <nav>
                    <ul className="list-unstyled navbar-nav">
                      <li className="nav-item">
                        <a href="/#" className="nav-link">
                          Help Center
                        </a>
                      </li>
                      <li>
                        <a href="/#">All Contacts</a>
                      </li>
                    </ul>
                  </nav>
                </div>
                <div className="col-md-6">
                  <h2 className="h6 text-white text-uppercase">
                    outages, emergencies &amp; power problems
                  </h2>
                  <address>
                    <div className="text-white">
                      <p>503-464-7777 or 800-544-1795</p>
                    </div>
                    <div className="text-white">
                      <p>24 Hours</p>
                    </div>
                  </address>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Copyright Footer --> */}
          <div className="row footer-navigation">
            <div className="col-md-8 col-xs-12">
              <ul className="list-inline">
                <li className="list-inline-item">
                  <a href="/#">Privacy</a>
                </li>
                <li className="list-inline-item">
                  <a href="/#">|</a>
                </li>
                <li className="list-inline-item">
                  <a href="/#">Legal</a>
                </li>
                <li className="list-inline-item">
                  <a href="/#">|</a>
                </li>
                <li className="list-inline-item">
                  <a href="/#">Sitemap</a>
                </li>
              </ul>
            </div>
            <div className="col-md-4 col-xs-12 align-self-center">
              <ul className="list-inline text-center text-md-right mb-0">
                <li className="list-inline-item" title="Facebook">
                  <a href="/#">
                    <FontAwesomeIcon icon={["fab", "facebook"]} />
                  </a>
                </li>
                <li className="list-inline-item" title="Instagram">
                  <a href="/#">
                    <FontAwesomeIcon icon={["fab", "instagram"]} />
                  </a>
                </li>
                <li className="list-inline-item" title="Linkedin">
                  <a href="/#">
                    <FontAwesomeIcon icon={["fab", "linkedin"]} />
                  </a>
                </li>

                <li className="list-inline-item" title="Twitter">
                  <a href="/#">
                    <FontAwesomeIcon icon={["fab", "twitter"]} />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* <!-- End Copyright Footer--> */}
        </div>
      </footer>
    </React.Fragment>
  );
}

export default App;
