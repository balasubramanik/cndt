import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import './HeaderBanner.css';
import Button from "@material-ui/core/Button";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      backgroundColor: "#ecfafc",
      [theme.breakpoints.up('xs')]: {
        padding: 0,
      },
      [theme.breakpoints.up('sm')]: {
        padding: '7vh 0'
      },
      "& > div > div": {
        [theme.breakpoints.down('xs')]: {
          flexDirection: 'column-reverse',
        }
      }
    },
    title: {
      color: "#20415b",
      fontFamily: 'Sans-serif',
    },
    subTitle: {
      color: "#20415b",
      fontFamily: 'Sans-serif',
      fontWeight: 'bold',
    },
    textGrid: {
      [theme.breakpoints.up('xs')]: {
        padding: '5vh 3vh',
      },
      [theme.breakpoints.up('sm')]: {
        padding: '0 3vh 0 0',
      },
    },
    image: {
      width: '100%',
      maxHeight: '270px',
      minHeight: '180px',
      [theme.breakpoints.up('xs')]: {
        height: '220px',
      },
    }
  })
);

interface ILink {
  text: string;
  url?: string;
}
interface IData {
  title: string;
  subTitle: string;
  image: string;
  content: string;
  link: ILink;
}

interface IProps {
  data: IData;
}

const HeaderBanner1 = (props: IProps): JSX.Element => {
  const { data } = props;
  const classes = useStyles();
  
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('xs'));
  
  return (
    
    <div className={classes.root + (matches ? ' xs' : ' sm') }>
      <div className={!matches ? 'container' : ''}>
        <Grid
          container
          direction="row"
          justify="flex-start"
        >
          <Grid item sm={6} className={classes.textGrid}>
            <Typography variant="h4" gutterBottom className={classes.title}>
              {data.title}
            </Typography>
            <Typography variant="subtitle1" gutterBottom className={classes.subTitle}>
              {data.subTitle}
            </Typography>
            <Typography variant="body1" gutterBottom className="mb-4">
              {data.content}
            </Typography>
            <Typography className="button">
              <Button color="primary" variant="contained">{data.link.text}</Button>
            </Typography>
          </Grid>

          <Grid item sm={6}>
            <img src={data.image} alt="Billing Payment" className={classes.image} />
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default HeaderBanner1;
