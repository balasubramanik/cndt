import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: '100%',
      height: "50vh",
      overflow: "hidden",
      border: 0,
      margin: 0,
      display: 'flex',
      padding: 0,
      position: 'relative',
      alignItems: 'center',
      backgroundSize: 'cover',
      backgroundPosition: 'top center',
      backgroundRepeat: 'no-repeat',
    }
    
  }),
);

interface ILink {
  text: string
}
interface IData  {
  title: string,
  image: string,
  content: string,
  link: ILink
};

interface IProps {
  data: IData
};

const HeaderBanner= (props: IProps): JSX.Element => {
  const { data } = props;
  const classes = useStyles();
  return (
    <div className={classes.root} style={{backgroundImage: "url(" + data.image + ")"}} >
      <Container>
        <Grid container direction="row" justify="flex-start" bg-color="secondary">
          <Grid item xs={12} sm={6} md={4} lg={3} xl={3}>
            <Typography variant="h6" gutterBottom>
              {data.title}
            </Typography>
            <Typography variant="body1" gutterBottom>
              {data.content}
            </Typography>
            <Typography>
              <Button variant="outlined">{data.link.text}</Button>
            </Typography>
            
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default HeaderBanner;
