import React, { PureComponent } from 'react';
import Accordion from "./Accordion/Accordion.tsx";
import FilterTiltShiftIcon from '@material-ui/icons/FilterTiltShift';

export default class Help extends PureComponent {
  render() {
    return (
      <section className="bg-light pb-10">
        <div className="container">
          {/* <!-- Heading --> */}
            <div>
              <div className="d-inline-block"></div>
              <h2 className="h4 mb-20">Help</h2>
            </div>
          {/* <!-- End Heading --> */}
          <Accordion
            panels={[
              {
                title: "Help Center",
                teaser: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida enim sed augue lobortis, ut luctus dolor dignissim. Mauris neque felis, pellentesque ac ligula nec, dignissim molestie ex. Sed in viverra eros. Suspendisse tempor diam justo, vitae laoreet Leo tincidunt eget. Aenean nec augue bibendum, blandit metus vitae, sodales ante.",
              },
              {
                title: "Bill Payment Assistance",
                teaser: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida enim sed augue lobortis, ut luctus dolor dignissim. Mauris neque felis, pellentesque ac ligula nec, dignissim molestie ex. Sed in viverra eros. Suspendisse tempor diam justo, vitae laoreet Leo tincidunt eget. Aenean nec augue bibendum, blandit metus vitae, sodales ante.",
              },
            ]}
            variant="teaser"
          />        
          <Accordion
            panels={[
              {
                title: "Help Center",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida enim sed augue lobortis, ut luctus dolor dignissim. Mauris neque felis, pellentesque ac ligula nec, dignissim molestie ex. Sed in viverra eros. Suspendisse tempor diam justo, vitae laoreet Leo tincidunt eget. Aenean nec augue bibendum, blandit metus vitae, sodales ante.",
              },
              {
                title: "Bill Payment Assistance",
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida enim sed augue lobortis, ut luctus dolor dignissim. Mauris neque felis, pellentesque ac ligula nec, dignissim molestie ex. Sed in viverra eros. Suspendisse tempor diam justo, vitae laoreet Leo tincidunt eget. Aenean nec augue bibendum, blandit metus vitae, sodales ante.",
              },
            ]}
            
          />        
          <Accordion
            panels={[
              {
                title: "Help Center",
                icon: <FilterTiltShiftIcon className="mr-10" />,
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida enim sed augue lobortis, ut luctus dolor dignissim. Mauris neque felis, pellentesque ac ligula nec, dignissim molestie ex. Sed in viverra eros. Suspendisse tempor diam justo, vitae laoreet Leo tincidunt eget. Aenean nec augue bibendum, blandit metus vitae, sodales ante.",
              },
              {
                title: "Bill Payment Assistance",
                icon: <FilterTiltShiftIcon className="mr-10" />,
                content:
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida enim sed augue lobortis, ut luctus dolor dignissim. Mauris neque felis, pellentesque ac ligula nec, dignissim molestie ex. Sed in viverra eros. Suspendisse tempor diam justo, vitae laoreet Leo tincidunt eget. Aenean nec augue bibendum, blandit metus vitae, sodales ante.",
              },
            ]}
            variant="icon"
          />        
        </div>
      </section>
    )
  }
}
