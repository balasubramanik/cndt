import React from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  }),
);

const SimpleBackdrop = (props: any): JSX.Element => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  return (
    <div>
      <Button variant="outlined" color="primary">
        Show backdrop
      </Button>
      <Backdrop className={classes.backdrop} open={open} >
        {props.children}
      </Backdrop>
    </div>
  );
}

export default SimpleBackdrop;
