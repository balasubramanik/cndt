import React from "react";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import Typography from "@material-ui/core/Typography";

// @material-ui/icons
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    flexBasis: "33.33%",
    flexShrink: 0,
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(15),
    color: theme.palette.text.secondary,
  },
}));

export default function Accordion(props) {
  const { data } = props;
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const accordion = data.map((accordion, index) => {
    return (
      <ExpansionPanel
        expanded={expanded === `panel${index}`}
        onChange={handleChange(`panel${index}`)}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls={`panel${index}bh-content`}
          id={`panel${index}bh-header`}
        >
          <Typography className={classes.heading}>{accordion.title}</Typography>
          <Typography className={classes.secondaryHeading}>
            {accordion.teaser}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Typography>{accordion.content}</Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  });
  return <div className={classes.root}>{accordion}</div>;
}

Accordion.propTypes = {
  // index of the default active collapse
  data: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string,
      teaser: PropTypes.string,
      content: PropTypes.node,
    })
  ).isRequired,
};
