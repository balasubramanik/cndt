import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const AccordionStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '0 0 20px',
      width: '100%',
      "& > div": {
        boxShadow: '0px 3px 6px #00000029',
        borderRadius: '6px',
        margin: '10px 0',
        "& > div > div:nth-child(1)":{
          display: "block",
        },
        "& > div.icon > div:nth-child(1)":{
          display: "flex",
        }
      },
      "& > div:before":{
        height: '0',
      }
      
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      flexBasis: '33.33%',
      flexShrink: 0,
    },
    secondaryHeading: {
      fontSize: theme.typography.pxToRem(15),
      color: theme.palette.text.secondary,
    },
    panelDetails: {
      padding: '8px 16px 30px'
    }
  }),
);
