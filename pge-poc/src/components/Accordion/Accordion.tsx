/**
  * Accordion Component
  * Props: 
  *   panels - (required) Array of Objects 
  *     Available Properties: 
  *       title - Panel Title 
  *       content - Panel Description
  *       teaser - teaser text with Title
  *       icon - icon with Title
  *   variant - (optional) String, Default title ( teaser | icon | title)
  * Author: Bala
*/
import React from 'react';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { AccordionStyles } from './AccordionStyles';

interface IPanel  {
  title: string;
  teaser?: string;
  content: string;
  icon?: JSX.Element;
}

interface IProps {
  panels: Array<IPanel>;
  variant?: string;
}
const Accordion = (props: IProps): JSX.Element => {
  const classes = AccordionStyles();
  const { panels, variant } = props;
  const [expanded, setExpanded] = React.useState<string | false>(false);
  
  // Handle Expand and Collapse of Accordion
  const handleChange = (panel: string) => (event: React.ChangeEvent<{}>, isExpanded: boolean): void => {
    setExpanded(isExpanded ? panel : false);
  };

  const accordion = panels.map((panel: IPanel, index: number): JSX.Element => {
    return (
      <ExpansionPanel
        expanded={expanded === `panel${index}`}
        onChange={handleChange(`panel${index}`)}
        key={index}
      >
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls={`panel${index}bh-content`}
          id={`panel${index}bh-header`}
          className={variant === 'icon' ? 'icon' : ''}
        >
          {
            variant==='icon' && panel.icon
          }
          <Typography className={classes.heading}>{panel.title}</Typography>
          {
            variant==='teaser' && <Typography className={classes.secondaryHeading}>
              {panel.teaser}
            </Typography>
          }
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={classes.panelDetails}>
          <Typography>{panel.content}</Typography>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    );
  });
  return <div className={classes.root}>{accordion}</div>;
}

export default Accordion;
