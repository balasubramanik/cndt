import React, { Component } from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Header extends Component {
  state = {
    showSideDrawer: false,
  };

  sideDrawerToggleHandler = () => {
    this.setState( ( prevState ) =>  {
      return {showSideDrawer: !prevState.showSideDrawer};
      
    });
  }
  render() {
    return (
      <header>
        {/* <!-- Header --> */}
        <nav className="navbar navbar-expand-sm bg-blue navbar-dark fixed-top">
          
            <div className="col-md-6 pa-0">
              <button className="navbar-toggler brd-none py-3 text-white" type="button" onClick={this.sideDrawerToggleHandler}>
                <FontAwesomeIcon icon="bars" />
              </button>
              <a className="navbar-brand" href="/#">
                <img
                  src="/assets/img/Logo.PNG"
                  alt="PGE logo"
                  className="img-fluid"
                />
                <span className="title d-none d-sm-inline-block">Good Morning</span>
              </a>
              <span className="title text-white d-sm-none d-inline-block">Good Morning</span>
            </div>
            
            <div className="col-md-6 d-none d-sm-block pa-0">
              <ul className="navbar-nav float-right">
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    Search
                    <FontAwesomeIcon icon="search" />
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    jonathan.smith@gmail.com
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    Sign out
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#">
                    English
                  </a>
                </li>
              </ul>
            
          </div>
          
        </nav>
        {/* <!-- End Header --> */}
        { /*<!-- Navigation  --> */}
        <div className={`main-navigation ${this.state.showSideDrawer ? 'drawer-open fixed-top' : 'drawer-close'}`}>
          <ul className="list-inline">
            <li className="list-inline-item">
              <a href="/#">My Account</a>
            </li>
            <li className="list-inline-item active">
              <a href="/#">Residential</a>
            </li>
            <li className="list-inline-item">
              <a href="/#">Business</a>
            </li>
            <li className="list-inline-item">
              <a href="/#">Outages &amp; Safety</a>
            </li>
            <li className="list-inline-item">
              <a href="/#">Help Center</a>
            </li>
            <li className="list-inline-item">
              <a href="/#">Company</a>
            </li>
          </ul>
        </div>
        {/* <!-- End Navigation  --> */}
      </header>
        
    )
  }
}

export default Header;
