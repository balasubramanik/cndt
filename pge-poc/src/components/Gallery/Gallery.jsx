import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";


class Gallery extends Component {
  constructor(props){
    super(props);
    this.galleryRef = null;
  }
  componentDidMount(){
    console.log(this.galleryRef.currentIndex);

  }
  onSlide = (index) => {
    
    console.debug('slid to index', index);
  }
  render() {
    if (this.galleryRef){
      console.log(this.galleryRef.currentIndex);
      
    }
    
    const { items, ...rest } = this.props;
    return(
      <ImageGallery items={items} onSlide={this.onSlide} ref={i => this.galleryRef = i } {...rest} />
    );
  }
}

export default Gallery;
