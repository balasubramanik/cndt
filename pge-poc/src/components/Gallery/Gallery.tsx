import React, { useEffect } from 'react';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";

interface IItem  {
  original: string;
  thumbnail: string;
}

interface IProps {
  items: Array<IItem>;
  showFullscreenButton: boolean;
  showPlayButton: boolean;
  showBullets: boolean;
}

const Gallery = (props: IProps): JSX.Element => {
  const { items, ...rest } = props;
  //let galleryRef = null;
  // Similar to componentDidMount and componentDidUpdate:
  /*useEffect(() => {
    console.log(galleryRef.getCurrentIndex());
    
  });
  */
  const getIndex = (index: number) => {
    console.log(index);
  }
  
  return(
    <React.Fragment>
      <ImageGallery items={items} {...rest} onSlide={getIndex} />
    </React.Fragment>
    
  );
}

export default Gallery;
