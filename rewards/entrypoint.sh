#!/bin/sh

echo "[ENTRYPOINT] - Replace Environment Variable"

BASEURL_UPDATE="$(echo $BASEURL | sed 's/\//\\\//g')"
CCZ_BASEURL_UPDATE="$(echo $CCZ_BASEURL | sed 's/\//\\\//g')"
REWARD_BASEURL_UPDATE="$(echo $REWARD_BASEURL | sed 's/\//\\\//g')"
GAF_BASEURL_UPDATE="$(echo $GAF_BASEURL | sed 's/\//\\\//g')"
PLACE_API_KEYS_UPDATE="$(echo $PLACE_API_KEYS | sed 's/\//\\\//g')"
RECAPTCHA_API_KEYS_UPDATE="$(echo $RECAPTCHA_API_KEYS | sed 's/\//\\\//g')"
DOMAIN_UPDATE="$(echo $DOMAIN | sed 's/\//\\\//g')"
CLIENT_ID_UPDATE="$(echo $CLIENT_ID | sed 's/\//\\\//g')"
AUDIENCE_UPDATE="$(echo $AUDIENCE | sed 's/\//\\\//g')"
GTM_CONTAINER_ID_UPDATE="$(echo $GTM_CONTAINER_ID | sed 's/\//\\\//g')"
IDLE_SESSION_UPDATE="$(echo $IDLE_SESSION_TIME | sed 's/\//\\\//g')"
SESSION_TIMEOUT_UPDATE="$(echo $SESSION_TIMEOUT | sed 's/\//\\\//g')"
TERMSANDCONDITIONS_URL_UPDATE="$(echo $TERMSANDCONDITIONS_URL | sed 's/\//\\\//g')"
DISABLE_CAPTCHA_UPDATE="$(echo $DISABLE_CAPTCHA | sed 's/\//\\\//g')"

perl -pi.backup -ne 's/(?<=baseUrl:")(.*?)(?=")/'${BASEURL_UPDATE}'/g; \
s/(?<=cczBaseUrl:")(.*?)(?=")/'${CCZ_BASEURL_UPDATE}'/g; \
s/(?<=rewardsBaseUrl:")(.*?)(?=")/'${REWARD_BASEURL_UPDATE}'/g; \
s/(?<=gafBaseUrl:")(.*?)(?=")/'${GAF_BASEURL_UPDATE}'/g; \
s/(?<=googlePlaceApiKeys:")(.*?)(?=")/'${PLACE_API_KEYS_UPDATE}'/g; \
s/(?<=googleRecaptchaApiKeys:")(.*?)(?=")/'${RECAPTCHA_API_KEYS_UPDATE}'/g; \
s/(?<=domain:")(.*?)(?=")/'${DOMAIN_UPDATE}'/g; \
s/(?<=clientId:")(.*?)(?=")/'${CLIENT_ID_UPDATE}'/g; \
s/(?<=audience:")(.*?)(?=")/'${AUDIENCE_UPDATE}'/g; \
s/(?<=idleSessionTime:")(.*?)(?=")/'${IDLE_SESSION_UPDATE}'/g; \
s/(?<=sessionTimeout:")(.*?)(?=")/'${SESSION_TIMEOUT_UPDATE}'/g; \
s/(?<=termsAndConditionUrl:")(.*?)(?=")/'${TERMSANDCONDITIONS_URL_UPDATE}'/g; \
s/(?<=disableCaptcha:")(.*?)(?=")/'${DISABLE_CAPTCHA_UPDATE}'/g; \
s/(?<=gtmContainerId:")(.*?)(?=")/'${GTM_CONTAINER_ID_UPDATE}'/g;' build/main.*.js

perl -pi.backup -ne 's/(?<=accessKey: ")(.*?)(?=")/'${JSCRAMBLER_ACCESS_KEY}'/g; \
s/(?<=secretKey: ")(.*?)(?=")/'${JSCRAMBLER_SECRET_KEY}'/g; \
s/(?<=applicationId: ")(.*?)(?=")/'${JSCRAMBLER_APP_ID}'/g;' jscrambler.config.js

perl -pi.backup -ne 's/gtmContainerId/'${GTM_CONTAINER_ID_UPDATE}'/g;' build/index.html


