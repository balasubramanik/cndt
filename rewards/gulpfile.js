const gulp = require('gulp');
const jscrambler = require('gulp-jscrambler');
const jscramblerCreds = require('./jscrambler.config');

gulp.task('jscrambler', function (done) {
  gulp
    .src(['build/*.js', '!build/gaf-vendors.*.js', '!build/vendors*.js'])
    .pipe(
      jscrambler(
        {
          keys: {
            accessKey: jscramblerCreds.accessKey,
            secretKey: jscramblerCreds.secretKey
          },
          applicationId: jscramblerCreds.applicationId,
          "params": [
            {
              "name": "whitespaceRemoval"
            },
            {
              "name": "identifiersRenaming"
            },
            {
              "name": "dotToBracketNotation"
            },
            {
              "name": "deadCodeInjection"
            },
            {
              "name": "stringConcealing"
            },
            {
              "name": "functionReordering"
            },
            {
              "name": "propertyKeysObfuscation"
            },
            {
              "name": "regexObfuscation"
            },
            {
              "name": "booleanToAnything"
            },
            {
              "name": "extendPredicates"
            },
            {
              "name": "numberToString"
            },
            {
              "name": "variableGrouping"
            }
          ],
          "areSubscribersOrdered": false,
          "applicationTypes": {
            "webBrowserApp": true,
            "desktopApp": false,
            "serverApp": false,
            "hybridMobileApp": false,
            "javascriptNativeApp": false,
            "html5GameApp": false
          },
          "languageSpecifications": {
            "es5": true,
            "es6": false,
            "es7": false
          },
          "useRecommendedOrder": true,
          "jscramblerVersion": "5.4",
          "host": "jscrambler.gaf.com",
          "port": "8081"
        }
      )
    )
    .pipe(gulp.dest('./'))
    .on('end', done);
});