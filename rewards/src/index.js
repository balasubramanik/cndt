import envConfig from 'envConfig'; //eslint-disable-line
__webpack_public_path__ = envConfig.cdnBuildUrl; //eslint-disable-line

import React from 'react'; //eslint-disable-line
import ReactDOM from 'react-dom'; //eslint-disable-line
import { Provider } from 'react-redux'; //eslint-disable-line
import { store } from './configureStore'; //eslint-disable-line
import Routes from './routes'; //eslint-disable-line
import '../assets/sass/index.scss'; //eslint-disable-line

function renderApp() {
  // This code starts up the React app when it runs in a browser. It sets up the routing configuration
  // and  injects the app into a DOM element.
  ReactDOM.render(
    <Provider store={store}>
      <Routes />
    </Provider>,
    document.getElementById('root')
  );
}

renderApp();
