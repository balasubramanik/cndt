import * as CLAIMINVOICEDETAILS from '../actionTypes/ClaimInvoiceDetails';
import * as claimInvoiceDetailsActionCreators from '../actionCreators/ClaimInvoiceDetails';

describe('Claim Invoice Details Actions for Get Claim Invoice List', () => {
  it('should return getClaimsInvoiceList action type', () => {
    const getClaimsInvoiceList = claimInvoiceDetailsActionCreators.getClaimsInvoiceList();
    expect(getClaimsInvoiceList.type).toEqual(CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST);
  });

  it('should return getClaimsInvoiceListSuccess action type', () => {
    const getClaimsInvoiceListSuccess = claimInvoiceDetailsActionCreators.getClaimsInvoiceListSuccess();
    expect(getClaimsInvoiceListSuccess.type).toEqual(CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST_SUCCESS);
  });

  it('should return getClaimsInvoiceListFailure action type', () => {
    const getClaimsInvoiceListFailure = claimInvoiceDetailsActionCreators.getClaimsInvoiceListFailure();
    expect(getClaimsInvoiceListFailure.type).toEqual(CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST_FAILURE);
  });

  it('should return getClaimsInvoiceListClear action type', () => {
    const getClaimsInvoiceListClear = claimInvoiceDetailsActionCreators.getClaimsInvoiceListClear();
    expect(getClaimsInvoiceListClear.type).toEqual(CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST_CLEAR);
  });
});

describe('Claim Invoice Details Actions for Download file', () => {
  it('should return downloadInvoice action type', () => {
    const downloadInvoice = claimInvoiceDetailsActionCreators.downloadInvoice();
    expect(downloadInvoice.type).toEqual(CLAIMINVOICEDETAILS.DOWNLOAD_FILE);
  });

  it('should return downloadInvoiceSuccess action type', () => {
    const downloadInvoiceSuccess = claimInvoiceDetailsActionCreators.downloadInvoiceSuccess();
    expect(downloadInvoiceSuccess.type).toEqual(CLAIMINVOICEDETAILS.DOWNLOAD_FILE_SUCCESS);
  });

  it('should return downloadInvoiceFailure action type', () => {
    const downloadInvoiceFailure = claimInvoiceDetailsActionCreators.downloadInvoiceFailure();
    expect(downloadInvoiceFailure.type).toEqual(CLAIMINVOICEDETAILS.DOWNLOAD_FILE_FAILURE);
  });

  it('should return downloadInvoiceClear action type', () => {
    const downloadInvoiceClear = claimInvoiceDetailsActionCreators.downloadInvoiceClear();
    expect(downloadInvoiceClear.type).toEqual(CLAIMINVOICEDETAILS.DOWNLOAD_FILE_CLEAR);
  });
});

describe('Claim Invoice Details Actions for Clear Error', () => {
  it('should return claimsInvoiceClearError action type', () => {
    const claimsInvoiceClearError = claimInvoiceDetailsActionCreators.claimsInvoiceClearError();
    expect(claimsInvoiceClearError.type).toEqual(CLAIMINVOICEDETAILS.CLAIMS_INVOICE_CLEAR_ERROR);
  });
});
