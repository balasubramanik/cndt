import * as TERMSANDCONDITIONS from '../actionTypes/TermsAndConditions';
import * as termsandconditionsActionCreators from '../actionCreators/TermsAndConditions';

describe('TermsAndConditions Actions', () => {
  it('should returns submitTermsAndConditions action type', () => {
    const submitTermsAndConditions = termsandconditionsActionCreators.submitTermsAndConditions();
    expect(submitTermsAndConditions.type).toEqual(TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS);
  });

  it('should returns submitTermsAndConditionsSuccess action type', () => {
    const submitTermsAndConditionsSuccess = termsandconditionsActionCreators.submitTermsAndConditionsSuccess();
    expect(submitTermsAndConditionsSuccess.type).toEqual(TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_SUCCESS);
  });

  it('should returns submitTermsAndConditionsFailure action type', () => {
    const submitTermsAndConditionsFailure = termsandconditionsActionCreators.submitTermsAndConditionsFailure();
    expect(submitTermsAndConditionsFailure.type).toEqual(TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE);
  });
});
