import * as SUGGESTION from '../actionTypes/SearchBy';

export function getSuggestion(searchText = '', searchByName) {
  return {
    type: SUGGESTION.GET_SUGGESTION,
    searchByName,
    searchText,
  };
}

export function getSuggestionSuccess(suggestions, searchByName) {
  return {
    type: SUGGESTION.GET_SUGGESTION_SUCCESS,
    searchByName,
    suggestions,
  };
}

export function getSuggestionFailure(error, searchByName) {
  return {
    type: SUGGESTION.GET_SUGGESTION_FAILURE,
    searchByName,
    error,
  };
}

export function updateSearchText(searchText, searchByName) {
  return {
    type: SUGGESTION.UPDATE_SEARCH_TEXT,
    searchByName,
    searchText,
  };
}

export function clearSearchText(searchByName) {
  return {
    type: SUGGESTION.CLEAR_SEARCH_TEXT,
    searchByName,
  };
}
