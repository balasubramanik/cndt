import * as CLAIMSLIST from '../actionTypes/ClaimsList';
import * as ClaimsListActionCreators from '../actionCreators/ClaimsList';


describe('Claims List Actions', () => {
  it('should return getClaimsList action type', () => {
    const getClaimsList = ClaimsListActionCreators.getClaimsList(CLAIMSLIST.GET_CLAIMS_LIST);
    expect(getClaimsList.type).toEqual(CLAIMSLIST.GET_CLAIMS_LIST);
  });
  it('should return getClaimsList action type with no request', () => {
    const getClaimsList = ClaimsListActionCreators.getClaimsList();
    expect(getClaimsList.type).toEqual(CLAIMSLIST.GET_CLAIMS_LIST);
  });
  it('should return get Claims list success action type', () => {
    const getClaimsListSuccess = ClaimsListActionCreators.getClaimsListSuccess();
    expect(getClaimsListSuccess.type).toEqual(CLAIMSLIST.GET_CLAIMS_LIST_SUCCESS);
  });

  it('should return get Claims list failure action type', () => {
    const getClaimsListFailure = ClaimsListActionCreators.getClaimsListFailure();
    expect(getClaimsListFailure.type).toEqual(CLAIMSLIST.GET_CLAIMS_LIST_FAILURE);
  });

  it('should return get Claims list clear action type', () => {
    const getClaimsListClear = ClaimsListActionCreators.getClaimsListClear();
    expect(getClaimsListClear.type).toEqual(CLAIMSLIST.GET_CLAIMS_LIST_CLEAR);
  });
  it('should return getExportCSVSuccess action type', () => {
    const getExportCSVSuccess = ClaimsListActionCreators.getExportCSVSuccess(CLAIMSLIST.GET_EXPORT_CSV_SUCCESS);
    expect(getExportCSVSuccess.type).toEqual(CLAIMSLIST.GET_EXPORT_CSV_SUCCESS);
  });

  it('should return claimsListClearError action type', () => {
    const claimsListClearError = ClaimsListActionCreators.claimsListClearError();
    expect(claimsListClearError.type).toEqual(CLAIMSLIST.CLAIM_LIST_CLEAR_ERROR);
  });
});
