import * as REDEEMPOINTS from '../actionTypes/RedeemPoints';
import * as redeemPointsActionCreators from '../actionCreators/RedeemPoints';

describe('Redeem points Actions', () => {
  it('should returns get catalogue action type', () => {
    const getGiftCatalogue = redeemPointsActionCreators.getGiftCatalogue();
    expect(getGiftCatalogue.type).toEqual(REDEEMPOINTS.GET_GIFT_CATALOGUE);
  });

  it('should returns get catalogue success action type', () => {
    const getGiftCatalogueSuccess = redeemPointsActionCreators.getGiftCatalogueSuccess();
    expect(getGiftCatalogueSuccess.type).toEqual(REDEEMPOINTS.GET_GIFT_CATALOGUE_SUCCESS);
  });

  it('should returns get catalogue failure action type', () => {
    const getGiftCatalogueFailure = redeemPointsActionCreators.getGiftCatalogueFailure();
    expect(getGiftCatalogueFailure.type).toEqual(REDEEMPOINTS.GET_GIFT_CATALOGUE_FAILURE);
  });

  it('should returns add to cart action type', () => {
    const addToCart = redeemPointsActionCreators.addToCart();
    expect(addToCart.type).toEqual(REDEEMPOINTS.ADD_TO_CART);
  });

  it('should returns add to cart success action type', () => {
    const addToCartSuccess = redeemPointsActionCreators.addToCartSuccess();
    expect(addToCartSuccess.type).toEqual(REDEEMPOINTS.ADD_TO_CART_SUCCESS);
  });

  it('should returns add to cart failure action type', () => {
    const addToCartFailure = redeemPointsActionCreators.addToCartFailure();
    expect(addToCartFailure.type).toEqual(REDEEMPOINTS.ADD_TO_CART_FAILURE);
  });

  it('should returns closePopup action type', () => {
    const closePopup = redeemPointsActionCreators.closePopup();
    expect(closePopup.type).toEqual(REDEEMPOINTS.CLOSE_POPUP);
  });

  it('should returns clearErrors action type', () => {
    const clearErrors = redeemPointsActionCreators.clearErrors();
    expect(clearErrors.type).toEqual(REDEEMPOINTS.CLEAR_ERRORS);
  });

  it('should returns updateCardInformation action type', () => {
    const updateCardInformation = redeemPointsActionCreators.updateCardInformation();
    expect(updateCardInformation.type).toEqual(REDEEMPOINTS.UPDATE_CARD_INFORMATION);
  });

  it('should returns getGiftCardDetails action type', () => {
    const getGiftCardDetails = redeemPointsActionCreators.getGiftCardDetails();
    expect(getGiftCardDetails.type).toEqual(REDEEMPOINTS.GET_GIFT_CARD_DETAILS);
  });

  it('should returns getGiftCardDetailsSuccess action type', () => {
    const getGiftCardDetailsSuccess = redeemPointsActionCreators.getGiftCardDetailsSuccess();
    expect(getGiftCardDetailsSuccess.type).toEqual(REDEEMPOINTS.GET_GIFT_CARD_DETAILS_SUCCESS);
  });

  it('should returns getGiftCardDetailsFailure action type', () => {
    const getGiftCardDetailsFailure = redeemPointsActionCreators.getGiftCardDetailsFailure();
    expect(getGiftCardDetailsFailure.type).toEqual(REDEEMPOINTS.GET_GIFT_CARD_DETAILS_FAILURE);
  });

  it('should returns resetAllGiftItems action type', () => {
    const resetAllGiftItems = redeemPointsActionCreators.resetAllGiftItems();
    expect(resetAllGiftItems.type).toEqual(REDEEMPOINTS.RESET_GIFT_ITEMS);
  });
});
