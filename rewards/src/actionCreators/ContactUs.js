import * as CONTACTUS from '../actionTypes/ContactUs';

export function submitContactUs(info) {
  return {
    type: CONTACTUS.SUBMIT_CONTACTUS,
    info,
  };
}

export function submitContactUsSuccess(data) {
  return {
    type: CONTACTUS.SUBMIT_CONTACTUS_SUCCESS,
    data,
  };
}

export function submitContactUsFailure(error) {
  return {
    type: CONTACTUS.SUBMIT_CONTACTUS_FAILURE,
    error,
  };
}

export function clearErrors() {
  return {
    type: CONTACTUS.CLEAR_ERRORS,
  };
}

export function resetContactUs() {
  return {
    type: CONTACTUS.RESET_CONTACTUS,
  };
}
