import * as CONTRACTORLIST from '../actionTypes/ContractorList';

export function getContractorList(info) {
  return {
    type: CONTRACTORLIST.GET_CONTRACTOR_LIST,
    info,
  };
}

export function getContractorListSuccess(data) {
  return {
    type: CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS,
    data,
  };
}

export function getContractorListFailure(error) {
  return {
    type: CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE,
    error,
  };
}
export function achToggle(index, selectedRow, email) {
  return {
    type: CONTRACTORLIST.ACH_TOGGLE,
    index,
    selectedRow,
    email,
  };
}
export function achToggleSuccess(successFlag) {
  return {
    type: CONTRACTORLIST.ACH_TOGGLE_SUCCESS,
    successFlag,
  };
}
export function achToggleFailure(error) {
  return {
    type: CONTRACTORLIST.ACH_TOGGLE_FAILURE,
    error,
  };
}

export function submitDetailedReport(data) {
  return {
    type: CONTRACTORLIST.SUBMIT_DETAILED_REPORT,
    data,
  };
}

export function submitDetailedReportSuccess(data) {
  return {
    type: CONTRACTORLIST.SUBMIT_DETAILED_REPORT_SUCCESS,
    data,
  };
}

export function submitDetailedReportFailure(error) {
  return {
    type: CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE,
    error,
  };
}

export function closeTogglePopup() {
  return {
    type: CONTRACTORLIST.CLOSE_TOGGLE_POPUP,
  };
}

export function closeDetailedPopup() {
  return {
    type: CONTRACTORLIST.CLOSE_DETAILED_POPUP,
  };
}

export function updatePageDetails(details) {
  return {
    type: CONTRACTORLIST.UPDATE_PAGE_DETAILS,
    details,
  };
}

export function resetInitialState() {
  return {
    type: CONTRACTORLIST.RESETINITIALSTATE,
  };
}
