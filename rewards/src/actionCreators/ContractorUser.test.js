import * as CONTRACTOR from '../actionTypes/ContractorUser';
import * as contractorUserActionCreators from '../actionCreators/ContractorUser';

describe('Contractor User Actions', () => {
  it('should returns get users action type', () => {
    const getUsers = contractorUserActionCreators.getUsers(CONTRACTOR.GET_USERS);
    expect(getUsers.type).toEqual(CONTRACTOR.GET_USERS);
  });

  it('should returns get user success action type', () => {
    const getUsersSuccess = contractorUserActionCreators.getUsersSuccess();
    expect(getUsersSuccess.type).toEqual(CONTRACTOR.GET_USERS_SUCCESS);
  });

  it('should returns get user failure action type', () => {
    const getUsersFailure = contractorUserActionCreators.getUsersFailure();
    expect(getUsersFailure.type).toEqual(CONTRACTOR.GET_USERS_FAILURE);
  });
});
