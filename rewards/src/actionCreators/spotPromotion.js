import * as SPOTPROMOTION from '../actionTypes/spotPromotion';

export function getPromotion(reqBody = {}) {
  return {
    type: SPOTPROMOTION.GET_PROMOTION_LIST,
    reqBody,
  };
}

export function getPromotionSuccess(data) {
  return {
    type: SPOTPROMOTION.GET_PROMOTION_LIST_SUCCESS,
    data,
  };
}

export function getPromotionFailure(error) {
  return {
    type: SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE,
    error,
  };
}

export function getPromotionListClear() {
  return {
    type: SPOTPROMOTION.GET_PROMOTION_LIST_CLEAR,
  };
}
