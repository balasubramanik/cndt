import * as ORDERDETAILS from '../actionTypes/OrderDetails';
import * as OrderDetailsActionCreators from '../actionCreators/OrderDetails';

describe('Order Details Actions', () => {
  it('should return getProduct action type', () => {
    const getProduct = OrderDetailsActionCreators.getProduct(ORDERDETAILS.GET_LIST_OF_PRODUCT);
    expect(getProduct.type).toEqual(ORDERDETAILS.GET_LIST_OF_PRODUCT);
  });

  it('should return getProductSuccess action type', () => {
    const getProductSuccess = OrderDetailsActionCreators.getProductSuccess();
    expect(getProductSuccess.type).toEqual(ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS);
  });

  it('should return getProductFailure action type', () => {
    const getProductFailure = OrderDetailsActionCreators.getProductFailure();
    expect(getProductFailure.type).toEqual(ORDERDETAILS.GET_LIST_OF_PRODUCT_FAILURE);
  });

  it('should return getProductClear action type', () => {
    const getProductClear = OrderDetailsActionCreators.getProductClear();
    expect(getProductClear.type).toEqual(ORDERDETAILS.GET_LIST_OF_PRODUCT_CLEAR);
  });

  it('should return updatePageDetails action type', () => {
    const updatePageDetails = OrderDetailsActionCreators.updatePageDetails();
    expect(updatePageDetails.type).toEqual(ORDERDETAILS.UPDATE_PAGE_DETAILS);
  });
});

