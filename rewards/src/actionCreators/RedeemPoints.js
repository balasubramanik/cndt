import * as REDEEMPOINTS from '../actionTypes/RedeemPoints';

export function getGiftCatalogue(category, key) {
  return {
    type: REDEEMPOINTS.GET_GIFT_CATALOGUE,
    category,
    key,
  };
}

export function getGiftCatalogueSuccess(key, category, data) {
  return {
    type: REDEEMPOINTS.GET_GIFT_CATALOGUE_SUCCESS,
    key,
    category,
    data,
  };
}

export function getGiftCatalogueFailure(key, error) {
  return {
    type: REDEEMPOINTS.GET_GIFT_CATALOGUE_FAILURE,
    key,
    error,
  };
}

export function getGiftCardDetails(productId) {
  return {
    type: REDEEMPOINTS.GET_GIFT_CARD_DETAILS,
    productId,
  };
}

export function getGiftCardDetailsSuccess(productId, data) {
  return {
    type: REDEEMPOINTS.GET_GIFT_CARD_DETAILS_SUCCESS,
    productId,
    data,
  };
}

export function getGiftCardDetailsFailure(error) {
  return {
    type: REDEEMPOINTS.GET_GIFT_CARD_DETAILS_FAILURE,
    error,
  };
}

export function addToCart(category, data) {
  return {
    type: REDEEMPOINTS.ADD_TO_CART,
    category,
    data,
  };
}

export function addToCartSuccess(data) {
  return {
    type: REDEEMPOINTS.ADD_TO_CART_SUCCESS,
    data,
  };
}

export function addToCartFailure(error) {
  return {
    type: REDEEMPOINTS.ADD_TO_CART_FAILURE,
    error,
  };
}

export function closePopup() {
  return {
    type: REDEEMPOINTS.CLOSE_POPUP,
  };
}

export function clearErrors() {
  return {
    type: REDEEMPOINTS.CLEAR_ERRORS,
  };
}

export function updateCardInformation(id, amount, fee, variantId, productImage) {
  return {
    type: REDEEMPOINTS.UPDATE_CARD_INFORMATION,
    id,
    amount,
    fee,
    variantId,
    productImage,
  };
}

export function resetAllGiftItems() {
  return {
    type: REDEEMPOINTS.RESET_GIFT_ITEMS,
  };
}
