import * as CLAIMDETAILS from '../actionTypes/ClaimDetails';
import * as ClaimDetailsActionCreators from '../actionCreators/ClaimDetails';

describe('Claim Details Actions', () => {
  it('should return get invoice list action type', () => {
    const getInvoiceList = ClaimDetailsActionCreators.getInvoiceList(CLAIMDETAILS.GET_INVOICE_LIST);
    expect(getInvoiceList.type).toEqual(CLAIMDETAILS.GET_INVOICE_LIST);
  });

  it('should return get invoice list action type when claim ID not passed', () => {
    const getInvoiceList = ClaimDetailsActionCreators.getInvoiceList();
    expect(getInvoiceList.type).toEqual(CLAIMDETAILS.GET_INVOICE_LIST);
  });

  it('should return get invoice list success action type', () => {
    const getInvoiceListSuccess = ClaimDetailsActionCreators.getInvoiceListSuccess();
    expect(getInvoiceListSuccess.type).toEqual(CLAIMDETAILS.GET_INVOICE_LIST_SUCCESS);
  });

  it('should return get invoice list failure action type', () => {
    const getInvoiceListFailure = ClaimDetailsActionCreators.getInvoiceListFailure();
    expect(getInvoiceListFailure.type).toEqual(CLAIMDETAILS.GET_INVOICE_LIST_FAILURE);
  });

  it('should return get invoice list clear action type', () => {
    const getInvoiceListClear = ClaimDetailsActionCreators.getInvoiceListClear();
    expect(getInvoiceListClear.type).toEqual(CLAIMDETAILS.GET_INVOICE_LIST_CLEAR);
  });
  it('should return update page details action type', () => {
    const updatePageDetails = ClaimDetailsActionCreators.updatePageDetails();
    expect(updatePageDetails.type).toEqual(CLAIMDETAILS.UPDATE_PAGE_DETAILS);
  });
});
