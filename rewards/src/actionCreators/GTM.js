import * as GTM from '../actionTypes/GTM';

export const gtmRoofInstaller = (value) => ({
  type: GTM.GTM_ROOFINSTALLER,
  EventCategory: 'Registration Questions',
  EventActions: 'Does your company Install Roofs?',
  EventLabel: value,
});

export const gtmContactusClaimDetails = () => ({
  type: GTM.GTM_CONTACTUS_CLAIM_DETAILS,
  EventCategory: 'Contact Us',
  EventActions: 'Claim Details',
  EventLabel: 'Claim Details',
});

export const gtmContactusClaimStatus = () => ({
  type: GTM.GTM_CONTACTUS_CLAIM_STATUS,
  EventCategory: 'Contact Us',
  EventActions: 'Claim Status',
  EventLabel: 'Claim Status',
});

export const contactusFormSubmit = () => ({
  type: GTM.GTM_SUBMIT_CONTACTUS_FORM,
  EventCategory: 'Contact us',
  EventActions: 'Submit Form',
  EventLabel: 'Claim Contact Form submitted',
});

export const gtmClaimDetilsToClaimStatus = () => ({
  type: GTM.GTM_CLAIM_DETAILS_CLAIM_STATUS,
  EventCategory: 'Claim Details',
  EventActions: 'Claim Status',
  EventLabel: 'Claim Status',
});

export const gtmClaimDetailsToContactus = (value) => ({
  type: GTM.GTM_CLAIM_DETAILS_CONTACTUS,
  EventCategory: 'Claim Details',
  EventActions: 'Contact Us',
  EventLabel: `${value} - Contact Us`,
});

export const gtmClaimSearch = (value) => ({
  type: GTM.GTM_SEARCH_CLAIM_DETAILS,
  EventCategory: 'Claim Details',
  EventActions: 'Claim Search',
  EventLabel: value,
});

export const gtmClaimDetailsCSVExport = () => ({
  type: GTM.GTM_CLAIM_DETAILS_CSV_EXPORT,
  EventCategory: 'Claim Details',
  EventActions: 'CSV Export',
  EventLabel: 'List of Invoice(s)',
});

export const gtmMyPlan = () => ({
  type: GTM.GTM_MYPLAN,
  EventCategory: 'What Qualifies',
  EventActions: 'My Plan',
  EventLabel: 'View My Plan',
});

export const gtmSpotPromotions = () => ({
  type: GTM.GTM_SPOTPROMOTIONS,
  EventCategory: 'What Qualifies',
  EventActions: 'Spot Promotions',
  EventLabel: 'View Spot Promotions',
});

export const gtmCategoryFilter = (value) => ({
  type: GTM.GTM_CATEGORY_FILTER,
  EventCategory: 'What Qualifies',
  EventActions: 'Category Filter',
  EventLabel: value,
});

export const gtmWhatQualifiesCSVExport = (value) => ({
  type: GTM.GTM_WHATQUALIFIES_CSV_EXPORT,
  EventCategory: 'What Qualifies',
  EventActions: 'CSV Export',
  EventLabel: value,
});

export const gtmCategoryMenu = (value1, value2) => ({
  type: GTM.GTM_CATEGORY_MENU,
  EventCategory: 'What Qualifies - My Plan',
  EventActions: value2,
  EventLabel: value1,
});

export const gtmSpotPromotionMenu = (value1, value2) => ({
  type: GTM.GTM_SPOTPROMOTION_MENU,
  EventCategory: 'What Qualifies - Spot Promotions',
  EventActions: value2,
  EventLabel: value1,
});

export const gtmSpotPromotionDetailsMenu = (value) => ({
  type: GTM.GTM_SPOTPROMOTION_DETAILS_MENU,
  EventCategory: 'What Qualifies - Spot Promotions',
  EventActions: 'Details',
  EventLabel: 'View Details' - value,
});

export const gtmStateMore = (value) => ({
  type: GTM.GTM_STATE_MORE,
  EventCategory: 'Promotion Details',
  EventActions: 'View More States',
  EventLabel: value,
});

export const gtmViewEligibleProducts = (value) => ({
  type: GTM.GTM_VIEW_ELIGIBLE_PRODUCTS,
  EventCategory: 'Promotion Details',
  EventActions: 'View Eligible Products',
  EventLabel: value,
});

export const gtmSubmitInvoiceLink = (value) => ({
  type: GTM.GTM_SUBMIT_INVOICE_LINK,
  EventCategory: 'Promotion Details',
  EventActions: 'Submit Invoice',
  EventLabel: value,
});

export const gtmOrderDetailsToPointsHistory = () => ({
  type: GTM.GTM_ORDER_DETAILS_POINT_HISTORY,
  EventCategory: 'Order Details',
  EventActions: 'Points History',
  EventLabel: 'View Points History',
});

export const gtmOrderDetailsSearch = (value) => ({
  type: GTM.GTM_ORDER_DETAILS_SEARCH_PRODUCT,
  EventCategory: 'Order Details',
  EventActions: 'Order Search',
  EventLabel: value,
});

export const gtmOrderDetailsCSVExport = () => ({
  type: GTM.GTM_ORDER_DETAILS_CSV,
  EventCategory: 'Order Details',
  EventActions: 'CSV Export',
  EventLabel: 'List of Products',
});

export const gtmClaimStatusAdvanceSearchToggle = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Advance Search',
  EventLabel: value,
});

export const gtmClaimStatusViewBySelection = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'View By',
  EventLabel: value,
});

export const gtmClaimStatusSearchClaimNo = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Search Claim Number',
  EventLabel: value,
});

export const gtmClaimStatusSearchProduct = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Product Filter',
  EventLabel: value,
});

export const gtmClaimStatusUserNameSelection = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'User Name',
  EventLabel: value,
});

export const gtmClaimStatusMatchItemSelection = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Match',
  EventLabel: value,
});

export const gtmClaimStatusStatusItemSelection = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Status',
  EventLabel: value,
});

export const gtmClaimStatusStartDateSelection = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'From Date',
  EventLabel: value,
});

export const gtmClaimStatusEndDateSelection = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'To Date',
  EventLabel: value,
});

export const gtmClaimStatusClearBtnClicked = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Clear',
  EventLabel: value,
});

export const gtmClaimStatusApplyBtnClicked = (value) => ({
  type: GTM.GTM_CLAIMSTATUSFILTERS,
  EventCategory: 'Claim Status Filters',
  EventActions: 'Apply',
  EventLabel: value,
});

export const gtmClaimStatusViewInvoice = (value) => ({
  type: GTM.GTM_LISTOFCLAIMS,
  EventCategory: 'Claim Status',
  EventActions: 'View Invoice',
  EventLabel: value,
});

export const gtmClaimStatusViewClaimDetails = (value) => ({
  type: GTM.GTM_LISTOFCLAIMS,
  EventCategory: 'Claim Status',
  EventActions: 'View Claim Details',
  EventLabel: value,
});

export const gtmClaimStatusCSVExport = (value) => ({
  type: GTM.GTM_LISTOFCLAIMS,
  EventCategory: 'Claim Status',
  EventActions: 'View Claim Details',
  EventLabel: value,
});

export const gtmUploadInvoiceOptionsMenu = (value) => ({
  type: GTM.GTM_UPLOADINVOICE_OPTIONSMENU,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Options menu item',
  EventLabel: value,
});

export const gtmUploadInvoice = () => ({
  type: GTM.GTM_UPLOADINVOICE,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Upload Invoices',
  EventLabel: 'Upload process begun',
});

export const gtmInvoiceCount = (value) => ({
  type: GTM.GTM_INVOICECOUNT,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Invoice Count',
  EventLabel: value,
});

export const gtmTotalFileSize = (value) => ({
  type: GTM.GTM_TOTALFILESIZE,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Total file size',
  EventLabel: value,
});

export const gtmResultMessage = (value) => ({
  type: GTM.GTM_SUBMITRESULTMESSAGE,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Submit Result',
  EventLabel: value,
});

export const gtmMailClaimForm = () => ({
  type: GTM.GTM_MAILCLAIMFORM,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Print Claim Form',
  EventLabel: 'Mail Claim Form',
});

export const gtmDistributorClaimForm = () => ({
  type: GTM.GTM_DISTRIBUTORCLAIMFORM,
  EventCategory: 'Submit an Invoice',
  EventActions: 'Print Claim Form',
  EventLabel: 'Distributor Claim Form',
});

export const gtmSuccessfulInvoice = () => ({
  type: GTM.GTM_SUCCESSINVOICEUPLOAD,
  EventCategory: 'Successful Invoice',
  EventActions: 'Claim Status',
  EventLabel: 'Check Claim Status',
});

export const gtmCartDelete = (value) => ({
  type: GTM.GTM_DELETE_CART,
  EventCategory: 'Cart Summary',
  EventActions: 'Delete Item',
  EventLabel: value,
});

export const gtmUpdateShippingAddress = (value) => ({
  type: GTM.GTM_UPDATE_SHIPPING_ADDRESS,
  EventCategory: 'Cart Summary',
  EventActions: 'Update Address',
  EventLabel: value,
});

export const gtmCardValue = (value) => ({
  type: GTM.GTM_CARD_VALUE,
  EventCategory: 'Cart Summary',
  EventActions: 'Card Value',
  EventLabel: value,
});

export const gtmSubmitOrderTotal = (value) => ({
  type: GTM.GTM_SUBMIT_ORDER_TOTAL,
  EventCategory: 'Cart Summary',
  EventActions: 'Submit Order',
  EventLabel: value,
});

export const gtmSubmitOrderFeeTotal = (value) => ({
  type: GTM.GTM_SUBMIT_ORDER_FEE_TOTAL,
  EventCategory: 'Cart Summary',
  EventActions: 'Submit Order - Fees',
  EventLabel: value,
});

export const gtmSubmitOrderProductTotal = (value) => ({
  type: GTM.GTM_SUBMIT_ORDER_ITEM_TOTAL,
  EventCategory: 'Cart Summary',
  EventActions: 'Submit Order - Product Value',
  EventLabel: value,
});

export const gtmBusinessResidentialPercent = (value) => ({
  type: GTM.GTM_RESIDENTIAL_PERCENTAGE,
  EventCategory: 'Registration Questions',
  EventActions: 'What Percentage of your Business is Residential?',
  EventLabel: value,
});

export const gtmBusinessCommercialPercent = (value) => ({
  type: GTM.GTM_COMMERCIAL_PERCENTAGE,
  EventCategory: 'Registration Questions',
  EventActions: 'What Percentage of your Business is Commercial?',
  EventLabel: value,
});

export const gtmAnnualRoofInstall = (value) => ({
  type: GTM.GTM_ANNUAL_INSTALL_INSTALL,
  EventCategory: 'Registration Questions',
  EventActions: 'Number of roofs installed annually?',
  EventLabel: value,
});

export const gtmPrimaryBusinessType = (value) => ({
  type: GTM.GTM_PRIMARY_BUSINESS_TYPE,
  EventCategory: 'Registration Questions',
  EventActions: 'What is your primary Business Type?',
  EventLabel: value,
});

export const gtmMaterialBuyingOutlet = (value) => ({
  type: GTM.GTM_MATERIAL_BUYING_OUTLET,
  EventCategory: 'Registration Questions',
  EventActions: 'Where do you primarily purchase roofing materials?',
  EventLabel: value,
});

export const gtmServicesOffered = (value) => ({
  type: GTM.GTM_SERVICES_OFFERED,
  EventCategory: 'Registration Questions',
  EventActions: 'What other services do you offer?',
  EventLabel: value,
});

export const gtmGafProductsBought = (value) => ({
  type: GTM.GTM_GAF_PRODUCTS_BOUGHT,
  EventCategory: 'Registration Questions',
  EventActions: 'What GAF Products are you buying?',
  EventLabel: value,
});

export const gtmEmailVerificationSuccess = () => ({
  type: GTM.GTM_EMAIL_VERIFICATION_SUCCESS,
  EventCategory: 'Registration Progress',
  EventActions: 'Next',
  EventLabel: 'Step 1 Complete - Email Verification Sent',
});

export const gtmRegistrationSuccess = () => ({
  type: GTM.GTM_REGISTRATION_SUCCESS,
  EventCategory: 'Registration Progress',
  EventActions: 'Register Now',
  EventLabel: 'Step 2 Complete - Registration',
});

export const gtmDebitCardSuccess = (value) => ({
  type: GTM.GTM_DEBITCARD_SUCCESS,
  EventCategory: 'Debit Card',
  EventActions: 'Add to Cart',
  EventLabel: value,
});

export const gtmAchSuccess = (value) => ({
  type: GTM.GTM_ACH_SUCCESS,
  EventCategory: 'Check/ACH',
  EventActions: 'Add to Cart',
  EventLabel: value,
});

export const gtmGiftCardType = (brand, value) => ({
  type: GTM.GTM_GIFTCARD_TYPE,
  EventCategory: 'Gift Card Redemption',
  EventActions: brand,
  EventLabel: value,
});

export const gtmGiftCardAmount = (brand, value) => ({
  type: GTM.GTM_GIFTCARD_AMOUNT,
  EventCategory: 'Gift Card Redemption',
  EventActions: brand,
  EventLabel: value,
});

export const gtmGiftCardQuantity = (brand, value) => ({
  type: GTM.GTM_GIFTCARD_QUANTITY,
  EventCategory: 'Gift Card Redemption',
  EventActions: brand,
  EventLabel: value,
});

export const gtmProceedToCheckoutItemCount = (value) => ({
  type: GTM.GTM_PROCEED_TO_CHECKOUT_ITEM_COUNT,
  EventCategory: 'Redeem Points',
  EventActions: 'Proceed to Checkout - Item count',
  EventLabel: value,
});

export const gtmProceedToCheckoutOrderTotal = (value) => ({
  type: GTM.GTM_PROCEED_TO_CHECKOUT_ORDER_TOTAL,
  EventCategory: 'Redeem Points',
  EventActions: 'Proceed to Checkout - Order Total',
  EventLabel: value,
});

export const gtmProceedToCheckout = () => ({
  type: GTM.GTM_PROCEED_TO_CHECKOUT,
  EventCategory: 'Redeem Points',
  EventActions: 'Proceed to Checkout',
  EventLabel: 'Proceed to Checkout',
});

export const gtmGiftCardCategory = (value) => ({
  type: GTM.GTM_GIFTCARD_CATEGORY,
  EventCategory: 'Gift Card',
  EventActions: 'Gift Card Category',
  EventLabel: value,
});

export const gtmGiftCardDetailView = (value) => ({
  type: GTM.GTM_GIFTCARD_DETAILVIEW,
  EventCategory: 'Gift Card',
  EventActions: 'Detailed View',
  EventLabel: value,
});

export const gtmFilterByEcard = (value) => ({
  type: GTM.GTM_GIFTCARD_FILTERBY_ECARD,
  EventCategory: 'Card Type',
  EventActions: 'e-Card',
  EventLabel: value,
});

export const gtmFilterByPhysicalCard = (value) => ({
  type: GTM.GTM_GIFTCARD_FILTERBY_PHYSICALCARD,
  EventCategory: 'Card Type',
  EventActions: 'Phsyical Card',
  EventLabel: value,
});

export const gtmGiftCardSearch = (value) => ({
  type: GTM.GTM_GIFTCARD_SEARCH,
  EventCategory: 'Gift Card',
  EventActions: 'Gift Card Search',
  EventLabel: value,
});

export const gtmPointsHistoryAdvanceSearchToggle = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'Advance Search',
  EventLabel: value,
});

export const gtmPointsHistoryViewBySelection = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'View By',
  EventLabel: value,
});

export const gtmPointsHistorySearchTransactionNo = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'Search Claim/Order Number',
  EventLabel: value,
});

export const gtmPointsHistoryUserNameSelection = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'User Name',
  EventLabel: value,
});

export const gtmPointsHistoryMatchItemSelection = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'Match',
  EventLabel: value,
});

export const gtmPointsHistoryTypeSelection = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'Type',
  EventLabel: value,
});

export const gtmPointsHistoryStartDateSelection = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'From Date',
  EventLabel: value,
});

export const gtmPointsHistoryEndDateSelection = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'To Date',
  EventLabel: value,
});

export const gtmPointsHistoryClearBtnClicked = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'Clear',
  EventLabel: value,
});

export const gtmPointsHistoryApplyBtnClicked = (value) => ({
  type: GTM.GTM_POINTSHISTORY_FILTERS,
  EventCategory: 'Points History Filters',
  EventActions: 'Apply',
  EventLabel: value,
});

export const gtmPointsHistoryViewTransactionDetails = (value) => ({
  type: GTM.GTM_LISTOFTRANSACTIONS,
  EventCategory: 'List of Transactions',
  EventActions: 'View Transaction Details',
  EventLabel: value,
});

export const gtmPointsHistoryCSVExport = (value) => ({
  type: GTM.GTM_LISTOFTRANSACTIONS,
  EventCategory: 'List of Transactions',
  EventActions: 'CSV Export',
  EventLabel: value,
});

export const gtmRedemptionDetails = (value) => ({
  type: GTM.GTM_REDEMPTION_DETAILS,
  EventCategory: 'Order Received',
  EventActions: 'Click Here for redemption details',
  EventLabel: value,
});

export const gtmCartQuantity = (value) => ({
  type: GTM.GTM_CART_QUANTITY,
  EventCategory: 'Cart Summary',
  EventActions: 'Quantity',
  EventLabel: value,
});

export const gtmHeaderMenuItem = (value) => ({
  type: GTM.GTM_HEADER_MENU_ITEM,
  EventCategory: 'Header Navigation',
  EventActions: 'Header Menu Item',
  EventLabel: value,
});

export const gtmWhatQualifies = () => ({
  type: GTM.GTM_WHAT_QUALIFIES,
  EventCategory: 'GAF Rewards Homepage Navigation',
  EventActions: 'What Qualifies',
  EventLabel: 'View Current Promotions, Rewards, and My Plan',
});

export const gtmSubmitInvoices = () => ({
  type: GTM.GTM_SUBMIT_INVOICES,
  EventCategory: 'GAF Rewards Homepage Navigation',
  EventActions: 'Submit Invoices',
  EventLabel: 'Submit Digital Upload and Postal Mail Invoices',
});

export const gtmRedeemPoints = () => ({
  type: GTM.GTM_REDEEM_POINTS,
  EventCategory: 'GAF Rewards Homepage Navigation',
  EventActions: 'Redeem Points',
  EventLabel: 'Redeem points for valuable rewards',
});

export const gtmViewRewardPointsDetails = () => ({
  type: GTM.GTM_VIEW_REWARD_POINTS_DETAILS,
  EventCategory: 'GAF Rewards Homepage Navigation',
  EventActions: 'View Reward Points Details',
  EventLabel: 'View Reward Points Details',
});

export const gtmViewPromotionDetails = (value) => ({
  type: GTM.GTM_VIEW_PROMOTION_DETAILS,
  EventCategory: 'GAF Rewards Homepage Navigation',
  EventActions: 'View Promotion Details',
  EventLabel: value,
});
