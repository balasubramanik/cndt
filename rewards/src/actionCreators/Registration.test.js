import * as REGISTRATION from '../actionTypes/Registration';
import * as registrationPageActionCreators from '../actionCreators/Registration';

describe('Email Verification Actions', () => {
  it('should returns changeAddress action type', () => {
    const changeAddress = registrationPageActionCreators.changeAddress();
    expect(changeAddress.type).toEqual(REGISTRATION.CHANGE_ADDRESS);
  });

  it('should returns selectAddress action type', () => {
    const selectAddress = registrationPageActionCreators.selectAddress();
    expect(selectAddress.type).toEqual(REGISTRATION.SELECT_ADDRESS);
  });

  it('should returns selectServices action type', () => {
    const selectServices = registrationPageActionCreators.selectServices();
    expect(selectServices.type).toEqual(REGISTRATION.SELECT_SERVICES);
  });

  it('should returns getRegistrationInfo action type', () => {
    const getRegistrationInfo = registrationPageActionCreators.getRegistrationInfo();
    expect(getRegistrationInfo.type).toEqual(REGISTRATION.REGISTRATION_INFO);
  });

  it('should returns getRegistrationInfoSuccess action type', () => {
    const getRegistrationInfoSuccess = registrationPageActionCreators.getRegistrationInfoSuccess();
    expect(getRegistrationInfoSuccess.type).toEqual(REGISTRATION.REGISTRATION_INFO_SUCCESS);
  });

  it('should returns getRegistrationInfoFailure action type', () => {
    const getRegistrationInfoFailure = registrationPageActionCreators.getRegistrationInfoFailure();
    expect(getRegistrationInfoFailure.type).toEqual(REGISTRATION.REGISTRATION_INFO_ERROR);
  });

  it('should returns registerNow action type', () => {
    const registerNow = registrationPageActionCreators.registerNow();
    expect(registerNow.type).toEqual(REGISTRATION.REGISTER_NOW);
  });

  it('should returns registration option action type', () => {
    const options = registrationPageActionCreators.updateRegistrationOption();
    expect(options.type).toEqual(REGISTRATION.UPDATE_REGISTRATION_OPTION);
  });

  it('should returns registerNowSuccess action type', () => {
    const registerNowSuccess = registrationPageActionCreators.registerNowSuccess();
    expect(registerNowSuccess.type).toEqual(REGISTRATION.REGISTER_NOW_SUCCESS);
  });

  it('should returns registerNowFailure action type', () => {
    const registerNowFailure = registrationPageActionCreators.registerNowFailure();
    expect(registerNowFailure.type).toEqual(REGISTRATION.REGISTER_NOW_ERROR);
  });

  it('should returns Clear errors action type', () => {
    const clearErrors = registrationPageActionCreators.clearErrors();
    expect(clearErrors.type).toEqual(REGISTRATION.CLEAR_ERRORS);
  });
});
