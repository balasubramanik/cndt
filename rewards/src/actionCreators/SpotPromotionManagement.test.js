import * as SPOTPROMOTIONMANAGEMENT from '../actionTypes/SpotPromotionManagement';
import * as SpotPromotionManagementActionCreators from '../actionCreators/SpotPromotionManagement';
describe('Spot Promotion Management Actions for Save Spot Promotion', () => {
  it('should return saveSpotPromotion action type', () => {
    const saveSpotPromotion = SpotPromotionManagementActionCreators.saveSpotPromotion(SPOTPROMOTIONMANAGEMENT.SAVE_SPOT_PROMOTION);
    expect(saveSpotPromotion.type).toEqual(SPOTPROMOTIONMANAGEMENT.SAVE_SPOT_PROMOTION);
  });

  it('should return saveSpotPromotionSuccess action type', () => {
    const saveSpotPromotionSuccess = SpotPromotionManagementActionCreators.saveSpotPromotionSuccess();
    expect(saveSpotPromotionSuccess.type).toEqual(SPOTPROMOTIONMANAGEMENT.SAVE_SPOTPROMOTION_SUCCESS);
  });
  it('should return saveSpotPromotionFailure action type', () => {
    const saveSpotPromotionFailure = SpotPromotionManagementActionCreators.saveSpotPromotionFailure();
    expect(saveSpotPromotionFailure.type).toEqual(SPOTPROMOTIONMANAGEMENT.SAVE_SPOTPROMOTION_FAILURE);
  });
  it('should return savePromotionPopupClose action type', () => {
    const savePromotionPopupClose = SpotPromotionManagementActionCreators.savePromotionPopupClose();
    expect(savePromotionPopupClose.type).toEqual(SPOTPROMOTIONMANAGEMENT.SAVE_SPOTPROMOTION_POPUP_CLOSE);
  });
});

describe('Spot Promotion Management Actions for discard and reset', () => {
  it('should return discardAllData action type', () => {
    const discardAllData = SpotPromotionManagementActionCreators.discardAllData();
    expect(discardAllData.type).toEqual(SPOTPROMOTIONMANAGEMENT.DISCARD_ALL_DATA);
  });

  it('should return endDateReset action type', () => {
    const endDateReset = SpotPromotionManagementActionCreators.endDateReset();
    expect(endDateReset.type).toEqual(SPOTPROMOTIONMANAGEMENT.END_DATE_RESET);
  });

  it('should return displayStartDateReset action type', () => {
    const displayStartDateReset = SpotPromotionManagementActionCreators.displayStartDateReset();
    expect(displayStartDateReset.type).toEqual(SPOTPROMOTIONMANAGEMENT.DISPLAY_START_DATE_RESET);
  });

  it('should return displayEndDateReset action type', () => {
    const displayEndDateReset = SpotPromotionManagementActionCreators.displayEndDateReset();
    expect(displayEndDateReset.type).toEqual(SPOTPROMOTIONMANAGEMENT.DISPLAY_END_DATE_CHANGE_RESET);
  });

  it('should return familyReset action type', () => {
    const familyReset = SpotPromotionManagementActionCreators.familyReset();
    expect(familyReset.type).toEqual(SPOTPROMOTIONMANAGEMENT.PRODUCT_FAMILY_RESET);
  });
});

describe('Spot Promotion Management Actions for Get Spot Promotion Options', () => {
  it('should return getSpotPromotionOptions action type', () => {
    const getSpotPromotionOptions = SpotPromotionManagementActionCreators.getSpotPromotionOptions(SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS);
    expect(getSpotPromotionOptions.type).toEqual(SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS);
  });

  it('should return getSpotPromotionOptionSuccess action type', () => {
    const getSpotPromotionOptionSuccess = SpotPromotionManagementActionCreators.getSpotPromotionOptionSuccess();
    expect(getSpotPromotionOptionSuccess.type).toEqual(SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS_SUCCESS);
  });
  it('should return getSpotPromotionOptionFailure action type', () => {
    const getSpotPromotionOptionFailure = SpotPromotionManagementActionCreators.getSpotPromotionOptionFailure();
    expect(getSpotPromotionOptionFailure.type).toEqual(SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS_FAILURE);
  });
});

describe('Spot Promotion Management Actions for Edit Spot Promotion', () => {
  it('should return editSpotPromotion action type', () => {
    const editSpotPromotion = SpotPromotionManagementActionCreators.editSpotPromotion(SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION);
    expect(editSpotPromotion.type).toEqual(SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION);
  });

  it('should return editSpotPromotionSuccess action type', () => {
    const editSpotPromotionSuccess = SpotPromotionManagementActionCreators.editSpotPromotionSuccess();
    expect(editSpotPromotionSuccess.type).toEqual(SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION_SUCCESS);
  });
  it('should return editSpotPromotionFailure action type', () => {
    const editSpotPromotionFailure = SpotPromotionManagementActionCreators.editSpotPromotionFailure();
    expect(editSpotPromotionFailure.type).toEqual(SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION_FAILURE);
  });
});
