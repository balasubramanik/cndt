import * as CARTSUMMARY from '../actionTypes/CartSummary';
import * as CartSummaryDetails from '../actionCreators/cartSummary';

describe('CARTSUMMARY Actions for GET CartSummary Details', () => {
  it('should return getCartSummaryDetails action type', () => {
    const getcartsummary = CartSummaryDetails.getCartSummary();
    expect(getcartsummary.type).toEqual(CARTSUMMARY.GET_CART_SUMMARY);
  });

  it('should return getCartSummarySuccess action type', () => {
    const getCartSummarySuccess = CartSummaryDetails.getCartSummarySuccess();
    expect(getCartSummarySuccess.type).toEqual(CARTSUMMARY.GET_CART_SUMMARY_SUCCESS);
  });

  it('should return getCartSummaryFailure action type', () => {
    const getCartSummaryFailure = CartSummaryDetails.getCartSummaryFailure();
    expect(getCartSummaryFailure.type).toEqual(CARTSUMMARY.GET_CART_SUMMARY_FAILURE);
  });
});

describe('CARTSUMMARY Actions for UPDATE CartSummary Details', () => {
  it('should return updateCartSummaryDetails action type', () => {
    const updateCartSummary = CartSummaryDetails.updateCartSummary();
    expect(updateCartSummary.type).toEqual(CARTSUMMARY.UPDATE_CART_SUMMARY);
  });

  it('should return updateCartSummarySuccess action type', () => {
    const updateCartSummarySuccess = CartSummaryDetails.updateCartSuccess();
    expect(updateCartSummarySuccess.type).toEqual(CARTSUMMARY.UPDATE_CART_SUMMARY_SUCCESS);
  });

  it('should return updateSummaryFailure action type', () => {
    const updateCartSummaryFailure = CartSummaryDetails.updateCartFailure();
    expect(updateCartSummaryFailure.type).toEqual(CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE);
  });
});

describe('CARTSUMMARY Actions for DELETE CartSummary Details', () => {
  it('should return deleteCartSummary action type', () => {
    const deleteCartSummary = CartSummaryDetails.deleteCartSummary();
    expect(deleteCartSummary.type).toEqual(CARTSUMMARY.DELETE_ITEM_CART_SUMMARY);
  });

  it('should return deleteCartSuccess action type', () => {
    const deleteCartSuccess = CartSummaryDetails.deleteCartSuccess();
    expect(deleteCartSuccess.type).toEqual(CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_SUCCESS);
  });

  it('should return deleteCartFailure action type', () => {
    const deleteCartFailure = CartSummaryDetails.deleteCartFailure();
    expect(deleteCartFailure.type).toEqual(CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_FAILURE);
  });
});

describe('CARTSUMMARY Actions for Submit CartSummary Details', () => {
  it('should return submitCartSummary action type', () => {
    const submitCartSummary = CartSummaryDetails.submitCartSummary();
    expect(submitCartSummary.type).toEqual(CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY);
  });

  it('should return submitCartSuccess action type', () => {
    const submitCartSuccess = CartSummaryDetails.submitCartSuccess();
    expect(submitCartSuccess.type).toEqual(CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_SUCCESS);
  });

  it('should return submitCartFailure action type', () => {
    const submitCartFailure = CartSummaryDetails.submitCartFailure();
    expect(submitCartFailure.type).toEqual(CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE);
  });
});

describe('CARTSUMMARY Actions for Clearing Error Resetting data and going to Initial State', () => {
  it('should return clearError action type', () => {
    const clearError = CartSummaryDetails.clearError();
    expect(clearError.type).toEqual(CARTSUMMARY.CLEAR_ERROR);
  });

  it('should return cartInitialState action type', () => {
    const cartInitialState = CartSummaryDetails.cartInitialState();
    expect(cartInitialState.type).toEqual(CARTSUMMARY.CART_INITIAL_STATE);
  });

  it('should return cartResetData action type', () => {
    const cartResetData = CartSummaryDetails.cartResetData();
    expect(cartResetData.type).toEqual(CARTSUMMARY.CART_RESET_DATA);
  });
});
