import * as WHATQUALIFIES from '../actionTypes/WhatQualifies';
import * as whatQualifiesActionCreators from '../actionCreators/WhatQualifies';

describe('WHATQUALIFIES Actions for myPlan Category', () => {
  it('should return getCategory action type', () => {
    const getCategory = whatQualifiesActionCreators.getCategory();
    expect(getCategory.type).toEqual(WHATQUALIFIES.GET_MYPLAN_CATEGORY);
  });

  it('should return getCategorySuccess action type', () => {
    const getCategorySuccess = whatQualifiesActionCreators.getCategorySuccess();
    expect(getCategorySuccess.type).toEqual(WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS);
  });

  it('should return getCategoryFailure action type', () => {
    const getCategoryFailure = whatQualifiesActionCreators.getCategoryFailure();
    expect(getCategoryFailure.type).toEqual(WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE);
  });

  it('should return updateMyPlanID action type', () => {
    const updateMyPlanID = whatQualifiesActionCreators.updateMyPlanID();
    expect(updateMyPlanID.type).toEqual(WHATQUALIFIES.MY_PLAN_ID);
  });

  it('should return clearErrors action type', () => {
    const clearErrors = whatQualifiesActionCreators.clearErrors();
    expect(clearErrors.type).toEqual(WHATQUALIFIES.CLEAR_ERRORS);
  });
});

describe('WHATQUALIFIES Actions for spotPromotions Category', () => {
  it('should return getCategoryPromotions action type', () => {
    const getCategoryPromotions = whatQualifiesActionCreators.getCategoryPromotions();
    expect(getCategoryPromotions.type).toEqual(WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY);
  });

  it('should return getCategoryPromotionsSuccess action type', () => {
    const getCategoryPromotionsSuccess = whatQualifiesActionCreators.getCategoryPromotionsSuccess();
    expect(getCategoryPromotionsSuccess.type).toEqual(WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS);
  });

  it('should return getCategoryPromotionsFailure action type', () => {
    const getCategoryPromotionsFailure = whatQualifiesActionCreators.getCategoryPromotionsFailure();
    expect(getCategoryPromotionsFailure.type).toEqual(WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE);
  });

  it('should return updateSpotPromotionPlanID action type', () => {
    const updateSpotPromotionPlanID = whatQualifiesActionCreators.updateSpotPromotionPlanID();
    expect(updateSpotPromotionPlanID.type).toEqual(WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_PLAN_ID);
  });

  it('should return updateSpotPromotionSelectedYear action type', () => {
    const updateSpotPromotionSelectedYear = whatQualifiesActionCreators.updateSpotPromotionSelectedYear();
    expect(updateSpotPromotionSelectedYear.type).toEqual(WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_SELECTED_YEAR);
  });

  it('should return clearErrors action type', () => {
    const clearErrors = whatQualifiesActionCreators.clearErrors();
    expect(clearErrors.type).toEqual(WHATQUALIFIES.CLEAR_ERRORS);
  });
});
