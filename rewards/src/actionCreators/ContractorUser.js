import * as CONTRACTOR from '../actionTypes/ContractorUser';

export function getUsers() {
  return {
    type: CONTRACTOR.GET_USERS,
  };
}

export function getUsersSuccess(data) {
  return {
    type: CONTRACTOR.GET_USERS_SUCCESS,
    data,
  };
}

export function getUsersFailure(error) {
  return {
    type: CONTRACTOR.GET_USERS_FAILURE,
    error,
  };
}
