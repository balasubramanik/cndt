import * as HEADER from '../actionTypes/Header';

export function getCartCount() {
  return {
    type: HEADER.GET_CART_COUNT,
  };
}

export function getCartCountSuccess(cart) {
  return {
    type: HEADER.GET_CART_COUNT_SUCCESS,
    cart,
  };
}

export function getCartCountFailure(error) {
  return {
    type: HEADER.GET_CART_COUNT_FAILURE,
    error,
  };
}

