import * as CLAIM_INVOICE_DETAILS from '../actionTypes/ClaimInvoiceDetails';

export function getClaimsInvoiceList(claimId = '') {
  return {
    type: CLAIM_INVOICE_DETAILS.GET_CLAIMS_INVOICE_LIST,
    claimId,
  };
}

export function getClaimsInvoiceListSuccess(data) {
  return {
    type: CLAIM_INVOICE_DETAILS.GET_CLAIMS_INVOICE_LIST_SUCCESS,
    data,
  };
}

export function getClaimsInvoiceListFailure(error) {
  return {
    type: CLAIM_INVOICE_DETAILS.GET_CLAIMS_INVOICE_LIST_FAILURE,
    error,
  };
}

export function getClaimsInvoiceListClear() {
  return {
    type: CLAIM_INVOICE_DETAILS.GET_CLAIMS_INVOICE_LIST_CLEAR,
  };
}

export function downloadInvoice(fileData) {
  return {
    type: CLAIM_INVOICE_DETAILS.DOWNLOAD_FILE,
    fileData,
  };
}

export function downloadInvoiceSuccess(data) {
  return {
    type: CLAIM_INVOICE_DETAILS.DOWNLOAD_FILE_SUCCESS,
    data,
  };
}

export function downloadInvoiceFailure(error) {
  return {
    type: CLAIM_INVOICE_DETAILS.DOWNLOAD_FILE_FAILURE,
    error,
  };
}

export function downloadInvoiceClear() {
  return {
    type: CLAIM_INVOICE_DETAILS.DOWNLOAD_FILE_CLEAR,
  };
}

export function claimsInvoiceClearError() {
  return {
    type: CLAIM_INVOICE_DETAILS.CLAIMS_INVOICE_CLEAR_ERROR,
  };
}
