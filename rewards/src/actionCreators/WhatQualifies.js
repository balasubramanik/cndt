import * as WHATQUALIFIES from '../actionTypes/WhatQualifies';

export function getCategory(year, contractorId) {
  return {
    type: WHATQUALIFIES.GET_MYPLAN_CATEGORY,
    year,
    contractorId,
  };
}

export function getCategorySuccess(data) {
  return {
    type: WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS,
    data,
  };
}

export function getCategoryFailure(error) {
  return {
    type: WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE,
    error,
  };
}

export function getCategoryPromotions(year, contractorId, id = 1) {
  return {
    type: WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY,
    year,
    contractorId,
    id,
  };
}

export function getCategoryPromotionsSuccess(data) {
  return {
    type: WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS,
    data,
  };
}

export function getCategoryPromotionsFailure(error) {
  return {
    type: WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE,
    error,
  };
}

export function updateSpotPromotionPlanID(id) {
  return {
    type: WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_PLAN_ID,
    id,
  };
}

export function updateMyPlanID(myPlanId) {
  return {
    type: WHATQUALIFIES.MY_PLAN_ID,
    myPlanId,
  };
}

export function updateSpotPromotionSelectedYear(selectedYear) {
  return {
    type: WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_SELECTED_YEAR,
    selectedYear,
  };
}

export function clearErrors() {
  return {
    type: WHATQUALIFIES.CLEAR_ERRORS,
  };
}
