import * as SPOTPROMOTION from '../actionTypes/spotPromotion';
import * as SpotPromotionActionCreators from '../actionCreators/spotPromotion';
describe('Spot Promotion Actions', () => {
  it('should return getPromotion action type', () => {
    const getPromotion = SpotPromotionActionCreators.getPromotion(SPOTPROMOTION.GET_LIST_OF_TRANSACTIONS);
    expect(getPromotion.type).toEqual(SPOTPROMOTION.GET_PROMOTION_LIST);
  });
  it('should return getPromotion action type with no request', () => {
    const getPromotion = SpotPromotionActionCreators.getPromotion();
    expect(getPromotion.type).toEqual(SPOTPROMOTION.GET_PROMOTION_LIST);
  });
  it('should return getPromotion success action type', () => {
    const getPromotionSuccess = SpotPromotionActionCreators.getPromotionSuccess();
    expect(getPromotionSuccess.type).toEqual(SPOTPROMOTION.GET_PROMOTION_LIST_SUCCESS);
  });
  it('should return getPromotion failure action type', () => {
    const getPromotionFailure = SpotPromotionActionCreators.getPromotionFailure();
    expect(getPromotionFailure.type).toEqual(SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE);
  });
  it('should return get Promotion list clear action type', () => {
    const getPromotionListClear = SpotPromotionActionCreators.getPromotionListClear();
    expect(getPromotionListClear.type).toEqual(SPOTPROMOTION.GET_PROMOTION_LIST_CLEAR);
  });
});
