import * as STATES from '../actionTypes/States';
import * as statesActionCreators from '../actionCreators/States';

describe('STATES Actions', () => {
  it('should returns getStates action type', () => {
    const getStates = statesActionCreators.getStates();
    expect(getStates.type).toEqual(STATES.GET_STATES);
  });
  it('should returns getStatesSuccess action type', () => {
    const getStatesSuccess = statesActionCreators.getStatesSuccess();
    expect(getStatesSuccess.type).toEqual(STATES.GET_STATES_SUCCESS);
  });
  it('should returns getStatesFailure action type', () => {
    const getStatesFailure = statesActionCreators.getStatesFailure();
    expect(getStatesFailure.type).toEqual(STATES.GET_STATES_FAILURE);
  });
});
