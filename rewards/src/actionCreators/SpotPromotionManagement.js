import * as PROMOTION from '../actionTypes/SpotPromotionManagement';


export function discardAllData() {
  return {
    type: PROMOTION.DISCARD_ALL_DATA,
  };
}

export function endDateReset() {
  return {
    type: PROMOTION.END_DATE_RESET,
  };
}

export function displayStartDateReset() {
  return {
    type: PROMOTION.DISPLAY_START_DATE_RESET,
  };
}

export function displayEndDateReset() {
  return {
    type: PROMOTION.DISPLAY_END_DATE_CHANGE_RESET,
  };
}

export function saveSpotPromotion(data, submitType) {
  return {
    type: PROMOTION.SAVE_SPOT_PROMOTION,
    data,
    submitType,
  };
}

export function saveSpotPromotionSuccess(data, submitType) {
  return {
    type: PROMOTION.SAVE_SPOTPROMOTION_SUCCESS,
    data,
    submitType,
  };
}

export function saveSpotPromotionFailure(error) {
  return {
    type: PROMOTION.SAVE_SPOTPROMOTION_FAILURE,
    error,
  };
}

export function savePromotionPopupClose() {
  return {
    type: PROMOTION.SAVE_SPOTPROMOTION_POPUP_CLOSE,
  };
}
export function getSpotPromotionOptions(optionType, key, filter) {
  return {
    type: PROMOTION.GET_SPOTPROMOTION_OPTIONS,
    optionType,
    key,
    filter,
  };
}

export function getSpotPromotionOptionSuccess(info, key) {
  return {
    type: PROMOTION.GET_SPOTPROMOTION_OPTIONS_SUCCESS,
    info,
    key,
  };
}

export function getSpotPromotionOptionFailure(error) {
  return {
    type: PROMOTION.GET_SPOTPROMOTION_OPTIONS_FAILURE,
    error,
  };
}

export function editSpotPromotion(id) {
  return {
    type: PROMOTION.GET_SPOT_PROMOTION,
    id,
  };
}

export function editSpotPromotionSuccess(data) {
  return {
    type: PROMOTION.GET_SPOT_PROMOTION_SUCCESS,
    data,
  };
}

export function editSpotPromotionFailure(error) {
  return {
    type: PROMOTION.GET_SPOT_PROMOTION_FAILURE,
    error,
  };
}

export function familyReset(data, index) {
  return {
    type: PROMOTION.PRODUCT_FAMILY_RESET,
    data,
    index,
  };
}
