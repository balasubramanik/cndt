import * as SPOTPROMOTIONDETAILS from '../actionTypes/SpotPromotionDetails';
import * as spotPromotionDetailsActionCreators from '../actionCreators/SpotPromotionDetails';

describe('SPOTPROMOTIONDETAILS Actions for getDetails', () => {
  it('should returns getProfileDetails action type', () => {
    const getDetails = spotPromotionDetailsActionCreators.getDetails();
    expect(getDetails.type).toEqual(SPOTPROMOTIONDETAILS.GET_DETAILS);
  });

  it('should returns getDetailsSuccess action type', () => {
    const getDetailsSuccess = spotPromotionDetailsActionCreators.getDetailsSuccess();
    expect(getDetailsSuccess.type).toEqual(SPOTPROMOTIONDETAILS.GET_DETAILS_SUCCESS);
  });

  it('should returns getDetailsFailure action type', () => {
    const getDetailsFailure = spotPromotionDetailsActionCreators.getDetailsFailure();
    expect(getDetailsFailure.type).toEqual(SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE);
  });
  it('should returns getDetailsClear action type', () => {
    const getDetailsClear = spotPromotionDetailsActionCreators.getDetailsClear();
    expect(getDetailsClear.type).toEqual(SPOTPROMOTIONDETAILS.GET_DETAILS_CLEAR);
  });
});
