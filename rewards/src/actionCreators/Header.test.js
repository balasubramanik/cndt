import * as HEADER from '../actionTypes/Header';
import * as headerActionCreators from '../actionCreators/Header';

describe('Header Actions', () => {
  it('should returns getCartCount action type', () => {
    const getCartCount = headerActionCreators.getCartCount();
    expect(getCartCount.type).toEqual(HEADER.GET_CART_COUNT);
  });

  it('should returns getCartCountSuccess action type', () => {
    const getCartCountSuccess = headerActionCreators.getCartCountSuccess();
    expect(getCartCountSuccess.type).toEqual(HEADER.GET_CART_COUNT_SUCCESS);
  });

  it('should returns getCartCountFailure action type', () => {
    const getCartCountFailure = headerActionCreators.getCartCountFailure();
    expect(getCartCountFailure.type).toEqual(HEADER.GET_CART_COUNT_FAILURE);
  });
});
