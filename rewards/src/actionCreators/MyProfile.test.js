import * as MYPROFILE from '../actionTypes/MyProfile';
import * as myProfileActionCreators from '../actionCreators/MyProfile';

describe('MYPROFILE Actions for profile', () => {
  it('should return getProfileDetails action type', () => {
    const getProfileDetails = myProfileActionCreators.getProfileDetails();
    expect(getProfileDetails.type).toEqual(MYPROFILE.GET_PROFILE);
  });

  it('should return getProfileDetailsSuccess action type', () => {
    const getProfileDetailsSuccess = myProfileActionCreators.getProfileDetailsSuccess();
    expect(getProfileDetailsSuccess.type).toEqual(MYPROFILE.GET_PROFILE_SUCCESS);
  });

  it('should return getProfileDetailsFailure action type', () => {
    const getProfileDetailsFailure = myProfileActionCreators.getProfileDetailsFailure();
    expect(getProfileDetailsFailure.type).toEqual(MYPROFILE.GET_PROFILE_FAILURE);
  });

  it('should return accessPage action type', () => {
    const accessPage = myProfileActionCreators.accessPage();
    expect(accessPage.type).toEqual(MYPROFILE.ACCESS_PAGE);
  });
});

describe('MYPROFILE Actions for rewards', () => {
  it('should return getRewardsDetails action type', () => {
    const getRewardsDetails = myProfileActionCreators.getRewardsDetails();
    expect(getRewardsDetails.type).toEqual(MYPROFILE.GET_REWARDS);
  });

  it('should return getProfileDetailsSuccess action type', () => {
    const getRewardsDetailsSuccess = myProfileActionCreators.getRewardsDetailsSuccess();
    expect(getRewardsDetailsSuccess.type).toEqual(MYPROFILE.GET_REWARDS_SUCCESS);
  });

  it('should return getProfileDetailsFailure action type', () => {
    const getRewardsDetailsFailure = myProfileActionCreators.getRewardsDetailsFailure();
    expect(getRewardsDetailsFailure.type).toEqual(MYPROFILE.GET_REWARDS_FAILURE);
  });
});

describe('MYPROFILE Actions for Admin Profile', () => {
  it('should return getAdminProfileDetails action type', () => {
    const getAdminProfileDetails = myProfileActionCreators.getAdminProfileDetails();
    expect(getAdminProfileDetails.type).toEqual(MYPROFILE.GET_ADMIN_PROFILE);
  });

  it('should return getAdminProfileDetailsSuccess action type', () => {
    const getAdminProfileDetailsSuccess = myProfileActionCreators.getAdminProfileDetailsSuccess();
    expect(getAdminProfileDetailsSuccess.type).toEqual(MYPROFILE.GET_ADMIN_PROFILE_SUCCESS);
  });

  it('should return getAdminProfileDetailsFailure action type', () => {
    const getAdminProfileDetailsFailure = myProfileActionCreators.getAdminProfileDetailsFailure();
    expect(getAdminProfileDetailsFailure.type).toEqual(MYPROFILE.GET_ADMIN_PROFILE_FAILURE);
  });
});
