import * as EMAILVERIFICATION from '../actionTypes/EmailVerification';
import * as emailVerificationActionCreators from '../actionCreators/EmailVerification';

describe('Email Verification Actions', () => {
  it('should returns change form state action type', () => {
    const changeFormState = emailVerificationActionCreators.changeFormState(EMAILVERIFICATION.CHANGE_ROOF_OPTIONS);
    expect(changeFormState.type).toEqual(EMAILVERIFICATION.CHANGE_ROOF_OPTIONS);
  });

  it('should returns submit email verification action type', () => {
    const submitEmailVerification = emailVerificationActionCreators.submitEmailVerification();
    expect(submitEmailVerification.type).toEqual(EMAILVERIFICATION.SUBMIT_EMAIL_VERIFICATION);
  });

  it('should returns submit email verification success action type', () => {
    const submitEmailVerificationSuccess = emailVerificationActionCreators.submitEmailVerificationSuccess();
    expect(submitEmailVerificationSuccess.type).toEqual(EMAILVERIFICATION.SUBMIT_EMAIL_VERIFICATION_SUCCESS);
  });

  it('should returns submit email verification failure action type', () => {
    const submitEmailVerificationFailure = emailVerificationActionCreators.submitEmailVerificationFailure();
    expect(submitEmailVerificationFailure.type).toEqual(EMAILVERIFICATION.SUBMIT_EMAIL_VERIFICATION_FAILURE);
  });

  it('should returns Resend email action type', () => {
    const resendEmail = emailVerificationActionCreators.resendEmail();
    expect(resendEmail.type).toEqual(EMAILVERIFICATION.RESEND_EMAIL);
  });

  it('should returns Resend email success action type', () => {
    const resendEmailSuccess = emailVerificationActionCreators.resendEmailSuccess();
    expect(resendEmailSuccess.type).toEqual(EMAILVERIFICATION.RESEND_EMAIL_SUCCESS);
  });

  it('should returns Resend email failure action type', () => {
    const resendEmailFailure = emailVerificationActionCreators.resendEmailFailure();
    expect(resendEmailFailure.type).toEqual(EMAILVERIFICATION.RESEND_EMAIL_FAILURE);
  });

  it('should returns Clear errors action type', () => {
    const clearErrors = emailVerificationActionCreators.clearErrors();
    expect(clearErrors.type).toEqual(EMAILVERIFICATION.CLEAR_ERRORS);
  });
});
