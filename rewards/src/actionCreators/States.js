import * as STATES from '../actionTypes/States';

export function getStates() {
  return {
    type: STATES.GET_STATES,
  };
}
export function getStatesSuccess(data) {
  return {
    type: STATES.GET_STATES_SUCCESS,
    data,
  };
}
export function getStatesFailure(error) {
  return {
    type: STATES.GET_STATES_FAILURE,
    error,
  };
}
