import * as CLAIMDETAILS from '../actionTypes/ClaimDetails';

export function getInvoiceList(claimID = '') {
  return {
    type: CLAIMDETAILS.GET_INVOICE_LIST,
    claimID,
  };
}

export function getInvoiceListSuccess(data) {
  return {
    type: CLAIMDETAILS.GET_INVOICE_LIST_SUCCESS,
    data,
  };
}

export function getInvoiceListFailure(error) {
  return {
    type: CLAIMDETAILS.GET_INVOICE_LIST_FAILURE,
    error,
  };
}

export function getInvoiceListClear(error) {
  return {
    type: CLAIMDETAILS.GET_INVOICE_LIST_CLEAR,
    error,
  };
}

export function updatePageDetails(details) {
  return {
    type: CLAIMDETAILS.UPDATE_PAGE_DETAILS,
    details,
  };
}
