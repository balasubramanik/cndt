import * as MYPROFILE from '../actionTypes/MyProfile';

export function getProfileDetails() {
  return {
    type: MYPROFILE.GET_PROFILE,
  };
}

export function getProfileDetailsSuccess(info) {
  return {
    type: MYPROFILE.GET_PROFILE_SUCCESS,
    info,
  };
}

export function getProfileDetailsFailure(error) {
  return {
    type: MYPROFILE.GET_PROFILE_FAILURE,
    error,
  };
}

export function accessPage(info) {
  return {
    type: MYPROFILE.ACCESS_PAGE,
    info,
  };
}

export function getRewardsDetails() {
  return {
    type: MYPROFILE.GET_REWARDS,
  };
}

export function getRewardsDetailsSuccess(info) {
  return {
    type: MYPROFILE.GET_REWARDS_SUCCESS,
    info,
  };
}

export function getRewardsDetailsFailure(error) {
  return {
    type: MYPROFILE.GET_REWARDS_FAILURE,
    error,
  };
}

export function getAdminProfileDetails() {
  return {
    type: MYPROFILE.GET_ADMIN_PROFILE,
  };
}

export function getAdminProfileDetailsSuccess(info) {
  return {
    type: MYPROFILE.GET_ADMIN_PROFILE_SUCCESS,
    info,
  };
}

export function getAdminProfileDetailsFailure(error) {
  return {
    type: MYPROFILE.GET_ADMIN_PROFILE_FAILURE,
    error,
  };
}
