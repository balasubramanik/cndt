import * as CARTSUMMARY from '../actionTypes/CartSummary';

export function getCartSummary() {
  return {
    type: CARTSUMMARY.GET_CART_SUMMARY,
  };
}

export function getCartSummarySuccess(data) {
  return {
    type: CARTSUMMARY.GET_CART_SUMMARY_SUCCESS,
    data,
  };
}

export function getCartSummaryFailure(error) {
  return {
    type: CARTSUMMARY.GET_CART_SUMMARY_FAILURE,
    error,
  };
}

export function updateCartSummary(data) {
  return {
    type: CARTSUMMARY.UPDATE_CART_SUMMARY,
    data,
  };
}

export function updateCartSuccess(data) {
  return {
    type: CARTSUMMARY.UPDATE_CART_SUMMARY_SUCCESS,
    data,
  };
}

export function updateCartFailure(error) {
  return {
    type: CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE,
    error,
  };
}

export function deleteCartSummary(data) {
  return {
    type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY,
    data,
  };
}

export function deleteCartSuccess(data) {
  return {
    type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_SUCCESS,
    data,
  };
}

export function deleteCartFailure(error) {
  return {
    type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_FAILURE,
    error,
  };
}

export function submitCartSummary(data) {
  return {
    type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY,
    data,
  };
}

export function submitCartSuccess(data) {
  return {
    type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_SUCCESS,
    data,
  };
}

export function submitCartFailure(error) {
  return {
    type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE,
    error,
  };
}

export function clearError() {
  return {
    type: CARTSUMMARY.CLEAR_ERROR,
  };
}

export function cartInitialState() {
  return {
    type: CARTSUMMARY.CART_INITIAL_STATE,
  };
}

export function cartResetData() {
  return {
    type: CARTSUMMARY.CART_RESET_DATA,
  };
}
