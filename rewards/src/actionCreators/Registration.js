import * as REGISTRATION from '../actionTypes/Registration';

export function changeAddress(name, value) {
  return {
    type: REGISTRATION.CHANGE_ADDRESS,
    name,
    value,
  };
}

export function selectAddress(name, address) {
  return {
    type: REGISTRATION.SELECT_ADDRESS,
    name,
    address,
  };
}

export function selectServices(key, value) {
  return {
    type: REGISTRATION.SELECT_SERVICES,
    key,
    value,
  };
}

export function updateRegistrationOption(fields) {
  return {
    type: REGISTRATION.UPDATE_REGISTRATION_OPTION,
    fields,
  };
}

export function getRegistrationInfo(id) {
  return {
    type: REGISTRATION.REGISTRATION_INFO,
    id,
  };
}

export function getRegistrationInfoSuccess(info) {
  return {
    type: REGISTRATION.REGISTRATION_INFO_SUCCESS,
    info,
  };
}

export function getRegistrationInfoFailure(error) {
  return {
    type: REGISTRATION.REGISTRATION_INFO_ERROR,
    error,
  };
}

export function registerNow(formData) {
  return {
    type: REGISTRATION.REGISTER_NOW,
    formData,
  };
}

export function registerNowSuccess(info) {
  return {
    type: REGISTRATION.REGISTER_NOW_SUCCESS,
    info,
  };
}

export function registerNowFailure(error) {
  return {
    type: REGISTRATION.REGISTER_NOW_ERROR,
    error,
  };
}

export function clearErrors() {
  return {
    type: REGISTRATION.CLEAR_ERRORS,
  };
}
