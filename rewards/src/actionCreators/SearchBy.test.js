import * as SearchSuggestions from '../actionTypes/SearchBy';
import * as SearchSuggestionsActionCreators from '../actionCreators/SearchBy';

describe('Search Suggestions', () => {
  it('should returns Search Suggestions action type', () => {
    const actionType = SearchSuggestionsActionCreators.getSuggestion();
    expect(actionType.type).toEqual(SearchSuggestions.GET_SUGGESTION);
  });

  it('should returns Search Suggestions Success action type', () => {
    const actionType = SearchSuggestionsActionCreators.getSuggestionSuccess();
    expect(actionType.type).toEqual(SearchSuggestions.GET_SUGGESTION_SUCCESS);
  });

  it('should returns Search Suggestions Failure action type', () => {
    const actionType = SearchSuggestionsActionCreators.getSuggestionFailure();
    expect(actionType.type).toEqual(SearchSuggestions.GET_SUGGESTION_FAILURE);
  });

  it('should returns Update Search Text action type', () => {
    const actionType = SearchSuggestionsActionCreators.updateSearchText();
    expect(actionType.type).toEqual(SearchSuggestions.UPDATE_SEARCH_TEXT);
  });

  it('should returns Clear Search Text action type', () => {
    const actionType = SearchSuggestionsActionCreators.clearSearchText();
    expect(actionType.type).toEqual(SearchSuggestions.CLEAR_SEARCH_TEXT);
  });
});
