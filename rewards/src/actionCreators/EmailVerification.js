import * as EMAIL from '../actionTypes/EmailVerification';

export function changeFormState(type, data = {}) {
  return {
    type,
    data,
  };
}

export function submitEmailVerification(formData) {
  return {
    type: EMAIL.SUBMIT_EMAIL_VERIFICATION,
    formData,
  };
}

export function submitEmailVerificationSuccess(data) {
  return {
    type: EMAIL.SUBMIT_EMAIL_VERIFICATION_SUCCESS,
    data,
  };
}

export function submitEmailVerificationFailure(error) {
  return {
    type: EMAIL.SUBMIT_EMAIL_VERIFICATION_FAILURE,
    error,
  };
}

export function resendEmail(reqBody) {
  return {
    type: EMAIL.RESEND_EMAIL,
    reqBody,
  };
}

export function resendEmailSuccess(data) {
  return {
    type: EMAIL.RESEND_EMAIL_SUCCESS,
    data,
  };
}

export function resendEmailFailure(error) {
  return {
    type: EMAIL.RESEND_EMAIL_FAILURE,
    error,
  };
}

export function clearErrors() {
  return {
    type: EMAIL.CLEAR_ERRORS,
  };
}
