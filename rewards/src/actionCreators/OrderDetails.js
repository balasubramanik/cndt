import * as ORDERDETAILS from '../actionTypes/OrderDetails';

export function getProduct(orderID) {
  return {
    type: ORDERDETAILS.GET_LIST_OF_PRODUCT,
    orderID,
  };
}

export function getProductSuccess(data) {
  return {
    type: ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS,
    data,
  };
}

export function getProductFailure(error) {
  return {
    type: ORDERDETAILS.GET_LIST_OF_PRODUCT_FAILURE,
    error,
  };
}

export function getProductClear(error) {
  return {
    type: ORDERDETAILS.GET_LIST_OF_PRODUCT_CLEAR,
    error,
  };
}

export function updatePageDetails(details) {
  return {
    type: ORDERDETAILS.UPDATE_PAGE_DETAILS,
    details,
  };
}
