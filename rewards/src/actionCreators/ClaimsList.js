import * as CLAIMSLIST from '../actionTypes/ClaimsList';

export function getClaimsList(request = {}, isExportCSV = false) {
  return {
    type: CLAIMSLIST.GET_CLAIMS_LIST,
    request,
    isExportCSV,
  };
}

export function getClaimsListSuccess(data) {
  return {
    type: CLAIMSLIST.GET_CLAIMS_LIST_SUCCESS,
    data,
  };
}

export function getClaimsListFailure(error) {
  return {
    type: CLAIMSLIST.GET_CLAIMS_LIST_FAILURE,
    error,
  };
}

export function getClaimsListClear() {
  return {
    type: CLAIMSLIST.GET_CLAIMS_LIST_CLEAR,
  };
}

export function getExportCSVSuccess(data) {
  return {
    type: CLAIMSLIST.GET_EXPORT_CSV_SUCCESS,
    data,
  };
}

export function claimsListClearError() {
  return {
    type: CLAIMSLIST.CLAIM_LIST_CLEAR_ERROR,
  };
}
