import * as CONTACTUS from '../actionTypes/ContactUs';
import * as ContactUsActionCreators from '../actionCreators/ContactUs';

describe('Contactus Actions', () => {
  it('should return submit contact action type', () => {
    const submitContactus = ContactUsActionCreators.submitContactUs(CONTACTUS.SUBMIT_CONTACTUS);
    expect(submitContactus.type).toEqual(CONTACTUS.SUBMIT_CONTACTUS);
  });

  it('should return submit contact success action type', () => {
    const submitContactusSuccess = ContactUsActionCreators.submitContactUsSuccess();
    expect(submitContactusSuccess.type).toEqual(CONTACTUS.SUBMIT_CONTACTUS_SUCCESS);
  });

  it('should return submit contact failure action type', () => {
    const submitContactFailure = ContactUsActionCreators.submitContactUsFailure();
    expect(submitContactFailure.type).toEqual(CONTACTUS.SUBMIT_CONTACTUS_FAILURE);
  });

  it('should return clear action type', () => {
    const clearErrors = ContactUsActionCreators.clearErrors();
    expect(clearErrors.type).toEqual(CONTACTUS.CLEAR_ERRORS);
  });

  it('should return reset contact us action type', () => {
    const resetContactUs = ContactUsActionCreators.resetContactUs();
    expect(resetContactUs.type).toEqual(CONTACTUS.RESET_CONTACTUS);
  });
});
