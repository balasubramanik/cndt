import * as CONTRACTORLIST from '../actionTypes/ContractorList';
import * as ContractorListActionCreators from '../actionCreators/ContractorList';

describe('Contractor List Actions for getting contractor List', () => {
  it('should return getContractorList action type', () => {
    const getContractorList = ContractorListActionCreators.getContractorList(CONTRACTORLIST.GET_CONTRACTOR_LIST);
    expect(getContractorList.type).toEqual(CONTRACTORLIST.GET_CONTRACTOR_LIST);
  });

  it('should return getContractorListSuccess action type', () => {
    const getContractorListSuccess = ContractorListActionCreators.getContractorListSuccess(CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS);
    expect(getContractorListSuccess.type).toEqual(CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS);
  });


  it('should return getContractorListFailure action type', () => {
    const getContractorListFailure = ContractorListActionCreators.getContractorListFailure(CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE);
    expect(getContractorListFailure.type).toEqual(CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE);
  });
});

describe('Contractor List Actions for ACH Toggle', () => {
  it('should return achToggle action type', () => {
    const achToggle = ContractorListActionCreators.achToggle(CONTRACTORLIST.ACH_TOGGLE);
    expect(achToggle.type).toEqual(CONTRACTORLIST.ACH_TOGGLE);
  });

  it('should return achToggleSuccess action type', () => {
    const achToggleSuccess = ContractorListActionCreators.achToggleSuccess(CONTRACTORLIST.ACH_TOGGLE_SUCCESS);
    expect(achToggleSuccess.type).toEqual(CONTRACTORLIST.ACH_TOGGLE_SUCCESS);
  });

  it('should return achToggleFailure action type', () => {
    const achToggleFailure = ContractorListActionCreators.achToggleFailure(CONTRACTORLIST.ACH_TOGGLE_FAILURE);
    expect(achToggleFailure.type).toEqual(CONTRACTORLIST.ACH_TOGGLE_FAILURE);
  });
});

describe('Contractor List Actions for Detailed Report', () => {
  it('should return submitDetailedReport action type', () => {
    const submitDetailedReport = ContractorListActionCreators.submitDetailedReport(CONTRACTORLIST.SUBMIT_DETAILED_REPORT);
    expect(submitDetailedReport.type).toEqual(CONTRACTORLIST.SUBMIT_DETAILED_REPORT);
  });

  it('should return submitDetailedReportSuccess action type', () => {
    const submitDetailedReportSuccess = ContractorListActionCreators.submitDetailedReportSuccess(CONTRACTORLIST.SUBMIT_DETAILED_REPORT_SUCCESS);
    expect(submitDetailedReportSuccess.type).toEqual(CONTRACTORLIST.SUBMIT_DETAILED_REPORT_SUCCESS);
  });

  it('should return submitDetailedReportFailure action type', () => {
    const submitDetailedReportFailure = ContractorListActionCreators.submitDetailedReportFailure(CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE);
    expect(submitDetailedReportFailure.type).toEqual(CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE);
  });
});

describe('Contractor List Actions for closing popups, updating page and reset', () => {
  it('should return closeTogglePopup action type', () => {
    const closeTogglePopup = ContractorListActionCreators.closeTogglePopup(CONTRACTORLIST.CLOSE_TOGGLE_POPUP);
    expect(closeTogglePopup.type).toEqual(CONTRACTORLIST.CLOSE_TOGGLE_POPUP);
  });

  it('should return closeDetailedPopup action type', () => {
    const closeDetailedPopup = ContractorListActionCreators.closeDetailedPopup(CONTRACTORLIST.CLOSE_DETAILED_POPUP);
    expect(closeDetailedPopup.type).toEqual(CONTRACTORLIST.CLOSE_DETAILED_POPUP);
  });

  it('should return updatePageDetails action type', () => {
    const updatePageDetails = ContractorListActionCreators.updatePageDetails(CONTRACTORLIST.UPDATE_PAGE_DETAILS);
    expect(updatePageDetails.type).toEqual(CONTRACTORLIST.UPDATE_PAGE_DETAILS);
  });
  it('should return resetInitialState action type', () => {
    const resetInitialState = ContractorListActionCreators.resetInitialState(CONTRACTORLIST.RESETINITIALSTATE);
    expect(resetInitialState.type).toEqual(CONTRACTORLIST.RESETINITIALSTATE);
  });
});
