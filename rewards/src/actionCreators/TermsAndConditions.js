import * as TERMSANDCONDITIONS from '../actionTypes/TermsAndConditions';


export function submitTermsAndConditions(formData) {
  return {
    type: TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS,
    formData,
  };
}

export function submitTermsAndConditionsSuccess(info) {
  return {
    type: TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_SUCCESS,
    info,
  };
}

export function submitTermsAndConditionsFailure(error) {
  return {
    type: TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE,
    error,
  };
}
