import * as POINTSHISTORY from '../actionTypes/PointsHistory';

export function getTransactions(reqBody = {}, isExportCSV = false) {
  return {
    type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS,
    reqBody,
    isExportCSV,
  };
}

export function getTransactionsSuccess(data) {
  return {
    type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_SUCCESS,
    data,
  };
}

export function getTransactionsFailure(error) {
  return {
    type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE,
    error,
  };
}

export function getTransactionListClear() {
  return {
    type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_CLEAR,
  };
}

export function getExportCSVSuccess(data) {
  return {
    type: POINTSHISTORY.GET_EXPORT_CSV_SUCCESS,
    data,
  };
}

export function transactionListClearError() {
  return {
    type: POINTSHISTORY.TRANSACTIONS_LIST_CLEAR_ERROR,
  };
}
