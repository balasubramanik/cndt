import * as SPOTPROMOTIONDETAILS from '../actionTypes/SpotPromotionDetails';

export function getDetails(id = '', contractorId = '') {
  return {
    type: SPOTPROMOTIONDETAILS.GET_DETAILS,
    id,
    contractorId,
  };
}

export function getDetailsSuccess(data) {
  return {
    type: SPOTPROMOTIONDETAILS.GET_DETAILS_SUCCESS,
    data,
  };
}

export function getDetailsFailure(error) {
  return {
    type: SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE,
    error,
  };
}

export function getDetailsClear() {
  return {
    type: SPOTPROMOTIONDETAILS.GET_DETAILS_CLEAR,
  };
}
