import * as POINTSHISTORY from '../actionTypes/PointsHistory';
import * as PointsHistoryActionCreators from '../actionCreators/PointsHistory';


describe('PointsHistory Actions', () => {
  it('should return getTransactions action type', () => {
    const getTransactions = PointsHistoryActionCreators.getTransactions(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS);
    expect(getTransactions.type).toEqual(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS);
  });
  it('should return getClaimsList action type with no request', () => {
    const getTransactions = PointsHistoryActionCreators.getTransactions();
    expect(getTransactions.type).toEqual(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS);
  });
  it('should return get Claims list success action type', () => {
    const getTransactionsSuccess = PointsHistoryActionCreators.getTransactionsSuccess();
    expect(getTransactionsSuccess.type).toEqual(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_SUCCESS);
  });

  it('should return get Transaction list failure action type', () => {
    const getTransactionsFailure = PointsHistoryActionCreators.getTransactionsFailure();
    expect(getTransactionsFailure.type).toEqual(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE);
  });

  it('should return get Transaction list clear action type', () => {
    const getTransactionListClear = PointsHistoryActionCreators.getTransactionListClear();
    expect(getTransactionListClear.type).toEqual(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_CLEAR);
  });
  it('should return getExportCSVSuccess action type', () => {
    const getExportCSVSuccess = PointsHistoryActionCreators.getExportCSVSuccess(POINTSHISTORY.GET_EXPORT_CSV_SUCCESS);
    expect(getExportCSVSuccess.type).toEqual(POINTSHISTORY.GET_EXPORT_CSV_SUCCESS);
  });

  it('should return transactionListClearError action type', () => {
    const transactionListClearError = PointsHistoryActionCreators.transactionListClearError();
    expect(transactionListClearError.type).toEqual(POINTSHISTORY.TRANSACTIONS_LIST_CLEAR_ERROR);
  });
});
