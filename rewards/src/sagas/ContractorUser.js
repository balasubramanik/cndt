import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as CONTRACTOR from '../actionTypes/ContractorUser';
import * as contractorUserActionCreators from '../actionCreators/ContractorUser';
import { doPost } from '../utils/fetchWrapper';


export function* getUsers() {
  try {
    const response = yield doPost(envConfig.apiEndPoints.getUsers);
    yield put(contractorUserActionCreators.getUsersSuccess(response.data));
  } catch (error) {
    yield put(contractorUserActionCreators.getUsersFailure(error));
  }
}

export function* contractorUserWatcher() {
  yield [
    takeLatest(CONTRACTOR.GET_USERS, getUsers),
  ];
}
