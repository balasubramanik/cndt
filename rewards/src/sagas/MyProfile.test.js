import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getProfileDetails, getAdminProfileDetails, getRewardsDetails, myProfileWatchers } from './MyProfile';
import * as MYPROFILE from '../actionTypes/MyProfile';
import * as myProfileActionCreators from '../actionCreators/MyProfile';

const t = test('test utils', (b) => b);

describe('getProfileDetails', () => {
  it('should call GET_PROFILE_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      contractor: {
        email: 'Brain@gaf.com',
        status: 'Active',
        roleId: 1,
        firstName: 'Brain',
        lastName: 'Granger',
      },
      contractorAccountInformation: {
        contractorName: 'Queens Roofing and Tinsmithing Inc',
        contractorImage: 'tinsmiths.jpg',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
      },
      RewardsInfo: {
        rewardsPoints: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    };
    const generator = getProfileDetails();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(myProfileActionCreators.getProfileDetailsSuccess(response.data)));
  });

  it('should call GET_PROFILE_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getProfileDetails();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(myProfileActionCreators.getProfileDetailsFailure(response.data)));
  });

  it('should call GET_PROFILE_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getProfileDetails();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(myProfileActionCreators.getProfileDetailsFailure(response)));
  });

  it('should call myProfileWatchers', () => {
    const generator = myProfileWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(MYPROFILE.GET_PROFILE, getProfileDetails),
    ]);
  });
});

describe('getAdminProfileDetails', () => {
  it('should call GET_ADMIN_PROFILE_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      contractor: {
        email: 'Brain@gaf.com',
        status: 'Active',
        roleId: 1,
        firstName: 'Brain',
        lastName: 'Granger',
      },
      contractorAccountInformation: {
        contractorName: 'Queens Roofing and Tinsmithing Inc',
        contractorImage: 'tinsmiths.jpg',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
      },
      RewardsInfo: {
        rewardsPoints: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    };
    const generator = getAdminProfileDetails();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(myProfileActionCreators.getAdminProfileDetailsSuccess(response.data)));
  });

  it('should call GET_ADMIN_PROFILE_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getAdminProfileDetails();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(myProfileActionCreators.getAdminProfileDetailsFailure(response.data)));
  });

  it('should call GET_ADMIN_PROFILE_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getAdminProfileDetails();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(myProfileActionCreators.getAdminProfileDetails(response)));
  });

  it('should call myProfileWatchers', () => {
    const generator = myProfileWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(MYPROFILE.GET_ADMIN_PROFILE, getAdminProfileDetails),
    ]);
  });
});

describe('getRewardsDetails', () => {
  it('should call GET_REWARDS_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      contractor: {
        email: 'Brain@gaf.com',
        status: 'Active',
        roleId: 1,
        firstName: 'Brain',
        lastName: 'Granger',
      },
      contractorAccountInformation: {
        contractorName: 'Queens Roofing and Tinsmithing Inc',
        contractorImage: 'tinsmiths.jpg',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
      },
      RewardsInfo: {
        rewardsPoints: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    };
    const generator = getRewardsDetails();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(myProfileActionCreators.getRewardsDetailsSuccess(response.data)));
  });

  it('should call GET_REWARDS_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getRewardsDetails();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(myProfileActionCreators.getRewardsDetailsFailure(response.data)));
  });

  it('should call GET_REWARDS_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getRewardsDetails();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(myProfileActionCreators.getRewardsDetailsFailure(response)));
  });

  it('should call myProfileWatchers', () => {
    const generator = myProfileWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(MYPROFILE.GET_REWARDS, getRewardsDetails),
    ]);
  });
});
