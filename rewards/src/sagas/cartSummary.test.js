import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getCartSummary, deleteCart, submitOrder, cartSummaryWatcher, updateCart } from './cartSummary';
import { getCartCount } from './Header';
import * as CARTSUMMARY from '../actionTypes/CartSummary';
import * as cartSummaryActionCreators from '../actionCreators/cartSummary';
import * as headerActionCreators from '../actionCreators/Header';

const t = test('test utils', (b) => b);

describe('getCartSummary', () => {
  it('should call CARTSUMMARY.GET_CART_SUMMARY_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      data: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        cartlist: [
          {
            id: 1,
            variantType: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };
    const generator = getCartSummary();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.getCartSummarySuccess(response.data)));
  });

  it('should call CARTSUMMARY.GET_CART_SUMMARY_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getCartSummary();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.getCartSummaryFailure(response.data)));
  });

  it('should call CARTSUMMARY.GET_CART_SUMMARY_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getCartSummary();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(cartSummaryActionCreators.getCartSummaryFailure(response)));
  });

  it('should call CARTSUMMARY.UPDATE_CART_SUMMARY_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = updateCart({ data: '' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.updateCartSuccess(response.data)));
  });

  it('should call CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = updateCart();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.updateCartFailure(response)));
  });

  it('should call CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = updateCart();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(cartSummaryActionCreators.updateCartFailure(response)));
  });

  it('should call CARTSUMMARY.DELETE_CART_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      data: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        cartlist: [
          {
            id: 1,
            variantType: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };
    const generator = deleteCart({ data: '' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.deleteCartSuccess(response.data)));
  });

  it('should call CARTSUMMARY.DELETE_CART_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = deleteCart();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.deleteCartFailure(response.data)));
  });

  it('should call CARTSUMMARY.DELETE_CART_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = deleteCart();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(cartSummaryActionCreators.deleteCartFailure(response)));
  });

  it('should call CARTSUMMARY.SUBMIT_CART_SUMMARY_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      data: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        cartlist: [
          {
            id: 1,
            variantType: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };
    const generator = submitOrder({ data: '' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.submitCartSuccess(response.data)));
  });

  it('should call CARTSUMMARY.SUBMIT_CART_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = submitOrder();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(cartSummaryActionCreators.submitCartFailure(response.data)));
  });

  it('should call CARTSUMMARY.SUBMIT_CART_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = submitOrder();
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(cartSummaryActionCreators.submitCartFailure(response)));
  });

  it('getCartCount', () => {
    const response = {
      responseCode: 200,
      data: {
        count: 5,
      },
    };
    const generator = getCartCount();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(headerActionCreators.getCartCountSuccess(response.data)));
  });

  it(' should call cartSummaryWatcher', () => {
    const generator = cartSummaryWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CARTSUMMARY.GET_CART_SUMMARY, getCartSummary),
      takeLatest(CARTSUMMARY.UPDATE_CART_SUMMARY, updateCart),
      takeLatest(CARTSUMMARY.DELETE_ITEM_CART_SUMMARY, deleteCart),
      takeLatest(CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY, submitOrder),
    ]);
  });
});
