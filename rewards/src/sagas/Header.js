import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as HEADER from '../actionTypes/Header';
import * as headerActionCreators from '../actionCreators/Header';
import { doPost } from '../utils/fetchWrapper';

export function* getCartCount() {
  try {
    const response = yield doPost(envConfig.apiEndPoints.getCartCount);
    yield put(headerActionCreators.getCartCountSuccess(response.data));
  } catch (error) {
    yield put(headerActionCreators.getCartCountFailure(error));
  }
}

export function* headerWatchers() {
  yield [
    takeLatest(HEADER.GET_CART_COUNT, getCartCount),
  ];
}
