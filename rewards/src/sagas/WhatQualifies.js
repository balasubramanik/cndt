import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as WHATQUALIFIES from '../actionTypes/WhatQualifies';
import * as whatQualifiesActionCreators from '../actionCreators/WhatQualifies';
import { doPost } from '../utils/fetchWrapper';


export function* getCategory(action) {
  const { year, contractorId = '' } = action;
  try {
    const reqBody = contractorId !== '' ? {
      planType: 0,
      year,
      contractorId,
    } : {
      planType: 0,
      year,
    };
    const response = yield doPost(envConfig.apiEndPoints.getMyplanCategory, reqBody);
    yield put(whatQualifiesActionCreators.getCategorySuccess(response.data));
  } catch (error) {
    yield put(whatQualifiesActionCreators.getCategoryFailure(error));
  }
}

export function* getCategoryPromotions(action) {
  const { year, contractorId = '', id } = action;
  try {
    const reqBody = contractorId !== '' ? {
      planType: id,
      year,
      contractorId,
    } : {
      planType: id,
      year,
    };
    const response = yield doPost(envConfig.apiEndPoints.getSpotPromotionsCategory, reqBody);
    yield put(whatQualifiesActionCreators.getCategoryPromotionsSuccess(response.data));
  } catch (error) {
    yield put(whatQualifiesActionCreators.getCategoryPromotionsFailure(error));
  }
}

export function* whatQualifiesWatcher() {
  yield [
    takeLatest(WHATQUALIFIES.GET_MYPLAN_CATEGORY, getCategory),
    takeLatest(WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY, getCategoryPromotions),
  ];
}
