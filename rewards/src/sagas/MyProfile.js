import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as MYPROFILE from '../actionTypes/MyProfile';
import * as myProfileActionCreators from '../actionCreators/MyProfile';
import { doPost } from '../utils/fetchWrapper';

export function* getProfileDetails() {
  try {
    const url = envConfig.apiEndPoints.getProfileDetails;
    const res = yield doPost(url);
    yield put(myProfileActionCreators.getProfileDetailsSuccess(res.data));
  } catch (error) {
    yield put(myProfileActionCreators.getProfileDetailsFailure(error));
  }
}

export function* getAdminProfileDetails() {
  try {
    const url = envConfig.apiEndPoints.getAdminProfile;
    const res = yield doPost(url);
    yield put(myProfileActionCreators.getAdminProfileDetailsSuccess(res.data));
  } catch (error) {
    yield put(myProfileActionCreators.getAdminProfileDetailsFailure(error));
  }
}

export function* getRewardsDetails() {
  try {
    const url = envConfig.apiEndPoints.getRewardsDetails;
    const res = yield doPost(url);
    yield put(myProfileActionCreators.getRewardsDetailsSuccess(res.data));
  } catch (error) {
    yield put(myProfileActionCreators.getRewardsDetailsFailure(error));
  }
}

export function* myProfileWatchers() {
  yield [
    takeLatest(MYPROFILE.GET_PROFILE, getProfileDetails),
    takeLatest(MYPROFILE.GET_REWARDS, getRewardsDetails),
    takeLatest(MYPROFILE.GET_ADMIN_PROFILE, getAdminProfileDetails),
  ];
}
