import { put, takeLatest, takeEvery } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as SPOTPROMOTION from '../actionTypes/SpotPromotionManagement';
import * as SpotPromotionActionCreators from '../actionCreators/SpotPromotionManagement';
import { doPost } from '../utils/fetchWrapper';


export function* saveSpotPromotion(action) {
  const { data, submitType } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.createNewSpotPromotion, data);
    yield put(SpotPromotionActionCreators.saveSpotPromotionSuccess(response.data, submitType));
  } catch (error) {
    yield put(SpotPromotionActionCreators.saveSpotPromotionFailure(error));
  }
}

export function* getSpotPromotionOptions(action) {
  const { optionType, key, filter } = action;
  try {
    const reqBody = {
      type: optionType,
      filter,
    };
    const response = yield doPost(envConfig.apiEndPoints.getSpotPromotionOptions, reqBody);
    yield put(SpotPromotionActionCreators.getSpotPromotionOptionSuccess(response.data, key));
  } catch (error) {
    yield put(SpotPromotionActionCreators.getSpotPromotionOptionFailure(error));
  }
}

export function* editSpotPromotion(action) {
  const { id } = action;
  try {
    const reqBody = {
      id,
    };
    const response = yield doPost(envConfig.apiEndPoints.getSpotPromotionEdit, reqBody);
    yield put(SpotPromotionActionCreators.editSpotPromotionSuccess(response.data));
  } catch (error) {
    yield put(SpotPromotionActionCreators.editSpotPromotionFailure(error));
  }
}

export function* SpotPromotionManagementWatchers() {
  yield [
    takeLatest(SPOTPROMOTION.SAVE_SPOT_PROMOTION, saveSpotPromotion),
    takeEvery(SPOTPROMOTION.GET_SPOTPROMOTION_OPTIONS, getSpotPromotionOptions),
    takeEvery(SPOTPROMOTION.GET_SPOT_PROMOTION, editSpotPromotion),
  ];
}
