import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import * as TERMSANDCONDITIONS from '../actionTypes/TermsAndConditions';
import * as termsandconditionsActionCreators from '../actionCreators/TermsAndConditions';
import { submitTermsAndConditions, termsAndConditionsWatcher } from './TermsAndCondition';


const t = test('test utils', (b) => b);

describe('submitTermsAndConditions', () => {
  it('should call TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = submitTermsAndConditions({ formData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(termsandconditionsActionCreators.submitTermsAndConditionsSuccess(response)));
  });

  it('should call TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = submitTermsAndConditions({ formData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(termsandconditionsActionCreators.submitTermsAndConditionsFailure(response)));
  });

  it('should call TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = submitTermsAndConditions({ formData: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(termsandconditionsActionCreators.submitTermsAndConditionsFailure(response)));
  });

  it('should call termsAndConditionWatcher', () => {
    const generator = termsAndConditionsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS, submitTermsAndConditions),
    ]);
  });
});
