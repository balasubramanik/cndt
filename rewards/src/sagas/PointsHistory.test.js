import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getTransactions, pointsHistoryWatcher } from './PointsHistory';
import * as POINTSHISTORY from '../actionTypes/PointsHistory';
import * as pointsHistoryActionCreators from '../actionCreators/PointsHistory';
const t = test('test utils', (b) => b);
describe('getTransactions', () => {
  it('should call POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getTransactions({ reqBody: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(pointsHistoryActionCreators.getTransactionsSuccess(response.data)));
  });
  it('should call POINTSHISTORY.GET_EXPORT_CSV_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getTransactions({ isExportCSV: true });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(pointsHistoryActionCreators.getExportCSVSuccess(response.data)));
  });
  it('should call POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getTransactions({ reqBody: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(pointsHistoryActionCreators.getTransactionsFailure(response.data)));
  });
  it('should call POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getTransactions({ reqBody: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(pointsHistoryActionCreators.getTransactionsFailure(response)));
  });
  it('should call pointsHistoryWatcher', () => {
    const generator = pointsHistoryWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS, getTransactions),
    ]);
  });
});
