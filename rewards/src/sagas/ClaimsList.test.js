import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getClaimsList, claimsListWatcher } from './ClaimsList';
import * as CLAIMSLIST from '../actionTypes/ClaimsList';
import * as claimsListActionCreators from '../actionCreators/ClaimsList';
const t = test('test utils', (b) => b);
describe('getClaimsList', () => {
  it('should call CLAIMSLIST.GET_CLAIMS_LIST_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getClaimsList({ request: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(claimsListActionCreators.getClaimsListSuccess(response.data)));
  });

  it('should call CLAIMSLIST.GET_EXPORT_CSV_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getClaimsList({ isExportCSV: true });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(claimsListActionCreators.getExportCSVSuccess(response.data)));
  });
  it('should call CLAIMSLIST.GET_CLAIMS_LIST_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getClaimsList({ request: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(claimsListActionCreators.getClaimsListFailure(response.data)));
  });
  it('should call CLAIMSLIST.GET_CLAIMS_LIST_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getClaimsList({ request: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(claimsListActionCreators.getClaimsListFailure(response)));
  });
  it('should call claimsListWatcher', () => {
    const generator = claimsListWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CLAIMSLIST.GET_CLAIMS_LIST, getClaimsList),
    ]);
  });
});
