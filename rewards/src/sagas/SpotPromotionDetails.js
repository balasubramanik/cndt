import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as SPOTPROMOTIONDETAILS from '../actionTypes/SpotPromotionDetails';
import * as spotPromotionDetailsActionCreators from '../actionCreators/SpotPromotionDetails';
import { doPost } from '../utils/fetchWrapper';


export function* getDetails(action) {
  const { id, contractorId } = action;
  try {
    let reqBody = {
      id,
    };
    if (contractorId) {
      reqBody = {
        ...reqBody,
        contractorId,
      };
    }
    const response = yield doPost(envConfig.apiEndPoints.getDetails, reqBody);
    yield put(spotPromotionDetailsActionCreators.getDetailsSuccess(response.data));
  } catch (error) {
    yield put(spotPromotionDetailsActionCreators.getDetailsFailure(error));
  }
}

export function* spotPromotionDetailsWatcher() {
  yield [
    takeLatest(SPOTPROMOTIONDETAILS.GET_DETAILS, getDetails),
  ];
}
