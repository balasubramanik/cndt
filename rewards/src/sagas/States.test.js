import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getStates, statesWatcher } from './States';
import * as STATES from '../actionTypes/States';
import * as statesActionCreators from '../actionCreators/States';
const t = test('test utils', (b) => b);
describe('getStates', () => {
  it('should call GET_STATES_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: [{
        name: 'New jersey',
        code: 'NJ',
      }],
    };
    const generator = getStates();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(statesActionCreators.getStatesSuccess(response.data)));
  });
  it('should call GET_STATES_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = getStates();
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(statesActionCreators.getStatesFailure(error)));
  });
  it('should call statesWatcher', () => {
    const generator = statesWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(STATES.GET_STATES, statesWatcher),
    ]);
  });
});
