import { put, takeEvery } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as CLAIMSLIST from '../actionTypes/ClaimsList';
import * as ClaimsListCreators from '../actionCreators/ClaimsList';
import { doPost } from '../utils/fetchWrapper';


export function* getClaimsList(action) {
  const { request, isExportCSV } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.getClaimsList, request);
    if (isExportCSV) {
      yield put(ClaimsListCreators.getExportCSVSuccess(response.data));
    } else {
      yield put(ClaimsListCreators.getClaimsListSuccess(response.data));
    }
  } catch (error) {
    yield put(ClaimsListCreators.getClaimsListFailure(error));
  }
}

export function* claimsListWatcher() {
  yield [
    takeEvery(CLAIMSLIST.GET_CLAIMS_LIST, getClaimsList),
  ];
}
