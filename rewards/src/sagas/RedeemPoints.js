import { put, takeLatest, takeEvery } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as REDEEMPOINTS from '../actionTypes/RedeemPoints';
import * as redeemPointsActionCreators from '../actionCreators/RedeemPoints';
import * as headerActionCreators from '../actionCreators/Header';
import { doPost } from '../utils/fetchWrapper';

export function* getGiftCatalogue(action) {
  const { category, key } = action;
  try {
    const reqBody = {
      type: category,
    };
    const response = yield doPost(envConfig.apiEndPoints.getGiftCatalogue, reqBody);
    if (response.data) {
      yield put(redeemPointsActionCreators.getGiftCatalogueSuccess(key, category, response.data));
    } else {
      const error = { status: response.status };
      yield put(redeemPointsActionCreators.getGiftCatalogueFailure(key, error));
    }
  } catch (error) {
    yield put(redeemPointsActionCreators.getGiftCatalogueFailure(key, error));
  }
}

export function* getGiftCardDetails(action) {
  const { productId } = action;
  try {
    const reqBody = {
      productId,
    };
    const response = yield doPost(envConfig.apiEndPoints.getProducts, reqBody);
    if (response.data) {
      yield put(redeemPointsActionCreators.getGiftCardDetailsSuccess(productId, response.data));
    } else {
      const error = { status: response.status };
      yield put(redeemPointsActionCreators.getGiftCardDetailsFailure(error));
    }
  } catch (error) {
    yield put(redeemPointsActionCreators.getGiftCardDetailsFailure(error));
  }
}

export function* addToCart(action) {
  const { data } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.addToCart, data);
    if (response.data) {
      yield put(redeemPointsActionCreators.addToCartSuccess(response.data));
      yield put(headerActionCreators.getCartCountSuccess(response.data));
    } else {
      const error = { status: response.status };
      yield put(redeemPointsActionCreators.addToCartFailure(error));
    }
  } catch (error) {
    let err = { ...error };
    if (error && error.errors && error.errors[0] && error.errors[0].code === '03022') {
      err = { ...error, errors: [{ code: 'giftCardExceeds' }] };
    }
    yield put(redeemPointsActionCreators.addToCartFailure(err));
  }
}

export function* redeemPointsWatchers() {
  yield [
    takeEvery(REDEEMPOINTS.GET_GIFT_CATALOGUE, getGiftCatalogue),
    takeLatest(REDEEMPOINTS.ADD_TO_CART, addToCart),
    takeEvery(REDEEMPOINTS.GET_GIFT_CARD_DETAILS, getGiftCardDetails),
  ];
}
