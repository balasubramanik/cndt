import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { submitContactUs, contactUsWatcher } from './ContactUs';
import * as CONTACTUS from '../actionTypes/ContactUs';
import * as contactusActionCreators from '../actionCreators/ContactUs';

const t = test('test utils', (b) => b);

describe('Contactus Saga', () => {
  it('should call SUBMIT_CONTACTUS_SUCCESS', () => {
    const response = {
      data: true,
    };
    const data = {
      info: {
        question: 'ddd',
        claimId: 'G00000002',
        contractorName: 'A Great Looking Roof Co Inc',
        certification: 'ME20840',
      },
      type: 'SUBMIT_CONTACTUS',
    };
    const generator = submitContactUs(data);
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contactusActionCreators.submitContactUsSuccess(response.data)));
  });
  it('should call SUBMIT_CONTACTUS_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const data = {
      info: {
        question: 'ddd',
        claimId: 'G00000002',
        contractorName: 'A Great Looking Roof Co Inc',
        certification: 'ME20840',
      },
      type: 'SUBMIT_CONTACTUS',
    };
    const generator = submitContactUs(data);
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(contactusActionCreators.submitContactUsFailure(error)));
  });
  it('should call contactUsWatcher', () => {
    const generator = contactUsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CONTACTUS.SUBMIT_CONTACTUS, submitContactUs),
    ]);
  });
});
