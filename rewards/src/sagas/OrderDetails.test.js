import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getProduct, orderDetailsWatcher } from './OrderDetails';
import * as ORDERDETAILS from '../actionTypes/OrderDetails';
import * as orderDetailsActionCreators from '../actionCreators/OrderDetails';

const t = test('test utils', (b) => b);

describe('getProduct', () => {
  it('should call ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data:
        [{
          firstName: 'Jude',
          lastName: 'Law',
          orderId: 'O00000215',
          orderDate: '2018-11-29T09:10:56.6173327Z',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          modifiedOn: '2018-11-29T09:10:56.6173327Z',
          cardValue: '5000',
          amountRedeemed: '2000',
        },
        {
          firstName: 'Calvin',
          lastName: 'Klein',
          orderId: 'O00000125',
          orderDate: '2018-11-29T09:10:56.6173327Z',
          serviceFeeType: '$',
          serviceFeeRate: '0.2',
          modifiedOn: '2018-11-29T09:10:56.6173327Z',
          cardValue: '6500',
          amountRedeemed: '5000',
        },
        ],
    };
    const generator = getProduct({ orderID: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(orderDetailsActionCreators.getProductSuccess(response.data)));
  });

  it('should call ORDERDETAILS.GET_LIST_OF_PRODUCT_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getProduct({ orderID: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(orderDetailsActionCreators.getProductFailure(response.data)));
  });

  it('should call ORDERDETAILS.GET_LIST_OF_PRODUCT_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getProduct({ orderID: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(orderDetailsActionCreators.getProductFailure(response)));
  });

  it('should call orderDetailsWatcher', () => {
    const generator = orderDetailsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(ORDERDETAILS.GET_DETAILS, getProduct),
    ]);
  });
});