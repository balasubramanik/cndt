import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getPromotion, spotPromotionWatcher } from './SpotPromotion';
import * as SPOTPROMOTION from '../actionTypes/spotPromotion';
import * as spotPromotionActionCreators from '../actionCreators/spotPromotion';
const t = test('test utils', (b) => b);
describe('getPromotion', () => {
  it('should call SPOTPROMOTION.GET_PROMOTION_LIST_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getPromotion({ reqBody: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(spotPromotionActionCreators.getPromotionSuccess(response.data)));
  });
  it('should call SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getPromotion({ reqBody: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(spotPromotionActionCreators.getPromotionFailure(response.data)));
  });
  it('should call SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getPromotion({ reqBody: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(spotPromotionActionCreators.getPromotionFailure(response)));
  });
  it('should call spotPromotionWatcher', () => {
    const generator = spotPromotionWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(SPOTPROMOTION.GET_PROMOTION_LIST, getPromotion),
    ]);
  });
});
