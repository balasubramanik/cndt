import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as REGISTRATION from '../actionTypes/Registration';
import * as registrationPageActionCreators from '../actionCreators/Registration';
import { doPost } from '../utils/fetchWrapper';

export function* getRegistrationInfo(action) {
  const { id } = action;
  try {
    const reqBody = {
      id,
    };
    const response = yield doPost(envConfig.apiEndPoints.getRegistrationInfo, reqBody);
    if (response.data) {
      yield put(registrationPageActionCreators.getRegistrationInfoSuccess(response.data));
    } else {
      const error = { status: response.status };
      yield put(registrationPageActionCreators.getRegistrationInfoFailure(error));
    }
  } catch (error) {
    yield put(registrationPageActionCreators.getRegistrationInfoFailure(error));
  }
}

export function* registerNow(action) {
  const { formData } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.registerNow, formData);
    if (response.data) {
      yield put(registrationPageActionCreators.registerNowSuccess(response.data));
    } else {
      const error = { status: response.status };
      yield put(registrationPageActionCreators.registerNowFailure(error));
    }
  } catch (error) {
    yield put(registrationPageActionCreators.registerNowFailure(error));
  }
}
export function* registrationWatchers() {
  yield [
    takeLatest(REGISTRATION.REGISTRATION_INFO, getRegistrationInfo),
    takeLatest(REGISTRATION.REGISTER_NOW, registerNow),
  ];
}
