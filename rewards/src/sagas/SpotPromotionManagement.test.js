import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { saveSpotPromotion, getSpotPromotionOptions, editSpotPromotion, SpotPromotionManagementWatchers } from './SpotPromotionManagement';
import * as SPOTPROMOTIONMANAGEMENT from '../actionTypes/SpotPromotionManagement';
import * as SpotPromotionActionCreators from '../actionCreators/SpotPromotionManagement';
const t = test('test utils', (b) => b);
describe('saveSpotPromotion', () => {
  it('should call SPOTPROMOTIONMANAGEMENT.SAVE_SPOTPROMOTION_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = saveSpotPromotion({ data: {}, submitType: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SpotPromotionActionCreators.saveSpotPromotionSuccess(response.data)));
  });

  it('should call SPOTPROMOTIONMANAGEMENT.SAVE_SPOTPROMOTION_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = saveSpotPromotion({ data: {}, submitType: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SpotPromotionActionCreators.saveSpotPromotionFailure(response.data)));
  });
  it('should call SPOTPROMOTIONMANAGEMENT.SAVE_SPOTPROMOTION_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = saveSpotPromotion({ data: {}, submitType: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(SpotPromotionActionCreators.saveSpotPromotionFailure(response)));
  });
  it('should call SpotPromotionManagementWatchers', () => {
    const generator = SpotPromotionManagementWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(SPOTPROMOTIONMANAGEMENT.SAVE_SPOT_PROMOTION, saveSpotPromotion),
    ]);
  });
});

describe('getSpotPromotionOptions', () => {
  it('should call SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getSpotPromotionOptions({ optionType: {}, key: {}, filter: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SpotPromotionActionCreators.getSpotPromotionOptionSuccess(response.data)));
  });

  it('should call SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getSpotPromotionOptions({ optionType: {}, key: {}, filter: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SpotPromotionActionCreators.getSpotPromotionOptionFailure(response.data)));
  });
  it('should call SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getSpotPromotionOptions({ optionType: {}, key: {}, filter: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(SpotPromotionActionCreators.getSpotPromotionOptionFailure(response)));
  });
  it('should call SpotPromotionManagementWatchers', () => {
    const generator = SpotPromotionManagementWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(SPOTPROMOTIONMANAGEMENT.GET_SPOTPROMOTION_OPTIONS, getSpotPromotionOptions),
    ]);
  });
});

describe('editSpotPromotion', () => {
  it('should call SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = editSpotPromotion({ id: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SpotPromotionActionCreators.editSpotPromotionSuccess(response.data)));
  });

  it('should call SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = editSpotPromotion({ id: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SpotPromotionActionCreators.editSpotPromotionFailure(response.data)));
  });
  it('should call SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = editSpotPromotion({ id: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(SpotPromotionActionCreators.editSpotPromotionFailure(response)));
  });
  it('should call SpotPromotionManagementWatchers', () => {
    const generator = SpotPromotionManagementWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(SPOTPROMOTIONMANAGEMENT.GET_SPOT_PROMOTION, editSpotPromotion),
    ]);
  });
});
