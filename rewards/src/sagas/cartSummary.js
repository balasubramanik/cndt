import { put, takeLatest, throttle } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as CARTSUMMARY from '../actionTypes/CartSummary';
import * as headerActionCreators from '../actionCreators/Header';
import * as cartActionCreators from '../actionCreators/cartSummary';
import { doPost } from '../utils/fetchWrapper';

export function* getCartSummary() {
  try {
    const response = yield doPost(envConfig.apiEndPoints.getCartSummary);
    yield put(cartActionCreators.getCartSummarySuccess(response.data));
  } catch (error) {
    yield put(cartActionCreators.getCartSummaryFailure(error));
  }
}

export function* updateCart(action) {
  try {
    const response = yield doPost(envConfig.apiEndPoints.addToCart, action.data);
    yield put(cartActionCreators.updateCartSuccess(response.data));
  } catch (error) {
    yield put(cartActionCreators.updateCartFailure(error));
  }
}

export function* deleteCart(action) {
  try {
    const response = yield doPost(envConfig.apiEndPoints.addToCart, action.data);
    if (response.data) {
      yield put(cartActionCreators.deleteCartSuccess(response.data));
      yield put(headerActionCreators.getCartCountSuccess(response.data));
    }
  } catch (error) {
    yield put(cartActionCreators.deleteCartFailure(error));
  }
}

export function* submitOrder(action) {
  try {
    const response = yield doPost(envConfig.apiEndPoints.submitOrder, action.data);
    yield put(cartActionCreators.submitCartSuccess(response));
    yield put(headerActionCreators.getCartCountSuccess());
  } catch (error) {
    yield put(cartActionCreators.submitCartFailure(error));
  }
}

export function* cartSummaryWatcher() {
  yield [
    takeLatest(CARTSUMMARY.GET_CART_SUMMARY, getCartSummary),
    throttle(3000, CARTSUMMARY.UPDATE_CART_SUMMARY, updateCart),
    takeLatest(CARTSUMMARY.DELETE_ITEM_CART_SUMMARY, deleteCart),
    takeLatest(CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY, submitOrder),
  ];
}
