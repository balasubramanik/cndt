import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as EMAIL from '../actionTypes/EmailVerification';
import * as emailVerificationActionCreators from '../actionCreators/EmailVerification';
import { doPost } from '../utils/fetchWrapper';


export function* submitEmailVerification(action) {
  const { formData } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.submitEmailVerification, formData);
    if (response.data) {
      yield put(emailVerificationActionCreators.submitEmailVerificationSuccess(response.data));
    } else {
      const error = { status: response.status };
      yield put(emailVerificationActionCreators.submitEmailVerificationFailure(error));
    }
  } catch (error) {
    yield put(emailVerificationActionCreators.submitEmailVerificationFailure(error));
  }
}

export function* resendEmail(action) {
  const { reqBody } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.resendEmail, reqBody);
    if (response.data) {
      yield put(emailVerificationActionCreators.resendEmailSuccess(response.data));
    } else {
      const error = { status: response.status };
      yield put(emailVerificationActionCreators.resendEmailFailure(error));
    }
  } catch (error) {
    yield put(emailVerificationActionCreators.resendEmailFailure(error));
  }
}

export function* emailVerificationWatchers() {
  yield [
    takeLatest(EMAIL.SUBMIT_EMAIL_VERIFICATION, submitEmailVerification),
    takeLatest(EMAIL.RESEND_EMAIL, resendEmail),
  ];
}
