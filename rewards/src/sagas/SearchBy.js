import { put, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import envConfig from 'envConfig'; //eslint-disable-line
import { searchByType } from '../constants/constants';
import * as searchBy from '../actionTypes/SearchBy';
import * as searchByActionCreators from '../actionCreators/SearchBy';
import { doPost } from '../utils/fetchWrapper';
import { authentication } from '../routes';

export function* getSearchBy(action) {
  const { searchText, searchByName } = action;
  const { contractorId } = authentication.getAuthDetails();
  const searchName = 'product';
  let url = '';
  let reqBody = {};
  if (searchByName === searchByType.ClaimStatusNumber) {
    url = envConfig.apiEndPoints.getClaimStatusNumber;
    reqBody = {
      claimNumber: searchText,
      contractorAccountId: contractorId,
    };
  } else if (searchByName === searchByType.PointsHistoryClaimNumber) {
    url = envConfig.apiEndPoints.getClaimAndOrderNumber;
    reqBody = {
      orderNumber: searchText,
      contractorAccountId: contractorId,
    };
  } else if (searchByName === searchByType.ClaimProductDetails || searchByName === searchByType.ClaimDetailProduct || searchByName === searchByType.OrderListProduct) {
    url = envConfig.apiEndPoints.getLookups;
    reqBody = {
      type: searchName,
      restrictTo: searchText,
    };
  }
  try {
    yield delay(500);
    const response = yield doPost(url, reqBody);
    yield put(searchByActionCreators.getSuggestionSuccess(response.data, searchByName));
  } catch (error) {
    yield put(searchByActionCreators.getSuggestionFailure(error, searchByName));
  }
}

export function* searchByWatcher() {
  yield [
    takeLatest(searchBy.GET_SUGGESTION, getSearchBy),
  ];
}
