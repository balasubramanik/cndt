import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getCategory, getCategoryPromotions, whatQualifiesWatcher } from './WhatQualifies';
import * as WHATQUALIFIES from '../actionTypes/WhatQualifies';
import * as whatQualifiesActionCreators from '../actionCreators/WhatQualifies';

const t = test('test utils', (b) => b);

describe('getCategory', () => {
  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      message: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getCategory({ year: '2018' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(whatQualifiesActionCreators.getCategorySuccess(response)));
  });

  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS with valid contractorID', () => {
    const response = {
      response: { responseCode: 200 },
      message: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getCategory({ year: '2018', contractorId: 'ABCD123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(whatQualifiesActionCreators.getCategorySuccess(response)));
  });

  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getCategory({ year: '2018', contractorId: 'ABCD123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(whatQualifiesActionCreators.getCategoryFailure(response.data)));
  });

  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getCategory({ year: '2018', contractorId: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(whatQualifiesActionCreators.getCategoryFailure(response)));
  });

  it('should call whatQualifiesWatcher', () => {
    const generator = whatQualifiesWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(WHATQUALIFIES.GET_MYPLAN_CATEGORY, getCategory),
    ]);
  });
});

describe('getCategoryPromotions', () => {
  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS', () => {
    const response = {
      response: { responseCode: 200 },
      message: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getCategoryPromotions({ year: '2018', planId: 'SS2345' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(whatQualifiesActionCreators.getCategoryPromotionsSuccess(response)));
  });
  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS with valid contractor ID', () => {
    const response = {
      response: { responseCode: 200 },
      message: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getCategoryPromotions({ year: '2018', contractorId: 'ABCD123', planId: 'SS2345' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(whatQualifiesActionCreators.getCategoryPromotionsSuccess(response)));
  });
  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getCategoryPromotions({ year: '2018', contractorId: 'ABCD123', planId: 'SS2345' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(whatQualifiesActionCreators.getCategoryPromotionsFailure(response.data)));
  });

  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getCategoryPromotions({ year: '2018', contractorId: {}, planId: 'SS2345' });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(whatQualifiesActionCreators.getCategoryPromotionsFailure(response)));
  });
});
