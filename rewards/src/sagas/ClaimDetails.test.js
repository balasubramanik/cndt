import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getInvoiceList, claimDetailsWatcher } from './ClaimDetails';
import * as CLAIMDETAILS from '../actionTypes/ClaimDetails';
import * as claimDetailsActionCreators from '../actionCreators/ClaimDetails';
const t = test('test utils', (b) => b);
describe('getInvoiceList', () => {
  it('should call CLAIMDETAILS.GET_INVOICE_LIST_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: [{
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Cobra® Snow Country',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: '-32',
        submissionValue: '-2816',
        pointsEarned: '-28.16',
        rate: '3',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
      ],
    };
    const generator = getInvoiceList({ claimID: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(claimDetailsActionCreators.getInvoiceListSuccess(response.data)));
  });
  it('should call CLAIMDETAILS.GET_INVOICE_LIST_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getInvoiceList({ claimID: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(claimDetailsActionCreators.getInvoiceListFailure(response.data)));
  });
  it('should call CLAIMDETAILS.GET_INVOICE_LIST_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getInvoiceList({ claimID: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(claimDetailsActionCreators.getInvoiceListFailure(response)));
  });
  it('should call claimDetailsWatcher', () => {
    const generator = claimDetailsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CLAIMDETAILS.GET_DETAILS, getInvoiceList),
    ]);
  });
});
