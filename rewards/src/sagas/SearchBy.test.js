import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getSearchBy, searchByWatcher } from './SearchBy';
import * as SEARCHBY from '../actionTypes/SearchBy';
import * as SearchByActionCreators from '../actionCreators/SearchBy';
const t = test('test utils', (b) => b);
describe('getSearchBy test cases', () => {
  it('should call SEARCHBY.GET_SUGGESTION_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getSearchBy({ searchText: 'SE1234', searchByName: 'cityList' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SearchByActionCreators.getSuggestionSuccess(response.data)));
  });
  it('should call SEARCHBY.GET_SUGGESTION_SUCCESS with searchByType ClaimStatusNumber', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getSearchBy({ searchText: {}, searchByName: 'CLAIM_STATUS_NUMBER' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SearchByActionCreators.getSuggestionSuccess(response.data)));
  });
  it('should call SEARCHBY.GET_SUGGESTION_SUCCESS with searchByType PointsHistoryClaimNumber', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getSearchBy({ searchText: {}, searchByName: 'POINT_HISTORY_CLAIM_NUMBER' });
    let next = generator.next();
    generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SearchByActionCreators.getSuggestionSuccess(response.data)));
  });
  it('should call SEARCHBY.GET_SUGGESTION_SUCCESS with searchByType ClaimProductDetails', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getSearchBy({ searchText: {}, searchByName: 'CLAIM_PRODUCT_DETAILS' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SearchByActionCreators.getSuggestionSuccess(response.data)));
  });
  it('should call SEARCHBY.GET_SUGGESTION_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getSearchBy({ searchText: {}, searchByName: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(SearchByActionCreators.getSuggestionFailure(response.data)));
  });
  it('should call SEARCHBY.GET_SUGGESTION_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getSearchBy({ searchText: {}, searchByName: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(SearchByActionCreators.getSuggestionFailure(response)));
  });
  it('should call searchByWatcher', () => {
    const generator = searchByWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(SEARCHBY.GET_SUGGESTION, getSearchBy),
    ]);
  });
});
