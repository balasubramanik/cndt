import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import * as REGISTRATION from '../actionTypes/Registration';
import * as registrationPageActionCreators from '../actionCreators/Registration';
import { getRegistrationInfo, registerNow, registrationWatchers } from './Registration';

const t = test('test utils', (b) => b);

describe('getRegistrationInfo verification', () => {
  it('should call REGISTRATION_INFO_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getRegistrationInfo({ id: '123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(registrationPageActionCreators.getRegistrationInfoSuccess(response.data)));
  });

  it('should call REGISTRATION_INFO_FAILURE without data', () => {
    const response = {
      status: 204,
      data: null,
    };
    const generator = getRegistrationInfo({ id: '123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(registrationPageActionCreators.getRegistrationInfoFailure({ status: 204 })));
  });

  it('should call REGISTRATION_INFO_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = getRegistrationInfo({ id: '123' });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(registrationPageActionCreators.getRegistrationInfoFailure(error)));
  });

  it('should call registrationWatchers', () => {
    const generator = registrationWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(REGISTRATION.REGISTRATION_INFO, getRegistrationInfo),
      takeLatest(REGISTRATION.REGISTER_NOW, registerNow),
    ]);
  });
});


describe('registerNow verification', () => {
  it('should call REGISTER_NOW_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = registerNow({ formData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(registrationPageActionCreators.registerNowSuccess(response.data)));
  });

  it('should call REGISTER_NOW_FAILURE without data', () => {
    const response = {
      status: 204,
      data: null,
    };
    const generator = registerNow({ formData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(registrationPageActionCreators.registerNowFailure({ status: 204 })));
  });

  it('should call REGISTER_NOW_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = registerNow({ formData: {} });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(registrationPageActionCreators.registerNowFailure(error)));
  });
});
