import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { submitEmailVerification, emailVerificationWatchers, resendEmail } from './EmailVerification';
import * as EMAIL from '../actionTypes/EmailVerification';
import * as emailVerificationActionCreators from '../actionCreators/EmailVerification';

const t = test('test utils', (b) => b);

describe('Email verification', () => {
  it('should call SUBMIT_EMAIL_VERIFICATION_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = submitEmailVerification({ formData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(emailVerificationActionCreators.submitEmailVerificationSuccess(response.data)));
  });

  it('should call SUBMIT_EMAIL_VERIFICATION_FAILURE without data', () => {
    const response = {
      status: 204,
      data: null,
    };
    const generator = submitEmailVerification({ formData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(emailVerificationActionCreators.submitEmailVerificationFailure({ status: 204 })));
  });

  it('should call SUBMIT_EMAIL_VERIFICATION_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = submitEmailVerification({ formData: {} });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(emailVerificationActionCreators.submitEmailVerificationFailure(error)));
  });

  it('should call emailVerificationWatchers', () => {
    const generator = emailVerificationWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(EMAIL.SUBMIT_EMAIL_VERIFICATION, submitEmailVerification),
      takeLatest(EMAIL.RESEND_EMAIL, resendEmail),
    ]);
  });
});

describe('Resend Email', () => {
  it('should call RESEND_EMAIL_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = resendEmail({ id: '123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(emailVerificationActionCreators.resendEmailSuccess(response.data)));
  });

  it('should call RESEND_EMAIL_FAILURE without data', () => {
    const response = {
      status: 204,
      data: null,
    };
    const generator = resendEmail({ id: '123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(emailVerificationActionCreators.resendEmailFailure({ status: 204 })));
  });

  it('should call RESEND_EMAIL_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = resendEmail({ id: '123' });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(emailVerificationActionCreators.resendEmailFailure(error)));
  });

  it('should call emailVerificationWatchers', () => {
    const generator = emailVerificationWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(EMAIL.SUBMIT_EMAIL_VERIFICATION, submitEmailVerification),
      takeLatest(EMAIL.RESEND_EMAIL, resendEmail),
    ]);
  });
});