import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as CONTACTUS from '../actionTypes/ContactUs';
import * as ContactUs from '../actionCreators/ContactUs';
import { doPost } from '../utils/fetchWrapper';


export function* submitContactUs(action) {
  try {
    const { info } = action;
    const reqBody = {
      ...info,
      comment: info.question,
    };
    const response = yield doPost(envConfig.apiEndPoints.contactSupport, reqBody);
    yield put(ContactUs.submitContactUsSuccess(response.data));
  } catch (error) {
    const isTimeoutError = error === 504;
    yield put(ContactUs.submitContactUsFailure({ isTimeoutError, isServerError: !isTimeoutError, error }));
  }
}

export function* contactUsWatcher() {
  yield [
    takeLatest(CONTACTUS.SUBMIT_CONTACTUS, submitContactUs),
  ];
}

