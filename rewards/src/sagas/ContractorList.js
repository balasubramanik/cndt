import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as CONTRACTORLIST from '../actionTypes/ContractorList';
import * as contractorListActionCreators from '../actionCreators/ContractorList';
import { doPost } from '../utils/fetchWrapper';


export function* getContractorList(action) {
  try {
    const response = yield doPost(envConfig.apiEndPoints.getContractorList, action.info);
    yield put(contractorListActionCreators.getContractorListSuccess(response.data));
  } catch (error) {
    yield put(contractorListActionCreators.getContractorListFailure(error));
  }
}

export function* achToggle(action) {
  const { contractorId, isAchEnabled } = action.selectedRow;
  const { email } = action;
  const data = {
    contractorId,
    isAchEnabled,
    modifiedBy: email,
  };
  try {
    const response = yield doPost(envConfig.apiEndPoints.achToggle, data);
    yield put(contractorListActionCreators.achToggleSuccess(response.data));
  } catch (error) {
    yield put(contractorListActionCreators.achToggleFailure(error));
  }
}

export function* submitDetailedReport(action) {
  try {
    const data = {
      year: action.data,
    };
    const response = yield doPost(envConfig.apiEndPoints.detailedReportSubmit, data);
    yield put(contractorListActionCreators.submitDetailedReportSuccess(response.data));
  } catch (error) {
    yield put(contractorListActionCreators.submitDetailedReportFailure(error));
  }
}

export function* contractorListWatchers() {
  yield [
    takeLatest(CONTRACTORLIST.GET_CONTRACTOR_LIST, getContractorList),
    takeLatest(CONTRACTORLIST.ACH_TOGGLE, achToggle),
    takeLatest(CONTRACTORLIST.SUBMIT_DETAILED_REPORT, submitDetailedReport),
  ];
}
