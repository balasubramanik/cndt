import { put, takeEvery } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as SPOTPROMOTION from '../actionTypes/spotPromotion';
import * as spotPromotionActionCreators from '../actionCreators/spotPromotion';
import { doPost } from '../utils/fetchWrapper';


export function* getPromotion(action) {
  const { reqBody } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.getSpotPromotion, reqBody);
    yield put(spotPromotionActionCreators.getPromotionSuccess(response.data));
  } catch (error) {
    yield put(spotPromotionActionCreators.getPromotionFailure(error));
  }
}

export function* spotPromotionWatcher() {
  yield [
    takeEvery(SPOTPROMOTION.GET_PROMOTION_LIST, getPromotion),
  ];
}
