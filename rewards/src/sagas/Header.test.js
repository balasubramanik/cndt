import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getCartCount, headerWatchers } from './Header';
import * as HEADER from '../actionTypes/Header';
import * as headerActionCreators from '../actionCreators/Header';

const t = test('test utils', (b) => b);

describe('getCartCount', () => {
  it('should call GET_CART_COUNT_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        count: 5,
      },
    };
    const generator = getCartCount();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(headerActionCreators.getCartCountSuccess(response.data)));
  });

  it('should call GET_CART_COUNT_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getCartCount();
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(headerActionCreators.getCartCountFailure(error)));
  });

  it('should call headerWatchers', () => {
    const generator = headerWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [takeLatest(HEADER.GET_CART_COUNT, getCartCount)]);
  });
});
