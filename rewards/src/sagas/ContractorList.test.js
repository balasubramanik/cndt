import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getContractorList, achToggle, submitDetailedReport, contractorListWatchers } from './ContractorList';
import * as CONTRACTORLIST from '../actionTypes/ContractorList';
import * as contractorListActionCreators from '../actionCreators/ContractorList';
const t = test('test utils', (b) => b);
describe('getContractorList', () => {
  it('should call CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getContractorList({ info: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorListActionCreators.getContractorListSuccess(response.data)));
  });
  it('should call CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getContractorList({ info: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorListActionCreators.getContractorListFailure(response.data)));
  });
  it('should call CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getContractorList({ info: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(contractorListActionCreators.getContractorListFailure(response)));
  });
  it('should call contractorListWatchers', () => {
    const generator = contractorListWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CONTRACTORLIST.GET_CONTRACTOR_LIST, getContractorList),
    ]);
  });
});
describe('achToggle', () => {
  it('should call CONTRACTORLIST.ACH_TOGGLE_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = achToggle({ selectedRow: { contractorId: 'SE1234', isAchEnabled: {} }, email: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorListActionCreators.achToggleSuccess(response.data)));
  });
  it('should call CONTRACTORLIST.ACH_TOGGLE_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = achToggle({ selectedRow: { contractorId: 'SE1234', isAchEnabled: {} }, email: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorListActionCreators.achToggleFailure(response.data)));
  });
  it('should call CONTRACTORLIST.ACH_TOGGLE_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = achToggle({ selectedRow: { contractorId: 'SE1234', isAchEnabled: {} }, email: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(contractorListActionCreators.achToggleFailure(response)));
  });
  it('should call contractorListWatchers', () => {
    const generator = contractorListWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CONTRACTORLIST.ACH_TOGGLE, achToggle),
    ]);
  });
});
describe('submitDetailedReport', () => {
  it('should call CONTRACTORLIST.SUBMIT_DETAILED_REPORT_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = submitDetailedReport({ year: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorListActionCreators.submitDetailedReportSuccess(response.data)));
  });
  it('should call CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = submitDetailedReport({ year: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorListActionCreators.submitDetailedReportFailure(response.data)));
  });
  it('should call CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = submitDetailedReport({ year: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(contractorListActionCreators.submitDetailedReportFailure(response)));
  });
  it('should call contractorListWatchers', () => {
    const generator = contractorListWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CONTRACTORLIST.SUBMIT_DETAILED_REPORT, submitDetailedReport),
    ]);
  });
});
