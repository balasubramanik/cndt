import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as ORDERDETAILS from '../actionTypes/OrderDetails';
import * as orderDetailsActionCreators from '../actionCreators/OrderDetails';
import { doPost } from '../utils/fetchWrapper';


export function* getProduct(action) {
  const { orderID } = action;
  try {
    const reqBody = {
      id: orderID,
    };
    const response = yield doPost(envConfig.apiEndPoints.getPoints, reqBody);
    yield put(orderDetailsActionCreators.getProductSuccess(response.data));
  } catch (error) {
    yield put(orderDetailsActionCreators.getProductFailure(error));
  }
}
export function* orderDetailsWatcher() {
  yield [
    takeLatest(ORDERDETAILS.GET_LIST_OF_PRODUCT, getProduct),
  ];
}
