import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getInvoiceList, downloadFile, claimInvoiceDetailsWatcher } from './ClaimInvoiceDetails';
import * as CLAIMINVOICEDETAILS from '../actionTypes/ClaimInvoiceDetails';
import * as InvoiceListCreators from '../actionCreators/ClaimInvoiceDetails';
const t = test('test utils', (b) => b);

describe('getInvoiceList', () => {
  it('should call CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = getInvoiceList({ claimId: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(InvoiceListCreators.getClaimsInvoiceListSuccess(response.data)));
  });
  it('should call CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getInvoiceList({ claimId: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(InvoiceListCreators.getClaimsInvoiceListFailure(response.data)));
  });
  it('should call CLAIMINVOICEDETAILS.GET_CLAIMS_INVOICE_LIST_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getInvoiceList({ claimId: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(InvoiceListCreators.getClaimsInvoiceListFailure(response)));
  });
  it('should call claimInvoiceDetailsWatcher', () => {
    const generator = claimInvoiceDetailsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CLAIMINVOICEDETAILS.GET_LIST_OF_TRANSACTIONS, getInvoiceList),
    ]);
  });
});

describe('downloadFile', () => {
  it('should call CLAIMINVOICEDETAILS.DOWNLOAD_FILE_SUCCESS', () => {
    const response = {
      responseCode: 200,
    };
    const generator = downloadFile({ fileData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(InvoiceListCreators.downloadInvoiceSuccess(response.data)));
  });
  it('should call CLAIMINVOICEDETAILS.DOWNLOAD_FILE_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = downloadFile({ fileData: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(InvoiceListCreators.downloadInvoiceFailure(response.data)));
  });
  it('should call CLAIMINVOICEDETAILS.DOWNLOAD_FILE_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = downloadFile({ fileData: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(InvoiceListCreators.downloadInvoiceFailure(response)));
  });
  it('should call claimInvoiceDetailsWatcher', () => {
    const generator = claimInvoiceDetailsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CLAIMINVOICEDETAILS.DOWNLOAD_FILE, downloadFile),
    ]);
  });
});
