import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getDetails, spotPromotionDetailsWatcher } from './SpotPromotionDetails';
import * as SPOTPROMOTIONDETAILS from '../actionTypes/SpotPromotionDetails';
import * as spotPromotionDetailsActionCreators from '../actionCreators/SpotPromotionDetails';

const t = test('test utils', (b) => b);

describe('getDetails', () => {
  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        startDate: '2018-08-01T16:57:19.0487212+05:30',
        endDate: '2018-08-11T16:57:19.0487212+05:30',
      },
    };
    const generator = getDetails({ planID: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(spotPromotionDetailsActionCreators.getDetailsSuccess(response.data)));
  });

  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE with service timeout', () => {
    const response = { responseCode: 504, data: 'timeout error' };
    const generator = getDetails({ planID: {} });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(spotPromotionDetailsActionCreators.getDetailsFailure(response.data)));
  });

  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE with service failure', () => {
    const error = { responseCode: 500, data: 'Internal server error' };
    const generator = getDetails({ planID: {} });
    generator.next();
    generator.next(error);
    const response = { isTimeoutError: false, isServerError: true, error };
    t.deepEqual(generator.throw(error).value, put(spotPromotionDetailsActionCreators.getDetailsFailure(response)));
  });

  it('should call spotPromotionDetailsWatcher', () => {
    const generator = spotPromotionDetailsWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(SPOTPROMOTIONDETAILS.GET_DETAILS, getDetails),
    ]);
  });
});
