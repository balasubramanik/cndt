import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as CLAIMDETAILS from '../actionTypes/ClaimDetails';
import * as ClaimDetailsCreators from '../actionCreators/ClaimDetails';
import { doPost } from '../utils/fetchWrapper';


export function* getInvoiceList(action) {
  const { claimID } = action;
  try {
    const reqBody = {
      id: claimID,
    };
    const response = yield doPost(envConfig.apiEndPoints.getInvoiceList, reqBody);
    yield put(ClaimDetailsCreators.getInvoiceListSuccess(response.data));
  } catch (error) {
    yield put(ClaimDetailsCreators.getInvoiceListFailure(error));
  }
}

export function* claimDetailsWatcher() {
  yield [
    takeLatest(CLAIMDETAILS.GET_INVOICE_LIST, getInvoiceList),
  ];
}

