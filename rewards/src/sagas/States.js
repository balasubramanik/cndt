import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as STATE from '../actionTypes/States';
import * as stateActionCreators from '../actionCreators/States';
import { doPost } from '../utils/fetchWrapper';
export function* getStates() {
  try {
    const reqBody = {
      type: 'state',
      restrictTo: 'USA',
    };
    const response = yield doPost(envConfig.apiEndPoints.getLookups, reqBody);
    yield put(stateActionCreators.getStatesSuccess(response.data));
  } catch (error) {
    yield put(stateActionCreators.getStatesFailure(error));
  }
}
export function* statesWatcher() {
  yield [
    takeLatest(STATE.GET_STATES, getStates),
  ];
}
