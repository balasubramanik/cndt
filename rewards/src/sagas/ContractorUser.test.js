import test from 'tape';
import { put, takeLatest } from 'redux-saga/effects';
import { getUsers, contractorUserWatcher } from './ContractorUser';
import * as CONTRACTOR from '../actionTypes/ContractorUser';
import * as contractorUserActionCreators from '../actionCreators/ContractorUser';

const t = test('test utils', (b) => b);

describe('Contractor User', () => {
  it('should call GET_USERS_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getUsers();
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(contractorUserActionCreators.getUsersSuccess(response.data)));
  });

  it('should call GET_USERS_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = getUsers();
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(contractorUserActionCreators.getUsersFailure(error)));
  });

  it('should call contractorUserWatcher', () => {
    const generator = contractorUserWatcher();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeLatest(CONTRACTOR.GET_USERS, getUsers),
    ]);
  });
});
