import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as TERMSANDCONDITIONS from '../actionTypes/TermsAndConditions';
import * as termsandconditionsActionCreators from '../actionCreators/TermsAndConditions';
import { doPost } from '../utils/fetchWrapper';


export function* submitTermsAndConditions(action) {
  const { formData } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.TermsAndConditions, formData);
    yield put(termsandconditionsActionCreators.submitTermsAndConditionsSuccess(response.data));
  } catch (error) {
    yield put(termsandconditionsActionCreators.submitTermsAndConditionsFailure(error));
  }
}

export function* termsAndConditionsWatcher() {
  yield [
    takeLatest(TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS, submitTermsAndConditions),
  ];
}

