import test from 'tape';
import { put, takeEvery, takeLatest } from 'redux-saga/effects';
import { getGiftCatalogue, redeemPointsWatchers, addToCart, getGiftCardDetails } from './RedeemPoints';
import * as REDEEMPOINTS from '../actionTypes/RedeemPoints';
import * as redeemPointsActionCreators from '../actionCreators/RedeemPoints';

const t = test('test utils', (b) => b);

describe('Redeem Points', () => {
  it('should call GET_GIFT_CATALOGUE_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getGiftCatalogue({ category: '', key: '' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(redeemPointsActionCreators.getGiftCatalogueSuccess('', '', response.data)));
  });

  it('should call GET_GIFT_CATALOGUE_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = getGiftCatalogue({ category: '', key: '' });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(redeemPointsActionCreators.getGiftCatalogueFailure('', error)));
  });

  it('should call GET_GIFT_CARD_DETAILS_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = getGiftCardDetails({ productId: 'MA123' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(redeemPointsActionCreators.getGiftCardDetailsSuccess('MA123', response.data)));
  });

  it('should call GET_GIFT_CARD_DETAILS_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = getGiftCardDetails({ productId: 'MA123' });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(redeemPointsActionCreators.getGiftCardDetailsFailure('', error)));
  });

  it('should call ADD_TO_CART_SUCCESS', () => {
    const response = {
      responseCode: 200,
      data: {
        statusCode: '300',
        status: 'success',
      },
    };
    const generator = addToCart({ data: '' });
    let next = generator.next();
    next = generator.next(response);
    t.deepEqual(next.value, put(redeemPointsActionCreators.addToCartSuccess(response.data)));
  });

  it('should call ADD_TO_CART_FAILURE with service failure', () => {
    const error = { responseCode: 500, error: 'Internal server error' };
    const generator = addToCart({ data: '' });
    generator.next();
    generator.next(error);
    t.deepEqual(generator.throw(error).value, put(redeemPointsActionCreators.addToCartFailure(error)));
  });

  it('should call redeemPointsWatchers', () => {
    const generator = redeemPointsWatchers();
    const next = generator.next().value;
    t.deepEqual(next, [
      takeEvery(REDEEMPOINTS.GET_GIFT_CATALOGUE, getGiftCatalogue),
      takeLatest(REDEEMPOINTS.ADD_TO_CART, addToCart),
      takeEvery(REDEEMPOINTS.GET_GIFT_CARD_DETAILS, getGiftCardDetails),
    ]);
  });
});
