import { put, takeEvery } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as POINTSHISTORY from '../actionTypes/PointsHistory';
import * as pointsHistoryActionCreators from '../actionCreators/PointsHistory';
import { doPost } from '../utils/fetchWrapper';


export function* getTransactions(action) {
  const { reqBody, isExportCSV } = action;
  try {
    const response = yield doPost(envConfig.apiEndPoints.getTransactions, reqBody);
    if (isExportCSV) {
      yield put(pointsHistoryActionCreators.getExportCSVSuccess(response.data));
    } else {
      yield put(pointsHistoryActionCreators.getTransactionsSuccess(response.data));
    }
  } catch (error) {
    yield put(pointsHistoryActionCreators.getTransactionsFailure(error));
  }
}

export function* pointsHistoryWatcher() {
  yield [
    takeEvery(POINTSHISTORY.GET_LIST_OF_TRANSACTIONS, getTransactions),
  ];
}
