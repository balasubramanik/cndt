import { put, takeLatest } from 'redux-saga/effects';
import envConfig from 'envConfig'; //eslint-disable-line
import * as INVOICELIST from '../actionTypes/ClaimInvoiceDetails';
import * as InvoiceListCreators from '../actionCreators/ClaimInvoiceDetails';
import { doPost, doPostBlob } from '../utils/fetchWrapper';

export function* getInvoiceList(action) {
  const { claimId } = action;
  try {
    const reqBody = {
      id: claimId,
    };
    const response = yield doPost(envConfig.apiEndPoints.getClaimInvoiceDetailsList, reqBody);
    yield put(InvoiceListCreators.getClaimsInvoiceListSuccess(response.data));
  } catch (error) {
    yield put(InvoiceListCreators.getClaimsInvoiceListFailure(error));
  }
}

export function* downloadFile(action) {
  const { fileData } = action;
  try {
    const response = yield doPostBlob(envConfig.apiEndPoints.downloadInvoice, fileData);
    yield put(InvoiceListCreators.downloadInvoiceSuccess(response));
  } catch (error) {
    yield put(InvoiceListCreators.downloadInvoiceFailure(error));
  }
}

export function* claimInvoiceDetailsWatcher() {
  yield [
    takeLatest(INVOICELIST.GET_CLAIMS_INVOICE_LIST, getInvoiceList),
    takeLatest(INVOICELIST.DOWNLOAD_FILE, downloadFile),
  ];
}
