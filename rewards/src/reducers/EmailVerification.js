import * as EMAIL from '../actionTypes/EmailVerification';

const initialState = {
  values: {
    step: 0,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case EMAIL.CHANGE_ROOF_OPTIONS:
      return {
        ...state,
        values: {
          ...state.values,
          installedOption: '',
          contractorClassification: '',
          authorizeeDesignation: '',
          description: '',
          termsAndConditions: false,
        },
      };
    case EMAIL.CHANGE_INSTALL_ROOF_OPTIONS:
      return {
        ...state,
        values: {
          ...state.values,
          authorizeeDesignation: '',
        },
      };
    case EMAIL.SUBMIT_EMAIL_VERIFICATION:
      return {
        ...state,
        isLoading: true,
        done: false,
        isEmailExist: false,
      };
    case EMAIL.SUBMIT_EMAIL_VERIFICATION_SUCCESS: {
      const { data } = action;
      const { emailExists, id, status } = data || {};
      return {
        ...state,
        isLoading: false,
        done: true,
        isEmailExist: emailExists,
        guid: id,
        status,
      };
    }
    case EMAIL.RESEND_EMAIL:
      return {
        ...state,
        isLoading: true,
      };
    case EMAIL.RESEND_EMAIL_SUCCESS: {
      const { data } = action;
      const { status } = data || {};
      return {
        ...state,
        isLoading: false,
        status,
      };
    }
    case EMAIL.RESEND_EMAIL_FAILURE:
    case EMAIL.SUBMIT_EMAIL_VERIFICATION_FAILURE:
      return {
        ...state,
        isLoading: false,
        done: false,
        error: action.error,
      };
    case EMAIL.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};
