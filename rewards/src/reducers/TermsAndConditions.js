import * as TERMSANDCONDITIONS from '../actionTypes/TermsAndConditions';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS:
      return {
        ...state,
        isLoading: true,
        done: false,
      };
    case TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        done: true,
      };
    case TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        done: false,
        error: action.error,
      };
    default:
      return state;
  }
};
