import * as SPOTPROMOTIONDETAILS from '../actionTypes/SpotPromotionDetails';
import Reducer from './SpotPromotionDetails';

const initialState = {
  imageInfo: [{
    headerDesc: 'Timberline Ultra HD Promotion',
  }],
  promotionsInfo: [{
    startDate: '2018-08-01T16:57:19.0487212+05:30',
    endDate: '2018-08-11T16:57:19.0487212+05:30',
    submissionDueDate: '2018-08-11T16:57:19.0487212+05:30',
    state: ['NJ, PA, OH, NY, CA'],
  }],
};

describe('SpotPromotionDetails Details Reducers', () => {
  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS', () => {
    const action = { type: SPOTPROMOTIONDETAILS.GET_DETAILS };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });


  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS_SUCCESS', () => {
    const action = {
      type: SPOTPROMOTIONDETAILS.GET_DETAILS_SUCCESS,
      data: [{
        endDate: '2018-08-11T16:57:19.0487212+05:30', startDate: '2018-08-01T16:57:19.0487212+05:30', state: ['NJ, PA, OH, NY, CA'], submissionDueDate: '2018-08-11T16:57:19.0487212+05:30',
      }],
    };
    const expected = {
      ...initialState,
      isLoading: false,
      promotionsInfo: [{
        endDate: '2018-08-11T16:57:19.0487212+05:30', startDate: '2018-08-01T16:57:19.0487212+05:30', state: ['NJ, PA, OH, NY, CA'], submissionDueDate: '2018-08-11T16:57:19.0487212+05:30',
      }],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE', () => {
    const action = { type: SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      promotionsError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SPOTPROMOTIONDETAILS.GET_DETAILS_CLEAR', () => {
    const action = { type: SPOTPROMOTIONDETAILS.GET_DETAILS_CLEAR };
    const expected = {
      imageInfo: [{
        headerDesc: 'Timberline Ultra HD Promotion',
      }],
      isLoading: false,
      promotionsInfo: {},
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      promotionsInfo: {},
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
