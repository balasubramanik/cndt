import * as CLAIMSLIST from '../actionTypes/ClaimsList';
import Reducer from './ClaimsList';

const initialState = {};

describe('CLAIMSLIST Reducers for get user info', () => {
  it('should call GET_CLAIMS_LIST', () => {
    const action = { type: CLAIMSLIST.GET_CLAIMS_LIST };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_LIST_SUCCESS', () => {
    const data = {
      filter: {
        claimNumber: '',
        product: '',
        status: '',
        userName: '',
        matchAll: true,
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'c.ModifiedOn',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      result: [{
        claimId: 'G00000179',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000179',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:26:18.4603748Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }, {
        claimId: 'G00000178',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000178',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:23:24.5218225Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }],
    };
    const action = {
      type: CLAIMSLIST.GET_CLAIMS_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      filter: {
        claimNumber: '',
        product: '',
        status: '',
        userName: '',
        matchAll: true,
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'c.ModifiedOn',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      listOfClaims: {
        ...initialState.listOfClaims,
        2: [{
          claimId: 'G00000179',
          claimStatus: 'Pending',
          contractorAccountId: '1000689',
          createdBy: null,
          createdOn: '0001-01-01T00:00:00',
          firstName: 'Ben',
          id: 'G00000179',
          impersonatedBy: null,
          impersonatedOn: null,
          invoiceDetails: null,
          invoices: null,
          lastName: 'Szweda',
          modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
          modifiedOn: '2018-12-13T16:26:18.4603748Z',
          pointsEarned: 0,
          submittedOn: '2018-12-14T08:00:09.3444349Z',
          userId: 'ben@gaf-uat1stchoiceroofing.com',
          value: 0,
        }, {
          claimId: 'G00000178',
          claimStatus: 'Pending',
          contractorAccountId: '1000689',
          createdBy: null,
          createdOn: '0001-01-01T00:00:00',
          firstName: 'Ben',
          id: 'G00000178',
          impersonatedBy: null,
          impersonatedOn: null,
          invoiceDetails: null,
          invoices: null,
          lastName: 'Szweda',
          modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
          modifiedOn: '2018-12-13T16:23:24.5218225Z',
          pointsEarned: 0,
          submittedOn: '2018-12-14T08:00:09.3444349Z',
          userId: 'ben@gaf-uat1stchoiceroofing.com',
          value: 0,
        }],
      },
      claimsListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_LIST_SUCCESS with no filter,paging inside data', () => {
    const data = {
      result: [{
        claimId: 'G00000179',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000179',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:26:18.4603748Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }, {
        claimId: 'G00000178',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000178',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:23:24.5218225Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }],
    };
    const action = {
      type: CLAIMSLIST.GET_CLAIMS_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      filter: {},
      paging: {
        token: '',
        totalRecords: 0,
      },
      listOfClaims: {
        ...initialState.listOfClaims,
        1: [{
          claimId: 'G00000179',
          claimStatus: 'Pending',
          contractorAccountId: '1000689',
          createdBy: null,
          createdOn: '0001-01-01T00:00:00',
          firstName: 'Ben',
          id: 'G00000179',
          impersonatedBy: null,
          impersonatedOn: null,
          invoiceDetails: null,
          invoices: null,
          lastName: 'Szweda',
          modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
          modifiedOn: '2018-12-13T16:26:18.4603748Z',
          pointsEarned: 0,
          submittedOn: '2018-12-14T08:00:09.3444349Z',
          userId: 'ben@gaf-uat1stchoiceroofing.com',
          value: 0,
        }, {
          claimId: 'G00000178',
          claimStatus: 'Pending',
          contractorAccountId: '1000689',
          createdBy: null,
          createdOn: '0001-01-01T00:00:00',
          firstName: 'Ben',
          id: 'G00000178',
          impersonatedBy: null,
          impersonatedOn: null,
          invoiceDetails: null,
          invoices: null,
          lastName: 'Szweda',
          modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
          modifiedOn: '2018-12-13T16:23:24.5218225Z',
          pointsEarned: 0,
          submittedOn: '2018-12-14T08:00:09.3444349Z',
          userId: 'ben@gaf-uat1stchoiceroofing.com',
          value: 0,
        }],
      },
      claimsListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_CLAIMS_LIST_FAILURE', () => {
    const action = { type: CLAIMSLIST.GET_CLAIMS_LIST_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      claimsListError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_LIST_FAILURE with no error value', () => {
    const action = { type: CLAIMSLIST.GET_CLAIMS_LIST_FAILURE, error: '' };
    const expected = {
      ...initialState,
      isLoading: false,
      claimsListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_LIST_CLEAR', () => {
    const action = { type: CLAIMSLIST.GET_CLAIMS_LIST_CLEAR };
    const expected = {
      ...initialState,
      isLoading: false,
      listOfClaims: {},
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_EXPORT_CSV_SUCCESS', () => {
    const data = {
      filter: {
        claimNumber: '',
        product: '',
        status: '',
        userName: '',
        matchAll: true,
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'c.ModifiedOn',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      result: [{
        claimId: 'G00000179',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000179',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:26:18.4603748Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }, {
        claimId: 'G00000178',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000178',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:23:24.5218225Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }],
    };
    const action = { type: CLAIMSLIST.GET_EXPORT_CSV_SUCCESS, data };
    const expected = {
      ...initialState,
      isLoading: false,
      listOfClaimsCSV: [{
        claimId: 'G00000179',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000179',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:26:18.4603748Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }, {
        claimId: 'G00000178',
        claimStatus: 'Pending',
        contractorAccountId: '1000689',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        firstName: 'Ben',
        id: 'G00000178',
        impersonatedBy: null,
        impersonatedOn: null,
        invoiceDetails: null,
        invoices: null,
        lastName: 'Szweda',
        modifiedBy: 'ben@gaf-uat1stchoiceroofing.com',
        modifiedOn: '2018-12-13T16:23:24.5218225Z',
        pointsEarned: 0,
        submittedOn: '2018-12-14T08:00:09.3444349Z',
        userId: 'ben@gaf-uat1stchoiceroofing.com',
        value: 0,
      }],
      claimsListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_EXPORT_CSV_SUCCESS with no data', () => {
    const action = { type: CLAIMSLIST.GET_EXPORT_CSV_SUCCESS };
    const expected = {
      ...initialState,
      isLoading: false,
      listOfClaimsCSV: [],
      claimsListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLAIM_LIST_CLEAR_ERROR', () => {
    const action = { type: CLAIMSLIST.CLAIM_LIST_CLEAR_ERROR };
    const expected = {
      ...initialState,
      claimsListError: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
