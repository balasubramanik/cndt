import * as TERMSANDCONDITIONS from '../actionTypes/TermsAndConditions';
import Reducer from './TermsAndConditions';

const initialState = {};


describe('TermsAndCondition Reducers', () => {
  it('should call TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS', () => {
    const action = { type: TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS };
    const expected = {
      ...initialState,
      isLoading: true,
      done: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });


  it('should call TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_SUCCESS', () => {
    const action = { type: TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_SUCCESS };
    const expected = {
      ...initialState,
      isLoading: false,
      done: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE', () => {
    const action = { type: TERMSANDCONDITIONS.SUBMIT_TERMSANDCONDITIONS_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

