import * as ORDERDETAILS from '../actionTypes/OrderDetails';
import { searchShowBy } from '../components/showBy/constants';
import { formatUSCurrency, roundToTwoDecimal, formatPoint, formatDateUTC } from '../utils/utils';

const initialState = {
  listOfOrder: [],
  pageNumber: 1,
  showPerPage: Number(searchShowBy.showBy[0].value),
  isAscending: '',
  sortField: '',
  searchText: '',
  pointsRedeemed: '',
  orderId: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ORDERDETAILS.GET_LIST_OF_PRODUCT: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS: {
      const { data = {} } = action;
      const {
        items = [],
        pointsRedeemed,
        modifiedOn,
        orderId,
      } = data || {};
      let formattedItems = [];
      if (items) {
        formattedItems = items.map((record) => {
          const serviceFee = (record.serviceFeeType === '$') ? formatUSCurrency(roundToTwoDecimal(record.serviceFeeRate)) : `${record.serviceFeeRate}%`;
          return {
            ...record,
            amountRedeemed: (record.amountRedeemed || record.amountRedeemed === 0) ? formatPoint(record.amountRedeemed) : 'N/A',
            serviceFeeRate: (record.serviceFeeRate === 0 || !record.serviceFeeRate || !record.serviceFeeType) ? 'N/A' : serviceFee,
            cardValue: (record.cardValue || record.cardValue === 0) ? `$${formatPoint(record.cardValue)}` : 'N/A',
            orderDate: (record.orderDate) ? formatDateUTC(record.orderDate) : 'N/A',
            modifiedOn: (modifiedOn) ? formatDateUTC(modifiedOn) : 'N/A',
          };
        });
      }
      const totalItems = items.length;
      return {
        ...state,
        isLoading: false,
        pageNumber: 1,
        listOfOrder: formattedItems,
        totalItems,
        modifiedOn,
        pointsRedeemed,
        orderId,
      };
    }
    case ORDERDETAILS.GET_LIST_OF_PRODUCT_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case ORDERDETAILS.UPDATE_PAGE_DETAILS: {
      const { details = {} } = action;
      return {
        ...state,
        ...details,
      };
    }
    default:
      return state;
  }
};
