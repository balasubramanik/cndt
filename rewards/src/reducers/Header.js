import * as HEADER from '../actionTypes/Header';

const initialState = {
  cartItemsCount: 0,
  cartItems: {
    orderTotal: 0,
    itemTotal: 0,
    feeTotal: 0,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case HEADER.GET_CART_COUNT:
      return {
        ...state,
        isLoading: true,
      };
    case HEADER.GET_CART_COUNT_SUCCESS: {
      const { cart } = action;
      const {
        orderTotal = 0, itemTotal = 0, feeTotal = 0, items,
      } = cart || {};
      const cartItemsCount = items ? items.length : 0;
      return {
        ...state,
        isLoading: false,
        cartItemsCount,
        cartItems: {
          ...cart,
          orderTotal,
          itemTotal,
          feeTotal,
        },
      };
    }
    case HEADER.GET_CART_COUNT_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
