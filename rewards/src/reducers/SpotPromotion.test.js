import * as SPOTPROMOTION from '../actionTypes/spotPromotion';
import Reducer from './SpotPromotion';

const initialState = {
  spotPromotion: {},
};

describe('SPOTPROMOTION Reducers', () => {
  it('should call GET_PROMOTION_LIST', () => {
    const action = { type: SPOTPROMOTION.GET_PROMOTION_LIST };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_PROMOTION_LIST_SUCCESS', () => {
    const data = {
      filter: {
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'status',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      result: [{
        promoTitle: 'Title 1',
        startDate: '2018-12-13T16:26:18.4603748Z',
        endDate: '2018-12-14T08:00:09.3444349Z',
        status: 'Draft',
      }, {
        promoTitle: 'Title 2',
        startDate: '2018-11-12T16:26:18.4603748Z',
        endDate: '2018-11-15T08:00:09.3444349Z',
        status: 'Active',
      }],
    };
    const action = {
      type: SPOTPROMOTION.GET_PROMOTION_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      filter: {
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'status',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      spotPromotion: {
        ...initialState.spotPromotion,
        2: [{
          promoTitle: 'Title 1',
          startDate: '2018-12-13T16:26:18.4603748Z',
          endDate: '2018-12-14T08:00:09.3444349Z',
          status: 'Draft',
        }, {
          promoTitle: 'Title 2',
          startDate: '2018-11-12T16:26:18.4603748Z',
          endDate: '2018-11-15T08:00:09.3444349Z',
          status: 'Active',
        }],
      },
      spotPromotionError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_PROMOTION_LIST_SUCCESS with no filter and paging', () => {
    const data = {
      result: [{
        promoTitle: 'Title 1',
        startDate: '2018-12-13T16:26:18.4603748Z',
        endDate: '2018-12-14T08:00:09.3444349Z',
        status: 'Draft',
      }, {
        promoTitle: 'Title 2',
        startDate: '2018-11-12T16:26:18.4603748Z',
        endDate: '2018-11-15T08:00:09.3444349Z',
        status: 'Active',
      }],
    };
    const action = {
      type: SPOTPROMOTION.GET_PROMOTION_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      filter: {},
      paging: {
        token: '',
        totalRecord: 0,
      },
      spotPromotion: {
        ...initialState.spotPromotion,
        1: [{
          promoTitle: 'Title 1',
          startDate: '2018-12-13T16:26:18.4603748Z',
          endDate: '2018-12-14T08:00:09.3444349Z',
          status: 'Draft',
        }, {
          promoTitle: 'Title 2',
          startDate: '2018-11-12T16:26:18.4603748Z',
          endDate: '2018-11-15T08:00:09.3444349Z',
          status: 'Active',
        }],
      },
      spotPromotionError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_PROMOTION_LIST_FAILURE', () => {
    const action = { type: SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      spotPromotionError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_PROMOTION_LIST_FAILURE with no error value', () => {
    const action = { type: SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE, error: '' };
    const expected = {
      ...initialState,
      isLoading: false,
      spotPromotionError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_PROMOTION_LIST_CLEAR', () => {
    const action = { type: SPOTPROMOTION.GET_PROMOTION_LIST_CLEAR };
    const expected = {
      ...initialState,
      isLoading: false,
      spotPromotion: {},
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

