import * as SPOTPROMOTIONDETAILS from '../actionTypes/SpotPromotionDetails';

const initialState = {
  promotionsInfo: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SPOTPROMOTIONDETAILS.GET_DETAILS:
      return {
        ...state,
        isLoading: true,
      };
    case SPOTPROMOTIONDETAILS.GET_DETAILS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        promotionsInfo: action.data,
      };
    case SPOTPROMOTIONDETAILS.GET_DETAILS_FAILURE:
      return {
        ...state,
        isLoading: false,
        promotionsError: action.error,
      };
    case SPOTPROMOTIONDETAILS.GET_DETAILS_CLEAR:
      return {
        ...state,
        isLoading: false,
        ...initialState,
      };
    default:
      return state;
  }
};

