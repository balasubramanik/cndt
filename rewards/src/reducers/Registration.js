import moment from 'moment';
import * as REGISTRATION from '../actionTypes/Registration';
import { formatDate } from '../utils/utils';

const countryName = 'United States';
const countryCode = 'USA';
const initialState = {
  values: {
    step: 1,
    hasHeadQuatersAddress: true,
    hasMailingAddress: true,
    canNotify: false,
    date: formatDate(moment()),
    countrybr: countryName,
    countryml: countryName,
    countryhq: countryName,
    countryCode,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REGISTRATION.CHANGE_ADDRESS: {
      const { name, value } = action;
      return {
        ...state,
        [`${name}LocationText`]: value,
        values: {
          ...state.values,
          [`address1${name}`]: value,
        },
      };
    }
    case REGISTRATION.SELECT_ADDRESS: {
      const { name, address } = action;
      return {
        ...state,
        [`${name}LocationText`]: address.address1,
        values: {
          ...state.values,
          [`address1${name}`]: address.address1,
          [`address2${name}`]: address.address2,
          [`city${name}`]: address.city,
          [`state${name}`]: address.state,
          [`zip${name}`]: address.zip,
        },
      };
    }
    case REGISTRATION.SELECT_SERVICES: {
      const { key, value } = action;
      return {
        ...state,
        values: {
          ...state.values,
          [key]: value,
        },
      };
    }
    case REGISTRATION.REGISTRATION_INFO:
      return {
        ...state,
        status: '',
        isLoading: true,
      };
    case REGISTRATION.REGISTRATION_INFO_SUCCESS: {
      const {
        status, contact, isCompany,
      } = action.info;
      const { firstName, lastName, emailAddress } = contact || {};
      return {
        ...state,
        status,
        isLoading: false,
        values: {
          ...state.values,
          firstName,
          lastName,
          email: emailAddress,
          isCompany,
        },
      };
    }
    case REGISTRATION.REGISTRATION_INFO_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case REGISTRATION.REGISTER_NOW:
      return {
        ...state,
        done: false,
        isLoading: true,
      };
    case REGISTRATION.REGISTER_NOW_SUCCESS:
      return {
        ...state,
        isLoading: false,
        done: true,
        registerNow: action.info,
      };
    case REGISTRATION.REGISTER_NOW_ERROR:
      return {
        ...state,
        isLoading: false,
        done: false,
        error: action.error,
      };
    case REGISTRATION.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    case REGISTRATION.UPDATE_REGISTRATION_OPTION: {
      const { fields } = action;
      return {
        ...state,
        values: {
          ...state.values,
          ...fields,
        },
      };
    }
    default:
      return state;
  }
};
