import * as CARTSUMMARY from '../actionTypes/CartSummary';
import { sortByDate, roundToTwoDecimal } from '../utils/utils';
import { cartSummaryConstants } from '../containers/cartSummary/Constants';

const initialState = {
  cartSummaryData: {},
  isLoading: true,
  isItemDelete: false,
  isDebitCardAvail: true,
  isCartSubmitAlready: false,
  done: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CARTSUMMARY.GET_CART_SUMMARY:
      return {
        ...state,
        isLoading: true,
        isGetCartSuccess: false,
      };

    case CARTSUMMARY.GET_CART_SUMMARY_SUCCESS: {
      const { data } = action;
      const { items = [] } = data || {};
      const cartItems = [...items];
      const sortedItem = sortByDate(cartItems, cartSummaryConstants.DESCENDING, cartSummaryConstants.CREATED_ON);
      const sortedData = sortedItem.map((item) => ({
        ...item,
        cardValue: item.category !== cartSummaryConstants.GIFT_CARD ? roundToTwoDecimal(item.cardValue) : item.cardValue,
      }));

      return {
        ...state,
        cartSummaryData: {
          ...action.data,
          items: sortedData,
        },
        isLoading: false,
        isGetCartSuccess: true,
      };
    }

    case CARTSUMMARY.GET_CART_SUMMARY_FAILURE:
      return {
        ...state,
        isLoading: false,
        getCartSummaryError: action.error,
        isGetCartSuccess: false,
      };

    case CARTSUMMARY.UPDATE_CART_SUMMARY:
      return {
        ...state,
      };

    case CARTSUMMARY.UPDATE_CART_SUMMARY_SUCCESS:
      return {
        ...state,
      };

    case CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE:
      return {
        ...state,
        updateCartSummaryError: action.error,
      };
    case CARTSUMMARY.DELETE_ITEM_CART_SUMMARY:
      return {
        ...state,
        isLoading: true,
        isItemDelete: false,
      };

    case CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_SUCCESS: {
      const { data } = action;
      const { items = [] } = data || {};
      const cartItems = [...items];
      const sortedItem = sortByDate(cartItems, cartSummaryConstants.DESCENDING, cartSummaryConstants.CREATED_ON);
      const sortedData = sortedItem.map((item) => ({
        ...item,
        cardValue: item.category !== cartSummaryConstants.GIFT_CARD ? roundToTwoDecimal(item.cardValue) : item.cardValue,
      }));

      return {
        ...state,
        cartSummaryData: {
          ...action.data,
          items: sortedData,
        },
        isLoading: false,
        isItemDelete: true,
        isDisableOrder: false,
      };
    }

    case CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_FAILURE:
      return {
        ...state,
        isLoading: false,
        isItemDelete: false,
        cartDeleteFailure: action.error,
      };
    case CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY:
      return {
        ...state,
        isLoading: true,
        isDebitCardAvail: true,
        isCartSubmitAlready: false,
        done: false,
      };
    case CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isDebitCardAvail: true,
        isCartSubmitAlready: false,
        done: action.data.success,
        orderNumber: action.data.data,
      };
    case CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE: {
      const { error = {} } = action;
      const { errors = [] } = error;
      let debitCardAvail = true;
      let submitCart = false;
      if (errors.length > 0) {
        errors.forEach((itm) => {
          if (itm.code === cartSummaryConstants.DEBIT_CARD_ALREADY_ISSUED_CODE) {
            debitCardAvail = false;
          } else if (itm.code === cartSummaryConstants.CART_ALREADY_SUBMITTED_CODE) {
            submitCart = true;
          }
        });
      }
      return {
        ...state,
        isLoading: false,
        isCartSubmitAlready: submitCart,
        isDebitCardAvail: debitCardAvail,
        submitCartSummaryError: debitCardAvail ? action.error : null,
      };
    }
    case CARTSUMMARY.CLEAR_ERROR:
      return {
        ...state,
        getCartSummaryError: null,
        cartDeleteFailure: null,
        submitCartSummaryError: null,
        updateCartSummaryError: null,
      };
    case CARTSUMMARY.CART_INITIAL_STATE:
      return {
        ...state,
        isItemDelete: false,
        isDebitCardAvail: true,
        isCartSubmitAlready: false,
      };
    case CARTSUMMARY.CART_RESET_DATA:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
};
