import * as CONTRACTORLIST from '../actionTypes/ContractorList';
import Reducer from './ContractorList';

const initialState = {
  contractorListInfo: [],
  pageNumber: 1,
  showPerPage: 25,
  isAscending: '',
  sortField: '',
  searchText: '',
  toggleIndex: '',
};

describe('CONTRACTORLIST Reducers for GET_CONTRACTOR_LIST', () => {
  it('should call GET_CONTRACTOR_LIST', () => {
    const action = { type: CONTRACTORLIST.GET_CONTRACTOR_LIST };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CONTRACTOR_LIST_SUCCESS', () => {
    const data = {
      result: [{
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: true,
      },
      {
        custID: '114457',
        contractorName: 'Kris Construction',
        address: {
          city: 'Lodi',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Non-Certified',
        planType: 'Standard',
        isAchEnabled: false,
      }],
    };
    const action = {
      type: CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      contractorListInfo: [
        {
          custID: '114457',
          contractorName: 'Kris Construction',
          address: {
            city: 'Lodi',
            stateOrProvince: 'NJ',
          },
          contractorType: 'Non-Certified',
          planType: 'Standard',
          isAchEnabled: false,
          city: 'Lodi',
          state: 'NJ',
        },
        {
          custID: '114456',
          contractorName: 'Ridgeline roofing',
          address: {
            city: 'Parsippany',
            stateOrProvince: 'NJ',
          },
          contractorType: 'Certified',
          planType: 'Custom',
          isAchEnabled: true,
          city: 'Parsippany',
          state: 'NJ',
        },
      ],
      totalItems: 2,

    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CONTRACTOR_LIST_SUCCESS with null data', () => {
    const data = null;
    const action = {
      type: CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      contractorListInfo: [],
      totalItems: 0,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CONTRACTOR_LIST_SUCCESS with no data object', () => {
    const action = {
      type: CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      totalItems: 0,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CONTRACTOR_LIST_FAILURE', () => {
    const action = { type: CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      contractorListError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

describe('CONTRACTORLIST Reducers for ACH_TOGGLE', () => {
  const secondState = {
    contractorListInfo: [{
      custID: '114456',
      contractorName: 'Ridgeline roofing',
      address: {
        city: 'Parsippany',
        stateOrProvince: 'NJ',
      },
      contractorType: 'Certified',
      planType: 'Custom',
      isAchEnabled: true,
    },
    {
      custID: '114457',
      contractorName: 'Kris Construction',
      address: {
        city: 'Lodi',
        stateOrProvince: 'NJ',
      },
      contractorType: 'Non-Certified',
      planType: 'Standard',
      isAchEnabled: false,
    }],
    pageNumber: 1,
    showPerPage: 25,
    isAscending: '',
    sortField: '',
    searchText: '',
    toggleIndex: '',
  };
  it('should call ACH_TOGGLE for index 1', () => {
    const action = { type: CONTRACTORLIST.ACH_TOGGLE, index: 1 };
    const expected = {
      ...secondState,
      contractorListInfo: [{
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: true,
      },
      {
        custID: '114457',
        contractorName: 'Kris Construction',
        address: {
          city: 'Lodi',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Non-Certified',
        planType: 'Standard',
        isAchEnabled: true,
      }],
      isToggled: true,
      toggleIndex: 1,
    };
    expect(Reducer(secondState, action)).toEqual(expected);
  });

  it('should call ACH_TOGGLE for index 0', () => {
    const action = { type: CONTRACTORLIST.ACH_TOGGLE, index: 0 };
    const expected = {
      ...secondState,
      contractorListInfo: [{
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: false,
      },
      {
        custID: '114457',
        contractorName: 'Kris Construction',
        address: {
          city: 'Lodi',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Non-Certified',
        planType: 'Standard',
        isAchEnabled: true,
      }],
      isToggled: true,
      toggleIndex: 0,
    };
    expect(Reducer(secondState, action)).toEqual(expected);
  });

  it('should call ACH_TOGGLE_SUCCESS', () => {
    const action = { type: CONTRACTORLIST.ACH_TOGGLE_SUCCESS, successFlag: true };
    const expected = {
      ...secondState,
      isToggled: false,
      successToggleFlag: true,
    };
    expect(Reducer(secondState, action)).toEqual(expected);
  });

  it('should call ACH_TOGGLE_FAILURE for isAchEnabled false', () => {
    const action = { type: CONTRACTORLIST.ACH_TOGGLE_FAILURE, index: 1 };
    const expected = {
      ...secondState,
      contractorListInfo: [{
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: false,
      },
      {
        custID: '114457',
        contractorName: 'Kris Construction',
        address: {
          city: 'Lodi',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Non-Certified',
        planType: 'Standard',
        isAchEnabled: false,
      }],
      toggleIndex: 1,
      isToggled: false,
    };
    expect(Reducer(secondState, action)).toEqual(expected);
  });
  it('should call ACH_TOGGLE_FAILURE for isAchEnabled true', () => {
    const action = { type: CONTRACTORLIST.ACH_TOGGLE_FAILURE, index: 0 };
    const expected = {
      ...secondState,
      contractorListInfo: [{
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: true,
      },
      {
        custID: '114457',
        contractorName: 'Kris Construction',
        address: {
          city: 'Lodi',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Non-Certified',
        planType: 'Standard',
        isAchEnabled: false,
      }],
      toggleIndex: 0,
      isToggled: false,
    };
    expect(Reducer(secondState, action)).toEqual(expected);
  });
});

describe('CONTRACTORLIST Reducers for SUBMIT_DETAILED_REPORT', () => {
  it('should call SUBMIT_DETAILED_REPORT', () => {
    const action = { type: CONTRACTORLIST.SUBMIT_DETAILED_REPORT };
    const expected = {
      ...initialState,
      isDetailedReportLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SUBMIT_DETAILED_REPORT_SUCCESS', () => {
    const data = [{
      custID: '114457',
      contractorName: 'Kris Construction',
      address: {
        city: 'Lodi',
        stateOrProvince: 'NJ',
      },
      contractorType: 'Non-Certified',
      planType: 'Standard',
      isAchEnabled: false,
      city: 'Lodi',
      state: 'NJ',
    },
    {
      custID: '114456',
      contractorName: 'Ridgeline roofing',
      address: {
        city: 'Parsippany',
        stateOrProvince: 'NJ',
      },
      contractorType: 'Certified',
      planType: 'Custom',
      isAchEnabled: true,
      city: 'Parsippany',
      state: 'NJ',
    }];
    const action = {
      type: CONTRACTORLIST.SUBMIT_DETAILED_REPORT_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isDetailedReportLoading: false,
      detailedReportData: [{
        custID: '114457',
        contractorName: 'Kris Construction',
        address: {
          city: 'Lodi',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Non-Certified',
        planType: 'Standard',
        isAchEnabled: false,
        city: 'Lodi',
        state: 'NJ',
      },
      {
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          stateOrProvince: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: true,
        city: 'Parsippany',
        state: 'NJ',
      }],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SUBMIT_DETAILED_REPORT_FAILURE', () => {
    const action = { type: CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isDetailedReportLoading: false,
      submitDetailError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});

describe('CONTRACTORLIST Reducers for Closing popups,updating state and resetting page details', () => {
  it('should call CLOSE_TOGGLE_POPUP', () => {
    const action = { type: CONTRACTORLIST.CLOSE_TOGGLE_POPUP };
    const expected = {
      ...initialState,
      isLoading: false,
      successToggleFlag: false,
      toggleIndex: 0,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLOSE_DETAILED_POPUP', () => {
    const action = {
      type: CONTRACTORLIST.CLOSE_DETAILED_POPUP,
    };
    const expected = {
      ...initialState,
      isSuccessSubmitReportPopup: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call UPDATE_PAGE_DETAILS', () => {
    const details = {
      pageNo: 2,
    };
    const action = { type: CONTRACTORLIST.UPDATE_PAGE_DETAILS, details };
    const expected = {
      ...initialState,
      pageNo: 2,
      contractorListInfo: [],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call UPDATE_PAGE_DETAILS with no details object', () => {
    const action = { type: CONTRACTORLIST.UPDATE_PAGE_DETAILS };
    const expected = {
      ...initialState,
      contractorListInfo: [],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call RESETINITIALSTATE', () => {
    const action = { type: CONTRACTORLIST.RESETINITIALSTATE };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});

