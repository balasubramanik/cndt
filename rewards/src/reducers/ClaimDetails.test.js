import * as CLAIMDETAILS from '../actionTypes/ClaimDetails';
import Reducer from './ClaimDetails';
import { searchShowBy } from '../components/showBy/constants';

const initialState = {
  listOfInvoice: [],
  pageNumber: 1,
  showPerPage: Number(searchShowBy.showBy[0].value),
  isAscending: '',
  sortField: '',
  searchText: '',
};

describe('CLAIMDETAILS Reducers', () => {
  it('should call GET_INVOICE_LIST', () => {
    const action = { type: CLAIMDETAILS.GET_INVOICE_LIST };
    const expected = {
      ...initialState,
      isLoading: true,
      searchText: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_INVOICE_LIST_SUCCESS', () => {
    const data = {
      claimId: '123',
      submittedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsEarned: '1300',
    };
    const action = {
      type: CLAIMDETAILS.GET_INVOICE_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfInvoice: [],
      totalItems: 0,
      claimId: '123',
      submittedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsEarned: '1300',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_INVOICE_LIST_SUCCESS for no data', () => {
    const action = {
      type: CLAIMDETAILS.GET_INVOICE_LIST_SUCCESS,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfInvoice: [],
      totalItems: 0,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_INVOICE_LIST_FAILURE', () => {
    const action = { type: CLAIMDETAILS.GET_INVOICE_LIST_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_INVOICE_LIST_CLEAR', () => {
    const action = { type: CLAIMDETAILS.GET_INVOICE_LIST_CLEAR, error: '' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: '',
      listOfInvoice: [],
      totalItems: 0,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call UPDATE_PAGE_DETAILS', () => {
    const action = {
      type: CLAIMDETAILS.UPDATE_PAGE_DETAILS,
    };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
