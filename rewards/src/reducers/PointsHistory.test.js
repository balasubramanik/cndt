import * as POINTSHISTORY from '../actionTypes/PointsHistory';
import Reducer from './PointsHistory';

const initialState = {};

describe('POINTSHISTORY Reducers', () => {
  it('should call GET_LIST_OF_TRANSACTIONS', () => {
    const action = { type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_TRANSACTIONS_SUCCESS', () => {
    const data = {
      filter: {
        claimNumber: '',
        product: '',
        status: '',
        userName: '',
        matchAll: true,
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'claimId',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      result: [{
        claimId: 'G00000179',
        type: 'Earned',
        pointsEarned: +250,
        firstName: 'Ben',
        submittedOn: '2018-12-14T08:00:09.3444349Z',
      }, {
        claimId: 'O00002345',
        type: 'Redeemed',
        pointsEarned: -115,
        firstName: 'Granger',
        submittedOn: '2018-12-15T08:00:09.3444349Z',
      }],
    };
    const action = {
      type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      filter: {
        claimNumber: '',
        product: '',
        status: '',
        userName: '',
        matchAll: true,
        toDate: null,
        viewBy: 30,
      },
      paging: {
        pageNo: 2,
        pageSize: 10,
        sortBy: 'claimId',
        sortOrder: 'desc',
        token: '',
        totalPages: 2,
        totalRecords: 20,
      },
      listOfTransactions: {
        ...initialState.listOfTransactions,
        2: [{
          claimId: 'G00000179',
          type: 'Earned',
          pointsEarned: +250,
          firstName: 'Ben',
          submittedOn: '2018-12-14T08:00:09.3444349Z',
        },
        {
          claimId: 'O00002345',
          type: 'Redeemed',
          pointsEarned: -115,
          firstName: 'Granger',
          submittedOn: '2018-12-15T08:00:09.3444349Z',
        }],
      },
      transactionListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_TRANSACTIONS_SUCCESS with no filter,paging inside data', () => {
    const data = {
      result: [{
        claimId: 'G00000179',
        type: 'Earned',
        pointsEarned: +250,
        firstName: 'Ben',
        submittedOn: '2018-12-14T08:00:09.3444349Z',
      }, {
        claimId: 'O00002345',
        type: 'Redeemed',
        pointsEarned: -115,
        firstName: 'Granger',
        submittedOn: '2018-12-15T08:00:09.3444349Z',
      }],
    };
    const action = {
      type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      filter: {},
      paging: {
        token: '',
        totalRecords: 0,
      },
      listOfTransactions: {
        ...initialState.listOfTransactions,
        1: [{
          claimId: 'G00000179',
          type: 'Earned',
          pointsEarned: +250,
          firstName: 'Ben',
          submittedOn: '2018-12-14T08:00:09.3444349Z',
        },
        {
          claimId: 'O00002345',
          type: 'Redeemed',
          pointsEarned: -115,
          firstName: 'Granger',
          submittedOn: '2018-12-15T08:00:09.3444349Z',
        }],
      },
      transactionListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_LIST_OF_TRANSACTIONS_FAILURE', () => {
    const action = { type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      transactionListError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_TRANSACTIONS_FAILURE with no error value', () => {
    const action = { type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE, error: '' };
    const expected = {
      ...initialState,
      isLoading: false,
      transactionListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_TRANSACTIONS_CLEAR', () => {
    const action = { type: POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_CLEAR };
    const expected = {
      ...initialState,
      isLoading: false,
      listOfTransactions: {},
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_EXPORT_CSV_SUCCESS', () => {
    const data = {
      result: [{
        claimId: 'G00000179',
        type: 'Earned',
        pointsEarned: +250,
        firstName: 'Ben',
        submittedOn: '2018-12-14T08:00:09.3444349Z',
      }, {
        claimId: 'O00002345',
        type: 'Redeemed',
        pointsEarned: -115,
        firstName: 'Granger',
        submittedOn: '2018-12-15T08:00:09.3444349Z',
      }],
    };
    const action = {
      type: POINTSHISTORY.GET_EXPORT_CSV_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      listOfTransactionCSV: [{
        claimId: 'G00000179',
        type: 'Earned',
        pointsEarned: +250,
        firstName: 'Ben',
        submittedOn: '2018-12-14T08:00:09.3444349Z',
      },
      {
        claimId: 'O00002345',
        type: 'Redeemed',
        pointsEarned: -115,
        firstName: 'Granger',
        submittedOn: '2018-12-15T08:00:09.3444349Z',
      }],
      transactionListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_EXPORT_CSV_SUCCESS with no data', () => {
    const action = { type: POINTSHISTORY.GET_EXPORT_CSV_SUCCESS };
    const expected = {
      ...initialState,
      isLoading: false,
      listOfTransactionCSV: [],
      transactionListError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call TRANSACTIONS_LIST_CLEAR_ERROR', () => {
    const action = { type: POINTSHISTORY.TRANSACTIONS_LIST_CLEAR_ERROR };
    const expected = {
      ...initialState,
      transactionListError: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
