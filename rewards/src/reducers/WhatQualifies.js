import * as WHATQUALIFIES from '../actionTypes/WhatQualifies';

const initialState = {
  myPlanCategory: [],
  spotPromotionsCategory: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WHATQUALIFIES.GET_MYPLAN_CATEGORY:
    case WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY:
      return {
        ...state,
        isLoading: true,
      };
    case WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        myPlanCategory: action.data,
      };
    case WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE:
    case WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS:
      return {
        ...state,
        isLoading: false,
        spotPromotionsCategory: action.data,
      };
    case WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_PLAN_ID: {
      const { id } = action;
      return {
        ...state,
        id,
      };
    }
    case WHATQUALIFIES.MY_PLAN_ID: {
      const { myPlanId } = action;
      return {
        ...state,
        myPlanId,
      };
    }
    case WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_SELECTED_YEAR: {
      const { selectedYear } = action;
      return {
        ...state,
        selectedYear,
      };
    }
    case WHATQUALIFIES.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};
