import * as PROMOTION from '../actionTypes/SpotPromotionManagement';
import { formatDateUTC } from '../utils/utils';

const initialState = {
  values: {
    imageData: {},
  },
  isShowPopup: false,
  saveDone: false,
  isApproveShowPopup: false,
  fetchCount: 0,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case PROMOTION.DISCARD_ALL_DATA: {
      return {
        ...initialState,
      };
    }
    case PROMOTION.END_DATE_RESET: {
      return {
        ...state,
        values: {
          ...state.values,
          endDate: '',
        },
      };
    }
    case PROMOTION.DISPLAY_START_DATE_RESET: {
      return {
        ...state,
        values: {
          ...state.values,
          displayStartDate: '',
        },
      };
    }
    case PROMOTION.DISPLAY_END_DATE_CHANGE_RESET: {
      return {
        ...state,
        values: {
          ...state.values,
          displayEndDate: '',
        },
      };
    }
    case PROMOTION.SAVE_SPOT_PROMOTION: {
      return {
        ...state,
        isLoading: true,
        saveDone: false,
      };
    }
    case PROMOTION.SAVE_SPOTPROMOTION_SUCCESS: {
      const { submitType, data } = action;
      const { code, id } = data;
      const submitTypeText = submitType.toLowerCase();
      return {
        ...state,
        isShowPopup: submitTypeText === 'save',
        isApproveShowPopup: submitTypeText === 'approve',
        saveDone: submitTypeText === 'save',
        isInactivePopup: submitTypeText === 'inactive',
        id,
        isLoading: false,
        responseCode: code,
      };
    }
    case PROMOTION.SAVE_SPOTPROMOTION_FAILURE: {
      const { error } = action;
      return {
        ...state,
        error,
        isLoading: false,
      };
    }
    case PROMOTION.SAVE_SPOTPROMOTION_POPUP_CLOSE: {
      return {
        ...state,
        isShowPopup: false,
        isApproveShowPopup: false,
        isInactivePopup: false,
        error: null,
      };
    }
    case PROMOTION.GET_SPOTPROMOTION_OPTIONS: {
      const { optionsTypes } = action;
      const { fetchCount } = state;
      return {
        ...state,
        optionsTypes,
        isLoading: true,
        fetchCount: fetchCount + 1,
      };
    }
    case PROMOTION.GET_SPOTPROMOTION_OPTIONS_SUCCESS: {
      const { info, key } = action;
      const { fetchCount } = state;
      const count = fetchCount - 1;
      return {
        ...state,
        [key]: info,
        isLoading: !(count === 0),
        fetchCount: count,
      };
    }
    case PROMOTION.GET_SPOTPROMOTION_OPTIONS_FAILURE: {
      const { error } = action;
      const { fetchCount } = state;
      const count = fetchCount - 1;
      return {
        ...state,
        error,
        isLoading: !(count === 0),
        fetchCount: count,
      };
    }
    case PROMOTION.GET_SPOT_PROMOTION: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case PROMOTION.GET_SPOT_PROMOTION_SUCCESS: {
      const { data } = action;
      const {
        title, description, disclaimer, startDate, endDate, categories, displayStartDate, displayEndDate, states, status, id, image,
      } = data;
      const startDateFormat = formatDateUTC(startDate);
      const endDateFormat = formatDateUTC(endDate);
      const displayStartDateFormat = formatDateUTC(displayStartDate);
      const displayEndDateFormat = formatDateUTC(displayEndDate);
      const productsData = [];
      const promotionStatus = status.toUpperCase();
      categories.forEach((item) => {
        const productFamilyItem = item.family.map((i) => i);
        const selectedUom = (item.uom.code === 'N/A') ? '' : item.uom.code;
        productsData.push({
          category: item.category,
          selectedProductFamily: productFamilyItem,
          allowanceType: item.allowanceType,
          uom: selectedUom,
          productValue: item.valueByDollars || item.valueByPercent,
        });
      });
      return {
        ...state,
        values: {
          ...state.values,
          title,
          description,
          disclaimer,
          selectedStates: states,
          startDate: startDateFormat,
          endDate: endDateFormat,
          displayStartDate: displayStartDateFormat,
          displayEndDate: displayEndDateFormat,
          products: productsData,
          imageData: image,
        },
        promotionStatus,
        id,
        isLoading: false,
      };
    }
    case PROMOTION.GET_SPOT_PROMOTION_FAILURE: {
      const { error } = action;
      return {
        ...state,
        error,
        isLoading: false,
      };
    }
    case PROMOTION.PRODUCT_FAMILY_RESET: {
      const { data, index } = action;
      return {
        ...state,
        values: {
          ...state.values,
          products: [
            ...state.values.products.slice(0, index),
            {
              ...state.values.products[index],
              ...data,
            },
            ...state.values.products.slice(index + 1),
          ],
        },
      };
    }
    default:
      return state;
  }
};
