import * as STATES from '../actionTypes/States';
import Reducer from './States';
const initialState = {};
describe('States container Reducers', () => {
  it('should call CHANGE_ADDRESS', () => {
    const action = {
      type: STATES.GET_STATES,
    };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_STATES_SUCCESS', () => {
    const action = {
      type: STATES.GET_STATES_SUCCESS,
      data: [{ name: 'New Jersey', code: 'NJ' }],
    };
    const expected = {
      ...initialState,
      isLoading: false,
      states: [{ name: 'New Jersey', code: 'NJ' }],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_STATES_FAILURE', () => {
    const action = {
      type: STATES.GET_STATES_FAILURE,
      error: 'Internal server error',
    };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
