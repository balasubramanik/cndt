import * as CLAIMINVOICELIST from '../actionTypes/ClaimInvoiceDetails';
import Reducer from './ClaimInvoiceDetails';

const initialState = {
  isInvoiceListLoading: false,
  claimInvoiceData: [],
  viewInvoiceError: '',
  isInvoiceDownloading: false,
  invoiceFileDetails: [],
  downloadInvoiceError: '',
  isInvoiceDownloadSuccess: false,
};

describe('ClaimInvoiceDetails Reducers for get claims invoice list', () => {
  it('should call GET_CLAIMS_INVOICE_LIST', () => {
    const action = { type: CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST };
    const expected = {
      ...initialState,
      isInvoiceListLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_INVOICE_LIST_SUCCESS', () => {
    const data = [{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Cobra® Snow Country',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-32',
      submissionValue: '-2816',
      pointsEarned: '-28.16',
      rate: '3',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    }];
    const action = {
      type: CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isInvoiceListLoading: false,
      claimInvoiceData: [{
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Cobra® Snow Country',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: '-32',
        submissionValue: '-2816',
        pointsEarned: '-28.16',
        rate: '3',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      }],
      viewInvoiceError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_INVOICE_LIST_SUCCESS with no data', () => {
    const action = {
      type: CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_SUCCESS,
    };
    const expected = {
      ...initialState,
      isInvoiceListLoading: false,
      viewInvoiceError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_CLAIMS_INVOICE_LIST_FAILURE', () => {
    const action = { type: CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isInvoiceListLoading: false,
      viewInvoiceError: 'Internal Server Error',
      claimInvoiceData: [],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CLAIMS_INVOICE_LIST_FAILURE with no error value', () => {
    const action = { type: CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_FAILURE, error: '' };
    const expected = {
      ...initialState,
      isInvoiceListLoading: false,
      viewInvoiceError: '',
      claimInvoiceData: [],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_CLAIMS_INVOICE_LIST_CLEAR', () => {
    const action = { type: CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_CLEAR, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isInvoiceListLoading: false,
      claimInvoiceData: [],
      viewInvoiceError: '',
      downloadInvoiceError: '',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

describe('ClaimInvoiceDetails Reducers for download file', () => {
  it('should call DOWNLOAD_FILE', () => {
    const action = { type: CLAIMINVOICELIST.DOWNLOAD_FILE };
    const expected = {
      ...initialState,
      isInvoiceDownloading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call DOWNLOAD_FILE_SUCCESS', () => {
    const data = {
      fileId: 'a160e85d-20ce-4aa7-93ad-f674aaf15991',
      fileName: 'Air_Confirmation.pdf',
      submittedOn: '2018-12-14T08:00:09.3444349Z',
    };
    const action = {
      type: CLAIMINVOICELIST.DOWNLOAD_FILE_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isInvoiceDownloading: false,
      invoiceFileDetails: action,
      downloadInvoiceError: '',
      isInvoiceDownloadSuccess: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call DOWNLOAD_FILE_FAILURE', () => {
    const action = { type: CLAIMINVOICELIST.DOWNLOAD_FILE_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isInvoiceDownloading: false,
      downloadInvoiceError: 'Internal Server Error',
      invoiceFileDetails: [],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call DOWNLOAD_FILE_FAILURE with no error value', () => {
    const action = { type: CLAIMINVOICELIST.DOWNLOAD_FILE_FAILURE, error: '' };
    const expected = {
      ...initialState,
      isInvoiceDownloading: false,
      downloadInvoiceError: '',
      invoiceFileDetails: [],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call DOWNLOAD_FILE_CLEAR', () => {
    const action = { type: CLAIMINVOICELIST.DOWNLOAD_FILE_CLEAR };
    const expected = {
      ...initialState,
      isInvoiceDownloadSuccess: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Claims Invoice Clear error', () => {
    const action = { type: CLAIMINVOICELIST.CLAIMS_INVOICE_CLEAR_ERROR, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      viewInvoiceError: null,
      downloadInvoiceError: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});
