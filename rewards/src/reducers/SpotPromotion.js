import * as SPOTPROMOTION from '../actionTypes/spotPromotion';

const initialState = {
  spotPromotion: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SPOTPROMOTION.GET_PROMOTION_LIST:
      return {
        ...state,
        isLoading: true,
      };
    case SPOTPROMOTION.GET_PROMOTION_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        filter: (action.data && action.data.filter) ? action.data.filter : {},
        paging: (action.data && action.data.paging) ? action.data.paging : { token: '', totalRecord: 0 },
        spotPromotion: {
          ...state.spotPromotion,
          [(action.data && action.data.paging) ? action.data.paging.pageNo : 1]: action.data.result,
        },
        spotPromotionError: '',
      };
    case SPOTPROMOTION.GET_PROMOTION_LIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        spotPromotionError: (action.error) ? action.error : '',
      };
    case SPOTPROMOTION.GET_PROMOTION_LIST_CLEAR:
      return {
        ...state,
        isLoading: false,
        spotPromotion: {},
      };
    default:
      return state;
  }
};
