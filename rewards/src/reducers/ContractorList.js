import * as CONTRACTORLIST from '../actionTypes/ContractorList';
import { searchShowBy } from '../components/showBy/constants';
import { ascending } from '../utils/utils';
import { getSortedData } from '../containers/contractorManagement/commonMethod';

const initialState = {
  contractorListInfo: [],
  pageNumber: 1,
  showPerPage: Number(searchShowBy.showBy[0].value),
  isAscending: '',
  sortField: '',
  searchText: '',
  toggleIndex: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CONTRACTORLIST.GET_CONTRACTOR_LIST:
    case CONTRACTORLIST.GET_LIST_OF_STATE:
      return {
        ...state,
        isLoading: true,
      };
    case CONTRACTORLIST.GET_CONTRACTOR_LIST_SUCCESS: {
      const { data = {} } = action;
      const { result = [] } = data || {};
      const ascenOrderData = result.sort(ascending('contractorName'));
      const totalItems = ascenOrderData.length;
      const finalData = getSortedData(ascenOrderData, state);
      return {
        ...state,
        isLoading: false,
        pageNumber: 1,
        contractorListInfo: finalData,
        totalItems,
      };
    }
    case CONTRACTORLIST.GET_CONTRACTOR_LIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        contractorListError: action.error,
      };
    case CONTRACTORLIST.ACH_TOGGLE: {
      const dataInfo = [...state.contractorListInfo];
      const rowsCount = (state.pageNumber - 1) * state.showPerPage;
      const originIndex = rowsCount + action.index;
      const toggledAch = (dataInfo[originIndex].isAchEnabled === true) ? {
        isAchEnabled: false,
      } : {
        isAchEnabled: true,
      };
      const formattedToggledRow = Object.assign(dataInfo[originIndex], toggledAch);
      dataInfo.splice(originIndex, 1, formattedToggledRow);
      return {
        ...state,
        contractorListInfo: dataInfo,
        toggleIndex: originIndex,
        isToggled: true,
      };
    }
    case CONTRACTORLIST.ACH_TOGGLE_SUCCESS: {
      return {
        ...state,
        isToggled: false,
        successToggleFlag: action.successFlag,
      };
    }
    case CONTRACTORLIST.ACH_TOGGLE_FAILURE: {
      const dataInfo = [...state.contractorListInfo];
      const rowsCount = (state.pageNumber - 1) * state.showPerPage;
      const originIndex = rowsCount + action.index;
      const toggledAch = (dataInfo[originIndex].isAchEnabled === true) ? {
        isAchEnabled: false,
      } : {
        isAchEnabled: true,
      };
      const formattedToggledRow = Object.assign(dataInfo[originIndex], toggledAch);
      dataInfo.splice(originIndex, 1, formattedToggledRow);
      return {
        ...state,
        contractorListInfo: dataInfo,
        toggleIndex: originIndex,
        isToggled: false,
      };
    }
    case CONTRACTORLIST.GET_LIST_OF_STATE_SUCCESS:
      return {
        ...state,
        isLoading: true,
        ListOfstates: action.data,
      };
    case CONTRACTORLIST.GET_LIST_OF_STATE_FAILURE:
      return {
        ...state,
        isLoading: false,
        statesError: action.error,
      };
    case CONTRACTORLIST.CLOSE_TOGGLE_POPUP:
      return {
        ...state,
        isLoading: false,
        successToggleFlag: false,
        toggleIndex: 0,
      };
    case CONTRACTORLIST.SUBMIT_DETAILED_REPORT:
      return {
        ...state,
        isDetailedReportLoading: true,
      };
    case CONTRACTORLIST.SUBMIT_DETAILED_REPORT_SUCCESS:
      return {
        ...state,
        isDetailedReportLoading: false,
        detailedReportData: action.data,
      };
    case CONTRACTORLIST.SUBMIT_DETAILED_REPORT_FAILURE:
      return {
        ...state,
        isDetailedReportLoading: false,
        submitDetailError: action.error,
      };
    case CONTRACTORLIST.CLOSE_DETAILED_POPUP:
      return {
        ...state,
        isSuccessSubmitReportPopup: false,
      };
    case CONTRACTORLIST.UPDATE_PAGE_DETAILS: {
      const { details = {} } = action;
      const finalData = getSortedData(state.contractorListInfo, details);
      return {
        ...state,
        ...details,
        contractorListInfo: finalData,
      };
    }
    case CONTRACTORLIST.RESETINITIALSTATE:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
};
