import * as GTM from '../actionTypes/GTM';

const initialState = {
  dataLayer: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GTM.ADD_TO_DATALAYER:
      return {
        ...state,
        dataLayer: {
          ...state.dataLayer,
          ...action.data,
        },
      };
    case GTM.RESET_DATALAYER:
      return {
        ...state,
        dataLayer: null,
      };
    default:
      return state;
  }
};
