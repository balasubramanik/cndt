import * as MYPROFILE from '../actionTypes/MyProfile';
import Reducer from './MyProfile';

const initialState = {
  accessInfo: false,
  userInfo: {},
  rewardsInfo: {},
  adminUserInfo: {},
};

describe('MYPROFILE Reducers for get user info', () => {
  it('should call GET_PROFILE', () => {
    const action = { type: MYPROFILE.GET_PROFILE };
    const expected = {
      ...initialState,
      isProfileLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_PROFILE_SUCCESS', () => {
    const info = {
      response: { responseCode: 200 },
      isAchEnabled: true,
      firstName: 'Brain',
      lastName: 'Granger',
      contractorAccountInformation: {
        contractorName: 'Queens Roofing and Tinsmithing Inc',
        contractorImage: 'tinsmiths.jpg',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
      },
      rewardsInformation: {
        rewardsPoints: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    };
    const action = {
      type: MYPROFILE.GET_PROFILE_SUCCESS,
      info,
    };
    const expected = {
      ...initialState,
      isProfileLoading: false,
      userInfo: {
        isAchEnabled: true,
        firstName: 'Brain',
        lastName: 'Granger',
        ...info.contractorAccountInformation,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_PROFILE_FAILURE', () => {
    const action = { type: MYPROFILE.GET_PROFILE_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isProfileLoading: false,
      profileError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

describe('MYPROFILE Reducers for get admin profile', () => {
  it('should call GET_ADMIN_PROFILE', () => {
    const action = { type: MYPROFILE.GET_ADMIN_PROFILE };
    const expected = {
      ...initialState,
      isAdminProfileLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_ADMIN_PROFILE_SUCCESS', () => {
    const info = {
      response: { responseCode: 200 },
      firstName: 'Brain',
      lastName: 'Granger',
      emailAddress: 'briangranger@easyroof.com',
    };
    const action = {
      type: MYPROFILE.GET_ADMIN_PROFILE_SUCCESS,
      info,
    };
    const expected = {
      ...initialState,
      isAdminProfileLoading: false,
      adminUserInfo: {
        firstName: 'Brain',
        lastName: 'Granger',
        emailAddress: 'briangranger@easyroof.com',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_ADMIN_PROFILE_FAILURE', () => {
    const action = { type: MYPROFILE.GET_ADMIN_PROFILE_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isAdminProfileLoading: false,
      adminProfileError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
describe('MYPROFILE Reducers for get rewards info', () => {
  it('should call GET_PROFILE', () => {
    const action = { type: MYPROFILE.GET_REWARDS };
    const expected = {
      ...initialState,
      isRewardsLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_REWARDS_SUCCESS', () => {
    const info = {
      RewardsPoint: '2500',
      ExpiryDate: '2018-08-09T16:57:19.0487212+05:30',
    };
    const action = {
      type: MYPROFILE.GET_REWARDS_SUCCESS,
      info,
    };
    const expected = {
      ...initialState,
      isRewardsLoading: false,
      rewardsInfo: info,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_REWARDS_FAILURE', () => {
    const action = { type: MYPROFILE.GET_REWARDS_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isRewardsLoading: false,
      rewardsError: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call ACCESS_PAGE', () => {
    const info = true;
    const action = { type: MYPROFILE.ACCESS_PAGE, info };
    const expected = {
      ...initialState,
      accessInfo: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
