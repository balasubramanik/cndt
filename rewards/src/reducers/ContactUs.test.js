import * as CONTACTUS from '../actionTypes/ContactUs';
import Reducer from './ContactUs';

const initialState = {
};

describe('Contactus Reducers', () => {
  it('should call submit conatactus SUBMIT_CONTACTUS', () => {
    const action = { type: CONTACTUS.SUBMIT_CONTACTUS };
    const expected = {
      ...initialState,
      isLoading: true,
      successContactUS: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call SUBMIT_CONTACTUS_SUCCESS', () => {
    const action = {
      type: CONTACTUS.SUBMIT_CONTACTUS_SUCCESS,
      data: {
        success: true,
        statusCode: 200,
        data: true,
        errors: null,
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      successContactUS: {
        success: true,
        statusCode: 200,
        data: true,
        errors: null,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call SUBMIT_CONTACTUS_FAILURE', () => {
    const action = { type: CONTACTUS.SUBMIT_CONTACTUS_FAILURE, error: 'internal server error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call RESET_CONTACTUS', () => {
    const action = { type: CONTACTUS.RESET_CONTACTUS };
    const expected = {
      successContactUS: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call CLEAR_ERRORS', () => {
    const action = { type: CONTACTUS.CLEAR_ERRORS, error: 'internal server error' };
    const expected = {
      error: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
