import * as PROMOTION from '../actionTypes/SpotPromotionManagement';
import Reducer from './SpotPromotionManagement';

const initialState = {
  values: {
    imageData: {},
  },
  isShowPopup: false,
  saveDone: false,
  isApproveShowPopup: false,
  fetchCount: 0,
};

describe('PROMOTION Reducers for discard and reset actions', () => {
  it('should call DISCARD_ALL_DATA', () => {
    const action = { type: PROMOTION.DISCARD_ALL_DATA };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call END_DATE_RESET', () => {
    const action = {
      type: PROMOTION.END_DATE_RESET,
    };
    const expected = {
      ...initialState,
      values: {
        imageData: {},
        endDate: '',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call DISPLAY_START_DATE_RESET', () => {
    const action = { type: PROMOTION.DISPLAY_START_DATE_RESET };
    const expected = {
      ...initialState,
      values: {
        imageData: {},
        displayStartDate: '',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call DISPLAY_END_DATE_CHANGE_RESET', () => {
    const action = { type: PROMOTION.DISPLAY_END_DATE_CHANGE_RESET };
    const expected = {
      ...initialState,
      values: {
        imageData: {},
        displayEndDate: '',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

describe('PROMOTION Reducers for SAVE_SPOT_PROMOTION', () => {
  it('should call SAVE_SPOT_PROMOTION', () => {
    const action = { type: PROMOTION.SAVE_SPOT_PROMOTION };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call SAVE_SPOTPROMOTION_SUCCESS', () => {
    const data = {
      planId: '123',
      code: 200,
    };
    const action = { type: PROMOTION.SAVE_SPOTPROMOTION_SUCCESS, data, submitType: 'Approve' };
    const expected = {
      ...initialState,
      isShowPopup: false,
      isApproveShowPopup: true,
      saveDone: false,
      isInactivePopup: false,
      planId: '123',
      isLoading: false,
      responseCode: 200,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call SAVE_SPOTPROMOTION_FAILURE', () => {
    const action = {
      type: PROMOTION.SAVE_SPOTPROMOTION_FAILURE, error: 'Internal Server Error',
    };
    const expected = {
      ...initialState,
      error: 'Internal Server Error',
      isLoading: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call SAVE_SPOTPROMOTION_POPUP_CLOSE', () => {
    const action = { type: PROMOTION.SAVE_SPOTPROMOTION_POPUP_CLOSE };
    const expected = {
      ...initialState,
      isShowPopup: false,
      isApproveShowPopup: false,
      isInactivePopup: false,
      error: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});

describe('PROMOTION Reducers for GET_SPOTPROMOTION_OPTIONS', () => {
  it('should call GET_SPOTPROMOTION_OPTIONS', () => {
    const action = { type: PROMOTION.GET_SPOTPROMOTION_OPTIONS, optionsTypes: 'category' };
    const expected = {
      ...initialState,
      optionsTypes: 'category',
      isLoading: true,
      fetchCount: 1,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_SPOTPROMOTION_OPTIONS_SUCCESS', () => {
    const action = { type: PROMOTION.GET_SPOTPROMOTION_OPTIONS_SUCCESS, info: 'SQ', key: 'uom' };
    const expected = {
      ...initialState,
      isLoading: true,
      uom: 'SQ',
      fetchCount: -1,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_SPOTPROMOTION_OPTIONS_FAILURE', () => {
    const action = {
      type: PROMOTION.GET_SPOTPROMOTION_OPTIONS_FAILURE, error: 'Internal Server Error',
    };
    const expected = {
      ...initialState,
      error: 'Internal Server Error',
      isLoading: true,
      fetchCount: -1,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});

describe('PROMOTION Reducers for GET_SPOT_PROMOTION', () => {
  it('should call GET_SPOT_PROMOTION', () => {
    const action = { type: PROMOTION.GET_SPOT_PROMOTION };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_SPOT_PROMOTION_SUCCESS', () => {
    const data = {
      title: 'One Day Rebate',
      description: 'One Day Only Timberline Offer! ',
      disclaimer: 'Lorem ipsum',
      startDate: '2018-12-19T21:12:44.0122176+05:30',
      endDate: '2018-12-19T21:12:44.0122176+05:30',
      categories: [{
        allowanceType: 'PCT',
        category: 'Timberline® Lifetime Shingles',
        valueByPercent: 2,
        uom: { code: 'N/A' },
        family: ['Timberline HD®'],
        productFamily: {
          isSelectAll: false,
          itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
        },
      },
      {
        allowanceType: 'DOL',
        category: 'Timberline® Busters',
        valueByDollars: 4,
        uom: { code: 'SQ', description: 'Square' },
        family: ['Timberline XXX'],
        productFamily: {
          isSelectAll: false,
          itemSelected: [['Timberline® SED', { title: 'Timberline® SED', value: 'Timberline® SED' }], ['Timberline FES', { title: 'Timberline FES', value: 'Timberline FES', checked: true }]],
        },
      }],
      displayStartDate: '2018-12-19T21:12:44.0122176+05:30',
      displayEndDate: '2018-12-20T21:12:44.0122176+05:30',
      states: {
        isSelectAll: true,
        itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
      },
      status: 'Approved',
      planId: 'SP01',
      image: 'GAF_Logo_jpg.jpg',
    };
    const action = { type: PROMOTION.GET_SPOT_PROMOTION_SUCCESS, data };
    const expected = {
      ...initialState,
      values: {
        title: 'One Day Rebate',
        description: 'One Day Only Timberline Offer! ',
        disclaimer: 'Lorem ipsum',
        selectedStates: {
          isSelectAll: true,
          itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
        },
        startDate: '12/19/2018',
        endDate: '12/19/2018',
        displayStartDate: '12/19/2018',
        displayEndDate: '12/20/2018',
        products: [{
          category: 'Timberline® Lifetime Shingles',
          selectedProductFamily: ['Timberline HD®'],
          allowanceType: 'PCT',
          uom: '',
          productValue: 2,
        },
        {
          category: 'Timberline® Busters',
          selectedProductFamily: ['Timberline XXX'],
          allowanceType: 'DOL',
          uom: 'SQ',
          productValue: 4,
        }],
        imageData: 'GAF_Logo_jpg.jpg',
      },
      promotionStatus: 'APPROVED',
      planId: 'SP01',
      isLoading: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_SPOT_PROMOTION_FAILURE', () => {
    const action = {
      type: PROMOTION.GET_SPOT_PROMOTION_FAILURE, error: 'Internal Server Error',
    };
    const expected = {
      ...initialState,
      error: 'Internal Server Error',
      isLoading: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});

describe('PROMOTION Reducers for family reset', () => {
  const initState = {
    values: {
      title: 'One Day Rebate',
      description: 'One Day Only Timberline Offer! ',
      disclaimer: 'Lorem ipsum',
      selectedStates: {
        isSelectAll: true,
        itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
      },
      startDate: '12/19/2018',
      endDate: '12/19/2018',
      displayStartDate: '12/19/2018',
      displayEndDate: '12/20/2018',
      products: [{
        category: 'Timberline® Lifetime Shingles',
        selectedProductFamily: ['Timberline HD®'],
        allowanceType: 'PCT',
        uom: '',
        productValue: 2,
      },
      {
        category: 'Timberline® Busters',
        selectedProductFamily: ['Timberline XXX'],
        allowanceType: 'DOL',
        uom: 'SQ',
        productValue: 4,
      }],
      imageData: 'GAF_Logo_jpg.jpg',
    },
    isShowPopup: false,
    saveDone: false,
    isApproveShowPopup: false,
    fetchCount: 0,
  };
  it('should call PRODUCT_FAMILY_RESET', () => {
    const data = {
      uom: '',
      allowanceType: 'PCT',
    };
    const action = {
      type: PROMOTION.PRODUCT_FAMILY_RESET, data, index: 1,
    };
    const expected = {
      ...initState,
      values: {
        title: 'One Day Rebate',
        description: 'One Day Only Timberline Offer! ',
        disclaimer: 'Lorem ipsum',
        selectedStates: {
          isSelectAll: true,
          itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
        },
        startDate: '12/19/2018',
        endDate: '12/19/2018',
        displayStartDate: '12/19/2018',
        displayEndDate: '12/20/2018',
        products: [{
          category: 'Timberline® Lifetime Shingles',
          selectedProductFamily: ['Timberline HD®'],
          allowanceType: 'PCT',
          uom: '',
          productValue: 2,
        },
        {
          category: 'Timberline® Busters',
          selectedProductFamily: ['Timberline XXX'],
          allowanceType: 'PCT',
          uom: '',
          productValue: 4,
        }],
        imageData: 'GAF_Logo_jpg.jpg',
      },
    };
    expect(Reducer(initState, action)).toEqual(expected);
  });
});
