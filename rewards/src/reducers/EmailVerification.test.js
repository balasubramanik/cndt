import * as EMAILVERIFICATION from '../actionTypes/EmailVerification';
import Reducer from './EmailVerification';

const initialState = {
  values: {
    step: 0,
  },
};

describe('Email verification Reducers', () => {
  it('should call CHANGE_ROOF_OPTIONS', () => {
    const action = { type: EMAILVERIFICATION.CHANGE_ROOF_OPTIONS };
    const expected = {
      ...initialState,
      values: {
        ...initialState.values,
        installedOption: '',
        contractorClassification: '',
        authorizeeDesignation: '',
        description: '',
        termsAndConditions: false,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CHANGE_INSTALL_ROOF_OPTIONS', () => {
    const action = { type: EMAILVERIFICATION.CHANGE_INSTALL_ROOF_OPTIONS };
    const expected = {
      ...initialState,
      values: {
        ...initialState.values,
        authorizeeDesignation: '',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SUBMIT_EMAIL_VERIFICATION', () => {
    const action = { type: EMAILVERIFICATION.SUBMIT_EMAIL_VERIFICATION };
    const expected = {
      ...initialState,
      isLoading: true,
      done: false,
      isEmailExist: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SUBMIT_EMAIL_VERIFICATION_SUCCESS', () => {
    const action = {
      type: EMAILVERIFICATION.SUBMIT_EMAIL_VERIFICATION_SUCCESS,
      data: {
        emailExists: true,
        id: '1234567890',
        status: 'pending',
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      done: true,
      isEmailExist: true,
      guid: '1234567890',
      status: 'pending',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SUBMIT_EMAIL_VERIFICATION_FAILURE', () => {
    const action = {
      type: EMAILVERIFICATION.SUBMIT_EMAIL_VERIFICATION_FAILURE,
      error: 'Internal Server Error',
    };
    const expected = {
      ...initialState,
      isLoading: false,
      done: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call RESEND_EMAIL', () => {
    const action = { type: EMAILVERIFICATION.RESEND_EMAIL };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call RESEND_EMAIL_SUCCESS', () => {
    const action = { type: EMAILVERIFICATION.RESEND_EMAIL_SUCCESS, data: { status: 'pending' } };
    const expected = {
      ...initialState,
      isLoading: false,
      status: 'pending',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call RESEND_EMAIL_FAILURE', () => {
    const action = {
      type: EMAILVERIFICATION.RESEND_EMAIL_FAILURE,
      error: 'Internal Server Error',
    };
    const expected = {
      ...initialState,
      isLoading: false,
      done: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLEAR_ERRORS', () => {
    const action = {
      type: EMAILVERIFICATION.CLEAR_ERRORS,
    };
    const expected = {
      ...initialState,
      error: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
