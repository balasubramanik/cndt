import * as CONTRACTOR from '../actionTypes/ContractorUser';
import Reducer from './ContractorUser';

const initialState = {
  users: [],
};

describe('Contractor User Reducers', () => {
  it('should call GET_USERS', () => {
    const action = { type: CONTRACTOR.GET_USERS };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_USERS_SUCCESS', () => {
    const action = { type: CONTRACTOR.GET_USERS_SUCCESS, data: [{ name: 'John' }, { name: 'Doe' }] };
    const expected = {
      ...initialState,
      isLoading: false,
      users: [
        { name: 'John' },
        { name: 'Doe' },
      ],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_USERS_FAILURE', () => {
    const action = { type: CONTRACTOR.GET_USERS_FAILURE, error: 'internal server error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
