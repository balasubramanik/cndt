import * as SEARCHBY from '../actionTypes/SearchBy';
// import { searchByType } from '../constants/constants';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case SEARCHBY.UPDATE_SEARCH_TEXT: {
      const { searchText, searchByName } = action;
      return {
        ...state,
        [searchByName]: {
          searchText,
        },
      };
    }
    case SEARCHBY.CLEAR_SEARCH_TEXT: {
      const { searchByName } = action;
      return {
        ...state,
        [searchByName]: {
          searchText: '',
        },
      };
    }
    case SEARCHBY.GET_SUGGESTION: {
      const { searchByName } = action;
      return {
        ...state,
        [searchByName]: {
          isSuggestionsLoading: true,
        },
      };
    }
    case SEARCHBY.GET_SUGGESTION_SUCCESS: {
      const { searchByName, suggestions } = action;
      return {
        ...state,
        [searchByName]: {
          isSuggestionsLoading: false,
          suggestions,
          suggestionsError: '',
        },
      };
    }
    case SEARCHBY.GET_SUGGESTION_FAILURE: {
      const { searchByName, error } = action;
      return {
        ...state,
        [searchByName]: {
          isSuggestionsLoading: false,
          suggestions: [],
          suggestionsError: error,
        },
      };
    }
    default:
      return state;
  }
};
