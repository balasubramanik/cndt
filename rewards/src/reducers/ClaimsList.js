import * as CLAIMSLIST from '../actionTypes/ClaimsList';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case CLAIMSLIST.GET_CLAIMS_LIST:
      return {
        ...state,
        isLoading: true,
      };
    case CLAIMSLIST.GET_CLAIMS_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        filter: (action.data && action.data.filter) ? action.data.filter : {},
        paging: (action.data && action.data.paging) ? action.data.paging : { token: '', totalRecords: 0 },
        listOfClaims: {
          ...state.listOfClaims,
          [(action.data && action.data.paging) ? action.data.paging.pageNo : 1]: action.data.result,
        },
        claimsListError: '',
      };
    case CLAIMSLIST.GET_CLAIMS_LIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        claimsListError: (action.error) ? action.error : '',
      };
    case CLAIMSLIST.GET_CLAIMS_LIST_CLEAR:
      return {
        ...state,
        isLoading: false,
        listOfClaims: {},
      };
    case CLAIMSLIST.GET_EXPORT_CSV_SUCCESS:
      return {
        ...state,
        isLoading: false,
        listOfClaimsCSV: (action.data && action.data.result) ? action.data.result : [],
        claimsListError: '',
      };
    case CLAIMSLIST.CLAIM_LIST_CLEAR_ERROR:
      return {
        ...state,
        claimsListError: null,
      };
    default:
      return state;
  }
};
