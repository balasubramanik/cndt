import * as ORDERDETAILS from '../actionTypes/OrderDetails';
import { searchShowBy } from '../components/showBy/constants';
import Reducer from './OrderDetails';

const initialState = {
  listOfOrder: [],
  pageNumber: 1,
  showPerPage: Number(searchShowBy.showBy[0].value),
  isAscending: '',
  sortField: '',
  searchText: '',
  pointsRedeemed: '',
};

describe('ORDERDETAILS Reducers', () => {
  it('should call GET_LIST_OF_PRODUCT', () => {
    const action = { type: ORDERDETAILS.GET_LIST_OF_PRODUCT };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_PRODUCT_SUCCESS', () => {
    const data = {
      items: [{
        productName: 'Jiffy Lube',
        serviceFeeType: '%',
        serviceFeeRate: '2',
        amountRedeemed: '2000',
        cardValue: '5000',
        orderDate: '2018-05-20T21:12:44.0122176+05:30',
      },
      {
        productName: 'Cabela’s',
        serviceFeeType: '$',
        serviceFeeRate: '2.0',
        amountRedeemed: '1000',
        cardValue: '5000',
        orderDate: '',
      }],
      pointsRedeemed: 2000,
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
    };
    const action = {
      type: ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfOrder: [{
        productName: 'Jiffy Lube',
        serviceFeeType: '%',
        serviceFeeRate: '2%',
        amountRedeemed: '2,000.00',
        cardValue: '$5,000.00',
        orderDate: '05/20/2018',
        modifiedOn: '09/28/2018',
      },
      {
        productName: 'Cabela’s',
        serviceFeeType: '$',
        serviceFeeRate: '$2.00',
        amountRedeemed: '1,000.00',
        cardValue: '$5,000.00',
        orderDate: 'N/A',
        modifiedOn: '09/28/2018',
      }],
      totalItems: 2,
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsRedeemed: 2000,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_PRODUCT_SUCCESS with empty values inside item', () => {
    const data = {
      items: [{
        productName: 'Jiffy Lube',
        serviceFeeType: '%',
        serviceFeeRate: '2',
        amountRedeemed: '2000',
        cardValue: '5000',
        orderDate: '2018-05-20T21:12:44.0122176+05:30',
      },
      {
        productName: 'Cabela’s',
        serviceFeeType: '',
        serviceFeeRate: '',
        amountRedeemed: '',
        cardValue: '',
        orderDate: '',
      }],
      pointsRedeemed: 2000,
      modifiedOn: '',
    };
    const action = {
      type: ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfOrder: [{
        productName: 'Jiffy Lube',
        serviceFeeType: '%',
        serviceFeeRate: '2%',
        amountRedeemed: '2,000.00',
        cardValue: '$5,000.00',
        orderDate: '05/20/2018',
        modifiedOn: 'N/A',
      },
      {
        productName: 'Cabela’s',
        serviceFeeType: '',
        serviceFeeRate: 'N/A',
        amountRedeemed: 'N/A',
        cardValue: 'N/A',
        orderDate: 'N/A',
        modifiedOn: 'N/A',
      }],
      totalItems: 2,
      modifiedOn: '',
      pointsRedeemed: 2000,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_PRODUCT_SUCCESS with no data supplied', () => {
    const action = {
      type: ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfOrder: [],
      totalItems: 0,
      modifiedOn: undefined,
      pointsRedeemed: undefined,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call GET_LIST_OF_PRODUCT_SUCCESS with empty items array', () => {
    const data = {
      items: [],
      pointsRedeemed: 2000,
      modifiedOn: '',
    };
    const action = {
      type: ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfOrder: [],
      totalItems: 0,
      modifiedOn: '',
      pointsRedeemed: 2000,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_PRODUCT_SUCCESS with data null', () => {
    const data = null;
    const action = {
      type: ORDERDETAILS.GET_LIST_OF_PRODUCT_SUCCESS,
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      pageNumber: 1,
      listOfOrder: [],
      totalItems: 0,
      modifiedOn: undefined,
      pointsRedeemed: undefined,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_LIST_OF_PRODUCT_FAILURE', () => {
    const action = { type: ORDERDETAILS.GET_LIST_OF_PRODUCT_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call UPDATE_PAGE_DETAILS', () => {
    const action = { type: ORDERDETAILS.UPDATE_PAGE_DETAILS };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
