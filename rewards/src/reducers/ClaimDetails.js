import * as CLAIMDETAILS from '../actionTypes/ClaimDetails';
import { searchShowBy } from '../components/showBy/constants';

const initialState = {
  listOfInvoice: [],
  pageNumber: 1,
  showPerPage: Number(searchShowBy.showBy[0].value),
  isAscending: '',
  sortField: '',
  searchText: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CLAIMDETAILS.GET_INVOICE_LIST:
      return {
        ...state,
        isLoading: true,
        searchText: '',
      };
    case CLAIMDETAILS.GET_INVOICE_LIST_SUCCESS: {
      const { data = {} } = action;
      const { claimId, submittedOn, invoiceDetails = [] } = data;
      const { pointsEarned } = data;
      const totalItems = invoiceDetails.length;
      return {
        ...state,
        isLoading: false,
        pageNumber: 1,
        listOfInvoice: invoiceDetails,
        totalItems,
        pointsEarned,
        claimId,
        submittedOn,
      };
    }
    case CLAIMDETAILS.GET_INVOICE_LIST_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case CLAIMDETAILS.GET_INVOICE_LIST_CLEAR:
      return {
        ...state,
        isLoading: false,
        error: '',
        listOfInvoice: [],
        totalItems: 0,
      };
    case CLAIMDETAILS.UPDATE_PAGE_DETAILS: {
      const { details = {} } = action;
      return {
        ...state,
        ...details,
      };
    }
    default:
      return state;
  }
};
