import * as CONTRACTOR from '../actionTypes/ContractorUser';

const initialState = {
  users: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CONTRACTOR.GET_USERS: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case CONTRACTOR.GET_USERS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        users: action.data,
      };
    }
    case CONTRACTOR.GET_USERS_FAILURE: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    default:
      return state;
  }
};
