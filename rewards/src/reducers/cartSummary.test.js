import * as CARTSUMMARY from '../actionTypes/CartSummary';
import Reducer from './cartSummary';

const initialState = {
  cartSummaryData: {},
  isLoading: true,
  isItemDelete: false,
  isDebitCardAvail: true,
  isCartSubmitAlready: false,
  done: false,
};

describe('getCartSummary', () => {
  it('should call GET_CART_SUMMARY', () => {
    const action = { type: CARTSUMMARY.GET_CART_SUMMARY };
    const expected = {
      ...initialState,
      isLoading: true,
      isGetCartSuccess: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.GET_CART_SUMMARY_SUCCESS', () => {
    const action = {
      type: CARTSUMMARY.GET_CART_SUMMARY_SUCCESS,
      data: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        items: [
          {
            cardValue: 1,
            category: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };

    const expected = {
      ...initialState,
      isLoading: false,
      isGetCartSuccess: true,
      cartSummaryData: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        items: [
          {
            cardValue: 1,
            category: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.GET_CART_SUMMARY_SUCCESS WITHOUT DATA', () => {
    const action = {
      type: CARTSUMMARY.GET_CART_SUMMARY_SUCCESS,
      data: null,
    };

    const expected = {
      ...initialState,
      isLoading: false,
      isGetCartSuccess: true,
      cartSummaryData: {},
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.GET_CART_SUMMARY_FAILURE', () => {
    const action = { type: CARTSUMMARY.GET_CART_SUMMARY_FAILURE, error: 'internal server error' };
    const expected = {
      ...initialState,
      isLoading: false,
      getCartSummaryError: action.error,
      isGetCartSuccess: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CART CLEAR ERROR', () => {
    const action = { type: CARTSUMMARY.CLEAR_ERROR };
    const expected = {
      ...initialState,
      getCartSummaryError: null,
      cartDeleteFailure: null,
      submitCartSummaryError: null,
      updateCartSummaryError: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CART INITIAL STATE', () => {
    const action = { type: CARTSUMMARY.CART_INITIAL_STATE };
    const expected = {
      ...initialState,
      isItemDelete: false,
      isDebitCardAvail: true,
      isCartSubmitAlready: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CART CART_RESET_DATA', () => {
    const action = { type: CARTSUMMARY.CART_RESET_DATA };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.UPDATE_CART_SUMMARY', () => {
    const action = { type: CARTSUMMARY.UPDATE_CART_SUMMARY };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.UPDATE_CART_SUMMARY_SUCCESS', () => {
    const action = { type: CARTSUMMARY.UPDATE_CART_SUMMARY_SUCCESS };
    const expected = {
      ...initialState,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE', () => {
    const action = {
      type: CARTSUMMARY.UPDATE_CART_SUMMARY_FAILURE,
      error: 'Internal server error',
    };
    const expected = {
      ...initialState,
      updateCartSummaryError: action.error,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.DELETE_CART_SUMMARY', () => {
    const action = { type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY };
    const expected = {
      ...initialState,
      isLoading: true,
      isItemDelete: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.DELETE_CART_SUMMARY_SUCCESS', () => {
    const action = {
      type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_SUCCESS,
      data: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        items: [
          {
            cardValue: 1,
            category: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      isItemDelete: true,
      isDisableOrder: false,
      cartSummaryData: {
        balance: 1000,
        role: 'contractor',
        firsttime_flag: true,
        items: [
          {
            cardValue: 1,
            category: 'giftCard',
            name: 'Macy',
            description: 'You will receive this gift card electronically after checkout.',
            terms: 'This item is non-refundable.',
          },
        ],
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.DELETE_CART_SUMMARY_SUCCESS WITHOUT DATA', () => {
    const action = {
      type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_SUCCESS,
      data: null,
    };

    const expected = {
      ...initialState,
      isLoading: false,
      isItemDelete: true,
      isDisableOrder: false,
      cartSummaryData: {},
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_FAILURE', () => {
    const action = {
      type: CARTSUMMARY.DELETE_ITEM_CART_SUMMARY_FAILURE,
      error: 'Internal server error',
    };
    const expected = {
      ...initialState,
      cartDeleteFailure: action.error,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY', () => {
    const action = { type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY };
    const expected = {
      ...initialState,
      isLoading: true,
      isDebitCardAvail: true,
      isCartSubmitAlready: false,
      done: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_SUCCESS', () => {
    const action = {
      type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_SUCCESS,
      data: {
        success: true,
        data: {
          orderNumber: 'o123456789',
        },
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      isDebitCardAvail: true,
      isCartSubmitAlready: false,
      done: action.data.success,
      orderNumber: action.data.data,

    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE WITH DEBIT CARD AVAIL FALSE', () => {
    const submitCart = true;
    const debitCardAvail = false;
    const action = {
      type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE,
      error: {
        success: false,
        statusCode: 500,
        data: null,
        errors: [
          {
            code: '03023',
            description: 'InternalServerError',
          },
        ],
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      isCartSubmitAlready: submitCart,
      isDebitCardAvail: debitCardAvail,
      submitCartSummaryError: debitCardAvail ? action.error : null,

    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE WITH DEBIT CARD AVAIL TRUE', () => {
    const submitCart = true;
    const debitCardAvail = true;
    const action = {
      type: CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE,
      error: {
        success: false,
        statusCode: 500,
        data: null,
        errors: [
          {
            code: '03021',
            description: 'InternalServerError',
          },
        ],
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      isCartSubmitAlready: submitCart,
      isDebitCardAvail: debitCardAvail,
      submitCartSummaryError: debitCardAvail ? action.error : null,

    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CARTSUMMARY.SUBMIT_ITEM_CART_SUMMARY_FAILURE WITH NO DATA', () => {
    const submitCart = true;
    const debitCardAvail = true;
    const action = {};
    const expected = {
      ...initialState,
      isLoading: false,
      isCartSubmitAlready: submitCart,
      isDebitCardAvail: debitCardAvail,
      submitCartSummaryError: debitCardAvail ? action.error : null,

    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
});
