import * as CONTACTUS from '../actionTypes/ContactUs';

const initialState = {
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CONTACTUS.SUBMIT_CONTACTUS:
      return {
        ...state,
        isLoading: true,
        successContactUS: false,
      };
    case CONTACTUS.SUBMIT_CONTACTUS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        successContactUS: action.data,
      };
    }
    case CONTACTUS.SUBMIT_CONTACTUS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    case CONTACTUS.RESET_CONTACTUS:
      return {
        ...state,
        successContactUS: false,
      };
    case CONTACTUS.CLEAR_ERRORS:
      return {
        ...state,
        error: null,
      };
    default:
      return state;
  }
};
