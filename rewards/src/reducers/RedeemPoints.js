import * as REDEEMPOINTS from '../actionTypes/RedeemPoints';

const initialState = {
  isShowPopup: false,
  fetchCount: 0,
  giftCardDetails: {},
  cardInformation: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case REDEEMPOINTS.GET_GIFT_CATALOGUE: {
      const { fetchCount } = state;
      return {
        ...state,
        isLoading: true,
        fetchCount: fetchCount + 1,
      };
    }
    case REDEEMPOINTS.GET_GIFT_CATALOGUE_SUCCESS: {
      const { fetchCount } = state;
      const { data, key, category } = action;
      const { categories, debitCardUserName } = data || {};
      const count = fetchCount - 1;
      return {
        ...state,
        [key]: categories,
        [`${category}CardHolderName`]: debitCardUserName,
        isLoading: !(count === 0),
        fetchCount: count,
      };
    }
    case REDEEMPOINTS.GET_GIFT_CATALOGUE_FAILURE: {
      const { fetchCount } = state;
      const { key } = action;
      const count = fetchCount - 1;
      return {
        ...state,
        [`${key}Error`]: action.error,
        isLoading: !(count === 0),
        fetchCount: count,
      };
    }
    case REDEEMPOINTS.GET_GIFT_CARD_DETAILS: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case REDEEMPOINTS.GET_GIFT_CARD_DETAILS_SUCCESS: {
      const { productId, data } = action;
      return {
        ...state,
        isLoading: false,
        giftCardDetails: {
          ...state.giftCardDetails,
          [productId]: data,
        },
      };
    }
    case REDEEMPOINTS.GET_GIFT_CARD_DETAILS_FAILURE: {
      return {
        ...state,
        isLoading: false,
        detailsError: action.error,
      };
    }
    case REDEEMPOINTS.UPDATE_CARD_INFORMATION: {
      const {
        id, amount, fee, variantId, productImage,
      } = action;
      return {
        ...state,
        cardInformation: {
          ...state.cardInformation,
          [id]: {
            ...state[id],
            amount,
            feeInfo: fee,
            variantId,
            productImage,
          },
        },
      };
    }
    case REDEEMPOINTS.CLEAR_ERRORS:
      return {
        ...state,
        debitCardInfoError: null,
        achInfoError: null,
        addToCartError: null,
        giftCardsError: null,
        detailsError: null,
      };
    case REDEEMPOINTS.ADD_TO_CART: {
      const { category } = action;
      return {
        ...state,
        isLoading: true,
        category,
      };
    }
    case REDEEMPOINTS.ADD_TO_CART_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isShowPopup: true,
      };
    case REDEEMPOINTS.ADD_TO_CART_FAILURE:
      return {
        ...state,
        isLoading: false,
        addToCartError: action.error,
      };
    case REDEEMPOINTS.CLOSE_POPUP:
      return {
        ...state,
        isShowPopup: false,
      };
    case REDEEMPOINTS.RESET_GIFT_ITEMS: {
      return {
        ...state,
        ...initialState,
        debitCardInfo: null,
        achInfo: null,
        giftCards: null,
      };
    }
    default:
      return state;
  }
};
