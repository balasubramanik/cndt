import * as MYPROFILE from '../actionTypes/MyProfile';

const initialState = {
  userInfo: {},
  rewardsInfo: {},
  accessInfo: false,
  adminUserInfo: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case MYPROFILE.GET_PROFILE:
      return {
        ...state,
        isProfileLoading: true,
      };
    case MYPROFILE.GET_PROFILE_SUCCESS: {
      const {
        contractorAccountInformation,
        firstName,
        lastName,
        createdBy,
        isAchEnabled,
        emailAddress,
      } = action.info;
      return {
        ...state,
        isProfileLoading: false,
        userInfo: {
          isAchEnabled,
          firstName,
          lastName,
          createdBy,
          emailAddress,
          ...contractorAccountInformation,
        },
      };
    }
    case MYPROFILE.GET_PROFILE_FAILURE:
      return {
        ...state,
        isProfileLoading: false,
        profileError: action.error,
      };
    case MYPROFILE.GET_ADMIN_PROFILE:
      return {
        ...state,
        isAdminProfileLoading: true,
      };
    case MYPROFILE.GET_ADMIN_PROFILE_SUCCESS: {
      const {
        firstName,
        lastName,
        emailAddress,
      } = action.info;
      return {
        ...state,
        isAdminProfileLoading: false,
        adminUserInfo: {
          firstName,
          lastName,
          emailAddress,
        },
      };
    }
    case MYPROFILE.GET_ADMIN_PROFILE_FAILURE:
      return {
        ...state,
        isAdminProfileLoading: false,
        adminProfileError: action.error,
      };

    case MYPROFILE.GET_REWARDS:
      return {
        ...state,
        isRewardsLoading: true,
      };
    case MYPROFILE.GET_REWARDS_SUCCESS:
      return {
        ...state,
        isRewardsLoading: false,
        rewardsInfo: action.info,
      };
    case MYPROFILE.GET_REWARDS_FAILURE:
      return {
        ...state,
        isRewardsLoading: false,
        rewardsError: action.error,
      };
    case MYPROFILE.ACCESS_PAGE:
      return {
        ...state,
        accessInfo: action.info,
      };
    default:
      return state;
  }
};
