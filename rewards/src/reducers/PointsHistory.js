import * as POINTSHISTORY from '../actionTypes/PointsHistory';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case POINTSHISTORY.GET_LIST_OF_TRANSACTIONS: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        filter: (action.data && action.data.filter) ? action.data.filter : {},
        paging: (action.data && action.data.paging) ? action.data.paging : { token: '', totalRecords: 0 },
        listOfTransactions: {
          ...state.listOfTransactions,
          [(action.data && action.data.paging) ? action.data.paging.pageNo : 1]: action.data.result,
        },
        transactionListError: '',
      };
    }
    case POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_FAILURE:
      return {
        ...state,
        isLoading: false,
        transactionListError: (action.error) ? action.error : '',
      };
    case POINTSHISTORY.GET_LIST_OF_TRANSACTIONS_CLEAR:
      return {
        ...state,
        isLoading: false,
        listOfTransactions: {},
      };
    case POINTSHISTORY.GET_EXPORT_CSV_SUCCESS:
      return {
        ...state,
        isLoading: false,
        listOfTransactionCSV: (action.data && action.data.result) ? action.data.result : [],
        transactionListError: '',
      };
    case POINTSHISTORY.TRANSACTIONS_LIST_CLEAR_ERROR:
      return {
        ...state,
        transactionListError: null,
      };
    default:
      return state;
  }
};

