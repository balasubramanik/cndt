import moment from 'moment';
import * as REGISTRATION from '../actionTypes/Registration';
import Reducer from './Registration';

const initialState = {
  values: {
    step: 1,
    hasHeadQuatersAddress: true,
    hasMailingAddress: true,
    canNotify: false,
    date: moment().format('MM/DD/YYYY'),
    countrybr: 'United States',
    countryml: 'United States',
    countryhq: 'United States',
    countryCode: 'USA',
  },
};
describe('Registration container Reducers', () => {
  it('should call CHANGE_ADDRESS', () => {
    const action = {
      type: REGISTRATION.CHANGE_ADDRESS,
      name: 'hq',
      value: '123',
    };
    const expected = {
      ...initialState,
      hqLocationText: '123',
      values: {
        ...initialState.values,
        address1hq: '123',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SELECT_ADDRESS', () => {
    const action = {
      type: REGISTRATION.SELECT_ADDRESS,
      name: 'hq',
      address: {
        address1: '5650 Westheimer Road',
        address2: 'Westside',
        city: 'Houston',
        state: 'Texas',
        zip: '77056',
        country: 'United States',
        stateShortName: 'DC',
        countryShortName: 'US',
      },
    };
    const expected = {
      ...initialState,
      hqLocationText: '5650 Westheimer Road',
      values: {
        ...initialState.values,
        address1hq: '5650 Westheimer Road',
        address2hq: 'Westside',
        cityhq: 'Houston',
        statehq: 'Texas',
        ziphq: '77056',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call SELECT_SERVICES', () => {
    const action = { type: REGISTRATION.SELECT_SERVICES, key: 'gutter', value: true };
    const expected = {
      ...initialState,
      values: {
        ...initialState.values,
        gutter: true,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call REGISTRATION_INFO', () => {
    const action = { type: REGISTRATION.REGISTRATION_INFO };
    const expected = {
      ...initialState,
      status: '',
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call REGISTRATION_INFO_SUCCESS', () => {
    const action = {
      type: REGISTRATION.REGISTRATION_INFO_SUCCESS,
      info: {
        status: 'completed',
        contact: {
          firstName: 'john',
          lastName: 'doe',
          emailAddress: 'johndoe@test.com',
        },
      },
    };
    const expected = {
      ...initialState,
      status: 'completed',
      isLoading: false,
      values: {
        ...initialState.values,
        firstName: 'john',
        lastName: 'doe',
        email: 'johndoe@test.com',
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call REGISTRATION_INFO_SUCCESS with no contact provided', () => {
    const action = {
      type: REGISTRATION.REGISTRATION_INFO_SUCCESS,
      info: {
        status: 'completed',
      },
    };
    const expected = {
      ...initialState,
      status: 'completed',
      isLoading: false,
      values: {
        ...initialState.values,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call REGISTRATION_INFO_ERROR', () => {
    const action = {
      type: REGISTRATION.REGISTRATION_INFO_ERROR,
      error: 'Internal server error',
    };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call REGISTER_NOW', () => {
    const action = { type: REGISTRATION.REGISTER_NOW };
    const expected = {
      ...initialState,
      done: false,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call REGISTER_NOW_SUCCESS', () => {
    const action = { type: REGISTRATION.REGISTER_NOW_SUCCESS, info: { status: 'completed' } };
    const expected = {
      ...initialState,
      isLoading: false,
      done: true,
      registerNow: { status: 'completed' },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call UPDATE_REGISTRATION_OPTION', () => {
    const action = { type: REGISTRATION.UPDATE_REGISTRATION_OPTION, fields: { gutters: false } };
    const expected = {
      ...initialState,
      values: { ...initialState.values, gutters: false },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call REGISTER_NOW_ERROR', () => {
    const action = {
      type: REGISTRATION.REGISTER_NOW_ERROR,
      error: 'Internal server error',
    };
    const expected = {
      ...initialState,
      isLoading: false,
      done: false,
      error: 'Internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLEAR_ERRORS', () => {
    const action = {
      type: REGISTRATION.CLEAR_ERRORS,
    };
    const expected = {
      ...initialState,
      error: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
