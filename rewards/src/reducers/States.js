import * as STATES from '../actionTypes/States';

const initialState = {};
export default (state = initialState, action) => {
  switch (action.type) {
    case STATES.GET_STATES: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case STATES.GET_STATES_SUCCESS: {
      const { data } = action;
      return {
        ...state,
        isLoading: false,
        states: data,
      };
    }
    case STATES.GET_STATES_FAILURE: {
      const { error } = action;
      return {
        ...state,
        isLoading: false,
        error,
      };
    }
    default:
      return state;
  }
};
