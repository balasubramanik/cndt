import * as REDEEMPOINTS from '../actionTypes/RedeemPoints';
import Reducer from './RedeemPoints';

const initialState = {
  fetchCount: 0,
  isShowPopup: false,
  giftCardDetails: {},
  cardInformation: {},
};

describe('RedeemPoints Reducers', () => {
  it('should call GET_GIFT_CATALOGUE', () => {
    const action = { type: REDEEMPOINTS.GET_GIFT_CATALOGUE };
    const expected = {
      ...initialState,
      isLoading: true,
      fetchCount: 1,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_GIFT_CATALOGUE_SUCCESS', () => {
    const action = {
      type: REDEEMPOINTS.GET_GIFT_CATALOGUE_SUCCESS,
      key: 'debitCardInfo',
      data: {
        categories: [
          {
            products: [
              {
                listprice: '0.000',
                cardType: 'Physical Card',
                feeType: '%',
                feeValue: '2.5',
                giftCardValue: null,
                imageicon: null,
                max: '99,999.00',
                min: '25.00',
                minmaxtype: '$',
                Variant_Images: null,
                DisplayName: 'VISA Reloadble Debit Card',
              },
            ],
          },
        ],
      },
    };
    const expected = {
      ...initialState,
      isLoading: true,
      fetchCount: -1,
      debitCardInfo: [
        {
          products: [
            {
              listprice: '0.000',
              cardType: 'Physical Card',
              feeType: '%',
              feeValue: '2.5',
              giftCardValue: null,
              imageicon: null,
              max: '99,999.00',
              min: '25.00',
              minmaxtype: '$',
              Variant_Images: null,
              DisplayName: 'VISA Reloadble Debit Card',
            },
          ],
        },
      ],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_GIFT_CATALOGUE_SUCCESS without data', () => {
    const action = {
      type: REDEEMPOINTS.GET_GIFT_CATALOGUE_SUCCESS,
      key: 'debitCardInfo',
    };
    const expected = {
      ...initialState,
      fetchCount: -1,
      isLoading: true,
      debitCardInfo: undefined,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_GIFT_CATALOGUE_FAILURE', () => {
    const action = { type: REDEEMPOINTS.GET_GIFT_CATALOGUE_FAILURE, key: 'debitCardInfo', error: 'internal server error' };
    const expected = {
      ...initialState,
      isLoading: true,
      fetchCount: -1,
      debitCardInfoError: 'internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLEAR_ERRORS', () => {
    const action = { type: REDEEMPOINTS.CLEAR_ERRORS };
    const expected = {
      ...initialState,
      debitCardInfoError: null,
      achInfoError: null,
      addToCartError: null,
      giftCardsError: null,
      detailsError: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call ADD_TO_CART', () => {
    const action = { type: REDEEMPOINTS.ADD_TO_CART, category: 'debit' };
    const expected = {
      ...initialState,
      isLoading: true,
      category: 'debit',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call ADD_TO_CART_SUCCESS', () => {
    const action = { type: REDEEMPOINTS.ADD_TO_CART_SUCCESS };
    const expected = {
      ...initialState,
      isLoading: false,
      isShowPopup: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call ADD_TO_CART_FAILURE', () => {
    const action = { type: REDEEMPOINTS.ADD_TO_CART_FAILURE, error: 'Internal server error' };
    const expected = {
      ...initialState,
      isLoading: false,
      addToCartError: action.error,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_GIFT_CARD_DETAILS', () => {
    const action = { type: REDEEMPOINTS.GET_GIFT_CARD_DETAILS };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_GIFT_CARD_DETAILS_SUCCESS', () => {
    const data = {
      listprice: '0.0000',
      cardType: 'E card',
      feeType: '$',
      feeValue: '1.0',
      giftCardValue: '25.0000',
      feeDescription: 'Aéropostale - $26',
      imageicon: null,
      max: '100.0',
      min: '1.0',
      minmaxtype: 'QTY',
      Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
      DisplayName: 'Aéropostale - $26',
    };
    const action = {
      type: REDEEMPOINTS.GET_GIFT_CARD_DETAILS_SUCCESS,
      productId: 'MA123',
      data,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      giftCardDetails: {
        MA123: data,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_GIFT_CARD_DETAILS_FAILURE', () => {
    const action = {
      type: REDEEMPOINTS.GET_GIFT_CARD_DETAILS_FAILURE,
      error: 'Internal server error',
    };
    const expected = {
      ...initialState,
      isLoading: false,
      detailsError: 'Internal server error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLOSE_POPUP', () => {
    const action = { type: REDEEMPOINTS.CLOSE_POPUP };
    const expected = {
      ...initialState,
      isShowPopup: false,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call UPDATE_CARD_INFORMATION', () => {
    const action = {
      type: REDEEMPOINTS.UPDATE_CARD_INFORMATION,
      id: 'MA123',
      amount: {},
      fee: {},
      variantId: {},
    };
    const expected = {
      ...initialState,
      cardInformation: {
        MA123: {
          amount: {},
          feeInfo: {},
          variantId: {},
        },
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });
  it('should call RESET_GIFT_ITEMS', () => {
    const action = {
      type: REDEEMPOINTS.RESET_GIFT_ITEMS,
    };
    const expected = {
      ...initialState,
      debitCardInfo: null,
      achInfo: null,
      giftCards: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
