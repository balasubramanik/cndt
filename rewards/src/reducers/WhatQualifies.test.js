import * as WHATQUALIFIES from '../actionTypes/WhatQualifies';
import Reducer from './WhatQualifies';

const initialState = {
  myPlanCategory: [],
  spotPromotionsCategory: [],
};

describe('WhatQualifies MyPlan Reducers', () => {
  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY', () => {
    const action = { type: WHATQUALIFIES.GET_MYPLAN_CATEGORY };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });


  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS', () => {
    const action = {
      type: WHATQUALIFIES.GET_MYPLAN_CATEGORY_SUCCESS,
      data: [{
        category: 'Three-Tab Shingles',
        products: [
          {
            planId: '53370',
            startDate: '2018-11-02T18:30:00+00:00',
            endDate: '2018-10-11T00:00:00',
            byPercent: 1,
            productFamily: 'Royal Sovereign®',
            dollarPerUnit: 0,
            uom: {
              code: 'N/A',
              description: 'N/A',
            },
            planType: 'Standard',
            allowanceType: 'PCT',
          },
          {
            planId: '53531',
            startDate: '2018-11-21T00:00:00',
            endDate: '2018-12-31T00:00:00',
            byPercent: 0,
            productFamily: 'Royal Sovereign®',
            dollarPerUnit: 3,
            uom: {
              code: 'BD',
              description: 'BD',
            },
            planType: 'Custom',
            allowanceType: 'DOL',
          },
        ],
      }],
    };
    const expected = {
      ...initialState,
      isLoading: false,
      myPlanCategory: [{
        category: 'Three-Tab Shingles',
        products: [
          {
            planId: '53370',
            startDate: '2018-11-02T18:30:00+00:00',
            endDate: '2018-10-11T00:00:00',
            byPercent: 1,
            productFamily: 'Royal Sovereign®',
            dollarPerUnit: 0,
            uom: {
              code: 'N/A',
              description: 'N/A',
            },
            planType: 'Standard',
            allowanceType: 'PCT',
          },
          {
            planId: '53531',
            startDate: '2018-11-21T00:00:00',
            endDate: '2018-12-31T00:00:00',
            byPercent: 0,
            productFamily: 'Royal Sovereign®',
            dollarPerUnit: 3,
            uom: {
              code: 'BD',
              description: 'BD',
            },
            planType: 'Custom',
            allowanceType: 'DOL',
          },
        ],
      }],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE', () => {
    const action = { type: WHATQUALIFIES.GET_MYPLAN_CATEGORY_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call WHATQUALIFIES.MY_PLAN_ID', () => {
    const action = { type: WHATQUALIFIES.MY_PLAN_ID, myPlanId: '53370' };
    const expected = {
      ...initialState,
      myPlanId: '53370',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLEAR_ERRORS', () => {
    const action = {
      type: WHATQUALIFIES.CLEAR_ERRORS,
    };
    const expected = {
      ...initialState,
      error: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});

describe('WhatQualifies SpotPromotions Reducers', () => {
  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY', () => {
    const action = { type: WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });


  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS', () => {
    const action = {
      type: WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_SUCCESS,
      data: [{
        planId: 'SP101',
        planTitle: 'Timberline Ultra HD 17',
        image: 'https://rewardsdevuse.blob.core.windows.net/rewards-static/SpotPromotion/be4c23dc-609b-4d3e-9f34-86d73f8d3772_shingles-feature-GAF (1).png',
        description: 'Description',
        products: [{
          startDate: '2017-10-01T00:00:00Z',
          endDate: '2017-12-31T00:00:00Z',
          byPercent: 0.0,
          productFamily: 'Timberline HD Reflector',
          dollarPerUnit: 2.0,
          uom: { code: 'SQ', description: 'Square' },
          planType: 'SpotPromotion',
          allowanceType: 'DOL',
        },
        {
          startDate: '2017-10-01T00:00:00Z',
          endDate: '2017-12-31T00:00:00Z',
          byPercent: 0.0,
          productFamily: 'Timberline HD®',
          dollarPerUnit: 2.0,
          uom: { code: 'SQ', description: 'Square' },
          planType: 'SpotPromotion',
          allowanceType: 'DOL',
        }],
      }],
    };
    const expected = {
      ...initialState,
      isLoading: false,
      spotPromotionsCategory: [{
        planId: 'SP101',
        planTitle: 'Timberline Ultra HD 17',
        image: 'https://rewardsdevuse.blob.core.windows.net/rewards-static/SpotPromotion/be4c23dc-609b-4d3e-9f34-86d73f8d3772_shingles-feature-GAF (1).png',
        description: 'Description',
        products: [{
          startDate: '2017-10-01T00:00:00Z',
          endDate: '2017-12-31T00:00:00Z',
          byPercent: 0.0,
          productFamily: 'Timberline HD Reflector',
          dollarPerUnit: 2.0,
          uom: { code: 'SQ', description: 'Square' },
          planType: 'SpotPromotion',
          allowanceType: 'DOL',
        },
        {
          startDate: '2017-10-01T00:00:00Z',
          endDate: '2017-12-31T00:00:00Z',
          byPercent: 0.0,
          productFamily: 'Timberline HD®',
          dollarPerUnit: 2.0,
          uom: { code: 'SQ', description: 'Square' },
          planType: 'SpotPromotion',
          allowanceType: 'DOL',
        }],
      }],
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });


  it('should call WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE', () => {
    const action = { type: WHATQUALIFIES.GET_SPOTPROMOTIONS_CATEGORY_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_PLAN_ID', () => {
    const action = { type: WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_PLAN_ID, planId: 'SP101' };
    const expected = {
      ...initialState,
      planId: 'SP101',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_SELECTED_YEAR', () => {
    const action = { type: WHATQUALIFIES.SPOTPROMOTIONS_DETAILS_SELECTED_YEAR, selectedYear: '2018' };
    const expected = {
      ...initialState,
      selectedYear: '2018',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call CLEAR_ERRORS', () => {
    const action = {
      type: WHATQUALIFIES.CLEAR_ERRORS,
    };
    const expected = {
      ...initialState,
      error: null,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
