import * as HEADER from '../actionTypes/Header';
import Reducer from './Header';

const initialState = {
  cartItemsCount: 0,
  cartItems: {
    orderTotal: 0,
    itemTotal: 0,
    feeTotal: 0,
  },
};

describe('Header Reducers', () => {
  it('should call GET_CART_COUNT', () => {
    const action = { type: HEADER.GET_CART_COUNT };
    const expected = {
      ...initialState,
      isLoading: true,
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CART_COUNT_SUCCESS', () => {
    const action = {
      type: HEADER.GET_CART_COUNT_SUCCESS,
      cart: {
        items: [{ name: 'MA123' }],
        orderTotal: 100,
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      cartItemsCount: 1,
      cartItems: {
        orderTotal: 100,
        feeTotal: 0,
        itemTotal: 0,
        items: [{ name: 'MA123' }],
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CART_COUNT_SUCCESS with order total of zero', () => {
    const action = {
      type: HEADER.GET_CART_COUNT_SUCCESS,
      cart: {
        items: [{ name: 'MA123' }],
      },
    };
    const expected = {
      ...initialState,
      isLoading: false,
      cartItemsCount: 1,
      cartItems: {
        orderTotal: 0,
        feeTotal: 0,
        itemTotal: 0,
        items: [{ name: 'MA123' }],
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CART_COUNT_SUCCESS with no cart object', () => {
    const action = {
      type: HEADER.GET_CART_COUNT_SUCCESS,
    };
    const expected = {
      ...initialState,
      isLoading: false,
      cartItemsCount: 0,
      cartItems: {
        orderTotal: 0,
        feeTotal: 0,
        itemTotal: 0,
      },
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call GET_CART_COUNT_FAILURE', () => {
    const action = { type: HEADER.GET_CART_COUNT_FAILURE, error: 'Internal Server Error' };
    const expected = {
      ...initialState,
      isLoading: false,
      error: 'Internal Server Error',
    };
    expect(Reducer(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined };
    const expected = {
      ...initialState,
    };
    expect(Reducer(undefined, action)).toEqual(expected);
  });
});
