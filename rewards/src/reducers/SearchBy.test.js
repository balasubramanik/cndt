import * as SEARCH from '../actionTypes/SearchBy';
import reducerFunction from './SearchBy';
import { searchByType } from '../constants/constants';

const initialState = {
  searByClaimStatus: {
    isSuggestionsLoading: false,
    searchText: '',
    suggestions: [],
    suggestionsError: '',
  },
};

const suggestionsResponse = {
  data: [
    {
      orderId: 1,
      type: 'Earned',
      points: '+ 250.00',
      user: 'J. Doe',
      date: 1534411158174,
    },
    {
      orderId: 2,
      type: 'Earned',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: 1534411158174,
    },
  ],
};

describe('Search page Reducers', () => {
  it('should call UPDATE_SEARCH_TEXT', () => {
    const action = {
      type: SEARCH.UPDATE_SEARCH_TEXT,
      searchText: 'sample',
      searchByName: 'CLAIM_STATUS_NUMBER',
    };
    if (action.searchByName === searchByType.ClaimStatusNumber) {
      initialState.searchText = action.searchText;
    }
    const expected = {
      ...initialState,
    };
    expect(reducerFunction(initialState, action)).toEqual(expected);
  });

  it('should call CLEAR_SEARCH_TEXT', () => {
    const action = {
      type: SEARCH.CLEAR_SEARCH_TEXT,
      searchByName: 'CLAIM_STATUS_NUMBER',
    };
    if (action.searchByName === searchByType.ClaimStatusNumber) {
      initialState.searchText = '';
    }
    const expected = {
      ...initialState,
    };
    expect(reducerFunction(initialState, action)).toEqual(expected);
  });

  it('should call GET_SUGGESTIONS', () => {
    const action = {
      type: SEARCH.GET_SUGGESTION,
      searchByName: 'CLAIM_STATUS_NUMBER',
    };
    if (action.searchByName === searchByType.ClaimStatusNumber) {
      initialState.isSuggestionsLoading = true;
    }
    const expected = {
      ...initialState,
    };
    expect(reducerFunction(initialState, action)).toEqual(expected);
  });

  it('should call SUGGESTIONS_SUCCESS', () => {
    const action = {
      type: SEARCH.GET_SUGGESTION_SUCCESS,
      searchByName: 'CLAIM_STATUS_NUMBER',
      suggestions: suggestionsResponse,
    };
    if (action.searchByName === searchByType.ClaimStatusNumber) {
      initialState.isSuggestionsLoading = false;
      initialState.suggestions = action.suggestions;
      initialState.suggestionsError = '';
    }
    const expected = {
      ...initialState,
    };
    expect(reducerFunction(initialState, action)).toEqual(expected);
  });

  it('should call SUGGESTIONS_FAILURE', () => {
    const action = {
      type: SEARCH.GET_SUGGESTION_FAILURE,
      searchByName: 'CLAIM_STATUS_NUMBER',
      error: '',
    };
    if (action.searchByName === searchByType.ClaimStatusNumbers) {
      initialState.isSuggestionsLoading = false;
      initialState.suggestions = [];
      initialState.suggestionsError = action.error;
    }
    const expected = {
      ...initialState,
    };
    expect(reducerFunction(initialState, action)).toEqual(expected);
  });

  it('should call Default', () => {
    const action = { type: undefined, error: '' };
    const expected = {
      ...initialState,
    };
    expect(reducerFunction(undefined, action)).toEqual(expected);
  });
});
