import * as CLAIMINVOICELIST from '../actionTypes/ClaimInvoiceDetails';

const initialState = {
  isInvoiceListLoading: false,
  claimInvoiceData: [],
  viewInvoiceError: '',
  isInvoiceDownloading: false,
  invoiceFileDetails: [],
  downloadInvoiceError: '',
  isInvoiceDownloadSuccess: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST:
      return {
        ...state,
        isInvoiceListLoading: true,
      };
    case CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_SUCCESS:
      return {
        ...state,
        isInvoiceListLoading: false,
        claimInvoiceData: (action.data) ? action.data : [],
        viewInvoiceError: '',
      };
    case CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_FAILURE:
      return {
        ...state,
        isInvoiceListLoading: false,
        viewInvoiceError: (action.error) ? action.error : '',
        claimInvoiceData: [],
      };
    case CLAIMINVOICELIST.GET_CLAIMS_INVOICE_LIST_CLEAR:
      return {
        ...state,
        isInvoiceListLoading: false,
        claimInvoiceData: [],
        viewInvoiceError: '',
        downloadInvoiceError: '',
      };
    case CLAIMINVOICELIST.DOWNLOAD_FILE:
      return {
        ...state,
        isInvoiceDownloading: true,
      };
    case CLAIMINVOICELIST.DOWNLOAD_FILE_SUCCESS:
      return {
        ...state,
        isInvoiceDownloading: false,
        invoiceFileDetails: action || {},
        downloadInvoiceError: '',
        isInvoiceDownloadSuccess: true,
      };
    case CLAIMINVOICELIST.DOWNLOAD_FILE_FAILURE:
      return {
        ...state,
        isInvoiceDownloading: false,
        downloadInvoiceError: (action.error) ? action.error : '',
        invoiceFileDetails: [],
      };
    case CLAIMINVOICELIST.DOWNLOAD_FILE_CLEAR:
      return {
        ...state,
        isInvoiceDownloadSuccess: false,
      };
    case CLAIMINVOICELIST.CLAIMS_INVOICE_CLEAR_ERROR:
      return {
        ...state,
        viewInvoiceError: null,
        downloadInvoiceError: null,
      };
    default:
      return state;
  }
};
