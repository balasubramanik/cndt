export const GET_CLAIMS_LIST = 'GET_CLAIMS_LIST';
export const GET_CLAIMS_LIST_SUCCESS = 'GET_CLAIMS_LIST_SUCCESS';
export const GET_CLAIMS_LIST_FAILURE = 'GET_CLAIMS_LIST_FAILURE';
export const GET_CLAIMS_LIST_CLEAR = 'GET_CLAIMS_LIST_CLEAR';
export const GET_EXPORT_CSV_SUCCESS = 'GET_EXPORT_CSV_SUCCESS';
export const CLAIM_LIST_CLEAR_ERROR = 'CLAIM_LIST_CLEAR_ERROR';
