import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import PropTypes from 'prop-types';
import { store } from './configureStore';
import { getProfileDetailsSuccess, getAdminProfileDetailsSuccess } from './actionCreators/MyProfile';
import envConfig from 'envConfig'; //eslint-disable-line
import HeaderContainer from './containers/header';
import EmsHeader from './components/emsHeader';
import Footer from './components/footer';
import Redirect from './components/redirect';
import RouteConstants from './constants/RouteConstants';
import Auth from './utils/Auth';
import { checkCZAuthentication, getImpersonatedBy, hasAdminEntry } from './utils/utils';
import { doPost } from './utils/fetchWrapper';
import AsyncComponent from './components/asyncComponent';
import TermsAndConditions from './components/termsandconditions';
import AccessRestrictPopup from './components/accessRestrictPopup';
import ScrollToTop from './components/scrollToTop';
import DetectDevices from './components/detectDevices';
import Authorization from './utils/Authorization';

// Create browser history to use in the Redux store
export const history = createBrowserHistory();

const DashboardContainer = () => import(/* webpackChunkName: "home" */ './containers/dashboard');
const WhatQualifies = () => import(/* webpackChunkName: "whatqualifies" */ './containers/whatQualifies');
const SpotPromotionDetails = () => import(/* webpackChunkName: "spotpromotiondetails" */ './containers/spotPromotionDetails');
const SubmitInvoice = () => import(/* webpackChunkName: "submitinvoice" */ './containers/submitInvoice');
const PointsHistory = () => import(/* webpackChunkName: "pointshistory" */ './containers/pointsHistory');
const OrderDetails = () => import(/* webpackChunkName: "orderdetails" */ './containers/orderDetails');
const ClaimDetails = () => import(/* webpackChunkName: "claimdetails" */ './containers/claimDetails');
const ClaimStatus = () => import(/* webpackChunkName: "claimstatus" */'./containers/claimStatus');
const ContactUs = () => import(/* webpackChunkName: "contactus" */ './containers/contactUs');
const RedeemPointsContainer = () => import(/* webpackChunkName: "redeempoints" */ './containers/redeemPoints');
const cartSummary = () => import(/* webpackChunkName: "cartsummary" */'./containers/cartSummary');
const SpotPromotion = () => import(/* webpackChunkName: "spotpromotion" */'./containers/spotPromotion');
const ContractorManagement = () => import(/* webpackChunkName: "contractormanagement" */'./containers/contractorManagement');
const PlanDetails = () => import(/* webpackChunkName: "Plandetails" */'./containers/planDetails');
const SpotPromotionManagement = () => import(/* webpackChunkName: "spotpromotionmanagement" */'./containers/SpotPromotionManagement');
const EmsContainer = () => import(/* webpackChunkName: "emscontainer" */'./containers/ems');
const PageNotFoundContainer = () => import(/* webpackChunkName: "pagenotfound" */'./containers/pageNotFound');

export const authentication = new Auth();
export const authorization = new Authorization();

// clear a existing rewards session if any
if (document.referrer !== '' && document.referrer.toLowerCase().indexOf('rewards') === -1) {
  authentication.clearAuthSession();
}

const handleAuth = ({ component, ...rest }) => (
  <Route
    {...rest}
    component={(props) => {
      if (checkCZAuthentication(props)) {
        return (
          <AsyncComponent
            moduleProvider={() => {
              authentication.SSOScope = authentication.nonAdminScope;
              if (hasAdminEntry()) {
                authentication.SSOScope = authentication.adminScope;
                authentication.redirectUri = `${envConfig.rewardsBaseUrl}${RouteConstants.ADMIN}`;
              }
              return authentication.hasSSOAuthenticated().then((response) => {
                if (response) {
                  if (authorization.hasRewardAccess || authorization.hasAdminAccess) {
                    rest.updateHeaderFooter();
                    return component().then((C) => C.default);
                  }
                  let url = envConfig.apiEndPoints.getProfileDetails;
                  let reqBody = {
                    isAdminRequest: false,
                  };
                  if (hasAdminEntry()) {
                    url = envConfig.apiEndPoints.getAdminProfile;
                    reqBody = {
                      isAdminRequest: true,
                    };
                    return makeAdminApiCall(url, reqBody, component, rest);
                  }
                  return makeNonAdminApiCall(url, reqBody, component, rest);
                }
                return <Redirect {...props} to={`${envConfig.cczBaseUrl}?redirectURL=${window.location.href}`} />;
              });
            }}
          />
        );
      } else if (!checkCZAuthentication(props)) {
        return <Redirect {...props} to={`${envConfig.cczBaseUrl}?redirectURL=${window.location.href}`} />;
      }
      return <AsyncComponent />;
    }
    }
  />
);

const SSOPromiseCall = (component, rest, isTermsAndCondition = false) => new Promise((resolve, reject) => {
  authentication.renewToken({ resolve, reject });
}).then(() => {
  if (!isTermsAndCondition) {
    rest.updateHeaderFooter();
    return component().then((C) => C.default);
  }
  return component;
});

const makeNonAdminApiCall = (url, reqBody, component, rest) => doPost(url, reqBody).then((res) => {
  if (res.data === null) {
    authorization.setContractorExist(false);
    return AccessRestrictPopup;
  }
  store.dispatch(getProfileDetailsSuccess(res.data));
  let scopeList = [];
  if (res.data.scopes && res.data.scopes.length) {
    scopeList = res.data.scopes.map((scope) => scope.name);
    authentication.nonAdminScope = `${authentication.SSOdefaultScope} ${scopeList.join(' ')}`;
    authentication.SSOScope = authentication.nonAdminScope;
  }
  if (res.data.isRewardsCertified) {
    authorization.setImpersonatorRoles(res.data);
    authorization.setRewardAccess(true);
    authorization.setUserRoles(res.data);
    if (authorization.userRolesAccess.length && authorization.checkAccess(rest.location.pathname)) {
      if (scopeList.length) {
        return SSOPromiseCall(component, rest);
      }
      rest.updateHeaderFooter();
      return component().then((C) => C.default);
    }
    return AccessRestrictPopup;
  }
  if (res.data.isTermsAndConditionsAccepted && !(res.data.isRewardsCertified)) {
    authorization.setRewardAccess(true);
    authorization.setImpersonatorRoles(res.data);
    authorization.setUserRoles(res.data);
    if (res.data && res.data.createdBy && res.data.createdBy === res.data.emailAddress) {
      authorization.setUserRoles('FULL_ACCESS');
    }
    if (authorization.userRolesAccess.length && authorization.checkAccess(rest.location.pathname)) {
      if (scopeList.length) {
        return SSOPromiseCall(component, rest);
      }
      rest.updateHeaderFooter();
      return component().then((C) => C.default);
    }
    return AccessRestrictPopup;
  }
  if (getImpersonatedBy()) {
    return AccessRestrictPopup;
  }
  return SSOPromiseCall(TermsAndConditions, rest, true);
}).catch((e) => {
  const { status = '' } = e;
  authorization.hasServiceError = true;
  if (status === 401 || status === 403) {
    authorization.hasServiceError = false;
  }
  return AccessRestrictPopup;
});


const makeAdminApiCall = (url, reqBody, component, rest) => {
  authorization.setAdminAccess(true);
  return doPost(url, reqBody).then((res) => {
    if (res.data === null) {
      return AccessRestrictPopup;
    }
    store.dispatch(getAdminProfileDetailsSuccess(res.data.admin));
    let scopeList = [];
    if (res.data.scopes && res.data.scopes.length) {
      scopeList = res.data.scopes.map((scope) => scope.name);
      authentication.adminScope = `${authentication.SSOdefaultScope} ${scopeList.join(' ')}`;
      authentication.SSOScope = authentication.adminScope;
    }
    authorization.setUserRoles(res.data.admin);
    if (authorization.userRolesAccess.length && authorization.checkAccess(rest.location.pathname)) {
      if (scopeList.length) {
        return SSOPromiseCall(component, rest);
      }
      rest.updateHeaderFooter();
      return component().then((C) => C.default);
    }
    return AccessRestrictPopup;
  }).catch((e) => {
    const { status = '' } = e;
    authorization.hasServiceError = true;
    if (status === 401 || status === 403) {
      authorization.hasServiceError = false;
    }
    return AccessRestrictPopup;
  });
};

handleAuth.propTypes = {
  component: PropTypes.object.isRequired,
};

const PublicRoute = ({ component, ...rest }) => {
  if (authentication.isAuthenticated() && rest.path.toString().toLowerCase() !== '/signup') {
    return handleAuth({ component, ...rest });
  }
  authentication.clearAuthSession();
  return (<Route
    {...rest}
    component={() => (
      <AsyncComponent
        moduleProvider={() => {
          if (rest.path === '*') {
            rest.updateAnonymousHeaderFooter();
          }
          return component().then((C) => C.default);
        }
        }
      />
    )}
  />
  );
};

PublicRoute.propTypes = {
  component: PropTypes.func.isRequired,
};

const AuthRoute = (props) => handleAuth(props);

class Routes extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      hasHeaderFooter: false,
      hasAnonymousHeaderFooter: false,
    };
  }

  updateHeaderFooter = () => {
    const { hasHeaderFooter } = this.state;
    if (!hasHeaderFooter) {
      this.setState({
        hasHeaderFooter: true,
      });
    }
  }

  updateAnonymousHeaderFooter = () => {
    const { hasAnonymousHeaderFooter } = this.state;
    if (!hasAnonymousHeaderFooter) {
      this.setState({
        hasAnonymousHeaderFooter: true,
      });
    }
  }

  render() {
    const { hasHeaderFooter, hasAnonymousHeaderFooter } = this.state;
    return (
      <Router history={history}>
        <DetectDevices >
          <ScrollToTop>
            {hasHeaderFooter && <HeaderContainer />}
            {hasAnonymousHeaderFooter && <EmsHeader />}
            <Switch>
              <AuthRoute path="/" exact updateHeaderFooter={this.updateHeaderFooter} component={DashboardContainer} />
              <AuthRoute path={RouteConstants.WHAT_QUALIFIES} updateHeaderFooter={this.updateHeaderFooter} component={WhatQualifies} />
              <AuthRoute path={RouteConstants.CARTSUMMARY} updateHeaderFooter={this.updateHeaderFooter} component={cartSummary} />
              <AuthRoute path={RouteConstants.SPOTPROMOTIONS_DETAILS} updateHeaderFooter={this.updateHeaderFooter} component={SpotPromotionDetails} />
              <AuthRoute path={RouteConstants.POINTS_HISTORY} updateHeaderFooter={this.updateHeaderFooter} component={PointsHistory} />
              <AuthRoute path={RouteConstants.ORDER_DETAILS} updateHeaderFooter={this.updateHeaderFooter} component={OrderDetails} />
              <AuthRoute path={RouteConstants.CLAIM_STATUS} updateHeaderFooter={this.updateHeaderFooter} component={ClaimStatus} />
              <AuthRoute path={RouteConstants.SUBMIT_INVOICE} updateHeaderFooter={this.updateHeaderFooter} component={SubmitInvoice} />
              <AuthRoute path={RouteConstants.CLAIM_DETAILS} updateHeaderFooter={this.updateHeaderFooter} component={ClaimDetails} />
              <AuthRoute path={RouteConstants.CONTACTUS} updateHeaderFooter={this.updateHeaderFooter} component={ContactUs} />
              <AuthRoute path={RouteConstants.REDEEM_POINTS} updateHeaderFooter={this.updateHeaderFooter} component={RedeemPointsContainer} />
              <AuthRoute path={RouteConstants.ADMIN} exact updateHeaderFooter={this.updateHeaderFooter} component={ContractorManagement} />
              <AuthRoute path={RouteConstants.CONTRACTORMANAGEMENT} updateHeaderFooter={this.updateHeaderFooter} component={ContractorManagement} />
              <AuthRoute path={RouteConstants.PLAN_DETAILS} updateHeaderFooter={this.updateHeaderFooter} component={PlanDetails} />
              <AuthRoute path={RouteConstants.SPOTPROMOTION} updateHeaderFooter={this.updateHeaderFooter} component={SpotPromotion} />
              <AuthRoute path={RouteConstants.NEW_SPOT_PROMOTION} updateHeaderFooter={this.updateHeaderFooter} component={SpotPromotionManagement} />
              <AuthRoute path={RouteConstants.EDIT_SPOTPROMOTION} updateHeaderFooter={this.updateHeaderFooter} component={SpotPromotionManagement} />
              <PublicRoute path={RouteConstants.SIGNUP} component={EmsContainer} />
              <PublicRoute path="*" updateAnonymousHeaderFooter={this.updateAnonymousHeaderFooter} updateHeaderFooter={this.updateHeaderFooter} component={PageNotFoundContainer} />
            </Switch>
            {(hasHeaderFooter || hasAnonymousHeaderFooter) && <Footer />}
          </ScrollToTop>
        </DetectDevices>
      </Router >
    );
  }
}

export default Routes;
