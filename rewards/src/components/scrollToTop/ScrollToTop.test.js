import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { ScrollToTop } from '../../components/scrollToTop';
configure({ adapter: new Adapter() });

describe('ScrollToTop', () => {
  window.scrollTo = jest.fn();
  const tree = shallow(<ScrollToTop location="google.com">
    <div>1</div>
  </ScrollToTop>);
  it('should be defined', () => {
    expect(ScrollToTop).toBeDefined();
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should scroll to top', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      location: 'yahoo.com',
    });
    expect(window.scrollTo).toBeCalled();
  });
  it('should not scroll to top when location is not updated', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      location: 'google.com',
    });
    expect(window.scrollTo).toBeCalled();
  });
});
