import React from 'react';
import { Col } from 'react-bootstrap';
import { Field } from 'redux-form';
import TextInput from '../textInput';
import { name } from './constants';
import { restrictSpecialChar } from '../../utils/utils';

/** @description Functional Component to render the Name */
const Name = () => {
  const firstNameLabel = `${name.FIRSTNAME}<sup>*</sup>`;
  const lastNameLabel = `${name.LASTNAME}<sup>*</sup>`;
  return (
    <React.Fragment>
      <Col md={6}>
        <Field
          className="gaf-form"
          name="firstName"
          id="firstName"
          type="text"
          component={TextInput}
          label={firstNameLabel}
          normalize={restrictSpecialChar}
          maxLength={20}
        />
      </Col>
      <Col md={6}>
        <Field
          className="gaf-form"
          name="lastName"
          id="lastName"
          type="text"
          component={TextInput}
          label={lastNameLabel}
          normalize={restrictSpecialChar}
          maxLength={20}
        />
      </Col>
    </React.Fragment>
  );
};

export default Name;
