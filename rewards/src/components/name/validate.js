import { errorMessages } from '../../containers/ems/Constants';

const validate = (values) => {
  const errors = {};
  const firstName = values.firstName ? values.firstName.trim() : '';
  const lastName = values.lastName ? values.lastName.trim() : '';
  if (!firstName) {
    errors.firstName = errorMessages.REQUIRED_FIELD;
  }
  if (!lastName) {
    errors.lastName = errorMessages.REQUIRED_FIELD;
  }
  return errors;
};

export { validate };
