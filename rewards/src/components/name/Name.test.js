import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Name from '../../components/name';


configure({ adapter: new Adapter() });

describe('Name with input prop', () => {
  const tree = shallow(<Name />);
  it('should be defined', () => {
    expect(Name).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render firstName', () => {
    expect(tree.find('#firstName').length).toBe(1);
  });

  it('should render firstName', () => {
    expect(tree.find('#firstName').length).toBe(1);
  });

  it('should render lastName', () => {
    expect(tree.find('#lastName').length).toBe(1);
  });
});
