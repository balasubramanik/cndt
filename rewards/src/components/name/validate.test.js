import { validate } from './validate';
import { errorMessages } from '../../containers/ems/Constants';

describe('validate', () => {
  it('should be equal to invalid firstname', () => {
    const errors = validate({});
    expect(errors.firstName).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid lastname', () => {
    const errors = validate({});
    expect(errors.lastName).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to valid firstname', () => {
    const errors = validate({ firstName: 'john' });
    expect(errors.firstName).toBe(undefined);
  });


  it('should be equal to valid lastname', () => {
    const errors = validate({ lastName: 'doe' });
    expect(errors.lastName).toBe(undefined);
  });
});
