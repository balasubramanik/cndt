import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TypeAhead from '../../components/TypeAHead';

configure({ adapter: new Adapter() });

const onChange = jest.fn();

const list = [{
  claimId: 'G00001012',
  firstName: 'Jude',
  lastName: 'Law',
  claimStatus: 'completed',
  submittedOn: '2018-09-28T20:09:35.3475807+05:30',
  modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
  pointsEarned: '2000',
},
{
  claimId: 'G00001100',
  firstName: 'Calvin',
  lastName: 'Klein',
  claimStatus: 'pending',
  submittedOn: '2018-09-28T20:09:35.3475807+05:30',
  modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
  pointsEarned: '5000',
},
{
  claimId: 'G00001224',
  firstName: 'Shane',
  lastName: 'Warne',
  claimStatus: 'processed',
  submittedOn: '2018-09-15T19:06:34.868719+05:30',
  modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
  pointsEarned: '3000',
}];

describe('TypeAhead with empty list', () => {
  const wrapper = shallow(<TypeAhead
    onChange={onChange}
  />);

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('should render error message', () => {
    expect(wrapper.find('.GAF-auto-suggesion-noresult').length).toBe(1);
  });
});

describe('TypeAhead with list of elements', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const wrapper = shallow(<TypeAhead
    list={list}
    onChange={onChange}
    isLoading={false}
    searchByKey="abc"
  />);

  it('should render Type Ahead Result container', () => {
    expect(wrapper.find('.GAF-auto-suggesion').length).toBe(1);
  });

  it('should render Type Ahead Suggestions', () => {
    expect(wrapper.find('.GAF-auto-suggesion-noresult').length).toBe(0);
  });
  it('should call onclick handler', () => {
    const simulateChange = wrapper.find('a').at(0).prop('onClick');
    simulateChange(event, onChange, {
      claimId: 'G00001012',
      firstName: 'Jude',
      lastName: 'Law',
      claimStatus: 'completed',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
      pointsEarned: '2000',
    });
    expect(onChange).toBeCalled();
  });
});
