import React from 'react';
import PropTypes from 'prop-types';
import { noResultsData } from './constants';

const selectOptionHandler = (event, callback, item) => {
  if (item) {
    callback(event, item);
    return;
  }
  callback(event);
};

const renderSuggestions = (list, searchByKey, callback) => (
  list && list.map((item, i) => <li key={i.toString()}><a href="/" onClick={(event) => selectOptionHandler(event, callback, item)}>{item}</a></li>)
);

const renderTypeHead = (list, searchByKey, callback) => (
  <React.Fragment>
    <ul className="aletheia-auto-result">
      {renderSuggestions(list, searchByKey, callback)}
    </ul>
  </React.Fragment>
);


const renderTypeHeadList = (list, isLoading, searchByKey, onChange) => (
  list && list.length > 0 ?
    <section className="GAF-auto-suggesion">
      {
        <section>{renderTypeHead(list, searchByKey, onChange)}</section>
      }
    </section> :
    <span className="GAF-auto-suggesion-noresult text-danger">
      {/* <FormattedMessage id="app.mypage.search.noresultsfound" /> */}
      { noResultsData.noResult }
    </span>
);

/** @description Class component to render the Type ahead
 */
const TypeAhead = ({
  list,
  isLoading,
  searchByKey,
  onChange,
}) => (
  <React.Fragment>
    {!isLoading && renderTypeHeadList(list, isLoading, searchByKey, onChange)}
  </React.Fragment>
);

TypeAhead.propTypes = {
  onChange: PropTypes.func.isRequired,
  list: PropTypes.array,
  isLoading: PropTypes.bool,
  searchByKey: PropTypes.string,
};

TypeAhead.defaultProps = {
  list: [],
};

export default TypeAhead;
