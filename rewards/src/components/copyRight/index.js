import React from 'react';
import moment from 'moment';
import { copyRight } from './constants';

const currentYear = moment().format('YYYY');

const CopyRight = () => (
  <p id="gaf-footer-copyright-text">&copy; {copyRight.COPYRIGHT} {currentYear} {copyRight.GAFMATCORP}</p>
);
export default CopyRight;
