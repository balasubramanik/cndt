import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CopyRight from '../../components/copyRight';


configure({ adapter: new Adapter() });

describe('CopyRight', () => {
  const tree = shallow(<CopyRight />);

  it('should be defined', () => {
    expect(CopyRight).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
