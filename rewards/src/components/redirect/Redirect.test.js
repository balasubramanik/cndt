import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Redirect from '../../components/redirect';

configure({ adapter: new Adapter() });

describe('Redirect', () => {
  const tree = shallow(<Redirect
    to="cczintmnr.gaf.com?redirectURL=https://www.google.com/"
  />);
  it('should be defined', () => {
    expect(Redirect).toBeDefined();
  });
  it('should render Loader', () => {
    expect(tree.find('Loader').length).toBe(1);
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
