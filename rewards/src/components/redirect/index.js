import React, { Component } from 'react';
import Loader from '../loader';

export class Redirect extends Component {
  constructor(props) {
    super();
    this.state = { ...props };
  }
  componentWillMount() {
    window.location.href = this.state.to;
  }
  render() {
    return (<Loader />);
  }
}

export default Redirect;
