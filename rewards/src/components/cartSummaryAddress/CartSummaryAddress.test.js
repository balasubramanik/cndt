import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryAddress from '../../components/cartSummaryAddress';

configure({ adapter: new Adapter() });

describe('Radio with Available Address', () => {
  const selectShippingAddress = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<CartSummaryAddress
    addressData={[{
      address1: 'test1', address2: '62nd Forest Hills, NY 11375', city: 'abc', stateOrProvince: 'XY', zipOrPostal: 'PO Box 5555', country: 'USA',
    },
    {
      address1: 'test2', address2: 'Beverley Hills, NY 11375', city: 'cba', stateOrProvince: 'YX', zipOrPostal: 'PO Box 7777', country: 'USA',
    }]}
    selectShippingAddress={selectShippingAddress}
    disableRadio
  />);

  it('should be defined', () => {
    expect(CartSummaryAddress).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render error message', () => {
    expect(tree.find('.gaf-error-message').length).toBe(0);
  });

  it('should render Address Description', () => {
    expect(tree.find('.update-address').length).toBe(1);
  });

  it('should call Change handler', () => {
    const simulateChange = tree.find('CartSummaryEachAddress').at(0).prop('handleSelect');
    simulateChange();
    expect(selectShippingAddress).toBeCalled();
  });
  it('should call Update address details form', () => {
    const simulateChange = tree.find('a').prop('onClick');
    simulateChange(event);
    expect(window.location.href).toBe('http://localhost/');
  });
});

describe('Radio with Available Address', () => {
  const selectShippingAddress = jest.fn();
  const tree = mount(<CartSummaryAddress
    addressData={[{
      address1: 'test1', address2: '62nd Forest Hills, NY 11375', city: 'abc', stateOrProvince: 'XY', zipOrPostal: 'PO Box 5555', country: 'USA',
    },
    {
      address1: 'test2', address2: 'Beverley Hills, NY 11375', city: 'cba', stateOrProvince: 'YX', zipOrPostal: 'PO Box 7777', country: 'USA',
    }]}
    selectShippingAddress={selectShippingAddress}
    disableRadio
  />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('checkradio should become true', () => {
    tree.setState({ checkRadio: true });
    expect(tree.find('.gaf-error-message').length).toBe(1);
  });
});
