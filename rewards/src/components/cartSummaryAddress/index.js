import React from 'react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import envConfig from 'envConfig'; //eslint-disable-line
import { cartSummaryAddresstext } from './constants';
import CartSummaryEachAddress from '../cartSummaryEachAddress';
import { checkPOAddress } from '../../utils/utils';

class CartSummaryAddress extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: this.props.isCheck,
      checkRadio: false,
    };
  }

  /**
   * @param index array index of the address selected by user
   */
  onSelectAddress = (i, item) => {
    this.setState({
      checked: i,
    });
    this.props.selectShippingAddress(item);
  };

  summaryAddress() {
    const { checkRadio } = this.state;
    const { addressData, disableRadio } = this.props;

    return addressData.map((item, i) => {
      const address = `${item.address1} ${item.address2} ${item.city} ${item.stateOrProvince} ${item.zipOrPostal} ${item.country}`;
      const poCheck = checkPOAddress(address);
      if (disableRadio && poCheck && !checkRadio) {
        this.setState({
          checkRadio: true,
        });
      }
      return (<CartSummaryEachAddress
        key={i.toString()}
        index={i}
        isChecked={this.state.checked === i}
        handleSelect={() => this.onSelectAddress(i, item)}
        disableValue={poCheck && disableRadio}
        {...item}
      />);
    });
  }

  updateAddressDetailsForm(e) {
    e.preventDefault();
    const links = {
      cczMyAccountUrl: '/compinfo/home',
    };
    this.props.updateShippingAddress(cartSummaryAddresstext.UPDATE_ADDRESS);
    window.location.href = `${envConfig.cczBaseUrl}${links.cczMyAccountUrl}`;
  }

  render() {
    const { checkRadio } = this.state;
    return (
      <Col md={8}>
        {checkRadio &&
          <div className="gaf-error-message">
            {cartSummaryAddresstext.PO_ERROR_MESSAGE}
          </div>
        }
        <p className="confirm-shopping" id="shopping-Address-title">{cartSummaryAddresstext.TITLE}</p>
        <div className="addresslist-wrapper">{this.summaryAddress()}</div>
        <Row className="show-grid">
          <Col md={12}>
            <div className="update-address">
              <p><a id="updat-address-link" href="/" onClick={(e) => this.updateAddressDetailsForm(e)}>{cartSummaryAddresstext.UPDATELINK}</a></p>
              <span><i id="info-icon" className="icon-info" role="button"></i></span>
              <span id="shopping-address-tool-tip">{cartSummaryAddresstext.TOOLTIP_TEXT}</span>
            </div>
          </Col>
        </Row>
      </Col>
    );
  }
}

CartSummaryAddress.propTypes = {
  isCheck: PropTypes.number,
  addressData: PropTypes.array,
  selectShippingAddress: PropTypes.func,
  disableRadio: PropTypes.bool,
  updateShippingAddress: PropTypes.func,
};

export default CartSummaryAddress;

