const cartSummaryAddresstext = {
  TITLE: 'Confirm Shipping Address',
  UPDATELINK: 'Update your address',
  TOOLTIP_TEXT: 'Please note that changing the address will take about 48 hours to get updated.',
  PO_ERROR_MESSAGE: 'Sorry, Visa Reloadable Cards cannot be shipped to a PO Box.',
  UPDATE_ADDRESS: 'Update Address',
};

export { cartSummaryAddresstext };
