import React from 'react';
import PropTypes from 'prop-types';
import Tables from '../tables';
import { promotionList } from './constants';
import { history } from '../../routes';
import { formatDateUTC } from '../../utils/utils';
import ToolTip from '../toolTip';
import RouteConstants from '../../constants/RouteConstants';

class SpotPromotionList extends React.Component {
  /* Redirect to Edit Promotion Page */
  redirectToEditPromotion = (e, row) => {
    e.preventDefault();
    history.push({
      pathname: RouteConstants.EDIT_SPOTPROMOTION.replace(':promoId', row.id),
      state: { row },
    });
  }

  formatData = (records) => {
    if (!records) return [];
    return records.map((record) => (
      {
        ...record,
        title: <ToolTip id={record.title} text={record.title}><span role="button">{record.title}</span></ToolTip>,
        startDate: formatDateUTC(record.startDate),
        endDate: formatDateUTC(record.endDate),
        modifiedOn: formatDateUTC(record.modifiedOn),
        details: <button className="btn gaf-btn-secondary gaf-btn-sm" onClick={(event) => this.redirectToEditPromotion(event, record)}>View/Edit</button>,
      }
    ));
  };

  render() {
    const { data } = this.props;
    const { loading, defaultSort } = this.props;
    return (
      <div className="spot-promotion-list">
        <Tables
          config={promotionList.headers}
          onSortingClick={(key, isAsc) => this.props.sortingData(key, isAsc)}
          data={this.formatData(data)}
          isLoading={loading}
          sortByKey={defaultSort}
        />
      </div>
    );
  }
}


SpotPromotionList.propTypes = {
  data: PropTypes.array,
  loading: PropTypes.bool,
  sortingData: PropTypes.func,
  defaultSort: PropTypes.string,
};

export default SpotPromotionList;
