const promotionList = {
  headers: [
    { title: 'Promo Title', key: 'title', sort: true },
    { title: 'Start Date ', key: 'startDate', sort: true },
    { title: 'End Date ', key: 'endDate', sort: true },
    { title: 'Updated Date', key: 'modifiedOn', sort: true },
    { title: 'Status', key: 'status', sort: true },
    { title: 'Details', key: 'details' },
  ],
};
export { promotionList };
