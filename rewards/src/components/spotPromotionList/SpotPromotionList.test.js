import React from 'react';
import test from 'tape';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SpotPromotionList from '../../components/spotPromotionList';
import { history } from '../../routes';

history.push = jest.fn();
configure({ adapter: new Adapter() });
const t = test('test utils', (b) => b);
describe('SpotPromotionList with props', () => {
  const sortingData = jest.fn();
  const tree = shallow(<SpotPromotionList
    data={[{
      id: 1,
      title: 'PromoTitle1',
      startDate: '07/01/2018',
      endDate: '31/09/2018',
    },
    {
      id: 2,
      title: 'PromoTitle2',
      startDate: '04/01/2018',
      endDate: '31/08/2018',
    }]}
    sortingData={sortingData}
  />);


  it('should be defined', () => {
    expect(SpotPromotionList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render SpotPromotionList Component', () => {
    expect(tree.find('SpotPromotionList').length).toBe(0);
  });

  it('should render Tables Component', () => {
    expect(tree.find('Tables').length).toBe(1);
  });

  it('should render viewEdit button prop', () => {
    expect(tree.find('.btn gaf-btn-secondary').length).toBe(0);
  });

  it('should call sortBy for descending', () => {
    const simulateClick = tree.find('Tables').prop('onSortingClick');
    simulateClick('id', false);
    expect(sortingData).toBeCalled();
  });

  it('should call sortBy for ascending', () => {
    const simulateClick = tree.find('Tables').prop('onSortingClick');
    simulateClick('id', true);
    expect(sortingData).toBeCalled();
  });
});
describe('SpotPromotionList without props', () => {
  const tree = shallow(<SpotPromotionList />);

  it('should be defined', () => {
    expect(SpotPromotionList).toBeDefined();
  });

  it('should not render Tables Component', () => {
    expect(tree.find('tr').length).toBe(0);
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render SpotPromotionList Component', () => {
    expect(tree.find('SpotPromotionList').length).toBe(0);
  });

  it('should render view-edit button prop', () => {
    expect(tree.find('.btn gaf-btn-secondary').length).toBe(0);
  });
});

describe('SpotPromotionList with props', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const sortingData = jest.fn();
  const tree = mount(<SpotPromotionList
    data={[{
      id: 1,
      title: 'PromoTitle1',
      startDate: '07/01/2018',
      endDate: '31/09/2018',
    },
    {
      id: 2,
      title: 'PromoTitle2',
      startDate: '04/01/2018',
      endDate: '31/08/2018',
    }]}
    sortingData={sortingData}
  />);
  it('should be defined', () => {
    expect(SpotPromotionList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render SpotPromotionList Component', () => {
    expect(tree.find('SpotPromotionList').length).toBe(1);
  });

  it('should render viewEdit button prop', () => {
    expect(tree.find('.btn gaf-btn-secondary').length).toBe(0);
  });

  it('should redirect to edit/view SpotPromotion when clicking the link', () => {
    const simulateClick = tree.find('button').at(0).prop('onClick');
    simulateClick(event);
    expect(history.push).toBeCalled();
  });
});
