import React, { PureComponent } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const TIMEOUT = 2000;

/** @description Functional component to render the Toast message */
class Toast extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: true,
    };
  }

  /** @description React Life cycle method
   * it will invoke, when component is mounted.
   */
  componentDidMount() {
    setTimeout(() => {
      this.updateState(false);
    }, TIMEOUT);
  }

  /** @description funtion to update state value based on key
   * @param {boolean} isVisible - flag to denotes the toast message whether it is visible or not
   */
  updateState = (isVisible) => this.setState({ isVisible });

  render() {
    const { isVisible } = this.state;
    const { icon, text } = this.props;
    const toastClass = classNames('toast-message', { 'toast-fade': !isVisible });
    return (
      <div className={toastClass}>
        <span className={icon}></span>
        <p className="toast-text">{text}</p>
      </div>
    );
  }
}

/** PropTypes:
 * icon - string - classname for icon
 * text - string - toast message
 */
Toast.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Toast;
