import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Toast from '../../components/toast';


configure({ adapter: new Adapter() });
jest.useFakeTimers();
describe('Toast', () => {
  const tree = shallow(<Toast
    icon="gaf-success"
    text="Your email address is Verified"
  />);

  it('should be defined', () => {
    expect(Toast).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Toast message', () => {
    expect(tree.find('.toast-message').length).toBe(1);
  });

  it('should render icon', () => {
    expect(tree.find('.gaf-success').length).toBe(1);
  });

  it('should add fade class for Toast message after 3sec', () => {
    jest.runAllTimers();
    expect(tree.find('.toast-fade').length).toBe(1);
  });
});
