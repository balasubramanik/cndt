import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import DetectDevices from '../../components/detectDevices';


configure({ adapter: new Adapter() });

describe('DetectDevices for Windows', () => {
  window.navigator.__defineGetter__('appVersion', () => 'Win'); //eslint-disable-line
  const tree = shallow(<DetectDevices><div id="child-ele">Sample</div></DetectDevices>);

  it('should be defined', () => {
    expect(DetectDevices).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should add class for windows', () => {
    expect(tree.find('.gaf-device-windows').length).toBe(1);
  });

  it('should not add class for mac', () => {
    expect(tree.find('.gaf-device-ios').length).toBe(0);
  });

  it('should render child element', () => {
    expect(tree.find('#child-ele').length).toBe(1);
  });
});

describe('DetectDevices for Mac', () => {
  window.navigator.__defineGetter__('appVersion', () => 'Mac'); //eslint-disable-line
  const tree = shallow(<DetectDevices><div id="child-ele">Sample</div></DetectDevices>);

  it('should be defined', () => {
    expect(DetectDevices).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should add class for mac', () => {
    expect(tree.find('.gaf-device-ios').length).toBe(1);
  });

  it('should not add class for windows', () => {
    expect(tree.find('.gaf-device-windows').length).toBe(0);
  });

  it('should render child element', () => {
    expect(tree.find('#child-ele').length).toBe(1);
  });
});
