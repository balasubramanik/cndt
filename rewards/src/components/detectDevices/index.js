import React from 'react';
import PropTypes from 'prop-types';
import { getImpersonatedBy } from '../../utils/utils';

export default class DetectDevices extends React.Component {
  constructor(props) {
    super(props);
    let browser = '';
    const isWindows = navigator.appVersion.indexOf('Win') !== -1;
    const isIos = navigator.appVersion.indexOf('Mac') !== -1;
    if (isWindows) {
      browser = 'gaf-device-windows';
    } else if (isIos) {
      browser = 'gaf-device-ios';
    }
    this.state = {
      browser,
    };
  }

  render() {
    const { browser } = this.state;
    const showImpersonation = getImpersonatedBy() ? 'impers-topspace' : null;
    return (
      <div className={`${browser} ${showImpersonation}`}>
        {this.props.children}
      </div>);
  }
}

DetectDevices.propTypes = {
  children: PropTypes.object.isRequired,
};
