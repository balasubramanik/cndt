import React from 'react';
import PropTypes from 'prop-types';
import ToolTip from '../toolTip';

/** @description Functional component to render the Radio button group field */
const Radio = (props) => {
  const {
    input, options, className, id, meta, showError,
  } = props;
  const { touched, error } = meta;
  return (
    <React.Fragment>
      {showError &&
        touched &&
        error &&
        <p className="error-message col-xs-12">{error}</p>}
      {
        options && options.map((option, i) => {
          const identifier = `${id}${option.value}`;
          return (
            <div className={className} key={i.toString()}>
              <input
                {...input}
                id={identifier}
                type="radio"
                value={option.value}
                checked={option.value === input.value}
              />
              <label htmlFor={identifier}>
                {option.title && <span dangerouslySetInnerHTML={{ __html: option.title }} />}
                {option.tooltipText &&
                  <ToolTip id={`tooltip-${identifier}`} text={option.tooltipText}>
                    <span className="icon-question" role="button" />
                  </ToolTip>}
              </label>
            </div>
          );
        })
      }
    </React.Fragment>
  );
};

/** PropTypes:
 * className - string - customize for radio button
 * options - array - contains the radio/checkbox configurations
 * input - object - property comes from redux-form
 * id - string - unique identifier for radio
 * meta - object - property comes from redux-form
 * showError - boolean - flag for show/hide error message
 */
Radio.propTypes = {
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    tooltipText: PropTypes.string,
  })),
  id: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object,
  showError: PropTypes.bool,
};

Radio.defaultProps = {
  id: '',
  input: { value: '', onChange: () => { } },
  meta: {},
};

export default Radio;
