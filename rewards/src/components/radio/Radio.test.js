import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Radio from '../../components/radio';


configure({ adapter: new Adapter() });

describe('Radio with options', () => {
  const tree = shallow(<Radio
    input={{ name: 'gaf-radio' }}
    meta={{ touched: true, error: true }}
    className="gaf-form-control"
    showError={true}
    options={[{ title: 'item1', value: 'item1', tooltipText: 'sample tooltip' }, { title: 'item2', value: 'item2' }]}
  />);

  it('should be defined', () => {
    expect(Radio).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label element', () => {
    expect(tree.find('label').length).not.toBe(0);
  });

  it('should render input element', () => {
    expect(tree.find('input[type="radio"]').length).not.toBe(0);
  });

  it('should render Tooltip element', () => {
    expect(tree.find('ToolTip').length).toBe(1);
  });

  it('should render Error Message', () => {
    expect(tree.find('.error-message').length).toBe(1);
  });
});


describe('Radio without options', () => {
  const tree = shallow(<Radio
    input={{ name: 'gaf-radio' }}
    className="gaf-form-control"
  />);

  it('should be defined', () => {
    expect(Radio).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render label element', () => {
    expect(tree.find('label').length).toBe(0);
  });

  it('should not render input element', () => {
    expect(tree.find('input[type="radio"]').length).toBe(0);
  });
});
