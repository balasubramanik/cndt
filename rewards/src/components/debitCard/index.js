
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import ImageHOC from '../imageHOC';
import Dropdown from '../dropdown';
import TextInput from '../textInput';
import { CardDetails } from '../cardDetails';
import { constants } from './constants';
import { validate } from './validate';
import {
  getFeeDetails, getDebitCardHolderName,
  getProductName, getVariantId,
  getProductdetails, getAddtionalFee,
  getDebitCardInfo, getDescription,
  getVariants,
} from './selectors';
import { roundToTwoDecimal, formatUSCurrency, formatCurrency, convertStringToNumber, calculateFee } from '../../utils/utils';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import * as redeemActionsCreators from '../../actionCreators/RedeemPoints';

const initialFee = formatCurrency(roundToTwoDecimal());

export class DebitCard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fee: initialFee,
    };
  }

  /** @description React Life cycle method
  * it will invoke, when component is updated.
  */
  componentDidUpdate(prevProps) {
    const {
      isShowPopup, category, reset, gtmActions, formValues,
    } = this.props;
    const { redeemableAmount } = formValues;
    if (prevProps.isShowPopup !== isShowPopup
      && isShowPopup
      && category === constants.TYPE) {
      gtmActions.gtmDebitCardSuccess(convertStringToNumber(redeemableAmount));
      reset();
      this.updateState('fee', initialFee);
    }
  }

  /** @description React Life cycle method
  * it will invoke, when component is unmounted.
  */
  componentWillUnmount() {
    this.props.redeemActions.resetAllGiftItems();
  }

  /** @description Function to update state value
   * @param {string} key - state key
   * @param {string} value - state value
   */
  updateState = (key, value) => this.setState({ [key]: value });

  /** @description Callback function for change debit amount
   * @param {object} event - triggered event(change)
   */
  debitAmountChangeHandler = (event) => {
    const { value } = event.target;
    const { feeDetails, additionalFee } = this.props;
    const { feeType, feeValue } = feeDetails;
    const val = convertStringToNumber(value);
    const fee = calculateFee(val, feeValue, feeType, additionalFee);
    const fomattedFee = formatCurrency(fee);
    this.setState({ fee: fomattedFee });
  }

  /** @description Callback function for submit debit card form
   * @param {object} values - form values
   */
  submitHandler = (values) => {
    const { fee } = this.state;
    const {
      product, feeDetails, formValues, variantId,
    } = this.props;
    const { cardHolderName } = formValues;
    const isReadOnly = Boolean(this.props.cardHolderName);
    const val = {
      ...values,
      productId: product.id,
      productName: product.name,
      productType: feeDetails.cardType,
      feeType: feeDetails.feeType,
      feeValue: feeDetails.feeValue,
      cardTypeDesc: feeDetails.typeDescription,
      feeTypeDesc: feeDetails.feeDescription,
      productImage: feeDetails.imageIcon,
      min: feeDetails.min,
      max: feeDetails.max,
      cardValue: convertStringToNumber(values.redeemableAmount),
      quantity: 1,
      fee: convertStringToNumber(fee),
      debitCardUserName: cardHolderName,
      debitCardIssuedTo: this.props.cardHolderName,
      isReloadable: isReadOnly,
      variantId,
      minMaxType: feeDetails.minmaxtype,
    };
    this.props.onSubmit(constants.TYPE, constants.DEBIT_CARD_INFO, val);
  }

  /** @description Callback function for view button */
  viewHandler = (isViewMore) => {
    if (isViewMore && this.debitCard) {
      const top = this.debitCard.offsetTop;
      window.scrollTo(0, top);
    }
  }

  /** @description function to format users for dropdown */
  formatUserName = () => {
    const { users } = this.props;
    if (users) {
      return users.map((user) => {
        const { firstName, lastName, emailAddress } = user;
        const name = `${firstName} ${lastName}`;
        return {
          label: name,
          value: emailAddress,
        };
      });
    }
    return [];
  }

  /** @description function to render the card name field */
  renderCardNameField = () => {
    const { cardHolderName } = this.props;
    const isReadOnly = Boolean(cardHolderName);
    return (
      <div className="form-group gaf-form nameon-card">
        <label htmlFor="cardHolderName">{constants.NAME_ON_CARD}</label>
        <Field
          name="cardHolderName"
          id="cardHolderName"
          component={Dropdown}
          menuItems={this.formatUserName()}
          placeholder="Select"
          readOnly={isReadOnly}
        />
      </div>
    );
  }

  /** @description function to render the redeem amount field */
  renderRedeemAmountField = () => {
    const { fee } = this.state;
    const { formValues } = this.props;
    const { redeemableAmount } = formValues;
    return (
      <div className="form-group gaf-form">
        <Field
          className="gaf-form"
          name="redeemableAmount"
          id="redeemableAmount"
          type="text"
          component={TextInput}
          normalize={formatUSCurrency}
          onChange={this.debitAmountChangeHandler}
          value={redeemableAmount}
          maxLength={10}
          placeholder="Type Amount"
        />
        <small>{constants.ADDITIONAL_FEE_TEXT} {fee}</small>
      </div>
    );
  }

  /** @description function to render the debit card form */
  renderDebitCardForm = () => {
    const { handleSubmit, errors } = this.props;
    const btnClass = classNames('btn gaf-btn-primary', { 'btn-disabled': errors });
    return (
      <form className="form-gaf-left" name="debitCardForm" onSubmit={handleSubmit(this.submitHandler)}>
        {this.renderCardNameField()}
        {this.renderRedeemAmountField()}
        <div className="addto-cart-btn">
          <Button id="debitcard-add-to-cart" type="submit" className={btnClass}>{constants.ADD_TO_CART}</Button>
        </div>
      </form>
    );
  }

  /** @description function to render the debit card details */
  renderDebitCardInfo = () => {
    const { descriptions, variantDetails, productName } = this.props;
    const { variants } = variantDetails || {};
    const { variant_Images: variantImage } = variants || {};
    const { descriptionTypeEcard, termsConditionEcard } = descriptions;
    const imagePath = variantImage || constants.DEFAULT_DEBIT_CARD_IMAGE;
    return (
      <Row className="redeempoints-description">
        <Col md={12} sm={12} xs={12}>
          <div className="card-img">
            <ImageHOC
              id="debitcard-image"
              isAbsolute={variantImage}
              src={imagePath}
              alt={productName}
              responsive
            />
          </div>
          <CardDetails
            description={{
              productDetails: descriptionTypeEcard,
              termsAndConditions: termsConditionEcard,
            }}
            onViewClick={this.viewHandler}
          />
        </Col>
      </Row>
    );
  }

  render() {
    const { productName } = this.props;
    return (
      <React.Fragment>
        <div ref={(ele) => { this.debitCard = ele; }} className="redeem-points-box mt-30" id="redeempoints-debitcard">
          <Row className="show-grid redeem-points-block">
            {productName && <h3>{productName}</h3>}
            <Col md={8} sm={8} xs={12}>
              {this.renderDebitCardInfo()}
            </Col>
            <Col md={4} sm={4} xs={12}>
              {this.renderDebitCardForm()}
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { form, contractorUserState, redeemPointsState } = state;
  const { users } = contractorUserState;
  const { debitCardForm = {} } = form;
  const { values, syncErrors } = debitCardForm;
  const { isShowPopup, category } = redeemPointsState;
  const cardHolderName = getDebitCardHolderName(state);
  return {
    initialValues: {
      cardHolderName,
    },
    cardHolderName,
    isShowPopup,
    users,
    category,
    formValues: values,
    errors: syncErrors,
    productName: getProductName(state),
    variantDetails: getVariants(state),
    variantId: getVariantId(state),
    additionalFee: getAddtionalFee(state),
    debitCardInfo: getDebitCardInfo(state),
    product: getProductdetails(state),
    feeDetails: getFeeDetails(state),
    descriptions: getDescription(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
    redeemActions: bindActionCreators(redeemActionsCreators, dispatch),
  };
}
/** PropTypes:
 * changeFormState - func - function for update redux form state
 * getDebitCardInfo - func - function for trigger api call for get debit card details
 * handleSubmit - func - function for submit the form
 * gtmActions - object- action for google tag manager
 * reset - func - function to reser the form
 * onSubmit - func - callback function for add to cart
 * product - object - contains details about product variant
 * feeDetails - object - contains the details of debit card fee information
 * formValues - object - Contains the values of valid form fields
 * description - object - contains the debit card information
 * users - array - contains the user information for logged in contractor.
 * errors - object - Contains the error messages for invalid form fields
 * additionalFee - number - contains the addtiona fee information for new debit card user
 * cardHolderName - string - name of debit card holder name
 * isShowPopup - boolean -show/hide the success popup
 * category - string - selected category
 * variantId - string - unique identifier for variant
 * productName - string - name of product
*/
DebitCard.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  gtmActions: PropTypes.object.isRequired,
  redeemActions: PropTypes.object.isRequired,
  product: PropTypes.object,
  feeDetails: PropTypes.object,
  formValues: PropTypes.object,
  descriptions: PropTypes.object,
  users: PropTypes.array,
  errors: PropTypes.object,
  additionalFee: PropTypes.number,
  cardHolderName: PropTypes.string,
  isShowPopup: PropTypes.bool,
  category: PropTypes.string,
  variantId: PropTypes.string,
  productName: PropTypes.string,
  variantDetails: PropTypes.object,
};

DebitCard.defaultProps = {
  formValues: {},
};

const Component = reduxForm({
  form: 'debitCardForm',
  validate,
})(DebitCard);

export default connect(mapStateToProps, mapDispatchToProps)(Component);
