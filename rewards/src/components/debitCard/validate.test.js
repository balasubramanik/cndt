import { validate } from './validate';
import { constants } from './constants';

describe('validate', () => {
  it('should be equal to invalid cardHolderName', () => {
    const errors = validate({});
    expect(errors.cardHolderName).toBe(true);
  });

  it('should be equal to invalid redeemableAmount', () => {
    const errors = validate({});
    expect(errors.redeemableAmount).toBe(true);
  });

  it('should be equal to invalid min redeemableAmount', () => {
    const errors = validate({ redeemableAmount: '50' }, { feeDetails: { min: 100, max: 500 } });
    expect(errors.redeemableAmount).toBe(constants.MIN_ERROR.replace('{{amount}}', '$100'));
  });

  it('should be equal to invalid max redeemableAmount', () => {
    const errors = validate({ redeemableAmount: '550' }, { feeDetails: { min: 100, max: 500 } });
    expect(errors.redeemableAmount).toBe(constants.MAX_ERROR.replace('{{amount}}', '$500'));
  });

  it('should be equal to valid cardHolderName', () => {
    const errors = validate({ cardHolderName: 'john' });
    expect(errors.cardHolderName).toBe(undefined);
  });
});
