import { constants } from './constants';
import { convertStringToNumber, formatUSCurrency } from '../../utils/utils';

const validate = (fields, props) => {
  const errors = {};
  const { users } = props || {};
  const amount = convertStringToNumber(fields.redeemableAmount);
  if (!fields.cardHolderName || !users || users.length === 0) {
    errors.cardHolderName = true;
  }
  if (!fields.redeemableAmount) {
    errors.redeemableAmount = true;
  }
  if (fields.redeemableAmount) {
    const { feeDetails } = props;
    const {
      max, min,
    } = feeDetails;
    if (min > amount) {
      errors.redeemableAmount = constants.MIN_ERROR.replace('{{amount}}', formatUSCurrency(min));
    }
    if (max < amount) {
      errors.redeemableAmount = constants.MAX_ERROR.replace('{{amount}}', formatUSCurrency(max));
    }
  }

  return errors;
};

export { validate };
