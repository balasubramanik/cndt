import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import DebitCardComponent, { DebitCard } from './index';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();
const event = {
  preventDefault: jest.fn(),
  target: {
    value: '100',
  },
};
describe('DebitCard', () => {
  window.open = jest.fn();
  const changeFormState = jest.fn();
  const handleSubmit = jest.fn();
  const getDebitCardInfo = jest.fn();
  const submitHandler = jest.fn();
  const reset = jest.fn();
  const users = [{ name: 'john', firstName: 'john', lastName: 'doe' }];
  const descriptions = {
    descriptionTypeEcard: 'description1',
    termsConditionEcard: 'description2',
    termsConditionPhysical: 'description2',
  };
  const syncErrors = {
    redeemableAmount: 'Please enter valid number',
  };
  const feeDetails = {
    listprice: '0.000',
    cardType: 'Physical Card',
    feeType: '%',
    feeValue: '2.5',
    giftCardValue: null,
    imageicon: null,
    max: '99,999.00',
    min: '25.00',
    minmaxtype: '$',
    Variant_Images: null,
    DisplayName: 'VISA Reloadble Debit Card',
  };
  const gtmActions = {
    gtmDebitCardSuccess: jest.fn(),
  };
  const tree = shallow(<DebitCard
    changeFormState={changeFormState}
    category="debit"
    getDebitCardInfo={getDebitCardInfo}
    handleSubmit={handleSubmit}
    debitCardUserName="john"
    feeDetails={feeDetails}
    values={{}}
    descriptions={descriptions}
    users={users}
    errors={syncErrors}
    onSubmit={submitHandler}
    debitCardInfo={{ ...feeDetails }}
    isShowPopup={true}
    gtmActions={gtmActions}
    reset={reset}
    productName="VISA Reloadble Debit Card"
  />);
  it('should be defined', () => {
    expect(DebitCard).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render debit card Image ', () => {
    expect(tree.find('Image').length).toBe(1);
  });

  it('should render debit card description ', () => {
    expect(tree.find('CardDetails').length).toBe(1);
  });

  it('should call debitAmountChangeHandler ', () => {
    const simulateClick = tree.find('Field[id="redeemableAmount"]').prop('onChange');
    simulateClick(event);
    expect(tree.state().fee).toBe('$2.50');
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isShowPopup: false,
    });
    expect(reset).toBeCalled();
  });
});

describe('DebitCardComponent with connected component', () => {
  window.open = jest.fn();
  const getDebitCardInfo = jest.fn();
  const changeFormState = jest.fn();
  const submitHandler = jest.fn();
  const state = {
    contractorUserState: {
      users: null,
    },
    myProfileState: {
      rewardsInfo: {
        rewardPoints: '1000',
      },
    },
    headerState: {
      availableBalance: 100,
    },
    redeemPointsState: {
      cardHolderName: 'john',
      totalBalance: 1000,
      debitCardInfo: [{
        categories: [{
          products: [{
            productSpecification: {
              descriptionTypeEcard: 'description1',
              termsConditionEcard: 'description2',
              termsConditionPhysical: 'description3',
            },
            variants: [{
              variants: {
                listprice: '0.000',
                cardType: 'Physical Card',
                feeType: '$',
                feeValue: 2.5,
                giftCardValue: null,
                imageicon: null,
                max: '99,999.00',
                min: '25.00',
                minmaxtype: '$',
                Variant_Images: null,
                DisplayName: 'VISA Reloadble Debit Card',
              },
            }],
          }],
        }],
      }],
    },
    form: {
      debitCardForm: {
        values: {},
        syncErrors: {
          redeemableAmount: 'Please enter valid number',
        },
      },
    },
  };
  const gtmActions = {
    gtmDebitCardSuccess: jest.fn(),
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <DebitCardComponent gtmActions={gtmActions} onSubmit={submitHandler} getDebitCardInfo={getDebitCardInfo} changeFormState={changeFormState} />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(DebitCardComponent).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render debit card Image ', () => {
    expect(tree.find('Image').length).toBe(1);
  });

  it('should render debit card description ', () => {
    expect(tree.find('CardDetails').length).toBe(1);
  });

  it('should call debitAmountChangeHandler ', () => {
    const simulateClick = tree.find('Field[id="redeemableAmount"]').prop('onChange');
    simulateClick(event);
    expect(changeFormState).toBeDefined();
  });

  it('should call submitHandler', () => {
    const simulateClick = tree.find('form[name="debitCardForm"]').prop('onSubmit');
    simulateClick();
    expect(submitHandler).toBeCalled();
  });
});
