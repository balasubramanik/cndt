const constants = {
  MIN_ERROR: 'You have entered the less than {{amount}} amount.',
  MAX_ERROR: 'Sorry, you can only redeem {{amount}} at a time.',
  ADDITIONAL_FEE_TEXT: 'Fee for this transfer is:',
  ADD_TO_CART: 'Add to cart',
  VIEW_MORE: 'View More',
  VIEW_LESS: 'View Less',
  DEBIT_CARD: 'Debit Card',
  DEBIT_CARD_INFO: 'debitCardInfo',
  TYPE: 'debit',
  DEFAULT_DEBIT_CARD_IMAGE: 'images/debit-card.jpg',
  NAME_ON_CARD: 'Name on Card',
  PRODUCT_DETAILS: 'Product Details',
  TERMS_AND_CONDITIONS: 'Terms and Conditions',
};

export { constants };
