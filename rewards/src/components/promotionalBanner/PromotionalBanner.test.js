import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PromotionalBanner from '../promotionalBanner';
import { history } from '../../routes';

configure({ adapter: new Adapter() });
history.push = jest.fn();
describe('PromotionalBanner with long plan title', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<PromotionalBanner
    promotionsInfo={{
      StartDate: '2018-08-01T16:57:19.0487212+05:30',
      EndDate: '2018-08-011T16:57:19.0487212+05:30',
      planTitle: 'Timberline Ultra HD Promotion Ultra HD Promotion Ultra HD Promotion Ultra HD',
      description: 'Rewards start at 0.5% for every qualifying GAF residential product purchase you make in 2018. Begin submitting invoices this March.',
    }}
    index={1}
  />);

  it('should be defined', () => {
    expect(PromotionalBanner).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Promotion Banner container', () => {
    expect(tree.find('.promotions').length).toBe(1);
  });

  it('should render Promotion Banner Image', () => {
    expect(tree.find('.promotion-thumbnail').length).toBe(1);
  });

  it('should render Promotion Banner Description', () => {
    expect(tree.find('.promotions-desc').length).toBe(1);
  });

  it('should render Promotion startdate', () => {
    expect(tree.find('.gaf-promotion-startdate').length).toBe(1);
  });

  it('should render Promotion enddate', () => {
    expect(tree.find('.gaf-promotion-enddate').length).toBe(1);
  });

  it('should render Tooltip', () => {
    expect('Tooltip').toBeDefined();
  });
  
  it('should render More details link', () => {
    expect(tree.find('.moredetail-link').length).toBe(1);
  });
  it('should redirect to Spot Promotion Details from promotion image', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(history.push).toBeCalled();
  });
  it('should redirect to Spot Promotion Details from more details link', () => {
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(history.push).toBeCalled();
  });
});

describe('PromotionalBanner', () => {
  const tree = shallow(<PromotionalBanner
    promotionsInfo={{
      StartDate: '2018-08-01T16:57:19.0487212+05:30',
      EndDate: '2018-08-011T16:57:19.0487212+05:30',
      planTitle: 'Timberline Ultra HD Promotion',
      description: 'Rewards start at 0.5% for every qualifying GAF residential product purchase you make in 2018. Begin submitting invoices this March.',
    }}
    index={1}
  />);

  it('should be defined', () => {
    expect(PromotionalBanner).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
