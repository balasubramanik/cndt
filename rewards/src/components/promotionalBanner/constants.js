const promotions = {
  START_DATE: 'Start Date',
  END_DATE: 'End Date',
  MORE_DETAILS: 'More Details',
  PLAN_TITLE_LENGTH: 50,
  DETAIL_DESC_LENGTH: 120,
  DEFAULT_IMAGE_URL: 'images/banner-image.jpg',
};

export { promotions };
