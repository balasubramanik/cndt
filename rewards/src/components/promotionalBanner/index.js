import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Col, Row } from 'react-bootstrap';
import _truncate from 'lodash/truncate';
import envConfig from 'envConfig'; //eslint-disable-line
import ToolTip from '../toolTip';
import { promotions } from './constants';
import { formatDateUTC } from '../../utils/utils';
import RouteConstants from '../../constants/RouteConstants';
import { history } from '../../routes';

/** @description Function to render the Promotional Banner Image */
const renderPromotionImage = (promotionsInfo, index, gtmActions, updateSelectedYear) => {
  const {
    planTitle, startDate, endDate, image, id,
  } = promotionsInfo;
  const imgpath = image || `${envConfig.cdnBuildUrl}${promotions.DEFAULT_IMAGE_URL}`;
  const planDetails = {
    id,
    index,
    planTitle,
    startDate,
    endDate,
  };
  return (
    <a href="/" onClick={(e) => redirectToSpotPromotionDetails(e, planDetails, gtmActions, updateSelectedYear)} >
      <figure className="promotion-thumbnail" style={{ backgroundImage: `url("${imgpath}")` }}>
      </figure>
    </a>);
};

const redirectToSpotPromotionDetails = (e, planDetails, gtmActions, updateSelectedYear) => {
  const {
    startDate,
    endDate,
    id,
    planTitle,
  } = planDetails;
  e.preventDefault();
  const currentYear = moment().year();
  const prevYear = currentYear - 1;
  const startYear = moment(startDate).year();
  let year = moment(endDate).year();
  if (currentYear >= startYear && startYear >= prevYear) {
    year = startYear;
  }
  history.push({
    pathname: RouteConstants.SPOTPROMOTIONS_DETAILS.replace(':planId', id),
  });

  updateSelectedYear(year);
  /* Trigger the GTM action */
  gtmActions.gtmViewPromotionDetails(planTitle);
};


/** @description Function to render the Promotion Details
 * @param {object} promotionsInfo - contains the details of promotions
 */
const renderPromotionDetails = (promotionsInfo, index, gtmActions, updateSelectedYear) => {
  const {
    planTitle, description, products = [], id,
  } = promotionsInfo;
  const { startDate, endDate } = products[0] || {};
  const planDetails = {
    id,
    index,
    planTitle,
    startDate,
    endDate,
  };
  return (
    <div className="promotions-desc">
      {planTitle && planTitle.length >= promotions.PLAN_TITLE_LENGTH ? <div><ToolTip id="promotions-title" text={planTitle}><h3><span role="button">{_truncate(planTitle, { length: promotions.HEADER_DESC_LENGTH })}</span></h3></ToolTip></div> : <h3>{planTitle}</h3>}
      <p>{_truncate(description, { length: promotions.DETAIL_DESC_LENGTH })}</p>
      <div className="promotion-date">
        <Row>
          <Col xs={6}>
            <dl className="gaf-promotion-startdate">
              <dt>{formatDateUTC(startDate)}</dt>
              <dd>{promotions.START_DATE}</dd>
            </dl>
          </Col>
          <Col xs={6}>
            <dl className="gaf-promotion-enddate">
              <dt>{formatDateUTC(endDate)}</dt>
              <dd>{promotions.END_DATE}</dd>
            </dl>
          </Col>
        </Row>
      </div>
      <div className="moredetail-link">
        <a href="/" onClick={(e) => redirectToSpotPromotionDetails(e, planDetails, gtmActions, updateSelectedYear)} >{promotions.MORE_DETAILS}<i className="icon-arrow" /></a>
      </div>
    </div>
  );
};

/** @description Functional component to render the Promotional Banner */
const PromotionalBanner = (props) => {
  const {
    promotionsInfo,
    index,
    gtmActions,
    updateSelectedYear,
  } = props;
  return (
    <div className="promotions">
      <Row>
        <Col xs={12} sm={12} md={6}>
          {renderPromotionImage(promotionsInfo, index, gtmActions, updateSelectedYear)}
        </Col>
        <Col xs={12} sm={12} md={6}>
          {renderPromotionDetails(promotionsInfo, index, gtmActions, updateSelectedYear)}
        </Col>
      </Row>
    </div>
  );
};

PromotionalBanner.propTypes = {
  promotionsInfo: PropTypes.object.isRequired,
  index: PropTypes.number,
  gtmActions: PropTypes.object,
  updateSelectedYear: PropTypes.func,
};


export default PromotionalBanner;
