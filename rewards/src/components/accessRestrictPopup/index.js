import React from 'react';
import envConfig from 'envConfig'; //eslint-disable-line
import { usernotavailable } from './constants';
import { authentication, authorization } from '../../routes';
import Dialog from '../dialog';
import ErrorNotification from '../errorNotification';
import { getImpersonatedBy } from '../../utils/utils';

class AccessRestrictionPopup extends React.Component {
  getAccessMessage = () => {
    let accessDeclineMessage = (getImpersonatedBy() && !authorization.hasRewardAccess) ? usernotavailable.IMPERSONATOR_MESSAGE : usernotavailable.USER_MESSAGE;
    if (!authorization.hasContractorExist) {
      accessDeclineMessage = usernotavailable.CONTRACTOR_NOT_FOUND;
    }
    if (authorization.hasAdminAccess) {
      accessDeclineMessage = usernotavailable.ADMIN_MESSGAE;
    }
    return (
      <div>
        {accessDeclineMessage}
      </div>
    );
  }

  handleClose(e) {
    const url = envConfig.cczBaseUrl;
    e.preventDefault();
    authentication.clearAuthSession();
    window.open(url, '_self');
  }

  render() {
    if (authorization.hasServiceError) {
      return (
        <ErrorNotification
          error
          onClear={(ev) => this.handleClose(ev)}
        />
      );
    }
    return (
      <div>
        <Dialog
          show
          onCloseClick={(ev) => this.handleClose(ev)}
          title={usernotavailable.ACCESS_DENIED}
          body={this.getAccessMessage()}
          className="gaf-model-popup"
        />

      </div>
    );
  }
}

export default AccessRestrictionPopup;
