const usernotavailable = {
  ACCESS_DENIED: 'ACCESS DENIED',
  USER_MESSAGE: 'You do not have permission to access GAF Rewards. Please contact a member of your company.',
  ADMIN_MESSGAE: 'You do not have the necessary permission to access the admin portal. Please contact the administrator.',
  IMPERSONATOR_MESSAGE: 'This contractor has not yet signed up for GAF Rewards. Please note: registration must be done by an authorized person at the contractor\'s company.',
  CONTRACTOR_NOT_FOUND: 'Sorry, the contractor ID/user you are trying to access doesn’t exist in GAF Rewards.',
};

export { usernotavailable };
