import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Pagination from '../../components/pagination';

configure({ adapter: new Adapter() });
describe('Pagination without props', () => {
  const tree = shallow(<Pagination />);

  it('should be defined', () => {
    expect(Pagination).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
describe('Pagination with props', () => {
  const pageClick = jest.fn();
  const tree = shallow(<Pagination perPage={20} totalRows={100} pageClick={pageClick} />);

  it('should be defined', () => {
    expect(Pagination).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should get correct page number', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      page: 1,
    });
    expect(tree.state().page).toBe(1);
    expect(tree.state().totalPage).toBe(5);
  });
});

describe('Pagination with mismatching perPage props', () => {
  const pageClick = jest.fn();
  const tree = shallow(<Pagination perPage={30} totalRows={90} pageClick={pageClick} />);

  it('should get correct default page', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      perPage: 20,
    });
    expect(tree.state().page).toBe(1);
  });
});

describe('Pagination with mismatching totalRows props', () => {
  const pageClick = jest.fn();
  const tree = shallow(<Pagination perPage={30} totalRows={120} pageClick={pageClick} />);

  it('should get correct page count', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      perPage: 30,
      totalRows: 60,
    });
    expect(tree.state().totalPage).toBe(4);
  });
});

describe('Pagination with default selected page changed', () => {
  const pageClick = jest.fn();
  const tree = shallow(<Pagination perPage={30} totalRows={90} pageClick={pageClick} selected={3} />);

  it('should get correct page number', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      selected: 2,
    });
    expect(tree.state().page).toBe(3);
  });
});


describe('Pagination for traversing to first and last pages', () => {
  const pageClick = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<Pagination perPage={10} totalRows={60} pageClick={pageClick} />);
  it('should traverse to first page', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(pageClick).toBeCalled();
    expect(tree.state().page).toBe(1);
  });
  it('should traverse to last page', () => {
    const simulateClick = tree.find('a').at(3).prop('onClick');
    simulateClick(event);
    expect(pageClick).toBeCalled();
    expect(tree.state().page).toBe(6);
  });
});

describe('Pagination for traversing to previous and next pages', () => {
  const pageClick = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<Pagination perPage={10} totalRows={60} pageClick={pageClick} selected={2} />);
  it('should traverse to next page', () => {
    const simulateClick = tree.find('a').at(2).prop('onClick');
    simulateClick(event);
    expect(pageClick).toBeCalled();
    expect(tree.state().page).toBe(3);
  });
  it('should traverse to previous page', () => {
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(pageClick).toBeCalled();
    expect(tree.state().page).toBe(2);
  });
});


describe('Pagination for going to next page when already at last page', () => {
  const pageClick = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<Pagination perPage={10} totalRows={60} pageClick={pageClick} selected={6} />);
  it('should traverse to next page', () => {
    const simulateClick = tree.find('a').at(2).prop('onClick');
    simulateClick(event);
    expect(tree.state().page).toBe(6);
  });
});

describe('Pagination for going to previous page when already at first page', () => {
  const pageClick = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<Pagination perPage={10} totalRows={60} pageClick={pageClick} selected={1} />);
  it('should traverse to next page', () => {
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(tree.state().page).toBe(1);
  });
});
