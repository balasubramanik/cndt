import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: props.selected,
      totalPage: this.getTotalPages(),
    };
  }

  componentDidUpdate(prevProps) {
    const { perPage, selected, totalRows } = this.props;
    if ((prevProps.perPage !== perPage && perPage) || (prevProps.totalRows !== totalRows && totalRows)) {
      this.updateState('totalPage', this.getTotalPages());
      this.updateState('page', 1);
    }
    if (prevProps.selected !== selected && selected) {
      this.updateState('page', selected);
    }
  }

  getTotalPages = () => {
    const { totalRows, perPage } = this.props;
    return Math.ceil(totalRows / perPage);
  }

  setFirstPage = (event) => {
    event.preventDefault();
    const page = 1;
    this.setState({ page });
    this.props.pageClick(page, 'first');
  }

  setLastPage = (event) => {
    const { totalPage } = this.state;
    event.preventDefault();
    this.setState({ page: totalPage });
    this.props.pageClick(totalPage, 'last');
  }

  updateState = (key, value) => this.setState({ [key]: value });

  decrementPage = (event) => {
    event.preventDefault();
    const { pageClick } = this.props;
    let { page } = this.state;
    if (page > 1) {
      page -= 1;
      this.setState({ page });
      pageClick(page, 'prev');
    }
  }
  incrementPage = (event) => {
    event.preventDefault();
    const { pageClick } = this.props;
    const { page, totalPage } = this.state;
    if (page < totalPage) {
      const pageNumber = page + 1;
      this.setState({ page: pageNumber });
      pageClick(pageNumber, 'next');
    }
  };
  render() {
    const { page, totalPage } = this.state;
    const disablePreviousClass = classNames({ disabled: page <= 1 });
    const disabledNextClass = classNames({ disabled: page >= totalPage });
    return (
      <ul className="pagination-actions" id="gaf-pagination-actions">
        <li className={disablePreviousClass}><a href="/" onClick={this.setFirstPage}><i className="icon-double-arrow-left"></i></a></li>
        <li className={disablePreviousClass}><a href="/" onClick={this.decrementPage} id="previous-page"><i className="icon-arrow-left"></i></a></li>
        <li><span>Page {page} of {totalPage}</span></li>
        <li className={disabledNextClass}><a href="/" onClick={this.incrementPage} id="next-page"><i className="icon-arrow-right"></i></a></li>
        <li className={disabledNextClass}><a href="/" onClick={this.setLastPage}><i className="icon-double-arrow-right" ></i></a></li>
      </ul>
    );
  }
}

Pagination.propTypes = {
  totalRows: PropTypes.number,
  perPage: PropTypes.number,
  pageClick: PropTypes.func,
  selected: PropTypes.number,
};

Pagination.defaultProps = {
  selected: 1,
};

export default Pagination;
