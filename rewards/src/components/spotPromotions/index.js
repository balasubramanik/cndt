import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Col, ControlLabel, Form, FormGroup, PanelGroup, Row, Tab } from 'react-bootstrap';
import Dropdown from '../dropdown';
import CSV from '../../components/csv';
import { tableHeaders } from './constants';
import RouteConstants from '../../constants/RouteConstants';
import { history } from '../../routes';
import Tables from '../../components/tables';
import { ascending, descending, formatDateUTC, truncateZero, roundToTwoDecimal } from '../../utils/utils';
import Accordion from '../../components/accordion';

/** @description Class component to render the Spot Promotion Tab */
class SpotPromotions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spotPromotionData: props.data || [],
      activePanelIndex: null,
    };
  }

  componentDidUpdate(prevProps) {
    const { data, loading, activeIndex } = this.props;
    if (prevProps.loading !== loading && !loading && activeIndex === 2) {
      this.updateSpotPromotionState(data);
    }
  }

  updateSpotPromotionState = (data) => {
    this.setState({ spotPromotionData: data }, () => {
      this.scrollHandler();
      this.props.updatePlanId();
    });
  }

  scrollHandler = () => {
    const { spotPromotionData } = this.state;
    const { selectedPlanId } = this.props;
    if (selectedPlanId && this[`spotpromotion${selectedPlanId}`]) {
      const activePanelIndex = spotPromotionData.findIndex((item) => item.id === selectedPlanId);
      const containerTop = this.spotPromotionContainer.offsetTop;
      const eleTop = this[`spotpromotion${selectedPlanId}`].offsetTop;
      this.setState({ activePanelIndex }, () => {
        this.props.scrollIntoView(containerTop + eleTop);
      });
    } else {
      this.setState({ activePanelIndex: null });
    }
  }

  /** @description Function available for sorting  */
  sortBy = (key, i, isAsc) => {
    const { spotPromotionData } = this.state;
    const categories = [...spotPromotionData];
    const selectedCategory = categories[i];
    categories.splice(i, 1);
    categories.splice(i, 0, {
      ...selectedCategory,
      products: key === tableHeaders.heading.Rate ? this.sortByRate(isAsc, selectedCategory.products) : selectedCategory.products.sort(isAsc ? ascending(key) : descending(key)),
    });
    this.setState({ spotPromotionData: categories });
  }

  sortByRate = (isAsc, products) => {
    const rateByDollar = [];
    const rateByPercent = [];
    const key = tableHeaders.heading.RateValue;
    products.forEach((item) => {
      if (item.allowanceType === tableHeaders.heading.Dollar) {
        const i = {
          ...item,
          [key]: item.dollarPerUnit,
        };
        rateByDollar.push(i);
      } else {
        const i = {
          ...item,
          [key]: item.byPercent,
        };
        rateByPercent.push(i);
      }
    });
    rateByDollar.sort(isAsc ? ascending(key) : descending(key));
    rateByPercent.sort(isAsc ? ascending(key) : descending(key));
    return rateByDollar.concat(rateByPercent);
  }

  /** @description Formating the available date into correct date format */
  formatProducts = (products) => {
    if (!products) return [];
    return products.map((product) => {
      const decimalRate = truncateZero(product.byPercent);
      const decimalDollar = roundToTwoDecimal(product.dollarPerUnit);
      const {
        startDate, endDate, allowanceType,
      } = product;
      return {
        ...product,
        startDate: formatDateUTC(startDate) || 'N/A',
        endDate: formatDateUTC(endDate) || 'N/A',
        rate: allowanceType === tableHeaders.heading.Dollar ? `$${decimalDollar}/${product.uom.code}` : `${decimalRate}%` || 'N/A',
      };
    });
  };

  createSpotPromotionCSV = () => {
    const csv = [];
    const { spotPromotionData } = this.state;
    if (spotPromotionData) {
      spotPromotionData.forEach((item) => {
        const formattedData = this.formatProducts(item.products);
        formattedData.forEach((product) => {
          csv.push({
            ...product,
            ...item,
            uom: product.uom.code,
            dollarPerUnit: product.allowanceType === tableHeaders.heading.Dollar ? product.dollarPerUnit : product.byPercent,
            allowanceType: product.allowanceType === tableHeaders.heading.Dollar ? 'Dollar' : 'Percent',
          });
        });
      });
    }
    return csv;
  }

  handleSelect = (activePanelIndex) => {
    this.setState({ activePanelIndex });
  }


  /** @description function for redirecting to details page  */
  detailsHandler = (e, id) => {
    const { location } = this.props;
    const { state } = location;
    e.preventDefault();
    history.push({
      pathname: RouteConstants.SPOTPROMOTIONS_DETAILS.replace(':planId', id),
      state,
    });
  };

  renderCSVLink = () => {
    const { spotPromotionData } = this.state;
    const CSVRecords = this.createSpotPromotionCSV();
    return (
      <div className="gaf-export-block pull-right">
        <CSV
          filename={tableHeaders.heading.filename}
          config={tableHeaders.csvConfig}
          data={CSVRecords}
          onClick={this.props.CSVHandler}
          disabled={!spotPromotionData || spotPromotionData.length === 0}
        >
          <div className="pull-right" id="sp-export-block">
            <span><i className="icon-Export-Icon"></i><span>{tableHeaders.heading.label}</span></span>
          </div>
        </CSV>
      </div>
    );
  }

  render() {
    const {
      loading,
      selectedYear,
    } = this.props;
    const { spotPromotionData, activePanelIndex } = this.state;
    return (


      <div className="gaf-tabs" id="spot-promotion-tab">
        <Tab eventKey={1} title={tableHeaders.heading.title}>
          <div className="tab-header-content" id="sp-tab-header">
            <Row className="show-grid">
              <Col xs={12} sm={12} md={4} lg={5}>
                <h4>Category</h4>
              </Col>
              <Col xs={12} sm={12} md={8} lg={7}>
                <div className="filterby" id="sp-tabheader-filterby">
                  <Form inline>
                    <FormGroup className="gaf-form gaf-select-box">
                      <ControlLabel>View by</ControlLabel>{' '}
                      <Dropdown
                        menuItems={tableHeaders.items}
                        onChangeHandler={this.props.onChange}
                        value={selectedYear}
                      />
                    </FormGroup>{' '}
                    {this.renderCSVLink()}
                  </Form>
                </div>
              </Col>
            </Row>
          </div>
          <Row>
            <div className="col-xs-12" ref={(ele) => { this.spotPromotionContainer = ele; }}>
              {/* GAF - Products Details */}
              {spotPromotionData && spotPromotionData.length > 0 ?
                <div className="gaf-accordian">
                  <PanelGroup
                    accordion
                    id="accordion-controlled-example"
                    activeKey={activePanelIndex}
                    onSelect={this.handleSelect}
                  >
                    {spotPromotionData.map((item, i) => (
                      <div ref={(ele) => { this[`spotpromotion${item.id}`] = ele; }}>
                        <Accordion
                          key={i.toString()}
                          title={item.planTitle}
                          details="Details"
                          body={<Tables config={tableHeaders.spotPromotions} data={this.formatProducts(item.products)} onSortingClick={(key, isAsc) => this.sortBy(key, i, isAsc)} />}
                          index={i}
                          onSelect={(e) => this.detailsHandler(e, item.id, selectedYear)}
                          id={`sp_accordion_${i}`}
                        />
                      </div>
                    ))}
                  </PanelGroup>
                </div> : (!loading && <div>{tableHeaders.heading.noData}</div>)}
            </div>
          </Row>
        </Tab>
      </div>

    );
  }
}

/** PropTypes:
 * onChange - Function for determining the on change behaviour of dropdown
 * data - Array of data supplied to the MyPlan component
 * handleSelect - Function to handle the tab selection
 * location - location prop
 */

SpotPromotions.propTypes = {
  onChange: PropTypes.func,
  data: PropTypes.array,
  updatePlanId: PropTypes.func,
  selectedPlanId: PropTypes.string,
  loading: PropTypes.bool,
  activeIndex: PropTypes.number,
  CSVHandler: PropTypes.func,
  selectedYear: PropTypes.number,
  scrollIntoView: PropTypes.func,
  location: PropTypes.object,
};

export default withRouter(SpotPromotions);
