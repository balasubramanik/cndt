import moment from 'moment';

const tableHeaders = {
  spotPromotions: [
    { title: 'PRODUCT', key: 'productFamily', sort: true },
    { title: 'RATE', key: 'rate', sort: true },
    { title: 'START DATE', key: 'startDate', sort: true },
    { title: 'END DATE', key: 'endDate', sort: true },
  ],
  details: [
    { details: 'Details', key: 'Details' },
  ],
  items: [
    { label: 'Current Year', value: moment().year() },
    { label: 'Previous Year', value: moment().year() - 1 },
  ],
  heading: {
    title: 'Spot Promotions',
    filename: 'SpotPromotions.csv',
    label: 'Export',
    Dollar: 'DOL',
    Rate: 'rate',
    RateValue: 'rateValue',
    noData: 'You do not have any spot promotion for the selected year',
  },
  csvConfig: [
    { colName: 'Category', key: 'planTitle' },
    { colName: 'Product', key: 'productFamily' },
    { colName: 'UOM', key: 'uom' },
    { colName: 'Value', key: 'dollarPerUnit' },
    { colName: 'Allowance Type', key: 'allowanceType' },
    { colName: 'Start Date', key: 'startDate' },
    { colName: 'End Date', key: 'endDate' },
  ],
};

export { tableHeaders };
