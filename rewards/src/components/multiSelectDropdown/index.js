import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { DropdownButton } from 'react-bootstrap';
import Checkbox from '../../components/checkbox';
import { createListener, removeListener } from '../../utils/utils';

/** @description Functional component to render the dropdown */
class MultiSelectDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
      isSelectAll: false,
    };
  }

  componentDidMount() {
    this.stateUpdate();
    createListener(null, ['click', 'touchstart', 'orientationchange'], this.closeDropDownHandler);
  }

  componentDidUpdate(prevProps) {
    const { menuItems } = this.props;
    const prevPropsObj = JSON.stringify(prevProps.menuItems);
    const newPropsObj = JSON.stringify(menuItems);
    if (prevPropsObj !== newPropsObj) {
      this.stateUpdate();
    }
  }

  /** @description React Life cycle method
  * it will invoke, when component is mounted.
  */
  componentWillUnmount() {
    removeListener(null, ['click', 'touchstart', 'orientationchange'], this.closeDropDownHandler);
  }

  stateUpdate = () => {
    this.setState({ checkedItems: new Map() }, () => {
      this.defaultHandleSelect();
    });
  }

  /** @description Callback function for close dropdown
   * @param {object} e - triggered event
   */
  closeDropDownHandler = (e) => {
    if (e && e.type === 'orientationchange') {
      this.setState({ isOpen: false });
      return;
    }
    if (this.node && !this.node.contains(e.target)) {
      this.setState({ isOpen: false });
    }
  }

  /** @description Callback function for open dropdown
   * @param {object} e - triggered event
   */
  openDropdownHandler = (isOpen) => {
    this.setState({ isOpen });
  }

  /** @description Function to render the previous checked items select box */
  defaultHandleSelect = () => {
    const dropdownValue = new Map(this.state.checkedItems);
    const {
      menuItems, isSelectAll, input,
    } = this.props;
    const AllItems = [];
    menuItems.forEach((item) => {
      const { title } = item;
      dropdownValue.set(title, item);
    });
    if (isSelectAll === true) {
      this.setState({ isSelectAll: true });
    }
    const SelectedAllItem = dropdownValue.toJSON();
    SelectedAllItem.forEach((data) => {
      AllItems.push(data[1]);
    });
    const isAllChecked = AllItems.every((i) => i.checked);
    this.setState({ isSelectAll: isAllChecked, checkedItems: dropdownValue }, () => {
      const onChangeValue = { itemSelected: dropdownValue.toJSON(), isSelectAll: this.state.isSelectAll };
      // callback function which is comes from redux form
      input.onChange(onChangeValue);
    });
  }

  /* @description Functional for select all value in dropdown */
  handleSelectAll = (e) => {
    const dropdownValue = new Map(this.state.checkedItems);
    const { menuItems } = this.props;
    const isSelectAllChecked = e.target.checked;
    menuItems.forEach((item) => {
      const { title, value } = item;
      const selectItemValue = { title, value, checked: isSelectAllChecked };
      dropdownValue.set(title, selectItemValue);
    });

    this.setState({ isSelectAll: isSelectAllChecked });
    return {
      dropdownValue,
    };
  }
  /** @description Functional for select the value in dropdown */
  selectHandle = (e, val) => {
    let dropdownValue = new Map(this.state.checkedItems);
    const { menuItems } = this.props;
    const AllItems = [];
    if (val) {
      const selectedItem = e.target.name;
      const isSelectChecked = e.target.checked;
      const selectItemValue = { title: selectedItem, value: val, checked: isSelectChecked };
      dropdownValue.set(selectedItem, selectItemValue);
      this.setState({ isSelectAll: false });
      const SelectedAllItem = dropdownValue.toJSON();
      SelectedAllItem.forEach((data) => {
        if (data[1] && data[1].checked) {
          AllItems.push(data[1]);
        }
      });
      if (AllItems.length === menuItems.length) {
        this.setState({ isSelectAll: true });
      }
    } else {
      dropdownValue = new Map();
      [...e.target.selectedOptions].forEach((item) => {
        const { value, text } = item;
        if (value) {
          const selectItemValue = { title: text, value, checked: true };
          dropdownValue.set(text, selectItemValue);
        }
      });
      this.setState({ isSelectAll: false });
      if (dropdownValue.size === menuItems.length) {
        this.setState({ isSelectAll: true });
      }
    }
    return {
      dropdownValue,
    };
  }

  handleChange = (e, val) => {
    const { input, onChangeHandler } = this.props;
    let selectHandleValues;
    if (val === 'all') {
      selectHandleValues = this.handleSelectAll(e);
    } else {
      selectHandleValues = this.selectHandle(e, val);
    }
    const {
      dropdownValue,
    } = selectHandleValues;
    this.setState({ checkedItems: dropdownValue }, () => {
      // callback function which is comes from redux
      const onChangeValue = { itemSelected: dropdownValue.toJSON(), isSelectAll: this.state.isSelectAll };
      input.onChange(onChangeValue);
      // callback function which is comes from parent component
      onChangeHandler(onChangeValue);
    });
  }
  /** @description Function to render the select box for mobile breakpoint */
  renderSelectField = () => {
    const {
      menuItems, input, readOnly, isDisabled,
    } = this.props;
    const { checkedItems } = this.state;
    const multiValueItem = checkedItems.toJSON();
    const selectValue = [];
    multiValueItem.forEach((data) => {
      if (data[1] && data[1].checked) {
        const { value } = data[1];
        selectValue.push(value);
      }
    });
    return (
      <select
        {...input}
        multiple
        className="visible-xs visible-sm form-control"
        value={selectValue}
        onChange={(e) => this.handleChange(e)}
        disabled={isDisabled}
      >
        {
          menuItems && menuItems.map((item) => {
            const { title, value } = item;
            return (
              <option
                disabled={readOnly}
                name={title}
                value={value}
              >
                {title}
              </option>
            );
          })
        }
      </select>
    );
  }

  render() {
    const {
      menuItems, placeholder, readOnly, isDisabled,
    } = this.props;
    const { isSelectAll, checkedItems, isOpen } = this.state;
    const titles = checkedItems.toJSON();
    const selected = [];
    if (titles.length) {
      titles.forEach((data) => {
        if (data[1] && data[1].checked) {
          selected.push(data[1].title);
        }
      });
    }
    const placeholderItems = selected.join(', ') || placeholder;
    const selectAllTitle = { title: 'Select All' };
    const { meta } = this.props;
    const { touched, error } = meta;
    const dropdownClass = classNames('rpm-dropdown', { 'dropdown-highlight': touched && error });
    return (
      <React.Fragment>
        <div className={dropdownClass}>
          <div className="custom-select-box hidden-xs hidden-sm" ref={(ele) => { this.node = ele; }}>
            <DropdownButton
              title={placeholderItems}
              id="multiple_dropdown"
              className="hidden-xs"
              open={isOpen}
              disabled={isDisabled}
              onToggle={this.openDropdownHandler}
            >
              <li>{menuItems && <Checkbox value="all" checked={isSelectAll} disabled={readOnly} option={selectAllTitle} onChange={(e) => this.handleChange(e, 'all')} />}</li>
              {
                menuItems && menuItems.map((item) => {
                  const { title, value } = item;
                  const { checked } = checkedItems.get(title) || {};
                  const itemTitle = { title };
                  return (
                    <li><Checkbox
                      disabled={readOnly}
                      key={title}
                      name={title}
                      value={value}
                      option={itemTitle}
                      checked={checked}
                      onChange={(e) => this.handleChange(e, value)}
                    />
                    </li>
                  );
                })
              }
            </DropdownButton>
          </div>
          {this.renderSelectField()}
          {touched && error && error.length > 0 && <p><span className="error-message text-danger">{error}</span></p>}
        </div>
      </React.Fragment>
    );
  }
}

/** PropTypes:
 *  menuItems - array - dropdown menu items
 *  prevSelected - array - previous Selected items
 *  onChangeHandler - func - callback function for select the option
 *  input - object - property comes from redux-form
*/

MultiSelectDropdown.propTypes = {
  menuItems: PropTypes.array,
  onChangeHandler: PropTypes.func,
  input: PropTypes.object,
  placeholder: PropTypes.string,
  isSelectAll: PropTypes.bool,
  readOnly: PropTypes.bool,
  meta: PropTypes.object,
  isDisabled: PropTypes.bool,
};

MultiSelectDropdown.defaultProps = {
  menuItems: [],
  onChangeHandler: () => { },
  input: { onChange: () => { } },
  meta: {},
};

export default MultiSelectDropdown;
