import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MultiSelectDropdown from '../../components/multiSelectDropdown';


configure({ adapter: new Adapter() });

describe('Dropdown with Menu items', () => {
  const onChange = jest.fn();
  const event = {
    preventDefault: jest.fn(),
    menuItems: {
      value: 'item1',
      title: 'item1',
      checked: false,
    },
  };
  const tree = shallow(<MultiSelectDropdown
    menuItems={[{ title: 'item1', value: 'item1', checked: false }, { title: 'item2', value: 'item2', checked: false }]}
    onChange={onChange}
  />);

  it('should be defined', () => {
    expect(MultiSelectDropdown).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render MenuItem', () => {
    expect(tree.find('menuItems').length).not.toBe(1);
  });

  it('should call selectHandler for dropdown field with data', () => {
    const simulateClick = tree.find('menuItems').at(0).prop('onChange');
    simulateClick(event, { title: 'item1', value: 'item1' });
    expect(tree.state().value).toBe('item1');
    expect(tree.state().title).toBe('item1');
  });

  it('should render select element', () => {
    expect(tree.find('select').length).toBe(1);
  });

  it('should call selectHandler for select field', () => {
    const simulateClick = tree.find('select').prop('onChange');
    simulateClick(event);
    expect(tree.state().value).toBe('item1');
    expect(tree.state().title).toBe('item1');
  });
});

describe('Dropdown without Placeholder', () => {
  const tree = shallow(<MultiSelectDropdown
    menuItems={[{ title: 'item1', value: 'item1', checked: false }]}
  />);

  it('should be defined', () => {
    expect(MultiSelectDropdown).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render MenuItem', () => {
    expect(tree.find('menuItems').length).not.toBe(1);
  });

  it('should render options with placeholder', () => {
    expect(tree.find('option').length).toBe(2);
  });
});

describe('Dropdown with Value', () => {
  const tree = shallow(<MultiSelectDropdown
    id="gaf-input"
    menuItems={[{ title: 'item1', value: 'item1', checked: false }]}
    value="item1"
  />);

  it('should be defined', () => {
    expect(MultiSelectDropdown).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
