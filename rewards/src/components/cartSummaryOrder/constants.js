const cartSummaryOrdertext = {
  ORDER_SUMMARY: 'Order Summary',
  PRODUCT_VALUE: 'Product(s) Value',
  FEES_APPLIED: 'Fees Applied',
  ORDER_TOTAL: 'Order Total',
  DISABLE_VALUE: 'disable',
  SUBMIT_ORDER: 'Submit Order',
  INSUFFICIENT_BALANCE_MESSAGE: 'You don\'t have enough points to redeem the selected item(s).',
};


export { cartSummaryOrdertext };
