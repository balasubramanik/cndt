import React from 'react';
import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';
import { cartSummaryOrdertext } from './constants';
import { convertStringToNumber } from '../../utils/utils';

class CartSummaryOrder extends React.PureComponent {
  render() {
    const {
      orderTotal = '',
      feeTotal = '',
      productTotal = '',
      handleOrderSubmit,
      handleCheckout,
      onDisableOrder,
      rewardsPoints,
    } = this.props;
    const isAvailableCheck = handleCheckout.length > 0;
    const sufficientBalance = convertStringToNumber(orderTotal) > convertStringToNumber(rewardsPoints);
    return (
      <Col md={4} >
        {sufficientBalance &&
          <span className="gaf-error-message balance"> {cartSummaryOrdertext.INSUFFICIENT_BALANCE_MESSAGE} </span>
        }
        <div className="order-summary" id="order-summary-details">
          <div className="order-summary-details">
            <p id="order-summary-title">{cartSummaryOrdertext.ORDER_SUMMARY}</p>
            <table>
              <tbody>
                <tr>
                  <td id="tot-prod-val-title">{cartSummaryOrdertext.PRODUCT_VALUE}</td>
                  <td id="tot-prod-val">{productTotal}</td>
                </tr>
                <tr>
                  <td id="tot-fee-val-title">{cartSummaryOrdertext.FEES_APPLIED}</td>
                  <td id="tot-fee-val">{feeTotal}</td>
                </tr>
              </tbody>
            </table>
            <div className="order-total">
              <p id="tot-order-val-title">{cartSummaryOrdertext.ORDER_TOTAL}<span id="tot-order-val">{orderTotal}</span></p>
            </div>
          </div>
        </div>
        <button
          id="cart-order-submit"
          className="btn gaf-btn-primary order-button"
          disabled={`${isAvailableCheck || sufficientBalance || onDisableOrder ? cartSummaryOrdertext.DISABLE_VALUE : ''}`}
          onClick={() => handleOrderSubmit(sufficientBalance)}
        >
          {cartSummaryOrdertext.SUBMIT_ORDER}
        </button>
      </Col>
    );
  }
}

CartSummaryOrder.propTypes = {
  orderTotal: PropTypes.string,
  feeTotal: PropTypes.string,
  productTotal: PropTypes.string,
  rewardsPoints: PropTypes.string,
  handleOrderSubmit: PropTypes.func,
  handleCheckout: PropTypes.array,
  onDisableOrder: PropTypes.string,

};

export default CartSummaryOrder;

