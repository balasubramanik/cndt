import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryOrder from '../../components/cartSummaryOrder';

configure({ adapter: new Adapter() });


describe('CartSummaryOrder List', () => {
  const routeChangeprop = jest.fn();
  const tree = shallow(<CartSummaryOrder
    orderData={{
      productValue: '',
      feesApplied: '',
      orderTotal: 1000,
    }}
    availBalance={500}
    value={{ value: '' }}
    index={0}
    handleOrderSubmit={routeChangeprop}
    handleCheckout={[true,
      true,
      false]
    }
  />);

  it('should be defined', () => {
    expect(CartSummaryOrder).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onclick handler', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange();
    expect(routeChangeprop).toBeCalled();
  });
});


describe('CartSummaryOrder List', () => {
  const routeChangeprop = jest.fn();
  const tree = shallow(<CartSummaryOrder
    orderData={{
      productValue: '',
      feesApplied: '',
      orderTotal: 100,
    }}
    availBalance={500}
    value={{ value: '' }}
    index={0}
    handleOrderSubmit={routeChangeprop}
    handleCheckout={[true,
      true,
      false]
    }
  />);

  it('should be defined', () => {
    expect(CartSummaryOrder).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onclick handler', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange();
    expect(routeChangeprop).toBeCalled();
  });
});


describe('CartSummaryOrder List with insufficient balance', () => {
  const routeChangeprop = jest.fn();
  const tree = shallow(<CartSummaryOrder
    orderTotal="200"
    value={{ value: '' }}
    index={0}
    handleOrderSubmit={routeChangeprop}
    rewardsPoints="100"
    handleCheckout={[true,
      true,
      false]
    }
  />);

  it('should be defined', () => {
    expect(CartSummaryOrder).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render error message', () => {
    expect(tree.find('span').at(0).text()).toBe(' Your cart total is greater than your available points balance. ');
  });
  it('should call onclick handler', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange();
    expect(routeChangeprop).toBeCalled();
  });
});

describe('CartSummaryOrder List with insufficient balance', () => {
  const routeChangeprop = jest.fn();
  const tree = shallow(<CartSummaryOrder
    orderTotal="200"
    value={{ value: '' }}
    index={0}
    handleOrderSubmit={routeChangeprop}
    rewardsPoints="100"
    handleCheckout={[]}
    onDisableOrder="true"
  />);

  it('should be defined', () => {
    expect(CartSummaryOrder).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render error message', () => {
    expect(tree.find('span').at(0).text()).toBe(' Your cart total is greater than your available points balance. ');
  });
  it('should call onclick handler', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange();
    expect(routeChangeprop).toBeCalled();
  });
});

describe('CartSummaryOrder List with sufficient balance', () => {
  const routeChangeprop = jest.fn();
  const tree = shallow(<CartSummaryOrder
    orderTotal="100"
    value={{ value: '' }}
    index={0}
    handleOrderSubmit={routeChangeprop}
    rewardsPoints="200"
    handleCheckout={[]}
    onDisableOrder="false"
  />);

  it('should be defined', () => {
    expect(CartSummaryOrder).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onclick handler', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange();
    expect(routeChangeprop).toBeCalled();
  });
});
