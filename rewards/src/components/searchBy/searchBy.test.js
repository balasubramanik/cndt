import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SearchBy from '../../components/searchBy';
import TypeAhead from '../../components/TypeAHead';

configure({ adapter: new Adapter() });
jest.useFakeTimers();
describe('SearchBy', () => {
  const optionSelectHandler = jest.fn();
  const changeHandler = jest.fn();
  const submitHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<SearchBy
    isSuggestionsLoading={false}
    suggestions={[
      {
        orderId: '3', type: 'Earned', points: '+ 250.00', user: 'J. Doe', date: '1534411158174',
      },
      {
        orderId: '1', type: 'Earned', points: '+ 50.00', user: 'Jack', date: '1534411158170',
      },
      {
        orderId: '2', type: 'Earned', points: '+ 205.00', user: 'Mnom', date: '1534411155179',
      },
    ]}
    optionSelected={optionSelectHandler}
    handleChange={changeHandler}
    placeholder="Search by"
    id="claimDetailProduct"
    charLength={{
      min: 3,
      max: 10,
    }}
    validationErrTxt="abcdxyz"
    searchBykey="mno"
    dynamicClass="pull-right"
    isSearchByIcon
    disabled={false}
    typeahead="false"
    handleSubmit={submitHandler}
    isResetSearch
    label="Try again"
  />);
  it('should be defined', () => {
    expect(SearchBy).toBeDefined();
  });
  it('should render correctly', () => {
    expect(SearchBy).toMatchSnapshot();
  });
  it('should remove search text', () => {
    tree.setState({ searchText: 'ggg', selectedOption: 'ggg' });
    expect(tree.state().searchText).toBe('ggg');
    expect(tree.state().selectedOption).toBe('ggg');
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(tree.state().searchText).toBe('');
    expect(tree.state().selectedOption).toBe('');
  });
  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isResetSearch: false,
    });
    expect(changeHandler).toBeCalled();
  });
  it('should call SearchHandler', () => {
    const e = {
      preventDefault: jest.fn(),
      target: { value: 'wxyz' },
    };
    const simulateClick = tree.find('input').prop('onChange');
    simulateClick(e);
    expect(tree.state().searchText).toBe('wxyz');
    expect(tree.state().selectedOption).toBe('wxyz');
    expect(tree.state().isShowTypeAhead).toBe(true);
    expect(changeHandler).toBeCalled();
  });
  it('should call SearchHandler with isSubmitted being true', () => {
    const e = {
      preventDefault: jest.fn(),
      target: { value: 'wxyz' },
    };
    tree.setState({ isSubmitted: true });
    const simulateClick = tree.find('input').prop('onChange');
    simulateClick(e);
    expect(tree.state().searchText).toBe('wxyz');
    expect(tree.state().selectedOption).toBe('wxyz');
    expect(tree.state().isSearchTextError).toBe(false);
    expect(tree.state().isShowTypeAhead).toBe(true);
    expect(changeHandler).toBeCalled();
  });
  it('should call SearchHandler with no target value', () => {
    const e = {
      preventDefault: jest.fn(),
      target: { value: '' },
    };
    const simulateClick = tree.find('input').prop('onChange');
    simulateClick(e);
    expect(tree.state().searchText).toBe('');
    expect(tree.state().selectedOption).toBe('');
    expect(tree.state().isShowTypeAhead).toBe(false);
    expect(changeHandler).toBeCalled();
  });
  // it('should call TypeAhead Handler', () => {
  //   const e = {
  //     preventDefault: jest.fn(),
  //     target: { value: 'lmnop' },
  //   };
  //   tree.setState({ isShowTypeAhead: true, isSearchTextError: false });
  //   const simulateClick = tree.find('TypeAhead').prop('onChange');
  //   expect(tree.state().isShowTypeAhead).toBe(false);
  //   simulateClick(e, 'wes');
  //   expect(tree.state().selectedOption).toBe('wes');
  //   expect(optionSelectHandler).toBeCalled();
  // });
  it('should call Search Blur Handler', () => {
    tree.setState({ isShowTypeAhead: true, isSubmitted: true, searchText: 'dt' });
    const simulateClick = tree.find('input').prop('onBlur');
    simulateClick(event);
    jest.advanceTimersByTime(200);
    expect(tree.state().isShowTypeAhead).toBe(false);
    expect(tree.state().isSearchTextError).toBe(true);
  });
  it('should call Search Blur Handler with false isShowTypeAhead and isSubmitted', () => {
    tree.setState({
      isShowTypeAhead: false, isSubmitted: false, searchText: 'hbo', isSearchTextError: true,
    });
    const simulateClick = tree.find('input').prop('onBlur');
    simulateClick(event);
    jest.advanceTimersByTime(200);
    expect(tree.state().isShowTypeAhead).toBe(false);
    expect(tree.state().isSearchTextError).toBe(true);
  });
  it('should call Search Focus Handler', () => {
    tree.setState({ isShowTypeAhead: true, isSubmitted: true, searchText: 'dt' });
    const simulateClick = tree.find('input').prop('onFocus');
    simulateClick(event);
    expect(tree.state().isSearchTextError).toBe(true);
    expect(tree.state().isShowTypeAhead).toBe(false);
  });
  it('should call Search Focus Handler with isSubmitted false and no search text', () => {
    tree.setState({ isShowTypeAhead: true, isSubmitted: false, searchText: '' });
    const simulateClick = tree.find('input').prop('onFocus');
    simulateClick(event);
    expect(tree.state().isShowTypeAhead).toBe(false);
  });
  it('should call form submit handler', () => {
    tree.setState({ isShowTypeAhead: true, isSubmitted: false, searchText: 'dt' });
    const simulateClick = tree.find('form').prop('onSubmit');
    simulateClick(event);
    expect(submitHandler).toBeCalled();
    expect(tree.state().isShowTypeAhead).toBe(false);
    expect(tree.state().isSearchTextError).toBe(true);
    expect(tree.state().isSubmitted).toBe(true);
    expect(tree.state().isRequired).toBe(true);
  });
  it('should call form submit handler with Valid Character Length false', () => {
    tree.setState({ isShowTypeAhead: true, isSubmitted: false, searchText: 'hbo' });
    const simulateClick = tree.find('form').prop('onSubmit');
    simulateClick(event);
    expect(submitHandler).toBeCalled();
    expect(tree.state().isShowTypeAhead).toBe(true);
    expect(tree.state().isSearchTextError).toBe(false);
  });
});

describe('SearchBy with no placeholder', () => {
  const optionSelectHandler = jest.fn();
  const changeHandler = jest.fn();
  const submitHandler = jest.fn();
  const tree = shallow(<SearchBy
    isSuggestionsLoading={false}
    suggestions={[
      {
        orderId: '3', type: 'Earned', points: '+ 250.00', user: 'J. Doe', date: '1534411158174',
      },
      {
        orderId: '1', type: 'Earned', points: '+ 50.00', user: 'Jack', date: '1534411158170',
      },
      {
        orderId: '2', type: 'Earned', points: '+ 205.00', user: 'Mnom', date: '1534411155179',
      },
    ]}
    optionSelected={optionSelectHandler}
    handleChange={changeHandler}
    placeholder=""
    id="claimDetailProduct"
    charLength={{
      min: 3,
      max: 10,
    }}
    validationErrTxt="abcdxyz"
    searchBykey="mno"
    dynamicClass="pull-right"
    isSearchByIcon
    disabled={false}
    typeahead="false"
    handleSubmit={submitHandler}
    isResetSearch={false}
    label="Try again"
  />);
  it('should be defined', () => {
    expect(TypeAhead).toBeDefined();
  });
  it('should render correctly', () => {
    expect(TypeAhead).toMatchSnapshot();
  });
  it('should not call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isResetSearch: false,
    });
    expect(changeHandler).not.toBeCalled();
  });
});

describe('SearchBy with no label', () => {
  const optionSelectHandler = jest.fn();
  const changeHandler = jest.fn();
  const submitHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<SearchBy
    isSuggestionsLoading={false}
    suggestions={[
      {
        orderId: '3', type: 'Earned', points: '+ 250.00', user: 'J. Doe', date: '1534411158174',
      },
      {
        orderId: '1', type: 'Earned', points: '+ 50.00', user: 'Jack', date: '1534411158170',
      },
      {
        orderId: '2', type: 'Earned', points: '+ 205.00', user: 'Mnom', date: '1534411155179',
      },
    ]}
    optionSelected={optionSelectHandler}
    handleChange={changeHandler}
    placeholder=""
    id="claimDetailProduct"
    charLength={{
      min: 3,
      max: 10,
    }}
    validationErrTxt="abcdxyz"
    searchBykey="mno"
    dynamicClass="pull-right"
    isSearchByIcon
    disabled={false}
    typeahead="true"
    handleSubmit={submitHandler}
    isResetSearch
    label=""
  />);
  it('should be defined', () => {
    expect(TypeAhead).toBeDefined();
  });
  it('should render correctly', () => {
    expect(TypeAhead).toMatchSnapshot();
  });
  it('should call TypeAhead Handler', () => {
    tree.setState({ isShowTypeAhead: true, isSearchTextError: false });
    const simulateClick = tree.find('TypeAhead').prop('onChange');
    expect(tree.state().isShowTypeAhead).toBe(false);
    simulateClick(event, 'wes');
    expect(tree.state().selectedOption).toBe('wes');
    expect(optionSelectHandler).toBeCalled();
  });
});
