import React, { } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import TypeAhead from '../TypeAHead';
import { isRequiredErrorMsg } from './constants';
class SearchBy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowTypeAhead: false,
      searchText: '',
      isSearchTextError: false,
      selectedOption: '',
      isRequired: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { isResetSearch } = this.props;
    if ((isResetSearch !== prevProps.isResetSearch) && isResetSearch) {
      this.resetSearchText();
    }
  }

  /* Reset Search Text Box Value */
  resetSearchText = () => {
    this.setState({
      searchText: '',
      selectedOption: '',
      isRequired: false,
    }, () => {
      this.props.handleChange(this.state.searchText, false);
    });
  }

  isValidCharcterLength = () => {
    const { charLength } = this.props;
    const { searchText } = this.state;
    if (searchText) {
      return (searchText.length < charLength.min || ((charLength.max) && searchText.length > charLength.max));
    }
    return false;
  }

  /** @description callback function for blur event in search field
   * @param {object} event
   */
  searchBlurHandler = () => {
    this.blurTimeout = setTimeout(() => {
      const { isShowTypeAhead } = this.state;
      if (isShowTypeAhead) {
        this.updateTypeAheadState(false);
      }
    }, 200);
  }

  /** @description callback function for focus event in search field
   * @param {object} event
   */
  searchFocusHandler = () => {
    const { searchText } = this.state;
    clearTimeout(this.blurTimeout);
    if (!searchText) {
      this.updateTypeAheadState(false);
      // this.props.handleChange(searchText, false);
    } else {
      const isDisplayTypeAhead = this.isValidCharcterLength();
      this.updateTypeAheadState(!isDisplayTypeAhead);
    }
  }

  /** @description callback function for search field
   * @param {object} event
   */
  searchHandler = (event) => {
    const { value } = event.target;
    this.setState({
      searchText: value,
      selectedOption: value,
      isRequired: false,
    }, () => {
      if (!value) {
        this.updateTypeAheadState(false);
        this.props.handleChange(value, false);
        this.setState({
          isSearchTextError: this.isValidCharcterLength(),
        });
      } else {
        const isDisplayTypeAhead = this.isValidCharcterLength();
        this.updateTypeAheadState(!isDisplayTypeAhead);
        this.props.handleChange(value, !isDisplayTypeAhead);
      }
    });
  }

  updateTypeAheadState = (isShowTypeAhead) => this.setState({ isShowTypeAhead });

  /* Remove Search Text when Close Icon Trigger */
  removeSearchText = (event) => {
    event.preventDefault();
    this.resetSearchText();
  }

  submitHandler = (event) => {
    event.preventDefault();
    if (this.isValidCharcterLength()) {
      this.updateTypeAheadState(false);
      this.setState({
        isSearchTextError: true,
      });
      this.props.handleSubmit(this.state.searchText, false);
    } else {
      this.updateTypeAheadState(this.state.searchText && this.state.searchText.length > 0);
      this.setState({
        isSearchTextError: false,
      });
      this.props.handleSubmit(this.state.searchText, true);
    }
    this.setState({
      isRequired: true,
    });
  }

  typeAheadHandler = (event, option) => {
    event.preventDefault();
    this.updateTypeAheadState(false);
    const { optionSelected } = this.props;
    this.setState({
      selectedOption: option,
    });
    optionSelected(option);
  };

  renderTypeAhead = () => {
    const { isSuggestionsLoading, suggestions, searchByKey } = this.props;
    const { isShowTypeAhead } = this.state;
    return isShowTypeAhead && <TypeAhead searchByKey={searchByKey} list={suggestions} isLoading={isSuggestionsLoading} onChange={this.typeAheadHandler} />;
  }

  render() {
    const {
      placeholder,
      id,
      isSearchByIcon,
      validationErrTxt,
      dynamicClass,
      disabled,
      typeahead,
      label,
    } = this.props;
    const {
      isShowTypeAhead,
      isSearchTextError,
      searchText,
      isRequired,
    } = this.state;
    const closeIcon = classNames('icon-close', { closeIcon: isSearchByIcon, closeIconRight: !isSearchByIcon });
    let errorMsg = '';
    if (placeholder && placeholder.toLowerCase().indexOf('search by') > -1) {
      errorMsg = `${placeholder.substr(9)}.`;
    } else if (label) {
      errorMsg = `${label}.`;
    }
    const errorMsgClass = classNames('text-danger input-error-msg', { 'media-object': (label && label.length > 0) });
    return (
      <form onSubmit={this.submitHandler} className="display-inline">
        <div className={`form-group gaf-form GAF-typeahead ${dynamicClass}`}>
          <input
            type="text"
            id={id}
            placeholder={placeholder}
            disabled={disabled}
            value={this.state.selectedOption}
            className="form-control"
            autoComplete="off"
            onChange={this.searchHandler}
            onBlur={this.searchBlurHandler}
            onFocus={this.searchFocusHandler}
          />
          {searchText && <a href="/#" onClick={this.removeSearchText} className={closeIcon}></a>}
          {isSearchByIcon && <a href="/#" onClick={this.submitHandler} className="icon-search"></a>}
          {JSON.parse(typeahead) && isShowTypeAhead && !isSearchTextError ? this.renderTypeAhead() : null}
          {isRequired && searchText && isSearchTextError && <span className={errorMsgClass}>{validationErrTxt}</span>}
          {isRequired && !searchText && <span className={errorMsgClass}>{`${isRequiredErrorMsg} ${errorMsg}`}</span>}
        </div>
      </form>
    );
  }
}

SearchBy.propTypes = {
  isSuggestionsLoading: PropTypes.bool,
  suggestions: PropTypes.array,
  optionSelected: PropTypes.func,
  handleChange: PropTypes.func,
  placeholder: PropTypes.string,
  id: PropTypes.string,
  isSearchByIcon: PropTypes.bool,
  charLength: PropTypes.object,
  validationErrTxt: PropTypes.string,
  searchByKey: PropTypes.string,
  dynamicClass: PropTypes.string,
  disabled: PropTypes.bool,
  typeahead: PropTypes.string,
  handleSubmit: PropTypes.func,
  isResetSearch: PropTypes.bool,
  label: PropTypes.string,
};

SearchBy.defaultProps = {
  handleSubmit: () => { },
  label: '',
};

export default SearchBy;

