import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryBalance from '../../components/cartSummaryBalance';

configure({ adapter: new Adapter() });

describe('User CartSummaryBalance with Balance', () => {
  const tree = shallow(<CartSummaryBalance balance={9999} />);
  it('should be defined', () => {
    expect(CartSummaryBalance).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render user CartSummary Available Balance', () => {
    expect(tree.find('p').length).toBe(1);
  });
});

describe('User CartSummaryBalance with Balance', () => {
  const tree = shallow(<CartSummaryBalance balance={null} />);
  it('should be defined', () => {
    expect(CartSummaryBalance).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render user CartSummary Available Balance', () => {
    expect(tree.find('p').length).toBe(1);
  });
});
