import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { cartSummaryBalanacetext } from './constants';
import { roundToTwoDecimal } from '../../utils/utils';

const CartSummaryBalance = ({ balance }) => {
  const availableBalance = (!balance ? `${roundToTwoDecimal(0)}` : `${balance}`);
  return (
    <div className="gaf-title-block">
      <Row className="show-grid">
        <Col xs={12} md={7} lg={8}>
          <h2 id="cart-balance">{cartSummaryBalanacetext.TITLE}</h2>
        </Col>
        <Col xs={12} md={5} lg={4}>
          <p id="avai-points-title">{cartSummaryBalanacetext.AVAILABLE_BALANCE}<span id="available-points-value">{`${availableBalance}`}</span></p>
        </Col>
      </Row>
    </div>
  );
};

CartSummaryBalance.propTypes = {
  balance: PropTypes.string,
};
export default CartSummaryBalance;
