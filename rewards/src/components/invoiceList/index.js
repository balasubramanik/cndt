import React from 'react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Tables from '../../components/tables';
import { invoiceListValue } from './constants';
import { formatDateUTC, formatPoint, formatRateValue } from '../../utils/utils';
import ToolTip from '../toolTip';
import CSV from '../../components/csv';
import { authorization } from '../../routes';

class InvoiceList extends React.Component {
  formatData = (records) => {
    if (!records) return [];
    return records.map((record) => {
      const decimalRate = (record.rateApplied) ? formatRateValue(record.rateApplied) : '-';
      return {
        ...record,
        invoiceDate: (record.invoiceDate) ? formatDateUTC(record.invoiceDate) : '-',
        productName: (record.productName) ? <ToolTip id={record.productName} text={record.productName}><span dangerouslySetInnerHTML={{ __html: record.productName }} role="button" /></ToolTip> : '-',
        promotionName: (record.promotionName) ? <ToolTip id={record.promotionName} text={record.promotionName}><span dangerouslySetInnerHTML={{ __html: record.promotionName }} role="button" /></ToolTip> : '-',
        submissionValue: (record.submissionValue) ? `$${formatPoint(record.submissionValue)}` : 'N/A',
        rateApplied: decimalRate,
        detailStatus: record.detailStatus === invoiceListValue.Approved ? record.detailStatus : this.elseApprovedStatus(record.detailStatus, record.reasonForDescAudit),
        pointsEarned: (record.pointsEarned) ? formatPoint(record.pointsEarned) : 0,
        uom: (record.uom) ? record.uom : '-',
        productQuantity: (record.productQuantity) ? record.productQuantity : '-',
        invoiceNumber: (record.invoiceNumber) ? record.invoiceNumber : '-',
      };
    });
  };

  elseApprovedStatus = (status, description) => (
    <span> {status} <span><ToolTip id="gaf-user-name" text={description}><i className="icon-info" role="button"></i></ToolTip></span></span>)


  createInvoiceListCSV = () => {
    const csv = [];
    const { exportCSVData, submittedOnDate, claimID } = this.props;
    if (exportCSVData) {
      exportCSVData.forEach((record) => {
        const item = {
          ...record,
          invoiceNumber: (record.invoiceNumber) ? record.invoiceNumber : '-',
          submissionPostMarkDate: (record.submissionPostMarkDate) ? formatDateUTC(record.submissionPostMarkDate) : '-',
          productName: (record.productName) ? record.productName : '-',
          promotionName: (record.promotionName) ? record.promotionName : '-',
          uom: (record.uom) ? record.uom : '-',
          productQuantity: (record.productQuantity || record.productQuantity === 0) ? record.productQuantity : '-',
          productPrice: (record.productPrice || record.productPrice === 0) ? record.productPrice : '-',
          submissionValue: (record.submissionValue || record.submissionValue === 0) ? record.submissionValue : 'N/A',
          pointsEarned: (record.pointsEarned || record.pointsEarned === 0) ? formatPoint(record.pointsEarned) : 0,
          detailStatus: record.detailStatus === invoiceListValue.Approved ? record.detailStatus : '-',
          distributorName: (record.distributorName) ? record.distributorName : '-',
          claimNumber: claimID,
          submittedDate: formatDateUTC(submittedOnDate),
          reasonCodeAudit: (record.reasonCodeAudit) ? record.reasonCodeAudit : '-',
        };
        csv.push(item);
      });
    }
    return csv;
  }

  handleInvoiceTableHeder = (InvoiceTableHeder) => {
    if (authorization.userRolesList.length && (authorization.userRolesList.includes('FULL_ACCESS') || authorization.userRolesList.includes('VIEW_POINTS'))) {
      const tableFields = [...InvoiceTableHeder];
      const tableheaderInvoiceList = [...tableFields, invoiceListValue.PTSEarned, invoiceListValue.status];
      return tableheaderInvoiceList;
    }
    const tableFields = [...InvoiceTableHeder];
    const tableheaderInvoiceList = [...tableFields, invoiceListValue.status];
    return tableheaderInvoiceList;
  }

  tableHeder = () => (
    <Row className="show-grid">
      <Col xs={8} sm={4} md={4} lg={6}>
        <h4>{invoiceListValue.TableTitle}</h4>
      </Col>
      <Col xs={4} sm={8} md={8} lg={6}>
        {this.renderCSVLink()}
      </Col>
    </Row>);

  renderCSVLink = () => {
    const CSVRecords = this.createInvoiceListCSV();
    return (
      <div className="gaf-export-block">
        <CSV
          filename={invoiceListValue.heading.filename}
          config={invoiceListValue.csvConfig}
          data={CSVRecords}
          onClick={this.props.CSVHandler}
        >
          <div className="pull-right">
            <span><i className="icon-Export-Icon"></i><span>{invoiceListValue.heading.label}</span></span>
          </div>
        </CSV>
      </div>
    );
  }


  render() {
    const {
      data,
      onSort,
      loading,
      defaultSort,
    } = this.props;
    return (
      <div>
        <div className="gaf-table-header">
          {this.tableHeder()}
        </div>
        <div className="claim-details-invoice-table">
          <Tables config={this.handleInvoiceTableHeder(invoiceListValue.invoiceListTableHeder)} data={this.formatData(data)} isLoading={loading} onSortingClick={onSort} sortByKey={defaultSort} />
        </div>
      </div>
    );
  }
}

InvoiceList.propTypes = {
  data: PropTypes.array,
  exportCSVData: PropTypes.array,
  onSort: PropTypes.func,
  loading: PropTypes.bool,
  CSVHandler: PropTypes.func,
  defaultSort: PropTypes.string,
  submittedOnDate: PropTypes.string,
  claimID: PropTypes.string,
};


export default InvoiceList;
