const invoiceListValue = {
  invoiceListTableHeder: [
    { title: 'INVOICE #', key: 'invoiceNumber', sort: true },
    { title: 'DATE', key: 'invoiceDate', sort: true },
    { title: 'PRODUCT', key: 'productName', sort: true },
    { title: 'PLAN NAME', key: 'promotionName', sort: true },
    { title: 'UOM', key: 'uom', sort: true },
    { title: 'QTY', key: 'productQuantity', sort: true },
    { title: 'VALUE', key: 'submissionValue', sort: true },
    { title: 'RATE', key: 'rateApplied', sort: true },
  ],
  PTSEarned: { title: 'PTS. EARNED', key: 'pointsEarned' },
  status: { title: 'STATUS', key: 'detailStatus' },
  Approved: 'Approved',
  heading: {
    filename: 'InvoiceList.csv',
    label: 'Export',
  },
  csvConfig: [
    { colName: 'INVOICE #', key: 'invoiceNumber' },
    { colName: 'DATE', key: 'invoiceDate' },
    { colName: 'PRODUCT', key: 'productName' },
    { colName: 'PLAN NAME', key: 'promotionName' },
    { colName: 'UOM', key: 'uom' },
    { colName: 'QTY', key: 'productQuantity' },
    { colName: 'RATE', key: 'productPrice' },
    { colName: 'VALUE', key: 'submissionValue' },
    { colName: 'PTS.EARNED', key: 'pointsEarned' },
    { colName: 'STATUS', key: 'detailStatus' },
    { colName: 'Distributor Name', key: 'distributorName' },
    { colName: 'Claim Number', key: 'claimNumber' },
    { colName: 'Submitted Date', key: 'submittedDate' },
    { colName: 'Reason Code', key: 'reasonCodeAudit' },
  ],
  TableTitle: 'List of Invoice(s)',
};

export { invoiceListValue };

