import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import InvoiceList from '../../components/invoiceList';

configure({ adapter: new Adapter() });

describe('InvoiceList with props', () => {
  const sortingHandler = jest.fn();
  const csvHandler = jest.fn();
  const refHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<InvoiceList
    data={[{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Cobra® Snow Country',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-32',
      submissionValue: '-2816',
      pointsEarned: '-28.16',
      rate: '3',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '',
      invoiceDate: '',
      productName: '',
      promotionName: '',
      uom: '',
      productQuantity: '',
      submissionValue: '',
      pointsEarned: '',
      rate: '',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    ]}
    onSort={sortingHandler}
    loading
    exportCsvData={[{
      invoiceNumber: '10006003-002',
      submissionPostMarkDate: '2018-12-19T09:00:56.6173327Z',
      productName: 'GlenwoodÂ™ Roofing Shingles',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-12',
      productPrice: '65',
      submissionValue: '-780',
      pointsEarned: '-7.8',
      detailStatus: 'Approved',
      distributorName: 'H&H LUMBER',
      reasonCodeAudit: '',
    },
    {
      invoiceNumber: '10006003-001',
      submissionPostMarkDate: '2018-12-19T09:00:56.6173327Z',
      productName: 'Grand Seq/Can Starter Strip',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '1',
      productPrice: '39',
      submissionValue: '39',
      pointsEarned: '0.39',
      detailStatus: 'Approved',
      distributorName: 'H&H LUMBER',
      reasonCodeAudit: '',
    },
    ]}
    onRef={refHandler}
    CSVHandler={csvHandler}
    defaultSort="invoiceDate"
    submittedOnDate="2018-11-29T09:10:56.6173337Z"
    claimID="G00000206"
  />);


  it('should be defined', () => {
    expect(InvoiceList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Tables Component', () => {
    expect(tree.find('Tables').length).toBe(1);
  });
  it('should call CSVHandler', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(csvHandler).toBeCalled();
  });
});

describe('InvoiceList with Tooltip for every record not approved', () => {
  const sortingHandler = jest.fn();
  const csvHandler = jest.fn();
  const refHandler = jest.fn();
  const tree = mount(<InvoiceList
    data={[{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Denied',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Cobra® Snow Country',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-32',
      submissionValue: '-2816',
      pointsEarned: '-28.16',
      rate: '3',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '',
      invoiceDate: '',
      productName: '',
      promotionName: '',
      uom: '',
      productQuantity: '',
      submissionValue: '',
      pointsEarned: '',
      rate: '',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    ]}
    onSort={sortingHandler}
    loading
    exportCsvData={[{
      invoiceNumber: '10006003-002',
      submissionPostMarkDate: '2018-12-19T09:00:56.6173327Z',
      productName: 'GlenwoodÂ™ Roofing Shingles',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-12',
      productPrice: '65',
      submissionValue: '-780',
      pointsEarned: '-7.8',
      detailStatus: 'Approved',
      distributorName: 'H&H LUMBER',
      reasonCodeAudit: '',
    },
    {
      invoiceNumber: '10006003-001',
      submissionPostMarkDate: '2018-12-19T09:00:56.6173327Z',
      productName: 'Grand Seq/Can Starter Strip',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '1',
      productPrice: '39',
      submissionValue: '39',
      pointsEarned: '0.39',
      detailStatus: 'Approved',
      distributorName: 'H&H LUMBER',
      reasonCodeAudit: '',
    },
    ]}
    onRef={refHandler}
    CSVHandler={csvHandler}
    defaultSort="invoiceDate"
    submittedOnDate="2018-11-29T09:10:56.6173337Z"
    claimID="G00000206"
  />);


  it('should be defined', () => {
    expect(InvoiceList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('InvoiceList without props', () => {
  const tree = shallow(<InvoiceList
    modifiedOn=""
  />);

  it('should be defined', () => {
    expect(InvoiceList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render InvoiceList Component', () => {
    expect(tree.find('InvoiceList').length).toBe(0);
  });
});
