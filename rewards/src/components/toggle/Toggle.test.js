import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Toggle from '../../components/toggle';
configure({ adapter: new Adapter() });

describe('Toggle with props', () => {
  const changeHandler = jest.fn();
  const tree = shallow(<Toggle
    onChange={changeHandler}
    isToggle="yes"
    id={11}
  />);
  it('should be defined', () => {
    expect(Toggle).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onChange event', () => {
    const simulateChange = tree.find('input').prop('onChange');
    simulateChange(changeHandler);
    expect(changeHandler).toBeCalled();
  });
});
