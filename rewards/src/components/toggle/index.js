import React from 'react';
import PropTypes from 'prop-types';


const Toggle = (props) => {
  const { onChange, id, isToggle } = props;
  return (
    <div className="gaf-toggle" >
      <input id={id} onChange={(e) => onChange(e, id)} type="checkbox" checked={(isToggle === 'yes')} />
      <label htmlFor={id}>
        <div className="gaf-toggle__switch" id={`${id}-${isToggle === 'yes'}`} data-unchecked="yes" data-checked="no"></div>
      </label>
    </div>
  );
};

/** PropTypes:
 * onChange - function - onChange function for toggle
 * isToggle - string - String value of yes or no for toggle
 * id - number - identifier for toggle
 */
Toggle.propTypes = {
  onChange: PropTypes.func.isRequired,
  isToggle: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
};
export default Toggle;
