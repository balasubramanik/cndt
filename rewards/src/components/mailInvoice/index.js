import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import pdfMake from 'pdfmake/build/pdfmake.min';
import '../../utils/vfs_fonts';
import Loader from '../../components/loader';
import { formatUSPhoneNumber } from '../../utils/utils';
import { mailInvoice, base64ImgData } from './Constants';
pdfMake.vfs = window.pdfMake.vfs;

export class MailInvoice extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
    this.onPdfClick = this.onPdfClick.bind(this);
    this.navigateToUploadInvoice = this.navigateToUploadInvoice.bind(this);
    this.docDefinition = {};
  }

  onPdfClick(e) {
    e.preventDefault();
    this.pdfCreation();
    pdfMake.createPdf(this.docDefinition).download(`${mailInvoice.FILENAME}.pdf`);
    const { userAgent } = navigator;
    const isIE = userAgent.indexOf('MSIE ') > -1 || userAgent.indexOf('Trident/') > -1 || userAgent.indexOf('Edge/') > -1;
    const isiOS = userAgent.match(/(iPad|iPhone|iPod)/g);
    const isAndroid = userAgent.toLowerCase().indexOf('android') > -1;
    if (!isIE && !isiOS && !isAndroid) {
      this.pdfCreation();
      pdfMake.createPdf(this.docDefinition).open();
    }
    /* Trigger the GTM action */
    this.props.gtmUploadInvoiceActions.gtmMailClaimForm();
  }

  navigateToUploadInvoice = (e) => {
    e.preventDefault();
    this.props.callback();
  }

  pdfCreation = () => {
    const { userInfo = {} } = this.props;
    const {
      contractorAccountNumber,
      contractorCertificationNumber,
      contractorName,
      emailAddress,
      contractorInfo = {},
    } = userInfo;
    const {
      address1,
      address2,
      address3,
      city,
      stateOrProvince,
      zipOrPostal,
      phone,
    } = contractorInfo.addresses[0];
    const notApplicable = 'N/A';
    const fullAddress = [];
    const certification = [];
    if (contractorCertificationNumber) certification.push([mailInvoice.CERTIFICATION, { text: contractorCertificationNumber, style: 'titleValues' }, '']);
    if (address1) fullAddress.push([mailInvoice.ADDRESSLINE1, { text: address1, style: 'titleValues' }, '']);
    if (address2) fullAddress.push([mailInvoice.ADDRESSLINE2, { text: address2, style: 'titleValues' }, '']);
    if (address3) fullAddress.push([mailInvoice.ADDRESSLINE3, { text: address3, style: 'titleValues' }, '']);
    pdfMake.fonts = {
      ITCAvantGardeStd: {
        normal: 'ITCAvantGardeStd-Bk.ttf',
        bold: 'ITCAvantGardeStd-Md.ttf',
      },
    };
    this.docDefinition = {
      pageSize: 'A4',
      pageOrientation: 'portrait',
      content: [
        {
          image: base64ImgData,
          fit: [211, 63],
          alignment: 'center',
        },
        {
          table: {
            widths: ['*'],
            body: [[' '], [' ']],
          },
          layout: {
            hLineWidth(i, node) {
              return (i === 0 || i === node.table.body.length) ? 0 : 0.5;
            },
            vLineWidth() {
              return 0;
            },
          },
        },
        {
          stack: [
            mailInvoice.HEADER,
          ],
          style: 'GAFHeading',
        },
        {
          text: '', fontSize: 14, bold: true, margin: [0, 0, 0, 8],
        },
        {
          style: 'tableExample',
          table: {
            headerRows: 1,
            widths: [210, 250, 140],
            heights: 26,
            body: [
              [{ text: '', style: '' }, { text: '', style: '' }, { text: '', style: '' }],
              [mailInvoice.COMPANY, { text: contractorName || notApplicable, style: 'titleValues' }, ''],
              ...certification,
              [mailInvoice.ACCOUNT, { text: contractorAccountNumber || notApplicable, style: 'titleValues' }, ''],
              ...fullAddress,
              [mailInvoice.CITY, { text: `${city || notApplicable}, ${stateOrProvince || notApplicable} ${zipOrPostal || notApplicable}`, style: 'titleValues' }, ''],
              [mailInvoice.PHONE, { text: phone ? formatUSPhoneNumber(phone) : notApplicable, style: 'titleValues' }, ''],
              [mailInvoice.EMAIL, { text: emailAddress || notApplicable, style: 'titleValues' }, ''],
            ],
            style: 'companyDetails',
          },
          layout: 'noBorders',
        },
        {
          style: 'tableExample',
          table: {
            headerRows: 1,
            widths: [200, 100, 200, 200],
            heights: 30,
            body: [
              [{ text: '', style: '' }, { text: '', style: '' }, { text: '', style: '' }, { text: '', style: '' }],
              [{ text: mailInvoice.SIGNATURE, bold: true }, '', { text: mailInvoice.DATE, bold: true }, ''],
            ],
          },
          layout: 'noBorders',
        },
        {
          canvas: [
            {
              type: 'line',
              x1: 60,
              y1: -20,
              x2: 290,
              y2: -20,
              lineWidth: 0.5,
              lineColor: '#494949',
            },
          ],
        },
        {
          canvas: [
            {
              type: 'line',
              x1: 350,
              y1: -20,
              x2: 515,
              y2: -20,
              lineWidth: 0.5,
              lineColor: '#494949',
            },
          ],
        },
        { text: mailInvoice.MAILFORM, fontSize: 12, margin: [0, 50, 0, 8] },
        {
          canvas: [
            {
              type: 'line',
              x1: 0,
              y1: 5,
              x2: 515,
              y2: 5,
              lineWidth: 0.3,
              dash: { length: 4, space: 2 },
              lineColor: '#494949',
            },
          ],
        },
        {
          image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAARCAYAAAA7bUf6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAhdEVYdFRpdGxlAGNvbnRlbnRfY3V0IC0gbWF0ZXJpYWwgY29weYPjcfUAAAHwSURBVDiNjZM9T1RREIafmU0s8AMspNQIrYlbSSEqoOTes1f8DST8AzsrQ2M0FvwDE0pt5d5z7iZKg4nRBgpLk9USowmI6wfsGQvuGj6uq1OezHlm5n1nJMuyMYDd3d2ddru9yYBIkuSaqj4GLgMfzexBCGFZnHPbwGmgC8x571/WAVqtVtPMXgM9YA1oAqNmNq+AA74CQ8Bz59xMHSTGeBc4YWZT3vsEuARsisg99d6/MrMWsDMIJCIXgO0QwlsA7/0nYB04rwAhhLUYYwv4NgD0HhhO03ShGm8CmATW5WCWc+4GkAMnga6q3s7zfDVN0ykRyasCAFvAMPDDzGYPQQAOfPgeY7wpImdFZKUPBp4CY0AnxrhUluXGMQhAlmXTe3t7X1R1pK6zo/laB8nzfLXRaAxXgPvV82IdAKC2E+fcVcCzvz/96JrZnRDCi39Cqq0sgFPsa7BYdTMEdEXkmZmNAR0RWSqK4rA7RwF9DbIsm44xrnDcnZ8xxtk/mqRpOlkH6GvEvisAC977kRjjBNBT1Ufa10BEDo4wVyPiOLDlvX8CUJblG6ob0iMiDjrCDnAmSZIrVeFzQFNEPuh/AhCRJeCXqq4650rgHTAaY3yoqtpU1fFer3fxbwCAoijWzewWsAFcF5HPZjYfQlj+DRqp7JT2G/NGAAAAAElFTkSuQmCC',
          fit: [10, 10],
          margin: [480, -5, 40, -5],
        },
        { text: mailInvoice.GAFADDRESS1, fontSize: 18, margin: [0, 50, 0, 8] },
        { text: mailInvoice.GAFADDRESS2, fontSize: 18, margin: [0, 0, 0, 8] },
        { text: mailInvoice.GAFADDRESS3, fontSize: 18, margin: [0, 0, 0, 8] },
        { text: mailInvoice.GAFADDRESS4, fontSize: 18, margin: [0, 0, 0, 8] },
      ],
      defaultStyle: {
        color: '#494949',
        font: 'ITCAvantGardeStd',
      },
      styles: {
        GAFHeading: {
          alignment: 'center',
          fontSize: 20,
          marginTop: 10,
          font: 'ITCAvantGardeStd',
          bold: true,
        },
        companyDetails: {
          fontSize: 13,
        },
        titleValues: {
          font: 'ITCAvantGardeStd',
          bold: true,
          fontWeight: 800,
          lineHeight: 1.5,
        },
      },
    };
  }

  render() {
    return (
      <div>
        <div className="mail-invoice-content  panel-body-content">
          <div className="mail-invoice-desc">
            <p>
              {mailInvoice.DESC}
            </p>
            <p>
              {mailInvoice.DESC1}
            </p>
          </div>
          <div className="mail-invoice-note">
            <p>
              <strong>{mailInvoice.NOTETEXT}</strong> {mailInvoice.NOTE}
            </p>
          </div>
          <div className="print-claim-from-btn">
            <button className="btn gaf-btn-primary" onClick={this.onPdfClick}>{mailInvoice.BUTTONTEXT}</button>
          </div>
        </div>
        {this.state.isLoading && <Loader />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { myProfileState } = state;
  const { userInfo, rewardsInfo, accessInfo } = myProfileState;
  return {
    userInfo,
    rewardsInfo,
    accessInfo,
  };
}

MailInvoice.propTypes = {
  callback: PropTypes.func,
  userInfo: PropTypes.object,
  gtmUploadInvoiceActions: PropTypes.object,
};

export default connect(mapStateToProps)(MailInvoice);
