import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MailInvoice from '../mailInvoice';


configure({ adapter: new Adapter() });

describe('MailInvoice', () => {
  const tree = shallow(<MailInvoice />);

  it('should be defined', () => {
    expect(MailInvoice).toBeDefined();
  });

  it('should render correctly', () => {
    const event = {
      preventDefault: jest.fn(),
    };
    expect(tree).toMatchSnapshot();
    tree.instance().navigateToUploadInvoice();
    tree.instance().onPdfClick(event);
  });
});
