import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { loadAsyncScript } from '../loadAsyncScript';
import Logo from '../logo';


configure({ adapter: new Adapter() });
describe('Loader with urls', () => {
  const AyncFunction = loadAsyncScript(Logo, ['https://sample.com']);
  const tree = shallow(<AyncFunction />);

  it('should be defined', () => {
    expect(loadAsyncScript).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Loader', () => {
    expect(tree.find('Loader').length).toBe(1);
  });
});

describe('Loader without urls', () => {
  const AyncFunction = loadAsyncScript(Logo);
  const tree = shallow(<AyncFunction />);

  it('should be defined', () => {
    expect(loadAsyncScript).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Logo', () => {
    expect(tree.find('Logo').length).toBe(1);
  });
});
