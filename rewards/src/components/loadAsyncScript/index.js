import React, { PureComponent } from 'react';
import Loader from '../loader';

/** @description Highorder component to load scripts */
export function loadAsyncScript(WrappedComponent, urls) {
  return class extends PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        isScriptLoaded: false,
        isLoading: true,
      };
    }

    /** @description React Life cycle method
     * it will invoke, when component is mounted.
     */
    componentDidMount() {
      if (urls) {
        const { length } = urls;
        urls.forEach((url, i) => {
          const script = document.createElement('script');
          script.src = url;
          script.async = true;
          if ((length - 1) === i) {
            script.onload = this.handleLoad;
            script.onerror = this.handleLoad;
          }
          document.body.appendChild(script);
        });
      } else {
        this.handleLoad();
      }
    }

    /** @description callback function for load scripts
     */
    handleLoad = () => {
      this.setState({ isScriptLoaded: true, isLoading: false });
    }

    render() {
      const { isScriptLoaded, isLoading } = this.state;
      return (
        <React.Fragment>
          {isScriptLoaded && <WrappedComponent {...this.props} />}
          {isLoading && <Loader />}
        </React.Fragment>
      );
    }
  };
}

