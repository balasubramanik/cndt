import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftCards from '../../components/giftCards';


configure({ adapter: new Adapter() });
const categories = [
  {
    categoryid: '2c60baca-93ed-4b3b-a046-d15473d8e692',
    displayName: 'apparels&shoes',
    description: 'RewardsCategoryDef',
    categories: [],
    products: [
      {
        variants: [
          {
            variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
            productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
            variantID: 'MER1001513-1',
            variants: {
              listprice: '0.0000',
              cardType: 'Physical Card',
              feeType: '$',
              feeValue: '1.0',
              giftCardValue: '25.0000',
              imageicon: null,
              max: '100.0',
              min: '1.0',
              minmaxtype: 'QTY',
              Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
              DisplayName: 'Aéropostale - $25',
            },
          },
        ],
        productSpecification: {
          descriptiontypeecard: 'Aéropostale is a specialty retailer of casual apparel and accessories, principally targeting 16 to 22-year-old young women and men through its Aéropostale and Aéropostale Factory Stores and website, aeropostale.com. The brand provides customers with a focused selection of high quality fashion and fashion basics at compelling values in an innovative and exciting store environment. Aéropostale currently operates more than 500 stores in the U.S.',
          termsconditionecard: 'Use of card constitutes acceptance of the following terms and conditions: Card may be used up to its remaining balance to redeem for merchandise at any Aéropostale store in the US or Puerto Rico, or online at www.aeropostale.com. This card is not redeemable for cash unless required by law. Any resale or other unauthorized use of this card will render it void and subject to cancellation. Issuer not responsible for lost, stolen, or damaged cards, except as required by law. Other conditions may apply, see www.aeropostale.com for details. For card balance please call 800-832-0656 or visit your local Aéropostale store. This card is issued by Aero GC Mgmt LLC.\nAéropostale is a registered trademark of ABG-AERO IPCO, LLC. All rights reserved. This Gift Card is subject to Aéropostale’s terms and conditions. Aéropostale is not a participating partner in or sponsor of this offer. www.aeropostale.com',
          termsconditionphysical: null,
        },
        cardTypes: ['eCard', 'physicalCard'],
        id: 'MER1001513',
        name: 'Aéropostale',
      },
      {
        variants: [
          {
            variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
            productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
            variantID: 'MER1001513-1',
            variants: {
              listprice: '0.0000',
              cardType: 'Physical Card',
              feeType: '$',
              feeValue: '1.0',
              giftCardValue: '25.0000',
              imageicon: null,
              max: '100.0',
              min: '1.0',
              minmaxtype: 'QTY',
              Variant_Images: null,
              DisplayName: 'Aéropostale - $25',
            },
          },
        ],
        cardTypes: ['eCard', 'physicalCard'],
        productSpecification: {
          descriptiontypeecard: 'Aéropostale is a specialty retailer of casual apparel and accessories, principally targeting 16 to 22-year-old young women and men through its Aéropostale and Aéropostale Factory Stores and website, aeropostale.com. The brand provides customers with a focused selection of high quality fashion and fashion basics at compelling values in an innovative and exciting store environment. Aéropostale currently operates more than 500 stores in the U.S.',
          termsconditionecard: 'Use of card constitutes acceptance of the following terms and conditions: Card may be used up to its remaining balance to redeem for merchandise at any Aéropostale store in the US or Puerto Rico, or online at www.aeropostale.com. This card is not redeemable for cash unless required by law. Any resale or other unauthorized use of this card will render it void and subject to cancellation. Issuer not responsible for lost, stolen, or damaged cards, except as required by law. Other conditions may apply, see www.aeropostale.com for details. For card balance please call 800-832-0656 or visit your local Aéropostale store. This card is issued by Aero GC Mgmt LLC.\nAéropostale is a registered trademark of ABG-AERO IPCO, LLC. All rights reserved. This Gift Card is subject to Aéropostale’s terms and conditions. Aéropostale is not a participating partner in or sponsor of this offer. www.aeropostale.com',
          termsconditionphysical: null,
        },
        id: 'MER1001513',
      }],
  }];
describe('GiftCards', () => {
  const onSubmit = jest.fn();
  const getDetails = jest.fn();
  const tree = shallow(<GiftCards
    categories={categories}
    onSubmit={onSubmit}
    getDetails={getDetails}
    gtmActions={{
      gtmFilterByEcard: jest.fn(),
      gtmFilterByPhysicalCard: jest.fn(),
      gtmGiftCardCategory: jest.fn(),
      gtmGiftCardDetailView: jest.fn(),
      gtmGiftCardSearch: jest.fn(),
    }}
    details={{
      MER1001513: {
        amount: {
          physicalCard: [{}],
          eCard: [{}],
        },
      },
    }}
  />);

  const event = {
    preventDefault: jest.fn(),
  };
  it('should be defined', () => {
    expect(GiftCards).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render title', () => {
    expect(tree.find('.gift-card-title').length).toBe(1);
  });

  it('should render card types', () => {
    expect(tree.find('.gaf-checkbox-wrap').length).toBe(1);
  });

  it('should call openHandler', () => {
    const simulateClick = tree.find('#card-item00').prop('onClick');
    simulateClick(event);
    expect(tree.state().activeIndex).toBe('00');
  });

  it('should call closeHandler', () => {
    const simulateClick = tree.find('GiftCardDetails').at(0).prop('onClose');
    simulateClick(event);
    expect(tree.state().activeIndex).toBe(-1);
  });

  it('should call filterByHandler ', () => {
    tree.setState({ hasECard: false, hasPhysicalCard: true });
    const simulateChange = tree.find('MultiSelectDropdown').prop('onChangeHandler');
    simulateChange({ itemSelected: [['apparels&shoes', { label: 'apparels&shoes', value: 'apparels&shoes', checked: true }]] });
    expect(tree.state().filterBy).toEqual(['apparels&shoes']);
  });

  it('should call cardTypeChangeHandler', () => {
    const simulateChange = tree.find('Checkbox').at(0).prop('onChange');
    simulateChange({ target: { checked: true } }, 'physicalCard');
    expect(tree.state().hasPhysicalCard).toBe(true);
  });

  it('should call handleSearch', () => {
    const simulateChange = tree.find('#searchBy').prop('onChange');
    simulateChange({ target: { value: 'sample' } });
    expect(tree.state().searchBy).toBe('sample');
  });

  it('should render filter', () => {
    tree.setState({ hasPhysicalCard: false, hasECard: true });
    tree.setState({ categories: [{ title: 'dinning', value: 'dinning' }] });
    expect(tree.find('.redeem-filter-panel').length).toBe(1);
  });
});

describe('GiftCards wihtout details', () => {
  const onSubmit = jest.fn();
  const getDetails = jest.fn();
  const tree = shallow(<GiftCards
    categories={categories}
    onSubmit={onSubmit}
    getDetails={getDetails}
    gtmActions={{
      gtmFilterByEcard: jest.fn(),
      gtmFilterByPhysicalCard: jest.fn(),
      gtmGiftCardCategory: jest.fn(),
      gtmGiftCardDetailView: jest.fn(),
      gtmGiftCardSearch: jest.fn(),
    }}
  />);

  const event = {
    preventDefault: jest.fn(),
  };
  it('should be defined', () => {
    expect(GiftCards).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render title', () => {
    expect(tree.find('.gift-card-title').length).toBe(1);
  });

  it('should render card types', () => {
    expect(tree.find('.gaf-checkbox-wrap').length).toBe(1);
  });

  it('should call openHandler', () => {
    const simulateClick = tree.find('#card-item00').prop('onClick');
    simulateClick(event);
    expect(getDetails).toBeCalled();
  });
});
