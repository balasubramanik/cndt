import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import { constants } from './constants';
import ImageHOC from '../../components/imageHOC';
import Checkbox from '../../components/checkbox';
import GiftCardDetails from '../../components/giftCardDetails';
import MultiSelectDropdown from '../../components/multiSelectDropdown';
import { removeSpecialChar } from '../../utils/utils';

export class GiftCards extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: -1,
      hasEcard: true,
      hasPhysicalCard: true,
      categories: null,
      filterBy: null,
      searchBy: '',
    };
    this.categories = [];
    this.timeout = null;
    this.giftCards = {};
    this.giftCardsDetails = {};
  }

  componentDidUpdate() {
    this.scrollHandler();
  }

  /** @description function to update categories into local state
   * @param {string} name - name of the category
   * @param {boolean} isLast - flag to denotes whether the given category name is last index or not
   */
  setCategories = (name, isLast) => {
    this.categories.push({ title: name, value: name, checked: false });
    const { categories } = this.state;
    if (!categories && isLast) {
      this.setState({ categories: this.categories });
      this.categories = [];
    }
  }

  /** @description function for set the scrolltop
   */
  scrollHandler = () => {
    const { activeIndex } = this.state;
    if (this.giftCardsDetails[activeIndex] && this.giftCards[activeIndex]) {
      const scrollTop = this.giftCards[activeIndex].offsetTop;
      window.scrollTo(0, scrollTop);
    }
  }

  /** @description callback function for open the gift card
   * @param {object} e - triggered event(click)
   * @param {string} activeIndex - selected gift card index
   * @param {string} productName - selected product name
   */
  openHandler = (e, activeIndex, productId, productName) => {
    const { details } = this.props;
    e.preventDefault();
    if (productId && !details[productId]) {
      this.props.getDetails(productId);
    }
    const index = this.state.activeIndex === activeIndex ? -1 : activeIndex;
    this.setState({ activeIndex: index }, () => {
      this.props.gtmActions.gtmGiftCardDetailView(productName);
    });
  }

  /** @description callback function for view button
   * @param {boolen} isViewMore
   */
  viewHandler = (isViewMore) => {
    if (isViewMore) {
      this.scrollHandler();
    }
  }

  /** @description callback function for close the gift card
   */
  closeHandler = () => {
    this.setState({ activeIndex: -1 });
  }

  /** @description callback function for change the card type
   * @param {object} e - triggered event(change)
   * @param {string} type - selected card type
   */
  cardTypeChangeHandler = (e, type) => {
    const { checked } = e.target;
    this.setState({ [type]: checked });
    if (type === 'hasEcard') {
      this.props.gtmActions.gtmFilterByEcard(checked);
    } else {
      this.props.gtmActions.gtmFilterByPhysicalCard(checked);
    }
  }

  /** @description callback function for change the filter type
   * @param {string} value - selected filter type
   */
  filterByCategoryHandler = (value) => {
    const selectedCatergoies = [];
    if (value.itemSelected) {
      value.itemSelected.forEach((item) => {
        if (item[1] && item[1].checked) {
          selectedCatergoies.push(item[0]);
        }
      });
    }
    this.closeHandler();
    this.setState({ filterBy: selectedCatergoies });
    this.props.gtmActions.gtmGiftCardCategory(selectedCatergoies.join(','));
  }

  formatCardTypes = (product) => {
    const { cardTypes } = product;
    return cardTypes && cardTypes.map((type) => {
      const isPhysicalCard = removeSpecialChar(type).toLowerCase() === constants.PHYSICAL_CARD.toLowerCase();
      return isPhysicalCard ? constants.PHYSICAL_CARD : constants.E_CARD;
    });
  }

  /** @description callback function for change search text
   * @param {object} e - triggered event(change)
   */
  handleSearch = (e) => {
    const { value } = e.target;
    this.closeHandler();
    this.setState({ searchBy: value });
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.props.gtmActions.gtmGiftCardSearch(value);
    }, 3000);
  }

  /** @description function to filter the card item based on card type;
   * @param {array} cardTypes - card types
   */
  filterByCardType = (cardTypes) => {
    const { hasPhysicalCard, hasEcard } = this.state;
    return (hasEcard && hasPhysicalCard)
      || (hasPhysicalCard && cardTypes.indexOf(constants.PHYSICAL_CARD) > -1)
      || (hasEcard && cardTypes.indexOf(constants.E_CARD) > -1);
  }

  /** @description function to render the card type
   */
  renderCardTypeFilter = () => (
    <Col md={9} sm={9} xs={12}>
      <div className="gaf-checkbox-wrap gaf-check-box" id="card-type-filter">
        <Col className="c-type-lable">{constants.CARD_TYPE}</Col>
        {constants.CARD_TYPES.map((type, i) => (
          <Checkbox
            key={i.toString()}
            checked={this.state[`has${type.value}`]}
            option={type}
            onChange={(e) => this.cardTypeChangeHandler(e, `has${type.value}`)}
          />
        ))}
      </div>
    </Col>
  );

  /** @description function to render the filter by category
   */
  renderFilterByCategory = () => {
    const { categories } = this.state;
    return (
      <Col lg={4} md={4} sm={6} xs={12}>
        {categories && categories.length > 0 &&
          <MultiSelectDropdown
            menuItems={categories}
            placeholder={constants.FILTER_BY_CATEGORY}
            onChangeHandler={this.filterByCategoryHandler}
          />}
      </Col>
    );
  }

  /** @description function to render the filter by category
 */
  renderSearchByFilter = () => {
    const { searchBy } = this.state;
    return (
      <Col lg={4} md={4} sm={6} xs={12} className="pull-right">
        <div className="form-group gaf-form gaf-input-icon">
          <input
            type="text"
            className="form-control"
            placeholder="Search by Product"
            id="searchBy"
            maxLength={40}
            value={searchBy}
            onChange={this.handleSearch}
          />
          <p className="search-icon"><span className="icon-search"></span></p>
        </div>
      </Col>
    );
  }

  /** @description function to render the card item
   * @param {string} key - selected card index
   * @param {object} item - selected card details
   */
  renderCardItem = (key, item) => {
    const { details } = this.props;
    const { activeIndex } = this.state;
    const openClass = classNames('main grid', { 'grid-open': activeIndex === key });
    const imageUrl = item.productImage || constants.DEFAULT_GIFT_CARD_IMAGE_URL;
    return (
      <div key={key} className={openClass}>
        <a
          ref={(ele) => { this.giftCards[key] = ele; }}
          id={`card-item${key}`}
          href="/"
          onClick={(e) => this.openHandler(e, key, item.id, item.name)}
        >
          <div className="face">
            <div className="face-title">{item.name}</div>
            <ImageHOC
              isAbsolute={item.productImage}
              src={imageUrl}
              alt={item.name}
              responsive
            />
          </div>
        </a>
        {key === activeIndex && details[item.id] &&
          <GiftCardDetails
            ref={(ele) => { this.giftCardsDetails[key] = ele; }}
            onClose={this.closeHandler}
            onSubmit={this.props.onSubmit}
            cardTypes={this.formatCardTypes(item)}
            formName={item.id}
            details={details[item.id]}
            productName={item.name}
            onViewClick={this.viewHandler}
          />}
      </div>
    );
  }

  /** @description function to render the products based on filter
   * @param {array} products - product details
   * @param {number} i - index of category
   */
  renderProducts = (products, i) => {
    const { searchBy } = this.state;
    let variants = [];
    if (products) {
      products.forEach((item, j) => {
        const key = `${i}${j}`;
        const cardTypes = this.formatCardTypes(item);
        let isVisible = this.filterByCardType(cardTypes);
        if (isVisible) {
          const name = item.name || '';
          const searchText = searchBy || '';
          isVisible = searchBy ? name.toLowerCase().indexOf(searchText.toLowerCase()) > -1 : true;
          if (isVisible) {
            variants = [...variants, this.renderCardItem(key, item)];
          }
        }
      });
    }
    return variants;
  }

  /** @description function to render the all gift cards
   */
  renderGiftCards = () => {
    const { filterBy } = this.state;
    const { categories } = this.props;
    let giftcards = [];
    if (categories) {
      categories.forEach((category, i) => {
        const { displayName, products } = category;
        const isLast = categories.length === i + 1;
        this.setCategories(displayName, isLast);
        if (filterBy && filterBy.length > 0) {
          filterBy.forEach((item) => {
            if (item === displayName) {
              const product = this.renderProducts(products, i);
              if (product && product.length > 0) {
                giftcards = [...giftcards, product];
              }
            }
          });
        } else {
          const product = this.renderProducts(products, i);
          if (product && product.length > 0) {
            giftcards = [...giftcards, product];
          }
        }
      });
    }
    return giftcards;
  }

  /** @description function to render the title of gift card
   */
  renderTitle = () => (
    <Col md={3} sm={3} xs={12}>
      <h3 className="gift-card-title no-pad">{constants.GIFT_CARD}</h3>
    </Col>
  );

  render() {
    const { isLoading } = this.props;
    const giftCards = this.renderGiftCards() || [];
    return (
      <div className="redeem-points-box mt-30">
        <Row className="show-grid redeem-points-block">
          {this.renderTitle()}
          {this.renderCardTypeFilter()}
        </Row>
        <Row>
          <div className="redeem-filter-panel clearfix">
            {this.renderFilterByCategory()}
            {this.renderSearchByFilter()}
          </div>
        </Row>
        <Row className="show-grid">
          <Col className="grid-item mt-30 list-3 list-2">
            {giftCards}
            {giftCards.length === 0 &&
              !isLoading &&
              <div className="rp-text text-danger">{constants.NO_RESULTS_FOUND}</div>}
          </Col>
        </Row>
      </div>
    );
  }
}

/** PropTypes:
 * onSubmit - func - callback function for add to cart
 * getDetails - func - callback function to get gift card details
 * gtmActions - object - actions for google tag manager
 * categories - array - contains the gift card categories
 * details - object - contains the gift card details
 * isLoading - boolean - flag for denotes whether page is loading or not
 */
GiftCards.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  getDetails: PropTypes.func.isRequired,
  gtmActions: PropTypes.object.isRequired,
  categories: PropTypes.array,
  details: PropTypes.object,
  isLoading: PropTypes.bool,
};

GiftCards.defaultProps = {
  categories: [],
  details: {},
};

export default GiftCards;
