const constants = {
  FILTER_BY_CATEGORY: 'Filter by Category',
  GIFT_CARD: 'Gift Card',
  CARD_TYPE: 'Card Type',
  CARD_TYPES: [
    {
      title: 'e-Card',
      value: 'Ecard',
    },
    {
      title: 'Physical Card',
      value: 'PhysicalCard',
    }],
  DEFAULT_GIFT_CARD_IMAGE_URL: 'images/debit-card.jpg',
  NO_RESULTS_FOUND: 'No matching gift cards found. Try again.',
  PHYSICAL_CARD: 'physicalCard',
  E_CARD: 'eCard',
};

export { constants };
