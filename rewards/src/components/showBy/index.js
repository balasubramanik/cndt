import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel } from 'react-bootstrap';
import Dropdown from '../dropdown';
import { searchShowBy } from './constants';

class ShowBy extends PureComponent {
  render() {
    const {
      t1,
      t2,
      showBy,
      id,
    } = searchShowBy;
    const { defaultValue } = this.props;
    return (
      <div className="filterby">
        <div className="form-inline">
          <FormGroup className="gaf-form gaf-select-box showby-select">
            <ControlLabel>{t1.label}</ControlLabel>
            <Dropdown id={id} menuItems={showBy} onChangeHandler={this.props.onChange} value={defaultValue} />
            <span className="input-short-text">{t2.label}</span>
          </FormGroup>
        </div>
      </div>
    );
  }
}
ShowBy.propTypes = {
  onChange: PropTypes.func,
  defaultValue: PropTypes.number,
};
export default ShowBy;
