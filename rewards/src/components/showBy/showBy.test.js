import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ShowBy from '../../components/ShowBy';
configure({ adapter: new Adapter() });
describe('ShowBy', () => {
  const tree = shallow(<ShowBy />);
  it('should be defined', () => {
    expect(ShowBy).toBeDefined();
  });
  it('should render correctly', () => {
    expect(ShowBy).toMatchSnapshot();
  });
  it('should render Dropdown component', () => {
    expect(tree.find('Dropdown').length).toBe(1);
  });
});
