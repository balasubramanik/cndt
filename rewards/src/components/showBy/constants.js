export const searchShowBy = {
  showBy: [
    { label: '25', value: '25' },
    { label: '50', value: '50' },
    { label: '75', value: '75' },
    { label: '100', value: '100' },
  ],
  t1: { label: 'show' },
  t2: { label: 'rows per page' },
  id: 'showBy',
};
