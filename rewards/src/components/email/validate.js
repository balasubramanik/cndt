import { errorMessages } from '../../containers/ems/Constants';

const getEmail = (value) => value ? value.trim() : '';

const checkGafDomain = (email, obj) => {
  const error = { ...obj };
  if (email && /^[A-Z0-9._%+-]+@gaf.[A-Z]{2,4}$/i.test(email)) {
    error.email = errorMessages.INVALID_EMAIL_WITH_GAF_DOMAIN;
  }
  return error;
};

const checkValidConfirmEmail = (email, confirmEmail, obj) => {
  const error = { ...obj };
  if (confirmEmail && email.toLowerCase() !== confirmEmail.toLowerCase()) {
    error.confirmEmail = errorMessages.INVALID_CONFIRM_EMAIL;
  }
  return error;
};

const validate = (values) => {
  let errors = {};
  const emailAddress = getEmail(values.email);
  const confirmEmailAddress = getEmail(values.confirmEmail);
  if (!emailAddress) {
    errors.email = errorMessages.REQUIRED_FIELD;
  }

  if (emailAddress && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(emailAddress)) {
    errors.email = errorMessages.INVALID_EMAIL;
  }

  errors = checkGafDomain(emailAddress, errors);

  if (values.step === 0) {
    if (!confirmEmailAddress) {
      errors.confirmEmail = errorMessages.REQUIRED_FIELD;
    }
    errors = checkValidConfirmEmail(emailAddress, confirmEmailAddress, errors);
  }
  return errors;
};

const warnings = (values) => {
  let warn = {};
  const emailAddress = getEmail(values.email);
  const confirmEmailAddress = getEmail(values.confirmEmail);
  warn = checkGafDomain(emailAddress, warn);
  if (values.step === 0) {
    warn = checkValidConfirmEmail(emailAddress, confirmEmailAddress, warn);
  }
  return warn;
};

export { validate, warnings };
