import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Email from '../../components/email';


configure({ adapter: new Adapter() });

describe('Email', () => {
  const tree = shallow(<Email isCompany={false} />);
  it('should be defined', () => {
    expect(Email).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render email', () => {
    expect(tree.find('#email').length).toBe(1);
  });

  it('should equal to column width', () => {
    expect(tree.find('Col').at(0).prop('md')).toBe(12);
  });

  it('should render confirmEmail', () => {
    expect(tree.find('#confirmEmail').length).toBe(1);
  });
});

describe('Email with isCompany true', () => {
  const tree = shallow(<Email isCompany={true} />);
  it('should be defined', () => {
    expect(Email).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render email', () => {
    expect(tree.find('#email').length).toBe(1);
  });

  it('should equal to column width', () => {
    expect(tree.find('Col').at(0).prop('md')).toBe(6);
  });

  it('should render confirmEmail', () => {
    expect(tree.find('#confirmEmail').length).toBe(1);
  });
});

