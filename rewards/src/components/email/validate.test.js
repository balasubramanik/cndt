import { validate, warnings } from './validate';
import { errorMessages } from '../../containers/ems/Constants';

describe('validate', () => {
  it('should be equal to invalid email with empty', () => {
    const errors = validate({});
    expect(errors.email).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid email', () => {
    const errors = validate({ email: 'sample.com' });
    expect(errors.email).toBe(errorMessages.INVALID_EMAIL);
  });

  it('should be equal to invalid confirmEmail', () => {
    const errors = validate({ step: 0 });
    expect(errors.confirmEmail).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid email', () => {
    const errors = validate({ email: 'john@gaf.com' });
    expect(errors.email).toBe(errorMessages.INVALID_EMAIL_WITH_GAF_DOMAIN);
  });

  it('should be equal to valid email', () => {
    const errors = validate({ email: 'john@test.com' });
    expect(errors.email).toBe(undefined);
  });

  it('should be equal to invalid confirmEmail', () => {
    const errors = validate({ email: 'john@sample.com', confirmEmail: 'john@test.com', step: 0 });
    expect(errors.confirmEmail).toBe(errorMessages.INVALID_CONFIRM_EMAIL);
  });

  it('should be equal to valid confirmEmail', () => {
    const errors = validate({ email: 'john@test.com', confirmEmail: 'john@test.com', step: 0 });
    expect(errors.confirmEmail).toBe(undefined);
  });

  it('should be equal to valid confirmEmail warning', () => {
    const errors = warnings({ email: 'john@test.com', confirmEmail: 'john@test.com', step: 0 });
    expect(errors.warn).toBe(undefined);
  });

  it('should be equal to valid confirmEmail warning', () => {
    const errors = warnings({ email: 'john@test.com', confirmEmail: 'john@test.com', step: 1 });
    expect(errors.warn).toBe(undefined);
  });
});
