import React from 'react';
import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';
import { Field } from 'redux-form';
import TextInput from '../textInput';
import { email } from './constants';

/** @description Functional Component to render the Email */
const Email = ({ editable, isCompany }) => {
  const emailLabel = `${email.EMAILADDRESS}<sup>*</sup>`;
  const confirmLabel = `${email.CONFIRM_EMAILADDRESS}<sup>*</sup>`;
  const emailColumn = isCompany ? { md: 6 } : { md: 12 };
  return (
    <React.Fragment>
      <Col {...emailColumn}>
        <Field
          className="gaf-form"
          name="email"
          id="email"
          type="text"
          readOnly={!editable}
          component={TextInput}
          label={emailLabel}
          maxLength={70}
        />
      </Col>
      {editable &&
        <Col md={6}>
          <Field
            className="gaf-form"
            name="confirmEmail"
            id="confirmEmail"
            type="text"
            component={TextInput}
            label={confirmLabel}
            maxLength={70}
          />
        </Col>}
    </React.Fragment>
  );
};

/** PropTypes:
 * editable - boolean - represents email field is editable/non-editable
 * isCompany - boolean - flag to represents the field whether it is company or not
 */
Email.propTypes = {
  editable: PropTypes.bool,
  isCompany: PropTypes.bool,
};

Email.defaultProps = {
  editable: true,
  isCompany: true,
};

export default Email;
