import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

class TextArea extends PureComponent {
  constructor() {
    super();
    this.state = {
      value: '',
      length: 0,
    };
  }

  componentDidUpdate = (prevProps) => {
    const { input } = prevProps;
    const { value } = input;
    if (value !== this.props.input.value) {
      this.textCountHandle();
    }
  }

  textCountHandle = () => {
    const { input } = this.props;
    const { value } = input;
    this.setState({
      length: value.length,
      value,
    });
  }

  handleChange = (event) => {
    const { restrictFormat, pattern, input } = this.props;
    if (pattern) {
      this.setState({
        value: restrictFormat(event.target.value),
        length: event.target.value.length,
      }, () => {
        const { value } = this.state;
        input.onChange(value);
      });
    } else {
      this.setState({
        value: event.target.value,
        length: event.target.value.length,
      }, () => {
        const { value } = this.state;
        input.onChange(value);
      });
    }
  }

  render() {
    const {
      input, meta, rows, cols, maxLength, placeholder, disabled,
    } = this.props;
    const { value } = this.state;
    const { touched, error } = meta;
    const inputClass = classNames('form-control', { 'input-error': touched && error });
    return (
      <div className="open other-desc-textarea">
        <textarea
          {...input}
          className={inputClass}
          rows={rows}
          cols={cols}
          value={value}
          maxLength={maxLength}
          onChange={this.handleChange}
          handleInputDataChange={this.props.handleInputDataChange}
          placeholder={placeholder}
          readOnly={disabled}
        >
        </textarea>
        <p className="char-count">{this.state.length}
          {maxLength ? <span>{`/${maxLength}`}</span> : null}
        </p>
        {touched && error && <span className="error-message text-danger">{error}</span>}
      </div>
    );
  }
}
/** PropTypes:
 * handleInputDataChange- func - callback function for inputData in text area.
 */

TextArea.propTypes = {

  input: PropTypes.object,
  meta: PropTypes.object,
  rows: PropTypes.number,
  cols: PropTypes.number,
  maxLength: PropTypes.number,
  length: PropTypes.number,
  placeholder: PropTypes.string,
  restrictFormat: PropTypes.func,
  pattern: PropTypes.bool,
  handleInputDataChange: PropTypes.func,
  disabled: PropTypes.bool,
};
TextArea.defaultProps = {
  meta: {},
  input: { onChange: () => { } },
};
export default TextArea;
