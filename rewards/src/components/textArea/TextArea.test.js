import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TextArea from '../../components/textArea';


configure({ adapter: new Adapter() });

describe('TextArea', () => {
  const restrictFormat = jest.fn();
  const handleinputdatachange = jest.fn();
  const tree = shallow(<TextArea
    row={8}
    cols={20}
    input={{
      onChange: jest.fn(),
      value: 'abc',
    }}
    meta={{ touched: true, error: true }}
    pattern
    countPosition="3"
    maxLength={100}
    restrictFormat={restrictFormat}
    handleinputdatachange={handleinputdatachange}
  />);

  it('should be defined', () => {
    expect(TextArea).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should get correct input length', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      input: {
        value: 'abcd',
      },
    });
    expect(tree.find('p').text()).toBe('4/100');
  });
  it('Called function handleChange', () => {
    const e = {
      target: {
        value: '',
        length: 0,
      },
    };
    tree.instance().handleChange(e);
  });
});


describe('TextArea with no change to input', () => {
  const restrictFormat = jest.fn();
  const handleinputdatachange = jest.fn();
  const tree = shallow(<TextArea
    row={8}
    cols={20}
    input={{
      onChange: jest.fn(),
      value: 'xyz',
    }}
    meta={{ touched: true, error: true }}
    pattern={false}
    countPosition="next"
    maxLength={20}
    restrictFormat={restrictFormat}
    handleinputdatachange={handleinputdatachange}
  />);

  it('should get correct input length', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      input: {
        value: 'xyz',
      },
    });
    expect(tree.state().length).toBe(3);
  });
  it('Called function handleChange', () => {
    const e = {
      target: {
        value: '',
        length: 0,
      },
    };
    tree.instance().handleChange(e);
  });
});

describe('TextArea with maxlength of zero', () => {
  const restrictFormat = jest.fn();
  const handleinputdatachange = jest.fn();
  const tree = shallow(<TextArea
    row={8}
    cols={20}
    input={{
      onChange: jest.fn(),
      value: 'xyz',
    }}
    meta={{ touched: true, error: true }}
    pattern={false}
    countPosition="next"
    maxLength={0}
    restrictFormat={restrictFormat}
    handleinputdatachange={handleinputdatachange}
  />);

  it('Called function handleChange', () => {
    const e = {
      target: {
        value: '',
        length: 0,
      },
    };
    tree.instance().handleChange(e);
  });
});
