import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TextInput from '../../components/textInput';


configure({ adapter: new Adapter() });

describe('TextInput with touched true', () => {
  const event = {
    target: {
      values: '',
    },
  };
  const blurHandler = jest.fn();
  const tree = shallow(<TextInput
    id="gaf-input"
    type="text"
    input={{ value: '', onBlur: blurHandler }}
    className="gaf-form-control"
    label="Username"
    meta={{ touched: true, error: true }}
  />);

  it('should be defined', () => {
    expect(TextInput).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label element', () => {
    expect(tree.find('label').length).toBe(1);
  });

  it('should render input element', () => {
    expect(tree.find('#gaf-input').length).toBe(1);
  });

  it('should render input element', () => {
    expect(tree.find('#gaf-input').length).toBe(1);
  });

  it('should call blur handler', () => {
    const simulateChange = tree.find('#gaf-input').prop('onBlur');
    simulateChange(event);
    expect(blurHandler).toBeCalled();
  });
});

describe('TextInput with touched false', () => {
  const tree = shallow(<TextInput
    id="gaf-input"
    type="text"
    input={{ value: '' }}
    label="Username"
    meta={{ touched: false, error: false }}
  />);

  it('should be defined', () => {
    expect(TextInput).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label element', () => {
    expect(tree.find('label').length).toBe(1);
  });

  it('should render input element', () => {
    expect(tree.find('#gaf-input').length).toBe(1);
  });

  it('should render input element', () => {
    expect(tree.find('#gaf-input').length).toBe(1);
  });
});
