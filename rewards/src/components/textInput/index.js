import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { trim } from '../../utils/utils';

/** @description Callback function for input blur
 * @param {object} event - triggered event(blur)
 * @param {object} input - input object which is comes from redux-form
 */
const blurHandler = (event, input) => {
  const { target } = event;
  const { value } = target;
  const trimmedValue = trim(value);
  input.onBlur(trimmedValue);
};

/** @description Functional component to render the text input field */
const TextInput = (props) => {
  const {
    input, label, meta, type, placeholder, id, className = '', readOnly, maxLength,
  } = props;
  const {
    touched, error, warning, visited,
  } = meta;
  const isInvalidField = (touched && error) || (visited && warning);
  const inputClass = classNames('form-control', { 'input-error': isInvalidField });
  const inputContainerClass = classNames(className, { 'error-highlighter': touched && error });
  return (
    <div className={inputContainerClass}>
      {label && <label htmlFor={id} dangerouslySetInnerHTML={{ __html: label }} />}
      <input
        className={inputClass}
        maxLength={maxLength}
        id={id}
        type={type}
        placeholder={placeholder}
        disabled={readOnly}
        {...input}
        onBlur={(event) => blurHandler(event, input)}
      />
      {(touched && error && error.length > 0 && <span className="error-message text-danger">{error}</span>) ||
        (visited && warning && warning.length > 0 && <span className="error-message text-danger">{warning}</span>)}
    </div>
  );
};

/** PropTypes:
 * id - string - unique identifier
 * type - string - type of input element
 * className - string - for customizing the element
 * label - string - label name
 * input - object - property comes from redux-form
 * maxLength - number - max length of element
 * meta - object - validation property comes from redux-form
 * placeholder - string - placeholder text of element
 * readOnly - boolean - represents the field is editable/non-editable
 */
TextInput.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
  label: PropTypes.string,
  input: PropTypes.object,
  maxLength: PropTypes.number,
  meta: PropTypes.object,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
};

TextInput.defaultProps = {
  input: {},
  meta: {},
};

export default TextInput;
