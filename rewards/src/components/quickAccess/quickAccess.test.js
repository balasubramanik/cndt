import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import QuickAccess from '../quickAccess';


configure({ adapter: new Adapter() });

describe('QuickAccess', () => {
  const onQuickAccess = jest.fn();
  const tree = shallow(<QuickAccess
    coreFeatures={[]}
    onQuickAccess={onQuickAccess}
  />);

  it('should be defined', () => {
    expect(QuickAccess).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render desktop quick links', () => {
    expect(tree.find('.hidden-xs').length).toBe(3);
  });

  it('should render quick links button', () => {
    expect(tree.find('.btn').length).toBe(3);
  });

  it('should call onQuickAccess for desktop', () => {
    const quickAccessHandler = tree.find('.btn').at(0).prop('onClick');
    quickAccessHandler();
    expect(onQuickAccess).toBeCalled();
  });

  it('should call onQuickAccess for mobile', () => {
    const quickAccessHandler = tree.find('a').at(0).prop('onClick');
    quickAccessHandler();
    expect(onQuickAccess).toBeCalled();
  });

  it('should render mobile quick links', () => {
    expect(tree.find('.visible-xs').length).toBe(3);
  });
});

describe('QuickAccess without links', () => {
  const onQuickAccess = jest.fn();
  const tree = shallow(<QuickAccess />);

  it('should be defined', () => {
    expect(QuickAccess).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render desktop quick links', () => {
    expect(tree.find('.hidden-xs').length).toBe(0);
  });

  it('should not render mobile quick links', () => {
    expect(tree.find('.visible-xs').length).toBe(0);
  });
});
