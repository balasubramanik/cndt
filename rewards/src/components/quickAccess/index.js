import React from 'react';
import PropTypes from 'prop-types';
import { quickAccessConfig } from './constants';

/** @description function to render desktop quick links */
const renderDesktopLinks = (item, i, onQuickAccess) => (
  <div key={`desktop${i.toString()}`} className="quick-access hidden-xs">
    <div className="row">
      <div className="col-md-3 col-lg-2 col-sm-3 col-xs-3">
        <div className="quality-access-vcenter">
          <div className="quick-access-icon">
            <span className={item.icon}></span>
          </div>
        </div>
      </div>
      <div className="col-md-5 col-lg-6 col-sm-5 col-xs-7" >
        <div className="quality-access-vcenter" >
          <p >{item.description}</p>
        </div>
      </div>
      <div className="col-md-4 col-lg-4 col-sm-4 col-xs-2">
        <div className="quality-access-vcenter">
          <button
            className="btn gaf-btn-primary btn-block qa-btn"
            onClick={(e) => onQuickAccess(e, item.url)}
          >
            {item.title}
          </button>
        </div>
      </div>
    </div>
  </div>
);

/** @description function to render desktop quick links */
const renderMobileLinks = (item, i, onQuickAccess) => (
  <div key={`mobile${i.toString()}`} className="quick-access visible-xs">
    <div className="row">
      <a href="/" onClick={(e) => onQuickAccess(e, item.url)}>
        <div className="col-md-3 col-lg-2 col-sm-3 col-xs-3">
          <div className="quality-access-vcenter">
            <div className="quick-access-icon">
              <span className={item.icon}></span>
            </div>
          </div>
        </div>
        <div className="col-md-5 col-lg-6 col-sm-5 col-xs-7" >
          <div className="quality-access-vcenter" >
            <p>{item.title}</p>
          </div>
        </div>
        <div className="col-md-4 col-lg-4 col-sm-4 col-xs-2">
          <div className="quality-access-vcenter">
            <span className="icon-arrow"></span>
          </div>
        </div>
      </a>
    </div>
  </div>
);

/** @description function to render all quick links */
const renderQuickAccessLinks = (coreFeatures, onQuickAccess) => {
  let links = [];
  if (coreFeatures && quickAccessConfig) {
    const desktopLinks = quickAccessConfig.map((item, i) => renderDesktopLinks(item, i, onQuickAccess));
    const mobileLinks = quickAccessConfig.map((item, i) => renderMobileLinks(item, i, onQuickAccess));
    links = [...desktopLinks, ...mobileLinks];
  }
  return links;
};

/** @description function to render the quick links */
const QuickAccess = (props) => {
  const { coreFeatures, onQuickAccess } = props;
  return (
    <section className="quick-access-list">
      {renderQuickAccessLinks(coreFeatures, onQuickAccess)}
    </section>
  );
};

QuickAccess.propTypes = {
  coreFeatures: PropTypes.array.isRequired,
  onQuickAccess: PropTypes.func.isRequired,
};

export default QuickAccess;
