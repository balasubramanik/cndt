import RouteConstants from '../../constants/RouteConstants';

const quickAccess = {
  WHATQUALIFIES: 'WHAT QUALIFIES',
  SUBMITINVOICE: 'SUBMIT INVOICES',
  REDEEMPOINTS: 'REDEEM POINTS',
  VIEWCURRENT: 'My Plan and Promotions',
  DIGITAL: 'Upload or Mail Invoices',
  REWARDS: 'Choose Your Rewards',
};

const quickAccessConfig = [
  {
    description: quickAccess.VIEWCURRENT,
    title: quickAccess.WHATQUALIFIES,
    icon: 'icon-star',
    url: RouteConstants.WHAT_QUALIFIES,
  },
  {
    description: quickAccess.DIGITAL,
    title: quickAccess.SUBMITINVOICE,
    icon: 'icon-upload',
    url: RouteConstants.SUBMIT_INVOICE,
  },
  {
    description: quickAccess.REWARDS,
    title: quickAccess.REDEEMPOINTS,
    icon: 'icon-star-rewards',
    url: RouteConstants.REDEEM_POINTS,
  },
];

export { quickAccess, quickAccessConfig };
