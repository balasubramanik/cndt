import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Checkbox from '../../components/checkbox';


configure({ adapter: new Adapter() });

describe('Checkbox with onChange', () => {
  const changeHandler = jest.fn();
  const tree = shallow(<Checkbox
    className="gaf-form-control"
    name="username"
    onChange={changeHandler}
    option={{ title: 'item', value: 'item' }}
  />);

  it('should be defined', () => {
    expect(Checkbox).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label element', () => {
    expect(tree.find('label').length).toBe(1);
  });

  it('should render input element', () => {
    expect(tree.find('input').length).toBe(1);
  });

  it('should call Change handler', () => {
    const simulateChange = tree.find('input').prop('onChange');
    simulateChange();
    expect(changeHandler).toBeCalled();
  });
});

describe('Checkbox without onChange', () => {
  const changeHandler = jest.fn();
  const tree = shallow(<Checkbox
    className="gaf-form-control"
    name="username"
    option={{ title: 'item', value: 'item' }}
  />);

  it('should be defined', () => {
    expect(Checkbox).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label element', () => {
    expect(tree.find('label').length).toBe(1);
  });

  it('should render input element', () => {
    expect(tree.find('input').length).toBe(1);
  });

  it('should call Change handler', () => {
    const simulateChange = tree.find('input').prop('onChange');
    simulateChange();
    expect(changeHandler).not.toBeCalled();
  });
});
