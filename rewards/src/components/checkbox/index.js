import React from 'react';
import PropTypes from 'prop-types';

/** @description Functional component to render the Checkbox field */
const Checkbox = (props) => {
  const {
    option, input, className, onChange, checked, name, disabled,
  } = props;
  const changeHandler = input.onChange || onChange;
  const nameHandler = input.name || name;
  return (
    <div className={`gaf-checkbox ${className}`}>
      <label className="control control-checkbox">
        <span dangerouslySetInnerHTML={{ __html: option.title }} />
        <input
          type="checkbox"
          {...input}
          checked={checked}
          onChange={changeHandler}
          name={nameHandler}
          disabled={disabled}
        />
        <div className="indicator"></div>
      </label>
    </div>
  );
};

/** PropTypes:
 * option - array - contains the checkbox configurations
 * name - string - name of checkbox element
 * className - string - for customizing the checkbox element
 * checked - boolean - denotes whether checkbox is checked or not
 * input - object - property comes from redux-form
 * disabled - boolean - represents the field is editable/non-editable
 */
Checkbox.propTypes = {
  option: PropTypes.shape({
    title: PropTypes.string.isRequired,
    value: PropTypes.string,
  }),
  onChange: PropTypes.func,
  className: PropTypes.string,
  checked: PropTypes.bool,
  input: PropTypes.object,
  name: PropTypes.string,
  disabled: PropTypes.bool,
};

Checkbox.defaultProps = {
  input: {},
  checked: false,
  onChange: () => { },
};

export default Checkbox;
