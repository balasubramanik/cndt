const tableHeaders = {
  listTranslation: [
    { title: 'CLAIM/ORDER #', key: 'transactionId', sort: true },
    { title: 'TYPE', key: 'type', sort: true },
    { title: 'POINTS', key: 'points', sort: true },
    { title: 'USER', key: 'user', sort: true },
    { title: 'DATE', key: 'date', sort: true },
    { title: 'DETAILS', key: 'details', sort: false },
  ],
  countPerPage: [
    { title: '25', value: '25' },
    { title: '50', value: '50' },
    { title: '75', value: '75' },
    { title: '100', value: '100' },
  ],
  text: {
    Tabletitle: 'Earned/Redeemed Points', Rows: 'rows per page', Export: 'Export', filename: 'PointsHistory.csv',
  },

  csvConfig: [
    { colName: 'CLAIM/ORDER #', key: 'transactionId' },
    { colName: 'TYPE', key: 'type' },
    { colName: 'POINTS', key: 'points' },
    { colName: 'USER', key: 'user' },
    { colName: 'DATE', key: 'date' },
  ],
};

export { tableHeaders };
