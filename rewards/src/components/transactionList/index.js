import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import ToolTip from '../toolTip';
import RouteConstants from '../../constants/RouteConstants';
import { history, authorization } from '../../routes';
import Tables from '../../components/tables';
import CSV from '../../components/csv';
import { tableHeaders } from './constants';
import { formatDateUTC, formatPoint } from '../../utils/utils';

class TransactionList extends React.Component {
  /* Redirect to Order Details Or Claim Status Page */
  redirectToOrderDetails = (e, row) => {
    e.preventDefault();
    if (row.type === 'Earned') {
      if (authorization.userRolesAccess && authorization.checkAccess(RouteConstants.CLAIM_DETAILS.replace(':claimId', row.id))) {
        history.push({
          pathname: RouteConstants.CLAIM_DETAILS.replace(':claimId', row.id),
          state: {
            id: row.id,
          },
        });
      } else {
        this.props.accessPage(true);
      }
      return;
    }
    this.props.viewTransactionDetails(row.id);
    history.push(RouteConstants.ORDER_DETAILS.replace(':orderId', row.id));
  };

  /* Format the Transaction List Table data */
  formatData = (records) => {
    if (!records || records.length === 0) return [];
    return records.map((record) => {
      const {
        date, points, user,
      } = record;
      return {
        ...record,
        date: formatDateUTC(date) || 'N/A',
        points: formatPoint(points) || 'N/A',
        user: <ToolTip id={user || 'N/A'} text={user || 'N/A'}><span role="button">{user || 'N/A'}</span></ToolTip>,
        details: <button className="btn gaf-btn-secondary gaf-btn-sm" onClick={(event) => this.redirectToOrderDetails(event, record)}>View</button>,
      };
    });
  }

  /* Creating Transactions List CSV file */
  createPointsHistoryCSV = () => {
    const { isAsyncExportCSV = false, exportCSVData = [], data = [] } = this.props;
    const csvData = JSON.parse(isAsyncExportCSV) ? exportCSVData : data;
    if (!csvData || csvData.length === 0) return [];
    return csvData.map((record) => {
      const {
        date, points, user,
      } = record;
      return {
        ...record,
        date: formatDateUTC(date) || 'N/A',
        points: formatPoint(points) || 'N/A',
        user: user || 'N/A',
      };
    });
  }

  tableHeadersContent = () => (
    <Row className="show-grid">
      <Col xs={8} sm={8} md={4} lg={6}>
        <h4>{tableHeaders.text.Tabletitle}</h4>
      </Col>
      <Col xs={4} sm={4} md={8} lg={6}>
        {this.renderCSVLink()}
      </Col>
    </Row>);

  renderCSVLink = () => {
    const { data, triggerDownload, isAsyncExportCSV } = this.props;
    const CSVRecords = this.createPointsHistoryCSV();
    return (
      <div className="gaf-export-block">
        <CSV
          filename={tableHeaders.text.filename}
          config={tableHeaders.csvConfig}
          data={CSVRecords}
          onClick={this.props.CSVHandler}
          isAsync={isAsyncExportCSV}
          triggerDownload={triggerDownload}
          disabled={!data || data.length === 0}
        >
          <div className="pull-right">
            <span><i className="icon-Export-Icon"></i><span>{tableHeaders.text.Export}</span></span>
          </div>
        </CSV>
      </div>
    );
  }


  render() {
    const { data, isloading, defaultSort } = this.props;
    return (
      <div>
        <div className="gaf-table-header">
          {this.tableHeadersContent()}
        </div>
        <Tables config={tableHeaders.listTranslation} data={this.formatData(data)} isLoading={isloading} onSortingClick={(key, isAsc) => this.props.sortingData(key, isAsc)} sortByKey={defaultSort} />
      </div>
    );
  }
}

TransactionList.propTypes = {
  data: PropTypes.array,
  sortingData: PropTypes.func,
  isloading: PropTypes.bool,
  CSVHandler: PropTypes.func,
  isAsyncExportCSV: PropTypes.string,
  exportCSVData: PropTypes.array,
  defaultSort: PropTypes.string,
  viewTransactionDetails: PropTypes.func,
  accessPage: PropTypes.func,
  triggerDownload: PropTypes.bool,
};

export default TransactionList;
