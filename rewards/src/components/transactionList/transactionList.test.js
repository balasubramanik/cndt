import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TransactionList from '../../components/transactionList';
import { history } from '../../routes';

configure({ adapter: new Adapter() });
history.push = jest.fn();
describe('TransactionList with props', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const sorthandler = jest.fn();
  const csvfilehandler = jest.fn();
  const refhandler = jest.fn();
  const viewtransactionhandler = jest.fn();
  const tree = shallow(<TransactionList
    data={[{
      id: 1,
      type: 'Earned',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    },
    {
      id: 2,
      type: 'Earned',
      points: '+ 250.00',
      user: 'J. Doe',
      date: '1534411158174',
    }]}
    sortingData={sorthandler}
    isLoading
    CSVHandler={csvfilehandler}
    onRef={refhandler}
    isAsyncExportCSV="true"
    exportCSVData={[{
      id: 1,
      type: 'Earned',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    },
    {
      id: 2,
      type: 'Earned',
      points: '+ 250.00',
      user: 'J. Doe',
      date: '1534411158174',
    }]}
    defaultSort="Date"
    viewTransactionDetails={viewtransactionhandler}
  />);


  it('should be defined', () => {
    expect(TransactionList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Tables Component', () => {
    expect(tree.find('Tables').length).toBe(1);
  });

  it('should handle CSV click', () => {
    const simulateChange = tree.find('a').at(0).prop('onClick');
    simulateChange(event);
    expect(csvfilehandler).toBeCalled();
  });
});
describe('TransactionList with empty date and points', () => {
  const sorthandler = jest.fn();
  const csvfilehandler = jest.fn();
  const refhandler = jest.fn();
  const viewtransactionhandler = jest.fn();
  const tree = shallow(<TransactionList
    data={[{
      id: 1,
      type: 'Earned',
      points: null,
      user: 'J. Doe1',
      date: '',
    },
    {
      id: 2,
      type: 'Earned',
      points: null,
      user: 'J. Doe',
      date: '',
    }]}
    sortingData={sorthandler}
    isLoading
    CSVHandler={csvfilehandler}
    onRef={refhandler}
    isAsyncExportCSV="true"
    exportCSVData={[{
      id: 1,
      type: 'Redeemed',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    },
    {
      id: 2,
      type: 'Earned',
      points: '+ 250.00',
      user: 'J. Doe',
      date: '1534411158174',
    }]}
    defaultSort="Date"
    viewTransactionDetails={viewtransactionhandler}
  />);

  it('should be defined', () => {
    expect(TransactionList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
describe('TransactionList mounted', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const sorthandler = jest.fn();
  const csvfilehandler = jest.fn();
  const refhandler = jest.fn();
  const viewtransactionhandler = jest.fn();
  const tree = mount(<TransactionList
    data={[{
      id: 1,
      type: 'Earned',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    },
    {
      id: 2,
      type: 'Earned',
      points: '+ 250.00',
      user: 'J. Doe',
      date: '1534411158174',
    }]}
    sortingData={sorthandler}
    isLoading
    CSVHandler={csvfilehandler}
    onRef={refhandler}
    isAsyncExportCSV="true"
    exportCSVData={[{
      id: 1,
      type: 'Redeemed',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    },
    {
      id: 2,
      type: 'Earned',
      points: '+ 250.00',
      user: 'J. Doe',
      date: '1534411158174',
    }]}
    defaultSort="Date"
    viewTransactionDetails={viewtransactionhandler}
  />);

  it('should redirect to view Claim details', () => {
    const simulateChange = tree.find('button').at(0).prop('onClick');
    simulateChange(event, {
      id: 1,
      type: 'Earned',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    });
    expect(history.push).toBeCalled();
  });

  it('should redirect to view Transaction details', () => {
    const simulateChange = tree.find('button').at(0).prop('onClick');
    simulateChange(event, {
      id: 1,
      type: 'Redeemed',
      points: '+ 50.00',
      user: 'J. Doe1',
      date: '1534411158174',
    });
    expect(history.push).toBeCalled();
  });
});
describe('TransactionList without props', () => {
  const tree = shallow(<TransactionList />);

  it('should be defined', () => {
    expect(TransactionList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Transaction List Component', () => {
    expect(tree.find('tr').length).toBe(0);
  });
});
