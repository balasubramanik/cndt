import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { tables } from './constants';

/** @description Class component to render the Table */
class Tables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowToggle: false,
      fieldName: props.sortByKey || '',
    };
  }

  /** @description Function available for handling the toggle sort  */
  toggleHandler = (e, key) => {
    e.preventDefault();
    const { isShowToggle, fieldName } = this.state;
    this.sortField = key;
    this.setState({
      fieldName: key,
      isShowToggle: fieldName === key ? !isShowToggle : false,
    }, () => {
      this.props.onSortingClick(key, this.state.isShowToggle);
    });
  }

  render() {
    const { isShowToggle, fieldName } = this.state;
    const {
      config,
      data,
      isLoading,
    } = this.props;
    const isRecordsFound = data && data.length > 0;
    return (
      <div className="gaf-table gaf-scrollbar">
        <Table responsive bordered>
          <thead>
            <tr>
              {config && config.map((item, i) => {
                const sortClass = classNames('arrow-sort', { 'arrow-active': item.key.toString().toLowerCase() === fieldName.toString().toLowerCase() });
                let arrow = null;
                if (item.key.toString().toLowerCase() === fieldName.toString().toLowerCase()) {
                  arrow = isShowToggle ? <span className={`arrow-ascending ${sortClass}`} /> : <span className={`arrow-descending ${sortClass}`} />;
                } else {
                  arrow = (<React.Fragment><span className={`arrow-ascending ${sortClass}`} /><span className={`arrow-descending ${sortClass}`} /></React.Fragment>);
                }
                return (
                  isRecordsFound && item.sort ? <th key={i.toString()}><a href="/" onClick={(e) => this.toggleHandler(e, item.key)}>{item.title} {arrow}</a></th>
                    : <th key={i.toString()}>{item.title}</th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {isRecordsFound && data && data.map((row, i) => (
              <tr key={i.toString()}>
                {config && config.map((item, j) => (
                  <td
                    key={j.toString()}
                    data-col-name={`${item.title}:`}
                  >
                    {row[item.key]}
                  </td>
                ))}
              </tr>
            ))}
            {!isRecordsFound &&
              (isLoading !== undefined && !isLoading) &&
              <tr>
                <td
                  colSpan={config.length}
                  className="text-danger text-center no-record-found"
                >
                  {tables.NO_MATCHING}
                </td>
              </tr>}
          </tbody>
        </Table>
      </div>
    );
  }
}

/** PropTypes:
 * config - configuration supplied to the table
 * data - Array of data supplied to the table component
 * onSortingClick - Function to handle the sorting functionality
 */

Tables.propTypes = {
  config: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  onSortingClick: PropTypes.func,
  isLoading: PropTypes.bool,
  sortByKey: PropTypes.string,
};

export default Tables;
