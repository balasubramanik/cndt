const tables = {
  PRODUCTS: 'PRODUCTS',
  RATE: 'RATE',
  START_DATE: 'START DATE',
  END_DATE: 'END DATE',
  NO_MATCHING: 'No matching records found.',
};

export { tables };
