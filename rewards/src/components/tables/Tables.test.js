import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Tables from '../../components/tables';


configure({ adapter: new Adapter() });

describe('Table with props', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const sortHandler = jest.fn();
  const tree = shallow(<Tables
    config={[
      {
        title: 'ACCOUNT #', key: 'cId', sort: true, align: 'right',
      },
      {
        title: 'CONTRACTOR NAME', key: 'cName', sort: true, align: 'right',
      },
      {
        title: 'CITY', key: 'cty', sort: false, align: '',
      },
    ]}
    data={[
      {
        cId: '111',
        cName: 'XXX',
        cty: 'CCC',
      },
      {
        cId: '222',
        cName: 'BBB',
        cty: 'SSS',
      },
    ]}
    isLoading
    onSortingClick={sortHandler}
    sortByKey="cName"
  />);
  it('should be defined', () => {
    expect(Tables).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render a valid table', () => {
    expect(tree.find('Table').length).toBe(1);
  });

  it('should render table header correctly', () => {
    expect(tree.find('thead').length).toBe(1);
  });

  it('should render table body correctly', () => {
    expect(tree.find('tbody').length).toBe(1);
  });

  it('should render table rows correctly', () => {
    expect(tree.find('tr').length).toBe(3);
  });
  it('should call onClick event', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event, 'cName');
    expect(sortHandler).toBeCalled();
  });
  it('should change isShowToggle value', () => {
    tree.setState({ isShowToggle: false });
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event, 'cName');
    expect(tree.state().isShowToggle).toEqual(true);
    expect(sortHandler).toBeCalled();
  });
});

describe('Table with no data', () => {
  const tree = shallow(<Tables
    config={[]}
    data={[]}
  />);

  it('should render a valid table', () => {
    expect(tree.find('Table').length).toBe(1);
  });

  it('should render table header correctly', () => {
    expect(tree.find('thead').length).toBe(1);
  });

  it('should render table body correctly', () => {
    expect(tree.find('tbody').length).toBe(1);
  });

  it('should render table rows correctly', () => {
    expect(tree.find('tr').length).toBe(1);
  });
});

describe('Table with isLoading false', () => {
  const sortHandler = jest.fn();
  const tree = shallow(<Tables
    config={[]}
    data={[]}
    isLoading={false}
    onSortingClick={sortHandler}
    sortByKey="cName"
  />);
  it('should display no matching record message', () => {
    expect(tree.find('.no-record-found').text()).toEqual('No matching records found.');
  });
});
