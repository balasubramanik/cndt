import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import envConfig from 'envConfig';//eslint-disable-line
import { uploadInvoice } from './Constants';
import FileUploadProgress from '../../components/fileUploadProgress';

export class UploadInvoice extends PureComponent {
  successCall = () => {
    this.props.callback();
  }

  render() {
    const { userInfo, isOpened, gtmUploadInvoiceActions } = this.props;
    const url = `${envConfig.baseUrl}${envConfig.apiEndPoints.uploadInvoice}`;
    return (
      <div>
        <div className="upload-invoice-content panel-body-content">
          <p>
            <span>{uploadInvoice.DESKTOP}</span>
            {uploadInvoice.CONTENT}
          </p>
          <p>
            <span>{uploadInvoice.MOBILE}</span>
            {uploadInvoice.CONTENT1}
          </p>
          <p>
            {uploadInvoice.INFO}
          </p>
          <p>
            <span>{uploadInvoice.NOTETEXT}</span> {uploadInvoice.NOTE}
          </p>
        </div>
        <div className="invoice-upload-files">
          <FileUploadProgress
            callback={this.successCall}
            allowFormats={uploadInvoice.ALLOWFORMATS}
            maxFileCount={uploadInvoice.MAXFILECOUNT}
            maxFileSize={uploadInvoice.MAXFILESIZE}
            url={url}
            userInfo={userInfo}
            inValidFiles={uploadInvoice.ERRORPLACEHOLDER}
            isOpened={isOpened}
            fileCountExceedMsg={uploadInvoice.FILECOUNTEXCEEDMSG}
            gtmUploadInvoiceActions={gtmUploadInvoiceActions}
          />
        </div>
      </div>
    );
  }
}

UploadInvoice.propTypes = {
  callback: PropTypes.func,
  userInfo: PropTypes.object,
  isOpened: PropTypes.bool,
  gtmUploadInvoiceActions: PropTypes.object,
};

export default UploadInvoice;
