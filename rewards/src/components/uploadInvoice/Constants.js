const uploadInvoice = {
  DESKTOP: 'Desktop: ',
  MOBILE: 'Mobile: ',
  CONTENT: 'Scan your invoice(s) and save them onto your desktop so you an easily find them for uploading. Be sure to save them as a .jpg, .png, or .pdf. When you click on the "UPLOAD" button, choose the invoice(s) you have scanned and saved onto your desktop.',
  CONTENT1: 'Simply take a clear picture of each invoice. When you click on the "UPLOAD" button, choose the picture(s) in your mobile device.',
  INFO: 'Please wait until all invoices are completely uploaded before clicking Submit.',
  NOTE: ' There is a maximum of 20 files or 30 MB per submission.',
  NOTETEXT: 'NOTE:',
  ALLOWFORMATS: ['application/pdf', 'image/png', 'image/jpeg'],
  MAXFILESIZE: 30,
  MAXFILECOUNT: 20,
  ERRORPLACEHOLDER: 'Please upload .jpg, .png, or .pdf document only.',
  FILECOUNTEXCEEDMSG: 'Sorry, you can only upload a maximum of 20 files at a time.',
};

export { uploadInvoice };

