import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import UploadInvoice from '../uploadInvoice';


configure({ adapter: new Adapter() });

describe('UploadInvoice', () => {
  const successFunc = jest.fn();
  const tree = shallow(<UploadInvoice callback={successFunc} />);

  it('should be defined', () => {
    expect(UploadInvoice).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
    tree.instance().successCall();
  });
});
