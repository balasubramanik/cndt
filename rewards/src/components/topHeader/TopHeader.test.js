import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TopHeader from '../../components/topHeader';


configure({ adapter: new Adapter() });

describe('TopHeader with showOverlay true', () => {
  const profileHandler = jest.fn();
  const cartHandler = jest.fn();
  const tree = shallow(
    <TopHeader
      showOverlay={true}
      cartCount={1}
      onProfileClick={profileHandler}
      onCartClick={cartHandler}
    />);
  const event = {
    preventDefault: jest.fn(),
  };
  it('should be defined', () => {
    expect(TopHeader).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render TopHeader container', () => {
    expect(tree.find('.header-top-logo-nav').length).toBe(1);
  });

  it('should render Logo', () => {
    expect(tree.find('Logo').length).toBe(1);
  });

  it('should render ProfileOverlay', () => {
    expect(tree.find('ProfileOverlay').length).toBe(1);
  });

  it('should render Cart Icon', () => {
    expect(tree.find('.icon-cart').length).toBe(1);
  });

  it('should render Badge', () => {
    expect(tree.find('.badge-size').length).toBe(1);
  });

  it('should call cartHandler', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(cartHandler).toBeCalled();
  });

  it('should render User Icon', () => {
    expect(tree.find('.icon-user-avtar').length).toBe(1);
  });

  it('should call profileHandler for click event', () => {
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(profileHandler).toBeCalled();
  });
});

describe('TopHeader with showOverlay false', () => {
  const profileHandler = jest.fn();
  const cartHandler = jest.fn();
  const tree = shallow(
    <TopHeader
      showOverlay={false}
      cartCount={0}
      onProfileClick={profileHandler}
      onCartClick={cartHandler}
    />);
  it('should be defined', () => {
    expect(TopHeader).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render TopHeader container', () => {
    expect(tree.find('.header-top-logo-nav').length).toBe(1);
  });

  it('should render Logo', () => {
    expect(tree.find('Logo').length).toBe(1);
  });

  it('should render ProfileOverlay', () => {
    expect(tree.find('ProfileOverlay').length).toBe(0);
  });

  it('should render Cart Icon', () => {
    expect(tree.find('.icon-cart').length).toBe(1);
  });

  it('should render User Icon', () => {
    expect(tree.find('.icon-user-avtar').length).toBe(1);
  });

  it('should not render Badge', () => {
    expect(tree.find('.badge-size').length).toBe(0);
  });
});
