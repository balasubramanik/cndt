import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Grid, Col, Row, Badge } from 'react-bootstrap';
import Logo from '../../components/logo';
import ProfileOverlay from '../../components/profileOverlay';
import { authorization } from '../../routes';
import { createListener, removeListener, getImpersonatedBy } from '../../utils/utils';
import RouteConstants from '../../constants/RouteConstants';
import ImageHOC from '../imageHOC';


export class TopHeader extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isShowOverlay: false,
    };
  }
  /** @description React life cycle method
     * it will invoke only when component is mounted
     */
  componentDidMount() {
    createListener(document, ['click', 'touchstart'], this.handleOutsideClick, false);
  }

  /** @description React life cycle method
     * It will invoke when the component is unmounted
     */
  componentWillUnmount() {
    removeListener(document, ['click', 'touchstart'], this.handleOutsideClick, false);
  }

  /** @description callback function for document click
    * @param {object} e - triggered event(click)
    */
  handleOutsideClick = (e) => {
    if ((!this.node.contains(e.target))) {
      this.setState({ isShowOverlay: false });
    }
  }

  /** @description callback function for user icon
  * @param {object} e - triggered event(click)
  */
  profileHandler = (e) => {
    const { isShowOverlay } = this.state;
    e.preventDefault();
    this.setState({ isShowOverlay: !isShowOverlay });
  }

  impersonatorEntry = () => (
    <span>
      <ImageHOC src="images/ion-alert-circle.png" alt="Impersonation" />
      Impersonation in effect
    </span>
  );

  ViewDetailsClick = (e) => {
    this.props.onViewDetailsClick(e);
    this.setState({ isShowOverlay: false });
  }


  /** @description Function to render the carticon
   * @param {number} cartCount - No of cart items
   * @param {func} cartHandler - callback function for cart icon
   */

  renderCartIcon = (cartCount, cartHandler) => (
    <a
      className="cart-icon"
      href="/"
      onClick={(event) => cartHandler(event, true)}
    >
      <span>
        <i className="icon-cart" />
        {cartCount
          && cartCount > 0
          ? <Badge className="badge-size">{cartCount}</Badge>
          : null}
      </span>
    </a>
  );

  /** @description Function to render the user icon
   * @param {func} profileHandler - callback function for user icon
   */
  renderUserIcon = () => (
    <a
      href="/"
      onClick={(event) => this.profileHandler(event)}
    >
      <span>
        <i className="icon-user-avtar" />
      </span>
    </a>
  );

  /** @description Function to render the cart and user icon */
  renderIcons = () => {
    const {
      cartCount, onCartClick, profileInfo, onSignOutClick, onMyAccountClick, isAdmin,
    } = this.props;
    const { isShowOverlay } = this.state;
    const overlayClass = classNames({ open: isShowOverlay });
    return (
      <ul className="header-top-nav pull-right">
        {getImpersonatedBy() && <li className="impersonate-link">{this.impersonatorEntry()} </li>}
        {!isAdmin && <li>{this.renderCartIcon(cartCount, onCartClick)}</li>}
        <li ref={(node) => { this.node = node; }} className={overlayClass}>
          {this.renderUserIcon()}
          {isShowOverlay &&
            <ProfileOverlay
              info={profileInfo}
              onSignOutClick={onSignOutClick}
              onMyAccountClick={onMyAccountClick}
              onViewDetailsClick={this.ViewDetailsClick}
              isRewardPointAccess={authorization.userRolesList.length && (authorization.userRolesList.includes('FULL_ACCESS') || authorization.userRolesList.includes('VIEW_POINTS'))}
              isAdmin={isAdmin}
            />
          }
        </li>
      </ul>
    );
  };

  /** @description To render the TopHeader section */
  render() {
    const { isShowOverlay } = this.state;
    const { isAdmin } = this.props;
    const showover = isShowOverlay ? <div className="profilepopup-overlay"></div> : '';
    const logoUrl = isAdmin ? RouteConstants.ADMIN : '/';
    return (
      <section className="header-top-logo-nav">
        <Grid>
          <Row className="show-grid">
            <Col xs={7} md={6}>
              <Logo linkHref={logoUrl} />
            </Col>
            <Col xs={5} md={6} className="top-header-icons pull-right">
              {showover}
              {this.renderIcons()}
            </Col>
          </Row>
        </Grid>
      </section>
    );
  }
}


/** PropTypes:
 * cartCount - number - no of cart items
 * profileInfo - object - contains the informations about user and rewards
 * onSignOutClick - func - function to trigger the logout api
 * onViewDetailsClick - func - callback function for view more button
 */
TopHeader.propTypes = {
  cartCount: PropTypes.number,
  onCartClick: PropTypes.func.isRequired,
  onSignOutClick: PropTypes.func.isRequired,
  profileInfo: PropTypes.object.isRequired,
  onViewDetailsClick: PropTypes.func.isRequired,
  onMyAccountClick: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool,
};

export default TopHeader;
