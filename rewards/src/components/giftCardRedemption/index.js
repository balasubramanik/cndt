import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col } from 'react-bootstrap';
import { Field, change } from 'redux-form';
import Dropdown from '../dropdown';
import Radio from '../radio';
import TextInput from '../textInput';
import { constants } from './constants';
import { validate } from './validate';
import { roundToTwoDecimal, removeSpecialChar, convertStringToNumber, allowOnlyNumber, formatUSCurrency, calculateFee } from '../../utils/utils';
import { updateCardInformation } from '../../actionCreators/RedeemPoints';
import { loadDynamicReduxForm } from '../../components/loadDynamicReduxForm';
import { getGiftCardInformation, getRedeemPointsFormState } from './selectors';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import ToolTip from '../toolTip';

const initialFee = roundToTwoDecimal();

export class GiftCardRedemption extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      fee: initialFee,
    };
    this.amount = {};
    this.feeDetails = {};
    this.variantId = {};
  }

  /** @description React Life cycle method
   * it will invoke, when component is mounted.
   */
  componentDidMount() {
    const { amount } = this.props;
    if (amount && Object.keys(amount).length > 0) {
      this.updateCardType();
    } else {
      this.groupCardsByType();
    }
  }

  /** @description React Life cycle method
   * it will invoke, when component is updated.
   */
  componentDidUpdate(prevProps) {
    const {
      isShowPopup, reset, category, gtmActions, cardValue, quantity, cardType, productName,
    } = this.props;
    if (prevProps.isShowPopup !== isShowPopup
      && isShowPopup
      && category === constants.TYPE) {
      gtmActions.gtmGiftCardType(productName, cardType);
      gtmActions.gtmGiftCardAmount(productName, cardValue);
      gtmActions.gtmGiftCardQuantity(productName, quantity);
      reset();
      this.updateCardType();
      this.updateState('fee', initialFee);
    }
  }

  /** @description Function to return the fee details from object
   * @param {object} feeDetails - contains the information about fee.
   */
  setFeeDetails = (type, variant) => {
    const {
      feeType, max, min, minmaxtype, feeValue, feeDescription, typeDescription,
    } = variant;
    if (this.feeDetails && !this.feeDetails[type]) {
      this.feeDetails = {
        ...this.feeDetails,
        [type]: {
          feeValue,
          feeType,
          feeTypeDesc: feeDescription,
          min,
          max,
          minMaxType: minmaxtype,
          cardTypeDesc: typeDescription,
        },
      };
    }
  }

  /** @description Function to update card information based on product
  * @param {string} type
  * @param {array} arr
  * @param {string} cardValue
  */
  setCardInfo = (type, cardValue, variantId) => {
    if (this.amount) {
      if (this.amount[type]) {
        const index = this.amount[type].findIndex((amt) => amt.value === cardValue);
        if (index === -1) {
          this.amount[type].push(cardValue);
        }
      } else {
        this.amount[type] = [cardValue];
      }
      this.variantId[type] = {
        ...this.variantId[type],
        [`${cardValue}`]: variantId,
      };
    }
  }

  /** @description Function to sort the amount value
   * @param {object} amt - amount
   */
  sortByAmount = (amt) => {
    let amount = {};
    Object.keys(amt).forEach((type) => {
      if (amt[type]) {
        amount = {
          ...amount,
          [type]: amt[type].sort((a, b) => a - b),
        };
      }
    });
    return amount;
  }

  /** @description Function to group the variants based on card type.
   */
  groupCardsByType = () => {
    const { variants: variantInfo, formName, setCardInformation } = this.props;
    let imageUrl = null;
    variantInfo.forEach((variant) => {
      const { variants, variantId } = variant;
      const { cardType, giftCardValue } = variants;
      if (variants) {
        imageUrl = variants.variant_Images;
        const isPhysicalCard = removeSpecialChar(cardType).toLowerCase() === constants.PHYSICAL_CARD.toLowerCase();
        const type = isPhysicalCard ? constants.PHYSICAL_CARD : constants.E_CARD;
        this.setCardInfo(type, giftCardValue, variantId);
        this.setFeeDetails(type, variants);
      }
    });
    this.updateCardType();
    this.amount = this.sortByAmount(this.amount);
    // action for update card informations
    setCardInformation(formName, this.amount, this.feeDetails, this.variantId, imageUrl);
  }


  /** @description function to update card information to store */
  updateCardType = () => {
    const { changeFormState, formName, cardTypes } = this.props;
    // condition for update cardType, fee details to store
    if (cardTypes && cardTypes[0]) {
      changeFormState(formName, 'cardType', cardTypes[0]);
    }
  }


  /** @description function to update state value
   * @param {string} key - state key
   * @param {string} value - state value
   */
  updateState = (key, value) => this.setState({ [key]: value });

  /** @description Function to calculate additional fee based on feeType
   * @param {string} type - card type
   * @param {string} quantity - quantity of product
   */
  calculateAdditionalFee = (type, quantity) => {
    const { feeInfo } = this.props;
    const value = quantity || 0;
    const { feeType, feeValue } = feeInfo[type] || {};
    const fee = calculateFee(value, feeValue, feeType);
    this.setState({ fee });
  }

  /** @description Callback function for change the card type
   * @param {object} e - triggered event(change)
   */
  cardTypeChangeHandler = (e) => {
    const {
      quantity, formName, amount, changeFormState,
    } = this.props;
    const { value } = e.target;
    const amt = amount[value];
    this.calculateAdditionalFee(value, quantity);
    if (amt && amt[0]) {
      changeFormState(formName, 'cardValue', amt[0]);
    }
    this.props.onCardTypeChange(value);
  }

  /** @description Callback function for change the amount
   * @param {object} e - triggered event(change)
   */
  amountChangeHandler = (e) => {
    const { cardType } = this.props;
    const { value } = e.target;
    this.calculateAdditionalFee(cardType, value);
  }

  submitHandler = (values) => {
    const { variantId, feeInfo, productImage } = this.props;
    const { cardType, quantity, cardValue } = values;
    const { fee } = this.state;
    const {
      feeType, min, max, feeValue, minMaxType, feeTypeDesc, cardTypeDesc,
    } = feeInfo[cardType] || {};
    const selectedVariantId = variantId[cardType] ? variantId[cardType][`${cardValue}`] : null;
    const val = {
      productType: cardType === constants.PHYSICAL_CARD ? constants.PHYSICALCARD : constants.ECARD,
      cardType,
      fee: convertStringToNumber(fee),
      cardValue,
      feeType,
      feeValue: feeValue || 0,
      quantity: Number(quantity),
      feeTypeDesc,
      cardTypeDesc,
      productImage,
      min,
      max,
      isReloadable: false,
      variantId: selectedVariantId,
      minMaxType,
    };
    this.props.onSubmit(val);
  }

  /** @description Function to format cardtype for dropdown
   */
  formatCardTypes = () => {
    const { cardTypes } = this.props;
    if (cardTypes) {
      return cardTypes.map((type) => {
        const value = type === constants.PHYSICAL_CARD ? constants.CARD_TYPES.PHYSICAL_CARD : constants.CARD_TYPES.E_CARD;
        return value;
      });
    }
    return null;
  }

  /** @description Function to format amount for dropdown
   */
  formatAmountData = () => {
    const { amount, cardType } = this.props;
    if (amount && amount[cardType]) {
      return amount[cardType].map((value) => {
        const label = value ? formatUSCurrency(value.toString()) : '';
        return {
          label,
          value,
        };
      });
    }
    return null;
  }

  /** @description Function to render the Quantity field
   */
  renderQuantity = () => {
    const { formName } = this.props;
    return (
      <Col md={4} sm={5} xs={6}>
        <div className="form-group">
          <Field
            className="gaf-form"
            name="quantity"
            id={`${formName}quantity`}
            type="text"
            component={TextInput}
            label={constants.QUANITY}
            normalize={allowOnlyNumber}
            onChange={this.amountChangeHandler}
          />
        </div>
      </Col>
    );
  }

  /** @description Function to render the Card type
   */
  renderCardType = () => {
    const { cardType, formName } = this.props;
    const items = this.formatCardTypes();
    return (
      <Row>
        <div className="form-group">
          <div className="gaf-radio">
            {items &&
              items.length > 0 &&
              <Field
                name="cardType"
                id={`${formName}-cardType`}
                component={Radio}
                className="col-md-12 col-lg-6 col-sm-6 col-xs-6"
                options={items}
                value={cardType}
                onChange={this.cardTypeChangeHandler}
              />}
          </div>
        </div>
      </Row>
    );
  }

  /** @description Function to render the fee information
   */
  renderFee = () => {
    const { cardType, feeInfo } = this.props;
    const { fee } = this.state;
    const { feeTypeDesc, feeValue } = feeInfo[cardType] || {};
    return feeValue ? (
      <small>
        {constants.FEE} {fee}
        {feeTypeDesc &&
          <ToolTip id="fee-description" text={feeTypeDesc}>
            <span className="icon-question" role="button" />
          </ToolTip>}
      </small>) : null;
  }

  /** @description Function to render the amount field
   */
  renderAmount = () => {
    const { cardValue, formName } = this.props;
    const items = this.formatAmountData();
    return (
      <Col md={5} sm={5} xs={6}>
        <div className="form-group">
          <label>{constants.AMOUNT}</label>
          {items &&
            items.length > 0 &&
            <Field
              id={`${formName}cardValue`}
              name="cardValue"
              menuItems={items}
              component={Dropdown}
              value={cardValue}
            />}
          {this.renderFee()}
        </div>
      </Col>
    );
  }

  /** @description Function to render the add to card button
   */
  renderAddToCartButton = () => {
    const { errors, formName } = this.props;
    const btnClass = classNames('btn gaf-btn-primary', { 'btn-disabled': errors });
    return (
      <Row>
        <Col md={5} sm={12} xs={12}>
          <div className="addto-cart-btn">
            <button id={`${formName}-add-to-cart`} type="submit" className={btnClass}>{constants.ADD_TO_CART}</button>
          </div>
        </Col>
      </Row>
    );
  }

  render() {
    const { handleSubmit, formName, errors } = this.props;
    return (
      <div className="gift-grid-innerbox">
        <form name={formName} onSubmit={handleSubmit(this.submitHandler)}>
          <Row>
            {this.renderAmount()}
            {this.renderQuantity()}
          </Row>
          <Row>
            <Col md={12}>
              <label>{constants.CARD_TYPE}</label>
            </Col>
          </Row>
          {this.renderCardType()}
          {errors && errors.fieldError && <p className="text-danger">{errors.fieldError}</p>}
          {this.renderAddToCartButton()}
        </form>
      </div>
    );
  }
}

/** PropTypes:
 * onSubmit - func - callback function for submit form
 * handleSubmit - func - property comes from  redux-form
 * reset - func - function to reset the form
 * changeFormState - func - function to update the form state in store
 * setCardInformation - func - function to update the card details in store
 * formName - string - unique key for form
 * onCardTypeChange - func - callback function for change the card type
 * gtmActions - object - actions for google tag manager
 * variants - array - contains the variants of selected product
 * errors - object - contains the error message for redemption form
 * cardValue - number - redeemable amount of selected product
 * cardTypes - array - contains the array of card types
 * cardType - string - selected card type
 * quantity - string - quantity of selected product
 * feeInfo - object - contains the details of fee
 * variantId - object - contains the variant id
 * amount - object - contains the variant amount for gift card
 * isShowPopup - boolean -show/hide the success popup
 * category - string - selected category
 * productImage - string - product image url
 * productName - string - name of the product
*/
GiftCardRedemption.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  changeFormState: PropTypes.func.isRequired,
  setCardInformation: PropTypes.func.isRequired,
  formName: PropTypes.string.isRequired,
  onCardTypeChange: PropTypes.func.isRequired,
  gtmActions: PropTypes.object.isRequired,
  variants: PropTypes.array,
  errors: PropTypes.object,
  cardValue: PropTypes.number,
  cardTypes: PropTypes.array,
  cardType: PropTypes.string,
  quantity: PropTypes.string,
  feeInfo: PropTypes.object,
  variantId: PropTypes.object,
  amount: PropTypes.object,
  isShowPopup: PropTypes.bool,
  category: PropTypes.string,
  productImage: PropTypes.string,
  productName: PropTypes.string,
};

GiftCardRedemption.defaultProps = {
  variants: [],
  feeInfo: {},
  amount: {},
};

function mapStateToProps(state, ownParams) {
  const { redeemPointsState } = state;
  const { isShowPopup, category } = redeemPointsState;
  const {
    amount, feeInfo, variantId, productImage,
  } = getGiftCardInformation(state, ownParams);
  const { values, syncErrors } = getRedeemPointsFormState(state, ownParams);
  const { cardType, cardValue, quantity } = values || {};
  return {
    cardType,
    cardValue,
    quantity,
    feeInfo,
    amount,
    variantId,
    errors: syncErrors,
    isShowPopup,
    category,
    productImage,
  };
}


function mapDispatchToProps(dispatch) {
  return {
    changeFormState: bindActionCreators(change, dispatch),
    setCardInformation: bindActionCreators(updateCardInformation, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

const Component = loadDynamicReduxForm(GiftCardRedemption, validate);

export default connect(mapStateToProps, mapDispatchToProps)(Component);
