import { createSelector } from 'reselect';

export const getRedeemPointsState = (state) => state.redeemPointsState;
export const getFormState = (state) => state.form;
export const getFormId = (state, ownParams) => ownParams.formName;

export const getGiftCardDetails = createSelector(
  [getRedeemPointsState],
  (redeemPointsState) => redeemPointsState.giftCardDetails,
);

export const getCategories = createSelector(
  [getRedeemPointsState],
  (redeemPointsState) => redeemPointsState.giftCards,
);

export const getGiftCardInformation = createSelector(
  [getRedeemPointsState, getFormId],
  (redeemPointsState, id) => redeemPointsState.cardInformation[id] || {},
);

export const getRedeemPointsFormState = createSelector(
  [getFormState, getFormId],
  (formState, id) => formState[id] || {},
);
