const constants = {
  MIN_QUANTITY_ERROR: 'You have the min limit of {{quantity}} quantity',
  MAX_QUANTITY_ERROR: 'Sorry, you can only redeem {{quantity}} gift cards at a time.',
  MIN_AMOUNT_ERROR: 'You have the min limit of {{amount}} amount.',
  MAX_AMOUNT_ERROR: 'Sorry, you can only redeem {{amount}} at a time.',
  ADD_TO_CART: 'Add to cart',
  VIEW_MORE: 'View More',
  VIEW_LESS: 'View Less',
  AMOUNT: 'Amount',
  FEE: 'Fee:',
  QUANITY: 'Quantity',
  PHYSICAL_CARD: 'physicalCard',
  E_CARD: 'eCard',
  PHYSICALCARD: 'Physical Card',
  ECARD: 'E-card',
  CARD_TYPE: 'Card Type',
  CARD_TYPES: {
    E_CARD: {
      title: 'e-Card',
      value: 'eCard',
    },
    PHYSICAL_CARD: {
      title: 'Physical Card',
      value: 'physicalCard',
    },
  },
  TYPE: 'giftcards',
};
export { constants };
