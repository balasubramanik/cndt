import test from 'tape';
import { getRedeemPointsState, getCategories, getGiftCardDetails, getFormState, getFormId, getGiftCardInformation, getRedeemPointsFormState } from './selectors';

const t = test('test utils', (b) => b);

describe('Redemption form Selectors', () => {
  it('should return RedeemPointsState', () => {
    const mock = {
      redeemPointsState: {
        debitCardInfo: {},
        giftCards: {},
        achInfo: {},
      },
    };
    t.deepEqual(mock.redeemPointsState, getRedeemPointsState(mock));
  });

  it('should return form state', () => {
    const mock = {
      form: {
        redemptionForm: {},
      },
    };
    t.deepEqual(mock.form, getFormState(mock));
  });

  it('should return form id', () => {
    const mock = {
      form: {
        redemptionForm: {},
      },
    };
    const ownParams = {
      formName: 'MA123',
    };
    t.deepEqual(ownParams.formName, getFormId(mock, ownParams));
  });

  it('should return getGiftCardDetails ', () => {
    const mock = {
      redeemPointsState: {
        giftCardDetails: [{
          products: [{
            listprice: '0.000',
            cardType: 'Physical Card',
            feeType: '%',
            feeValue: '2.5',
            giftCardValue: null,
            imageicon: null,
            max: '99,999.00',
            min: '25.00',
            minmaxtype: '$',
            Variant_Images: null,
            DisplayName: 'VISA Reloadble Debit Card',
          }],
        }],
      },
    };
    t.deepEqual(mock.redeemPointsState.giftCardDetails, getGiftCardDetails(mock));
  });

  it('should return getCategories  ', () => {
    const mock = {
      redeemPointsState: {
        categories: [{
          displayName: 'dinning',
        },
        {
          displayName: 'flowers',
        }],
      },
    };
    t.deepEqual(mock.redeemPointsState.categories, getCategories(mock));
  });

  it('should return GiftCard Information', () => {
    const ownParams = {
      formName: 'MA123',
    };
    const mock = {
      redeemPointsState: {
        cardInformation: {
          MA123: {
            products: [{
              listprice: '0.000',
              cardType: 'Physical Card',
              feeType: '%',
              feeValue: '2.5',
              giftCardValue: null,
              imageicon: null,
              max: '99,999.00',
              min: '25.00',
              minmaxtype: '$',
              Variant_Images: null,
              DisplayName: 'VISA Reloadble Debit Card',
            }],
          },
        },
      },
    };
    const formData = mock.redeemPointsState.cardInformation.MA123;
    t.deepEqual(formData, getGiftCardInformation(mock, ownParams));
  });

  it('should return RedeemPoints Form State', () => {
    const ownParams = {
      formName: 'MA123',
    };
    const mock = {
      form: {
        MA123: {
          products: [{
            listprice: '0.000',
            cardType: 'Physical Card',
            feeType: '%',
            feeValue: '2.5',
            giftCardValue: null,
            imageicon: null,
            max: '99,999.00',
            min: '25.00',
            minmaxtype: '$',
            Variant_Images: null,
            DisplayName: 'VISA Reloadble Debit Card',
          }],
        },
      },
    };
    const formData = mock.form.MA123;
    t.deepEqual(formData, getRedeemPointsFormState(mock, ownParams));
  });

  it('should return RedeemPoints Form State without formName', () => {
    const ownParams = {
      formName: 'MA123',
    };
    const mock = {
      form: {},
    };
    t.deepEqual({}, getRedeemPointsFormState(mock, ownParams));
  });
});
