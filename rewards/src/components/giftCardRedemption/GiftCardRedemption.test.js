import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import GiftCardRedemptionComponent, { GiftCardRedemption } from '../../components/giftCardRedemption';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();

const variants = [
  {
    variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
    productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
    variantID: 'MER1001513-1',
    variants: {
      listprice: '0.0000',
      cardType: 'E card',
      feeType: '$',
      feeValue: '1.0',
      giftCardValue: '25.0000',
      feeDescription: 'Aéropostale - $25',
      imageicon: null,
      max: '100.0',
      min: '1.0',
      minmaxtype: 'QTY',
      Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
      DisplayName: 'Aéropostale - $25',
    },
  },
  {
    variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
    productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
    variantID: 'MER1001513-2',
    variants: {
      listprice: '0.0000',
      cardType: 'E card',
      feeType: '$',
      feeValue: '1.0',
      giftCardValue: '25.0000',
      feeDescription: 'Aéropostale - $26',
      imageicon: null,
      max: '100.0',
      min: '1.0',
      minmaxtype: 'QTY',
      Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
      DisplayName: 'Aéropostale - $26',
    },
  },
  {
    variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
    productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
    variantID: 'MER1001513-1',
    variants: {
      listprice: '0.0000',
      cardType: 'Physical Card',
      feeType: '$',
      feeValue: '1.0',
      giftCardValue: '25.0000',
      imageicon: null,
      max: '100.0',
      min: '1.0',
      minmaxtype: 'QTY',
      Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
      DisplayName: 'Aéropostale - $25',
    },
  }];
const gtmActions = {
  gtmGiftCardSuccess: jest.fn(),
  gtmGiftCardType: jest.fn(),
  gtmGiftCardAmount: jest.fn(),
  gtmGiftCardQuantity: jest.fn(),
};
describe('GiftCardRedemptionForm', () => {
  window.open = jest.fn();
  const setCardInformation = jest.fn();
  const handleSubmit = jest.fn();
  const changeFormState = jest.fn();
  const reset = jest.fn();
  const cardTypeChangeHandler = jest.fn();
  const syncErrors = {
    fieldError: 'You have the min limit of 1 quantity',
  };
  const tree = shallow(<GiftCardRedemption
    cardType="eCard"
    cardValue="1000"
    quantity="2"
    feeInfo={{ eCard: { feeType: '%', min: 1, max: 100, feeTypeDesc: 'Aéropostale - $25', description: 'sample' }, physicalCard: { feeType: '%', min: 1, max: 100, description: 'sample' } }}
    amount={{ eCard: [{ title: '10000', value: 10000 }] }}
    cardTypes={['eCard', 'physicalCard']}
    errors={syncErrors}
    changeFormState={changeFormState}
    handleSubmit={handleSubmit}
    setCardInformation={setCardInformation}
    formName="redemptionForm"
    isShowPopup={true}
    reset={reset}
    category="giftcards"
    variants={variants}
    onCardTypeChange={cardTypeChangeHandler}
    variantId={{}}
    gtmActions={gtmActions}
  />);
  it('should be defined', () => {
    expect(GiftCardRedemption).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Redemption Form', () => {
    expect(tree.find('form[name="redemptionForm"]').length).toBe(1);
  });

  it('should render Amount', () => {
    expect(tree.find('Field[name="cardValue"]').length).toBe(1);
  });

  it('should render Add to cart button', () => {
    expect(tree.find('.addto-cart-btn').length).toBe(1);
  });

  it('should render cardType', () => {
    expect(tree.find('Field[name="cardType"]').length).toBe(1);
  });

  it('should call cardTypeChangeHandler', () => {
    const simulateChange = tree.find('Field[name="cardType"]').prop('onChange');
    simulateChange({ target: { value: 'eCard' } });
    expect(changeFormState).toBeCalled();
  });

  it('should call amountChangeHandler', () => {
    const simulateChange = tree.find('Field[name="quantity"]').prop('onChange');
    simulateChange({ target: { value: '1' } });
    expect(changeFormState).toBeCalled();
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isShowPopup: false,
    });
    expect(reset).toBeCalled();
  });
});

describe('GiftCardRedemptionForm without amount, feeinfo', () => {
  const setCardInformation = jest.fn();
  const handleSubmit = jest.fn();
  const changeFormState = jest.fn();
  const reset = jest.fn();
  const cardTypeChangeHandler = jest.fn();
  const syncErrors = {
    firstName: 'Please enter valid firstname',
  };
  const tree = shallow(<GiftCardRedemption
    cardType="eCard"
    cardValue="1000"
    quantity="2"
    cardTypes={['eCard', 'Physical Card']}
    errors={syncErrors}
    changeFormState={changeFormState}
    handleSubmit={handleSubmit}
    setCardInformation={setCardInformation}
    formName="redemptionForm"
    reset={reset}
    category="giftcards"
    variants={variants}
    onCardTypeChange={cardTypeChangeHandler}
    gtmActions={gtmActions}
  />);
  it('should be defined', () => {
    expect(GiftCardRedemption).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Redemption Form', () => {
    expect(tree.find('form[name="redemptionForm"]').length).toBe(1);
  });

  it('should render Add to cart button', () => {
    expect(tree.find('.addto-cart-btn').length).toBe(1);
  });
});

describe('GiftCardRedemption with connected component', () => {
  const state = {
    form: {
      MA123: {
        values: {
          cardType: 'physicalCard',
          cardValue: 1000,
          quantity: 2,
        },
        syncErrors: {
          firstName: 'Please enter valid Quantity',
        },
      },
    },
    redeemPointsState: {
      cardInformation: {
        MA123: {
          feeInfo: { eCard: { feeType: '$', min: 1, max: 100, description: 'sample' }, physicalCard: { feeType: '$', min: 1, max: 100, description: 'sample' } },
          amount: { eCard: [{ title: '10000', value: 10000 }] },
          variantId: {},
        },
      },
    },
  };
  const store = fakeStore(state);
  const submitHandler = jest.fn();
  const setCardInformation = jest.fn();
  const changeFormState = jest.fn();
  const cardTypeChangeHandler = jest.fn();
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <GiftCardRedemptionComponent
          formName="MA123"
          onSubmit={submitHandler}
          setCardInformation={setCardInformation}
          changeFormState={changeFormState}
          onCardTypeChange={cardTypeChangeHandler}
          cardTypes={['eCard', 'Physical Card']}
          gtmActions={gtmActions}
        />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(GiftCardRedemptionComponent).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call cardTypeChangeHandler ', () => {
    const simulateChange = tree.find('Field[name="cardType"]').prop('onChange');
    simulateChange({ target: { value: 'eCard' } });
    expect(changeFormState).toBeDefined();
  });

  it('should call submitHandler', () => {
    const simulateSubmit = tree.find('form[name="MA123"]').prop('onSubmit');
    simulateSubmit();
    expect(submitHandler).toBeCalled();
  });
});
