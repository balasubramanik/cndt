import { validate } from './validate';
import { constants } from './constants';

describe('validate', () => {
  it('should be equal to invalid quantity', () => {
    const errors = validate({}, { availableBalance: 1000 });
    expect(errors.quantity).toBe(true);
  });

  it('should be equal to invalid min quantity', () => {
    const errors = validate({ cardType: 'eCard', quantity: 1 }, { feeInfo: { eCard: { minMaxType: 'qty', min: 2, max: 100 } } });
    expect(errors.fieldError).toBe(constants.MIN_QUANTITY_ERROR.replace('{{quantity}}', 2));
  });

  it('should be equal to invalid max quantity', () => {
    const errors = validate({ cardType: 'eCard', quantity: 101 }, { feeInfo: { eCard: { minMaxType: 'qty', min: 1, max: 100 } } });
    expect(errors.fieldError).toBe(constants.MAX_QUANTITY_ERROR.replace('{{quantity}}', 100));
  });

  it('should be equal to invalid min amount', () => {
    const errors = validate({ cardType: 'eCard', cardValue: 1, quantity: 1 }, { feeInfo: { eCard: { minMaxType: '$', min: 2, max: 100 } } });
    expect(errors.fieldError).toBe(constants.MIN_AMOUNT_ERROR.replace('{{amount}}', '$2'));
  });

  it('should be equal to invalid max amount', () => {
    const errors = validate({ cardType: 'eCard', cardValue: 101, quantity: 1 }, { feeInfo: { eCard: { feeValue: 2, minMaxType: '$', min: 1, max: 100 } } });
    expect(errors.fieldError).toBe(constants.MAX_AMOUNT_ERROR.replace('{{amount}}', '$100'));
  });
});
