import { constants } from './constants';
import { formatUSCurrency } from '../../utils/utils';

const validate = (fields, props) => {
  const errors = {};
  const { quantity, cardValue, cardType } = fields;
  const { feeInfo = {} } = props;
  const {
    max, min, minMaxType,
  } = feeInfo[cardType] || {};
  const type = minMaxType ? minMaxType.toUpperCase() : '';

  if (!quantity) {
    errors.quantity = true;
  }

  if (quantity && type === 'QTY') {
    const count = Number(quantity);
    if (min > count) {
      errors.quantity = true;
      errors.fieldError = constants.MIN_QUANTITY_ERROR.replace('{{quantity}}', min);
    }
    if (max < count) {
      errors.quantity = true;
      errors.fieldError = constants.MAX_QUANTITY_ERROR.replace('{{quantity}}', max);
    }
  }

  if (quantity && minMaxType === '$') {
    const value = Number(cardValue);
    if (min > value) {
      errors.cardValue = true;
      errors.fieldError = constants.MIN_AMOUNT_ERROR.replace('{{amount}}', formatUSCurrency(min));
    }
    if (max < value) {
      errors.cardValue = true;
      errors.fieldError = constants.MAX_AMOUNT_ERROR.replace('{{amount}}', formatUSCurrency(max));
    }
  }

  return errors;
};

export { validate };
