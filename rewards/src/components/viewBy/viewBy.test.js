import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ViewBy from '../../components/viewBy';
configure({ adapter: new Adapter() });
describe('ViewBy', () => {
  const tree = shallow(<ViewBy />);
  it('should be defined', () => {
    expect(ViewBy).toBeDefined();
  });
  it('should render correctly', () => {
    expect(ViewBy).toMatchSnapshot();
  });
  it('should render Dropdown component', () => {
    expect(tree.find('Dropdown').length).toBe(1);
  });
});
