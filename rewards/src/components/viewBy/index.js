import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel } from 'react-bootstrap';
import { labelName } from './constants';
import Dropdown from '../dropdown';

const ViewBy = (props) => (
  <div className="filterby">
    <div className="form-inline">
      <FormGroup className="gaf-form gaf-select-box">
        <ControlLabel>{labelName.viewBy}</ControlLabel>
        <Dropdown id={props.id} menuItems={props.options} onChangeHandler={props.onChange} />
      </FormGroup>
    </div>
  </div>
);

ViewBy.propTypes = {
  onChange: PropTypes.func,
  options: PropTypes.array.isRequired,
  id: PropTypes.string,
};
export default ViewBy;
