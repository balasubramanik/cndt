const rewardsInfo = {
  REWARDSPOINTS: 'Reward Points',
  EXPIRYON: 'Expires on',
  VIEWDETAILS: 'view details',
  TOOLTIP: 'Submit or Redeem before {MM/DD/YYYY}, or your points will expire!',
  ACCESS_DENIED: 'Access Denied',
};

export { rewardsInfo };
