import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import { rewardsInfo } from './constants';
import ToolTip from '../toolTip';
import { roundToTwoDecimal, formatDateUTC, formatCurrency } from '../../utils/utils';

/** @description Function to render the rewards Logo */
const rewardsLogo = () => (
  <Col xs={4}>
    <i className="icon-rewards-ribbon ribbon-size" />
  </Col>
);

/** @description Function to render the tooltip
 * @param {string} date - contains the expiry date
 */
const renderTooltip = (date) => (
  <ToolTip
    id="rewards-info"
    text={rewardsInfo.TOOLTIP.replace('{MM/DD/YYYY}', date)}
  >
    <i role="button" className="icon-question" />
  </ToolTip>
);

/** @description Function to render the rewards information
 * @param {object} details - contains the details of reward points
 * @param {boolean} isShowTooltip - flag to show/hide the tooltip
 */
const renderRewardsInfo = (details, isShowTooltip) => {
  const { rewardPoints, expiryDate } = details;
  const balance = formatCurrency(roundToTwoDecimal(rewardPoints));
  const availbleBalance = balance || roundToTwoDecimal(0);
  const formattedExpiryDate = formatDateUTC(expiryDate);
  return (
    <div>
      <p className="gaf-user-rewardpoint">{availbleBalance}</p>
      <p className="gaf-user-rewardpoint-text">{rewardsInfo.REWARDSPOINTS}</p>
      {formattedExpiryDate &&
        <p className="gaf-user-expiry">
          {`${rewardsInfo.EXPIRYON}: ${formattedExpiryDate}`}
          {isShowTooltip && renderTooltip(formattedExpiryDate)}
        </p>}
    </div>
  );
};

/** @description Functional component to render the rewards information for contractor */
const RewardsInfo = ({
  isShowLogo,
  details,
  onViewDetailsClick,
  isRewardPointAccess,
}) => {
  const rewardsClass = classNames('user-rewardpoints', { 'whiten-area': !isRewardPointAccess || (!details || Object.keys(details).length === 0) });
  return (
    <div className={rewardsClass}>
      {isShowLogo ?
        <Row className="show-grid">
          {rewardsLogo()}
          <Col xs={8}>
            {renderRewardsInfo(details, isShowLogo)}
          </Col>
        </Row> :
        renderRewardsInfo(details)}
      <div className="viewdetail-link">
        <a href="/" onClick={(event) => onViewDetailsClick(event)}>{rewardsInfo.VIEWDETAILS}<i className="icon-arrow" /></a>
      </div>
    </div>
  );
};

/** PropTypes:
 * details - object - contains the details of reward points
 * onViewDetailsClick - func - callback function for viewmore button
 * isShowLogo - boolean - Flag for show/hide the rewards icon
 */
RewardsInfo.propTypes = {
  details: PropTypes.object.isRequired,
  onViewDetailsClick: PropTypes.func.isRequired,
  isShowLogo: PropTypes.bool,
  isRewardPointAccess: PropTypes.bool,
};

export default RewardsInfo;
