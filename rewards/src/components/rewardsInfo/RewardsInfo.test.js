import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RewardsInfo from '../../components/rewardsInfo';

configure({ adapter: new Adapter() });

describe('RewardsInfo with isShowLogo false', () => {
  const viewHandler = jest.fn();
  const tree = shallow(<RewardsInfo
    details={{
      rewardPoints: '2500',
      expiryDate: '2018-08-09T16:57:19.0487212+05:30',
    }}
    onViewDetailsClick={viewHandler}
  />);
  it('should be defined', () => {
    expect(RewardsInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Reward points container', () => {
    expect(tree.find('.user-rewardpoints').length).toBe(1);
  });

  it('should render Reward points', () => {
    expect(tree.find('.gaf-user-rewardpoint').length).toBe(1);
  });

  it('should render Reward points Text', () => {
    expect(tree.find('.gaf-user-rewardpoint-text').length).toBe(1);
  });

  it('should render Reward points Expiry', () => {
    expect(tree.find('.gaf-user-expiry').length).toBe(1);
  });

  it('should render Reward points Expiry', () => {
    expect(tree.find('.gaf-user-expiry').length).toBe(1);
  });

  it('should not render Tooltip', () => {
    expect(tree.find('.icon-question').length).toBe(0);
  });

  it('should call viewHandler', () => {
    const simulateClick = tree.find('.viewdetail-link').find('a').prop('onClick');
    simulateClick();
    expect(viewHandler).toBeCalled();
  });
});

describe('RewardsInfo with isShowLogo true', () => {
  const viewHandler = jest.fn();
  const tree = shallow(<RewardsInfo
    isShowLogo
    details={{
      rewardPoints: '2500',
      expiryDate: '2018-08-09T16:57:19.0487212+05:30',
    }}
    onViewDetailsClick={viewHandler}
    isRewardPointAccess
  />);
  it('should be defined', () => {
    expect(RewardsInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Reward points container', () => {
    expect(tree.find('.user-rewardpoints').length).toBe(1);
  });

  it('should render Rewards Logo', () => {
    expect(tree.find('.icon-rewards-ribbon').length).toBe(1);
  });

  it('should render Reward points', () => {
    expect(tree.find('.gaf-user-rewardpoint').length).toBe(1);
  });

  it('should render Reward points Text', () => {
    expect(tree.find('.gaf-user-rewardpoint-text').length).toBe(1);
  });

  it('should render Reward points Expiry', () => {
    expect(tree.find('.gaf-user-expiry').length).toBe(1);
  });

  it('should render Reward points Expiry', () => {
    expect(tree.find('.gaf-user-expiry').length).toBe(1);
  });

  it('should render Tooltip', () => {
    expect(tree.find('.icon-question').length).toBe(1);
  });
});

describe('RewardsInfo without details and isRewardPointAccess false', () => {
  const viewHandler = jest.fn();
  const tree = shallow(<RewardsInfo details={{}} onViewDetailsClick={viewHandler} isRewardPointAccess={false} />);
  it('should be defined', () => {
    expect(RewardsInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render Reward points container', () => {
    expect(tree.find('.user-rewardpoints').length).toBe(1);
  });

  it('should not render Rewards Logo', () => {
    expect(tree.find('.icon-rewards-ribbon').length).toBe(0);
  });

  it('should render Reward points', () => {
    expect(tree.find('.gaf-user-rewardpoint').length).toBe(1);
  });

  it('should not render expiry date', () => {
    expect(tree.find('.gaf-user-expiry').length).toBe(0);
  });
});

describe('RewardsInfo with zero reward points and isRewardPointAccess false', () => {
  const viewHandler = jest.fn();
  const tree = shallow(<RewardsInfo
    details={{
      rewardPoints: '-',
      expiryDate: '2018-08-09T16:57:19.0487212+05:30',
    }}
    isShowLogo
    onViewDetailsClick={viewHandler}
    isRewardPointAccess
  />);
  it('should be defined', () => {
    expect(RewardsInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render zero balance', () => {
    expect((tree).find('.gaf-user-rewardpoint').text()).toBe('0.00');
  });
});
