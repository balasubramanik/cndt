import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryDebit from '../../components/cartSummaryDebit';

configure({ adapter: new Adapter() });

describe('CartSummaryDebit List', () => {
  const onUpdate = jest.fn();
  const onDelete = jest.fn();
  const updateDisabledHandler = jest.fn();

  const tree = shallow(<CartSummaryDebit
    index={1}
    item={
      {
        isAvailable: false,
        feeType: '%',
        productName: 'VISA Reloadble Debit Card',
        cardType: 'Physical Card',
        img: 'https://d1c4vlisoi682j.cloudfront.net/workstride-assets/images/vendors/vendorimages_gafvisa.png?1537394534897',
        cardTypeDesc: 'A Physical card will be sent your address after checkout.\n\n*This item is non-refundable.',
        cardValue: 25.0,
        feeRate: 2.5,
        feeTypeDesc: 'Service fee of 2.5% of your transfer amount, and one time fee of 3 points for a plastic card is applicable.',
        category: 'debit',
        debitCardIssuedTo: 'prabu.swaminathan@photoninfotech.net',
        min: 25.0,
        max: 99999.0,
      }
    }
    onUpdate={onUpdate}
    onUpdateDisableSubmit={updateDisabledHandler}
    onDelete={onDelete}
  />);

  const event = {
    preventDefault: jest.fn(),
    target: {
      value: '12',
      keyCode: 'abc-efg',
    },
  };
  it('should be defined', () => {
    expect(CartSummaryDebit).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onKeyPress', () => {
    const simulateChange = tree.find('input').at(0).prop('onKeyPress');
    simulateChange(event);
  });

  it('should call Delete handler', () => {
    const simulateChange = tree.find('a').prop('onClick');
    simulateChange(event);
    expect(onDelete).toBeCalled();
  });

  it('should call onChange for input', () => {
    const simulateChange = tree.find('input').at(0).prop('onChange');
    simulateChange(event);
    expect(onUpdate).toBeCalled();
    expect(updateDisabledHandler).toBeCalled();
  });
});

describe('CartSummaryDebit for empty item key values', () => {
  const onUpdate = jest.fn();
  const onDelete = jest.fn();
  const updateDisabledHandler = jest.fn();

  const tree = shallow(<CartSummaryDebit
    index={1}
    item={
      {
        isAvailable: true,
        productName: '',
        cardType: 'E-card',
        img: '',
        cardTypeDesc: '',
        cardValue: '',
        feeTypeDesc: '',
        category: '',
        debitCardIssuedTo: null,
      }
    }
    onUpdate={onUpdate}
    onUpdateDisableSubmit={updateDisabledHandler}
    onDelete={onDelete}
  />);

  it('should be defined', () => {
    expect(CartSummaryDebit).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('CartSummaryDebit with tooltip and error messages', () => {
  const onUpdate = jest.fn();
  const onDelete = jest.fn();
  const updateDisabledHandler = jest.fn();

  const tree = mount(<CartSummaryDebit
    index={0}
    item={
      {
        isAvailable: false,
        feeType: '%',
        productName: 'VISA Reloadble Debit Card',
        cardType: 'Physical Card',
        img: 'https://d1c4vlisoi682j.cloudfront.net/workstride-assets/images/vendors/vendorimages_gafvisa.png?1537394534897',
        cardTypeDesc: 'A Physical card will be sent your address after checkout.\n\n*This item is non-refundable.',
        cardValue: 25.0,
        feeRate: 2.5,
        feeTypeDesc: 'Service fee of 2.5% of your transfer amount, and one time fee of 3 points for a plastic card is applicable.',
        category: 'debit',
        debitCardIssuedTo: 'prabu.swaminathan@photoninfotech.net',
        min: 25.0,
        max: 99999.0,
      }
    }
    onUpdate={onUpdate}
    onUpdateDisableSubmit={updateDisabledHandler}
    onDelete={onDelete}
  />);
  it('should render error message for debit not available', () => {
    expect(tree.find('.gaf-error-message').text()).toEqual('Sorry, this product is no longer available.');
  });
  it('should render error message for check not available', () => {
    const checktree = mount(<CartSummaryDebit
      index={0}
      item={
        {
          isAvailable: false,
          category: 'check',
        }
      }
    />);
    expect(checktree.find('.gaf-error-message').text()).toEqual('Check is no longer available. Please remove this Check from the cart.');
  });
  it('should render error message for card amount less than minimum', () => {
    const checktree = mount(<CartSummaryDebit
      index={0}
      item={
        {
          cardValue: 10.0,
          min: 25.0,
          max: 100.0,
        }
      }
    />);
    expect(checktree.find('.gaf-error-message').text()).toEqual('You have entered the less than minimum amount $25');
  });
  it('should render error message for card amount more than maximum', () => {
    const checktree = mount(<CartSummaryDebit
      index={0}
      item={
        {
          cardValue: 101.0,
          min: 25.0,
          max: 100.0,
        }
      }
    />);
    expect(checktree.find('.gaf-error-message').text()).toEqual('You have exceeded maximum amount.');
  });
});

