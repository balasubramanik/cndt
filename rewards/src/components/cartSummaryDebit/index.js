import React from 'react';
import PropTypes from 'prop-types';
import { cartSummaryListtext } from './constants';
import ImageHOC from '../imageHOC';
import { roundToTwoDecimal, formatUSCurrency, formatCurrency, removeSpecialChar, calculateFee, convertStringToNumber } from '../../utils/utils';
import ToolTip from '../toolTip';

class CartSummaryDebit extends React.PureComponent {
  onKeyPress(event) {
    const keyCode = event.keyCode || event.which;
    const keyValue = String.fromCharCode(keyCode);
    if (/\+|-/.test(keyValue)) {
      event.preventDefault();
    }
  }
  handleInput = (value) => {
    let typedCardValue = value;
    const cardAmt = removeSpecialChar(value);
    const isDotAvail = cardAmt.indexOf('.') !== -1;
    if (!isDotAvail && cardAmt.length > 5) {
      const maxValue = cardAmt.substr(0, 5);
      const maxValueDecimal = cardAmt.substr(5, 7);
      const cardValue = `${maxValue}.${maxValueDecimal}`;
      typedCardValue = formatUSCurrency(cardValue);
    }
    const {
      category = '',
      min = '',
      max = '',
      feeType = '',
      feeRate = '',
      debitCardIssuedTo,
    } = this.props.item;
    const updatedValue = formatUSCurrency(removeSpecialChar(typedCardValue));
    const appliedCardValue = convertStringToNumber(updatedValue);
    const isItemOrQuantityUpdate = min <= appliedCardValue && max >= appliedCardValue;
    this.props.onUpdate({
      index: this.props.index,
      unitValue: typedCardValue,
      value: updatedValue.replace(/[$,]+/g, ''),
      feeValue: category === cartSummaryListtext.DEBIT ? calculateFee(appliedCardValue, feeRate, feeType, Number(`${!debitCardIssuedTo ? 3 : 0}`)) : '',
      isItemOrQuantityUpdate,
    });
    this.props.onUpdateDisableSubmit(!isItemOrQuantityUpdate);
  }

  deleteHandler = (e) => {
    e.preventDefault();
    const { productName } = this.props.item;
    this.props.onDelete(this.props.index, productName);
  }

  validateDebitInput = (event) => {
    const isDecimal = event.target.value.indexOf('.') !== -1;
    if (!isDecimal) {
      const cardValue = convertStringToNumber(removeSpecialChar(event.target.value));
      const cardVal = formatUSCurrency(roundToTwoDecimal(cardValue));
      this.handleInput(cardVal);
    }
  }

  renderCardValue() {
    const {
      isAvailable,
      productName = '',
      cardTypeDesc = '',
      category = '',
      cardValue = '',
    } = this.props.item;
    const cardAmtValue = formatUSCurrency(cardValue);

    const debId = category === cartSummaryListtext.DEBIT ? `debit-${this.props.index}` : `check-${this.props.index}`;
    return (
      <React.Fragment>
        <div className="card-description">
          <label id={`${debId}-label`}>{productName}</label>
          <p id={`${debId}-description`}>{cardTypeDesc}</p>
        </div>
        <div className="card-qty-value">
          <div className="card-value-qty">
            <div className="filterby">
              <div className="form-group gaf-form">
                <label id={`${debId}-card-value`}>
                  {category === cartSummaryListtext.DEBIT ? cartSummaryListtext.DEBIT_VALUE : cartSummaryListtext.CHECK_VALUE}
                </label>
                <input
                  id={`${debId}-input`}
                  type="text"
                  maxLength="12"
                  className="form-control"
                  onKeyPress={this.onKeyPress}
                  value={cardAmtValue}
                  onChange={(event) => this.handleInput(event.target.value)}
                  onBlur={this.validateDebitInput}
                  disabled={!isAvailable}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="card-qty-value box-visibility-hidden">
          <div className="filterby">
            <div className="form-group gaf-form"><label>{cartSummaryListtext.DEBIT_VALUE}</label>
              <input
                id={`debit-hidden${this.props.index}`}
                type="text"
                className="form-control"
              />
            </div>
          </div>
        </div>
      </React.Fragment >
    );
  }

  renderCardsPointFee() {
    const {
      feeTypeDesc = '',
      feeRate = '',
      feeType = '',
      category = '',
      cardValue = '',
      debitCardIssuedTo = '',
    } = this.props.item;
    const cardAmtValue = formatUSCurrency(roundToTwoDecimal(cardValue));
    const cardAmt = convertStringToNumber(cardAmtValue);
    const debId = category === cartSummaryListtext.DEBIT ? `debit-${this.props.index}` : `check-${this.props.index}`;
    const totalFee = calculateFee(cardAmt, feeRate, feeType, Number(`${!debitCardIssuedTo ? 3 : 0}`));
    return (
      <React.Fragment>
        <div className="card-points-fees">
          <p>
            <strong id={`${debId}-points`}>{cartSummaryListtext.POINTS}</strong>
            <strong id={`${debId}-card-amt`}>{formatCurrency(roundToTwoDecimal(cardAmt))}</strong>
          </p>
          {category === cartSummaryListtext.DEBIT ?
            <span id={`${debId}-tooltip`}>
              {cartSummaryListtext.FEE}: {totalFee}
              {feeTypeDesc &&
                <ToolTip id={`tooltip-${debId}`} text={feeTypeDesc}>
                  <i className="icon-question" role="button"></i>
                </ToolTip>
              }
            </span>
            : null}
        </div>
      </React.Fragment>
    );
  }

  renderDeleteHandler() {
    const { category = '' } = this.props.item;
    const debId = category === cartSummaryListtext.DEBIT ? `debit-${this.props.index}` : `check-${this.props.index}`;
    return (
      <React.Fragment>
        <div className="card-delete">
          <a href="/" onClick={this.deleteHandler}><i id={`${debId}-delete`} className="icon-delete"></i></a>
        </div>
      </React.Fragment>
    );
  }

  render() {
    const {
      min = '',
      max = '',
      isAvailable,
      img = '',
      category = '',
      cardValue = '',
      productName = '',
    } = this.props.item;
    const cardAmtValue = formatUSCurrency(roundToTwoDecimal(cardValue));
    const image = `${category === cartSummaryListtext.DEBIT ? cartSummaryListtext.DEFAULT_GIFT_CARD_IMAGE_URL : cartSummaryListtext.DEFAULT_CHECK_CARD_IMAGE_URL}`;
    const debId = category === cartSummaryListtext.DEBIT ? `debit-${this.props.index}` : `check-${this.props.index}`;
    const cardAmt = convertStringToNumber(cardAmtValue);
    return (
      <li>
        <div className="product-icon">
          <figure>
            <ImageHOC
              id={`${debId}-img-src`}
              isAbsolute={img}
              src={!img ? image : img}
              alt={productName}
            />
          </figure>
        </div>

        <div className="produtct-details" id="cart-debit-details">
          {this.renderCardValue()}
          {this.renderCardsPointFee()}
          {this.renderDeleteHandler()}
        </div>
        {category === cartSummaryListtext.DEBIT && !isAvailable ?
          <div className="gaf-error-message">
            {cartSummaryListtext.NOT_AVAILABLE_DEBIT}
          </div>
          : ''}
        {category === cartSummaryListtext.CHECK && !isAvailable ?
          <div className="gaf-error-message">
            {cartSummaryListtext.NOT_AVAILABLE_CHECK}
          </div>
          : ''}
        {cardAmt < min ?
          <div className="gaf-error-message">
            {cartSummaryListtext.MINIMUM_VALUE} {`$${min}`}
          </div> : ''
        }
        {cardAmt > max ?
          <div className="gaf-error-message">
            {cartSummaryListtext.MAXIMUM_VALUE}
          </div> : ''
        }
      </li>
    );
  }
}

CartSummaryDebit.propTypes = {
  index: PropTypes.number,
  item: PropTypes.object,
  onUpdate: PropTypes.func,
  onUpdateDisableSubmit: PropTypes.func,
  onDelete: PropTypes.func,
  min: PropTypes.number,
  img: PropTypes.string,
};
export default CartSummaryDebit;
