const cartSummaryListtext = {
  DEBIT_VALUE: 'Card Value',
  CHECK_VALUE: 'Value',
  POINTS: 'Points',
  DEBIT: 'debit',
  CHECK: 'check',
  GIFT_CARD: 'giftcards',
  FEE: 'Fee',
  NOT_AVAILABLE_CHECK: 'Check is no longer available. Please remove this Check from the cart.',
  TITLE: 'Alert Message',
  OK: 'Ok',
  CANCEL: 'Cancel',
  NOT_AVAILABLE_DEBIT: 'Sorry, this product is no longer available.',
  MINIMUM_VALUE: 'You have entered the less than minimum amount',
  MAXIMUM_VALUE: 'You have exceeded maximum amount.',
  DEFAULT_GIFT_CARD_IMAGE_URL: 'images/debit-card.jpg',
  DEFAULT_CHECK_CARD_IMAGE_URL: 'images/checkleaf.jpg',
};
export { cartSummaryListtext };

