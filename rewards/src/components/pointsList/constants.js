const tableHeaders = {
  pointListHeader: [
    { title: 'TYPE', key: 'cardType' },
    { title: 'PRODUCT', key: 'companyName' },
    { title: 'QTY', key: 'quantity' },
    { title: 'VALUE', key: 'amountRedeemed' },
    { title: 'FEE', key: 'serviceFee' },
    { title: 'TOTAL POINTS', key: 'denominationPoints' },
    { title: 'STATUS', key: 'itemStatus' },
    { title: 'UPDATED DATE', key: 'modifiedOn' },
  ],
  text: {
    Tabletitle: 'List of Product(s)', Rows: 'rows per page', Export: 'Export', filename: 'OrderDetails.csv',
  },

  csvConfig: [
    { colName: 'TYPE', key: 'cardType' },
    { colName: 'PRODUCT', key: 'companyName' },
    { colName: 'QTY', key: 'quantity' },
    { colName: 'VALUE', key: 'amountRedeemed' },
    { colName: 'FEE', key: 'serviceFee' },
    { colName: 'TOTAL POINTS', key: 'denominationPoints' },
    { colName: 'STATUS', key: 'itemStatus' },
    { colName: 'UPDATED DATE', key: 'modifiedOn' },
  ],
};

export { tableHeaders };
