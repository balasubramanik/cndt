import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryEachAddress from '../../components/cartSummaryEachAddress';

configure({ adapter: new Adapter() });


describe('cartSummaryEachAddress List', () => {
  const handleSelect = jest.fn();
  const tree = shallow(<CartSummaryEachAddress
    index={0}
    isChecked
    disableValue
    handleSelect={handleSelect}
    addressdescription="description"
  />);

  it('should be defined', () => {
    expect(CartSummaryEachAddress).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label element', () => {
    expect(tree.find('label').length).not.toBe(0);
  });

  it('should call onChange handler', () => {
    const simulateChange = tree.find('input').at(0).prop('onChange');
    simulateChange();
    expect(handleSelect).toBeCalled();
  });
});

describe('cartSummaryEachAddress List with Branch address', () => {
  const handleSelect = jest.fn();
  const tree = shallow(<CartSummaryEachAddress
    index={0}
    isChecked
    disableValue
    addressType="1"
    handleSelect={handleSelect}
    addressdescription="description"
  />);
  it('should be defined', () => {
    expect(CartSummaryEachAddress).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label as Office', () => {
    expect(tree.find('label').text()).toBe('Office');
  });
});

describe('cartSummaryEachAddress List with Mailing address', () => {
  const handleSelect = jest.fn();
  const tree = shallow(<CartSummaryEachAddress
    index={0}
    isChecked
    disableValue
    addressType="2"
    handleSelect={handleSelect}
    addressdescription="description"
  />);
  it('should be defined', () => {
    expect(CartSummaryEachAddress).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label as Mailing', () => {
    expect(tree.find('label').text()).toBe('Mailing');
  });
});

describe('cartSummaryEachAddress List with Headquarters address', () => {
  const handleSelect = jest.fn();
  const tree = shallow(<CartSummaryEachAddress
    index={0}
    addressType="3"
    handleSelect={handleSelect}
    addressdescription="description"
  />);
  it('should be defined', () => {
    expect(CartSummaryEachAddress).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render label as Headquarters', () => {
    expect(tree.find('label').text()).toBe('Headquaters');
  });
});
