import React from 'react';
import PropTypes from 'prop-types';
import { cartSummaryEachAddresstext } from './constants';

class CartSummaryEachAddress extends React.Component {
  handleAddressLabel(addressType) {
    const type = Number(addressType);
    switch (type) {
      case 1:
        return cartSummaryEachAddresstext.BRANCH;
      case 2:
        return cartSummaryEachAddresstext.MAILING;
      case 3:
        return cartSummaryEachAddresstext.HEAD_QUATERS;
      default:
        return null;
    }
  }
  render() {
    const {
      index,
      address1 = '',
      address2 = '',
      city = '',
      zipOrPostal = '',
      stateOrProvince = '',
      isChecked = '',
      disableValue = '',
      addressType = '',
    } = this.props;

    return (
      <div className="cart-address">
        <div className="address">
          <div className="gaf-radio">
            <input
              type="radio"
              checked={isChecked}
              id={`address${index}`}
              key={`${index.toString()}`}
              onChange={() => this.props.handleSelect(index)}
              disabled={disableValue}
              name="radio-group"
            />
            <label htmlFor={`address${index}`}>{this.handleAddressLabel(addressType)}</label>
          </div>
        </div>

        <div className="address-details">
          <p> {address1} </p>
          <p> {address2} </p>
          <p> {city},{stateOrProvince} {zipOrPostal} </p>
        </div>
      </div>
    );
  }
}

CartSummaryEachAddress.propTypes = {
  index: PropTypes.number,
  address1: PropTypes.string,
  address2: PropTypes.string,
  city: PropTypes.string,
  zipOrPostal: PropTypes.string,
  stateOrProvince: PropTypes.string,
  isChecked: PropTypes.bool,
  disableValue: PropTypes.bool,
  addressType: PropTypes.string,
  handleSelect: PropTypes.func,
};

export default CartSummaryEachAddress;
