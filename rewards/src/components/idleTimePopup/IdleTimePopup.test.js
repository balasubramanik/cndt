import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import IdleTimePopup from '../../components/idleTimePopup';


configure({ adapter: new Adapter() });
jest.useFakeTimers();
describe('IdleTimePopup', () => {
  const logoutHandler = jest.fn();
  const onConfirmClick = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<IdleTimePopup
    onConfirmClick={onConfirmClick}
    onLogout={logoutHandler}
  />);
  it('should be defined', () => {
    expect(IdleTimePopup).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render modal container', () => {
    expect(tree.find('.idle-popup').length).toBe(1);
  });

  it('should call onCloseClick', () => {
    const simulateClick = tree.find('Dialog').prop('onCloseClick');
    simulateClick(event);
    expect(tree.state().idle).toBe(false);
  });

  it('should call logoutHandler', () => {
    const simulateClick = tree.find('Dialog').prop('footer').props.children[0].props.onClick;
    simulateClick();
    expect(logoutHandler).toBeCalled();
  });

  it('should call onConfirmClick', () => {
    const simulateClick = tree.find('Dialog').prop('footer').props.children[1].props.onClick;
    simulateClick();
    expect(onConfirmClick).toBeCalled();
  });

  it('should call unmount', () => {
    jest.runAllTimers();
    tree.unmount();
  });
});
