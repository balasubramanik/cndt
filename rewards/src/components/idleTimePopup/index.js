
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-bootstrap';
import envConfig from 'envConfig'; //eslint-disable-line
import { createListener, removeListener } from '../../utils/utils';
import Dialog from '../../components/dialog';
import { idleTimePopup } from './constants';

const { idleSessionTime = '1800000', sessionTimeout = '60000' } = envConfig;

/** @description Class based component to render the Idle time popup
 */
class IdleTimePopup extends PureComponent {
  constructor(props) {
    super(props);
    this.sessionTimer = null;
    this.state = {
      idle: false,
    };
  }

  /** @description React life cycle method
   * It will invoke when the component is mounted
   */
  componentDidMount() {
    createListener(null, null, this.reset);
    this.reset();
  }

  /** @description React life cycle method
   * It will invoke when the component is unmounted
   */
  componentWillUnmount() {
    clearTimeout(this.sessionTimer);
    removeListener(null, null, this.reset);
  }

  /** @description callback function for close popup
   * @param {bool} restoreSession
   */
  closePopupHandler = (restoreSession) => {
    this.setState({ idle: false }, () => {
      if (!restoreSession) {
        this.clearAllTimeout();
        this.props.onLogout();
      } else {
        this.reset();
        this.props.onConfirmClick();
      }
    });
  }

  /** @description function to clear all timeouts
   */
  clearAllTimeout = () => {
    clearTimeout(this.idleTimer);
    clearTimeout(this.sessionTimer);
  }

  handleSessionTimeout = () => {
    this.sessionTimer = setTimeout(() => {
      this.closePopupHandler(false);
    }, sessionTimeout);
  }

  reset = () => {
    const { idle } = this.state;
    if (!idle) {
      this.clearAllTimeout();
      this.idleTimer = setTimeout(() => {
        this.setState({
          idle: true,
        });
        this.handleSessionTimeout();
      }, idleSessionTime);
    }
  }

  /** @description function to render idle time popup footer
   */
  renderFooter = () => (
    <div className="idle-popup-footer">
      <Button onClick={() => this.closePopupHandler(false)} className="btn gaf-btn-secondary btn-block">{idleTimePopup.CANCELTEXT}</Button>
      <Button onClick={() => this.closePopupHandler(true)} className="btn gaf-btn-primary btn-block">{idleTimePopup.CONFIRMTEXT}</Button>
    </div>
  );

  render() {
    const { idle } = this.state;
    return (
      <Dialog
        show={idle}
        title={idleTimePopup.TITLE}
        body={<p>{idleTimePopup.MESSAGE}</p>}
        onCloseClick={(e) => {
          e.preventDefault();
          this.closePopupHandler(false);
        }}
        footer={this.renderFooter()}
        className="gaf-model-popup idle-popup"
      />
    );
  }
}

/** PropTypes:
 * onConfirmClick - function - callback function for confirm/close button
 * onLogout - function - function to trigger the logout api
 */
IdleTimePopup.propTypes = {
  onConfirmClick: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
};

export default IdleTimePopup;
