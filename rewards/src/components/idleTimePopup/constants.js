const idleTimePopup = {
  TITLE: 'Session Expired',
  MESSAGE: 'Your session has been expired due to inactivity. Please click OK to continue your session.',
  CONFIRMTEXT: 'Ok',
  CANCELTEXT: 'Cancel',
  TIMEOUT_CONFIRMATION: 1800000,
  SESSION_TIMEOUT: 60000,
};

export { idleTimePopup };
