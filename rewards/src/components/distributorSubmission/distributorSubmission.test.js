import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import DistributorSubmission from '../distributorSubmission';


configure({ adapter: new Adapter() });

describe('DistributorSubmission', () => {
  const tree = shallow(<DistributorSubmission />);

  it('should be defined', () => {
    expect(DistributorSubmission).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
    tree.instance().onPdfClick();
  });
});
