const distributorSubmission = {
  DESCRIPTION: 'You can ask your distributor to submit a purchase activity report on your behalf.',
  NOTEINFO: 'PLEASE NOTE: ',
  NOTE1: 'Purchase activity reports will only be accepted if the ',
  NOTE2: 'distributor sends the information directly from their email address to GAFRewards@acbcoop.com. The file must include the correct fields ',
  NOTE3: '(fields are listed on the distributor instruction sheet). Reports will not be accepted if submitted by anyone other than the distributor regardless of the file format.',
  NOTE4: 'Please download the distributor instruction sheet and email it to your distributor.',
  NOTE5: 'Thank you!',
  BUTTONNAME: 'DOWNLOAD',
  FILENAME: 'Distributor_Submission',
};


export { distributorSubmission };
