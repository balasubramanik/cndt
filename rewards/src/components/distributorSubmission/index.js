import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import envConfig from 'envConfig';//eslint-disable-line
import { distributorSubmission } from './Constants';

export class DistributorSubmission extends PureComponent {
  onPdfClick = () => {
    const excelURL = envConfig.cdnUrl + envConfig.apiEndPoints.distributorInstructions;
    window.open(excelURL, '_blank');
    /* Trigger the GTM action */
    this.props.gtmUploadInvoiceActions.gtmDistributorClaimForm();
  }
  render() {
    return (
      <div className="distributor-submission-desc  panel-body-content">
        <div className="mail-invoice-desc">
          <p>{distributorSubmission.DESCRIPTION}</p>
        </div>
        <div className="mail-invoice-note">
          <p><strong>{distributorSubmission.NOTEINFO}</strong>{distributorSubmission.NOTE1}<strong>{distributorSubmission.NOTE2}</strong>{distributorSubmission.NOTE3}</p>
        </div>
        <p>{distributorSubmission.NOTE4}</p>
        <p><strong>{distributorSubmission.NOTE5}</strong></p>
        <div className="print-claim-form-btn">
          <button className="btn gaf-btn-primary" onClick={this.onPdfClick}>{distributorSubmission.BUTTONNAME}</button>
        </div>
      </div>
    );
  }
}

DistributorSubmission.propTypes = {
  gtmUploadInvoiceActions: PropTypes.object,
};

export default DistributorSubmission;
