import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-rangeslider';

/** @description Class based component to render Range slider */
class RangeSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
  }

  /** @description Callback function for change the range
   * @param {number} value
   */
  handleChange = (value) => {
    this.setState({ value });
    this.props.onChange(value);
  };

  render() {
    const {
      labels, formatValue, step, min, max, tooltip,
    } = this.props;
    const { value } = this.state;
    return (
      <div className="col-md-9 slider custom-labels ">
        <Slider
          min={min}
          max={max}
          step={step}
          orientation="horizontal"
          value={value}
          labels={labels}
          format={formatValue}
          onChange={this.handleChange}
          tooltip={tooltip}
        />
      </div>
    );
  }
}

/** PropTypes:
 * labels - object - labels for range slider
 * formatValue - func - callback function for format the value
 * min - number - minimum value of range slider
 * max - number - maximum value of range slider
 * step - number - step value of range slider
 * onChange - func - callback func for change the range
 * tooltip - boolean - flag for show/hide the tooltip
 */
RangeSlider.propTypes = {
  labels: PropTypes.object,
  formatValue: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  onChange: PropTypes.func,
  value: PropTypes.number,
  tooltip: PropTypes.bool,
};

RangeSlider.defaultProps = {
  formatValue: (value) => value,
  min: 0,
  max: 100,
  step: 1,
  value: 0,
  onChange: () => { },
  tooltip: false,
};

export default RangeSlider;
