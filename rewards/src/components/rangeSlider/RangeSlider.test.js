import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RangeSlider from '../../components/rangeSlider';


configure({ adapter: new Adapter() });

describe('RangeSlider with props', () => {
  const formatHandler = jest.fn();
  const changehandler = jest.fn();
  const tree = shallow(<RangeSlider
    labels={{
      0: '|',
      1: '|',
      2: '|',
      3: '|',
    }}
    formatValue={formatHandler}
    onChange={changehandler}
  />);

  it('should be defined', () => {
    expect(RangeSlider).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render slider element', () => {
    expect(tree.find('.slider').length).toBe(1);
  });

  it('should render Slider Component', () => {
    expect(tree.find('Slider').length).toBe(1);
  });

  it('should render changehandler', () => {
    const simulateChange = tree.find('Slider').prop('onChange');
    simulateChange();
    expect(changehandler).toBeCalled();
  });

  it('should render formatHandler', () => {
    const simulateChange = tree.find('Slider').prop('format');
    simulateChange();
    expect(formatHandler).toBeCalled();
  });
});


describe('RangeSlider without props', () => {
  const formatHandler = jest.fn();
  const changehandler = jest.fn();
  const tree = shallow(<RangeSlider
    labels={{
      0: '|',
      1: '|',
      2: '|',
      3: '|',
    }}
  />);

  it('should be defined', () => {
    expect(RangeSlider).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render slider element', () => {
    expect(tree.find('.slider').length).toBe(1);
  });

  it('should render Slider Component', () => {
    expect(tree.find('Slider').length).toBe(1);
  });

  it('should render changehandler', () => {
    const simulateChange = tree.find('Slider').prop('onChange');
    simulateChange();
    expect(changehandler).not.toBeCalled();
  });

  it('should render formatHandler', () => {
    const simulateChange = tree.find('Slider').prop('format');
    simulateChange();
    expect(formatHandler).not.toBeCalled();
  });
});
