import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Field } from 'redux-form';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import { locationSearch } from './constants';
import { trim } from '../../utils/utils';

let validGoogleApi = true;
window.gm_authFailure = () => { validGoogleApi = false; };

class LocationSearchInput extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: '',
    };
    this.activeSuggestion = null;
  }

  /** @description function to clear an error message
   */
  clearErrorMessage = () => {
    const { errorMessage } = this.state;
    if (errorMessage) {
      this.setState({ errorMessage: '' });
    }
  }

  /** @description callBack function for blur event in search field
   */
  handleBlur = (e, value) => {
    const val = trim(value);
    if (this.activeSuggestion) {
      this.handleLocationSelect(this.activeSuggestion);
    }
    this.props.onBlur(val);
  }

  /** @description callBack function for change an address in search field
   * @param {string} address - address text
   */
  handleLocationChange = (address) => {
    this.props.onChange(address);
    this.clearErrorMessage();
  };

  /** @description callBack function for select an address in dropdown
   * @param {string} address - address text
   */
  handleLocationSelect = (address) => {
    let addressComponents = null;
    geocodeByAddress(address)
      .then((results) => {
        [addressComponents] = results;
        return getLatLng(addressComponents);
      }).then((latLng) => {
        this.activeSuggestion = null;
        this.props.onSelect(addressComponents, latLng);
      });
  };

  /** @description callBack function for error address
   * @param {string} status - error message
   * @param {function} clearSuggestions - function to remove an address text from search field
   */
  handleLocationError = (status, clearSuggestions) => {
    this.setState({ errorMessage: status }, () => {
      clearSuggestions();
    });
  };

  /** @description function to render the autocomplete dropdown
   * @param {array} suggestion
   * @param {function} getSuggestionItemProps
   * @param {boolean} loading
   */
  renderDropdown = ({
    suggestions,
    getSuggestionItemProps,
    loading,
  }) => {
    this.activeSuggestion = null;
    return (
      <div className="autocomplete-dropdown-container">
        {loading && validGoogleApi && <div className="add-loading">{locationSearch.LOADING}</div>}
        {suggestions.map((suggestion, i) => {
          if (suggestion.active) {
            this.activeSuggestion = suggestion.description;
          }
          const className = suggestion.active
            ? 'suggestion-item-active'
            : 'suggestion-item';
          return (
            <div
              key={i.toString()}
              {...getSuggestionItemProps(suggestion, {
                className,
              })}
            >
              <span>{suggestion.description}</span>
            </div>
          );
        })}
      </div>
    );
  }
  /** @description function to render the input field
  * @param {object} fields
  */
  renderInput = (fields) => {
    const {
      input, type, autoComplete, onKeyDown, role, maxLength, className, meta: { touched, error },
    } = fields;
    const isInvalidField = touched && error;
    const attr = {
      'aria-activedescendant': fields['aria-activedescendant'],
      'aria-autocomplete': fields['aria-autocomplete'],
      'aria-expanded': fields['aria-expanded'],
      autoComplete,
      onKeyDown,
      role,
      maxLength,
      className: classNames(`${className}`, { 'input-error': isInvalidField }),
    };
    return (
      <div>
        <input
          {...attr}
          {...input}
          type={type}
          onBlur={(event) => {
            const { target } = event;
            const { value } = target;
            const trimmedValue = trim(value);
            input.onBlur(trimmedValue);
          }}
        />
        {isInvalidField && <span className="error-message">{error}</span>}
      </div>
    );
  }

  /** @description function to render the input field
  * @param {function} getInputProps
  * @param {array} suggestion
  * @param {function} getSuggestionItemProps
  * @param {boolean} loading
  */
  renderSearchField = ({
    getInputProps,
    suggestions,
    getSuggestionItemProps,
    loading,
  }) => {
    const { name } = this.props;
    return (
      <div className="location-search">
        <Field
          type="text"
          name={name}
          maxLength={55}
          component={this.renderInput}
          {...getInputProps({
            className: 'form-control location-search-input',
            onBlur: this.handleBlur,
          })}
        />
        {
          this.renderDropdown({
            suggestions,
            getSuggestionItemProps,
            loading,
          })
        }
      </div>
    );
  }

  render() {
    const { value } = this.props;
    return (
      <React.Fragment>
        <PlacesAutocomplete
          value={value}
          onChange={this.handleLocationChange}
          onSelect={this.handleLocationSelect}
          onError={this.handleLocationError}
          searchOptions={{
            componentRestrictions: { country: 'us' },
          }}
        >
          {this.renderSearchField}
        </PlacesAutocomplete>
      </React.Fragment>
    );
  }
}

/** PropTypes:
 * name - string - name of element
 * onSelect - function - callback function for select an address from dropdown
 * onChange - function - callback function for select an address from search field
 * onBlur - function - callback function for focusout the address field
 * value - string - value of location input
 */
LocationSearchInput.propTypes = {
  name: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  value: PropTypes.string,
};

LocationSearchInput.defaultProps = {
  value: '',
};

export default LocationSearchInput;
