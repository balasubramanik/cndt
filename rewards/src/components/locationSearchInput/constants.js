const locationSearch = {
  NO_RESULTS: 'No Results',
  LOADING: 'Loading...',
};

export { locationSearch };
