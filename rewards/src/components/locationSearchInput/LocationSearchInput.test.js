import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LocationSearchInput from '../../components/locationSearchInput';

configure({ adapter: new Adapter() });
describe('LocationSearchInput', () => {
  const changeHandler = jest.fn();
  const selecthandler = jest.fn();
  const clearSuggestions = jest.fn();
  const blurHandler = jest.fn();
  const getSuggestionItemProps = jest.fn();
  const suggestions = [{ active: true, name: 'orlando' }, { active: false, name: 'texas' }];
  const tree = shallow(<LocationSearchInput
    name="address1"
    onChange={changeHandler}
    onSelect={selecthandler}
    onBlur={blurHandler}
  />);
  it('should be defined', () => {
    expect(LocationSearchInput).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render PlacesAutocomplete', () => {
    expect(tree.find('PlacesAutocomplete').length).toBe(1);
  });

  it('should call selecthandler', () => {
    const getInputProps = jest.fn();
    tree.instance().renderSearchField({
      getInputProps,
      suggestions,
      getSuggestionItemProps,
    });
    const simulateClick = tree.find('PlacesAutocomplete').prop('onSelect');
    simulateClick('orlando');
    expect(changeHandler).not.toBeCalled();
  });

  it('should call handleError', () => {
    tree.instance().renderInput({
      meta: {
        touched: true,
        error: true,
      },
    });
    const simulateClick = tree.find('PlacesAutocomplete').prop('onError');
    simulateClick('no results found', clearSuggestions);
    expect(tree.state().errorMessage).toBe('no results found');
  });

  it('should call changeHandler', () => {
    const simulateClick = tree.find('PlacesAutocomplete').prop('onChange');
    simulateClick('texas');
    expect(changeHandler).toBeCalled();
  });

  it('should call HandleBlur', () => {
    tree.instance().handleBlur({}, '123');
    expect(blurHandler).toBeCalled();
  });

  // it('should render Search Field', () => {
  //   expect(tree.find('input').length).toBe(1);
  // });

  // it('should render Autocomplete', () => {
  //   const { renderDropdown } = tree.instance();
  //   renderDropdown({ suggestions, getSuggestionItemProps, loading: true });
  //   expect(tree.find('.autocomplete-dropdown-container').length).toBe(1);
  // });
});
