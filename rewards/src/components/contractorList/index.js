import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Tables from '../tables';
import Toggle from '../toggle';
import ToolTip from '../toolTip';
import { history } from '../../routes';
import { contractorListConstants } from './constants';
import RouteConstants from '../../constants/RouteConstants';
class ContractorList extends React.Component {
  onToggleChange = (e, index) => {
    e.preventDefault();
    const { contractorListInfo } = this.props;
    this.props.toggleAction(contractorListInfo[index], index);
  }

  sortBy = () => {
    this.props.sortByAction();
  }

  formatContractorList = () => {
    const { contractorListInfo } = this.props;
    if (contractorListInfo) {
      return contractorListInfo.map((item, i) => {
        const {
          displayContractorId, contractorName, contractorType, planType, address: { city, stateOrProvince },
        } = item;
        return {
          ...item,
          displayContractorId: displayContractorId || 'N/A',
          contractorName: <ToolTip id={contractorName || 'N/A'} text={contractorName || 'N/A'}><span role="button">{contractorName || 'N/A'}</span></ToolTip>,
          city: <ToolTip id={city || 'N/A'} text={city || 'N/A'}><span role="button">{city || 'N/A'}</span></ToolTip>,
          state: stateOrProvince || 'N/A',
          contractorType: contractorType || 'N/A',
          planType: planType || 'N/A',
          isAchEnabled: <Toggle id={i} onChange={this.onToggleChange} isToggle={item.isAchEnabled ? 'yes' : 'no'} />,
          planDetails: <button className="btn gaf-btn-secondary gaf-btn-sm" onClick={(e) => this.redirectToPlanType(e, item)}>{contractorListConstants.VIEW_BUTTON}</button>,
        };
      });
    }
    return [];
  }

  redirectToPlanType = (e, item) => {
    e.preventDefault();
    const {
      contractorId,
      displayContractorId,
      contractorName,
      parentContractorId,
      address,
    } = item;
    const data = {
      contractorId,
      displayContractorId,
      contractorName,
      parentContractorId,
      address,
      index: 1,
      selectedYear: moment().year(),
    };
    history.push({
      pathname: RouteConstants.PLAN_DETAILS,
      state: data,
    });
  }

  render() {
    const { onSort, loading } = this.props;
    return (
      <div>
        <Tables
          config={contractorListConstants.headers}
          data={this.formatContractorList()}
          onSortingClick={onSort}
          isLoading={loading}
        >
        </Tables>
      </div>
    );
  }
}

/** PropTypes:
 * contractorListInfo- array containing contractor table records
 * sortByAction- action for table sorting
 * toggleAction- action for toggle change
 */
ContractorList.propTypes = {
  contractorListInfo: PropTypes.array,
  sortByAction: PropTypes.func,
  toggleAction: PropTypes.func,
  onSort: PropTypes.func,
  loading: PropTypes.bool,
};
export default ContractorList;
