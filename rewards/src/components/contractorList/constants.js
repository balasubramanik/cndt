const contractorListConstants = {
  headers: [
    { title: 'ACCOUNT #', key: 'displayContractorId', sort: true },
    { title: 'CONTRACTOR NAME', key: 'contractorName', sort: true },
    { title: 'CITY', key: 'city', sort: true },
    { title: 'STATE', key: 'state', sort: true },
    { title: 'CONTRACTOR TYPE', key: 'contractorType', sort: true },
    { title: 'PLAN TYPE', key: 'planType', sort: true },
    { title: 'REDEEM ACH', key: 'isAchEnabled', sort: false },
    { title: 'PLAN DETAILS', key: 'planDetails', sort: false },
  ],
  VIEW_BUTTON: 'View',
};

export { contractorListConstants };
