import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ContractorList from '../../components/contractorList';
import { history } from '../../routes';
configure({ adapter: new Adapter() });
history.push = jest.fn();
describe('contractorList', () => {
  const sortOrderHandler = jest.fn();
  const toggleHandler = jest.fn();
  const sortHandler = jest.fn();
  const tree = shallow(<ContractorList
    contractorListInfo={[
      {
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        address: {
          city: 'Parsippany',
          state: 'NJ',
        },
        contractorType: 'Certified',
        planType: 'Custom',
        redeemAch: 'yes',
      },
    ]}
    sortByAction={sortOrderHandler}
    toggleAction={toggleHandler}
    onSort={sortHandler}
    loading
  />);
  it('should be defined', () => {
    expect(ContractorList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Table Component', () => {
    expect(tree.find('Tables').length).toBe(1);
  });

  it('should not show Table Rows when contractList array not supplied', () => {
    const tableTree = shallow(<ContractorList
      contractorListInfo={undefined}
    />);
    expect(tableTree.find('Tables').prop('data').length).toBe(0);
  });
});

describe('contractorList with redirect link to plan details', () => {
  const sortOrderHandler = jest.fn();
  const sortHandler = jest.fn();
  const toggleHandler = jest.fn();
  const e = { preventDefault: () => {} };
  const tree = mount(<ContractorList
    contractorListInfo={[
      {
        custID: '114456',
        contractorName: 'Ridgeline roofing',
        contractorType: 'Certified',
        planType: 'Custom',
        isAchEnabled: 'yes',
        address: {
          city: 'Parsippany',
          state: 'NJ',
        },
      },
    ]}
    sortByAction={sortOrderHandler}
    toggleAction={toggleHandler}
    onSort={sortHandler}
    loading
  />);

  it('should call onChange event', () => {
    const simulateChange = tree.find('input').prop('onChange');
    simulateChange(e);
    expect(toggleHandler).toBeCalled();
  });
  it('should redirect to Plan Details', () => {
    const simulateChange = tree.find('button').at(0).prop('onClick');
    simulateChange(e, {
      custID: '114456',
      contractorName: 'Ridgeline roofing',
      contractorType: 'Certified',
      planType: 'Custom',
      isAchEnabled: 'yes',
      address: {
        city: 'Parsippany',
        state: 'NJ',
      },
    });
    expect(history.push).toBeCalled();
  });
});

describe('contractorList with unsupplied contractorlist property values', () => {
  const sortOrderHandler = jest.fn();
  const toggleHandler = jest.fn();
  const sortHandler = jest.fn();
  const tree = shallow(<ContractorList
    contractorListInfo={[
      {
        custID: '114456',
        contractorName: '',
        address: {
          city: '',
          state: 'NJ',
        },
        contractorType: '',
        planType: '',
        isAchEnabled: 'yes',
      },
    ]}
    sortByAction={sortOrderHandler}
    toggleAction={toggleHandler}
    onSort={sortHandler}
    loading
  />);
  it('should be defined', () => {
    expect(ContractorList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
