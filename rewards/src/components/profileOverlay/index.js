import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import classNames from 'classnames';
import RewardsInfo from '../../components/rewardsInfo';
import UserInfo from '../../components/userInfo';
import { profileOverlay } from './constants';

const ProfileOverlay = ({
  info, onSignOutClick, onMyAccountClick, onViewDetailsClick, isRewardPointAccess, isAdmin,
}) => {
  const names = classNames('btn gaf-btn-primary', { 'btn-block': !isAdmin });
  const details = isAdmin ? info.adminUserInfo : info.userInfo;
  return (
    <div className="userinfo-dropdown">
      <UserInfo details={details} isAdmin={isAdmin} />
      {!isAdmin && <RewardsInfo details={info.rewardsInfo} onViewDetailsClick={onViewDetailsClick} isRewardPointAccess={isRewardPointAccess} />}
      <div className="userinfo-buttons">
        <Row className="show-grid">
          {!isAdmin &&
          <Col xs={6} md={6}>
            <button className="btn gaf-btn-secondary btn-block" onClick={onMyAccountClick}>{profileOverlay.MYACCOUNT}</button>
          </Col>
          }
          <Col xs={isAdmin ? 12 : 6} md={isAdmin ? 12 : 6} className={isAdmin ? 'text-center' : ''}>
            <button className={names} onClick={onSignOutClick}>{profileOverlay.SIGNOUT}</button>
          </Col>
        </Row>
      </div>
    </div>
  );
};

/** PropTypes:
 * info - object - contains the rewards information and profile information
 * onSignOutClick - func - function to trigger the logout api
 * onViewDetailsClick - func - callback function for view more button
 * onMyAccountClick - func - callback function for myaccount button
 */
ProfileOverlay.propTypes = {
  info: PropTypes.object.isRequired,
  onSignOutClick: PropTypes.func.isRequired,
  onViewDetailsClick: PropTypes.func.isRequired,
  onMyAccountClick: PropTypes.func.isRequired,
  isRewardPointAccess: PropTypes.bool,
  isAdmin: PropTypes.bool,
};

export default ProfileOverlay;
