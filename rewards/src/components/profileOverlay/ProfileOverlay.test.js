import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ProfileOverlay from '../../components/profileOverlay';


configure({ adapter: new Adapter() });

describe('ProfileOverlay', () => {
  const signouthandler = jest.fn();
  const viewdetailshandler = jest.fn();
  const myaccounthandler = jest.fn();
  const tree = shallow(<ProfileOverlay
    info={{
      userInfo: {
        email: 'Brain@gaf.com',
        firstName: 'Brain',
        lastName: 'Granger',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
        contractInfo: {
          contractorName: 'Queens Roofing and Tinsmithing Inc',
          contractorImage: 'tinsmiths.jpg',
        },
      },
      rewardsInfo: {
        rewardsPoint: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    }}
    onSignOutClick={signouthandler}
    onViewDetailsClick={viewdetailshandler}
    onMyAccountClick={myaccounthandler}
    isAdmin={false}
  />);
  it('should be defined', () => {
    expect(ProfileOverlay).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render User Info', () => {
    expect(tree.find('UserInfo').length).toBe(1);
  });

  it('should render Rewards Info', () => {
    expect(tree.find('RewardsInfo').length).toBe(1);
  });

  it('should render My account button', () => {
    expect(tree.find('.gaf-btn-secondary').length).toBe(1);
  });

  it('should render Signout button', () => {
    expect(tree.find('.gaf-btn-primary').length).toBe(1);
  });
});

describe('ProfileOverlay with isAdmin true', () => {
  const signouthandler = jest.fn();
  const viewdetailshandler = jest.fn();
  const myaccounthandler = jest.fn();
  const tree = shallow(<ProfileOverlay
    info={{
      userInfo: {
        email: 'Brain@gaf.com',
        firstName: 'Brain',
        lastName: 'Granger',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
        contractInfo: {
          contractorName: 'Queens Roofing and Tinsmithing Inc',
          contractorImage: 'tinsmiths.jpg',
        },
      },
      rewardsInfo: {
        rewardsPoint: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    }}
    onSignOutClick={signouthandler}
    onViewDetailsClick={viewdetailshandler}
    onMyAccountClick={myaccounthandler}
    isAdmin
  />);
  it('should be defined', () => {
    expect(ProfileOverlay).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
