import React from 'react';
import PropTypes from 'prop-types';
import { Panel } from 'react-bootstrap';

const Accordion = (props) => {
  const {
    title, body, index, details, onSelect, id,
  } = props;
  return (
    <Panel eventKey={index} id={id}>
      <Panel.Heading>
        <Panel.Title toggle>{title}</Panel.Title>
        <Panel.Title><a href="/" className="pull-right" onClick={(e) => onSelect(e, index)}>{details}</a></Panel.Title>
      </Panel.Heading>
      {body &&
        <Panel.Body collapsible>
          <div>
            {body}
          </div>
        </Panel.Body>
      }
    </Panel>
  );
};
/** PropTypes:
 * body - {string || number || element} - body content of Accordion
 * onSelect - func - function to select the accordion
 * title - {string || number || element} - title of Accordion
 * index - id of the accordion
 * details - link to the spot Promotion details page
 */
Accordion.propTypes = {
  body: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
  index: PropTypes.number,
  details: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
  onSelect: PropTypes.func,
  id: PropTypes.string,
};

export default Accordion;
