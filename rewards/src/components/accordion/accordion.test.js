import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Accordion from '../../components/accordion';


configure({ adapter: new Adapter() });

describe('Accordion with onClick', () => {
  const onSelect = jest.fn();
  const tree = shallow(<Accordion
    title={<p className="title">TITLE</p>}
    onClick={onSelect}
    body={<p className="body-content">Sample Body</p>}
  />);
  it('should be defined', () => {
    expect(Accordion).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render panel heading', () => {
    expect(tree.find('PanelHeading').length).toBe(1);
  });

  it('should render panel title', () => {
    expect(tree.find('PanelTitle').length).toBe(1);
  });

  it('should render panel body', () => {
    expect(tree.find('PanelBody').length).toBe(1);
  });

  it('should call onClick handler', () => {
    const simulateClick = tree.find('a').prop('onClick');
    simulateClick();
    expect(onSelect).toBeCalled();
  });
});


describe('Accordion without onClick', () => {
  const onSelect = jest.fn();
  const tree = shallow(<Accordion
    title={<p className="title">TITLE</p>}
    body={<p className="body-content">Sample Body</p>}
  />);
  it('should be defined', () => {
    expect(Accordion).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render panel header', () => {
    expect(tree.find('PanelHeading').length).toBe(0);
  });

  it('should not render panel title', () => {
    expect(tree.find('PanelTitle').length).toBe(0);
  });

  it('should not render panel body', () => {
    expect(tree.find('PanelBody').length).toBe(0);
  });

  it('should call onClick handler', () => {
    const simulateChange = tree.find('a').prop('onClick');
    simulateChange();
    expect(onSelect).not.toBeCalled();
  });
});
