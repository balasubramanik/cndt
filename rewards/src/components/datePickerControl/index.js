import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import DatePicker from 'react-datepicker';

/** @description Callback function for date change
  * @param {object} date - selected date
  * @param {func} callback - callback function for date change
  * @param {object} input - property which is comes from redux-form
  */
const handleChange = (date, callBack, input) => {
  callBack(date);
  input.onChange(date);
};

/** @description Functional component to render the date picker */
const DatePickerControl = (props) => {
  const {
    id, label, value, input, disabled, placeholder, meta, min, max, onChange, className,
  } = props;
  const { touched, error } = meta;
  let selectedDate = value || input.value;
  selectedDate = selectedDate ? moment(selectedDate) : null;
  const datePickerClass = classNames('form-control', { 'input-error': touched && error });
  const minDate = min ? { minDate: typeof (min) === 'object' ? min : moment().subtract(min, 'days') } : {};
  const maxDate = max ? { maxDate: typeof (max) === 'object' ? max : moment().add(max, 'days') } : {};
  const calenderClass = (className) ? 'gaf-form-calender pull-right' : 'gaf-form-calender';
  return (
    <div className="gaf-form clearfix">
      {label && <label dangerouslySetInnerHTML={{ __html: label }} />}
      <div className={calenderClass}>
        <DatePicker
          calendarClassName="gaf-date-picker"
          id={id}
          className={datePickerClass}
          selected={selectedDate}
          onChange={(date, event) => { event.preventDefault(); handleChange(date, onChange, input); }}
          onChangeRaw={(event) => event.preventDefault()}
          disabled={disabled}
          placeholderText={placeholder}
          fixedHeight
          showMonthDropdown
          useShortMonthInDropdown
          dropdownMode="select"
          {...minDate}
          {...maxDate}
        >
          <button className="date-clear-btn" onClick={(event) => { event.preventDefault(); handleChange('', onChange, input); }}>Clear</button>
        </DatePicker>
        <label className="icon-calendar" htmlFor={id}></label>
        {touched && error && error.length > 0 && <span className="error-message text-danger">{error}</span>}
      </div>
    </div>
  );
};

/** PropTypes:
 * onChange - func - callback function for select date
 * label - string - label name
 * meta - object - validation property comes from redux-form
 * input - object - property comes from redux-form
 * value - object - value of datepicker
 * placeholder - string - placeholder text of element
 * readOnly - boolean - flag to enable datepicker
 */
DatePickerControl.propTypes = {
  onChange: PropTypes.func,
  label: PropTypes.string,
  meta: PropTypes.object,
  input: PropTypes.object,
  value: PropTypes.object,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
  min: PropTypes.object,
  max: PropTypes.object,
  id: PropTypes.string,
  className: PropTypes.string,
};

DatePickerControl.defaultProps = {
  onChange: () => { },
  input: { value: '', onChange: () => { } },
  meta: {},
  placeholder: '',
  disabled: false,
};

export default DatePickerControl;
