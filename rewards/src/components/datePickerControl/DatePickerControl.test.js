import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import moment from 'moment';
import DatePickerControl from '../../components/datePickerControl';


configure({ adapter: new Adapter() });

describe('DatePickerControl with min max as number', () => {
  const onChangeHandler = jest.fn();
  const inputOnChange = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<DatePickerControl
    onChange={onChangeHandler}
    label="date"
    meta={{ touched: true, error: 'abcdexyz' }}
    input={{
      value: [2018, 4, 12],
      onChange: inputOnChange,
    }}
    value={{}}
    placeholder="select"
    min={5}
    max={5}
    className="abc"
  />);

  it('should be defined', () => {
    expect(DatePickerControl).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render DatePicker', () => {
    expect(tree.find('DatePicker').length).toBe(1);
  });

  it('should call onChange for datepicker', () => {
    const simulateChange = tree.find('DatePicker').prop('onChange');
    simulateChange('10/11/2018', event);
    expect(onChangeHandler).toBeCalled();
    expect(inputOnChange).toBeCalled();
  });

  it('should call onChangeRaw', () => {
    const simulateChange = tree.find('DatePicker').prop('onChangeRaw');
    simulateChange(event);
  });

  it('should call onChange for Clear button', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange(event);
    expect(inputOnChange).toBeCalled();
  });
});

describe('DatePickerControl with min, max as objects', () => {
  const onChangeHandler = jest.fn();
  const inputOnChange = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<DatePickerControl
    onChange={onChangeHandler}
    placeholder="select"
    min={moment()}
    max={moment()}
    input={{
      value: [2018, 4, 12],
      onChange: inputOnChange,
    }}
    label="date"
    meta={{ touched: true, error: true }}
  />);

  it('should be defined', () => {
    expect(DatePickerControl).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render DatePicker', () => {
    expect(tree.find('DatePicker').length).toBe(1);
  });

  it('should render onChange for datepicker', () => {
    const simulateChange = tree.find('DatePicker').prop('onChange');
    simulateChange('10/11/2018', event);
    expect(inputOnChange).toBeCalled();
  });

  it('should call onChange for Clear button', () => {
    const simulateChange = tree.find('button').prop('onClick');
    simulateChange(event);
    expect(inputOnChange).toBeCalled();
  });
});

describe('DatePickerControl with error message', () => {
  const onChange = jest.fn();
  const tree = shallow(<DatePickerControl
    onChange={onChange}
    placeholder="select"
    label="date"
    meta={{ touched: true, error: true }}
  />);

  it('should be defined', () => {
    expect(DatePickerControl).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
