const constants = {
  500: 'Something went wrong. Please try again.',
  504: 'The server is taking too long to respond OR something is wrong with your internet connection. Please try again later.',
  400: 'Something went wrong. Please try again.',
  204: 'Something went wrong. Please try again.',
  '01036': 'You have already completed the registration. You cannot submit it again.',
  '03020': 'Available balance is not enough to place the order',
  '03021': 'Some Gift cards are not available',
  '03022': 'Gift Card value does not fall on the range',
  '03023': 'Sorry, your cart is empty. Please try again',
  '01039': 'Your email address has already been verified. So verification email cannot resent.',
  '05100': 'Promotion title you have entered is already exist in the system. Please check and try again.',
  '05102': 'This promotion has already been approved.',
  '05103': 'This promotion has already been inactivated.',
  giftCardExceeds: 'Sorry, you can only redeem [max] [category] at a time.',
};

export { constants };
