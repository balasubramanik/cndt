import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '../dialog';
import { constants } from './constants';

/** @description Function to get error message based on conditions
 * @param {object} err - contains the error details
 */
const getErrorMessage = (err, data) => {
  const { errors, status } = err || {};
  const code = (errors && errors[0] && errors[0].code) ? errors[0].code : null;
  let errorMessage = constants[code] || constants[status] || constants[500];
  if (data) {
    errorMessage = errorMessage.replace(/\[(\w+)\]/g, (match, key) => data[key] || '');
  }
  return (
    <div className="gaf-toast-alert-dialog">
      <span className="icon-alert-danger"></span>
      <p>{errorMessage}</p>
    </div>);
};

/** @description Functional based component for render Server Error */
const ErrorNotification = (props) => {
  const { error, data, onClear } = props;
  if (error) {
    return (
      <Dialog
        className="gaf-model-popup gaf-toast-alert-main"
        body={getErrorMessage(error, data)}
        onCloseClick={onClear}
        show={Boolean(error)}
      />
    );
  }
  return null;
};

/** PropTypes
 * error - any - contains the error messages
 * data - object - contains the dyamic values
 * onClear - func - function to clear all messages
 */
ErrorNotification.propTypes = {
  error: PropTypes.any,
  data: PropTypes.object,
  onClear: PropTypes.func,
};

export default ErrorNotification;
