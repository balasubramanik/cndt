import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ErrorNotification from '../../components/errorNotification';


configure({ adapter: new Adapter() });

describe('ErrorNotification with Invalid data error', () => {
  const clearHandler = jest.fn();
  const tree = shallow(<ErrorNotification error={{ errors: [{ code: '01036' }] }} onClear={clearHandler} />);

  it('should be defined', () => {
    expect(ErrorNotification).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Dialog', () => {
    expect(tree.find('Dialog').length).toBe(1);
  });

  it('should call clearHandler', () => {
    const simulateClick = tree.find('Dialog').prop('onCloseClick');
    simulateClick();
    expect(clearHandler).toBeCalled();
  });
});

describe('ErrorNotification without server error', () => {
  const clearHandler = jest.fn();
  const tree = shallow(<ErrorNotification error={{ status: 400 }} onClear={clearHandler} />);

  it('should be defined', () => {
    expect(ErrorNotification).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Dialog', () => {
    expect(tree.find('Dialog').length).toBe(1);
  });

  it('should call clearHandler', () => {
    const simulateClick = tree.find('Dialog').prop('onCloseClick');
    simulateClick();
    expect(clearHandler).toBeCalled();
  });
});

describe('ErrorNotification with error', () => {
  const tree = shallow(<ErrorNotification />);

  it('should be defined', () => {
    expect(ErrorNotification).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render Dialog', () => {
    expect(tree.find('Dialog').length).toBe(0);
  });
});
