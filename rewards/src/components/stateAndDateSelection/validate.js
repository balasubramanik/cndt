import { texts } from '../../containers/SpotPromotionManagement/constant';

const validate = (values) => {
  const { REQUIRED_FIELD } = texts;
  const errors = {};
  if (!values.title) {
    errors.title = REQUIRED_FIELD;
  }
  if (values.states) {
    const { states } = values;
    if (states && states.itemSelected) {
      const selectedStates = states.itemSelected.filter((item) => item[1].checked);
      if (selectedStates.length === 0) {
        errors.states = REQUIRED_FIELD;
      }
    } else if (states.length === 0) {
      errors.states = REQUIRED_FIELD;
    }
  }
  if (!values.startDate) {
    errors.startDate = REQUIRED_FIELD;
  }
  if (values.startDate && !values.endDate) {
    errors.endDate = REQUIRED_FIELD;
  }
  if (values.endDate && !values.displayStartDate) {
    errors.displayStartDate = REQUIRED_FIELD;
  }
  if (values.displayStartDate && !values.displayEndDate) {
    errors.displayEndDate = REQUIRED_FIELD;
  }
  return errors;
};
export { validate };

