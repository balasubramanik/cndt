import React, { PureComponent } from 'react';
import { Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Field } from 'redux-form';
import moment from 'moment';
import TextInput from '../textInput';
import { Texts } from './constant';
import DatePickerControl from '../../components/datePickerControl';
import { dateDifference, allowSpecialCharacter } from '../../utils/utils';
import MultiSelectDropdown from '../../components/multiSelectDropdown';

export class StateAndDate extends PureComponent {
  startDateChange = (date) => {
    const { values, action, fieldChangeHandler } = this.props;
    const { endDate } = values;
    const { endDateResetAction, displayStartDateResetAction } = action;
    const dateDiff = dateDifference(date, endDate);
    fieldChangeHandler();
    if (dateDiff > 0) {
      endDateResetAction();
      displayStartDateResetAction();
    }
    if (dateDiff < 0) {
      displayStartDateResetAction();
    }
  }

  endDateChange = (date) => {
    const { values, action, fieldChangeHandler } = this.props;
    const { displayStartDate } = values;
    const { displayEndDateResetAction } = action;
    const dateDiff = dateDifference(date, displayStartDate);
    fieldChangeHandler();
    if (dateDiff > 0) {
      displayEndDateResetAction();
    }
  }

  preventKeyPress = (event) => {
    event.preventDefault();
  }

  render() {
    const {
      TITLE, STATES, START_DATE, END_DATE, DISPLAY_START_DATE, DISPLAY_END_DATE, ACTIVE_STATUS, APPROVE_STATUS, PUBLISHED_STATUS, INACTIVATED_STATUS, EXPIRED_STATUS,
    } = Texts;
    const {
      values, states, disabled, fieldChangeHandler,
    } = this.props;
    const { startDate, endDate, displayStartDate } = values;
    const activeStatus = (disabled === APPROVE_STATUS);
    const activePublishedStatus = ((disabled === ACTIVE_STATUS) || (disabled === PUBLISHED_STATUS));
    const expiredInactive = ((disabled === INACTIVATED_STATUS) || (disabled === EXPIRED_STATUS));
    const isTitleDisabled = activePublishedStatus || expiredInactive || activeStatus;
    const isStartDateDisabled = activePublishedStatus || expiredInactive || activeStatus;
    const isEndDateDisabled = !startDate || expiredInactive;
    const isStateDisabled = activePublishedStatus || expiredInactive || activeStatus;
    const isDisplayStartDateDisabled = !endDate || activePublishedStatus || expiredInactive || activeStatus;
    const isDisaplyEndDateDisabled = !displayStartDate || expiredInactive;
    return (
      <React.Fragment>
        <section>
          <div className="gaf-global-box gaf-create-new-form">
            <Row className="show-grid">
              <Col xs={12}>
                <div className="gaf-globle-main clearfix">
                  <div className="gaf-globle-box gaf-create-new-form">
                    <Row>
                      <Col md={4} sm={12} xs={12}>
                        <Field
                          className="form-group gaf-form gaf-form-inner clearfix title-input"
                          name="title"
                          id="title"
                          type="text"
                          component={TextInput}
                          label={`${TITLE}<sup>*</sup>`}
                          maxLength={100}
                          readOnly={isTitleDisabled}
                          onChange={() => fieldChangeHandler()}
                          normalize={allowSpecialCharacter}
                        />
                      </Col>
                      <Col md={4} sm={6} xs={6} className="pad-40">
                        <Field
                          name="startDate"
                          className="pull-right"
                          label={`${START_DATE}<sup>*</sup>`}
                          id={START_DATE}
                          component={DatePickerControl}
                          onKeyDown={this.preventKeyPress}
                          min={moment().subtract(30, 'days')}
                          onChange={(date) => this.startDateChange(date)}
                          disabled={isStartDateDisabled}
                        />
                      </Col>
                      <Col md={4} sm={6} xs={6}>
                        <Field
                          name="endDate"
                          className="pull-right"
                          label={`${END_DATE}<sup>*</sup>`}
                          id={END_DATE}
                          component={DatePickerControl}
                          onKeyDown={this.preventKeyPress}
                          onChange={(date) => this.endDateChange(date)}
                          min={moment(startDate)}
                          disabled={isEndDateDisabled}
                        />
                      </Col>
                    </Row>
                    <Row className="mt-30">
                      <Col md={4} sm={12} xs={12}>
                        <div className="gaf-form clearfix">
                          <label>{STATES}<sup>*</sup></label>
                          <div className="gaf-form-inner state-dropdown">
                            <Field
                              placeholder="Select"
                              menuItems={states}
                              name="states"
                              component={MultiSelectDropdown}
                              readOnly={isStateDisabled}
                              onChangeHandler={() => fieldChangeHandler()}
                            />
                          </div>
                        </div>
                      </Col>
                      <Col md={4} sm={6} xs={6} className="pad-40 display-start-date">
                        <Field
                          name="displayStartDate"
                          className="pull-right"
                          label={`${DISPLAY_START_DATE}<sup>*</sup>`}
                          id={DISPLAY_START_DATE}
                          component={DatePickerControl}
                          onKeyDown={this.preventKeyPress}
                          max={moment(startDate)}
                          min={moment(startDate).subtract(30, 'days')}
                          disabled={isDisplayStartDateDisabled}
                          onChange={() => fieldChangeHandler()}
                        />
                      </Col>
                      <Col md={4} sm={6} xs={6} className="display-end-date">
                        <Field
                          name="displayEndDate"
                          className="pull-right"
                          label={`${DISPLAY_END_DATE}<sup>*</sup>`}
                          id={DISPLAY_END_DATE}
                          component={DatePickerControl}
                          onKeyDown={this.preventKeyPress}
                          min={moment(endDate)}
                          disabled={isDisaplyEndDateDisabled}
                          onChange={() => fieldChangeHandler()}
                        />
                      </Col>
                    </Row>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
        </section>
      </React.Fragment>
    );
  }
}
StateAndDate.propTypes = {
  values: PropTypes.object,
  action: PropTypes.object,
  states: PropTypes.array,
  disabled: PropTypes.string,
  fieldChangeHandler: PropTypes.func,
};

export default StateAndDate;
