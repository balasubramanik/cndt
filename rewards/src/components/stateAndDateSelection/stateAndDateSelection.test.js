import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import StateAndDate from '../../components/stateAndDateSelection';

configure({ adapter: new Adapter() });
describe('StateAndDate', () => {
  const fieldChangeHandler = jest.fn();
  const endDateResetAction = jest.fn();
  const displayStartDateResetAction = jest.fn();
  const displayEndDateResetAction = jest.fn();
  const preventDefault = jest.fn();

  const tree = shallow(<StateAndDate
    values={{
      startDate: '01/04/2019',
      displayStartDate: '12/05/2018',
      endDate: '01/03/2019',
    }}
    states={[{ title: 'Alabama', value: 'AL', checked: true }, { title: 'Alaska', value: 'AK', checked: true }]}
    disabled="APPROVE"
    action={{
      endDateResetAction,
      displayStartDateResetAction,
      displayEndDateResetAction,
    }}
    fieldChangeHandler={fieldChangeHandler}
  />);

  it('should render input for title field', () => {
    const simulateClick = tree.find('Field').at(0).prop('onChange');
    simulateClick();
    expect(fieldChangeHandler).toBeCalled();
  });


  it('should render datepicker for start date change', () => {
    const simulateClick = tree.find('Field').at(1).prop('onChange');
    simulateClick('01/04/2019');
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render datepicker for start date change', () => {
    const simulateClick = tree.find('Field').at(1).prop('onChange');
    simulateClick('01/02/2019');
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render datepicker onkeyDown prevent', () => {
    const simulateClick = tree.find('Field').at(1).prop('onKeyDown');
    simulateClick({ preventDefault });
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render datepicker end date change', () => {
    const simulateClick = tree.find('Field').at(2).prop('onChange');
    simulateClick('01/02/2019');
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render multiselect dropdown for states', () => {
    const simulateClick = tree.find('Field').at(3).prop('onChangeHandler');
    simulateClick();
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render datepicker for display start date change', () => {
    const simulateClick = tree.find('Field').at(4).prop('onChange');
    simulateClick();
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render datepicker for display end date change', () => {
    const simulateClick = tree.find('Field').at(5).prop('onChange');
    simulateClick();
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should be defined', () => {
    expect(StateAndDate).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render title input', () => {
    expect(tree.find('.title-input').length).toBe(1);
  });

  it('should render state Dropdown', () => {
    expect(tree.find('.state-dropdown').length).toBe(1);
  });
});
