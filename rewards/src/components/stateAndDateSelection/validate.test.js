import { validate } from './validate';

describe('validate', () => {
  it('title should be come', () => {
    const errors = validate({});
    expect(errors.title).toBe('This field is required');
  });
  it('states should be come', () => {
    const errors = validate({ states: [{ title: 'Alabama', value: 'AL', checked: true }, { title: 'Alaska', value: 'AK', checked: true }], itemSelected: [{ title: 'Alaska', value: 'AK', checked: true }] });
    expect(errors.states).toBe('This field is required');
  });
  it('states lenth should be 0', () => {
    const errors = validate({ states: [] });
    expect(errors.states).toBe('This field is required');
  });
  it('endDate should be come', () => {
    const errors = validate({ startDate: '22/01/2018' });
    expect(errors.endDate).toBe('This field is required');
  });
  it('displayStartDate should be come', () => {
    const errors = validate({ endDate: '22/01/2018' });
    expect(errors.displayStartDate).toBe('This field is required');
  });
  it('displayEndDate should be come', () => {
    const errors = validate({ displayStartDate: '22/01/2018' });
    expect(errors.displayEndDate).toBe('This field is required');
  });
});
