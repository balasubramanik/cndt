const Texts = {
  TITLE: 'Title',
  STATES: 'States',
  START_DATE: 'Start Date',
  END_DATE: 'End Date',
  DISPLAY_START_DATE: 'Display Start Date',
  DISPLAY_END_DATE: 'Display End Date',
  ACTIVE_STATUS: 'ACTIVE',
  PUBLISHED_STATUS: 'PUBLISHED',
  INACTIVATED_STATUS: 'INACTIVATED',
  EXPIRED_STATUS: 'EXPIRED',
  APPROVE_STATUS: 'APPROVED',
};
export { Texts };
