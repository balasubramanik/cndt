import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Footer from '../footer';


configure({ adapter: new Adapter() });

describe('Footer', () => {
  const tree = shallow(<Footer />);

  it('should be defined', () => {
    expect(Footer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
