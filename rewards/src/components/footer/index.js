import React from 'react';
import { Grid, Col, Row } from 'react-bootstrap';
import envConfig from 'envConfig';//eslint-disable-line
import CopyRight from '../../components/copyRight';
import { footer } from './constants';

const Footer = () => (
  <footer id="gaf-footer-wrapper">
    <Grid>
      <Row className="show-grid">
        <Col xs={12}>
          <CopyRight />
          <ul id="gaf-footer-link">
            <li id="gaf-footer-termsofuse"><a id="termsofuse" className="footerlink" href={footer.GAFTERMSLINK} target="_blank" rel="noopener noreferrer">{footer.TERMSOFUSE}</a></li>
            <li id="gaf-footer-privacy-policy"><a id="privacy-policy" className="footerlink" href={footer.GAFPOLICYLINK} target="_blank" rel="noopener noreferrer">{footer.PRIVACYPOLICY}</a></li>
            <li id="gaf-footer-termsandcondition"><a id="termsandcondition" className="footerlink" href={envConfig.termsAndConditionUrl} target="_blank" rel="noopener noreferrer">{footer.TERMSANDCONDITION}</a></li>
            <li id="gaf-footer-allrightres" className="footerlink" rel="noopener noreferrer">{footer.ALLRIGHTRES}</li>
          </ul>
        </Col>
      </Row>
    </Grid>
  </footer>
);

export default Footer;
