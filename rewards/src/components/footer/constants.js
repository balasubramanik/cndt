const footer = {
  TERMSOFUSE: 'Terms of Use',
  PRIVACYPOLICY: 'Privacy Policy',
  TERMSANDCONDITION: 'Terms and Conditions',
  ALLRIGHTRES: 'All Rights Reserved.',
  GAFTERMSLINK: 'https://www.gaf.com/en-us/our-company/privacy-and-legal/terms-of-use',
  GAFPOLICYLINK: 'https://www.gaf.com/en-us/our-company/privacy-and-legal/privacy-policy',
};

export { footer };
