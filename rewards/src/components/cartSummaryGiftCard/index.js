import React from 'react';
import PropTypes from 'prop-types';
import { cartSummaryListtext } from './constants';
import Dropdown from '../../components/dropdown';
import ToolTip from '../toolTip';
import ImageHOC from '../imageHOC';
import { roundToTwoDecimal, removeSpecialChar, formatUSCurrency, formatCurrency } from '../../utils/utils';

class CartSummaryGiftCard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.amount = {};
    this.variantId = {};
  }

  componentDidMount() {
    const { action, amount } = this.props;
    const { cardType, productId } = this.props.item;
    this.cardType = removeSpecialChar(cardType).toLowerCase();
    if (productId && Object.keys(amount).length === 0) {
      action.getGiftCardDetails(productId);
    }
  }

  componentDidUpdate() {
    const { cardDetails, amount } = this.props;
    const { productId } = this.props.item;
    if (cardDetails[productId] && cardDetails[productId].variants) {
      if (Object.keys(amount).length === 0) {
        this.groupCardsByType(cardDetails[productId].variants);
      }
    }
  }


  /** @description Function to update card information based on product
  * @param {string} type
  * @param {array} arr
  * @param {string} cardValue
  */
  setCardInfo = (type, cardValue, variantId) => {
    if (this.amount) {
      if (this.amount[type]) {
        const index = this.amount[type].findIndex((amt) => amt.value === cardValue);
        if (index === -1) {
          this.amount[type].push(cardValue);
        }
      } else {
        this.amount[type] = [cardValue];
      }
      this.variantId[type] = {
        ...this.variantId[type],
        [`${cardValue}`]: variantId,
      };
    }
  }
  /** @description Function to group the variants based on card type.
*/

  groupCardsByType = (variantList) => {
    this.amount = {};
    const { productId } = this.props.item;
    variantList.forEach((variant) => {
      const { variants, variantId } = variant;
      const { cardType, giftCardValue } = variants;
      if (variants) {
        this.imageUrl = variants.variant_Images;
        const isPhysicalCard = removeSpecialChar(cardType).toLowerCase() === cartSummaryListtext.physicalCard;
        const type = isPhysicalCard ? cartSummaryListtext.physicalCard : cartSummaryListtext.eCard;
        this.setCardInfo(type, giftCardValue, variantId);
      }
    });

    this.props.selectedCardAmount({
      productId,
      variantId: this.variantId,
      amount: this.amount,
    });
  }

  formatAmountData = () => {
    const { amount } = this.props;
    if (amount[this.cardType] && amount[this.cardType].length > 0) {
      return amount[this.cardType].map((value) => {
        const label = value ? formatUSCurrency(value.toString()) : '';
        return {
          label,
          value,
        };
      });
    }
    return null;
  }

  handleDelete = (e) => {
    e.preventDefault();
    const { productName } = this.props.item;
    this.props.onDelete(this.props.index, productName);
  }

  renderDeleteCartItem() {
    return (
      <React.Fragment>
        <div className="card-delete">
          <a id={`gift-card-delete-${this.props.index}`} href="/" onClick={this.handleDelete}>
            <i id={`gift-card-delete-icon-${this.props.index}`} className="icon-delete">
            </i>
          </a>
        </div>
      </React.Fragment>
    );
  }

  renderFeeDetails() {
    const {
      cardValue,
      quantity,
      feeRate,
      feeTypeDesc = '',
    } = this.props.item;
    const value = roundToTwoDecimal(Number(cardValue) * Number(quantity));
    const TotalFee = roundToTwoDecimal(quantity * feeRate);
    return (
      <React.Fragment>
        <div className="card-points-fees">
          <p>
            <strong id={`gift-fee-points-title-${this.props.index}`}>{cartSummaryListtext.POINTS}</strong>
            <strong id={`gift-fee-value-${this.props.index}`}> {formatCurrency(value)} </strong>
          </p>
          {feeRate ?
            <span id={`gift-card-fee-details-${this.props.index}`}>  {cartSummaryListtext.FEE}: {TotalFee}
              {feeTypeDesc &&
                <ToolTip
                  id={`gift-card-fee-tooltip-${this.props.index}`}
                  text={feeTypeDesc}
                >
                  <i
                    id={`gift-card-icon-question-${this.props.index}`}
                    className="icon-question"
                    role="button"
                  >
                  </i>
                </ToolTip>
              }
            </span>
            : ''}
        </div>
      </React.Fragment>
    );
  }

  renderCardDetails() {
    const {
      cardValue,
      quantity,
      feeRate,
      max,
      productName = '',
      cardTypeDesc = '',
    } = this.props.item;

    const { index, cardVariant } = this.props;
    const unitValue = Number(cardValue);
    const amount = this.formatAmountData();
    return (
      <React.Fragment>
        <div className="card-description">
          <label id={`gift-product-name-${this.props.index}`}>{productName}</label>
          <p id={`gift-descrption-${this.props.index}`}>{cardTypeDesc}</p>
        </div>
        <div className="card-qty-value">
          <div className="card-value-qty clearfix">
            <div className="filterby">
              <div className="form-group gaf-form">
                <label id={`gift-card-value-${this.props.index}`}>{cartSummaryListtext.CARD_VALUE}</label>
                {amount &&
                  <Dropdown
                    id={`gaf-input-${index}`}
                    value={unitValue}
                    menuItems={amount}
                    onChangeHandler={(val) => this.props.onItemSelectHandler({
                      qty: quantity,
                      selectedVariantId: (cardVariant && cardVariant[this.cardType]) ? cardVariant[this.cardType][`${val}`] : null,
                      val,
                      index,
                      unitValue,
                    })
                    }
                  />}
              </div>
            </div>
            <div className="form-group gaf-form qty-box">
              <label htmlFor="quantity" id={`gift-quantity-value-${this.props.index}`}>{cartSummaryListtext.QUANTITY}</label>
              <div className="form-control">
                <button
                  id={`gift-card-quantity-decrement-${this.props.index}`}
                  className={`control-quantity decrement ${Number(quantity) === 1 ? cartSummaryListtext.GREY_OUT : ''}`}
                  onClick={() => this.props.onhandleQuantityInput({
                    action: cartSummaryListtext.DECREMENT,
                    qty: quantity,
                    index,
                    unitValue,
                    feeRate,
                  })}
                >
                  -
                </button>
                <button
                  id={`gift-card-quantity-increment-${this.props.index}`}
                  className={`control-quantity increment ${Number(quantity) === Number(max) ? cartSummaryListtext.GREY_OUT : ''}`}
                  onClick={() => this.props.onhandleQuantityInput({
                    action: cartSummaryListtext.INCREMENT,
                    qty: quantity,
                    index,
                    unitValue,
                    feeRate,
                  })}
                >
                  +
                </button>
                {quantity}
              </div>
            </div>
            {
              Number(quantity) === Number(max) ?
                <div className="gaf-error-message">
                  {cartSummaryListtext.MAXIMUM_QTY}
                </div>
                : null
            }
          </div>
        </div>
      </React.Fragment>
    );
  }

  render() {
    const {
      img = '',
      productName = '',
      isAvailable,
      category,
    } = this.props.item;

    return (
      <li>
        <div className="product-icon">
          <figure>
            <ImageHOC
              id={`gift-card-img-${this.props.index}`}
              isAbsolute={img}
              src={img}
              alt={productName}
            />
          </figure>
        </div>

        <div className="produtct-details" id="cart-gift-card-list">
          {this.renderCardDetails()}
          {this.renderFeeDetails()}
          {this.renderDeleteCartItem()}
        </div>
        {category === cartSummaryListtext.GIFT_CARD && !isAvailable ?
          <div className="gaf-error-message">
            {cartSummaryListtext.NOT_AVAILABLE}
          </div>
          : ''}
      </li>
    );
  }
}

CartSummaryGiftCard.propTypes = {
  index: PropTypes.number,
  item: PropTypes.object,
  productId: PropTypes.string,
  onDelete: PropTypes.func,
  amount: PropTypes.object,
  cardVariant: PropTypes.object,
  selectedCardAmount: PropTypes.func,
  action: PropTypes.object,
  onItemSelectHandler: PropTypes.func,
  onhandleQuantityInput: PropTypes.func,
  cardDetails: PropTypes.object,

};

export default CartSummaryGiftCard;
