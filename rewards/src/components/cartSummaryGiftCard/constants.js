const cartSummaryListtext = {
  CARD_VALUE: 'Card Value',
  QUANTITY: 'Quantity',
  POINTS: 'Points',
  FEE: 'Fee',
  ALERT_MESSAGE: 'You have reached the maximum quantity',
  physicalCard: 'physicalcard',
  eCard: 'e-card',
  GIFT_CARD: 'giftcards',
  INCREMENT: 'increment',
  DECREMENT: 'decrement',
  GREY_OUT: 'grey',
  MAXIMUM_QTY: 'You have reached the maximum quantity.',
  NOT_AVAILABLE: 'Sorry, this product is no longer available.',
};
export { cartSummaryListtext };
