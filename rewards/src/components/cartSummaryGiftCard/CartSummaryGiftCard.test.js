import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryGiftCard from '../../components/cartSummaryGiftCard';

configure({ adapter: new Adapter() });


describe('CartSummaryGiftCard List', () => {
  const listItemSelectHandler = jest.fn();
  const handleQuantityInput = jest.fn();
  const onDelete = jest.fn();
  const onUpdate = jest.fn();
  const tree = shallow(<CartSummaryGiftCard

    listItemSelectHandler={listItemSelectHandler}
    handleQuantityInput={handleQuantityInput}
    i={0}
    tooltip={'tooltipData'}
    unitValue={'unitValue'}
    index={0}
    maxQty={1}
    value={''}
    quantity={1}
    onDelete={onDelete}
    onUpdate={onUpdate}
  />);

  const event = {
    preventDefault: jest.fn(),
    target: {
      value: '1'
    }
  };

  it('should be defined', () => {
    expect(CartSummaryGiftCard).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onChange handler', () => {
    const simulateChange = tree.find('#gaf-input-0').prop('onChange');
    simulateChange({ target: { value: '30' } });
    expect(onUpdate).toBeCalled();
  });

  it('should call onClick increment handler', () => {
    const simulateChange = tree.find('span').find('.decrement').prop('onClick');
    simulateChange(event, 2);
    expect(onUpdate).toBeCalled();
  });


  it('should call onClick decrement handler', () => {
    const simulateChange = tree.find('span').find('.increment').prop('onClick');
    simulateChange(event, 1);
    expect(onUpdate).toBeCalled();
  });
  it('should call onClick decrement handler', () => {
    const simulateChange = tree.find('.card-delete').find('a').prop('onClick');
    simulateChange(event);
    expect(onDelete).toBeCalled();
  });


});


describe('CartSummaryGiftCard List', () => {
  const listItemSelectHandler = jest.fn();
  const handleQuantityInput = jest.fn();
  const onDelete = jest.fn();
  const onUpdate = jest.fn();
  const tree = shallow(<CartSummaryGiftCard

    listItemSelectHandler={listItemSelectHandler}
    handleQuantityInput={handleQuantityInput}
    i={0}
    tooltip={'tooltipData'}
    unitValue={'unitValue'}
    index={0}
    maxQty={1}
    value={''}
    quantity={''}
    onDelete={onDelete}
    onUpdate={onUpdate}
  />);

  const event = {
    preventDefault: jest.fn(),
    target: {
      value: '1'
    }
  };

  it('should be defined', () => {
    expect(CartSummaryGiftCard).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call onChange handler', () => {
    const simulateChange = tree.find('#gaf-input-0').prop('onChange');
    simulateChange({ target: { value: '30' } });
    expect(onUpdate).toBeCalled();
  });

  it('should call onClick increment handler', () => {
    const simulateChange = tree.find('span').find('.decrement').prop('onClick');
    simulateChange(event, 2);
    expect(onUpdate).toBeCalled();
  });


  it('should call onClick decrement handler', () => {
    const simulateChange = tree.find('span').find('.increment').prop('onClick');
    simulateChange(event, 1);
    expect(onUpdate).toBeCalled();
  });
  it('should call onClick decrement handler', () => {
    const simulateChange = tree.find('.card-delete').find('a').prop('onClick');
    simulateChange(event);
    expect(onDelete).toBeCalled();
  });
});


