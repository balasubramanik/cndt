import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ProductSection from '../../components/productSection';

configure({ adapter: new Adapter() });
describe('ProductSection', () => {
  const fieldChangeHandler = jest.fn();
  const onDelete = jest.fn();
  const familyDataReset = jest.fn();
  const uomResetHandler = jest.fn();
  const resetHandler = jest.fn();
  const tree = shallow(<ProductSection
    index={1}
    fieldChangeHandler={fieldChangeHandler}
    onDelete={onDelete}
    name="products[0]"
    details={{
      category: 'Three-Tab Shingles',
      allowanceType: 'DOL',
      uom: 'GL',
      productValue: 10,
      selectedProductFamily: ['Royal Sovereign®', 'Marquis® WeatherMax®'],
      productFamily: {
        isSelectAll: true,
        itemSelected: [['Royal Sovereign®', { title: 'Royal Sovereign®', value: 'Royal Sovereign®', checked: true }],
          ['Marquis® WeatherMax®', { title: 'Marquis® WeatherMax®', value: 'RMarquis® WeatherMax®', checked: false }],
        ],
      },
    }}
    category={[{ title: 'Three-Tab Shingles', value: 'Three-Tab Shingles' }, { title: 'Timberline® Lifetime Shingles', value: 'Timberline® Lifetime Shingles' }]}
    productFamily={[{ title: 'Royal Sovereign®', value: 'Royal Sovereign®', checked: true }, { title: 'Marquis® WeatherMax®', value: 'Marquis® WeatherMax®', checked: true }]}
    allowanceType={[{ title: 'Dollar', value: 'DOL' }, { title: 'Percentage', value: 'PCT' }]}
    uom={[{ title: 'Gallon', value: 'GL' }, { title: 'Dry Lbs', value: 'DL' }]}
    disabled="APPROVED"
    deleteDisabled
    familyDataReset={familyDataReset}
    uomResetHandler={uomResetHandler}
    anyTouch
    syncError={1}
  />);
  it('should be defined', () => {
    expect(ProductSection).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render product section input', () => {
    expect(tree.find('#Value').length).toBe(1);
  });
  it('should render dropdown for family data reset', () => {
    const simulateClick = tree.find('Field').at(3).prop('onChangeHandler');
    simulateClick('Three-Tab Shingles', familyDataReset);
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render input for field change handler', () => {
    const simulateClick = tree.find('Field').at(4).prop('onChange');
    simulateClick();
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render input for field change handler', () => {
    const simulateClick = tree.find('Field').at(3).prop('onChangeHandler');
    simulateClick('Three-Tab Shingles', resetHandler, 'allowanceType');
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render input for field change handler', () => {
    const simulateClick = tree.find('Field').at(2).prop('onChangeHandler');
    simulateClick('DOL');
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render input for field change handler', () => {
    const simulateClick = tree.find('Field').at(2).prop('onChangeHandler');
    simulateClick('PCT');
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render input for field change handler', () => {
    const simulateClick = tree.find('Field').at(2).prop('onChangeHandler');
    simulateClick();
    expect(fieldChangeHandler).toBeCalled();
  });

  it('should render product section Dropdown', () => {
    expect(tree.find('.spotPromotionDropdown').length).toBe(4);
  });

  it('should call component did update for dollar allowance type', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      details: {
        allowanceType: 'DOL',
      },
    });
    expect(tree.state().type).toBe('$');
  });
  it('should call component did update for percentage allowance type', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      details: {
        allowanceType: 'PCT',
      },
    });
    expect(tree.state().type).toBe('%');
  });
  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      details: {
        allowanceType: '',
      },
    });
    expect(tree.state().type).toBe('');
  });
});
describe('ProductSection render based on condition', () => {
  const fieldChangeHandler = jest.fn();
  const familyDataReset = jest.fn();
  const uomResetHandler = jest.fn();
  const tree = shallow(<ProductSection
    index={0}
    fieldChangeHandler={fieldChangeHandler}
    details={{
      category: 'Three-Tab Shingles',
      allowanceType: 'DOL',
      uom: 'GL',
      productValue: 10,
      selectedProductFamily: ['Royal Sovereign®', 'Marquis® WeatherMax®'],
      productFamily: ['Royal Sovereign®', 'Marquis® WeatherMax®'],
    }}
    familyDataReset={familyDataReset}
    uomResetHandler={uomResetHandler}
    anyTouch
    syncError={{ category: 'Three-Tab Shingles' }}
  />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('ProductSection render based productFamily', () => {
  const tree = shallow(<ProductSection
    name="products[0]"
    details={{
      category: 'Three-Tab Shingles',
      allowanceType: 'DOL',
      uom: 'GL',
      productValue: 10,
    }}
    anyTouch
    syncError={{ category: 'Three-Tab Shingles' }}
  />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
