import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Row, Col } from 'react-bootstrap';
import { Field } from 'redux-form';
import { Texts } from './constant';
import Dropdown from '../dropdown';
import TextInput from '../textInput';
import MultiSelectDropdown from '../../components/multiSelectDropdown';
import { formatValue } from '../../utils/utils';

export class ProductSection extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      type: '',
    };
  }

  componentDidUpdate = (prevProps) => {
    const { details } = prevProps;
    const { allowanceType } = details;
    const { dollar, percentage } = Texts;
    if (allowanceType === dollar) {
      this.stateUpdatehandler('$');
    } else if (allowanceType === percentage) {
      this.stateUpdatehandler('%');
    }
    if (!details.allowanceType) {
      this.stateUpdatehandler('');
    }
  }

  stateUpdatehandler = (type) => {
    this.setState({ type });
  }

  deleteHandler = (e) => {
    e.preventDefault();
    const { onDelete, fieldChangeHandler } = this.props;
    onDelete(e);
    fieldChangeHandler();
  }

  familyCheckHandler = () => {
    const { details } = this.props;
    let isFamilyCheck = false;
    const { productFamily = [] } = details;
    if (productFamily && productFamily.itemSelected) {
      productFamily.itemSelected.forEach((data) => {
        if (data[1] && data[1].checked) {
          isFamilyCheck = true;
        }
      });
    } else if (productFamily) {
      productFamily.forEach((data) => {
        if (data) {
          isFamilyCheck = true;
        }
      });
    }
    return isFamilyCheck;
  }

  productFamilyReset = (value, resetHandler, type) => {
    const { dollar, percentage } = Texts;
    if (resetHandler) {
      resetHandler(value);
      this.props.fieldChangeHandler();
    } else {
      this.props.fieldChangeHandler();
    }
    if (type === 'allowanceType') {
      if (value === dollar) {
        this.setState({ type: '$' });
      } else if (value === percentage) {
        this.setState({ type: '%' });
      }
    }
  }

  renderDropdown = (label, menuItems, component, key, className, isdisabled, resetHandler, isreadOnly) => {
    const { name } = this.props;
    return (
      <div className={`form-group gaf-form spotPromotionDropdown ${className}`}>
        <label htmlFor="exampleCategory">{label}<sup>*</sup> </label>
        <Field
          id="gaf-input"
          placeholder="Select"
          menuItems={menuItems}
          component={component}
          name={`${name}.${key}`}
          readOnly={isdisabled}
          isDisabled={isreadOnly}
          onChangeHandler={(value) => this.productFamilyReset(value, resetHandler, key)}
        />
      </div>
    );
  }

  render() {
    const {
      index, name, details, category, productFamily, allowanceType, uom, disabled, deleteDisabled, fieldChangeHandler, familyDataReset, uomResetHandler, fieldResetHandler, syncError, anyTouch,
    } = this.props;
    const isFamilyDisabled = this.familyCheckHandler();
    const {
      categoryText, productFamilyText, allowanceTypeText, uomText, approveText,
    } = Texts;
    const { type } = this.state;
    const isUomDisabled = ((!details.allowanceType || details.allowanceType.length === 0) || details.allowanceType === 'PCT');
    const isProductFamilyDisabled = ((!productFamily) || (productFamily.length === 0));
    const isProductValueDisabled = ((!details.allowanceType) || (details.allowanceType.length === 0));
    const approveStatus = (disabled === approveText);
    const activePublishedStatus = ((disabled === Texts.activeStatus) || (disabled === Texts.publishedStatus));
    const expiredInactivated = ((disabled === Texts.inactivatedStatus) || (disabled === Texts.expiredStatus));
    const categoryDisabled = activePublishedStatus || expiredInactivated || approveStatus;
    const productFamilyDisabled = isProductFamilyDisabled || activePublishedStatus || expiredInactivated || approveStatus;
    const allowanceTypeDisabled = !isFamilyDisabled || activePublishedStatus || expiredInactivated || approveStatus;
    const uomDisabled = isUomDisabled || activePublishedStatus || expiredInactivated || approveStatus;
    const productValueDisabled = isProductValueDisabled || activePublishedStatus || expiredInactivated || approveStatus;
    const valueClassName = classNames({ 'form-group gaf-form dollar': type === '$', 'form-group gaf-form percentage': type === '%' });
    const productSectionClass = (index > 0) ? 'gaf-global-box spot-promotion-edit gaf-bg-color' : 'gaf-global-box gaf-bg-color';
    const productClass = (Object.keys(syncError).length > 0 && anyTouch) ? 'spot-error' : '';
    return (
      <div className={productSectionClass}>
        <Row>
          <Col xs={12} className={productClass}>
            <div className="form-inner create-spot-promotion-addform">
              {category && category.length > 0 && this.renderDropdown(categoryText, category, Dropdown, 'category', 'create-category', categoryDisabled, familyDataReset)}
              {this.renderDropdown(productFamilyText, productFamily, MultiSelectDropdown, 'productFamily', 'product-famliy', productFamilyDisabled, fieldResetHandler, isProductFamilyDisabled)}
              {allowanceType && allowanceType.length > 0 && this.renderDropdown(allowanceTypeText, allowanceType, Dropdown, 'allowanceType', 'allowance-type', allowanceTypeDisabled, uomResetHandler)}
              {uom && uom.length > 0 && this.renderDropdown(uomText, uom, Dropdown, 'uom', 'uom', uomDisabled)}
              <div className="form-group spot-promoton-value gaf-form">
                <Field
                  id={Texts.value}
                  name={`${name}.productValue`}
                  className={valueClassName}
                  component={TextInput}
                  label={`${Texts.value}<sup>*</sup>`}
                  type="text"
                  readOnly={productValueDisabled}
                  normalize={formatValue}
                  onChange={() => fieldChangeHandler()}
                />
                {index > 0 && deleteDisabled && <a href="/" onClick={this.deleteHandler} className="delete-catogory"><i className="icon-delete"></i></a>}
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

ProductSection.propTypes = {
  index: PropTypes.number,
  onDelete: PropTypes.func,
  name: PropTypes.string,
  details: PropTypes.object,
  category: PropTypes.array,
  productFamily: PropTypes.array,
  allowanceType: PropTypes.array,
  uom: PropTypes.array,
  disabled: PropTypes.string,
  deleteDisabled: PropTypes.bool,
  fieldChangeHandler: PropTypes.func,
  familyDataReset: PropTypes.func,
  uomResetHandler: PropTypes.func,
  fieldResetHandler: PropTypes.func,
  anyTouch: PropTypes.bool,
  syncError: PropTypes.object,
};

export default ProductSection;
