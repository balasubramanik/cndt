const Texts = {
  categoryText: 'Category',
  productFamilyText: 'Product Family',
  allowanceTypeText: 'Allowance Type',
  uomText: 'UOM',
  value: 'Value',
  activeStatus: 'ACTIVE',
  publishedStatus: 'PUBLISHED',
  inactivatedStatus: 'INACTIVATED',
  expiredStatus: 'EXPIRED',
  approveText: 'APPROVED',
  categoryPlaceHolder: 'Select Category',
  productFamilyPlaceHolder: 'Select Product Family',
  allowanceTypePlaceHolder: 'Select Allowance type',
  uomPlaceHolder: 'Select UOM',
  dollar: 'DOL',
  percentage: 'PCT',
};
export { Texts };
