import { texts } from '../../containers/SpotPromotionManagement/constant';

const validate = (values) => {
  const { REQUIRED_FIELD } = texts;
  const { products } = values;
  const errors = {};
  const productsError = [];
  if (products && products.length > 0) {
    products.forEach((product, index) => {
      let selectedFamily = [];
      let productsErrors = {};
      if (!products[index].category) {
        productsErrors = { ...productsErrors, category: REQUIRED_FIELD };
      }
      if (products[index].category && products[index].productFamily) {
        const { productFamily } = products[index];
        if (productFamily && productFamily.itemSelected) {
          selectedFamily = productFamily.itemSelected.filter((item) => item[1].checked);
          if (selectedFamily.length === 0) {
            productsErrors = { ...productsErrors, productFamily: REQUIRED_FIELD };
          }
        }
      }
      if (selectedFamily.length > 0 && !products[index].allowanceType) {
        productsErrors = { ...productsErrors, allowanceType: REQUIRED_FIELD };
      }
      if (products[index].allowanceType && !products[index].uom) {
        if (products[index].allowanceType === 'DOL') {
          productsErrors = { ...productsErrors, uom: REQUIRED_FIELD };
        }
      }
      if (!products[index].productValue && products[index].productValue !== 0) {
        if (products[index].allowanceType === 'DOL' && products[index].uom) {
          productsErrors = { ...productsErrors, productValue: REQUIRED_FIELD };
        } else if (products[index].allowanceType === 'PCT') {
          productsErrors = { ...productsErrors, productValue: REQUIRED_FIELD };
        }
      }
      productsError.push(productsErrors);
    });
  }
  errors.products = productsError;
  return errors;
};
export { validate };
