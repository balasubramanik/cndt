import test from 'tape';
import { getAchInfo, getProductdetails, getVariantId, getVariants, getDescription, getFeeDetails } from './selectors';

const t = test('test utils', (b) => b);

describe('Ach Selectors', () => {
  it('should return Ach info', () => {
    const mock = {
      redeemPointsState: {
        achInfo: {
          listprice: '0.000',
          cardType: 'Physical Card',
          feeType: '%',
          feeValue: '2.5',
          giftCardValue: null,
          imageicon: null,
          max: '99,999.00',
          min: '25.00',
          minmaxtype: '$',
          Variant_Images: null,
          DisplayName: 'VISA Reloadble Debit Card',
        },
      },
    };
    t.deepEqual(mock.redeemPointsState.achInfo, getAchInfo(mock));
  });

  it('should return product info with data', () => {
    const mock = {
      redeemPointsState: {
        achInfo: [{
          categories: [{
            products: [{
              listprice: '0.000',
              cardType: 'Physical Card',
              feeType: '%',
              feeValue: '2.5',
              giftCardValue: null,
              imageicon: null,
              max: '99,999.00',
              min: '25.00',
              minmaxtype: '$',
              Variant_Images: null,
              DisplayName: 'VISA Reloadble Debit Card',
            }],
          }],
        }],
      },
    };
    const variant = mock.redeemPointsState.achInfo[0].categories[0].products[0];
    t.deepEqual(variant, getProductdetails(mock));
  });

  it('should return debit card variant info with data', () => {
    const mock = {
      redeemPointsState: {
        achInfo: [{
          categories: [{
            products: [{
              variants: [{
                variants: {
                  listprice: '0.000',
                  cardType: 'Physical Card',
                  feeType: '%',
                  feeValue: '2.5',
                  giftCardValue: null,
                  imageicon: null,
                  max: '99,999.00',
                  min: '25.00',
                  minmaxtype: '$',
                  Variant_Images: null,
                  DisplayName: 'VISA Reloadble Debit Card',
                },
              }],
            }],
          }],
        }],
      },
    };
    const variant = mock.redeemPointsState.achInfo[0].categories[0].products[0].variants[0];
    t.deepEqual(variant, getVariants(mock));
  });

  it('should return debit card variant id', () => {
    const mock = {
      redeemPointsState: {
        achInfo: [{
          categories: [{
            products: [{
              variants: [{
                variants: {
                  variantId: '123',
                  listprice: '0.000',
                  cardType: 'Physical Card',
                  feeType: '%',
                  feeValue: '2.5',
                  giftCardValue: null,
                  imageicon: null,
                  max: '99,999.00',
                  min: '25.00',
                  minmaxtype: '$',
                  Variant_Images: null,
                  DisplayName: 'VISA Reloadble Debit Card',
                },
              }],
            }],
          }],
        }],
      },
    };
    const variantId = mock.redeemPointsState.achInfo[0].categories[0].products[0].variants[0].variants.id;
    t.deepEqual(variantId, getVariantId(mock));
  });

  it('should return ach fee info with data', () => {
    const mock = {
      redeemPointsState: {
        achInfo: [{
          categories: [{
            products: [{
              variants: [{
                variants: {
                  listprice: '0.000',
                  cardType: 'Physical Card',
                  feeType: '%',
                  feeValue: '2.5',
                  giftCardValue: null,
                  imageicon: null,
                  max: '99,999.00',
                  min: '25.00',
                  minmaxtype: '$',
                  Variant_Images: null,
                  DisplayName: 'VISA Reloadble Debit Card',
                },
              }],
            }],
          }],
        }],
      },
    };
    const variant = mock.redeemPointsState.achInfo[0].categories[0].products[0].variants[0].variants;
    t.deepEqual(variant, getFeeDetails(mock));
  });

  it('should return ach fee info without data', () => {
    const mock = {
      redeemPointsState: {
        achInfo: [{
          categories: [{
            products: [{
              variants: [{
                variants: {},
              }],
            }],
          }],
        }],
      },
    };
    t.deepEqual({}, getFeeDetails(mock));
  });

  it('should return description with data', () => {
    const mock = {
      redeemPointsState: {
        achInfo: [{
          categories: [{
            products: [{
              productSpecification: {
                description: 'sample',
              },
            }],
          }],
        }],
      },
    };
    const description = mock.redeemPointsState.achInfo[0].categories[0].products[0].productSpecification;
    t.deepEqual(description, getDescription(mock));
  });

  it('should return description without data', () => {
    const mock = {
      redeemPointsState: {
        achInfo: null,
      },
    };
    t.deepEqual({}, getDescription(mock));
  });
});
