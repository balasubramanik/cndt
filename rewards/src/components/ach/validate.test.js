import { validate } from './validate';
import { constants } from './constants';

describe('validate', () => {
  it('should be equal to invalid achAmount', () => {
    const errors = validate({});
    expect(errors.ach).toBe(true);
  });

  it('should be equal to invalid min achAmount', () => {
    const errors = validate({ ach: '50' }, { feeDetails: { min: 100, max: 500 } });
    expect(errors.ach).toBe(constants.MIN_ERROR.replace('{{amount}}', '$100'));
  });

  it('should be equal to invalid max achAmount', () => {
    const errors = validate({ ach: '550' }, { feeDetails: { min: 100, max: 500 } });
    expect(errors.ach).toBe(constants.MAX_ERROR.replace('{{amount}}', '$500'));
  });

  it('should be equal to valid achAmount', () => {
    const errors = validate({ ach: '500' }, { feeDetails: { min: 100, max: 1000 }, availableBalance: '1000' });
    expect(errors.ach).toBe(undefined);
  });
});
