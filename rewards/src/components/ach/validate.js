import { constants } from './constants';
import { convertStringToNumber, formatUSCurrency } from '../../utils/utils';

const validate = (fields, props) => {
  const errors = {};
  const amount = convertStringToNumber(fields.ach);
  if (!fields.ach) {
    errors.ach = true;
  }

  if (fields.ach) {
    const { feeDetails } = props;
    const { max, min } = feeDetails;
    const maxAch = Number(max);
    const minAch = Number(min);
    if (minAch > amount) {
      errors.ach = constants.MIN_ERROR.replace('{{amount}}', formatUSCurrency(min));
    }
    if (maxAch < amount) {
      errors.ach = constants.MAX_ERROR.replace('{{amount}}', formatUSCurrency(max));
    }
  }

  return errors;
};

export { validate };
