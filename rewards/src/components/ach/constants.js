const constants = {
  MIN_ERROR: 'You have entered the less than {{amount}} amount.',
  MAX_ERROR: 'Sorry, you can only redeem {{amount}} at a time.',
  ADD_TO_CART: 'Add to cart',
  VIEW_MORE: 'View More',
  VIEW_LESS: 'View Less',
  ACH: 'ACH',
  ACH_INFO: 'achInfo',
  TYPE: 'check',
  DEFAULT_ACH_IMAGE: 'images/checkleaf.jpg',
  PRODUCT_DETAILS: 'Product Details',
  TERMS_AND_CONDITIONS: 'Terms and Conditions',
};

export { constants };
