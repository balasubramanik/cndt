
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import ImageHOC from '../../components/imageHOC';
import TextInput from '../textInput';
import CardDetails from '../cardDetails';
import { constants } from './constants';
import { validate } from './validate';
import { getProductdetails, getFeeDetails, getProductName, getDescription, getVariantId, getVariants } from './selectors';
import { formatUSCurrency, convertStringToNumber } from '../../utils/utils';
import * as gtmActionsCreators from '../../actionCreators/GTM';

export class Ach extends PureComponent {
  /** @description React Life cycle method
   * it will invoke, when component is updated.
   */
  componentDidUpdate(prevProps) {
    const {
      isShowPopup, category, reset, gtmActions, formValues,
    } = this.props;
    const { ach } = formValues;
    if (prevProps.isShowPopup !== isShowPopup
      && isShowPopup
      && category === constants.TYPE) {
      gtmActions.gtmAchSuccess(convertStringToNumber(ach));
      reset();
    }
  }

  /** @description Callback function for submit debit card form
   * @param {object} values - form values
   */
  submitHandler = (values) => {
    const { product, feeDetails, variantId } = this.props;
    const val = {
      ...values,
      productId: product.id,
      productName: product.name,
      productType: feeDetails.cardType,
      feeType: feeDetails.feeType,
      feeValue: 0,
      quantity: 1,
      cardTypeDesc: feeDetails.typeDescription,
      feeTypeDesc: feeDetails.feeDescription,
      productImage: feeDetails.imageIcon,
      min: feeDetails.min,
      max: feeDetails.max,
      cardValue: convertStringToNumber(values.ach),
      fee: 0,
      isReloadable: false,
      variantId,
      minMaxType: feeDetails.minmaxtype,
    };
    this.props.onSubmit(constants.TYPE, constants.ACH_INFO, val);
  }

  /** @description Callback function for view button */
  viewHandler = (isViewMore) => {
    if (isViewMore && this.ach) {
      const top = this.ach.offsetTop;
      window.scrollTo(0, top);
    }
  }

  /** @description function to render the redeem ach field */
  renderAchAmountField = () => {
    const { formValues } = this.props;
    const { ach } = formValues;
    return (
      <div className="form-group gaf-form">
        <Field
          className="gaf-form"
          name="ach"
          id="ach"
          type="text"
          component={TextInput}
          normalize={formatUSCurrency}
          value={ach}
          placeholder="Type Amount"
          maxLength={10}
        />
      </div>
    );
  }

  /** @description function to render the Ach form */
  renderAchForm = () => {
    const { handleSubmit, errors } = this.props;
    const btnClass = classNames('btn gaf-btn-primary', { 'btn-disabled': errors });
    return (
      <form className="form-gaf-left" name="achForm" onSubmit={handleSubmit(this.submitHandler)}>
        {this.renderAchAmountField()}
        <div className="addto-cart-btn">
          <Button id="ach-add-to-cart" type="submit" className={btnClass}>{constants.ADD_TO_CART}</Button>
        </div>
      </form>
    );
  }

  /** @description function to render the Ach details */
  renderAchInfo = () => {
    const { descriptions, variantDetails, productName } = this.props;
    const { variants } = variantDetails || {};
    const { variant_Images: variantImage } = variants || {};
    const { descriptionTypeEcard, termsConditionEcard } = descriptions;
    const imagePath = variantImage || constants.DEFAULT_ACH_IMAGE;
    return (
      <Row>
        <Col md={12} sm={12} xs={12}>
          <div className="card-img">
            <ImageHOC
              id="ach-image"
              isAbsolute={variantImage}
              src={imagePath}
              alt={productName}
              responsive
            />
          </div>
          <CardDetails
            description={{
              productDetails: descriptionTypeEcard,
              termsAndConditions: termsConditionEcard,
            }}
            onViewClick={this.viewHandler}
          />
        </Col>
      </Row>
    );
  }

  render() {
    const { product, productName } = this.props;
    return (
      <React.Fragment>
        {product &&
          Object.keys(product).length > 0 &&
          <div ref={(ele) => { this.ach = ele; }} className="redeem-points-box mt-30">
            <Row className="show-grid redeem-points-block">
              {productName && <h3>{productName}</h3>}
              <Col md={8} sm={8} xs={12}>
                {this.renderAchInfo()}
              </Col>
              <Col md={4} sm={4} xs={12}>
                {this.renderAchForm()}
              </Col>
            </Row>
          </div>}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { form, redeemPointsState } = state;
  const { isShowPopup, category } = redeemPointsState;
  const { achForm = {} } = form;
  const { values, syncErrors } = achForm;
  return {
    formValues: values,
    errors: syncErrors,
    isShowPopup,
    category,
    productName: getProductName(state),
    variantDetails: getVariants(state),
    variantId: getVariantId(state),
    product: getProductdetails(state),
    feeDetails: getFeeDetails(state),
    descriptions: getDescription(state),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

/** PropTypes:
 * handleSubmit - func - function for submit the form
 * reset - func - function to reset the form
 * onSubmit - func - callback function for add to cart
 * gtmActions - object- action for google tag manager
 * product - object - contains details about product variant
 * feeDetails - object - contains the details of debit card fee information
 * formValues - object - Contains the values of valid form fields
 * description - object - contains the debit card information
 * errors - object - Contains the error messages for invalid form fields
 * isShowPopup - boolean -show/hide the success popup
 * category - string - selected category
 * variantId - string - unique identifier for variant
 * productName - string - name of product
*/
Ach.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  gtmActions: PropTypes.object.isRequired,
  product: PropTypes.object,
  feeDetails: PropTypes.object,
  formValues: PropTypes.object,
  descriptions: PropTypes.object,
  errors: PropTypes.object,
  isShowPopup: PropTypes.bool,
  category: PropTypes.string,
  variantId: PropTypes.string,
  productName: PropTypes.string,
  variantDetails: PropTypes.object,
};

Ach.defaultProps = {
  formValues: {},
};

const Component = reduxForm({
  form: 'achForm',
  validate,
})(Ach);

export default connect(mapStateToProps, mapDispatchToProps)(Component);

