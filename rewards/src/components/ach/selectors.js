import { createSelector } from 'reselect';

export const getAchInfo = (state) => state.redeemPointsState.achInfo;

export const getProductdetails = createSelector(
  [getAchInfo],
  (achInfo) => {
    if (achInfo) {
      const [info] = achInfo;
      const { categories } = info || {};
      const [category] = categories || [];
      const { products } = category || {};
      const [product] = products || [];
      return product;
    }
    return {};
  },
);

export const getProductName = createSelector(
  [getProductdetails],
  (productDetails) => {
    if (productDetails) {
      const { name } = productDetails;
      return name;
    }
    return null;
  },
);

export const getVariants = createSelector(
  [getProductdetails],
  (productDetails) => {
    if (productDetails) {
      const { variants } = productDetails;
      const [details] = variants || [];
      return details;
    }
    return {};
  },
);

export const getFeeDetails = createSelector(
  [getVariants],
  (details) => {
    if (details && details.variants) {
      return details.variants;
    }
    return {};
  },
);

export const getVariantId = createSelector(
  [getVariants],
  (details) => {
    if (details) {
      const { variantId } = details;
      return variantId;
    }
    return null;
  },
);

export const getDescription = createSelector(
  [getProductdetails],
  (productDetails) => {
    if (productDetails) {
      const { productSpecification } = productDetails;
      return productSpecification || {};
    }
    return {};
  },
);
