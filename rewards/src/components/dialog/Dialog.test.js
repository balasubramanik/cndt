import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Dialog from '../../components/dialog';


configure({ adapter: new Adapter() });

describe('Dialog with props', () => {
  const closeHandler = jest.fn();
  const tree = shallow(<Dialog
    onCloseClick={closeHandler}
    title={<p className="title">GAF</p>}
    body={<p className="body-content">Sample Body</p>}
    footer={<p className="footer-content">Sample Footer</p>}
  />);
  it('should be defined', () => {
    expect(Dialog).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render modal header', () => {
    expect(tree.find('ModalHeader').length).toBe(1);
  });

  it('should render modal title', () => {
    expect(tree.find('ModalTitle').length).toBe(1);
  });

  it('should render modal body', () => {
    expect(tree.find('ModalBody').length).toBe(1);
  });

  it('should render modal footer', () => {
    expect(tree.find('ModalFooter').length).toBe(1);
  });

  it('should call onHide event', () => {
    const simulateClick = tree.find('ModalHeader').find('a').prop('onClick');
    simulateClick();
    expect(closeHandler).toBeCalled();
  });
});


describe('Dialog without props', () => {
  const tree = shallow(<Dialog />);
  it('should be defined', () => {
    expect(Dialog).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render modal header', () => {
    expect(tree.find('ModalHeader').length).toBe(0);
  });

  it('should not render close button', () => {
    expect(tree.find('ModalHeader').find('a').length).toBe(0);
  });

  it('should not render modal title', () => {
    expect(tree.find('ModalTitle').length).toBe(0);
  });

  it('should not render modal body', () => {
    expect(tree.find('ModalBody').length).toBe(0);
  });

  it('should not render modal footer', () => {
    expect(tree.find('ModalFooter').length).toBe(0);
  });
});
