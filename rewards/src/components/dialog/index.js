
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'react-bootstrap';

class Dialog extends PureComponent {
  render() {
    const {
      show, onCloseClick, title, body, footer, className,
    } = this.props;
    const isIos = navigator.appVersion.indexOf('Mac') !== -1;
    const modalClass = isIos ? `${className} gaf-device-ios` : className;
    return (
      <Modal show={show} className={modalClass || ''}>
        {onCloseClick &&
          <Modal.Header>
            <a href="/" onClick={onCloseClick}>
              <span className="icon-close" />
            </a>
            {title && <Modal.Title>{title}</Modal.Title>}
          </Modal.Header>
        }
        {body &&
          <Modal.Body>
            {body}
          </Modal.Body>
        }
        {footer &&
          <Modal.Footer>
            {footer}
          </Modal.Footer>
        }
      </Modal>
    );
  }
}

/** PropTypes:
 * body - {string || number || element} - body content of dialog
 * footer - {string || number || element} - footer content of dialog
 * onCloseClick - func - function to close the popup
 * show - boolean - show/hide the popup based on the value
 * title - {string || number || element} - title of dialog
 */
Dialog.propTypes = {
  body: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
  footer: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
  onCloseClick: PropTypes.func,
  show: PropTypes.bool,
  className: PropTypes.string,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.element,
  ]),
};

export default Dialog;
