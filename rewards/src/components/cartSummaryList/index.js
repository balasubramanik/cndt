import React from 'react';
import PropTypes from 'prop-types';
import CartSummaryGiftCard from '../cartSummaryGiftCard';
import CartSummaryDebit from '../cartSummaryDebit';

const CartSummaryList = (props) => {
  const {
    cartlist,
    giftCards,
    cardAmount,
    handleDelete,
    action,
    cardDetails,
    giftCardList,
    handleQuantityInputValue,
    handleItemSelectHandler,
    updateCardAmount,
  } = props;
  const isShowGiftCard = giftCards && Object.keys(giftCards).length !== 0;
  return (
    <div className="cart-list-items" id="cart-list">
      <ul>
        {
          cartlist && cartlist.map((item, i) => {
            switch (item.category) {
              case 'giftcards': {
                if (isShowGiftCard) {
                  return (<CartSummaryGiftCard
                    key={parseInt(i.toString(), 10)}
                    index={i}
                    action={action}
                    amount={cardAmount[item.productId] || {}}
                    cardVariant={cardAmount.variant ? cardAmount.variant[item.productId] : {}}
                    cardDetails={cardDetails}
                    item={giftCardList[item.productId] || item}
                    onDelete={(index, productName) => handleDelete(index, productName)}
                    selectedCardAmount={(obj) => updateCardAmount(obj)}
                    onItemSelectHandler={(obj) => handleItemSelectHandler(obj)}
                    onhandleQuantityInput={(obj) => handleQuantityInputValue(obj)}
                  />);
                }
                return false;
              }
              case 'debit':
              case 'check':
                return (<CartSummaryDebit
                  key={parseInt(i.toString(), 10)}
                  index={i}
                  item={giftCardList[item.productId] || item}
                  maxQty={item.max}
                  onUpdate={(updatedObj) => props.handleUpdate(updatedObj)}
                  onDelete={(index, productName) => props.handleDelete(index, productName)}
                  onUpdateDisableSubmit={(value) => props.onDisableOrderSubmit(value)}
                />);
              default:
                return false;
            }
          })
        }
      </ul>
    </div>
  );
};

CartSummaryList.propTypes = {
  cartlist: PropTypes.array,
  giftCards: PropTypes.array,
  handleDelete: PropTypes.func,
  updateCardAmount: PropTypes.func,
  handleItemSelectHandler: PropTypes.func,
  handleQuantityInputValue: PropTypes.func,
  action: PropTypes.object,
  cardAmount: PropTypes.object,
  giftCardList: PropTypes.object,
  cardDetails: PropTypes.object,
};

export default CartSummaryList;
