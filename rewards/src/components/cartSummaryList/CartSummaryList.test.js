import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryList from '../../components/cartSummaryList';

configure({ adapter: new Adapter() });

describe('cartSummaryList List for CartSummaryGiftCard', () => {
  const giftCards = [{
    categories: null,
    categoryId: '2c60baca-93ed-4b3b-a046-d15473d8e692',
    createdBy: null,
    createdOn: '0001-01-01T00:00:00',
    description: 'RewardsCategoryDef',
    displayName: 'Apparels & Shoes',
    id: null,
    impersonatedBy: null,
    impersonatedOn: null,
    modifiedBy: null,
    products: [{
      productImage: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{2bd235ca-c19a-499d-8ce7-a89eed71d930}.png',
      cardTypes: ['Physical Card', 'e-Card'],
    }],
  }];

  const cardAmount = {
    MER1001503: {
      physicalcard: [25, 50, 100],
    },
    variant: {
      MER1001503: {
        physicalcard: {
          15: 'MER1001503-1',
          25: 'MER1001503-2',
          50: 'MER1001503-3',
          100: 'MER1001503-4',
          500: 'MER1001503-5',
        },
      },
    },
  };
  const cartlist = [{
    cardType: 'Physical Card',
    cardTypeDesc: 'A physical gift item is non-refundable.',
    cardValue: 25,
    category: 'giftcards',
    createdBy: 'brian@gaf-qaricksmainroofing.com',
    createdOn: '2018-12-08T12:36:17.9005055Z',
    debitCardIssuedTo: null,
    debitCardUserName: null,
    feeRate: 1,
    feeType: '$',
    feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
    feeValue: 1,
    img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
    impersonatedBy: null,
    impersonatedOn: null,
    isAvailable: true,
    isModified: false,
    isReloadableDebitCard: false,
    max: 100,
    min: 1,
    minmaxType: 'Qty',
    modifiedBy: 'brian@gaf-qaricksmainroofing.com',
    modifiedOn: '2018-12-08T12:36:17.9005055Z',
    productId: 'MER1001503',
    productName: 'Olive Garden®',
    quantity: 1,
    totalValue: 26,
    variantId: 'MER1001503-1',
  }];
  const giftCardList = [
    {
      cardType: 'Physical Card',
      cardTypeDesc: 'A physical gift item is non-refundable.',
      cardValue: 25,
      category: 'giftcards',
      createdBy: 'brian@gaf-qaricksmainroofing.com',
      createdOn: '2018-12-08T12:36:17.9005055Z',
      debitCardIssuedTo: null,
      debitCardUserName: null,
      feeRate: 1,
      feeType: '$',
      feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
      feeValue: 1,
      img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
      impersonatedBy: null,
      impersonatedOn: null,
      isAvailable: true,
      isModified: false,
      isReloadableDebitCard: false,
      max: 100,
      min: 1,
      minmaxType: 'Qty',
      modifiedBy: 'brian@gaf-qaricksmainroofing.com',
      modifiedOn: '2018-12-08T12:36:17.9005055Z',
      productId: 'MER1001503',
      productName: 'Olive Garden®',
      quantity: 1,
      totalValue: 26,
      variantId: 'MER1001503-1',
    },
    {
      cardType: 'Physical Card',
      cardTypeDesc: 'A physical gift.',
      cardValue: 5,
      category: 'giftcards',
      createdBy: 'brian@gaf-qaricksmainroofing.com',
      createdOn: '2018-12-08T12:36:04.6545166Z',
      debitCardIssuedTo: null,
      debitCardUserName: null,
      feeRate: 1,
      feeType: '$',
      feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
      feeValue: 1,
      img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{464158ea-4931-4370-b775-54982aab3d11}.png',
      impersonatedBy: null,
      impersonatedOn: null,
      isAvailable: true,
      isModified: false,
      isReloadableDebitCard: false,
      max: 100,
      min: 1,
      minmaxType: 'QTY',
      modifiedBy: 'brian@gaf-qaricksmainroofing.com',
      modifiedOn: '2018-12-08T12:36:04.6545166Z',
      productId: 'MER1001524',
      productName: 'Boston Market',
      quantity: 1,
      totalValue: 6,
      variantId: 'MER1001524-1',
    },
  ];

  const cardDetails = {
    MER1001503: {
      avgRating: '0',
      brandName: '',
      businessArea: 'Food & Dining',
      cardTypes: null,
      classification: '',
      classificationSubCategory: '',
      colors: null,
      colorsSwatches: null,
      desc: '',
      externalId: '3beaa57c-cc85-42e7-9aaa-fc2a52d7ba98',
      fiveStarCount: '0',
      fourStarCount: '0',
      id: 'MER1001503',
      variants: [{
        variantId: 'MER1001503-1',
        variants: {
          cardType: 'Physical Card',
          displayName: 'Olive Garden - $25',
          feeDescription: 'Service fee of 1 point is applicable for each physical card.',
          feeType: '$',
          feeValue: 1,
          giftCardValue: 25,
          imageIcon: 'Olivegarden.png',
          listprice: '0.0000',
          max: 100,
          min: 1,
          minmaxtype: 'Qty',
          typeDescription: 'A physical gift card will be sent to your address within 10 business days after checkout. *This item is non-refundable.',
          variant_Images: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
        },
      }],
    },
  };
  const handleDelete = jest.fn();
  const updateCardAmount = jest.fn();
  const handleItemSelectHandler = jest.fn();
  const handleQuantityInputValue = jest.fn();
  const tree = shallow(<CartSummaryList
    cartlist={cartlist}
    giftCards={giftCards}
    cardAmount={cardAmount}
    action={{}}
    amount={{}}
    cardDetails={cardDetails}
    giftCardList={giftCardList}
    handleDelete={handleDelete}
    updateCardAmount={updateCardAmount}
    handleItemSelectHandler={handleItemSelectHandler}
    handleQuantityInputValue={handleQuantityInputValue}
  />);
  it('should be defined', () => {
    expect(CartSummaryList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryGiftCard').at(0).prop('onDelete');
    simulateChange(1, 'Nike');
    expect(handleDelete).toBeCalled();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryGiftCard').at(0).prop('selectedCardAmount');
    simulateChange(1, 'Nike');
    expect(updateCardAmount).toBeCalled();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryGiftCard').at(0).prop('onItemSelectHandler');
    simulateChange({
      qty: 1,
      index: 1,
      unitValue: '25',
    });
    expect(handleItemSelectHandler).toBeCalled();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryGiftCard').at(0).prop('onhandleQuantityInput');
    simulateChange({
      qty: 1,
      index: 1,
      unitValue: 25,
      feeRate: 0.63,
    });
    expect(handleQuantityInputValue).toBeCalled();
  });
});

describe('cartSummaryList List for CartSummaryGiftCard Variant Empty and Amount Empty object', () => {
  const giftCards = [{
    categories: null,
    categoryId: '2c60baca-93ed-4b3b-a046-d15473d8e692',
    createdBy: null,
    createdOn: '0001-01-01T00:00:00',
    description: 'RewardsCategoryDef',
    displayName: 'Apparels & Shoes',
    id: null,
    impersonatedBy: null,
    impersonatedOn: null,
    modifiedBy: null,
    products: [{
      productImage: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{2bd235ca-c19a-499d-8ce7-a89eed71d930}.png',
      cardTypes: ['Physical Card', 'e-Card'],
    }],
  }];

  const cardAmount = {
    MER1001503: null,
    variant: null,
  };
  const cartlist = [{
    cardType: 'Physical Card',
    cardTypeDesc: 'A physical gift item is non-refundable.',
    cardValue: 25,
    category: 'giftcards',
    createdBy: 'brian@gaf-qaricksmainroofing.com',
    createdOn: '2018-12-08T12:36:17.9005055Z',
    debitCardIssuedTo: null,
    debitCardUserName: null,
    feeRate: 1,
    feeType: '$',
    feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
    feeValue: 1,
    img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
    impersonatedBy: null,
    impersonatedOn: null,
    isAvailable: true,
    isModified: false,
    isReloadableDebitCard: false,
    max: 100,
    min: 1,
    minmaxType: 'Qty',
    modifiedBy: 'brian@gaf-qaricksmainroofing.com',
    modifiedOn: '2018-12-08T12:36:17.9005055Z',
    productId: 'MER1001503',
    productName: 'Olive Garden®',
    quantity: 1,
    totalValue: 26,
    variantId: 'MER1001503-1',
  }];
  const giftCardList = [
    {
      cardType: 'Physical Card',
      cardTypeDesc: 'A physical gift item is non-refundable.',
      cardValue: 25,
      category: 'giftcards',
      createdBy: 'brian@gaf-qaricksmainroofing.com',
      createdOn: '2018-12-08T12:36:17.9005055Z',
      debitCardIssuedTo: null,
      debitCardUserName: null,
      feeRate: 1,
      feeType: '$',
      feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
      feeValue: 1,
      img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
      impersonatedBy: null,
      impersonatedOn: null,
      isAvailable: true,
      isModified: false,
      isReloadableDebitCard: false,
      max: 100,
      min: 1,
      minmaxType: 'Qty',
      modifiedBy: 'brian@gaf-qaricksmainroofing.com',
      modifiedOn: '2018-12-08T12:36:17.9005055Z',
      productId: 'MER1001503',
      productName: 'Olive Garden®',
      quantity: 1,
      totalValue: 26,
      variantId: 'MER1001503-1',
    },
    {
      cardType: 'Physical Card',
      cardTypeDesc: 'A physical gift.',
      cardValue: 5,
      category: 'giftcards',
      createdBy: 'brian@gaf-qaricksmainroofing.com',
      createdOn: '2018-12-08T12:36:04.6545166Z',
      debitCardIssuedTo: null,
      debitCardUserName: null,
      feeRate: 1,
      feeType: '$',
      feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
      feeValue: 1,
      img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{464158ea-4931-4370-b775-54982aab3d11}.png',
      impersonatedBy: null,
      impersonatedOn: null,
      isAvailable: true,
      isModified: false,
      isReloadableDebitCard: false,
      max: 100,
      min: 1,
      minmaxType: 'QTY',
      modifiedBy: 'brian@gaf-qaricksmainroofing.com',
      modifiedOn: '2018-12-08T12:36:04.6545166Z',
      productId: 'MER1001524',
      productName: 'Boston Market',
      quantity: 1,
      totalValue: 6,
      variantId: 'MER1001524-1',
    },
  ];

  const cardDetails = {
    MER1001503: {
      avgRating: '0',
      brandName: '',
      businessArea: 'Food & Dining',
      cardTypes: null,
      classification: '',
      classificationSubCategory: '',
      colors: null,
      colorsSwatches: null,
      desc: '',
      externalId: '3beaa57c-cc85-42e7-9aaa-fc2a52d7ba98',
      fiveStarCount: '0',
      fourStarCount: '0',
      id: 'MER1001503',
      variants: [{
        variantId: 'MER1001503-1',
        variants: {
          cardType: 'Physical Card',
          displayName: 'Olive Garden - $25',
          feeDescription: 'Service fee of 1 point is applicable for each physical card.',
          feeType: '$',
          feeValue: 1,
          giftCardValue: 25,
          imageIcon: 'Olivegarden.png',
          listprice: '0.0000',
          max: 100,
          min: 1,
          minmaxtype: 'Qty',
          typeDescription: 'A physical gift card will be sent to your address within 10 business days after checkout. *This item is non-refundable.',
          variant_Images: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
        },
      }],
    },
  };
  const handleDelete = jest.fn();
  const updateCardAmount = jest.fn();
  const handleItemSelectHandler = jest.fn();
  const handleQuantityInputValue = jest.fn();
  const tree = shallow(<CartSummaryList
    cartlist={cartlist}
    giftCards={giftCards}
    cardAmount={cardAmount}
    action={{}}
    amount={{}}
    cardDetails={cardDetails}
    giftCardList={giftCardList}
    handleDelete={handleDelete}
    updateCardAmount={updateCardAmount}
    handleItemSelectHandler={handleItemSelectHandler}
    handleQuantityInputValue={handleQuantityInputValue}
  />);
  it('should be defined', () => {
    expect(CartSummaryList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('cartSummaryList List for CartSummaryDebitCard', () => {
  const giftCards = [{
    categories: null,
    categoryId: '2c60baca-93ed-4b3b-a046-d15473d8e692',
    createdBy: null,
    createdOn: '0001-01-01T00:00:00',
    description: 'RewardsCategoryDef',
    displayName: 'Apparels & Shoes',
    id: null,
    impersonatedBy: null,
    impersonatedOn: null,
    modifiedBy: null,
    products: [{
      productImage: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{2bd235ca-c19a-499d-8ce7-a89eed71d930}.png',
      cardTypes: ['Physical Card', 'e-Card'],
    }],
  }];

  const cardAmount = {
    MER1001503: {
      physicalcard: [25, 50, 100],
    },
    variant: {
      MER1001503: {
        physicalcard: {
          15: 'MER1001503-1',
          25: 'MER1001503-2',
          50: 'MER1001503-3',
          100: 'MER1001503-4',
          500: 'MER1001503-5',
        },
      },
    },
  };
  const cartlist = [{
    cardType: 'Physical Card',
    cardTypeDesc: 'A physical gift item is non-refundable.',
    cardValue: 25,
    category: 'debit',
    createdBy: 'brian@gaf-qaricksmainroofing.com',
    createdOn: '2018-12-08T12:36:17.9005055Z',
    debitCardIssuedTo: null,
    debitCardUserName: null,
    feeRate: 1,
    feeType: '$',
    feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
    feeValue: 1,
    img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
    impersonatedBy: null,
    impersonatedOn: null,
    isAvailable: true,
    isModified: false,
    isReloadableDebitCard: false,
    max: 100,
    min: 1,
    minmaxType: 'Qty',
    modifiedBy: 'brian@gaf-qaricksmainroofing.com',
    modifiedOn: '2018-12-08T12:36:17.9005055Z',
    productId: 'MER1001503',
    productName: 'Olive Garden®',
    quantity: 1,
    totalValue: 26,
    variantId: 'MER1001503-1',
  }];
  const giftCardList = [
    {
      cardType: 'Physical Card',
      cardTypeDesc: 'A physical gift item is non-refundable.',
      cardValue: 25,
      category: 'giftcards',
      createdBy: 'brian@gaf-qaricksmainroofing.com',
      createdOn: '2018-12-08T12:36:17.9005055Z',
      debitCardIssuedTo: null,
      debitCardUserName: null,
      feeRate: 1,
      feeType: '$',
      feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
      feeValue: 1,
      img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
      impersonatedBy: null,
      impersonatedOn: null,
      isAvailable: true,
      isModified: false,
      isReloadableDebitCard: false,
      max: 100,
      min: 1,
      minmaxType: 'Qty',
      modifiedBy: 'brian@gaf-qaricksmainroofing.com',
      modifiedOn: '2018-12-08T12:36:17.9005055Z',
      productId: 'MER1001503',
      productName: 'Olive Garden®',
      quantity: 1,
      totalValue: 26,
      variantId: 'MER1001503-1',
    },
    {
      cardType: 'Physical Card',
      cardTypeDesc: 'A physical gift.',
      cardValue: 5,
      category: 'giftcards',
      createdBy: 'brian@gaf-qaricksmainroofing.com',
      createdOn: '2018-12-08T12:36:04.6545166Z',
      debitCardIssuedTo: null,
      debitCardUserName: null,
      feeRate: 1,
      feeType: '$',
      feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
      feeValue: 1,
      img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{464158ea-4931-4370-b775-54982aab3d11}.png',
      impersonatedBy: null,
      impersonatedOn: null,
      isAvailable: true,
      isModified: false,
      isReloadableDebitCard: false,
      max: 100,
      min: 1,
      minmaxType: 'QTY',
      modifiedBy: 'brian@gaf-qaricksmainroofing.com',
      modifiedOn: '2018-12-08T12:36:04.6545166Z',
      productId: 'MER1001524',
      productName: 'Boston Market',
      quantity: 1,
      totalValue: 6,
      variantId: 'MER1001524-1',
    },
  ];

  const cardDetails = {
    MER1001503: {
      avgRating: '0',
      brandName: '',
      businessArea: 'Food & Dining',
      cardTypes: null,
      classification: '',
      classificationSubCategory: '',
      colors: null,
      colorsSwatches: null,
      desc: '',
      externalId: '3beaa57c-cc85-42e7-9aaa-fc2a52d7ba98',
      fiveStarCount: '0',
      fourStarCount: '0',
      id: 'MER1001503',
      variants: [{
        variantId: 'MER1001503-1',
        variants: {
          cardType: 'Physical Card',
          displayName: 'Olive Garden - $25',
          feeDescription: 'Service fee of 1 point is applicable for each physical card.',
          feeType: '$',
          feeValue: 1,
          giftCardValue: 25,
          imageIcon: 'Olivegarden.png',
          listprice: '0.0000',
          max: 100,
          min: 1,
          minmaxtype: 'Qty',
          typeDescription: 'A physical gift card will be sent to your address within 10 business days after checkout. *This item is non-refundable.',
          variant_Images: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
        },
      }],
    },
  };
  const handleDelete = jest.fn();
  const handleUpdate = {
    value: 555,
    index: 1,
    unitValue: 25,
    feeValue: 0.63,
  };
  const onDisableOrderSubmit = true;
  const updateCardAmount = jest.fn();
  const handleItemSelectHandler = jest.fn();
  const handleQuantityInputValue = jest.fn();

  const tree = shallow(<CartSummaryList
    cartlist={cartlist}
    giftCards={giftCards}
    cardAmount={cardAmount}
    action={{}}
    amount={{}}
    cardDetails={cardDetails}
    giftCardList={giftCardList}
    handleDelete={handleDelete}
    updateCardAmount={updateCardAmount}
    handleItemSelectHandler={handleItemSelectHandler}
    handleQuantityInputValue={handleQuantityInputValue}
  />);
  it('should be defined', () => {
    expect(CartSummaryList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryDebit').at(0).prop('onDelete');
    simulateChange(1, 'Nike');
    expect(handleDelete).toBeCalled();
  });

  it('should call handleUpdate', () => {
    const simulateChange = tree.find('CartSummaryDebit').at(0).prop('onUpdate');
    simulateChange({
      value: 555,
      index: 1,
      unitValue: 25,
      feeValue: 0.63,
    });
    expect(handleUpdate).toBeCalled();
  });

  it('should call onDisableOrderSubmit', () => {
    const simulateChange = tree.find('CartSummaryDebit').at(0).prop('onUpdateDisableSubmit');
    simulateChange(true);
    expect(onDisableOrderSubmit).toBeCalled();
  });
});

describe('cartSummaryList List for Default', () => {
  const giftCards = [{
    categories: null,
    categoryId: '2c60baca-93ed-4b3b-a046-d15473d8e692',
    createdBy: null,
    createdOn: '0001-01-01T00:00:00',
    description: 'RewardsCategoryDef',
    displayName: 'Apparels & Shoes',
    id: null,
    impersonatedBy: null,
    impersonatedOn: null,
    modifiedBy: null,
    products: [{
      productImage: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{2bd235ca-c19a-499d-8ce7-a89eed71d930}.png',
      cardTypes: ['Physical Card', 'e-Card'],
    }],
  }];
  const cartlist = [{
    cardType: 'Physical Card',
    cardTypeDesc: 'A physical gift item is non-refundable.',
    cardValue: 25,
    category: null,
    createdBy: 'brian@gaf-qaricksmainroofing.com',
    createdOn: '2018-12-08T12:36:17.9005055Z',
    debitCardIssuedTo: null,
    debitCardUserName: null,
    feeRate: 1,
    feeType: '$',
    feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
    feeValue: 1,
    img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
    impersonatedBy: null,
    impersonatedOn: null,
    isAvailable: true,
    isModified: false,
    isReloadableDebitCard: false,
    max: 100,
    min: 1,
    minmaxType: 'Qty',
    modifiedBy: 'brian@gaf-qaricksmainroofing.com',
    modifiedOn: '2018-12-08T12:36:17.9005055Z',
    productId: 'MER1001503',
    productName: 'Olive Garden®',
    quantity: 1,
    totalValue: 26,
    variantId: 'MER1001503-1',
  }];
  const tree = shallow(<CartSummaryList
    cartlist={cartlist}
    giftCards={giftCards}
  />);
  it('should be defined', () => {
    expect(CartSummaryList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('cartSummaryList List for giftCard False', () => {
  const giftCards = [];
  const cartlist = [{
    cardType: 'Physical Card',
    cardTypeDesc: 'A physical gift item is non-refundable.',
    cardValue: 25,
    category: 'giftcards',
    createdBy: 'brian@gaf-qaricksmainroofing.com',
    createdOn: '2018-12-08T12:36:17.9005055Z',
    debitCardIssuedTo: null,
    debitCardUserName: null,
    feeRate: 1,
    feeType: '$',
    feeTypeDesc: 'Service fee of 1 point is applicable for each physical card.',
    feeValue: 1,
    img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{bb29748c-1641-4ad2-8a17-4cc5c350f135}.png',
    impersonatedBy: null,
    impersonatedOn: null,
    isAvailable: true,
    isModified: false,
    isReloadableDebitCard: false,
    max: 100,
    min: 1,
    minmaxType: 'Qty',
    modifiedBy: 'brian@gaf-qaricksmainroofing.com',
    modifiedOn: '2018-12-08T12:36:17.9005055Z',
    productId: 'MER1001503',
    productName: 'Olive Garden®',
    quantity: 1,
    totalValue: 26,
    variantId: 'MER1001503-1',
  }];
  const tree = shallow(<CartSummaryList
    cartlist={cartlist}
    giftCards={giftCards}
  />);
  it('should be defined', () => {
    expect(CartSummaryList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

