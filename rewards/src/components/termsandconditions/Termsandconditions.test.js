import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import TermsAndConditions from '../termsandconditions';


configure({ adapter: new Adapter() });

describe('TermsAndConditions', () => {
  const tree = shallow(<TermsAndConditions />);

  it('should be defined', () => {
    expect(TermsAndConditions).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
    tree.instance().handleClose();
  });
});
