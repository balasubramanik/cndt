import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Modal, Row, Col } from 'react-bootstrap';
import envConfig from 'envConfig'; //eslint-disable-line
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { authentication, history, authorization } from '../../routes';
import Loader from '../loader';
import { termsandconditions } from './constants';
import * as termsandconditionsActionCreators from '../../actionCreators/TermsAndConditions';
import { inquiryDetails } from '../../constants/constants';

class TermsAndConditions extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.handleClose = this.handleClose.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCheck = this.handleCheck.bind(this);

    this.state = {
      show: true,
      isComplete: false,
      isSubmitted: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { done } = this.props;
    if (prevProps.done !== done && done) {
      authorization.setRewardAccess(true);
      authorization.setUserRoles('FULL_ACCESS');
      history.push('/');
    }
  }

  handleClose() {
    const url = `${envConfig.cczBaseUrl}`;
    this.setState({ show: false });
    authentication.clearAuthSession();
    window.open(url, '_self');
  }

  handleCheck = (event) => {
    this.setState({
      isComplete: event.target.checked,
    });
  }

  handleSubmit() {
    const { email } = authentication.getAuthDetails();
    this.setState({ isSubmitted: true }, () => {
      const { isComplete } = this.state;
      const { userInfo } = this.props;
      const { firstName = '', lastName = '' } = userInfo;
      if (isComplete) {
        this.props.actions.submitTermsAndConditions({
          isTermsAndConditionsAccepted: isComplete,
          eSignName: `${firstName} ${lastName}`,
          eSignDate: moment(this.today).format(),
          user: {
            firstName,
            lastName,
            emailAddress: email,
          },
        });
      }
    });
  }

  render() {
    const { isComplete, isSubmitted } = this.state;
    const authCheckboxClass = classNames('indicator', { 'indicator-error': !isComplete && isSubmitted });
    const acceptButtonClass = classNames('btn btn-block gaf-btn-primary', { 'btn-disabled': !isComplete });
    const modelTitle = termsandconditions.MODAL_TITLE;
    const { MODAL_BODY } = termsandconditions;
    return (
      <div>
        <Modal className="gaf-model-popup terms-and-conditions" show={this.state.show}>
          <Modal.Header>
            <button onClick={this.handleClose} className="icon-close modal-close-btn"></button>
            <Modal.Title>{modelTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <p className="welcome-gaf">{MODAL_BODY.welcomeGaf}</p>
              <p className="Please-make-sure-you">{MODAL_BODY.pleaseMakeSureYou}
                {MODAL_BODY.query} <strong>{`${MODAL_BODY.call} ${inquiryDetails.Phone}`} {inquiryDetails.Email}</strong>
              </p>
              <div className="gaf-checkbox">
                <label className="control control-checkbox"><span>{MODAL_BODY.gafCheckbox} <a id="tncpopup" href={envConfig.termsAndConditionUrl} target="_blank" rel="noopener noreferrer">{MODAL_BODY.linkText} </a></span>
                  <input type="checkbox" checked={this.state.isComplete} onChange={this.handleCheck} />
                  <div className={authCheckboxClass}></div>
                </label>
              </div>
              <div className="decline-accept-btn">
                <Row>
                  <Col md={6} xs={12}>
                    <button className="btn btn-block gaf-btn-secondary mtb" onClick={this.handleClose}>{MODAL_BODY.decline}</button>
                  </Col>
                  <Col md={6} xs={12}>
                    <button className={acceptButtonClass} onClick={this.handleSubmit}>{MODAL_BODY.accept}</button>
                  </Col>
                </Row>
              </div>
            </div>
          </Modal.Body>
        </Modal>
        {this.props.isLoading && <Loader />}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { TermsAndConditionsState, myProfileState } = state;
  const { done, isLoading } = TermsAndConditionsState;
  const { userInfo } = myProfileState;
  return {
    done,
    userInfo,
    isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(termsandconditionsActionCreators, dispatch),
  };
}

TermsAndConditions.propTypes = {
  actions: PropTypes.object.isRequired,
  done: PropTypes.bool,
  userInfo: PropTypes.object,
  isLoading: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(TermsAndConditions);
