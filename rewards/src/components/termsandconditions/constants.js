const termsandconditions = {
  VALID_SIGNATURE_MESSAGE: 'Please enter a valid signature',
  MODAL_TITLE: 'Terms and Conditions',
  MODAL_BODY: {
    welcomeGaf: 'Welcome to GAF Rewards',
    pleaseMakeSureYou: 'Please make sure you are authorized, understand, and accept our Terms & Conditions before using this site. ',
    query: 'If you have any further questions before accepting our Terms & Conditions, please contact us at',
    or: 'or',
    call: 'call',
    gafCheckbox: 'I am an authorized person to accept the GAF Rewards',
    linkText: 'Terms & Conditions',
    decline: 'Decline',
    accept: 'ACCEPT',
  },
};

export { termsandconditions };
