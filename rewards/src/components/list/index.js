import React from 'react';
import PropTypes from 'prop-types';

const List = (props) => {
  const { data } = props;
  return (
    <div className="eligible-states">
      <ul>
        {data && data.map((item, i) => (
          <li key={i.toString()}>{item.name} ({item.code})</li>
        ))}
      </ul>
    </div>
  );
};

List.propTypes = {
  data: PropTypes.array,
};

export default List;
