import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import List from '../../components/list';

configure({ adapter: new Adapter() });

describe('List with array', () => {
  const tree = shallow(<List
    data={[{ name: 'AAA', code: '111' }, { name: 'BBB', code: '222' }, { name: 'CCC', code: '333' }, { name: 'DDD', code: '444' }]
    }
  />);

  it('should be defined', () => {
    expect(List).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
