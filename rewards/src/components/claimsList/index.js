import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import CSV from '../../components/csv';
import Tables from '../../components/tables';
import { claimsListValue } from './constants';
import ToolTip from '../toolTip';
import { formatDateUTC, formatPoint } from '../../utils/utils';
import RouteConstants from '../../constants/RouteConstants';
import { authorization, history } from '../../routes';

class ClaimsList extends React.Component {
  /* Redirect to Claims Details Page */
  redirectToClaimsDetails = (e, row) => {
    e.preventDefault();
    if (row.claimStatus.toLowerCase() === 'completed') {
      this.props.viewClaimDetails(row.claimId);
      history.push(RouteConstants.CLAIM_DETAILS.replace(':claimId', row.id));
    }
  }

  /* Format the Claims List Table data */
  formatData = (records) => {
    if (!records || records.length === 0) return [];
    return records.map((record) => (
      {
        ...record,
        lastName: (record.firstName && record.lastName) ? this.displayUsername(record.firstName, record.lastName) : '',
        displayName: (record.firstName && record.lastName) ? `${record.firstName.charAt(0).toUpperCase()}. ${record.lastName.charAt(0).toUpperCase() + record.lastName.substr(1)}` : '',
        submittedOn: (record.submittedOn) ? formatDateUTC(record.submittedOn) : '',
        modifiedOn: (record.modifiedOn) ? formatDateUTC(record.modifiedOn) : '',
        value: (record.claimStatus && record.claimStatus.toLowerCase() === 'completed') ? `$${formatPoint(record.value)}` : 'N/A',
        pointsEarned: (record.claimStatus && record.claimStatus.toLowerCase() === 'completed') ? `${formatPoint(record.pointsEarned)}` : 'N/A',
        claimStatus: (record.claimStatus) ? `${record.claimStatus.charAt(0).toUpperCase() + record.claimStatus.substr(1)}` : '',
        invoice: (record.claimId && record.claimId.charAt(0).toUpperCase() === 'G') ? <a href="/" className="text-underline" onClick={(e) => this.props.invoiceDetails(e, record.claimId, record.id)}>View Invoice(s)</a> : 'N/A',
        details: <button className="btn gaf-btn-secondary gaf-btn-sm" disabled={record.claimStatus && (record.claimStatus.toLowerCase() !== 'completed')} onClick={(event) => this.redirectToClaimsDetails(event, record)}>View</button>,
      }
    ));
  };

  displayUsername = (firstName, lastName) => (
    <ToolTip
      id={`${firstName.charAt(0).toUpperCase()}. ${lastName.charAt(0).toUpperCase() + lastName.substr(1)}`}
      text={`${firstName.charAt(0).toUpperCase()}. ${lastName.charAt(0).toUpperCase() + lastName.substr(1)}`}
    >
      <span role="button">{`${firstName.charAt(0).toUpperCase()}. ${lastName.charAt(0).toUpperCase() + lastName.substr(1)}`}</span>
    </ToolTip>
  );

  /* Creating Claims List CSV file */
  createClaimsListCSV = () => {
    const { isAsyncExportCSV = false, exportCSVData = [], data = [] } = this.props;
    const formattedData = JSON.parse(isAsyncExportCSV) ? this.formatData(exportCSVData) : this.formatData(data);
    return formattedData;
  }

  /* Creating Claims List Table Header */
  tableHeder = () => (
    <Row className="show-grid">
      <Col xs={8} sm={4} md={4} lg={6}>
        <h4>{claimsListValue.TableTitle}</h4>
      </Col>
      <Col xs={4} sm={8} md={8} lg={6}>
        {this.renderCSVLink()}
      </Col>
    </Row>);


  /* Format the table coloumn based on the contractor role */
  handleClaimsListTableHeder = (claimsListTableHeder) => {
    if (authorization.userRolesList.length && (authorization.userRolesList.includes('FULL_ACCESS') || authorization.userRolesList.includes('VIEW_POINTS'))) {
      const tableheaderInvoiceList = [...claimsListTableHeder];
      tableheaderInvoiceList.splice(tableheaderInvoiceList.length - 3, 0, claimsListValue.PTSEarned);
      return tableheaderInvoiceList;
    }
    const tableheaderInvoiceList = [...claimsListTableHeder];
    return tableheaderInvoiceList;
  }

  /* Rendering CSV File */
  renderCSVLink = () => {
    const { data, triggerDownload } = this.props;
    const isAsync = true;
    const CSVRecords = this.createClaimsListCSV();
    return (
      <div className="gaf-export-block">
        <CSV
          filename={claimsListValue.heading.filename}
          config={claimsListValue.csvConfig}
          data={CSVRecords}
          onClick={this.props.CSVHandler}
          isAsync={isAsync}
          triggerDownload={triggerDownload}
          disabled={!data || data.length === 0}
        >
          <div className="pull-right">
            <span><i className="icon-Export-Icon"></i><span>{claimsListValue.heading.label}</span></span>
          </div>
        </CSV>
      </div>
    );
  }

  render() {
    const { loading, data, defaultSort } = this.props;
    return (
      <div>
        <div className="gaf-table-header">
          {this.tableHeder()}
        </div>
        <div className="claim-list-table">
          <Tables config={this.handleClaimsListTableHeder(claimsListValue.claimsListTableHeder)} data={this.formatData(data)} isLoading={loading} onSortingClick={(key, isAsc) => this.props.sortingData(key, isAsc)} sortByKey={defaultSort} />
        </div>
      </div>
    );
  }
}

ClaimsList.propTypes = {
  data: PropTypes.array,
  sortingData: PropTypes.func,
  invoiceDetails: PropTypes.func,
  loading: PropTypes.bool,
  CSVHandler: PropTypes.func,
  isAsyncExportCSV: PropTypes.string,
  exportCSVData: PropTypes.array,
  defaultSort: PropTypes.string,
  viewClaimDetails: PropTypes.func,
  triggerDownload: PropTypes.bool,
};

export default ClaimsList;
