export const claimsListValue = {
  claimsListTableHeder: [
    { title: 'CLAIM #', key: 'claimId', sort: true },
    { title: 'USER NAME', key: 'lastName', sort: true },
    { title: 'SUBMITTED', key: 'submittedOn', sort: true },
    { title: 'UPDATED', key: 'modifiedOn', sort: true },
    { title: 'VALUE', key: 'value', sort: true },
    { title: 'STATUS', key: 'claimStatus', sort: true },
    { title: 'INVOICE', key: 'invoice', sort: false },
    { title: 'DETAILS', key: 'details', sort: false },
  ],
  PTSEarned: {
    title: 'PTS. EARNED', key: 'pointsEarned', sort: true,
  },
  heading: {
    filename: 'ClaimsList.csv',
    label: 'Export',
  },
  csvConfig: [
    { colName: 'Claim', key: 'claimId' },
    { colName: 'Username', key: 'displayName' },
    { colName: 'Submitted', key: 'submittedOn' },
    { colName: 'Updated', key: 'modifiedOn' },
    { colName: 'Value', key: 'value' },
    { colName: 'PTS Earned(C)', key: 'pointsEarned' },
    { colName: 'Status', key: 'claimStatus' },
  ],
  TableTitle: 'List of Claims',
};

export const claimInvoiceDetails = {
  claimInvoiceListTableHeder: [
    { title: 'FILE NAME', key: 'fileName', sort: true },
    { title: 'SUBMITTED', key: 'submittedOn', sort: true },
  ],
};

