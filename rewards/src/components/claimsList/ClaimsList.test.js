import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ClaimsList from '../../components/claimsList';
import { history } from '../../routes';
configure({ adapter: new Adapter() });
history.push = jest.fn();
describe('claimsList', () => {
  const invoiceHandler = jest.fn();
  const sortingDataHandler = jest.fn();
  const csvHandler = jest.fn();
  const refHandler = jest.fn();
  const viewClaimHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<ClaimsList
    data={[{
      claimId: 'G00001012',
      firstName: 'Jude',
      lastName: 'Law',
      claimStatus: 'completed',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
      pointsEarned: '2000',
    },
    {
      claimId: 'G00001100',
      firstName: 'Calvin',
      lastName: 'Klein',
      claimStatus: 'pending',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
      pointsEarned: '5000',
    },
    {
      claimId: 'G00001224',
      firstName: 'Shane',
      lastName: 'Warne',
      claimStatus: 'processed',
      submittedOn: '2018-09-15T19:06:34.868719+05:30',
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsEarned: '3000',
    },
    {
      claimId: 'A00001224',
      firstName: '',
      lastName: '',
      claimStatus: '',
      submittedOn: '',
      modifiedOn: '',
      pointsEarned: '0',
    },
    ]}
    sortingData={sortingDataHandler}
    invoiceDetails={invoiceHandler}
    loading
    CSVHandler={csvHandler}
    onRef={refHandler}
    isAsyncExportCSV="true"
    exportCSVData={[
      {
        claimId: 'G00001012',
        lastName: 'Law',
        submittedOn: '2018-09-28T20:09:35.3475807+05:30',
        modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
        value: -3724,
        pointsEarned: -37.24,
        claimStatus: 'completed',
      },
      {
        claimId: 'G00001100',
        lastName: 'Klein',
        submittedOn: '2018-09-28T20:09:35.3475807+05:30',
        modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
        value: -2816,
        pointsEarned: -28.16,
        claimStatus: 'pending',
      },
      {
        claimId: 'G00001224',
        lastName: 'Warne',
        submittedOn: '2018-09-15T19:06:34.868719+05:30',
        modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
        value: 2666,
        pointsEarned: 26.66,
        claimStatus: 'processed',
      },
      {
        claimId: 'A00001224',
        lastName: '',
        submittedOn: '',
        modifiedOn: '',
        value: 0,
        pointsEarned: 0,
        claimStatus: '',
      },
    ]}
    viewClaimDetails={viewClaimHandler}
  />);
  it('should be defined', () => {
    expect(ClaimsList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Table Component', () => {
    expect(tree.find('Tables').length).toBe(1);
  });

  it('should not show Table Rows when contractList array not supplied', () => {
    const tableTree = shallow(<ClaimsList
      data={undefined}
    />);
    expect(tableTree.find('Tables').prop('data').length).toBe(0);
  });
  it('should call CSVHandler', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(csvHandler).toBeCalled();
  });
  it('should call table sort for first name', () => {
    const simulateClick = tree.find('Tables').at(0).prop('onSortingClick');
    simulateClick('firstName', true);
    expect(sortingDataHandler).toBeCalled();
  });
});

describe('claimsList with onClick triggered inside tables', () => {
  const invoiceHandler = jest.fn();
  const sortingDataHandler = jest.fn();
  const csvHandler = jest.fn();
  const refHandler = jest.fn();
  const viewClaimHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = mount(<ClaimsList
    data={[{
      claimId: 'G00001012',
      firstName: 'Jude',
      lastName: 'Law',
      claimStatus: 'completed',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
      pointsEarned: '2000',
    },
    {
      claimId: 'G00001100',
      firstName: 'Calvin',
      lastName: 'Klein',
      claimStatus: 'pending',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
      pointsEarned: '5000',
    },
    {
      claimId: 'G00001224',
      firstName: 'Shane',
      lastName: 'Warne',
      claimStatus: 'processed',
      submittedOn: '2018-09-15T19:06:34.868719+05:30',
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsEarned: '3000',
    },
    {
      claimId: 'A00001224',
      firstName: '',
      lastName: '',
      claimStatus: '',
      submittedOn: '',
      modifiedOn: '',
      pointsEarned: '0',
    },
    ]}
    sortingData={sortingDataHandler}
    invoiceDetails={invoiceHandler}
    loading
    CSVHandler={csvHandler}
    onRef={refHandler}
    isAsyncExportCSV="true"
    exportCSVData={[
      {
        claimId: 'G00001012',
        lastName: 'Law',
        submittedOn: '2018-09-28T20:09:35.3475807+05:30',
        modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
        value: -3724,
        pointsEarned: -37.24,
        claimStatus: 'completed',
      },
      {
        claimId: 'G00001100',
        lastName: 'Klein',
        submittedOn: '2018-09-28T20:09:35.3475807+05:30',
        modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
        value: -2816,
        pointsEarned: -28.16,
        claimStatus: 'pending',
      },
      {
        claimId: 'G00001224',
        lastName: 'Warne',
        submittedOn: '2018-09-15T19:06:34.868719+05:30',
        modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
        value: 2666,
        pointsEarned: 26.66,
        claimStatus: 'processed',
      },
      {
        claimId: 'A00001224',
        lastName: '',
        submittedOn: '',
        modifiedOn: '',
        value: 0,
        pointsEarned: 0,
        claimStatus: '',
      },
    ]}
    viewClaimDetails={viewClaimHandler}
  />);
  it('should redirect to Claim Details', () => {
    const simulateClick = tree.find('Tables').find('button').at(0).prop('onClick');
    simulateClick(event);
    expect(viewClaimHandler).toBeCalled();
  });
  it('should display list of invoices', () => {
    const simulateClick = tree.find('Tables').find('.text-underline').at(0).prop('onClick');
    simulateClick(event);
    expect(invoiceHandler).toBeCalled();
  });
});


describe('claimsList with View Claim Details triggered for Claim Status not being completed', () => {
  const invoiceHandler = jest.fn();
  const sortingDataHandler = jest.fn();
  const csvHandler = jest.fn();
  const refHandler = jest.fn();
  const viewClaimHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = mount(<ClaimsList
    data={[{
      claimId: 'G00001012',
      firstName: 'Jude',
      lastName: 'Law',
      claimStatus: 'completed',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
      pointsEarned: '2000',
    },
    {
      claimId: 'G00001100',
      firstName: 'Calvin',
      lastName: 'Klein',
      claimStatus: 'pending',
      submittedOn: '2018-09-28T20:09:35.3475807+05:30',
      modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
      pointsEarned: '5000',
    },
    {
      claimId: 'G00001224',
      firstName: 'Shane',
      lastName: 'Warne',
      claimStatus: 'processed',
      submittedOn: '2018-09-15T19:06:34.868719+05:30',
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsEarned: '3000',
    },
    {
      claimId: 'A00001224',
      firstName: '',
      lastName: '',
      claimStatus: '',
      submittedOn: '',
      modifiedOn: '',
      pointsEarned: '0',
    },
    ]}
    sortingData={sortingDataHandler}
    invoiceDetails={invoiceHandler}
    loading
    CSVHandler={csvHandler}
    onRef={refHandler}
    isAsyncExportCSV="true"
    exportCSVData={[
      {
        claimId: 'G00001012',
        lastName: 'Law',
        submittedOn: '2018-09-28T20:09:35.3475807+05:30',
        modifiedOn: '2018-11-27T15:35:39.3639104+05:30',
        value: -3724,
        pointsEarned: -37.24,
        claimStatus: 'completed',
      },
      {
        claimId: 'G00001100',
        lastName: 'Klein',
        submittedOn: '2018-09-28T20:09:35.3475807+05:30',
        modifiedOn: '2018-09-28T19:19:51.6259884+05:30',
        value: -2816,
        pointsEarned: -28.16,
        claimStatus: 'pending',
      },
      {
        claimId: 'G00001224',
        lastName: 'Warne',
        submittedOn: '2018-09-15T19:06:34.868719+05:30',
        modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
        value: 2666,
        pointsEarned: 26.66,
        claimStatus: 'processed',
      },
      {
        claimId: 'A00001224',
        lastName: '',
        submittedOn: '',
        modifiedOn: '',
        value: 0,
        pointsEarned: 0,
        claimStatus: '',
      },
    ]}
    viewClaimDetails={viewClaimHandler}
  />);
  it('should not redirect to Claim Details', () => {
    const simulateClick = tree.find('Tables').find('button').at(1).prop('onClick');
    simulateClick(event);
    expect(viewClaimHandler).not.toBeCalled();
  });
});
