import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import HeaderMenu from '../../components/headerMenu';


configure({ adapter: new Adapter() });

describe('HeaderMenu redirect page according to current location', () => {
  const navigationHandler = jest.fn();
  const navLinks = [
    { title: 'Home', url: '/', isExternal: false },
    { title: 'What Qualifies', url: '', isExternal: false },
    { title: 'Submit Invoice', url: '', isExternal: false },
    { title: 'Redeem', url: '', isExternal: false },
    { title: 'Claim Status', url: '', isExternal: false },
    { title: 'Points Histroy', url: '', isExternal: false },
    { title: 'Contractor Zone', url: '', isExternal: false },
  ];
  const tree = shallow(<HeaderMenu
    navLinks={navLinks}
    onNavigationClick={navigationHandler}
    currentLocation="spotpromotiondetails"
  />);
  const event = {
    preventDefault: jest.fn(),
  };
  it('should be defined', () => {
    expect(HeaderMenu).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should navigate from SpotPromotionDetails to WhatQualifies', () => {
    const simulateClick = tree.find('NavItem').at(0).prop('onClick');
    simulateClick(event);
    expect(navigationHandler).toBeCalled();
  });
  it('should navigate from OrderDetails to PointsHistory', () => {
    const newtree = shallow(<HeaderMenu
      navLinks={navLinks}
      onNavigationClick={navigationHandler}
      currentLocation="orderdetails"
    />);
    const simulateClick = newtree.find('NavItem').at(0).prop('onClick');
    simulateClick(event);
    expect(navigationHandler).toBeCalled();
  });
  it('should navigate from ClaimDetails to ClaimStatus', () => {
    const newtree = shallow(<HeaderMenu
      navLinks={navLinks}
      onNavigationClick={navigationHandler}
      currentLocation="claimdetails"
    />);
    const simulateClick = newtree.find('NavItem').at(0).prop('onClick');
    simulateClick(event);
    expect(navigationHandler).toBeCalled();
  });
  it('should navigate from Admin Page Plan Details to Contractor Management', () => {
    const newtree = shallow(<HeaderMenu
      navLinks={navLinks}
      onNavigationClick={navigationHandler}
      currentLocation="/admin/plandetails"
    />);
    const simulateClick = newtree.find('NavItem').at(0).prop('onClick');
    simulateClick(event);
    expect(navigationHandler).toBeCalled();
  });
  it('should navigate from Admin Page Edit Spot Promotion to Spot Promotion', () => {
    const newtree = shallow(<HeaderMenu
      navLinks={navLinks}
      onNavigationClick={navigationHandler}
      currentLocation="/admin/editspotpromotion/"
    />);
    const simulateClick = newtree.find('NavItem').at(0).prop('onClick');
    simulateClick(event);
    expect(navigationHandler).toBeCalled();
  });
});

describe('HeaderMenu without navlinks', () => {
  const tree = shallow(<HeaderMenu />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render NavItem', () => {
    expect(tree.find('NavItem').length).toBe(0);
  });
});
