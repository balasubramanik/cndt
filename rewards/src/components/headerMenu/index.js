import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import RouteConstants from '../../constants/RouteConstants';

/** @description Function to render the navigation links based on Role */
const renderNavigation = ({ navLinks, onNavigationClick, currentLocation }) => navLinks.map((link, i) => {
  let selectedUrl = currentLocation;
  if (/spotpromotiondetails/.test(selectedUrl)) {
    selectedUrl = RouteConstants.WHAT_QUALIFIES;
  }
  if (/orderdetails/.test(selectedUrl)) {
    selectedUrl = RouteConstants.POINTS_HISTORY;
  }
  if (/claimdetails/.test(selectedUrl) || /contactus/.test(selectedUrl)) {
    selectedUrl = RouteConstants.CLAIM_STATUS;
  }
  if (new RegExp('/admin$').test(selectedUrl) ||
    /^admin$/.test(selectedUrl) ||
    new RegExp('/admin/plandetails').test(selectedUrl)) {
    selectedUrl = RouteConstants.CONTRACTORMANAGEMENT;
  }
  if (/admin\/editspotpromotion/.test(selectedUrl)) {
    selectedUrl = RouteConstants.SPOTPROMOTION;
  }
  const activeMenu = classNames({ active: selectedUrl === link.url });
  return (
    <NavItem className={activeMenu} href="/" key={i.toString()} onClick={(event) => onNavigationClick(event, link)}>
      {link.title}
    </NavItem>
  );
});

/** @description Functional component to render the header navigation links */
const HeaderMenu = (props) => (
  <section className="navbar-section">
    {props.navLinks && props.onNavigationClick &&
      <div className="header-menu">
        <Navbar collapseOnSelect>
          <Navbar.Header>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav>
              {renderNavigation(props)}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>}
  </section>
);

HeaderMenu.propTypes = {
  navLinks: PropTypes.array.isRequired,
  onNavigationClick: PropTypes.func.isRequired,
};

export default HeaderMenu;
