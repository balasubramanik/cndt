
import React, { PureComponent } from 'react';
import PropsTypes from 'prop-types';
import Loader from '../loader';

export default class AsyncComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      Component: null,
    };
  }

  componentWillMount() {
    if (!this.state.Component) {
      this.setComponentState();
    }
  }

  setComponentState = () => {
    const { moduleProvider } = this.props;
    if (moduleProvider) {
      this.props.moduleProvider().then((response) => {
        this.setState({ Component: response });
      });
    }
  }

  render() {
    const { Component } = this.state;
    return (
      <div>
        {Component ? <Component /> : <Loader />}
      </div>
    );
  }
}

AsyncComponent.propTypes = {
  moduleProvider: PropsTypes.any,
};
