const company = {
  COMPANY: 'Company',
  DOIND_BUSINESS_AS_DBA: 'Doing Business As - DBA',
};

export { company };
