import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Company from '../../components/company';


configure({ adapter: new Adapter() });

describe('Company', () => {
  const tree = shallow(<Company />);

  it('should be defined', () => {
    expect(Company).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render company field', () => {
    expect(tree.find('Field[name="company"]').length).toBe(1);
  });

  it('should render astreik symbol in company field', () => {
    const label = tree.find('Field[name="company"]').prop('label');
    expect(label).toBe('Company<sup>*</sup>');
  });

  it('should render DBA field', () => {
    expect(tree.find('Field[name="dba"]').length).toBe(1);
  });
});
