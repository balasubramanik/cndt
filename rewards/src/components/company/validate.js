import { errorMessages } from '../../containers/ems/Constants';

const validate = (values) => {
  const errors = {};
  if (!values.company && values.isCompany) {
    errors.company = errorMessages.REQUIRED_FIELD;
  }
  return errors;
};

export { validate };
