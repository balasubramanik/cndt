import React from 'react';
import { Col } from 'react-bootstrap';
import { Field } from 'redux-form';
import TextInput from '../textInput';
import { company } from './constants';

/** @description Functional component to render the address */
const Company = () => (
  <React.Fragment>
    <Col md={6}>
      <Field
        id="company"
        name="company"
        className="form-group gaf-form"
        component={TextInput}
        label={`${company.COMPANY}<sup>*</sup>`}
        maxLength={40}
        type="text"
      />
    </Col>
    <Col md={12}>
      <Field
        id="dba"
        name="dba"
        className="form-group gaf-form"
        component={TextInput}
        label={company.DOIND_BUSINESS_AS_DBA}
        maxLength={40}
        type="text"
      />
    </Col>
  </React.Fragment>
);

export default Company;
