import { validate } from './validate';
import { errorMessages } from '../../containers/ems/Constants';

describe('validate', () => {
  it('should be equal to invalid company', () => {
    const errors = validate({ isCompany: true });
    expect(errors.company).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to valid company', () => {
    const errors = validate({ isCompany: true, company: 'gaf' });
    expect(errors.company).toBe(undefined);
  });
});
