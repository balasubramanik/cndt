import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ToolTip from '../../components/toolTip';


configure({ adapter: new Adapter() });

describe('ToolTip', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<ToolTip
    id="tooltip"
    text="sample text"
  >
    <i className="icon-question" />
  </ToolTip>);
  it('should be defined', () => {
    expect(ToolTip).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call open tooltip handler', () => {
    const simulateClick = tree.find('a').prop('onMouseEnter');
    simulateClick();
    expect(tree.state().isOpen).toBe(true);
  });

  it('should call close tooltip handler', () => {
    const simulateClick = tree.find('a').prop('onMouseLeave');
    simulateClick();
    expect(tree.state().isOpen).toBe(false);
  });

  it('should call prevent default', () => {
    tree.setState({ isOpen: true });
    const simulateClick = tree.find('a').prop('onClick');
    simulateClick(event);
    expect(tree.state().isOpen).toBe(true);
  });
});

describe('ToolTip with show false', () => {
  const tree = shallow(<ToolTip
    id="tooltip"
    show={false}
    text="sample text"
  >
    <i className="icon-question" />
  </ToolTip>);
  it('should be defined', () => {
    expect(ToolTip).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
