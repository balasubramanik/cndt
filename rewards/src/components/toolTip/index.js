import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'react-tippy';


/** @description Functional component to render the TopHeader section */
class ToolTip extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  /** @description callback function for open tooltip */
  openToolTipHandler = () => this.setState({
    isOpen: true,
  });

  /** @description callback function for close tooltip */
  closeToolTipHandler = () => this.setState({
    isOpen: false,
  });

  render() {
    const { id, text, placement } = this.props;
    const { isOpen } = this.state;
    return (
      <Tooltip
        arrow
        id={id}
        title={text}
        open={isOpen}
        placement={placement}
      >
        <a
          href="/"
          onMouseEnter={this.openToolTipHandler}
          onMouseLeave={this.closeToolTipHandler}
          onClick={(e) => { e.preventDefault(); }}
        >
          {this.props.children}
        </a>
      </Tooltip >
    );
  }
}

/** PropTypes:
 * id - string - identifier for tooltip
 * text - string - content of tooltip
 * placement - string - sets the direction the Tooltip is positioned towards.
 */
ToolTip.propTypes = {
  children: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  placement: PropTypes.string,
};

ToolTip.defaultProps = {
  placement: 'top',
};

export default ToolTip;
