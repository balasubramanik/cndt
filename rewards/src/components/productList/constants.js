const tableHeaders = {
  ProductListHeader: [
    { title: 'TYPE', key: 'cardType', sort: true },
    { title: 'ITEM', key: 'productName', sort: true },
    { title: 'QTY', key: 'quantity', sort: true },
    { title: 'VALUE', key: 'cardValue', sort: true },
    { title: 'FEE', key: 'serviceFeeRate', sort: true },
    { title: 'TOTAL POINTS', key: 'amountRedeemed', sort: true },
    { title: 'STATUS', key: 'itemStatus', sort: true },
    { title: 'UPDATED DATE', key: 'modifiedOn', sort: true },
  ],
  countPerPage: [
    { title: '25', value: '25' },
    { title: '50', value: '50' },
    { title: '75', value: '75' },
    { title: '100', value: '100' },
  ],
  text: {
    Tabletitle: 'Item(s) ordered', Rows: 'rows per page', Export: 'Export', filename: 'OrderDetails.csv',
  },

  csvConfig: [
    { colName: 'TYPE', key: 'cardType' },
    { colName: 'ITEM', key: 'productName' },
    { colName: 'QTY', key: 'quantity' },
    { colName: 'VALUE', key: 'cardValue' },
    { colName: 'FEE', key: 'serviceFeeRate' },
    { colName: 'TOTAL POINTS', key: 'amountRedeemed' },
    { colName: 'STATUS', key: 'itemStatus' },
    { colName: 'UPDATED DATE', key: 'modifiedOn' },
  ],
};

export { tableHeaders };
