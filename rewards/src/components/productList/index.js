import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import Tables from '../../components/tables';
import CSV from '../../components/csv';
import { tableHeaders } from './constants';

class ProductList extends React.Component {
  formatProductList = () => {
    const { data } = this.props;
    if (data) {
      return data.map((item) => {
        const {
          cardType,
        } = item;
        return {
          ...item,
          cardType: cardType || 'N/A',
        };
      });
    }
    return [];
  }
  tableHeadersContent = () => (
    <Row className="show-grid">
      <Col xs={8} sm={4} md={4} lg={6}>
        <h4>{tableHeaders.text.Tabletitle}</h4>
      </Col>
      <Col xs={4} sm={8} md={8} lg={6}>
        {this.renderCSVLink()}
      </Col>
    </Row>);


  renderCSVLink = () => {
    const CSVRecords = this.formatProductList() || [];
    return (
      <div className="gaf-export-block">
        <CSV
          filename={tableHeaders.text.filename}
          config={tableHeaders.csvConfig}
          data={CSVRecords}
          onChage={this.props.CSVHandler}
        >
          <div className="pull-right">
            <span><i className="icon-Export-Icon"></i><span>{tableHeaders.text.Export}</span></span>
          </div>
        </CSV>
      </div>
    );
  }

  render() {
    const { onSort, isloading } = this.props;
    return (
      <div>
        <div className="gaf-table-header">
          {this.tableHeadersContent()}
        </div>
        <div className="order-details-table">
          <Tables config={tableHeaders.ProductListHeader} data={this.formatProductList()} isLoading={isloading} onSortingClick={onSort} />
          {/* {listOfTransactions} */}
        </div>
      </div>
    );
  }
}

ProductList.propTypes = {
  data: PropTypes.array,
  onSort: PropTypes.func,
  isloading: PropTypes.bool,
  CSVHandler: PropTypes.func,
};

export default ProductList;

