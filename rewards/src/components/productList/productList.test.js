import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ProductList from '../../components/productList';

configure({ adapter: new Adapter() });

describe('ProductList with props', () => {
  const sortingHandler = jest.fn();
  const csvHandler = jest.fn();
  const refHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<ProductList
    data={[{
      firstName: 'Jude',
      lastName: 'Law',
      orderId: 'O00000215',
      orderDate: '2018-11-29T09:10:56.6173327Z',
      serviceFeeType: '$',
      serviceFeeRate: '0.0',
      modifiedOn: '2018-11-29T09:10:56.6173327Z',
      cardValue: '5000',
      amountRedeemed: '2000',
    },
    {
      firstName: 'Calvin',
      lastName: 'Klein',
      orderId: 'O00000125',
      orderDate: '2018-11-29T09:10:56.6173327Z',
      serviceFeeType: '$',
      serviceFeeRate: '0.2',
      modifiedOn: '2018-11-29T09:10:56.6173327Z',
      cardValue: '6500',
      amountRedeemed: '5000',
    },
    {
      firstName: 'Shane',
      lastName: 'Warne',
      orderId: 'O00000105',
      orderDate: '2018-11-29T09:10:56.6173327Z',
      serviceFeeType: '$',
      serviceFeeRate: '0.5',
      modifiedOn: '2018-11-29T09:10:56.6173327Z',
      cardValue: '4000',
      amountRedeemed: '3000',
    },
    {
      firstName: '',
      lastName: '',
      orderId: '',
      orderDate: '',
      serviceFeeType: '',
      modifiedOn: '',
      cardValue: '',
      amountRedeemed: '',
    },
    ]}
    onSort={sortingHandler}
    exportCsvData={[{
      firstName: 'Jude',
      lastName: 'Law',
      orderId: 'O00000215',
      orderDate: '2018-11-29T09:10:56.6173327Z',
      serviceFeeType: '$',
      serviceFeeRate: '0.0',
      modifiedOn: '2018-11-29T09:10:56.6173327Z',
      cardValue: '5000',
      amountRedeemed: '2000',
    },
    {
      firstName: 'Calvin',
      lastName: 'Klein',
      orderId: 'O00000125',
      orderDate: '2018-11-29T09:10:56.6173327Z',
      serviceFeeType: '$',
      serviceFeeRate: '0.2',
      modifiedOn: '2018-11-29T09:10:56.6173327Z',
      cardValue: '6500',
      amountRedeemed: '5000',
    },
    {
      firstName: 'Shane',
      lastName: 'Warne',
      orderId: 'O00000105',
      orderDate: '2018-11-29T09:10:56.6173327Z',
      serviceFeeType: '$',
      serviceFeeRate: '0.5',
      modifiedOn: '2018-11-29T09:10:56.6173327Z',
      cardValue: '4000',
      amountRedeemed: '3000',
    },
    {
      firstName: '',
      lastName: '',
      orderId: '',
      orderDate: '',
      serviceFeeType: '',
      modifiedOn: '',
      cardValue: '',
      amountRedeemed: '',
    },
    ]}
    modifiedOn="2018-11-29T09:10:56.6173337Z"
    isLoading
    onRef={refHandler}
    CSVHandler={csvHandler}
  />);


  it('should be defined', () => {
    expect(ProductList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Tables Component', () => {
    expect(tree.find('Tables').length).toBe(1);
  });
  it('should call CSVHandler', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(csvHandler).toBeCalled();
  });
});

describe('ProductList without props', () => {
  const tree = shallow(<ProductList
    modifiedOn=""
  />);

  it('should be defined', () => {
    expect(ProductList).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render ProductList List Component', () => {
    expect(tree.find('ProductList').length).toBe(0);
  });
});
