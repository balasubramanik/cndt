import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { loadDynamicReduxForm } from '../loadDynamicReduxForm';
import GiftCardRedemption from '../giftCardRedemptionForm';


configure({ adapter: new Adapter() });
describe('Loader with urls', () => {
  const AyncFunction = loadDynamicReduxForm(GiftCardRedemption);
  const tree = shallow(<AyncFunction />);

  it('should be defined', () => {
    expect(loadDynamicReduxForm).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render correctly', () => {
    expect(GiftCardRedemption).toBeDefined();
  });
});

