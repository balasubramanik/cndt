import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';

/** @description Highorder component to load dynamic redux form */
export function loadDynamicReduxForm(WrappedComponent, validate) {
  class Form extends PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        Component: null,
      };
    }

    /** @description React Life cycle method
     * it will invoke, when component is mounted.
     */
    componentDidMount() {
      const { formName } = this.props;
      this.updateComponentState(reduxForm({
        form: formName,
        validate,
      })(WrappedComponent));
    }

    /** @description Function to update the component state
     * @param {object} component
     */
    updateComponentState = (component) => this.setState({ Component: component });

    render() {
      const { Component } = this.state;
      return Component ? <Component {...this.props} /> : null;
    }
  }

  /** PropTypes:
   * formName - string - unique identifier for redux form
  */
  Form.propTypes = {
    formName: PropTypes.string,
  };

  return Form;
}
