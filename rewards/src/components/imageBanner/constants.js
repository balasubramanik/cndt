const promotions = {
  START_DATE: 'Start Date',
  END_DATE: 'End Date',
  MORE_DETAILS: 'more',
  HEADER_DESC_LENGTH: 50,
  SUBMISSIONS_DUE_DATE: 'Submissions Due By',
  STATE: 'State(s)',
  VIEW_ELIGIBLE_PRODUCTS: 'View Eligible Products',
  ELIGIBLE_STATES: 'Eligible States',
  DEFAULT_IMAGE_URL: 'images/banner-image.jpg',
  STATE_MAX_LENGTH: 5,
};

export { promotions };
