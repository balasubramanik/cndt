import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Col, Row } from 'react-bootstrap';
import _truncate from 'lodash/truncate';
import { promotions } from './constants';
import { formatDateUTC } from '../../utils/utils';
import RouteConstants from '../../constants/RouteConstants';
import { authorization, history } from '../../routes';
import Dialog from '../dialog';
import List from '../list';
import ImageHOC from '../imageHOC';

/** @description Class component to render the Image Banner */
export class ImageBanner extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isShowPopup: false,
    };
  }

  /** @description Function to redirect to the Spot Promotions page */
  redirectToSpotPromotions = (e) => {
    e.preventDefault();
    const { promotionsInfo, selectedYear } = this.props;
    const { id } = promotionsInfo;
    if (authorization.hasAdminAccess) {
      history.goBack(-1);
    } else {
      history.push({ pathname: RouteConstants.WHAT_QUALIFIES });
    }
    this.props.updatePlanId(id);
    this.props.updateSelectedYear(selectedYear);
    this.props.gtmActions.gtmViewEligibleProducts(promotionsInfo.title);
  };

  /** @description Function to Open the Dialog for the eligible states */
  openPopUp = (e) => {
    const { promotionsInfo } = this.props;
    e.preventDefault();
    this.setState({ isShowPopup: true });
    this.props.gtmActions.gtmStateMore(promotionsInfo.title);
  }
  /** @description Function to close the Dialog */
  closePopUp = (e) => {
    e.preventDefault();
    this.setState({ isShowPopup: false });
  }

  /** @description Function to render the Promotion Details */
  renderPromotionDetails = () => {
    const { isShowPopup } = this.state;
    const { isLoading, promotionsInfo } = this.props;
    const {
      startDate, endDate, states, title, image, isAllStatesApplicable,
    } = promotionsInfo;
    const imgpath = image || (!isLoading && promotions.DEFAULT_IMAGE_URL);
    const size = states ? Object.keys(states).length : null;
    const applicableStates = states ? states.slice(0, promotions.STATE_MAX_LENGTH) : [];
    return (
      <div>
        <div className="spot-promotion-title">
          <ImageHOC isAbsolute={image} src={imgpath} alt={title} />
          {title && title.length >= promotions.HEADER_DESC_LENGTH ?
            <div className="banner-image-header">{_truncate(title, { length: promotions.HEADER_DESC_LENGTH })}</div> : <div className="banner-image-header">{title}</div>}
        </div>
        <div className="banner-property-detail">
          <Row>
            <Col md={7} xs={12}>
              <div className="banner-property-detail-list">
                <div className="banner-detail-item">
                  <span>
                    {promotions.START_DATE}
                  </span>
                  <span><strong>{formatDateUTC(startDate)}</strong></span>
                </div>
                <div className="banner-detail-item">
                  <span>
                    {promotions.END_DATE}
                  </span>
                  <span><strong>{formatDateUTC(endDate)}</strong></span>
                </div>
                <div className="banner-detail-item">
                  <span>
                    {promotions.STATE}
                  </span>
                  {
                    isAllStatesApplicable ? <span>All States</span> :
                      applicableStates && <span><strong>{applicableStates.map((item) => (item.code)).join(',')}</strong></span>}

                  {
                    (size > promotions.STATE_MAX_LENGTH && !isAllStatesApplicable) ?
                      <div className="last-item">
                        <a href="/" onClick={this.openPopUp}>{promotions.MORE_DETAILS}</a>
                      </div> : null
                  }
                </div>
              </div>
            </Col>
            <Col md={3} mdOffset={2} xs={12}>
              <div className="eligible-products">
                <a href="/" onClick={this.redirectToSpotPromotions}>{promotions.VIEW_ELIGIBLE_PRODUCTS}</a>
              </div>
            </Col>
          </Row>
        </div>
        <Dialog className="gaf-model-popup" show={isShowPopup} title={promotions.ELIGIBLE_STATES} onCloseClick={this.closePopUp} body={<List data={states} />} />
      </div>
    );
  }
  render() {
    return (
      <div className="banner-image">
        {this.renderPromotionDetails()}
      </div>
    );
  }
}

/** PropTypes:
 * startDate - startDate for the ImageBanner component
 * endDate - endDate for the ImageBanner component
 * submissionsDueDate - submissionsDueDate for the ImageBanner component
 * state - string - state available for spot promotion
 * headerDesc - string - Header for the image
 * image - string - image url for the image banner component
 * promotionsInfo - object - Props information for the Imagebanner component
 * location - Object - Location
 */

ImageBanner.propTypes = {
  promotionsInfo: PropTypes.object,
  updatePlanId: PropTypes.func,
  updateSelectedYear: PropTypes.func,
  selectedYear: PropTypes.number,
  gtmActions: PropTypes.func,
  isLoading: PropTypes.bool,
};

export default withRouter(ImageBanner);
