import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import { ImageBanner } from '../imageBanner';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';

history.push = jest.fn();
configure({ adapter: new Adapter() });

describe('ImageBanner', () => {
  const gtmStateMore = jest.fn();
  const gtmViewEligibleProducts = jest.fn();
  const planIdHandler = jest.fn();
  const yearHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<ImageBanner
    promotionsInfo={{
      planId: 'SP039',
      selectedYear: '2018',
      image: 'https://rewardsqauseblob.blob.core.windows.net/rewards-static/SpotPromotion/2cf9906e-2e5b-4943-8e5b-a4a52417d821_IMG_0292.PNG',
      title: 'BayerSPRoofings',
      description: 'Bayer Experimental Science for enabling sciences to advance your work',
      startDate: '2018-11-22T00:00:00Z',
      endDate: '2018-11-30T00:00:00Z',
      submissionDueDate: '2019-01-29T00:00:00',
      isAllStatesApplicable: false,
      states: [{ code: 'AK', name: 'Alaska' }, { code: 'FL', name: 'Florida' }, { code: 'MO', name: 'Missouri' }, { code: 'NY', name: 'New York' }, { code: 'PR', name: 'Puerto Rico' }, { code: 'WA', name: 'Washington' }],
    }}
    updatePlanId={planIdHandler}
    updateSelectedYear={yearHandler}
    gtmActions={{ gtmViewEligibleProducts, gtmStateMore }}
  />);

  it('should be defined', () => {
    expect(ImageBanner).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should open the popup', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(tree.state().isShowPopup).toBe(true);
    expect(gtmStateMore).toBeCalled();
  });
  it('should close the popup', () => {
    tree.setState({ isShowPopup: true });
    const simulateClick = tree.find('.gaf-model-popup').prop('onCloseClick');
    simulateClick(event);
    expect(tree.state().isShowPopup).toBe(false);
  });
});

describe('ImageBanner mount', () => {
  const state = { isShowPopup: true };
  const store = fakeStore(state);
  const gtmStateMore = jest.fn();
  const gtmViewEligibleProducts = jest.fn();
  const planIdHandler = jest.fn();
  const yearHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <ImageBanner
        promotionsInfo={{
          planId: 'SP039',
          selectedYear: '2018',
          image: 'https://rewardsqauseblob.blob.core.windows.net/rewards-static/SpotPromotion/2cf9906e-2e5b-4943-8e5b-a4a52417d821_IMG_0292.PNG',
          title: 'BayerSPRoofings',
          description: 'Bayer Experimental Science for enabling sciences to advance your work',
          startDate: '2018-11-22T00:00:00Z',
          endDate: '2018-11-30T00:00:00Z',
          submissionDueDate: '2019-01-29T00:00:00',
          isAllStatesApplicable: false,
          states: [{ code: 'AK', name: 'Alaska' }, { code: 'FL', name: 'Florida' }, { code: 'MO', name: 'Missouri' }, { code: 'NY', name: 'New York' }, { code: 'PR', name: 'Puerto Rico' }, { code: 'WA', name: 'Washington' }],
        }}
        updatePlanId={planIdHandler}
        updateSelectedYear={yearHandler}
        gtmActions={{ gtmViewEligibleProducts, gtmStateMore }}
      />
    </Router>
  </Provider >);
  it('should redirect to spot promotions page', () => {
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(planIdHandler).toBeCalled();
    expect(yearHandler).toBeCalled();
    expect(gtmViewEligibleProducts).toBeCalled();
  });
});
describe('ImageBanner mount with all states applicable and no image', () => {
  const state = { isShowPopup: false };
  const store = fakeStore(state);
  const gtmStateMore = jest.fn();
  const gtmViewEligibleProducts = jest.fn();
  const planIdHandler = jest.fn();
  const yearHandler = jest.fn();
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <ImageBanner
        promotionsInfo={{
          planId: 'SP039',
          selectedYear: '2018',
          image: '',
          title: 'BayerSPRoofings',
          description: 'Bayer Experimental Science for enabling sciences to advance your work',
          startDate: '2018-11-22T00:00:00Z',
          endDate: '2018-11-30T00:00:00Z',
          submissionDueDate: '2019-01-29T00:00:00',
          isAllStatesApplicable: true,
          states: [{ code: 'AK', name: 'Alaska' }, { code: 'FL', name: 'Florida' }, { code: 'MO', name: 'Missouri' }, { code: 'NY', name: 'New York' }, { code: 'PR', name: 'Puerto Rico' }, { code: 'WA', name: 'Washington' }],
        }}
        updatePlanId={planIdHandler}
        updateSelectedYear={yearHandler}
        gtmActions={{ gtmViewEligibleProducts, gtmStateMore }}
      />
    </Router>
  </Provider >);
  it('should not render last item link', () => {
    expect(tree.find('.last-item').length).toBe(0);
  });
});
