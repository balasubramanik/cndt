import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import StepWizard from '../../components/stepWizard';


configure({ adapter: new Adapter() });

describe('StepWizard with step 1', () => {
  const tree = shallow(<StepWizard step="1" />);

  it('should be defined', () => {
    expect(StepWizard).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Desktop steps', () => {
    expect(tree.find('.hidden-xs').find('.ribbon').length).toBe(3);
  });

  it('should append Inprogress className for current step', () => {
    const className = tree.find('.hidden-xs').find('.ribbon').at(1).prop('className');
    expect(className.indexOf('in-progress') > -1).toBe(true);
  });

  it('should append Completed className for current step', () => {
    const className = tree.find('.hidden-xs').find('.ribbon').at(0).prop('className');
    expect(className.indexOf('completed') > -1).toBe(true);
  });

  it('should append YetToComplete className for current step', () => {
    const className = tree.find('.hidden-xs').find('.ribbon').at(2).prop('className');
    expect(className.indexOf('yet-to-complete') > -1).toBe(true);
  });

  it('should render Mobile steps', () => {
    expect(tree.find('.visible-xs').find('.ribbon').length).toBe(3);
  });
});
