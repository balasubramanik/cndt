import React from 'react';
import PropTypes from 'prop-types';
import { stepWizard } from './constants';

/** @description function to render the step wizard for mobile and table breakpoints
 * @param {string} stepName - name of the current step
 * @param {number} index - index of an element
 * @param {string} className - customizing the step
 */
const renderMobileWizard = (stepName, i, className) => (
  <div key={i.toString()} className={`ribbon ${className}`}>
    <svg viewBox="0 0 120 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
      <g>
        <path
          d="M0.934258546,39.5 L107.712815,39.5 L119.416858,19.4359252 L107.72114,0.5 L0.934258546,0.5 L13.9342585,20 L0.934258546,39.5 Z"
          id="Path-1"
          fillRule="nonzero"
        >
        </path>
      </g>
    </svg>
    <p>{stepName}</p>
  </div>
);

/** @description function to render the step wizard for desktop breakpoints
 * @param {string} stepName - name of the current step
 * @param {number} index - index of an element
 * @param {string} className - customizing the step
 */
const renderDesktopWizard = (stepName, i, className) => (
  <div key={i.toString()} className={`ribbon ${className}`}>
    <svg viewBox="0 0 240 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
      <g>
        <path d="M1.20710678,0.5 L20.7071068,20 L1.20710678,39.5 L219.792893,39.5 L239.292893,20 L219.792893,0.5 L1.20710678,0.5 Z" id="Rectangle-1"></path>
      </g>
    </svg>
    <p>{stepName}</p>
  </div>
);

/** @description function to find the progress of step
 * @param {number} stepNumber
 * @param {number} index
 */
const getProgress = (stepNumber, index) => {
  const { length } = stepWizard.steps;
  const isLastStep = (length - 1 === stepNumber);
  if (stepNumber < index) {
    return 'yet-to-complete';
  }
  if (stepNumber > index || isLastStep) {
    return 'completed';
  }
  return 'in-progress';
};

const StepWizard = ({ step }) => (
  <React.Fragment>
    {stepWizard.steps && step < stepWizard.steps.length &&
      <div>
        <div className="ribbon-block text-center visible-xs">
          {stepWizard.steps.map((stepName, i) => {
            const className = getProgress(step, i);
            return renderMobileWizard(stepName, i, className);
          })}
        </div>

        <div className="ribbon-block text-center hidden-xs">
          {stepWizard.steps.map((stepName, i) => {
            const className = getProgress(step, i);
            return renderDesktopWizard(stepName, i, className);
          })}
        </div>
      </div>}
  </React.Fragment>
);

/** PropTypes:
 * step - number - no of steps
 */
StepWizard.propTypes = {
  step: PropTypes.number,
};

StepWizard.defaultProps = {
  step: 0,
};

export default StepWizard;
