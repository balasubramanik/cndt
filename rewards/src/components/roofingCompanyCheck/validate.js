import { values } from './constants';
import { errorMessages } from '../../containers/ems/Constants';

const validate = (fields) => {
  const errors = {};
  if (!fields.isRoofingContractor) {
    errors.isRoofingContractor = errorMessages.REQUIRED_QUESTIONS;
  }

  if (fields.isRoofingContractor === values.YES) {
    if (!fields.installedOption) {
      errors.installedOption = errorMessages.REQUIRED_QUESTIONS;
    }
    if (fields.installedOption === values.COMPANY && !fields.authorizeeDesignation) {
      errors.authorizeeDesignation = errorMessages.REQUIRED_FIELD;
    }
  }

  if (fields.isRoofingContractor === values.NO) {
    if (!fields.contractorClassification) {
      errors.contractorClassification = errorMessages.REQUIRED_QUESTIONS;
    }
    if (fields.contractorClassification === values.OTHERS && !fields.description) {
      errors.description = errorMessages.REQUIRED_FIELD;
    }
  }

  return errors;
};

export { validate };
