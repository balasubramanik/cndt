import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RoofingCompanyCheck from '../../components/roofingCompanyCheck';


configure({ adapter: new Adapter() });

describe('RoofingCompanyCheck with { isRoofingContractor: yes, installedOption: remodeler }', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const changeHandler = jest.fn();
  const tree = shallow(<RoofingCompanyCheck
    options={{
      isRoofingContractor: 'yes',
      installedOption: 'remoduler',
    }}
    onChange={changeHandler}
  />);
  it('should be defined', () => {
    expect(RoofingCompanyCheck).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render roofOption', () => {
    expect(tree.find('Field[name="isRoofingContractor"]').length).toBe(1);
  });

  it('should render yes options', () => {
    expect(tree.find('.install-yes').length).toBe(1);
  });

  it('should render Changehandler for Choices', () => {
    const e = {
      ...event,
      target: {
        value: 'no',
      },
    };
    const simulateClick = tree.find('Field[name="isRoofingContractor"]').prop('onChange');
    simulateClick(e);
    expect(changeHandler).toBeCalled();
  });

  it('should render installedOption', () => {
    expect(tree.find('Field[name="installedOption"]').length).toBe(2);
  });

  it('should render Changehandler for installed options', () => {
    const e = {
      ...event,
      target: {
        value: 'remodeler',
      },
    };
    const simulateClick = tree.find('Field[name="installedOption"]').at(0).prop('onChange');
    simulateClick(e);
    expect(changeHandler).toBeCalled();
  });

  it('should render TextArea', () => {
    expect(tree.find('Field[name="description"]').length).toBe(1);
    const simulateClick = tree.find('Field[name="description"]').prop('component');
    simulateClick({ meta: { touched: true, error: true } });
  });
});


describe('RoofingCompanyCheck with { isRoofingContractor: yes, installedOption: professional }', () => {
  const changeHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<RoofingCompanyCheck
    options={{
      isRoofingContractor: 'yes',
      installedOption: 'company',
    }}
    onChange={changeHandler}
  />);
  it('should be defined', () => {
    expect(RoofingCompanyCheck).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render roofOption', () => {
    expect(tree.find('Field[name="isRoofingContractor"]').length).toBe(1);
  });

  it('should render yes options', () => {
    expect(tree.find('.install-yes').length).toBe(1);
  });

  it('should render installedOption', () => {
    expect(tree.find('Field[name="installedOption"]').length).toBe(2);
  });

  it('should render Changehandler for installed options', () => {
    const e = {
      ...event,
      target: {
        value: 'remodeler',
      },
    };
    const simulateClick = tree.find('Field[name="installedOption"]').at(1).prop('onChange');
    simulateClick(e);
    expect(changeHandler).toBeCalled();
  });

  it('should render Professional category', () => {
    expect(tree.find('.ems-select-box').length).toBe(1);
  });
});

describe('RoofingCompanyCheck with { isRoofingContractor: no, contractorClassification: homeowner }', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const changeHandler = jest.fn();
  const tree = shallow(<RoofingCompanyCheck
    options={{
      isRoofingContractor: 'no',
      contractorClassification: 'homeowner',
    }}
    onChange={changeHandler}
  />);
  it('should be defined', () => {
    expect(RoofingCompanyCheck).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render roofOption', () => {
    expect(tree.find('Field[name="isRoofingContractor"]').length).toBe(1);
  });

  it('should render not installed options', () => {
    expect(tree.find('Field[name="contractorClassification"]').length).toBe(1);
  });

  it('should render Changehandler for Choices', () => {
    const e = {
      ...event,
      target: {
        value: 'homeowner',
      },
    };
    const simulateClick = tree.find('Field[name="contractorClassification"]').prop('onChange');
    simulateClick(e);
    expect(changeHandler).toBeCalled();
  });
});

describe('RoofingCompanyCheck with { isRoofingContractor: no, contractorClassification: others }', () => {
  const changeHandler = jest.fn();
  const tree = shallow(<RoofingCompanyCheck
    options={{
      isRoofingContractor: 'no',
      contractorClassification: 'others',
    }}
    onChange={changeHandler}
  />);
  it('should be defined', () => {
    expect(RoofingCompanyCheck).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render roofOption', () => {
    expect(tree.find('Field[name="isRoofingContractor"]').length).toBe(1);
  });

  it('should render not installed options', () => {
    expect(tree.find('Field[name="contractorClassification"]').length).toBe(1);
  });

  it('should render other description', () => {
    expect(tree.find('.others-decs').length).toBe(1);
  });
});
