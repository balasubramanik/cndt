import { validate } from './validate';
import { values } from './constants';
import { errorMessages } from '../../containers/ems/Constants';

describe('validate', () => {
  it('should be equal to invalid roof option', () => {
    const errors = validate({});
    expect(errors.isRoofingContractor).toBe(errorMessages.REQUIRED_QUESTIONS);
  });

  it('should be equal to invalid installed options', () => {
    const errors = validate({
      isRoofingContractor: values.YES,
    });
    expect(errors.installedOption).toBe(errorMessages.REQUIRED_QUESTIONS);
  });

  it('should be equal to valid installed options', () => {
    const errors = validate({
      isRoofingContractor: values.YES,
      installedOption: values.COMPANY,
    });
    expect(errors.installedOption).toBe(undefined);
  });

  it('should be equal to invalid authorizeeDesignation', () => {
    const errors = validate({
      isRoofingContractor: values.YES,
      installedOption: values.COMPANY,
    });
    expect(errors.authorizeeDesignation).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to valid authorizeeDesignation', () => {
    const errors = validate({
      isRoofingContractor: values.YES,
      installedOption: values.COMPANY,
      authorizeeDesignation: 'foreman',
    });
    expect(errors.authorizeeDesignation).toBe(undefined);
  });

  it('should be equal to invalid contractorClassification', () => {
    const errors = validate({
      isRoofingContractor: values.NO,
    });
    expect(errors.contractorClassification).toBe(errorMessages.REQUIRED_QUESTIONS);
  });

  it('should be equal to valid contractorClassification', () => {
    const errors = validate({
      isRoofingContractor: values.NO,
      contractorClassification: values.HOMEOWNER,
    });
    expect(errors.contractorClassification).toBe(undefined);
  });

  it('should be equal to invalid description', () => {
    const errors = validate({
      isRoofingContractor: values.NO,
      contractorClassification: values.OTHERS,
    });
    expect(errors.description).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to valid description', () => {
    const errors = validate({
      isRoofingContractor: values.NO,
      contractorClassification: values.OTHERS,
      description: 'other',
    });
    expect(errors.description).toBe(undefined);
  });
});
