import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Col } from 'react-bootstrap';
import { Field } from 'redux-form';
import Radio from '../radio';
import Dropdown from '../dropdown';
import * as EMAIL from '../../actionTypes/EmailVerification';
import { constants, values, roofOptions } from './constants';
import { trim } from '../../utils/utils';

const isShowError = true;

/** @description callback function for radiobutton
   * @param {object} event - triggered event(change)
   * @param {string} key - state to update.
   */
const changeHandler = (event, type, callback) => {
  const { value } = event.target;
  callback(type, value);
};

/** @description function to render the options
 * it will render only, if user selects yes in roofing company check
 * @param {string} option - selected value of eligible roof option
 * @param {function} callback - callback function for change the radio button
 */
const renderCompanyOption = (option, callback) => (
  <Field
    name="installedOption"
    type="radio"
    className="gaf-install-options"
    component={Radio}
    showError={isShowError}
    options={[roofOptions.installedOptions[0]]}
    value={option}
    onChange={(event) => changeHandler(event, EMAIL.CHANGE_INSTALL_ROOF_OPTIONS, callback)}
  />
);

/** @description function to render the options
 * it will render only, if user selects yes in roofing company check
 * @param {string} option - selected value of eligible roof option
 * @param {function} callback - callback function for change the radio button
 */
const renderIndividualOption = (option, callback) => (
  <Field
    name="installedOption"
    type="radio"
    className="gaf-install-options"
    component={Radio}
    options={[roofOptions.installedOptions[1]]}
    value={option}
    onChange={(event) => changeHandler(event, EMAIL.CHANGE_INSTALL_ROOF_OPTIONS, callback)}
  />
);

/** @description function to render the options
 * it will render only, if user selects no in roofing company check
 * @param {string} option - selected value of not-eligible roof option
 * @param {function} callback - callback function for change the radio button
 */
const renderNotInstalledOptions = (option, callback) => (
  <Field
    name="contractorClassification"
    type="radio"
    className="col-md-6 col-lg-4"
    component={Radio}
    showError={isShowError}
    options={roofOptions.notInstalledOptions}
    value={option}
    onChange={(event) => changeHandler(event, null, callback)}
  />
);

/** @description function to render the catergories
 * @param {string} value - selected value of professional category
 */
const renderProfessionalCategory = (value) => (
  <div className="ems-select-box">
    <Field
      name="authorizeeDesignation"
      id="authorizeeDesignation"
      component={Dropdown}
      menuItems={roofOptions.professionalCategory}
      value={value}
    />
  </div>
);

/** @description function to render the text area
 * @param {object} obj - property comes from redux-form
 */
const renderTextArea = (obj) => {
  const { meta } = obj;
  const { error, touched } = meta;
  const isInvalidField = touched && error;
  const textAreaClass = classNames({ 'input-error': isInvalidField });
  return (
    <div>
      <textarea
        className={textAreaClass}
        {...obj.input}
        maxLength={120}
        onBlur={(event) => {
          const { target } = event;
          const { value } = target;
          const trimmedValue = trim(value);
          obj.input.onBlur(trimmedValue);
        }}
      />
      {isInvalidField && <p><span className="error-message">{error}</span></p>}
    </div>
  );
};

/** @description function to render the catergory classification
 * it will render only, if user selects no in roofing company check
 * @param {string} option - selected value of not-eligible roof option
 * @param {function} callback - callback function for change the radio button
 */
const renderCategoryClassification = (option, callback) => {
  const categoryClass = classNames('gaf-radio others-decs', { hidden: option !== values.OTHERS });
  return (
    <div className="gaf-radio install-no ems-classify clearfix">
      <Col md={12}>
        <p className="classify-yourself">{constants.CLASSIFY_YOURSELF}<sup>*</sup></p>
      </Col>
      {renderNotInstalledOptions(option, callback)}
      <div className={categoryClass}>
        <div className="open other-desc-textarea">
          <Field
            name="description"
            id="description"
            component={renderTextArea}
          />
        </div>
      </div>
    </div >
  );
};

/** @description function to render the options for roofing company check
 * @param {string} option - selected value of roof option(yes, no)
 * @param {function} callback - callback function for change the radio button
 */
const renderChoices = (option, callback) => (
  <Col md={6}>
    <div className="gaf-radio radio-total-inline row error-question">
      <Field
        name="isRoofingContractor"
        type="radio"
        className="gaf-install-options col-md-3"
        component={Radio}
        showError={isShowError}
        options={roofOptions.choices}
        value={option}
        onChange={(event) => changeHandler(event, EMAIL.CHANGE_ROOF_OPTIONS, callback)}
      />
    </div>
  </Col>
);

/** @description Functional Component to render the Name */
const RoofingCompanyCheck = (props) => {
  const { options, onChange } = props;
  const {
    isRoofingContractor, installedOption, contractorClassification, authorizeeDesignation,
  } = options;
  const roofingContractorClass = classNames({ hidden: isRoofingContractor !== values.YES });
  const nonRoofingContractorClass = classNames('error-question', { hidden: isRoofingContractor !== values.NO });
  return (
    <React.Fragment>
      <Col md={12}>
        <p className="install-roofs">{constants.CONFIRMATION_QUESTION}<sup>*</sup></p>
      </Col>
      <div className="install-yes-no yes-open no-open">
        {renderChoices(isRoofingContractor, onChange)}
        <Col md={12} className={roofingContractorClass}>
          <div className="gaf-radio install-yes error-question">
            {renderCompanyOption(installedOption, onChange)}
            {installedOption === values.COMPANY &&
              renderProfessionalCategory(authorizeeDesignation)}
            {renderIndividualOption(installedOption, onChange)}
          </div>
        </Col>
        <div className={nonRoofingContractorClass}>
          {renderCategoryClassification(contractorClassification, onChange)}
        </div>
      </div>
    </React.Fragment>
  );
};

/** PropTypes:
 * onChange - func - callback function for change the radio button
 * options - object - contains the information about roofing company check
 */
RoofingCompanyCheck.propTypes = {
  onChange: PropTypes.func.isRequired,
  options: PropTypes.shape({
    isRoofingContractor: PropTypes.string,
    installedOption: PropTypes.string,
    contractorClassification: PropTypes.string,
  }),
};

RoofingCompanyCheck.defaultProps = {
  options: {},
};

export default RoofingCompanyCheck;
