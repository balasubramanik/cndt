const constants = {
  CONFIRMATION_QUESTION: 'Do you or does your company install roofs?',
  REMODELER: 'remodeler',
  CLASSIFY_YOURSELF: 'How would you classify yourself or your business?',
  TERMS_AND_CONDITIONS: 'I certify that I am an authorized person to sign the Terms & Conditions on behalf of my company.',
};

const values = {
  YES: 'yes',
  NO: 'no',
  COMPANY: 'company',
  INDIVIDUAL: 'individual',
  PROFESSIONAL: 'professional',
  HOMEOWNER: 'Homeowner',
  BUILDING_OWNER: 'Building Owner',
  DISTRIBUTOR: 'Distributor',
  ARCHITECT: 'Architect',
  ROOFING_CONSULTANT: 'Roofing Consultant',
  REALESTATE_PROFESSIONAL: 'Real Estate Professional',
  PROPERTY_MANAGER: 'Property Manager',
  OTHERS: 'others',
};

const roofOptions = {
  choices: [
    {
      title: 'Yes',
      value: values.YES,
    },
    {
      title: 'No',
      value: values.NO,
    },
  ],
  installedOptions: [
    {
      title: 'I am registering a company for GAF Rewards',
      value: values.COMPANY,
      tooltipText: 'Register as a company if you are submitting invoices on behalf of a company.',
    },
    {
      title: 'I am registering as an individual for GAF Rewards',
      value: values.INDIVIDUAL,
      tooltipText: 'Register as an individual if you are submitting a personal invoice.',
    },
  ],
  notInstalledOptions: [
    {
      title: 'Homeowner',
      value: values.HOMEOWNER,
    },
    {
      title: 'Building Owner',
      value: values.BUILDING_OWNER,
    },
    {
      title: 'Distributor',
      value: values.DISTRIBUTOR,
    },
    {
      title: 'Architect',
      value: values.ARCHITECT,
    },
    {
      title: 'Roofing Consultant',
      value: values.ROOFING_CONSULTANT,
    },
    {
      title: 'Property Manager',
      value: values.PROPERTY_MANAGER,
    },
    {
      title: 'Real Estate Professional',
      value: values.REALESTATE_PROFESSIONAL,
    },
    {
      title: 'Other (Please describe below)',
      value: values.OTHERS,
    },
  ],
  professionalCategory: [
    {
      label: 'Owner',
      value: 'Owner',
    },
    {
      label: 'Office Staff',
      value: 'Office Staff',
    },
    {
      label: 'Foreman',
      value: 'Foreman',
    },
    {
      label: 'Installer/Crew',
      value: 'Installer/Crew',
    },
    {
      label: 'Sales Professional',
      value: 'Sales Professional',
    },
  ],
};

export {
  constants,
  values,
  roofOptions,
};
