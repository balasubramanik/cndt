import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Dropdown from '../../components/dropdown';


configure({ adapter: new Adapter() });

describe('Dropdown with Menu items', () => {
  const onChangeHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
    target: {
      selectedIndex: 1,
      value: 'item1',
    },
  };
  const tree = shallow(<Dropdown
    menuItems={[{ label: 'item1', value: 'item1' }, { label: 'item2', value: 'item2' }]}
    onChangeHandler={onChangeHandler}
    input={{
      onChange: jest.fn(),
      value: '',
      label: '',
    }}
    placeholder="select"
    meta={{ touched: true, error: true }}
  />);

  it('should be defined', () => {
    expect(Dropdown).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render custom-select-box', () => {
    expect(tree.find('.custom-select-box').length).toBe(1);
  });

  it('should render MenuItem', () => {
    expect(tree.find('MenuItem').length).not.toBe(1);
  });

  it('should call selectHandler for dropdown field with data', () => {
    const simulateClick = tree.find('MenuItem').at(0).prop('onSelect');
    simulateClick(event, { label: 'item1', value: 'item1' });
    expect(tree.state().value).toBe('item1');
    expect(tree.state().label).toBe('item1');
  });

  it('should render select element', () => {
    expect(tree.find('select').length).toBe(1);
  });

  it('should call selectHandler for select field', () => {
    const simulateClick = tree.find('select').prop('onChange');
    simulateClick(event);
    expect(tree.state().value).toBe('item1');
    expect(tree.state().label).toBe('item1');
  });

  it('should call unmount', () => {
    tree.unmount();
  });
});

describe('Dropdown without Placeholder', () => {
  const tree = shallow(<Dropdown
    id="gaf-input"
    input={{
      onChange: jest.fn(),
    }}
    menuItems={[{ label: 'item1', value: 'item1' }]}
  />);

  it('should be defined', () => {
    expect(Dropdown).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render custom-select-box', () => {
    expect(tree.find('.custom-select-box').length).toBe(1);
  });

  it('should not render MenuItem', () => {
    expect(tree.find('MenuItem').length).toBe(1);
  });

  it('should equal to first value of menuitem ', () => {
    expect(tree.state().value).toBe('item1');
  });

  it('should not render options except placeholder', () => {
    expect(tree.find('option').length).toBe(1);
  });

  it('should call openDropdownHandler', () => {
    const simulateClick = tree.find('#gaf-input').prop('onToggle');
    simulateClick(true);
    expect(tree.state().isOpen).toBe(true);
  });

  it('should call closeDropDownHandler with event click', () => {
    const e = {
      type: 'click',
    };
    tree.instance().closeDropDownHandler(e);
    expect(tree.state().isOpen).toBe(true);
  });

  it('should call closeDropDownHandler with event orientationchange', () => {
    const e = {
      type: 'orientationchange',
    };
    tree.instance().closeDropDownHandler(e);
    expect(tree.state().isOpen).toBe(false);
  });
});

describe('Dropdown with Value', () => {
  const tree = shallow(<Dropdown
    id="gaf-input"
    menuItems={[{ label: 'item1', value: 'item1' }]}
    value="item1"
  />);

  it('should be defined', () => {
    expect(Dropdown).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      value: 'item2',
    });
    expect(tree.state().value).toBe('item1');
  });
});
