import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Select from 'react-select';
import { createListener, removeListener } from '../../utils/utils';

/** @description Functional component to render the dropdown */
class Dropdown extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      label: '',
    };
  }

  /** @description React Life cycle method
   * it will invoke, when component is mounted.
   */
  componentDidMount() {
    this.setInitialValue();
    createListener(null, ['click', 'touchstart', 'orientationchange'], this.closeDropDownHandler);
  }

  /** @description React Life cycle method
   * it will invoke, when component is updated.
   * @param {object} prevProps - previous props of component
   */
  componentDidUpdate(prevProps) {
    const { value, input, menuItems } = this.props;
    const val = value || input.value;
    const prevValue = prevProps.value || prevProps.input.value;
    const menuitems = JSON.stringify(menuItems);
    const prevMenuitems = JSON.stringify(prevProps.menuItems);
    if (prevValue !== val || menuitems !== prevMenuitems) {
      this.setInitialValue();
    }
  }

  /** @description React Life cycle method
  * it will invoke, when component is mounted.
  */
  componentWillUnmount() {
    removeListener(null, ['click', 'touchstart', 'orientationchange'], this.closeDropDownHandler);
  }

  /** @description Function to get title based on given value
   * @param {string} value - selected value of dropdown
   */
  getTitle = (value) => {
    const { menuItems } = this.props;
    const selectedItem = menuItems.find((item) => (item.value && value && (item.value.toString() === value.toString())));
    if (selectedItem) {
      return selectedItem.label;
    }
    return 'Select';
  }

  /** @description Function to set initial value of dropdown
   */
  setInitialValue = () => {
    const {
      value, placeholder, menuItems, input,
    } = this.props;
    const val = value || input.value;
    if (val) {
      this.updateState(val, this.getTitle(val));
      return;
    }
    if (placeholder) {
      this.updateState('', placeholder);
      return;
    }
    if (menuItems.length > 0) {
      this.updateState(menuItems[0].value, menuItems[0].label);
    }
  }

  /** @description Function to update the state value based on user selection
   * @param {string} value - value of dropdown
   * @param {string} item - title of dropdown
   */
  updateState = (value, label) => {
    const { input } = this.props;
    this.setState({ value, label }, () => {
      input.onChange(this.state.value);
    });
  }

  /** @description Callback function for select option
   * @param {object} e - triggered event(click, change)
   * @param {object} item - selected dropdown item
   */
  selectHandler = (e, item) => {
    let selectedOption = item;
    const { onChangeHandler, input, menuItems } = this.props;
    if (!selectedOption) {
      const { selectedIndex, value } = e.target;
      const targetEle = menuItems[selectedIndex - 1];
      const label = targetEle ? targetEle.label : '';
      selectedOption = { value, label };
    }
    const { value } = selectedOption;
    this.setState(selectedOption);
    // callback function which is comes from redux-form
    // function to update the form state
    input.onChange(value);
    // callback function which is comes from parent component
    onChangeHandler(value);
  }

  /** @description Function to render the select box for mobile breakpoint */
  renderSelectField = () => {
    const {
      input, menuItems, placeholder, readOnly,
    } = this.props;
    const { value } = this.state;
    return (
      <select
        className="visible-xs visible-sm form-control"
        {...input}
        value={value}
        onChange={this.selectHandler}
        disabled={readOnly}
      >
        {placeholder && <option value="" hidden>{placeholder}</option>}
        {menuItems && menuItems.map((item, i) => <option key={i.toString()} value={item.value}>{item.label}</option>)}
      </select>
    );
  }

  /** @description Function to render the dropdown for desktop breakpoint */
  renderSelect = () => {
    const {
      input, readOnly, id, menuItems,
    } = this.props;
    const { value, label } = this.state;
    return (
      <Select
        classNamePrefix="form-control"
        {...input}
        value={{ label, value }}
        onChange={(val) => this.selectHandler(null, val)}
        onBlur={(e) => { e.preventDefault(); }}
        isDisabled={readOnly || !(menuItems && menuItems.length > 0)}
        options={menuItems}
        id={id}
      />
    );
  }

  render() {
    const { meta } = this.props;
    const { touched, error } = meta;
    const dropdownClass = classNames('display-inline', { 'dropdown-highlight': touched && error });
    return (
      <div className={dropdownClass}>
        <div
          className="custom-select-box hidden-xs hidden-sm"
          ref={(ele) => { this.node = ele; }}
        >
          {this.renderSelect()}
        </div>
        {this.renderSelectField()}
        {touched && error && error.length > 0 && <p><span className="error-message">{error}</span></p>}
      </div>
    );
  }
}

/** PropTypes:
 * menuItems - array - menu items
 * id - string - unique identifier
 * onChange - func - callback function for select the option
 * className - string - for customizing the element
 * label - string - label name
 * input - object - property comes from redux-form
 * value - string - value of dropdown
 * meta - object - validation property comes from redux-form
 * placeholder - string - placeholder text of element
 * readOnly - boolean - flag to enable dropdown
 * meta - object - property comes from redux-form
 */
Dropdown.propTypes = {
  menuItems: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.bool,
    ]),
  })).isRequired,
  id: PropTypes.string.isRequired,
  onChangeHandler: PropTypes.func,
  className: PropTypes.string,
  label: PropTypes.string,
  input: PropTypes.object,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.bool,
  ]),
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  meta: PropTypes.object,
};

Dropdown.defaultProps = {
  onChangeHandler: () => { },
  input: { onChange: () => { } },
  readOnly: false,
  meta: {},
};

export default Dropdown;
