import React, { PureComponent } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import FileSaver from 'file-saver';

const Json2csvParser = require('json2csv').Parser;

class CSV extends PureComponent {
  componentDidUpdate(prevProps) {
    const { triggerDownload, isAsync } = this.props;
    if (isAsync && prevProps.triggerDownload !== triggerDownload && triggerDownload) {
      this.downloadCSVFile();
    }
  }

  createCSV = (config, data) => {
    const csvHead = config.map((col) => col.colName);
    const csvBody = data.map((item) => {
      let obj = {};
      config.forEach((col) => {
        let value = item[col.key];
        if (value && String(value).indexOf('=') === 0) {
          value = String(value);
        }
        obj = {
          ...obj,
          [col.colName]: value,
        };
      });
      return obj;
    });
    return {
      head: csvHead,
      body: csvBody,
    };
  };

  handleClick = (e) => {
    e.preventDefault();
    const { isAsync } = this.props;
    if (!isAsync) {
      this.downloadCSVFile();
    }
    this.props.onClick();
  }

  downloadCSVFile = () => {
    const { data, filename, config } = this.props;
    const { userAgent } = navigator;
    const { head, body } = this.createCSV(config, data);
    if (data && data.length > 0) {
      const json2csvParser = new Json2csvParser(head);
      const csv = json2csvParser.parse(body);
      if (navigator.msSaveBlob) { // IE10 and above
        navigator.msSaveBlob(new Blob([csv], { type: 'text/csv' }), filename);
        return;
      }
      const URL = window.URL || window.webkitURL;
      const blobUrl = URL.createObjectURL(new Blob([csv], { type: 'text/csv' }));
      const isIOS = userAgent.match(/(iPad|iPhone|iPod)/g);
      if (isIOS) {
        const template = `<html><head><title>${filename}</title></head><body height="100%" width="100%"><embed src="${blobUrl}" height="100%" width="100%"></embed></body></html>`;
        const win = window.open();
        win.document.write(template);
      } else {
        FileSaver.saveAs(blobUrl, filename);
      }
    }
  };

  render() {
    const { disabled } = this.props;
    const disabledClass = classNames({ 'disable-export': disabled });
    return (
      <a
        className={disabledClass}
        href="/"
        onClick={this.handleClick}
      >
        {this.props.children}
      </a>
    );
  }
}

/** PropTypes:
 * data - data provided to the CSV component
 * filename - Filename for that particular csv
 * label - Label for the export file
 */
CSV.propTypes = {
  children: PropTypes.object.isRequired,
  data: PropTypes.array,
  filename: PropTypes.string,
  config: PropTypes.array,
  isAsync: PropTypes.bool,
  triggerDownload: PropTypes.bool,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

export default CSV;
