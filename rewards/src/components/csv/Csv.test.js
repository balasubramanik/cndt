import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CSV from '../../components/csv';


configure({ adapter: new Adapter() });

describe('CSV with props', () => {
  const onRefHandler = jest.fn();
  const tree = shallow(<CSV
    filename="Sample.csv"
    config={[
      { colName: 'CUSTOMER ID #', key: 'contractorId' },
      { colName: 'CONTRACTOR NAME', key: 'contractorName' },
      { colName: 'CITY', key: 'city' },
    ]}
    data={[
      { contractorId: '111', contractorName: 'AAA', city: 'Auckland' },
      { contractorId: '222', contractorName: 'BBB', city: 'Wellington' },
      { contractorId: '333', contractorName: 'CCC', city: 'Sydney' },
    ]}
    onRef={onRefHandler}
  />);

  it('should be defined', () => {
    expect(CSV).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render CSVLink correctly', () => {
    expect(tree.find('CSVLink').length).toBe(1);
  });
});

describe('CSV without props', () => {
  const onRefHandler = jest.fn();
  const tree = shallow(<CSV
    filename="Sample.csv"
    config={[]}
    data={[]}
    onRef={onRefHandler}
  />);

  it('should be defined', () => {
    expect(CSV).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render CSVLink', () => {
    expect(tree.find('CSVLink').length).toBe(0);
  });
});
