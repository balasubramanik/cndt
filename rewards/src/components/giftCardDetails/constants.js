const constants = {
  TYPE: 'giftcards',
  GIFT_CARDS: 'giftCards',
  PHYSICAL_CARD: 'physicalCard',
  E_CARD: 'eCard',
};

export { constants };
