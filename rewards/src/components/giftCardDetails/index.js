import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'react-bootstrap';
import { constants } from './constants';
import GiftCardRedemption from '../../components/giftCardRedemption';
import CardDetails from '../../components/cardDetails';

export class GiftCardDetails extends PureComponent {
  constructor(props) {
    super(props);
    const { cardTypes } = props;
    const [cardType] = cardTypes || [];
    this.state = {
      cardType,
    };
  }

  /** @description Callback function for change card type
  * @param {string} type - selected card type
  */
  cardTypeChangeHandler = (type) => {
    this.setState({ cardType: type });
  }

  /** @description Callback function for view more button
  * @param {object} e - triggered event(click)
  */
  closeGiftCardHandler = (e) => {
    e.preventDefault();
    this.props.onClose();
  }

  /** @description Callback function for submit button
   * @param {object} values - values of redemption form
   */
  submitHandler = (values) => {
    const { details } = this.props;
    const val = {
      ...values,
      productId: details.id,
      productName: details.name,
    };
    this.props.onSubmit(constants.TYPE, constants.GIFT_CARDS, val);
  };

  /** @description Function to render the description
  */
  renderDescription = () => {
    const { cardType } = this.state;
    const { details, onViewClick } = this.props;
    const { productSpecification = {} } = details;
    const {
      descriptionTypeEcard, termsConditionEcard,
      descriptionTypePhysical, termsConditionPhysical,
    } = productSpecification;
    let description = null;
    if (cardType === constants.E_CARD) {
      description = {
        productDetails: descriptionTypeEcard,
        termsAndConditions: termsConditionEcard,
      };
    } else {
      description = {
        productDetails: descriptionTypePhysical,
        termsAndConditions: termsConditionPhysical,
      };
    }
    return <CardDetails description={description} onViewClick={onViewClick} />;
  }

  /** @description Function to render the close button
  */
  renderCloseButton = () => <a href="/" id="giftcard-close" onClick={this.closeGiftCardHandler} className="c-box-close"><span className="icon-close" /></a>;

  render() {
    const {
      details, formName, cardTypes, productName,
    } = this.props;
    return (
      <div className="c-box">
        <div className="grid-content">
          {this.renderCloseButton()}
          <div className="c-box-inner">
            <Row>
              <Col md={8} sm={6} xs={12}>
                {this.renderDescription()}
              </Col>
              {details.variants &&
                details.variants.length > 0 &&
                <Col md={4} sm={6} xs={12}>
                  <GiftCardRedemption
                    className="giftcards-form"
                    cardTypes={cardTypes}
                    formName={formName}
                    variants={details.variants}
                    onSubmit={this.submitHandler}
                    onCardTypeChange={this.cardTypeChangeHandler}
                    productName={productName}
                  />
                </Col>}
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

/** PropTypes:
 * onClose - func - callback function for close details
 * onSubmit - func - callback function for submit form
 * formName - string - unique key for form
 * productName - string - name of the product
 * cardTypes - array - contains the card types
 * details - array - contains the product information
 * descriptions - object - contains the description of product
 * cardTypes - number - available balance of logged in user
 * onViewClick - function - callback function for view button
*/
GiftCardDetails.propTypes = {
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  formName: PropTypes.string.isRequired,
  productName: PropTypes.string,
  cardTypes: PropTypes.array,
  details: PropTypes.object,
  onViewClick: PropTypes.func,
};

GiftCardDetails.defaultProps = {
  cardTypes: [],
  details: {},
};

export default GiftCardDetails;
