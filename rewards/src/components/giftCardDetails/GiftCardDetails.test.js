import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import GiftCardDetails from '../../components/giftCardDetails';


configure({ adapter: new Adapter() });

describe('GiftCardDetails', () => {
  const onSubmit = jest.fn();
  const onClose = jest.fn();
  const tree = shallow(<GiftCardDetails
    onClose={onClose}
    formName="MA123"
    onSubmit={onSubmit}
    details={{
      variants: [{}],
      productSpecification: {
        descriptionTypeEcard: 'sample',
        termsConditionEcard: 'sample',
        termsConditionPhysical: 'sample',
      },
    }
    }
  />);
  const event = {
    preventDefault: jest.fn(),
  };
  it('should be defined', () => {
    expect(GiftCardDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render grid-content', () => {
    expect(tree.find('.grid-content').length).toBe(1);
  });

  it('should render description', () => {
    expect(tree.find('CardDetails').length).toBe(1);
  });

  it('should render close button', () => {
    expect(tree.find('.c-box-close').length).toBe(1);
  });

  it('should call closeGiftCardHandler ', () => {
    const simulateClick = tree.find('.c-box-close').prop('onClick');
    simulateClick(event);
    expect(onClose).toBeCalled();
  });

  it('should render GiftCardRedemption', () => {
    expect(tree.find('.giftcards-form').length).toBe(1);
  });

  it('should call cardTypeChangeHandler ', () => {
    const simulateChange = tree.find('.giftcards-form').prop('onCardTypeChange');
    simulateChange('eCard');
    expect(tree.state().cardType).toBe('eCard');
  });

  it('should render submitHandler', () => {
    const simulateClick = tree.find('.giftcards-form').prop('onSubmit');
    simulateClick(event);
    expect(onSubmit).toBeCalled();
  });
});
