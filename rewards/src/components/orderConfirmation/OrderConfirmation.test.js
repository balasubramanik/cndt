import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import OrderConfirmation from '../../components/orderConfirmation';

configure({ adapter: new Adapter() });

describe('OrderConfirmation with props', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const receivedOrderHandler = jest.fn();
  const tree = shallow(<OrderConfirmation
    orderNumber="O11223344"
    orderReceived={receivedOrderHandler}
  />);
  it('should be defined', () => {
    expect(OrderConfirmation).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should render redirect link', () => {
    expect(tree.find('.redemption-link').length).toBe(1);
  });
  it('should render redirect to points history', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(receivedOrderHandler).toBeCalled();
  });
});

