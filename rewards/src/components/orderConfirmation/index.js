import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';
import { history } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import { OrderConfirmationText } from './Constant';

const orderReceived = (e, order) => {
  e.preventDefault();
  order(OrderConfirmationText.ORDER_RECEIVED);
  history.push({
    pathname: RouteConstants.POINTS_HISTORY,
  });
};

const OrderConfirmation = (props) => (
  <div>
    <div className="cart-summary-confirmation text-center">
      <section>
        <Grid>
          <div className="receive-confirm">
            <Row className="show-grid">
              <Col md={6} mdOffset={3}>
                <div className="order-check"><span className="icon-check"></span></div>
                <div className="order-received">{OrderConfirmationText.TITLE}</div>
                <div className="order-number" id="order-confirm-number">{OrderConfirmationText.ORDER_TEXT}<span> {props.orderNumber} </span></div>
                <div className="request-submitted">{OrderConfirmationText.SUBMIT_TEXT}</div>
                <p className="redemption-link">
                  <strong>
                    <a href="/" onClick={(e) => orderReceived(e, props.orderReceived)}>{OrderConfirmationText.CLICK_LINK}</a>
                  </strong>
                  {OrderConfirmationText.REDEMPTION_DETAILS}
                </p>
                <div className="redemption-details">
                  <p> {OrderConfirmationText.CONTACT_DETAILS_LINE_1}<strong>{OrderConfirmationText.CONTACT_MAIL}</strong>{OrderConfirmationText.CONTACT_DETAILS_LINE_2} </p>
                </div>
              </Col>
            </Row>
          </div>
        </Grid>
      </section>
    </div>
  </div>
);

OrderConfirmation.propTypes = {
  orderNumber: PropTypes.string,
  orderReceived: PropTypes.func,
};

export default OrderConfirmation;
