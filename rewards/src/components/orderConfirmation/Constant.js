import { inquiryDetails } from '../../constants/constants';

const OrderConfirmationText = {
  TITLE: 'Your order has been received!',
  SUBMIT_TEXT: 'Your redemption request has been submitted for processing.',
  CLICK_LINK: 'Click here',
  REDEMPTION_DETAILS: ' to check your redemption details.',
  ORDER_TEXT: 'Order Number:',
  CONTACT_DETAILS_LINE_1: `If you have any questions, please contact us at ${inquiryDetails.Phone} `,
  CONTACT_MAIL: `${inquiryDetails.Email}`,
  CONTACT_DETAILS_LINE_2: ' Monday through Friday, 8:00 a.m. to 5.00 p.m. ET.',
  ORDER_RECEIVED: 'Redemption Details',
};

export { OrderConfirmationText };
