import { validate } from './validate';
import { errorMessages } from '../../containers/ems/Constants';

describe('validate', () => {
  it('should be equal to invalid address1br', () => {
    const errors = validate({});
    expect(errors.address1br).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid citybr', () => {
    const errors = validate({});
    expect(errors.citybr).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid statebr', () => {
    const errors = validate({});
    expect(errors.statebr).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid zipbr', () => {
    const errors = validate({});
    expect(errors.zipbr).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid countrybr', () => {
    const errors = validate({});
    expect(errors.countrybr).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid workPhonebr', () => {
    const errors = validate({ workPhonebr: '123' });
    expect(errors.workPhonebr).toBe(errorMessages.INVALID_MOBILE_NUMBER);
  });

  it('should be equal to invalid address1hq', () => {
    const errors = validate({ address: [{ name: 'hq' }] });
    expect(errors.address1hq).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid cityhq', () => {
    const errors = validate({ address: [{ name: 'hq' }] });
    expect(errors.cityhq).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid statehq', () => {
    const errors = validate({ address: [{ name: 'hq' }] });
    expect(errors.statehq).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid ziphq', () => {
    const errors = validate({ address: [{ name: 'hq' }] });
    expect(errors.ziphq).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid countryhq', () => {
    const errors = validate({ address: [{ name: 'hq' }] });
    expect(errors.countryhq).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to invalid workPhonehq', () => {
    const errors = validate({ address: [{ name: 'hq' }], workPhonehq: '123' });
    expect(errors.workPhonehq).toBe(errorMessages.INVALID_MOBILE_NUMBER);
  });

  it('should be equal to valid address1br', () => {
    const errors = validate({ address1br: 'sample' });
    expect(errors.address1hq).toBe(undefined);
  });

  it('should be equal to valid citybr', () => {
    const errors = validate({ citybr: 'new york' });
    expect(errors.citybr).toBe(undefined);
  });

  it('should be equal to valid statebr', () => {
    const errors = validate({ statebr: 'new york' });
    expect(errors.statehq).toBe(undefined);
  });

  it('should be equal to valid zipbr', () => {
    const errors = validate({ zipbr: '5650' });
    expect(errors.zipbr).toBe(undefined);
  });

  it('should be equal to valid countrybr', () => {
    const errors = validate({ countrybr: 'USA' });
    expect(errors.countrybr).toBe(undefined);
  });

  it('should be equal to valid workPhonebr', () => {
    const errors = validate({ workPhonebr: '1234567890' });
    expect(errors.workPhonebr).toBe(undefined);
  });

  it('should be equal to valid companybr', () => {
    const errors = validate({ companybr: 'gaf' });
    expect(errors.companybr).toBe(undefined);
  });

  it('should be equal to valid address1hq', () => {
    const errors = validate({ address: [{ name: 'hq' }], address1hq: 'sample' });
    expect(errors.address1hq).toBe(undefined);
  });

  it('should be equal to valid cityhq', () => {
    const errors = validate({ address: [{ name: 'hq' }], cityhq: 'New york' });
    expect(errors.cityhq).toBe(undefined);
  });

  it('should be equal to valid statehq', () => {
    const errors = validate({ address: [{ name: 'hq' }], statehq: 'New York' });
    expect(errors.statehq).toBe(undefined);
  });

  it('should be equal to valid ziphq', () => {
    const errors = validate({ address: [{ name: 'hq' }], ziphq: '5650' });
    expect(errors.ziphq).toBe(undefined);
  });

  it('should be equal to valid countryhq', () => {
    const errors = validate({ address: [{ name: 'hq' }], countryhq: 'USA' });
    expect(errors.countryhq).toBe(undefined);
  });

  it('should be equal to valid workPhonehq', () => {
    const errors = validate({ address: [{ name: 'hq' }], workPhonehq: '1234567890' });
    expect(errors.workPhonehq).toBe(undefined);
  });
});
