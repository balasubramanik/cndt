import React from 'react';
import PropTypes from 'prop-types';
import { Col } from 'react-bootstrap';
import { Field } from 'redux-form';
import LocationSearchInput from '../locationSearchInput';
import Checkbox from '../checkbox';
import TextInput from '../textInput';
import Dropdown from '../dropdown';
import { address } from './constants';
import { formatUSPhoneNumber, allowOnlyNumber, formatPhoneNumber, restrictSpecialChar } from '../../utils/utils';

const readOnly = true;

/** @description Callback function for change the HeadQuaters and Mailing Address checkboxes
 * @param {object} e  - triggered event(change)
 * @param {object} fields - object comes from redux-form
 * @param {boolean} isMailingAddress - denotes change checkbox whether it is mailing address or not
 */
const changeHandler = (e, fields, isMailingAddress) => {
  const { checked } = e.target;
  const addressType = isMailingAddress ? address.MAIL : address.HEADQUATERS;
  if (checked) {
    const index = fields.getAll().findIndex((field) => field.name === addressType);
    if (index > -1) {
      fields.remove(index);
    }
    return;
  }
  if (!isMailingAddress) {
    fields.unshift({ name: addressType });
    return;
  }
  fields.push({ name: addressType });
};

/** @description Callback function for focus/blur the phone number field
 * @param {object} e  - triggered event(change)
 * @param {string} name - name of the field
 * @param {object} value - value of the field
 * @param {function} callback - callback function which is comes from parent
 */
const changePhoneNumber = (e, name, value, callback) => {
  e.preventDefault();
  if (e.type === 'focus') {
    callback(name, formatPhoneNumber(value));
  } else {
    callback(name, formatUSPhoneNumber(value));
  }
};

/** @description Functional component to render the addressline 1
 * @param {string} name - name of the element
 * @param {string} value - value of location input
 * @param {function} onAddressChange - callback function for change address in search field
 * @param {function} onAddressSelect - callback function for select address in dropdown field
 */
const renderAddressLine1 = (name, value, onAddressChange, onAddressSelect) => (
  <Col md={6}>
    <div className="form-group gaf-form">
      <label htmlFor="exampleInputAddressLine1">{address.ADDRESSLINE1}<sup>*</sup></label>
      <LocationSearchInput
        name={`address1${name}`}
        value={value}
        onBlur={(val) => onAddressChange(val, name)}
        onChange={(val) => onAddressChange(val, name)}
        onSelect={(val) => onAddressSelect(val, name)}
      />
    </div>
  </Col>
);

/** @description Functional component to render the addressline 2
 * @param {string} name - name of the element
 */
const renderAddressLine2 = (name) => (
  <Col md={6}>
    <Field
      name={`address2${name}`}
      id={`address2${name}`}
      type="text"
      className="form-group gaf-form"
      component={TextInput}
      label={address.ADDRESSLINE2}
      maxLength={55}
    />
  </Col>
);

/** @description Functional component to render the city
 * @param {string} name - name of the element
 */
const renderCity = (name) => (
  <Col md={6}>
    <Field
      name={`city${name}`}
      id={`city${name}`}
      type="text"
      className="form-group gaf-form"
      component={TextInput}
      label={`${address.CITY}<sup>*</sup>`}
      normalize={restrictSpecialChar}
      maxLength={30}
    />
  </Col>
);

/** @description Functional component to render the state
 * @param {string} name - name of the element
 * @param {array} states - state list
 */
const renderState = (name, states) => (
  <Col md={3} className="gaf-form form-group">
    <label htmlFor={`state${name}`}>{address.STATE}<sup>*</sup></label>
    {states && states.length > 0 &&
      <Field
        name={`state${name}`}
        id={`state${name}`}
        menuItems={states}
        component={Dropdown}
        placeholder="Select"
      />}
  </Col>
);

/** @description Functional component to render the zipcode
 * @param {string} name - name of the element
 */
const renderZipCode = (name) => (
  <Col md={3}>
    <Field
      name={`zip${name}`}
      id={`zip${name}`}
      type="text"
      className="form-group gaf-form"
      component={TextInput}
      normalize={allowOnlyNumber}
      label={`${address.ZIPCODE}<sup>*</sup>`}
      maxLength={5}
    />
  </Col>
);

/** @description Functional component to render the country
 * @param {string} name - name of the element
 */
const renderCountry = (name) => (
  <Col md={6}>
    <Field
      name={`country${name}`}
      id={`country${name}`}
      type="text"
      readOnly={readOnly}
      className="form-group gaf-form"
      component={TextInput}
      label={`${address.COUNTRY}<sup>*</sup>`}
      normalize={restrictSpecialChar}
      maxLength={3}
    />
  </Col>
);

/** @description Functional component to render the work phone number
 * @param {string} name - name of the element
 */
const renderWorkPhone = (name, onPhoneNumberChange) => {
  const isMandatory = name !== address.MAIL ? '<sup>*<sup>' : '';
  return (
    <Col md={6}>
      <Field
        name={`workPhone${name}`}
        id={`workPhone${name}`}
        type="text"
        className="form-group gaf-form"
        component={TextInput}
        onFocus={(e) => {
          const { target } = e;
          const { value } = target;
          changePhoneNumber(e, `workPhone${name}`, value, onPhoneNumberChange);
        }}
        onBlur={(e, value) => changePhoneNumber(e, `workPhone${name}`, value, onPhoneNumberChange)}
        normalize={allowOnlyNumber}
        label={`${address.WORKPHONE}${isMandatory}`}
        placeholder="(XXX) XXX-XXXX"
        maxLength={10}
      />
    </Col>
  );
};

/** @description Functional component to render the HeadQuaters and Mailing Address checkboxes
 * @param {object} fields - object comes from redux-form
 */
const renderAddressCheckbox = (fields, values) => (
  <div className="address-ha-ma">
    <Col md={12}>
      <Field
        name="hasHeadQuatersAddress"
        checked={values.hasHeadQuatersAddress}
        component={Checkbox}
        option={{ title: address.THIS_IS_THE_HEADQUATERS_ADDRES }}
        onChange={(e) => changeHandler(e, fields, false)}
      />
      <Field
        name="hasMailingAddress"
        checked={values.hasMailingAddress}
        component={Checkbox}
        option={{ title: address.THIS_IS_THE_MAILING_ADDRESS }}
        onChange={(e) => changeHandler(e, fields, true)}
      />
    </Col>
  </div>
);

/** @description Functional component to render the address
 * @param {string} name - name of the element
 * @param {string} value - value of location input
 * @param {function} onAddressChange - callback function for change address in search field
 * @param {function} onAddressSelect - callback function for select address in dropdown field
 * @param {array} states - state list
 */
const renderAddress = (name, value, onAddressChange, onAddressSelect, states, onPhoneNumberChange) => (
  <React.Fragment key={name}>
    {renderAddressLine1(name, value, onAddressChange, onAddressSelect)}
    {renderAddressLine2(name)}
    {renderCity(name)}
    {renderState(name, states)}
    {renderZipCode(name)}
    {renderCountry(name)}
    {renderWorkPhone(name, onPhoneNumberChange)}
  </React.Fragment>
);

/** @description Functional component to render the address */
const Address = ({
  fields, locationText, values, onAddressChange, onAddressSelect, states, onPhoneNumberChange,
}) =>
  (
    <React.Fragment>
      {renderAddress(
        address.BRANCH,
        locationText.brLocationText,
        onAddressChange,
        onAddressSelect,
        states,
        onPhoneNumberChange,
      )}
      {renderAddressCheckbox(fields, values)}
      {fields && fields.map((key, index) => {
        const field = fields.get(index);
        const title = field.name === address.MAIL ? address.MAILING_ADDRESS : address.HEADQUATERS_ADDRESS;
        return (
          <React.Fragment key={field.name}>
            <Col md={12}><p className="address-title"><strong>{title}</strong></p></Col>
            {renderAddress(
              field.name,
              locationText[`${field.name}LocationText`],
              onAddressChange,
              onAddressSelect,
              states,
              onPhoneNumberChange,
            )}
          </React.Fragment>);
      })}
    </React.Fragment>
  );

/** PropTypes:
 * fields - object - Property comes from redux-form
 * locationText - object - contains the location text of all addresses
 * values - object - contains the values of HeadQuaters and Mailing Address checkboxes
 * onAddressChange - func - callback function for change address in search field
 * onAddressSelect - func - callback function for select address in dropdown field
 * onPhoneNumberChange - func - callback function for change phone number field
 * states - array - contains the state list
 */
Address.propTypes = {
  fields: PropTypes.object.isRequired,
  locationText: PropTypes.shape({
    brLocationText: PropTypes.string,
    hqLocationText: PropTypes.string,
    mlLocationText: PropTypes.string,
  }),
  values: PropTypes.shape({
    hasMailingAddress: PropTypes.bool,
    hasHeadQuatersAddress: PropTypes.bool,
  }),
  onAddressChange: PropTypes.func,
  onAddressSelect: PropTypes.func,
  onPhoneNumberChange: PropTypes.func,
  states: PropTypes.array,
};

Address.defaultProps = {
  locationText: {
    brLocationText: '',
    hqLocationText: '',
    mlLocationText: '',
  },
  values: {
    hasMailingAddress: true,
    hasHeadQuatersAddress: true,
  },
  onAddressChange: () => { },
  onAddressSelect: () => { },
};

export default Address;
