const address = {
  ADDRESSLINE1: 'Address line 1',
  ADDRESSLINE2: 'Address line 2',
  CITY: 'City',
  STATE: 'State',
  ZIPCODE: 'Zip Code',
  COUNTRY: 'Country',
  THIS_IS_THE_HEADQUATERS_ADDRES: 'This is the headquarter\'s address (or not applicable)',
  THIS_IS_THE_MAILING_ADDRESS: 'This is the mailing address',
  HEADQUATERS_ADDRESS: 'Headquarters',
  MAILING_ADDRESS: 'Mailing address',
  WORKPHONE: 'Phone',
  BRANCH: 'br',
  HEADQUATERS: 'hq',
  MAIL: 'ml',
};

export { address };
