import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Address from '../../components/address';


configure({ adapter: new Adapter() });

describe('Address without fields', () => {
  const onAddressChange = jest.fn();
  const onAddressSelect = jest.fn();
  const tree = shallow(<Address
    states={[{ name: 'New Jersey', code: 'NJ' }]}
    onAddressChange={onAddressChange}
    onAddressSelect={onAddressSelect}
  />);

  it('should be defined', () => {
    expect(Address).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render AddressLine 1', () => {
    expect(tree.find('LocationSearchInput').length).toBe(1);
  });

  it('should call onAddressChange', () => {
    const simulateChange = tree.find('LocationSearchInput').prop('onChange');
    simulateChange();
    expect(onAddressChange).toBeCalled();
  });

  it('should call blur handler', () => {
    const simulateChange = tree.find('LocationSearchInput').prop('onBlur');
    simulateChange();
    expect(onAddressChange).toBeCalled();
  });

  it('should call onAddressSelect', () => {
    const simulateClick = tree.find('LocationSearchInput').prop('onSelect');
    simulateClick();
    expect(onAddressSelect).toBeCalled();
  });

  it('should render AddressLine 2', () => {
    expect(tree.find('Field[name="address2br"]').length).toBe(1);
  });

  it('should render City', () => {
    expect(tree.find('Field[name="citybr"]').length).toBe(1);
  });

  it('should render State', () => {
    expect(tree.find('Field[name="statebr"]').length).toBe(1);
  });

  it('should render ZipCode', () => {
    expect(tree.find('Field[name="zipbr"]').length).toBe(1);
  });

  it('should render Country', () => {
    expect(tree.find('Field[name="countrybr"]').length).toBe(1);
  });

  it('should render WorkPhone', () => {
    expect(tree.find('Field[name="workPhonebr"]').length).toBe(1);
  });
});

describe('Address with fields', () => {
  const onAddressChange = jest.fn();
  const onAddressSelect = jest.fn();
  const fields = [{ name: 'hq' }, { name: 'ml' }];
  fields.get = () => ({ name: 'hq' });
  fields.getAll = () => [{ name: 'hq' }];
  fields.unshift = jest.fn();
  fields.remove = jest.fn();
  fields.push = jest.fn();
  const tree = shallow(<Address
    editable={{
      brEditable: false,
      hqEditable: false,
      mlEditable: false,
    }}
    fields={fields}
    onAddressChange={onAddressChange}
    onAddressSelect={onAddressSelect}
  />);

  it('should be defined', () => {
    expect(Address).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render HeadQuatersAddress checkbox', () => {
    expect(tree.find('Field[name="hasHeadQuatersAddress"]').length).toBe(1);
  });

  it('should render ChangeHandler for hasHeadQuatersAddress', () => {
    const simulateChange = tree.find('Field[name="hasHeadQuatersAddress"]').prop('onChange');
    const e = {
      target: {
        checked: false,
      },
    };
    simulateChange(e);
    expect(fields.unshift).toBeCalled();
  });

  it('should render ChangeHandler for hasMailingAddress', () => {
    const simulateChange = tree.find('Field[name="hasMailingAddress"]').prop('onChange');
    const e = {
      target: {
        checked: false,
      },
    };
    simulateChange(e);
    expect(fields.push).toBeCalled();
  });

  it('should render ChangeHandler for hasHeadQuatersAddress with checked true', () => {
    const simulateChange = tree.find('Field[name="hasHeadQuatersAddress"]').prop('onChange');
    const e = {
      target: {
        checked: true,
      },
    };
    simulateChange(e);
    expect(fields.remove).toBeCalled();
  });
});
