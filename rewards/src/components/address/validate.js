import { errorMessages } from '../../containers/ems/Constants';
import { convertStringToNumber } from '../../utils/utils';

const validate = (values) => {
  const errors = {};
  const address = [{ name: 'br' }, ...values.address || []];
  address.forEach((key) => {
    if (!values[`address1${key.name}`]) {
      errors[`address1${key.name}`] = errorMessages.REQUIRED_FIELD;
    }
    if (!values[`city${key.name}`]) {
      errors[`city${key.name}`] = errorMessages.REQUIRED_FIELD;
    }
    if (!values[`state${key.name}`]) {
      errors[`state${key.name}`] = errorMessages.REQUIRED_FIELD;
    }
    if (!values[`zip${key.name}`]) {
      errors[`zip${key.name}`] = errorMessages.REQUIRED_FIELD;
    }
    if (!values[`country${key.name}`]) {
      errors[`country${key.name}`] = errorMessages.REQUIRED_FIELD;
    }

    if (key.name !== 'ml' && !values[`workPhone${key.name}`]) {
      errors[`workPhone${key.name}`] = errorMessages.REQUIRED_FIELD;
    }

    if (values[`workPhone${key.name}`]
      && (values[`workPhone${key.name}`].replace(/[^0-9]+/g, '').length < 10
        || !convertStringToNumber(values[`workPhone${key.name}`]))) {
      errors[`workPhone${key.name}`] = errorMessages.INVALID_MOBILE_NUMBER;
    }
  });
  return errors;
};

export { validate };
