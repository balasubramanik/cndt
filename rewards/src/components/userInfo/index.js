import React from 'react';
import PropTypes from 'prop-types';
import { userInfo } from './constants';
import ToolTip from '../toolTip';
import { authentication } from '../../routes';
import ImageHOC from '../imageHOC';

/** @description function to render the contractor logo
 * @param {string} url - image path
 */
const renderContractorLogo = (url) => {
  const imageUrl = url || 'images/contractor.png';
  return (
    <figure>
      <div className="figure-outline">
        <ImageHOC isAbsolute={url} src={imageUrl} alt="Profile" />
      </div>
    </figure>
  );
};

/** @description function to render the contractor information
 * @param {string} accNo - contractor's account number
 * @param {string} certificateNo - contractor's certificate number
 */
const renderContractorInfo = (accNo, certificateNo) => (
  <div className="user-info">
    {accNo &&
      <dl className="gaf-user-accno">
        <dt>{accNo}</dt>
        <dd>{userInfo.ACCOUNT}</dd>
      </dl>
    }
    {certificateNo &&
      <dl className="gaf-user-certification">
        <dt>{certificateNo}</dt>
        <dd>{userInfo.CERTIFICATION}</dd>
      </dl>
    }
  </div>
);

/** @description Functional component to render the User information
 */
const UserInfo = ({ isShowLogo, details, isAdmin }) => {
  if (!details || Object.keys(details).length === 0) {
    return null;
  }
  const {
    firstName, lastName, contractorAccountNumber, contractorCertificationNumber, contractorInfo = {}, contractorImage,
  } = details;
  const { dba = '', legalName = '' } = contractorInfo;
  const contractorName = dba || legalName;
  const { email } = authentication.getAuthDetails();
  return (
    <div className="user-acc-info">
      {isShowLogo && renderContractorLogo(contractorImage)}
      <div>
        <h3 className={isAdmin ? 'admin-user-info' : ''}>
          <ToolTip id="gaf-user-name" text={`${firstName} ${lastName}`}>
            <span role="button">{`${firstName} ${lastName}`}</span>
          </ToolTip>
        </h3>
        {isAdmin && <p>{email}</p>}
      </div>
      {isShowLogo &&
        contractorName &&
        <div>
          <p className="gaf-user-desc">
            <ToolTip id="gaf-user-desc" text={contractorName}>
              <span role="button">{contractorName}</span>
            </ToolTip>
          </p>
        </div>
      }
      {!isAdmin && renderContractorInfo(contractorAccountNumber, contractorCertificationNumber)}
    </div>
  );
};

/** PropTypes:
 * details - object - contains the information about user
 * isShowLogo - boolean - Flag for show/hide the contractor image and description
 */
UserInfo.propTypes = {
  details: PropTypes.object.isRequired,
  isShowLogo: PropTypes.bool,
  isAdmin: PropTypes.bool,
};

UserInfo.defaultProps = {
  isAdmin: false,
};

export default UserInfo;
