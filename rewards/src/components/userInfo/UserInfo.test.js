import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import UserInfo from '../../components/userInfo';


configure({ adapter: new Adapter() });

describe('UserInfo with isShowLogo false', () => {
  const tree = shallow(<UserInfo
    details={{
      email: 'Brain@gaf.com',
      firstName: 'Brain',
      lastName: 'Granger',
      contractorAccountNumber: '1234567890',
      contractorCertificationNumber: 'ME08560',
      contractorName: 'Queens Roofing and Tinsmithing Inc',
      contractorImage: 'tinsmiths.jpg',
    }}
  />);
  it('should be defined', () => {
    expect(UserInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render userName', () => {
    expect(tree.find('h3').length).toBe(1);
  });

  it('should render Account Number', () => {
    expect(tree.find('.gaf-user-accno').length).toBe(1);
  });

  it('should render Cerification Number', () => {
    expect(tree.find('.gaf-user-certification').length).toBe(1);
  });
});

describe('UserInfo with isShowLogo true', () => {
  const tree = shallow(<UserInfo
    isShowLogo={true}
    details={{
      email: 'Brain@gaf.com',
      firstName: 'Brain',
      lastName: 'Granger',
      contractorAccountNumber: '1234567890',
      contractorCertificationNumber: 'ME08560',
      contractorName: 'Queens Roofing and Tinsmithing Inc',
      contractorImage: '',
    }}
  />);
  it('should be defined', () => {
    expect(UserInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render userName', () => {
    expect(tree.find('h3').length).toBe(1);
  });

  it('should render Account Number', () => {
    expect(tree.find('.gaf-user-accno').length).toBe(1);
  });

  it('should render Cerification Number', () => {
    expect(tree.find('.gaf-user-certification').length).toBe(1);
  });

  it('should render Contractor Image', () => {
    expect(tree.find('figure').length).toBe(1);
  });

  it('should render default Contractor Image', () => {
    const image = tree.find('Image').prop('src');
    expect(image).toBe('images/logo.png');
  });

  it('should render  Contractor Description', () => {
    expect(tree.find('.gaf-user-desc').length).toBe(1);
  });
});

describe('UserInfo without details', () => {
  const tree = shallow(<UserInfo
    isShowLogo={true}
    details={{}}
  />);
  it('should be defined', () => {
    expect(UserInfo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should not render Account Number', () => {
    expect(tree.find('.gaf-user-accno').length).toBe(0);
  });

  it('should not render Cerification Number', () => {
    expect(tree.find('.gaf-user-certification').length).toBe(0);
  });

  it('should not render Contractor Image', () => {
    expect(tree.find('figure').length).toBe(0);
  });

  it('should not render  Contractor Description', () => {
    expect(tree.find('.gaf-user-desc').length).toBe(0);
  });
});
