import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-bootstrap';
import envConfig from 'envConfig'; //eslint-disable-line

const ImageHOC = (props) => {
  const { isAbsolute, src } = props;
  const imageUrl = isAbsolute ? src : `${envConfig.cdnBuildUrl}${src}`;
  return (<Image {...props} src={imageUrl} />);
};

ImageHOC.propTypes = {
  src: PropTypes.string.isRequired,
  isAbsolute: PropTypes.bool,
};

export default ImageHOC;
