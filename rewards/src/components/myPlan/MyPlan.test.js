import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MyPlan from '../../components/myPlan';


configure({ adapter: new Adapter() });

describe('MyPlan', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<MyPlan
    onChange={changehandler}
    data={[{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Cobra® Snow CountryTM',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Cobra® Ridge Runner®',
          dollarPerUnit: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    }]}
    handleSelect={selecthandler}
    loading
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should be defined', () => {
    expect(MyPlan).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Accordion component', () => {
    expect(tree.find('Accordion').length).toBe(2);
  });

  it('should render Dropdown component', () => {
    expect(tree.find('Dropdown').length).toBe(1);
  });
  it('should call onChange event', () => {
    const simulateChange = tree.find('a').prop('onClick');
    simulateChange(event);
    expect(CSVHandler).toBeCalled();
  });
  it('should call ascending table sort for product family', () => {
    const instance = tree.instance();
    instance.sortBy('productFamily', '1', true);
    expect(tree.state().myPlanData).toEqual([{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Cobra® Ridge Runner®',
          dollarPerUnit: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Cobra® Snow CountryTM',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
      ],
    }]);
  });
  it('should call descending table sort for product family', () => {
    const instance = tree.instance();
    instance.sortBy('productFamily', '1', false);
    expect(tree.state().myPlanData).toEqual([{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Cobra® Snow CountryTM',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Cobra® Ridge Runner®',
          dollarPerUnit: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    }]);
  });
  it('should call descending table sort for rate', () => {
    const instance = tree.instance();
    tree.setState({
      myPlanData: [{
        category: 'Three-Tab Shingles',
        products: [
          {
            planId: '53370',
            startDate: '2018-11-02T18:30:00+00:00',
            endDate: '2018-10-11T00:00:00',
            byPercent: 1,
            productFamily: 'Royal Sovereign®',
            dollarPerUnit: 0,
            uom: {
              code: 'N/A',
              description: 'N/A',
            },
            planType: 'Standard',
            allowanceType: 'PCT',
          },
          {
            planId: '53531',
            startDate: '2018-11-21T00:00:00',
            endDate: '2018-12-31T00:00:00',
            byPercent: 0,
            productFamily: 'Royal Sovereign®',
            dollarPerUnit: 3,
            uom: {
              code: 'BD',
              description: 'BD',
            },
            planType: 'Custom',
            allowanceType: 'DOL',
          },
        ],
      },
      {
        category: 'Timberline® Lifetime Shingles',
        products: [
          {
            planId: '53370',
            startDate: '2018-11-02T18:30:00+00:00',
            endDate: '2018-10-11T00:00:00',
            byPercent: 1,
            productFamily: 'Cobra® Snow CountryTM',
            dollarPerUnit: 0,
            uom: {
              code: 'N/A',
              description: 'N/A',
            },
            planType: 'Standard',
            allowanceType: 'PCT',
          },
          {
            planId: '53531',
            startDate: '2018-11-21T00:00:00',
            endDate: '2018-12-31T00:00:00',
            byPercent: 0,
            productFamily: 'Cobra® Ridge Runner®',
            dollarPerUnit: 2,
            uom: {
              code: 'P',
              description: 'P',
            },
            planType: 'Custom',
            allowanceType: 'DOL',
          },
        ],
      }],
    });
    instance.sortBy('rate', '1', false);
    expect(tree.state().myPlanData).toEqual([{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Cobra® Ridge Runner®',
          dollarPerUnit: 2,
          rateValue: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Cobra® Snow CountryTM',
          rateValue: 1,
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
      ],
    }]);
  });
});
describe('MyPlan mounted', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = mount(<MyPlan
    onChange={changehandler}
    data={[{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Cobra® Snow CountryTM',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Cobra® Ridge Runner®',
          dollarPerUnit: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    }]}
    handleSelect={selecthandler}
    loading
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should call function to handle tab selection', () => {
    const simulateChange = tree.find('#accordion-controlled-example').at(0).prop('onSelect');
    simulateChange(event);
    expect(updateIdHandler).toBeCalled();
  });
});
describe('MyPlan with data not loading for current year', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const tree = shallow(<MyPlan
    onChange={changehandler}
    handleSelect={selecthandler}
    loading={false}
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should give no data message', () => {
    expect(tree.find('.gaf-accordian').length).toBe(0);
  });
  it('should not render CSV component', () => {
    expect(tree.find('CSV').length).toBe(0);
  });
});


describe('MyPlan when component gets updated', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const tree = shallow(<MyPlan
    onChange={changehandler}
    data={[{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Timberline® Natural Shadow®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Timberline® Natural Shadow®',
          dollarPerUnit: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    }]}
    handleSelect={selecthandler}
    loading={false}
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should update myPlan data with accordions', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      loading: 'true',
    });
    expect(tree.state().myPlanData.length).toBe(2);
  });
});

describe('MyPlan when component does not get updated', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const tree = shallow(<MyPlan
    onChange={changehandler}
    data={[{
      category: 'Three-Tab Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Royal Sovereign®',
          dollarPerUnit: 3,
          uom: {
            code: 'BD',
            description: 'BD',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [
        {
          planId: '53370',
          startDate: '2018-11-02T18:30:00+00:00',
          endDate: '2018-10-11T00:00:00',
          byPercent: 1,
          productFamily: 'Timberline® Natural Shadow®',
          dollarPerUnit: 0,
          uom: {
            code: 'N/A',
            description: 'N/A',
          },
          planType: 'Standard',
          allowanceType: 'PCT',
        },
        {
          planId: '53531',
          startDate: '2018-11-21T00:00:00',
          endDate: '2018-12-31T00:00:00',
          byPercent: 0,
          productFamily: 'Timberline® Natural Shadow®',
          dollarPerUnit: 2,
          uom: {
            code: 'P',
            description: 'P',
          },
          planType: 'Custom',
          allowanceType: 'DOL',
        },
      ],
    }]}
    handleSelect={selecthandler}
    loading
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should not update myPlan data in state', () => {
    tree.setState({ myPlanData: [] });
    const instance = tree.instance();
    instance.componentDidUpdate({
      loading: 'true',
    });
    expect(tree.state().myPlanData.length).toBe(0);
  });
});

describe('MyPlan with no products inside data', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const tree = shallow(<MyPlan
    onChange={changehandler}
    data={[{
      category: 'Three-Tab Shingles',
      products: [],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [],
    }]}
    handleSelect={selecthandler}
    loading={false}
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should not render CSV component', () => {
    expect(tree.find('CSV').length).toBe(0);
  });
});

describe('MyPlan with no start and end date inside data and incorrect allowance type', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const tree = shallow(<MyPlan
    onChange={changehandler}
    data={[{
      category: 'Three-Tab Shingles',
      products: [{
        planId: '53370',
        startDate: '',
        endDate: '',
        byPercent: 1,
        productFamily: 'Timberline® Natural Shadow®',
        dollarPerUnit: 0,
        uom: {
          code: 'N/A',
          description: 'N/A',
        },
        planType: 'Standard',
        allowanceType: 'PXA',
      },
      {
        planId: '53531',
        startDate: '',
        endDate: '',
        byPercent: 0,
        productFamily: 'Royal Sovereign®',
        dollarPerUnit: 3,
        uom: {
          code: 'BD',
          description: 'BD',
        },
        planType: 'Custom',
        allowanceType: 'FRD',
      },
      ],
    },
    {
      category: 'Timberline® Lifetime Shingles',
      products: [{
        planId: '53356',
        startDate: '',
        endDate: '',
        byPercent: 1,
        productFamily: 'Timberline® NaturalD Shadow®',
        dollarPerUnit: 0,
        uom: {
          code: 'N/A',
          description: 'N/A',
        },
        planType: 'Standard',
        allowanceType: 'PXA',
      },
      {
        planId: '53590',
        startDate: '',
        endDate: '',
        byPercent: 0,
        productFamily: 'RoyalS Sovereign®',
        dollarPerUnit: 3,
        uom: {
          code: 'BD',
          description: 'BD',
        },
        planType: 'Custom',
        allowanceType: 'FRD',
      },
      ],
    }]}
    handleSelect={selecthandler}
    loading={false}
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('MyPlan with no data', () => {
  const CSVHandler = jest.fn();
  const changehandler = jest.fn();
  const selecthandler = jest.fn();
  const refhandler = jest.fn();
  const updateIdHandler = jest.fn();
  const tree = mount(<MyPlan
    onChange={changehandler}
    handleSelect={selecthandler}
    loading={false}
    activeIndex={1}
    onRef={refhandler}
    CSVHandler={CSVHandler}
    selectedYear={2018}
    updatePlanId={updateIdHandler}
  />);

  it('should not render CSV component', () => {
    expect(tree.find('CSV').length).toBe(0);
  });
});
