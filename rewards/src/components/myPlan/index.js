import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Col, ControlLabel, Form, FormGroup, PanelGroup, Row, Tab } from 'react-bootstrap';
import CSV from '../../components/csv';
import Accordion from '../../components/accordion';
import Tables from '../../components/tables';
import Dropdown from '../dropdown';
import { tableHeaders } from './constants';
import { ascending, descending, formatDateUTC, truncateZero, roundToTwoDecimal } from '../../utils/utils';

/** @description Class component to render the My Plans Tab */
class MyPlan extends React.Component {
  constructor(props) {
    super(props);
    this.currentYear = moment().year();
    this.state = {
      myPlanData: props.data || [],
    };
  }

  componentDidUpdate(prevProps) {
    const { data, loading, activeIndex } = this.props;
    if (prevProps.loading !== loading && !loading && activeIndex === 1) {
      this.updateMyplanState(data);
      this.props.updatePlanId();
    }
  }

  updateMyplanState = (data) => {
    this.setState({ myPlanData: data });
  }

  /** @description Function available for sorting  */
  sortBy = (key, i, isAsc) => {
    const { myPlanData } = this.state;
    const categories = [...myPlanData];
    const selectedCategory = categories[i];
    categories.splice(i, 1);
    categories.splice(i, 0, {
      ...selectedCategory,
      products: key === tableHeaders.heading.Rate ? this.sortByRate(isAsc, selectedCategory.products) : selectedCategory.products.sort(isAsc ? ascending(key) : descending(key)),
    });
    this.setState({ myPlanData: categories });
  }

  sortByRate = (isAsc, products) => {
    const rateByDollar = [];
    const rateByPercent = [];
    const key = tableHeaders.heading.RateValue;
    products.forEach((item) => {
      if (item.allowanceType === tableHeaders.heading.Dollar) {
        const i = {
          ...item,
          [key]: item.dollarPerUnit,
        };
        rateByDollar.push(i);
      } else {
        const i = {
          ...item,
          [key]: item.byPercent,
        };
        rateByPercent.push(i);
      }
    });
    rateByDollar.sort(isAsc ? ascending(key) : descending(key));
    rateByPercent.sort(isAsc ? ascending(key) : descending(key));
    return rateByDollar.concat(rateByPercent);
  }

  /** @description Formating the available date into correct date format */
  formatProducts = (products) => {
    if (!products) return [];
    return products.map((product) => {
      const decimalRate = truncateZero(product.byPercent);
      const decimalDollar = roundToTwoDecimal(product.dollarPerUnit);
      const {
        startDate, endDate, allowanceType,
      } = product;
      return {
        ...product,
        startDate: formatDateUTC(startDate) || 'N/A',
        endDate: formatDateUTC(endDate) || 'N/A',
        rate: allowanceType === tableHeaders.heading.Dollar ? `$${decimalDollar}/${product.uom.code}` : `${decimalRate}%` || 'N/A',
      };
    });
  };

  createMyPlanCSV = () => {
    const csv = [];
    const { myPlanData } = this.state;
    if (myPlanData) {
      myPlanData.forEach((item) => {
        const formattedData = this.formatProducts(item.products);
        formattedData.forEach((product) => {
          csv.push({
            ...product,
            ...item,
            uom: product.uom.code,
            dollarPerUnit: product.allowanceType === tableHeaders.heading.Dollar ? product.dollarPerUnit : product.byPercent,
            allowanceType: product.allowanceType === tableHeaders.heading.Dollar ? 'Dollar' : 'Percent',
          });
        });
      });
    }
    return csv;
  }

  handleSelect = (selectedPlanId) => {
    this.props.updatePlanId(selectedPlanId);
  }


  renderCSVLink = () => {
    const { myPlanData } = this.state;
    const CSVRecords = this.createMyPlanCSV();
    return (
      <div className="gaf-export-block pull-right">
        <CSV
          filename={tableHeaders.heading.filename}
          config={tableHeaders.csvConfig}
          data={CSVRecords}
          onClick={this.props.CSVHandler}
          disabled={!myPlanData || myPlanData.length === 0}
        >
          <div className="pull-right" id="myplan-export-block">
            <span><i className="icon-Export-Icon"></i><span>{tableHeaders.heading.label}</span></span>
          </div>
        </CSV>
      </div>
    );
  }
  render() {
    const {
      selectedPlanId,
      loading,
      selectedYear,
    } = this.props;
    const { myPlanData } = this.state;
    const noDataMsg = (!loading && (selectedYear === this.currentYear) ?
      <div>{tableHeaders.heading.currentYearNoData}</div> :
      <div>{tableHeaders.heading.PreviousYearNoData}</div>
    );
    return (

      <div className="gaf-tabs" id="myplan-tab">
        <Tab eventKey={1} title={tableHeaders.heading.title}>
          <div className="tab-header-content" id="myplan-tab-header">
            <Row className="show-grid">
              <Col xs={12} sm={12} md={4} lg={5}>
                <h4>Category</h4>
              </Col>
              <Col xs={12} sm={12} md={8} lg={7}>
                <div className="filterby" id="myplan-tabheader-filterby">
                  <Form inline>
                    <FormGroup className="gaf-form gaf-select-box">
                      <ControlLabel>View by</ControlLabel>{' '}
                      <Dropdown
                        menuItems={tableHeaders.items}
                        onChangeHandler={this.props.onChange}
                        value={selectedYear}
                      />
                    </FormGroup>{' '}
                    {this.renderCSVLink()}
                  </Form>
                </div>
              </Col>
            </Row>
          </div>
          <Row>
            <Col xs={12}>
              {/* GAF - Products Details */}
              {myPlanData && myPlanData.length > 0 ?
                <div className="gaf-accordian">
                  <PanelGroup
                    accordion
                    id="accordion-controlled-example"
                    onSelect={this.handleSelect}
                    activeKey={selectedPlanId}
                  >
                    {myPlanData.map((item, i) => (
                      <Accordion
                        key={i.toString()}
                        title={item.category}
                        body={<Tables config={tableHeaders.myPlan} data={this.formatProducts(item.products)} onSortingClick={(key, isAsc) => this.sortBy(key, i, isAsc)} />}
                        index={i}
                        id={`myplan_accrodian_${i}`}
                      />
                    ))}
                  </PanelGroup>
                </div> : noDataMsg}
            </Col>
          </Row>
        </Tab>
      </div>

    );
  }
}

/** PropTypes:
 * onChange - Function for determining the on change behaviour of dropdown
 * data - Array of data supplied to the MyPlan component
 * handleSelect - Function to handle the tab selection
 */

MyPlan.propTypes = {
  onChange: PropTypes.func,
  data: PropTypes.array,
  selectedPlanId: PropTypes.number,
  loading: PropTypes.bool,
  activeIndex: PropTypes.number,
  CSVHandler: PropTypes.func,
  selectedYear: PropTypes.number,
  updatePlanId: PropTypes.func,
};

export default MyPlan;
