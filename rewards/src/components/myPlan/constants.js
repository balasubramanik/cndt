import moment from 'moment';
import { inquiryDetails } from '../../constants/constants';

const tableHeaders = {
  myPlan: [
    { title: 'PRODUCT', key: 'productFamily', sort: true },
    { title: 'RATE', key: 'rate', sort: true },
    { title: 'START DATE', key: 'startDate', sort: true },
    { title: 'END DATE', key: 'endDate', sort: true },
  ],
  items: [
    { label: 'Current Year', value: moment().year() },
    { label: 'Previous Year', value: moment().year() - 1 },
  ],
  heading: {
    title: 'My Plan',
    filename: 'MyPlan.csv',
    label: 'Export',
    Dollar: 'DOL',
    Rate: 'rate',
    RateValue: 'rateValue',
    currentYearNoData: `The system is currently processing to correctly assign the eligible GAF products and the % you could earn on your purchases. Please allow 4-6 hours for us to complete the process. If you have any questions, please reach out to us at ${inquiryDetails.Phone} ${inquiryDetails.Email}.`,
    PreviousYearNoData: 'No record found for the selected year',
  },
  csvConfig: [
    { colName: 'Category', key: 'category' },
    { colName: 'Product', key: 'productFamily' },
    { colName: 'UOM', key: 'uom' },
    { colName: 'Value', key: 'dollarPerUnit' },
    { colName: 'Allowance Type', key: 'allowanceType' },
    { colName: 'Start Date', key: 'startDate' },
    { colName: 'End Date', key: 'endDate' },
  ],
};

export { tableHeaders };
