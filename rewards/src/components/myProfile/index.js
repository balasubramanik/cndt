import React from 'react';
import PropTypes from 'prop-types';
import UserInfo from '../../components/userInfo';
import RewardsInfo from '../../components/rewardsInfo';

/** @description Functional Component to render the My profile */
const MyProfile = ({ info, onViewDetailsClick, isRewardPointAccess }) => {
  const isShowLogo = true;
  return (
    <aside className="gaf-myprofile text-center">
      <div>
        <UserInfo details={info.userInfo} isShowLogo={isShowLogo} />
        <RewardsInfo
          details={info.rewardsInfo}
          isShowLogo={isShowLogo}
          onViewDetailsClick={onViewDetailsClick}
          isRewardPointAccess={isRewardPointAccess}
        />
      </div>
    </aside>
  );
};

/** PropTypes:
 * info - object - contains the rewards information and profile information
 * onViewDetailsClick - func - callback function for view more button
 */
MyProfile.propTypes = {
  info: PropTypes.object.isRequired,
  onViewDetailsClick: PropTypes.func.isRequired,
  isRewardPointAccess: PropTypes.bool,
};

export default MyProfile;
