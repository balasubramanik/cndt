import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MyProfile from '../../components/myProfile';


configure({ adapter: new Adapter() });

describe('MyProfile', () => {
  const tree = shallow(<MyProfile
    info={{
      userInfo: {
        email: 'Brain@gaf.com',
        firstName: 'Brain',
        lastName: 'Granger',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
        contractInfo: {
          contractorName: 'Queens Roofing and Tinsmithing Inc',
          contractorImage: 'tinsmiths.jpg',
        },
      },
      rewardsInfo: {
        rewardsPoint: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    }}
  />);
  it('should be defined', () => {
    expect(MyProfile).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render MyProfile Container', () => {
    expect(tree.find('.gaf-myprofile').length).toBe(1);
  });

  it('should render UserInfo', () => {
    expect(tree.find('UserInfo').length).toBe(1);
  });

  it('should render RewardsInfo', () => {
    expect(tree.find('RewardsInfo').length).toBe(1);
  });
});
