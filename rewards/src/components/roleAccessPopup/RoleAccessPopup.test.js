import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import RoleAccessPopup from '../../components/roleAccessPopup';

configure({ adapter: new Adapter() });
jest.useFakeTimers();
describe('RoleAccessPopup', () => {
  const accessTextHandler = jest.fn();
  const tree = shallow(<RoleAccessPopup
    accessText={accessTextHandler}
    show={true}
  />);

  it('should be defined', () => {
    expect(RoleAccessPopup).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should display access message', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      show: false,
    });
    jest.runAllTimers();
    expect(tree.find('.gaf-toast-alert-dialog').length).toBe(1);
    expect(accessTextHandler).toBeCalled();
  });
  it('when show is true and not changed', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      show: true,
    });
    jest.runAllTimers();
    expect(tree.find('.gaf-toast-alert-dialog').length).toBe(1);
    expect(accessTextHandler).toBeCalled();
  });
});
