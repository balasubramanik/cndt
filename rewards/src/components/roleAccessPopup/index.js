import React, { PureComponent } from 'react';
import classNames from 'classnames';
import { Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { roleaccess } from './constants';


const TIMEOUT = 2000;


/** @description Functional component to render the RoleAccessPopup message */
class RoleAccessPopup extends PureComponent {
  /** @description React Life cycle method
     * it will invoke, when component is mounted.
     */
  componentDidUpdate(prevProps) {
    const { show } = this.props;
    if (prevProps.show !== show && show) {
      setTimeout(() => {
        this.props.accessText(false);
      }, TIMEOUT);
    }
  }


  render() {
    const { show } = this.props;
    const toastClass = classNames('gaf-toast-alert-dialog');
    return (
      <Modal show={show} className="gaf-model-popup gaf-toast-alert-main">
        <Modal.Body>
          {
            <div className={toastClass}>
              <span className="icon-alert-danger"></span>
              <p>{roleaccess.ACCESS_DENIED_MESSAGE}</p>
            </div>
          }
        </Modal.Body>
      </Modal>
    );
  }
}

/** PropTypes:
 * accessText - bool - based on user access message is dispalyed
 * show - bool - toast message
 */
RoleAccessPopup.propTypes = {
  accessText: PropTypes.func,
  show: PropTypes.bool,
};

export default RoleAccessPopup;
