const roleaccess = {
  ACCESS_DENIED_MESSAGE: 'Sorry, you do not have access to this feature.',
};

export { roleaccess };
