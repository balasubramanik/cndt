import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ImageHOC from '../imageHOC';

const Logo = (props) => {
  const { linkHref } = props;
  return (
    <div className="header-logo">
      <Link href={linkHref} to={linkHref}>
        <ImageHOC src="images/gaf-rewards-logo.png" alt="GAF Rewards" />
      </Link>
    </div>
  );
};

Logo.propTypes = {
  linkHref: PropTypes.string,
};

Logo.defaultProps = {
  linkHref: '/',
};

export default Logo;
