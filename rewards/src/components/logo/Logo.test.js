import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Logo from '../../components/logo';


configure({ adapter: new Adapter() });

describe('Logo', () => {
  const tree = shallow(<Logo />);

  it('should be defined', () => {
    expect(Logo).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render header-logo element', () => {
    expect(tree.find('.header-logo').length).toBe(1);
  });

  it('should render Logo Image', () => {
    expect(tree.find('ImageHOC').length).toBe(1);
  });
});
