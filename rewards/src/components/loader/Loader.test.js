import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Loader from '../loader';


configure({ adapter: new Adapter() });

describe('Loader', () => {
  const tree = shallow(<Loader />);

  it('should be defined', () => {
    expect(Loader).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render loader image', () => {
    expect(tree.find('.logo-svg').length).toBe(1);
  });

  it('should render loader-inner', () => {
    expect(tree.find('.loader-inner').length).toBe(1);
  });
});
