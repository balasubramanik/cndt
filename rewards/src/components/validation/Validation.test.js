import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Validation from '../../components/validation';


configure({ adapter: new Adapter() });

describe('Validation', () => {
  const tree = shallow(<Validation />);
  it('should be defined', () => {
    expect(Validation).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render title', () => {
    expect(tree.find('.thank-you-text').length).toBe(1);
  });

  it('should render Instruction', () => {
    expect(tree.find('.email-instruction').length).toBe(1);
  });

  it('should render Mobilenumber', () => {
    expect(tree.find('.mobilenumber').length).toBe(1);
  });
});
