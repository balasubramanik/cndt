const validation = {
  TITLE: 'Thank you!',
  DESCRIPTION: `Once we have validated your registration (process takes about 3-5 business days), 
   we will send you an email to either log in to your account or to create a password.`,
  IF_YOU_HAVE_ANY_QUESTIONS: 'If you have any questions, ',
  CONTACT_US: 'contact us at ',
  PHONE_NUMBER: '877-GAF-ROOF (877-423-7663), ',
  EMAIL: 'option 2, or GAFRewards@gaf.com.',
};

export { validation };
