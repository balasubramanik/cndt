import React from 'react';
import { validation } from './Constants';


const Validation = () => (
  <div className="text-center register-final-page">
    <p className="thank-you-text">{validation.TITLE}</p>
    <p className="email-instruction">{validation.DESCRIPTION}</p>
    <div className="query-time">
      <p>{validation.IF_YOU_HAVE_ANY_QUESTIONS}</p>
      <p>{validation.CONTACT_US}</p>
      <p className="mobilenumber"><strong>{validation.PHONE_NUMBER}</strong></p>
      <p>{validation.EMAIL}</p>
    </div>
  </div>
);

export default Validation;
