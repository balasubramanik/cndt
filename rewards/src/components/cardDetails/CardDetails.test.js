import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CardDetails from '../../components/cardDetails';


configure({ adapter: new Adapter() });

describe('CardDetails with descriptions', () => {
  const tree = shallow(<CardDetails
    description={{
      productDetails: 'description1',
      termsAndConditions: 'description2',
    }}
  />);

  const event = {
    preventDefault: jest.fn(),
  };

  it('should be defined', () => {
    expect(CardDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render debit-card-info', () => {
    expect(tree.find('.debit-card-info').length).toBe(1);
  });

  it('should render description', () => {
    expect(tree.find('.c-box-inner-desc').length).toBe(1);
  });

  it('should render product-details', () => {
    expect(tree.find('.product-details').length).toBe(1);
  });

  it('should render terms-and-conditions', () => {
    expect(tree.find('.terms-and-conditions').length).toBe(1);
  });

  it('should render View more button', () => {
    expect(tree.find('.view-more').length).toBe(1);
  });

  it('should call onNavigationClick', () => {
    const simulateClick = tree.find('.view-more').prop('onClick');
    simulateClick(event);
    expect(tree.state().isViewMore).toBe(false);
  });
});

describe('CardDetails without descriptions', () => {
  const tree = shallow(<CardDetails />);

  it('should be defined', () => {
    expect(CardDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render debit-card-info', () => {
    expect(tree.find('.debit-card-info').length).toBe(0);
  });

  it('should render description', () => {
    expect(tree.find('.c-box-inner-desc').length).toBe(0);
  });

  it('should render product-details', () => {
    expect(tree.find('.product-details').length).toBe(0);
  });

  it('should render terms-and-conditions', () => {
    expect(tree.find('.terms-and-conditions').length).toBe(0);
  });

  it('should render View more button', () => {
    expect(tree.find('.view-more').length).toBe(0);
  });
});
