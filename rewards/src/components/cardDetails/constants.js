const constants = {
  VIEW_MORE: 'View More',
  VIEW_LESS: 'View Less',
  PRODUCT_DETAILS: 'Product Details',
  TERMS_AND_CONDITIONS: 'Terms and Conditions',
};

export { constants };
