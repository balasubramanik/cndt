import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { constants } from './constants';

export class CardDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isViewMore: true,
    };
  }

  /** @description Callback function for view more button
   * @param {object} e - triggered event(click)
   */
  viewMoreHandler = (e) => {
    e.preventDefault();
    const { isViewMore } = this.state;
    this.setState({ isViewMore: !isViewMore }, () => {
      this.props.onViewClick(this.state.isViewMore);
    });
  }

  render() {
    const { description } = this.props;
    const { productDetails, termsAndConditions } = description;
    const { isViewMore } = this.state;
    const viewMoreClass = classNames('debit-card-info', { 'view-more-xs': !isViewMore });
    if (!productDetails && !termsAndConditions) {
      return null;
    }
    return (
      <div className={viewMoreClass}>
        <div className="c-box-inner-desc gaf-scrollbar">
          {productDetails &&
            <div className="product-details">
              <h4>{constants.PRODUCT_DETAILS}</h4>
              <p>{productDetails}</p>
            </div>}
          {termsAndConditions &&
            <div className="terms-and-conditions">
              <h4>{constants.TERMS_AND_CONDITIONS}</h4>
              <p>{termsAndConditions}</p>
            </div>}
        </div>
        <a href="/" className="view-more visible-xs" id="giftcard-viewmore" onClick={this.viewMoreHandler}>
          {isViewMore ? constants.VIEW_MORE : constants.VIEW_LESS}
          <i className="icon-down-arrow-circle" />
        </a>
      </div>
    );
  }
}

/** PropTypes:
 * description - object - contains the product and terms and condition descriptions
 * onViewClick - function - callback function for view button
*/
CardDetails.propTypes = {
  description: PropTypes.object,
  onViewClick: PropTypes.func,
};

CardDetails.defaultProps = {
  description: {},
  onViewClick: () => { },
};

export default CardDetails;
