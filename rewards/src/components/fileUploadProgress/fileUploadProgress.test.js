import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import FileUploadProgress from '../fileUploadProgress';

configure({ adapter: new Adapter() });

describe('FileUploadProgress', () => {
  const successFunc = jest.fn();
  const formGetter = jest.fn();
  const formCustomizer = jest.fn();
  const beforeSend = jest.fn();
  const tree = shallow(<FileUploadProgress url="url" callback={successFunc} formGetter={formGetter} formCustomizer={formCustomizer} beforeSend={beforeSend} />);
  const event = {
    preventDefault: jest.fn(),
    target: {
      files: ['sample.pdf'],
    },
  };

  it('should be defined', () => {
    expect(FileUploadProgress).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
    tree.instance().successCall();
    tree.instance().onSubmit(event);
    tree.instance().onCancelClick(event);
    tree.instance().onDeleteClick(event, 1);
    tree.instance().uploadFile(event);
  });

  it('should call uploadFile', () => {
    const uploadFile = tree.find('#file-upload-btn').prop('onChange');
    uploadFile(event);
    expect(tree.state().files.length).toBe(1);
  });
});
