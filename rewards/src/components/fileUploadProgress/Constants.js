const fileUploadProgress = {
  MAXFILESIZE: 30,
  MAXFILECOUNT: 20,
  MBCONVERSION: 1024,
  FILESIZEEXCEEDSMSG1: 'Your upload is more than',
  FILESIZEEXCEEDSMSG2: 'MBs. Please try again.',
  UPLOADTEXT: 'UPLOAD',
  CLEARMESSAGE: 'You have cancelled your upload.',
};

export { fileUploadProgress };
