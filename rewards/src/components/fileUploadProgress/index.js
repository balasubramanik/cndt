import { EventEmitter } from 'events';
import React from 'react';
import PropTypes from 'prop-types';
import FileSaver from 'file-saver';
import { fileUploadProgress } from './Constants';
import { authentication } from '../../routes';
import Loader from '../../components/loader';
import Dialog from '../../components/dialog';
import RouteConstants from '../../constants/RouteConstants';
import ErrorNotification from '../../components/errorNotification';

const TIMEOUT = 2000;

class FileUploadProgress extends React.Component {
  constructor(props) {
    super(props);
    this.proxy = new EventEmitter();
    this.state = {
      files: [],
      fileSizeExceeds: false,
      inValidFiles: false,
      isLoading: false,
      isCancel: false,
      errorResponse: null,
      isFileCountExceed: false,
    };
  }

  /** @description React Life cycle method
     * it will invoke, when component is mounted.
     */
  componentDidUpdate(prevProps) {
    const { files, isOpened } = this.props;
    const prevPropsObj = JSON.stringify(prevProps.files);
    const newPropsObj = JSON.stringify(files);
    if (prevPropsObj !== newPropsObj) {
      this.updateState('files', files);
    }
    if (this.state.isCancel) {
      setTimeout(() => {
        this.onCancelPopup();
      }, TIMEOUT);
    }
    if ((prevProps.isOpened !== isOpened) && isOpened) {
      this.onCancelClick(false);
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.doUpload();
  }

  onCancelClick = (bool, e) => {
    if (e) e.preventDefault();
    this.setState({
      files: [],
      fileSizeExceeds: false,
      inValidFiles: false,
      isCancel: bool,
    });
  }

  onCancelPopup = () => {
    this.setState({
      isCancel: false,
    });
  }

  onDeleteClick = (e, index) => {
    e.preventDefault();
    const { files } = this.state;
    const selectedFiles = [...files];
    selectedFiles.splice(Number(index), 1);
    this.setState({
      files: selectedFiles,
      inValidFiles: false,
    }, () => {
      let fileSize = 0;
      Object.keys(this.state.files).map((i) => {
        const file = this.state.files[i];
        fileSize += file.file.size;
        return fileSize;
      });
      fileSize /= fileUploadProgress.MBCONVERSION * fileUploadProgress.MBCONVERSION;

      this.setState({
        fileSizeExceeds: (fileSize > this.props.maxFileSize),
      });
      this.props.getFiles(this.state.files);
    });
  }

  getBlobFromFile = (event) => {
    Object.keys(event.target.files).forEach((index) => {
      const { length } = event.target.files;
      const file = event.target.files[index];
      if (this.props.allowFormats.indexOf(file.type) !== -1) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          const { files } = this.state;
          const fileObj = [...files, { name: file.name, file, blob: reader.result }];
          this.setState({ files: fileObj }, () => {
            if (length === this.state.files.length) {
              this.props.getFiles(this.state.files);
            }
          });
        };
      }
    });
  }

  getFormData() {
    const {
      contractorId,
      email,
      impersonatedBy,
    } = authentication.getAuthDetails();
    const { userInfo } = this.props;
    const form = new FormData();
    form.append('contractorid', contractorId);
    form.append('userid', email);
    form.append('impersonatorid', impersonatedBy);
    form.append('firstname', userInfo.firstName);
    form.append('lastname', userInfo.lastName);
    form.append('url', RouteConstants.CLAIM_STATUS);

    for (let i = 0, len = this.state.files.length; i < len; i += 1) {
      const { userAgent } = navigator;
      if (userAgent.indexOf('MSIE ') > -1 || userAgent.indexOf('Trident/') > -1 || userAgent.indexOf('Edge/') > -1) {
        form.append('form', this.state.files[i].file, this.state.files[i].file.name);
      } else {
        form.append('form', this.state.files[i].file);
      }
    }
    if (this.props.formGetter) {
      return this.props.formGetter();
    }
    return form;
  }

  updateState = (key, value) => {
    this.setState({
      [key]: value,
    });
  }

  doUpload() {
    const { accessToken, tokenId } = authentication.getAuthDetails();
    this.setState({
      isLoading: true,
    });
    const form = this.getFormData();
    const req = new XMLHttpRequest();
    req.open('POST', this.props.url);
    req.setRequestHeader('Authorization', `Bearer ${accessToken}`);
    req.setRequestHeader('Authorization2', tokenId);
    req.onreadystatechange = (e) => {
      if (e.target.readyState === 4) {
        this.setState({
          isLoading: false,
        });
        if (e.target.status === 200) {
          this.props.callback();
          /* Trigger the GTM action */
          this.props.gtmUploadInvoiceActions.gtmResultMessage('Success');
        } else {
          const response = response ? JSON.parse(e.target.response) : {};
          this.errorResponse = { status: e.target.status, ...response };
          const { maxFileSize, inValidFiles } = this.props;
          const { FILESIZEEXCEEDSMSG1, FILESIZEEXCEEDSMSG2 } = fileUploadProgress;
          this.renderErrorMessage();
          /* Trigger the GTM action */
          this.props.gtmUploadInvoiceActions.gtmResultMessage(`${FILESIZEEXCEEDSMSG1} ${maxFileSize} ${FILESIZEEXCEEDSMSG2}${inValidFiles}${maxFileSize}`);
        }
      }
    };
    this.props.beforeSend(req).send(this.props.formCustomizer(form));
  }

  uploadFile = (event) => {
    const { files } = this.state;
    const fileCount = event.target.files.length + files.length;
    let fileSize = 0;
    let inValidFiles = false;
    this.setState({
      isFileCountExceed: (fileCount > this.props.maxFileCount),
    });
    Object.keys(event.target.files).map((index) => {
      const file = event.target.files[index];
      if (this.props.allowFormats.indexOf(file.type) !== -1) {
        fileSize += file.size;
      } else {
        inValidFiles = true;
      }
      return fileSize;
    });
    Object.keys(files).map((index) => {
      const file = files[index];
      fileSize += file.file.size;
      return fileSize;
    });
    fileSize /= fileUploadProgress.MBCONVERSION * fileUploadProgress.MBCONVERSION;

    this.setState({
      fileSizeExceeds: (fileSize > this.props.maxFileSize),
      inValidFiles,
    });

    if (fileCount <= this.props.maxFileCount && fileSize <= this.props.maxFileSize) {
      this.getBlobFromFile(event);
    }
    /* Trigger the GTM action */
    if (this.props.gtmUploadInvoiceActions) {
      this.props.gtmUploadInvoiceActions.gtmUploadInvoice();
      this.props.gtmUploadInvoiceActions.gtmInvoiceCount(fileCount);
      this.props.gtmUploadInvoiceActions.gtmTotalFileSize(fileSize);
    }
  }

  openImageHandler = (e, file) => {
    e.preventDefault();
    if (typeof (file) === 'object') {
      FileSaver.saveAs(file);
    } else {
      window.open(file);
    }
  }

  /** @description function to render the error message for File Upload */
  renderErrorMessage = () => {
    this.setState({ isLoading: false, errorResponse: this.errorResponse });
  }

  render() {
    const {
      isCloseDisabled, inValidFiles, maxFileSize, fileCountExceedMsg,
    } = this.props;
    const { FILESIZEEXCEEDSMSG1, FILESIZEEXCEEDSMSG2 } = fileUploadProgress;
    const listItems = this.state.files.length > 0 ? this.state.files.map((file, index) => {
      const { name, file: fileObj } = file;
      return (<li><a href="/" onClick={(e) => this.openImageHandler(e, fileObj)}>{name}</a> {!isCloseDisabled && <a href="/" onClick={(e) => this.onDeleteClick(e, index)}><span className="icon-close" /></a>}</li>);
    }) : <li></li>;
    const customClass = (this.state.files.length === this.props.maxFileCount) ? 'disabled' : '';
    const toastBody = (
      <div className="gaf-toast-alert-dialog">
        <span className="icon-close"></span>
        <p>{fileUploadProgress.CLEARMESSAGE}</p>
      </div>
    );
    return (
      <div className="invoice-upload-files">
        <Dialog
          show={this.state.isCancel}
          body={toastBody}
          className="gaf-model-popup gaf-toast-alert-main"
        />
        {this.state.isLoading && <Loader />}
        {this.state.inValidFiles ? <p className="gaf-error-message">{inValidFiles}</p> : null}
        {this.state.fileSizeExceeds ? <p className="gaf-error-message">{FILESIZEEXCEEDSMSG1} {maxFileSize} {FILESIZEEXCEEDSMSG2}</p> : null}
        {this.state.isFileCountExceed ? <p className="gaf-error-message">{fileCountExceedMsg}</p> : null}
        <form className="_react_fileupload_form_content" onSubmit={this.onSubmit} method="post">
          <div>
            <label htmlFor="file-upload-btn" className={`gaf-btn-primary upload-btn ${customClass}`}>
              {fileUploadProgress.UPLOADTEXT}
            </label>
            <input type="file" id="file-upload-btn" disabled={this.state.files.length === this.props.maxFileCount} name="file" multiple accept=".pdf,.jpg,.png,.jpeg" onChange={this.uploadFile} onClick={(e) => { e.target.value = null; }} />
          </div>
          {this.state.files.length > 0 &&
            <div className="uploaded-files gaf-scrollbar">
              <ul>
                {listItems}
              </ul>
            </div>}
          {this.state.files.length > 0 && this.props.type === undefined ?
            <div className="upload-save-cancel-submit clearfix">
              <div className="upload-cancel-btn">
                <button className="btn btn-block gaf-btn-secondary" onClick={(event) => { this.onCancelClick(true, event); }}>CANCEL</button>
              </div>
              <div className="upload-submit-btn">
                <button className="btn btn-block gaf-btn-primary" type="submit" disabled={this.state.files.length === 0} >SUBMIT</button>
              </div>
            </div>
            : null}
        </form>
        {this.state.errorResponse && <ErrorNotification error={this.errorResponse} onClear />}
      </div>
    );
  }
}

FileUploadProgress.propTypes = {
  url: PropTypes.string.isRequired,
  type: PropTypes.string,
  callback: PropTypes.func,
  formGetter: PropTypes.func,
  formCustomizer: PropTypes.func,
  beforeSend: PropTypes.func,
  allowFormats: PropTypes.array,
  maxFileCount: PropTypes.number,
  maxFileSize: PropTypes.number,
  userInfo: PropTypes.object,
  getFiles: PropTypes.func,
  isCloseDisabled: PropTypes.bool,
  files: PropTypes.object,
  inValidFiles: PropTypes.string,
  isOpened: PropTypes.bool,
  gtmUploadInvoiceActions: PropTypes.object,
  fileCountExceedMsg: PropTypes.string,
};

FileUploadProgress.defaultProps = {
  formCustomizer: (form) => form,
  beforeSend: (request) => request,
  userInfo: {},
  getFiles: () => { },
  isOpened: false,
};

export default FileUploadProgress;
