import React from 'react';
import { Grid } from 'react-bootstrap';
import ImageHOC from '../imageHOC';

/** @description Functional component to render the EmsHeader section */
const EmsHeader = () => (
  <header className="header-top-logo-nav">
    <Grid>
      <div className="header-logo">
        <a href="https://www.gaf.com/rewards">
          <ImageHOC src="images/gaf-rewards-logo.png" alt="GAF Rewards" />
        </a>
      </div>
    </Grid>
  </header>
);

export default EmsHeader;
