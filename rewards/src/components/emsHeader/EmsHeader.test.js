import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import EmsHeader from '../../components/emsHeader';


configure({ adapter: new Adapter() });

describe('EmsHeader', () => {
  const tree = shallow(<EmsHeader />);
  it('should be defined', () => {
    expect(EmsHeader).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render header-top-logo-nav', () => {
    expect(tree.find('.header-top-logo-nav').length).toBe(1);
  });

  it('should render Logo', () => {
    expect(tree.find('Logo').length).toBe(1);
  });
});
