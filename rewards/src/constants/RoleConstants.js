import RouteConstants from './RouteConstants';
import envConfig from 'envConfig'; //eslint-disable-line

const RoleConstants = {
  VIEW_POINTS: [
    '/',
    RouteConstants.POINTS_HISTORY,
    RouteConstants.ORDER_DETAILS,
    RouteConstants.CLAIM_DETAILS,
    RouteConstants.CONTACTUS,
    `${envConfig.cczBaseUrl}`],
  VIEW_PLAN: [
    '/',
    RouteConstants.WHAT_QUALIFIES,
    RouteConstants.SPOTPROMOTIONS_DETAILS,
    `${envConfig.cczBaseUrl}`,
  ],
  SUBMIT_INVOICES: [
    '/',
    RouteConstants.SUBMIT_INVOICE,
    RouteConstants.CLAIM_STATUS,
    RouteConstants.CLAIM_DETAILS,
    RouteConstants.CONTACTUS,
    `${envConfig.cczBaseUrl}`,
  ],
  FULL_ACCESS: [
    '/',
    RouteConstants.WHAT_QUALIFIES,
    RouteConstants.SPOTPROMOTIONS_DETAILS,
    RouteConstants.SUBMIT_INVOICE,
    RouteConstants.CLAIM_STATUS,
    RouteConstants.CLAIM_DETAILS,
    RouteConstants.CONTACTUS,
    RouteConstants.POINTS_HISTORY,
    RouteConstants.ORDER_DETAILS,
    RouteConstants.REDEEM_POINTS,
    RouteConstants.CARTSUMMARY,
    `${envConfig.cczBaseUrl}`,
  ],
  ADMINISTRATOR: [
    RouteConstants.ADMIN,
    RouteConstants.CONTRACTORMANAGEMENT,
    RouteConstants.PLAN_DETAILS,
    RouteConstants.SPOTPROMOTION,
    RouteConstants.NEW_SPOT_PROMOTION,
    RouteConstants.EDIT_SPOTPROMOTION,
  ],
};

export default RoleConstants;
