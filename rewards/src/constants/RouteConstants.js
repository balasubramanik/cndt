const RouteConstants = {
  DASHBOARD: '/dashboard',
  SIGNUP: '/signup',
  WHAT_QUALIFIES: '/whatqualifies',
  SPOTPROMOTIONS_DETAILS: '/spotpromotiondetails/:planId',
  SUBMIT_INVOICE: '/submitinvoice',
  POINTS_HISTORY: '/pointshistory',
  REDEEM_POINTS: '/redeempoints',
  CLAIM_STATUS: '/claimstatus',
  ORDER_DETAILS: '/orderdetails/:orderId',
  CLAIM_DETAILS: '/claimdetails/:claimId',
  CONTACTUS: '/contactus/:claimId',
  CARTSUMMARY: '/cartsummary',
  ADMIN: '/admin',
  SPOTPROMOTION: '/admin/spotpromotion',
  EDIT_SPOTPROMOTION: '/admin/editspotpromotion/:promoId',
  NEW_SPOT_PROMOTION: '/admin/createspotpromotion',
  CONTRACTORMANAGEMENT: '/admin/contractormanagement',
  PLAN_DETAILS: '/admin/plandetails',
};

export default RouteConstants;
