export const searchByType = {
  ClaimStatusNumber: 'CLAIM_STATUS_NUMBER',
  PointsHistoryClaimNumber: 'POINT_HISTORY_CLAIM_NUMBER',
  ClaimProductDetails: 'CLAIM_PRODUCT_DETAILS',
  ClaimDetailProduct: 'claimDetailProduct',
  OrderListProduct: 'orderListProduct',
  PromotionTitle: 'promotionTitle',
  CityList: 'cityList',
  ContractorNameOrId: 'contractorNameOrId',
};

export const pageTitle = {
  HOME: 'GAF Rewards | Home',
  SUBMITINVOICE: 'GAF Rewards | Submit Invoice',
  CLAIMDETAILS: 'GAF Rewards | Claim Details',
  CONTACTUS: 'GAF Rewards | Contact Us',
  CONTRACTOR_MANAGEMENT: 'GAF Rewards | Contractor Management',
  ORDER_DETAILS: 'GAF Rewards | Order Details',
  POINTSHISTORY: 'GAF Rewards | Points History',
  WHATQUALIFIES: 'GAF Rewards | What Qualifies',
  CLAIMSTATUS: 'GAF Rewards | Claim Status',
  SPOTPROMOTIONDETAILS: 'GAF Rewards | Spot Promotion Details',
  SPOTPROMOTION_MANAGEMENT: 'GAF Rewards | Spot Promotion Management',
  REDEEMPOINTS: 'GAF Rewards | Redeem Points',
  CARTSUMMARY: 'GAF Rewards | Cart Summary',
  SIGN_UP: 'GAF Rewards | Sign Up',
};

export const inquiryDetails = {
  Email: 'option 2, or GAFRewards@gaf.com',
  Phone: '877-GAF-ROOF (877-423-7663),',
};
