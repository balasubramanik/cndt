import baseConfig from './baseConfig';

const prod = {
  baseUrl: 'rewardsint.gaf.com',
  cczBaseUrl: 'cczintmnr.gaf.com',
  rewardsBaseUrl: 'rewardsint.gaf.com',
  cdnUrl: 'rewardsdevcdn.gaf.com/',
  cdnBuildUrl: 'rewardsdevcdn.gaf.com/build/',
  gafBaseUrl: 'www.gaf.com',
  googlePlaceApiKeys: 'AIzaSyAQGJ9SjMSB_9upoC8xgQvtVOjYRNRCXsg',
  googleRecaptchaApiKeys: '6Lf-W24UAAAAAG-GpQKIqVGSPa6lz1RaZ0J7mXiW',
  gtmContainerId: 'GTM-W53DJS2',
  authConfig: {
    domain: 'ssodev.gaf.com',
    clientId: '4AKl4QCKIfTiXNVzJAdJyizXmIOYUIFB',
    audience: 'rewardsapiint.gaf.com',
  },
  idleSessionTime: '1800000',
  sessionTimeout: '60000',
  termsAndConditionUrl: 'webcsauthor.gaf.com/rewards-terms-and-conditions',
  disableCaptcha: 'false',
};

export default {
  ...baseConfig,
  ...prod,
};
