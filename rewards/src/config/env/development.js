import baseConfig from './baseConfig';

const development = {
  baseUrl: 'https://rewardsqa.gaf.com',
  cczBaseUrl: 'https://cczqa.gaf.com',
  rewardsBaseUrl: 'http://localhost:8080',
  cdnUrl: 'https://rewardsqacdn.gaf.com/',
  cdnBuildUrl: '/',
  gafBaseUrl: 'www.gaf.com',
  googlePlaceApiKeys: 'AIzaSyAQGJ9SjMSB_9upoC8xgQvtVOjYRNRCXsg',
  googleRecaptchaApiKeys: '6Lf-W24UAAAAAG-GpQKIqVGSPa6lz1RaZ0J7mXiW',
  gtmContainerId: 'GTM-W53DJS2',
  authConfig: {
    domain: 'ssoqa.gaf.com',
    clientId: '31vjHpEismTU1ZnE5K8FES2AqyBfIafW',
    audience: 'https://rewardsapiqa.gaf.com',
  },
  idleSessionTime: '1800000',
  sessionTimeout: '60000',
  disableCaptcha: false,
};

export default {
  ...baseConfig,
  ...development,
};
