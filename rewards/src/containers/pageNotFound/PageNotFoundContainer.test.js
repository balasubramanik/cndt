import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PageNotFoundContainer from '../../containers/pageNotFound';
import { history } from '../../routes';
configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('PageNotFoundContainer', () => {
  const tree = shallow(<PageNotFoundContainer />);
  it('should be defined', () => {
    expect(PageNotFoundContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render 404 image correctly', () => {
    expect(tree.find('Image').length).toBe(1);
  });
  it('should render redirect link', () => {
    expect(tree.find('.goto-home').length).toBe(1);
  });
});


describe('PageNotFoundContainer', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = mount(<PageNotFoundContainer />);
  it('should be defined', () => {
    expect(PageNotFoundContainer).toBeDefined();
  });

  it('should redirect to dashboard when clicking the link', () => {
    const simulateClick = tree.find('a').prop('onClick');
    simulateClick(event);
    expect(history.push).toBeCalled();
  });
});
