const constants = {
  t1: {
    errorCode: 'Error 404 - Page not found',
    pageNotFound: 'The page you requested could not be found',
    goHome: 'Go to home ',
  },
};

export { constants };
