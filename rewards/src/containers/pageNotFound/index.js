import React from 'react';
import classNames from 'classnames';
import { constants } from './constants';
import { history, authentication } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import ImageHOC from '../../components/imageHOC';

/** @description callback function for redirecting to dashboard
   * @param {object} e - triggered event(click)
   */
const redirectToDashboard = (e) => {
  e.preventDefault();
  history.push(RouteConstants.DASHBOARD);
};

const renderPage = (isAuthenticated) => {
  const imagePath = 'images/404.png';
  return (
    <React.Fragment>
      <figure>
        <ImageHOC id="error-image" src={imagePath} alt="Page Not Found" responsive />
      </figure>
      <h6>{constants.t1.errorCode}</h6>
      <p>{constants.t1.pageNotFound}</p>
      {isAuthenticated && <a href="/" onClick={redirectToDashboard} className="goto-home">{constants.t1.goHome}<i className="icon-arrow"></i></a>}
    </React.Fragment>
  );
};

const PageNotFoundContainer = () => {
  const isAuthenticated = authentication.isAuthenticated();
  const errorClass = classNames('error-page', { 'postlogin-error': isAuthenticated });
  return (
    <div className={errorClass}>
      {renderPage(isAuthenticated)}
    </div>);
};
export default PageNotFoundContainer;
