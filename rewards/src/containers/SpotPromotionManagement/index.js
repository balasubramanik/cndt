import React, { PureComponent } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { Grid, Row, Col } from 'react-bootstrap';
import { reduxForm, FieldArray, Field, change } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import StateAndDate from '../../components/stateAndDateSelection';
import ProductSection from '../../components/productSection';
import { texts } from './constant';
import TextArea from '../../components/textArea';
import { endDateReset, displayStartDateReset, familyReset, displayEndDateReset, discardAllData, saveSpotPromotion, savePromotionPopupClose, getSpotPromotionOptions, editSpotPromotion } from '../../actionCreators/SpotPromotionManagement';
import { getStates } from '../../actionCreators/States';
import validate from './validate';
import FileUploadProgress from '../../components/fileUploadProgress';
import ErrorNotification from '../../components/errorNotification';
import Dialog from '../../components/dialog';
import TextInput from '../../components/textInput';
import Loader from '../../components/loader';
import RouteConstants from '../../constants/RouteConstants';
import { pageTitle } from '../../constants/constants';
import { history } from '../../routes';
import { formatDate, convertStringToNumber } from '../../utils/utils';

const SUCCESS_TIMEOUT = 2000;
const PROMOTION_SUBMIT_TYPE_APPROVE = 'approve';
const PROMOTION_SUBMIT_TYPE_SAVE = 'save';
const PROMOTION_SUBMIT_TYPE_INACTIVE = 'inactive';

export class SpotPromotionManagement extends PureComponent {
  constructor(props) {
    super(props);
    this.selectedProduct = {};
    this.file = '';
    this.imageName = '';
    this.uomData = {};
    this.status = this.props.promotionStatus;
    this.promotionId = this.getPromotionId();
    this.saveClicked = false;
    this.state = {
      isCancelPopup: false,
      isInactive: false,
      isSaveEnable: true,
    };
  }

  componentDidMount() {
    const {
      CATEGORY, ALLOWANCE_TYPE, UOM,
    } = texts;
    if (this.promotionId) {
      this.props.editSpotPromotion(this.promotionId);
    }
    this.props.getStates();
    this.props.getSpotPromotionOptions(CATEGORY, CATEGORY);
    this.props.getSpotPromotionOptions(ALLOWANCE_TYPE, ALLOWANCE_TYPE);
    this.props.getSpotPromotionOptions(UOM, UOM);
  }

  componentDidUpdate(prevProps) {
    const {
      isShowPopup, isApproveShowPopup, isInactivePopup, saveDone,
    } = this.props;
    if (prevProps.isShowPopup !== isShowPopup && isShowPopup) {
      this.closePopupHandler();
    }
    if (prevProps.isApproveShowPopup !== isApproveShowPopup && isApproveShowPopup) {
      this.closePopupHandler();
      setTimeout(() => {
        this.popupRedirect();
      }, SUCCESS_TIMEOUT);
    }
    if (prevProps.isInactivePopup !== isInactivePopup && isInactivePopup) {
      this.closePopupHandler();
      setTimeout(() => {
        this.popupRedirect();
      }, SUCCESS_TIMEOUT);
    }
    const { values } = this.props;
    if (prevProps.values.imageData !== values.imageData) {
      this.imageName = values.imageData.fileName;
      this.props.changeFormState('spotPromotionForm', 'image', values.imageData.fileName);
    }
    if (prevProps.saveDone !== saveDone) {
      this.saveClicked = true;
      this.saveDoneUpdate();
    }
  }

  getPromotionId = () => {
    const { match } = this.props;
    if (match && match.params && match.params.promoId) {
      return match.params.promoId;
    }
    return null;
  }

  saveDoneUpdate = () => {
    this.setState({ isSaveEnable: true });
  }

  categoryHandle = (category) => category && category.map((data) => {
    const products = data.products.map((product) => ({ title: product.productFamily, value: product.productFamily }));
    this.selectedProduct[data.category] = products;
    return ({ label: data.category, value: data.category });
  });

  uomHandle = (uom) => uom && uom.map((data) => {
    this.uomData[data.code] = data.description;
    return ({ label: data.description, value: data.code });
  });

  allowanceTypeHandle = (allowanceType) => allowanceType && allowanceType.map((data) => ({ label: data.description, value: data.code }));

  formatFormData = (values) => {
    const { DOLLOR, PERCENTAGE } = texts;
    const { id, stateItem } = this.props;
    const productsData = [];
    const selectedStates = [];
    let isAllSelected = false;
    const {
      title, states, startDate, endDate, displayStartDate, displayEndDate, products, description, disclaimer, imageData,
    } = values;
    const { blobName } = imageData;
    const startDateFormat = formatDate(startDate);
    const endDateFormat = formatDate(endDate);
    const displayStartDateFormat = formatDate(displayStartDate);
    const displayEndDateFormat = formatDate(displayEndDate);
    if (states && states.itemSelected) {
      const { isSelectAll, itemSelected } = states;
      isAllSelected = isSelectAll;
      itemSelected.forEach((data) => {
        const { value, checked } = data[1];
        if (checked) {
          selectedStates.push({
            Code: value,
            Name: data[1].title,
          });
        }
      });
    } else if (states.length > 0) {
      stateItem.forEach((item) => {
        const { code, name } = item;
        states.forEach((data) => {
          if (data === code) {
            selectedStates.push({
              Code: data,
              Name: name,
            });
          }
        });
      });
    }
    products.forEach((item) => {
      const productFamilyItem = [];
      let ValueByPercent = 0;
      let ValueByDollars = 0;
      if (item.allowanceType === DOLLOR) {
        ValueByDollars = convertStringToNumber(item.productValue);
      } else if (item.allowanceType === PERCENTAGE) {
        ValueByPercent = convertStringToNumber(item.productValue);
      }
      if (item.productFamily && item.productFamily.itemSelected) {
        item.productFamily.itemSelected.forEach((data) => {
          if (data[1].checked) {
            productFamilyItem.push(data[1].title);
          }
        });
      } else if (item.productFamily.length > 0) {
        item.productFamily.forEach((data) => {
          productFamilyItem.push(data);
        });
      }
      productsData.push({
        Category: item.category,
        Family: productFamilyItem,
        AllowanceType: item.allowanceType,
        Uom: {
          Code: item.uom || 'N/A',
          description: this.uomData[item.uom] || 'N/A',
        },
        ValueByDollars,
        ValueByPercent,
      });
    });
    return ({
      title,
      description,
      disclaimer,
      image: {
        fileName: this.imageName,
        blobUrl: null,
        blobName: !(this.file) ? blobName : null,
      },
      imageContent: this.file,
      startDate: startDateFormat,
      endDate: endDateFormat,
      displayStartDate: displayStartDateFormat,
      displayEndDate: displayEndDateFormat,
      isAllStatesApplicable: isAllSelected,
      states: selectedStates,
      categories: productsData,
      status: this.status || 'Draft',
      id: id || null,
    });
  }

  statesMenuItems = (menu, defaultMenu) =>
    menu && menu.map((item) => {
      const { name, code } = item;
      const MenuItems = { title: name, value: code };
      let selectedItems;
      if (defaultMenu && defaultMenu.length > 0) {
        defaultMenu.forEach((data) => {
          if (name === data.name) {
            selectedItems = { title: data.name, value: data.code, checked: true };
          }
        });
      }
      return selectedItems || MenuItems;
    })

  productFamilyMenuItems = (menu, defaultMenu) => menu && menu.map((item) => {
    const { title, value } = item;
    const MenuItems = { title, value };
    let selectedItems;
    if (defaultMenu && defaultMenu.length > 0) {
      defaultMenu.forEach((data) => {
        if (title === data) {
          selectedItems = { title, value, checked: true };
        }
      });
    }
    return selectedItems || MenuItems;
  })

  promotionNavigation = (e) => {
    e.preventDefault();
    history.push({
      pathname: RouteConstants.SPOTPROMOTION,
    });
  }

  // gaf title block Start
  gafTitleBlock = () => {
    const {
      HEADER, STATUS, SPOT_PROMOTIONS,
    } = texts;
    const { promotionStatus, values } = this.props;
    const statusFormat = (promotionStatus) ? promotionStatus.charAt(0) + promotionStatus.substr(1).toLowerCase() : '';
    const { title } = values;
    const headerTitle = (this.promotionId) ? <h2>{SPOT_PROMOTIONS} - {title}</h2> : <h2>{HEADER}</h2>;
    return (
      <div className="gaf-title-block">
        <Row>
          <Col md={8} sm={12} xs={12}>
            {headerTitle}
            {this.promotionId && <p>{STATUS} : {statusFormat}</p>}
          </Col>
          <Col md={4} sm={12} xs={12}>
            <div className="links">
              <a
                href={RouteConstants.SPOTPROMOTION}
                onClick={(e) => this.promotionNavigation(e)}
                className="spot-promotion-link"
              >
                {SPOT_PROMOTIONS}
              </a>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
  // gaf title block End

  FormData = () => {
    const { values } = this.props;
    this.status = 'Draft';
    this.values = this.formatFormData(values);
    this.props.saveSpotPromotion(this.values, PROMOTION_SUBMIT_TYPE_SAVE);
  }

  // Description and Disclaimer TextArea Start
  descriptionBlock = (label) => {
    const {
      ACTIVE_STATUS, PUBLISHED_STATUS, INACTIVATED_STATUS, EXPIRED_STATUS, APPROVE_STATUS,
    } = texts;
    const { promotionStatus } = this.props;
    const approveStatus = (promotionStatus === APPROVE_STATUS);
    const activePublishedStatus = ((promotionStatus === ACTIVE_STATUS) || (promotionStatus === PUBLISHED_STATUS));
    const expiredInactivatedStatus = ((promotionStatus === INACTIVATED_STATUS) || (promotionStatus === EXPIRED_STATUS));
    return (
      <Col xs={12}>
        <div className="form-group gaf-form gaf-textarea-wrap">
          <label>{label}<sup>*</sup></label>
          <Field
            name={label}
            className="form-control gaf-textarea"
            maxLength={1000}
            totalText={1000}
            component={TextArea}
            disabled={approveStatus || activePublishedStatus || expiredInactivatedStatus}
            onChange={() => this.inputChangeHandler()}
          />
        </div>
      </Col>
    );
  }
  // Description and Disclaimer TextArea End

  // Product Section added, delete function Start
  addProductHandler = (e, fields) => {
    e.preventDefault();
    if (fields.length < 10) {
      fields.push({});
    }
  }

  deleteProductHandler = (e, i, fields) => {
    e.preventDefault();
    fields.remove(i);
  }

  // Product Section added, delete function End

  // Cancel and popup function Start
  cancelAlert = (e) => {
    e.preventDefault();
    this.setState({ isCancelPopup: true });
  }

  handleClose(e) {
    e.preventDefault();
    this.setState({ isCancelPopup: false, isInactive: false });
  }

  cancelAlertPopup = () => {
    const { isCancelPopup } = this.state;
    return (
      <Dialog
        show={isCancelPopup}
        onCloseClick={(e) => this.handleClose(e)}
        body={this.cancelPopupContent()}
        className="gaf-model-popup gaf-cancel-popup"
      />
    );
  }

  DiscardData = (e) => {
    e.preventDefault();
    if (this.promotionId) {
      history.push({
        pathname: RouteConstants.SPOTPROMOTION,
      });
    } else {
      this.props.discardAllData();
      history.push({
        pathname: RouteConstants.SPOTPROMOTION,
      });
    }
  }

  cancelPopupContent = () => (
    <div className="cancel-popup">
      <p>{texts.CANCEL_ALERT_MESSAGE}</p>
      <div className="spot-promotion-action clearfix">
        <a href="/" className="btn upload-cancel-btn btn-block gaf-btn-secondary" onClick={(e) => this.handleClose(e)}>NO</a>
        <a href="/" className="btn upload-submit-btn btn-block gaf-btn-primary" onClick={(e) => this.DiscardData(e)}>YES</a>
      </div>
    </div>
  )
  // Cancel and popup function End

  errorMessageHandler = (eve, callback) => {
    eve.preventDefault();
    const { handleSubmit } = this.props;
    const submitter = handleSubmit(callback);
    submitter();
  }

  // save and cancel button Start
  promotionDraftHandler = () => {
    const {
      SAVE_BUTTON_ABOVE_TEXT, APPROVE_BUTTON_ABOVE_TEXT, APPROVE_TEXT, INACTIVATED_STATUS,
    } = texts;
    const { promotionStatus, saveDone } = this.props;
    const statusText = promotionStatus && promotionStatus.toUpperCase();
    const issaveDone = (saveDone || this.promotionId) ? 'btn btn-block gaf-btn-primary' : 'btn btn-block gaf-btn-primary btn-disabled';
    const isButtonShow = this.promotionId && promotionStatus === INACTIVATED_STATUS;
    const isSaveEnable = (!this.state.isSaveEnable || (!this.promotionId && !saveDone) || (this.saveClicked && !saveDone));
    const isSaveButtonEnable = isSaveEnable ? 'upload-submit-btn btn btn-block gaf-btn-primary' : 'upload-submit-btn btn btn-block btn-disabled btn-event-disabled gaf-btn-primary';
    return (
      <React.Fragment>
        {!isButtonShow &&
          <section>
            <p>{SAVE_BUTTON_ABOVE_TEXT}</p>
            <div className="spot-promotion-action clearfix">
              <a href="/" className="btn btn-block upload-cancel-btn gaf-btn-secondary" onClick={this.cancelAlert}>Cancel</a>
              <a href="/" className={isSaveButtonEnable} onClick={(e) => this.errorMessageHandler(e, this.FormData)}>Save</a>
            </div>
            {promotionStatus !== APPROVE_TEXT && <p>{APPROVE_BUTTON_ABOVE_TEXT}</p>}
            <div className="clearfix">
              <div className="approve-btn">
                <button type="submit" className={issaveDone} disabled={isSaveEnable} >{texts[statusText] || APPROVE_TEXT}</button>
              </div>
            </div>
          </section>}
      </React.Fragment>
    );
  }

  // save and cancel button End
  submitHandler = () => {
    const { APPROVE_STATUS } = texts;
    const { values } = this.props;
    const { promotionStatus } = this.props;
    if (promotionStatus === APPROVE_STATUS) {
      this.status = 'Inactivated';
    } else {
      this.status = 'Approved';
    }
    this.values = this.formatFormData(values);
    if (this.promotionId && promotionStatus === APPROVE_STATUS) {
      this.setState({ isInactive: true });
    } else {
      this.props.saveSpotPromotion(this.values, PROMOTION_SUBMIT_TYPE_APPROVE);
    }
  }

  inActiveSuccesss = (e) => {
    e.preventDefault();
    this.setState({ isInactive: false });
    this.props.saveSpotPromotion(this.values, PROMOTION_SUBMIT_TYPE_INACTIVE);
  }

  inActivePopupContent = () => (
    <div className="cancel-popup">
      <p>{texts.INACTIVE_MESSAGE}</p>
      <p>{texts.INACTIVE_DISCLAIMER_MESSAGE}</p>
      <div className="spot-promotion-action clearfix">
        <a href="/" className="btn upload-cancel-btn btn-block gaf-btn-secondary" onClick={(e) => this.handleClose(e)}>NO</a>
        <a href="/" className="btn upload-submit-btn btn-block gaf-btn-primary" onClick={(e) => this.inActiveSuccesss(e)}>YES</a>
      </div>
    </div>
  )

  inActiveAlertPopup = () => {
    const { isInactive } = this.state;
    const { INACTIVATION_ALERT } = texts;
    return (
      <Dialog
        show={isInactive}
        title={INACTIVATION_ALERT}
        onCloseClick={(e) => this.handleClose(e)}
        body={this.inActivePopupContent()}
        className="gaf-model-popup gaf-cancel-popup"
      />
    );
  }

  inActiveSuccess = () => {
    const { values, responseCode } = this.props;
    const { title } = values;
    const inactiveMessage = `"${title}" ${texts[responseCode]}`;
    return (
      <div className="gaf-toast-dialog">
        <span className="icon-check"></span>
        <p>{inactiveMessage}</p>
      </div>
    );
  }

  inActiveSuccessPopup = () => {
    const { isInactivePopup } = this.props;
    return (
      <Dialog
        show={isInactivePopup}
        onCloseClick={(e) => this.popupRedirect(e)}
        body={this.inActiveSuccess()}
        className="gaf-model-popup gaf-toast-main"
      />
    );
  }

  popupRedirect = (e) => {
    if (e) {
      e.preventDefault();
    }
    history.push({
      pathname: RouteConstants.SPOTPROMOTION,
    });
  }

  approvePopupContent = () => {
    const { values, responseCode } = this.props;
    const { title } = values;
    const approveMessage = `"${title}" ${texts[responseCode]}`;
    return (
      <div className="gaf-toast-dialog">
        <span className="icon-check"></span>
        <p>{approveMessage}</p>
      </div>
    );
  }

  approvePopup = () => {
    const { isApproveShowPopup } = this.props;
    return (
      <Dialog
        show={isApproveShowPopup}
        onCloseClick={(e) => this.popupRedirect(e)}
        body={this.approvePopupContent()}
        className="gaf-model-popup gaf-toast-main"
      />
    );
  }

  inputChangeHandler = () => {
    this.setState({ isSaveEnable: false });
  }

  // Image upload function Start
  imageUploadHandler = () => {
    const {
      DRAFT_TEXT, ALLOW_FORMATS, MAX_FILE_COUNT, MAX_FILE_SIZE, ERROR_PLACEHOLDER, UPLOADE_NOTE, UPLOAD_TEXT, FILE_COUNT_EXCEED_MSG,
    } = texts;
    const {
      syncErrors, anyTouched, values, promotionStatus,
    } = this.props;
    const { image } = syncErrors;
    const { fileName, blobUrl } = values.imageData;
    const isDisabled = this.promotionId && promotionStatus !== DRAFT_TEXT;
    return (
      <div className="upload-new-spot-promotion">
        <Row>
          <Col xs={12}>
            <p>{UPLOADE_NOTE}</p>
            <div className="invoice-upload-files">
              <FileUploadProgress
                allowFormats={ALLOW_FORMATS}
                maxFileCount={MAX_FILE_COUNT}
                maxFileSize={MAX_FILE_SIZE}
                inValidFiles={ERROR_PLACEHOLDER}
                getFiles={this.imageGetter}
                type
                files={[{ name: fileName, fileType: 'image/png', file: blobUrl }]}
                isCloseDisabled={isDisabled}
                fileCountExceedMsg={FILE_COUNT_EXCEED_MSG}
              />
            </div>
            {image && anyTouched && <p className="gaf-error-message">{UPLOAD_TEXT}</p>}
          </Col>
        </Row>
      </div>
    );
  }

  imageGetter = (image) => {
    const { PNG_BLOB_REPLACE, JPG_BLOB_REPLACE } = texts;
    if (image[0]) {
      this.file = image[0].blob.replace(PNG_BLOB_REPLACE, '').replace(JPG_BLOB_REPLACE, '');
      const name = image && image[0] ? image[0].name : '';
      this.imageName = name;
      this.props.changeFormState('spotPromotionForm', 'image', name);
    } else if (image.length === 0) {
      this.props.changeFormState('spotPromotionForm', 'image', null);
    }
    this.inputChangeHandler();
  }
  // Image upload function End

  // promotion success popup content start
  savePopupContent = () => {
    const { responseCode, values } = this.props;
    const { title } = values;
    const { SAVE_SUCCESS_CODE } = texts;
    const draftMessage = (responseCode === SAVE_SUCCESS_CODE) ? `"${title}" ${texts[responseCode]}` : `${texts[responseCode]}`;
    return (
      <div className="gaf-toast-dialog">
        <span className="icon-check"></span>
        <p>{draftMessage}</p>
      </div>
    );
  }

  promotionSuccessPopupClose = (e) => {
    e.preventDefault();
    this.props.savePromotionPopupClose();
  }
  promotionSuccessPopup = () => {
    const { isShowPopup } = this.props;
    return (
      <Dialog
        show={isShowPopup}
        onCloseClick={(e) => this.promotionSuccessPopupClose(e)}
        body={this.savePopupContent()}
        className="gaf-model-popup gaf-toast-main"
      />
    );
  }

  closePopupHandler = () => {
    setTimeout(() => {
      this.props.savePromotionPopupClose();
    }, SUCCESS_TIMEOUT);
  }
  // promotion success popup content end

  uomResetHandler = (value, data, index) => {
    const { values } = this.props;
    const { products = {} } = values;
    const updatedProduct = [...products];
    if (updatedProduct[index]) {
      const uomReset = {
        uom: '',
        allowanceType: value,
      };
      this.props.familyReset(uomReset, index);
    }
  }

  fieldResetHandler = (value, data, index) => {
    const { itemSelected } = value;
    const checkedItems = itemSelected.filter((item) => item[1].checked);
    if (checkedItems.length === 0) {
      const updatedProduct = {
        productFamily: value,
        uom: '',
        allowanceType: '',
        productValue: '',
      };
      this.props.familyReset(updatedProduct, index);
    }
  }

  productFamilyResetHandler = (category, data, index) => {
    const { values } = this.props;
    const { products = {} } = values;
    const updatedProduct = [...products];
    if (updatedProduct[index]) {
      const selectedProduct = {
        ...updatedProduct[index],
        category,
        itemSelected: [...data || []].map((item) => {
          const { title, value } = item;
          return ([title, { value, title, checked: false }]);
        }),
        selectedProductFamily: [],
      };
      updatedProduct.splice(index, 1);
      updatedProduct.splice(index, 0, selectedProduct);
    }
    this.props.familyReset(updatedProduct, index);
  }

  // product render Start
  renderProduct = ({
    fields, products, category, allowanceType, uom,
  }) => {
    if (fields.length === 0) {
      fields.push({});
    }
    const {
      values, promotionStatus, syncErrors, anyTouched,
    } = this.props;
    const { products: productError } = syncErrors;
    const isAddDisabled = (this.promotionId && promotionStatus === texts.DRAFT_TEXT) || !this.promotionId;
    const addBtnClass = classNames('gaf-add-button gaf-btn-secondary', { 'btn-disabled': (products && products.length === 10) || !isAddDisabled });
    const addButtonDisabled = !isAddDisabled || (products && products.length === 10);
    return (
      <div>
        {
          fields.map((name, i) =>
            (
              <ProductSection
                key={i.toString()}
                name={name}
                onDelete={(e) => this.deleteProductHandler(e, i, fields)}
                details={products[i]}
                index={i}
                category={category}
                productFamily={this.productFamilyMenuItems(this.selectedProduct[values.products[i].category], values.products[i].selectedProductFamily)}
                allowanceType={allowanceType}
                uom={uom}
                values={values}
                disabled={promotionStatus}
                deleteDisabled={isAddDisabled}
                fieldChangeHandler={this.inputChangeHandler}
                familyDataReset={(value) => this.productFamilyResetHandler(value, this.selectedProduct[values.products[i].category], i)}
                uomResetHandler={(value) => this.uomResetHandler(value, values.products[i], i)}
                fieldResetHandler={(value) => this.fieldResetHandler(value, values.products[i], i)}
                syncError={productError[i]}
                anyTouch={anyTouched}
              />
            ))
        }
        <div className="add-new-spot-promotion clearfix" >
          <Row>
            <Col xs={12}>
              <button onClick={(e) => this.addProductHandler(e, fields)} disabled={addButtonDisabled} className={addBtnClass}><i className="icon-add"></i>add</button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
  // product render End

  render() {
    const { DESCRIPTION, DISCLAIMER } = texts;
    const {
      values, productFamily, stateItem, category, allowanceType, uom, promotionStatus, isLoading, error,
    } = this.props;
    const { products } = values;
    return (
      <main className="main-content">
        <Helmet>
          <title>{pageTitle.SPOTPROMOTION_MANAGEMENT}</title>
        </Helmet>
        <Grid>
          <Row className="show-grid">
            <Col xs={12}>
              {this.gafTitleBlock()}
              <form name="spotPromotionForm" className="spotPromotionForm" onSubmit={(e) => this.errorMessageHandler(e, this.submitHandler)} autoComplete="off" >
                <div className="gaf-global-main cleafix">
                  <Row>
                    <Col xs={12}>
                      <StateAndDate
                        values={values}
                        action={{
                          endDateResetAction: this.props.endDateReset,
                          displayStartDateResetAction: this.props.displayStartDateReset,
                          displayEndDateResetAction: this.props.displayEndDateReset,
                        }}
                        states={this.statesMenuItems(stateItem, values.selectedStates)}
                        disabled={promotionStatus}
                        fieldChangeHandler={this.inputChangeHandler}
                      />
                    </Col>
                  </Row>
                  <FieldArray name="products" productFamily={productFamily} products={products} category={this.categoryHandle(category)} allowanceType={this.allowanceTypeHandle(allowanceType)} component={this.renderProduct} uom={this.uomHandle(uom)} categoryOnChange={this.categoryOnChange} />
                  {this.imageUploadHandler()}
                  <Field
                    id="uploadImage"
                    name="image"
                    className="form-group gaf-form hidden"
                    component={TextInput}
                    type="text"
                  />
                  <div className="gaf-global-box">
                    <div className="desc-new-spot-promotion">
                      <Row>
                        {this.descriptionBlock(DESCRIPTION)}
                        {this.descriptionBlock(DISCLAIMER)}
                      </Row>
                    </div>
                    {isLoading && <Loader />}
                    <ErrorNotification error={error} onClear={(ev) => this.promotionSuccessPopupClose(ev)} />
                    {this.promotionDraftHandler()}
                    {this.cancelAlertPopup()}
                    {this.promotionSuccessPopup()}
                    {this.approvePopup()}
                    {this.inActiveAlertPopup()}
                    {this.inActiveSuccessPopup()}
                  </div>
                </div>
              </form>
            </Col>
          </Row>
        </Grid>
      </main>
    );
  }
}
function mapStateToProps(state, match) {
  const { form, states } = state;
  const { spotPromotionForm } = form;
  const { states: stateItem } = states;
  const {
    values, productFamily, isShowPopup, isInactivePopup, saveDone, syncErrors, anyTouched, category, allowanceType, uom, isApproveShowPopup, promotionStatus, id, isLoading, error, responseCode,
  } = spotPromotionForm;
  return {
    values,
    isShowPopup,
    isApproveShowPopup,
    isInactivePopup,
    saveDone,
    syncErrors,
    anyTouched,
    stateItem,
    category,
    allowanceType,
    uom,
    productFamily,
    match,
    promotionStatus,
    id,
    isLoading,
    error,
    responseCode,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    discardAllData: bindActionCreators(discardAllData, dispatch),
    saveSpotPromotion: bindActionCreators(saveSpotPromotion, dispatch),
    savePromotionPopupClose: bindActionCreators(savePromotionPopupClose, dispatch),
    changeFormState: bindActionCreators(change, dispatch),
    getSpotPromotionOptions: bindActionCreators(getSpotPromotionOptions, dispatch),
    getStates: bindActionCreators(getStates, dispatch),
    editSpotPromotion: bindActionCreators(editSpotPromotion, dispatch),
    endDateReset: bindActionCreators(endDateReset, dispatch),
    displayStartDateReset: bindActionCreators(displayStartDateReset, dispatch),
    displayEndDateReset: bindActionCreators(displayEndDateReset, dispatch),
    familyReset: bindActionCreators(familyReset, dispatch),
  };
}

SpotPromotionManagement.propTypes = {
  values: PropTypes.object,
  handleSubmit: PropTypes.func,
  endDateReset: PropTypes.func,
  displayEndDateReset: PropTypes.func,
  displayStartDateReset: PropTypes.func,
  discardAllData: PropTypes.func,
  saveSpotPromotion: PropTypes.func,
  isShowPopup: PropTypes.bool,
  isApproveShowPopup: PropTypes.bool,
  isInactivePopup: PropTypes.bool,
  savePromotionPopupClose: PropTypes.func,
  changeFormState: PropTypes.func,
  syncErrors: PropTypes.object,
  anyTouched: PropTypes.bool,
  getSpotPromotionOptions: PropTypes.func,
  stateItem: PropTypes.array,
  category: PropTypes.array,
  productFamily: PropTypes.array,
  allowanceType: PropTypes.array,
  uom: PropTypes.array,
  editSpotPromotion: PropTypes.func,
  promotionStatus: PropTypes.string,
  id: PropTypes.string,
  match: PropTypes.object,
  isLoading: PropTypes.bool,
  saveDone: PropTypes.bool,
  getStates: PropTypes.func,
  familyReset: PropTypes.func,
  error: PropTypes.func,
  responseCode: PropTypes.string,
};

const Component = connect(mapStateToProps, mapDispatchToProps)(withRouter(SpotPromotionManagement));

export default (reduxForm({
  formCustomizer: (form) => form,
  form: 'spotPromotionForm',
  validate,
  touchOnBlur: false,
})(Component));
