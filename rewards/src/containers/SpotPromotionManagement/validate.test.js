import validate from './validate';

describe('validate', () => {
  it('description should be come', () => {
    const errors = validate({});
    expect(errors.description).toBe('This field is required');
  });
  it('description should be come', () => {
    const errors = validate({ description: true });
    expect(errors.description).toBe(undefined);
  });
  it('disclaimer should be come', () => {
    const errors = validate({});
    expect(errors.disclaimer).toBe('This field is required');
  });
  it('disclaimer should be come', () => {
    const errors = validate({ disclaimer: true });
    expect(errors.disclaimer).toBe(undefined);
  });
  it('image lenth should be come', () => {
    const errors = validate({});
    expect(errors.image).toBe(true);
  });
  it('image should be come', () => {
    const errors = validate({ image: true });
    expect(errors.image).toBe(undefined);
  });
});
