import { validate as StateAndDate } from '../../components/stateAndDateSelection/validate';
import { validate as productSection } from '../../components/productSection/validate';
import { texts } from '../SpotPromotionManagement/constant';

const validate = (values) => {
  const errors = {};
  if (!values.description) {
    errors.description = texts.REQUIRED_FIELD;
  }
  if (!values.disclaimer) {
    errors.disclaimer = texts.REQUIRED_FIELD;
  }
  if (!values.image) {
    errors.image = true;
  }
  return errors;
};

export default function (values) {
  return {
    ...StateAndDate(values),
    ...productSection(values),
    ...validate(values),
  };
}
