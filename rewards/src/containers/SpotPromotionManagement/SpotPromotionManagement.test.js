import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import { history } from '../../routes';
import SpotPromotionManage, { SpotPromotionManagement } from '../../containers/SpotPromotionManagement';
import { fakeStore } from '../../config/jest/fakeStore';

configure({ adapter: new Adapter() });
jest.useFakeTimers();
history.push = jest.fn();

describe('SpotPromotionManagement', () => {
  const preventDefault = jest.fn();
  const saveSpotPromotion = jest.fn();
  const getStates = jest.fn();
  const getSpotPromotionOptions = jest.fn();
  const changeFormState = jest.fn();
  const editSpotPromotion = jest.fn();
  const savePromotionPopupClose = jest.fn();
  const familyReset = jest.fn();
  const handleSubmit = jest.fn();
  const values = {
    description: 'One Day Only Timberline Offer! ',
    disclaimer: 'Lorem ipsum',
    displayEndDate: '12/20/2018',
    displayStartDate: '12/19/2018',
    endDate: '12/19/2018',
    image: 'GAF_Logo_jpg.jpg',
    startDate: '12/19/2018',
    title: 'One Day Rebate',
    states: {
      isSelectAll: true,
      itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
    },
    imageData: {},
    products: [{
      allowanceType: 'DOL',
      category: 'Timberline® Lifetime Shingles',
      productValue: 2,
      uom: '',
      selectedProductFamily: ['Timberline HD®'],
      productFamily: {
        isSelectAll: false,
        itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
      },
    }],
    selectedStates: ['Alabama', 'Alaska'],
  };
  const props = {
    stateItem: [{ title: 'Alabama', value: 'AL', checked: true }, { title: 'Alaska', value: 'AK', checked: true }],
    isShowPopup: true,
    planId: 'SP01',
    values: {
      imageData: {},
    },
    saveDone: true,
    match: {
      params: {
        promoId: 'SP01',
      },
    },
    isApproveShowPopup: true,
    isInactivePopup: true,
    responseCode: '05101',
    promotionStatus: 'APPROVE',
  };
  const tree = shallow(<SpotPromotionManagement
    {...props}
    handleSubmit={handleSubmit}
    getStates={getStates}
    savePromotionPopupClose={savePromotionPopupClose}
    editSpotPromotion={editSpotPromotion}
    changeFormState={changeFormState}
    getSpotPromotionOptions={getSpotPromotionOptions}
    isLoading
    familyReset={familyReset}
    APPROVE_TEXT="APPROVE"
    saveSpotPromotion={saveSpotPromotion}
    values={{
      description: 'One Day Only Timberline Offer! ',
      disclaimer: 'Lorem ipsum',
      displayEndDate: '12/20/2018',
      displayStartDate: '12/19/2018',
      endDate: '12/19/2018',
      image: 'GAF_Logo_jpg.jpg',
      startDate: '12/19/2018',
      title: 'One Day Rebate',
      states: {
        isSelectAll: true,
        itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
      },
      imageData: {},
      products: [{
        allowanceType: 'PCT',
        category: 'Timberline® Lifetime Shingles',
        productValue: 2,
        uom: '',
        selectedProductFamily: ['Timberline HD®'],
        productFamily: {
          isSelectAll: false,
          itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
        },
      }],
      selectedStates: ['Alabama', 'Alaska'],
    }}
    syncErrors={{
      image: true,
      description: 'This field is required',
      disclaimer: 'This field is required',
      startDate: 'This field is required',
      title: 'This field is required',
      products: [{ category: 'This field is required' }],
    }}
    category={[{
      category: 'Three-Tab Shingles', products: [{ productFamily: 'Royal Sovereign®' }, { productFamily: 'Marquis® WeatherMax®' }],
    }]}
    uom={[{ code: 'PP', description: 'Pails' }, { code: 'CY', description: 'Cylinder' }]}
    allowanceType={[{ code: 'DOL', description: 'Dollar' }, { code: 'PCT', description: 'Percentage' }]}
    stateItem={[{ title: 'Alabama', value: 'AL', checked: true }, { title: 'Alaska', value: 'AK', checked: true }]}
    anyTouched
    image
  />);
  it('should call component did update1', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isShowPopup: false,
      values: {
        imageData: {
          blobName: '4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          blobUrl: 'https://rewardsuatcdn.gaf.com/SpotPromotion/4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          fileName: 'GAF_Logo_jpg.jpg',
        },
      },
    });
    jest.runAllTimers();
    expect(savePromotionPopupClose).toBeCalled();
  });

  it('should call component did update2', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      values: {
        imageData: {
          blobName: '4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          blobUrl: 'https://rewardsuatcdn.gaf.com/SpotPromotion/4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          fileName: 'GAF_Logo_jpg.jpg',
        },
      },
      isShowPopup: true,
    });
    jest.runAllTimers();
    expect(savePromotionPopupClose).toBeCalled();
  });

  it('should call component did update4', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isApproveShowPopup: false,
      values: {
        imageData: {
          blobName: '4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          blobUrl: 'https://rewardsuatcdn.gaf.com/SpotPromotion/4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          fileName: 'GAF_Logo_jpg.jpg',
        },
      },
    });
    jest.runAllTimers();
    expect(savePromotionPopupClose).toBeCalled();
  });

  it('should call component did update5', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isInactivePopup: false,
      values: {
        imageData: {
          blobName: '4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          blobUrl: 'https://rewardsuatcdn.gaf.com/SpotPromotion/4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          fileName: 'GAF_Logo_jpg.jpg',
        },
      },
    });
    jest.runAllTimers();
    expect(savePromotionPopupClose).toBeCalled();
  });

  it('should call component did update6', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      values: {
        imageData: {
          blobName: '4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          blobUrl: 'https://rewardsuatcdn.gaf.com/SpotPromotion/4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
          fileName: 'GAF_Logo_jpg.jpg',
        },
      },
    });
    jest.runAllTimers();
    expect(changeFormState).toBeCalled();
  });

  it('should call for ErrorNotification ', () => {
    const simulateClick = tree.find('ErrorNotification').at(0).prop('onClear');
    simulateClick({ preventDefault });
    expect(savePromotionPopupClose).toBeCalled();
  });


  it('should call for cancelAlertPopup  ', () => {
    const simulateClick = tree.find('Dialog').at(0).prop('onCloseClick');
    simulateClick({ preventDefault });
    expect(tree.state().isCancelPopup).toBe(false);
    expect(tree.state().isInactive).toBe(false);
  });

  it('should call for inActiveAlertPopup   ', () => {
    const simulateClick = tree.find('Dialog').at(1).prop('onCloseClick');
    simulateClick({ preventDefault });
    expect(tree.state().isCancelPopup).toBe(false);
    expect(tree.state().isInactive).toBe(false);
  });

  it('should call for inActiveSuccessPopup    ', () => {
    const simulateClick = tree.find('Dialog').at(2).prop('onCloseClick');
    simulateClick({ preventDefault });
  });
  it('should call for inActiveSuccessPopup    ', () => {
    const simulateClick = tree.find('Dialog').at(2).prop('onCloseClick');
    simulateClick();
  });

  it('should be defined', () => {
    expect(SpotPromotionManagement).toBeDefined();
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should render formatFormData', () => {
    tree.instance().formatFormData(values);
  });
  it('should render DiscardData ', () => {
    tree.instance().DiscardData({ preventDefault });
  });
  it('should render productFamilyResetHandler ', () => {
    tree.instance().productFamilyResetHandler('Timberline® Lifetime Shingles', [{ title: 'Royal Sovereign®', value: 'Royal Sovereign®' }, { title: 'Marquis® WeatherMax®', value: 'Marquis® WeatherMax®' }], 0);
  });
  it('should render imageGetter  ', () => {
    tree.instance().imageGetter({ name: 'sample.jpg' });
  });
  it('should render productFamilyMenuItems ', () => {
    tree.instance().productFamilyMenuItems([{ title: 'Royal Sovereign®', value: 'Royal Sovereign®' }, { title: 'Marquis® WeatherMax®', value: 'Marquis® WeatherMax®' }], ['Royal Sovereign®', 'Marquis® WeatherMax®']);
  });

  it('should render submitHandler  ', () => {
    tree.instance().submitHandler(values);
  });

  it('should render FormData  ', () => {
    tree.instance().FormData(values);
  });
  it('should render inActiveSuccesss  ', () => {
    tree.instance().inActiveSuccesss({ preventDefault });
  });

  it('should render inActiveSuccesss  ', () => {
    tree.instance().inActiveSuccesss({ preventDefault });
  });

  it('should render uomResetHandler  ', () => {
    tree.instance().uomResetHandler('SQ', [{
      allowanceType: 'PCT',
      category: 'Timberline® Lifetime Shingles',
      productValue: 2,
      uom: 'SQ',
      selectedProductFamily: ['Timberline HD®'],
      productFamily: {
        isSelectAll: false,
        itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
      },
    }], 0);
  });
  it('should render fieldResetHandler  ', () => {
    tree.instance().fieldResetHandler([['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®', checked: false }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]], [{
      allowanceType: 'PCT',
      category: 'Timberline® Lifetime Shingles',
      productValue: 2,
      uom: 'SQ',
      selectedProductFamily: ['Timberline HD®'],
      productFamily: {
        isSelectAll: false,
        itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®', checked: true }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
      },
    }], 0);
  });
});

describe('SpotPromotionManagement', () => {
  const getStates = jest.fn();
  const getSpotPromotionOptions = jest.fn();
  const changeFormState = jest.fn();
  const editSpotPromotion = jest.fn();
  const savePromotionPopupClose = jest.fn();
  const values = {
    description: 'One Day Only Timberline Offer! ',
    disclaimer: 'Lorem ipsum',
    displayEndDate: '12/20/2018',
    displayStartDate: '12/19/2018',
    endDate: '12/19/2018',
    image: 'GAF_Logo_jpg.jpg',
    startDate: '12/19/2018',
    title: 'One Day Rebate',
    states: ['Alabama', 'Alaska'],
    imageData: {},
    products: [{
      allowanceType: 'PCT',
      category: 'Timberline® Lifetime Shingles',
      productValue: 2,
      uom: '',
      selectedProductFamily: ['Timberline HD®'],
      productFamily: ['Timberline® Natural Shadow®', 'Timberline HD®'],
    }],
    selectedStates: ['Alabama', 'Alaska'],
  };
  const props = {
    isShowPopup: true,
    planId: 'SP01',
    values: {
      imageData: {},
    },
    saveDone: false,
    match: {
      params: {
        promoId: '',
      },
    },
    promotionStatus: '',
    stateItem: [{ title: 'Alabama', value: 'AL', checked: true }, { title: 'Alaska', value: 'AK', checked: true }],
  };
  const tree = shallow(<SpotPromotionManagement
    {...props}
    getStates={getStates}
    savePromotionPopupClose={savePromotionPopupClose}
    editSpotPromotion={editSpotPromotion}
    changeFormState={changeFormState}
    getSpotPromotionOptions={getSpotPromotionOptions}
    values={{
      description: 'One Day Only Timberline Offer! ',
      disclaimer: 'Lorem ipsum',
      displayEndDate: '12/20/2018',
      displayStartDate: '12/19/2018',
      endDate: '12/19/2018',
      image: 'GAF_Logo_jpg.jpg',
      startDate: '12/19/2018',
      title: 'One Day Rebate',
      states: {
        isSelectAll: true,
        itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
      },
      imageData: {},
      products: [{
        allowanceType: 'PCT',
        category: 'Timberline® Lifetime Shingles',
        productValue: 2,
        uom: '',
        selectedProductFamily: ['Timberline HD®'],
        productFamily: {
          isSelectAll: false,
          itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
        },
      }],
    }}
    syncErrors={{
      image: true,
      description: 'This field is required',
      disclaimer: 'This field is required',
      startDate: 'This field is required',
      title: 'This field is required',
      products: [{ category: 'This field is required' }],
    }}
  />);
  it('should render formatFormData', () => {
    tree.instance().formatFormData(values);
  });
  it('should be defined', () => {
    expect(SpotPromotionManagement).toBeDefined();
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});

describe('SpotPromotionManagement mount', () => {
  const preventDefault = jest.fn();
  const getStates = jest.fn();
  const handleSubmit = jest.fn();
  const submitHandler = jest.fn();
  const getSpotPromotionOptions = jest.fn();
  const changeFormState = jest.fn();
  const editSpotPromotion = jest.fn();
  const savePromotionPopupClose = jest.fn();
  const familyReset = jest.fn();
  const state = {
    form: {
      spotPromotionForm: {
        values: {
          description: 'One Day Only Timberline Offer! ',
          disclaimer: 'Lorem ipsum',
          displayEndDate: '12/20/2018',
          displayStartDate: '12/19/2018',
          endDate: '12/19/2018',
          image: 'GAF_Logo_jpg.jpg',
          startDate: '12/19/2018',
          title: 'One Day Rebate',
          imageData: {
            blobName: '4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
            blobUrl: 'https://rewardsuatcdn.gaf.com/SpotPromotion/4f7f320c-d43c-44c2-ac47-bb2335373a6c_GAF_Logo_jpg.jpg',
            fileName: 'GAF_Logo_jpg.jpg',
          },
          states: {
            isSelectAll: true,
            itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
          },
          products: [{
            allowanceType: 'PCT',
            category: 'Timberline® Lifetime Shingles',
            productValue: 2,
            uom: '',
            selectedProductFamily: ['Timberline HD®'],
            productFamily: {
              isSelectAll: false,
              itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
            },
          }],
        },
        allowanceType: [{ code: 'DOL', description: 'Dollar' }, { code: 'PCT', description: 'Percentage' }],
        category: [{
          category: 'Three-Tab Shingles', products: [{ productFamily: 'Royal Sovereign®' }, { productFamily: 'Marquis® WeatherMax®' }],
        }],
        isApproveShowPopup: true,
        isLoading: false,
        isShowPopup: false,
        planId: 'SP050',
        promotionStatus: 'APPROVED',
        saveDone: false,
        syncErrors: {
          products: [{}],
        },
        uom: [{ code: 'PP', description: 'Pails' }, { code: 'CY', description: 'Cylinder' }],
      },
    },
    states: [{ title: 'Alabama', value: 'AL', checked: true }, { title: 'Alaska', value: 'AK', checked: true }],
  };
  const props = {
    isShowPopup: true,
    planId: 'SP01',
    values: {
      imageData: {},
    },
    saveDone: false,
    match: {
      params: {
        promoId: '',
      },
    },
    promotionStatus: 'Draft',
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <SpotPromotionManage
          {...props}
          DRAFT_TEXT="DRAFT"
          handleSubmit={handleSubmit}
          getStates={getStates}
          savePromotionPopupClose={savePromotionPopupClose}
          editSpotPromotion={editSpotPromotion}
          changeFormState={changeFormState}
          getSpotPromotionOptions={getSpotPromotionOptions}
          familyReset={familyReset}
          values={{
            description: 'One Day Only Timberline Offer! ',
            disclaimer: 'Lorem ipsum',
            displayEndDate: '12/20/2018',
            displayStartDate: '12/19/2018',
            endDate: '12/19/2018',
            image: 'GAF_Logo_jpg.jpg',
            startDate: '12/19/2018',
            title: 'One Day Rebate',
            states: {
              isSelectAll: true,
              itemSelected: [['Alabama', { title: 'Alabama', value: 'AL', checked: true }], ['Alaska', { title: 'Alaska', value: 'AK', checked: true }]],
            },
            imageData: {},
            products: [{
              allowanceType: 'PCT',
              category: 'Timberline® Lifetime Shingles',
              productValue: 2,
              uom: '',
              selectedProductFamily: ['Timberline HD®'],
              productFamily: {
                isSelectAll: false,
                itemSelected: [['Timberline® Natural Shadow®', { title: 'Timberline® Natural Shadow®', value: 'Timberline® Natural Shadow®' }], ['Timberline HD®', { title: 'Timberline HD®', value: 'Timberline HD®', checked: true }]],
              },
            }],
          }}
          syncErrors={{
            image: true,
            description: 'This field is required',
            disclaimer: 'This field is required',
            startDate: 'This field is required',
            title: 'This field is required',
            products: [{ category: 'This field is required' }],
          }}
        />
      </Router>
    </Provider>);
  it('should call for form ', () => {
    const simulateClick = tree.find('form').at(0).prop('onSubmit');
    simulateClick({ preventDefault }, submitHandler);
    expect(handleSubmit).toBeCalled();
  });

  it('should call for productFamilyResetHandler field with data', () => {
    const simulateClick = tree.find('ProductSection').at(0).prop('familyDataReset');
    simulateClick('Timberline® Lifetime Shingles', [{ title: 'Royal Sovereign®', value: 'Royal Sovereign®' }, { title: 'Marquis® WeatherMax®', value: 'Marquis® WeatherMax®' }], 0);
    expect(familyReset).toBeCalled();
  });

  it('should call for StateAndDate field with data', () => {
    const simulateClick = tree.find('StateAndDate').at(0).prop('fieldChangeHandler');
    simulateClick();
    expect(tree.state().isSaveEnable).toBe(false);
  });

  it('should be defined', () => {
    expect(SpotPromotionManagement).toBeDefined();
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
