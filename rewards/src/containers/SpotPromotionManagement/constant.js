const texts = {
  HEADER: 'Create New Spot Promotion',
  SPOT_PROMOTIONS: 'Spot Promotion',
  DESCRIPTION: 'description',
  DISCLAIMER: 'disclaimer',
  SAVE_BUTTON_ABOVE_TEXT: 'If you would like to save your work and return later to complete this promotion, click SAVE below. If you do not want to save your work and do not want to move this promotion along, click CANCEL.',
  APPROVE_BUTTON_ABOVE_TEXT: 'If you wanted to apply this promotion to all the eligible contractors, please click APPROVE below. Please note that it will be displyed to the contractors based on the dates you entered above.',
  UPLOADE_NOTE: 'Note: Please upload a high-resolution PNG/JPG image 1140 x 400 in dimension',
  UPLOAD_TEXT: 'Please Upload image',
  ALLOW_FORMATS: ['image/png', 'image/jpeg'],
  MAX_FILE_COUNT: 1,
  MAX_FILE_SIZE: 5,
  ERROR_PLACEHOLDER: 'Please upload .png/.jpg image only.',
  CANCEL_ALERT_MESSAGE: 'Are you sure you want to leave this page?',
  STATE: 'state',
  STATE_KEY: 'states',
  STATE_FILTER: 'usa',
  CATEGORY: 'category',
  PRODUCT: 'product',
  PRODUCT_FAMILY: 'productFamily',
  ALLOWANCE_TYPE: 'allowanceType',
  UOM: 'uom',
  DRAFT: 'APPROVE',
  APPROVED: 'INACTIVATE',
  PENDING: 'INACTIVE',
  ACTIVE: 'INACTIVE',
  ACTIVE_STATUS: 'ACTIVE',
  PUBLISHED_STATUS: 'PUBLISHED',
  INACTIVATED_STATUS: 'INACTIVATED',
  EXPIRED_STATUS: 'EXPIRED',
  APPROVE_STATUS: 'APPROVED',
  STATUS: 'Status',
  DRAFT_TEXT: 'DRAFT',
  APPROVE_TEXT: 'APPROVE',
  ERROR_MESSAGE_SAVE: 'Please fill out all the required fields indicated with * to save this promotion.',
  ERROR_MESSAGE_APPROVE: 'Please fill out all the required fields indicated with * to approve this promotion.',
  DOLLOR: 'DOL',
  PERCENTAGE: 'PCT',
  INACTIVATION_ALERT: 'Inactivation Alert',
  INACTIVE_MESSAGE: 'Are you sure you want to inactivate this promotion?',
  INACTIVE_TEXT: 'INACTIVATED',
  INACTIVE_DISCLAIMER_MESSAGE: 'Please note: Inactivating this promotion will remove all details that are currently being displayed to all the eligible contractors.',
  PNG_BLOB_REPLACE: 'data:image/png;base64,',
  JPG_BLOB_REPLACE: 'data:image/jpeg;base64,',
  FILE_COUNT_EXCEED_MSG: 'Sorry, you can upload only one image.',
  SAVE_SUCCESS_CODE: '05101',
  REQUIRED_FIELD: 'This field is required',
  '05101': 'has been successfully saved.',
  '05105': 'has been successfully approved.',
  '05106': 'Please click Approve to apply this promotion to all the contractors.',
  '05107': 'has been successfully inactivated.',
  '05104': 'Promotion end dates have been updated.',
};
export { texts };
