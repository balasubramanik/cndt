import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import DashboardCont, { DashboardContainer } from '../../containers/dashboard';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';

configure({ adapter: new Adapter() });

history.push = jest.fn();
window.open = jest.fn();
describe('DashboardContainer', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const accessPage = jest.fn();
  const getPromotions = jest.fn();
  const tree = shallow(<DashboardContainer
    getPromotions={getPromotions}
    actions={{
      accessPage,
    }}
    spotPromotionsCategory={[{
      StartDate: '2018-08-01T16:57:19.0487212+05:30',
      EndDate: '2018-08-011T16:57:19.0487212+05:30',
    }]}
  />);
  it('should be defined', () => {
    expect(DashboardContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
    tree.instance().onQuickAccessClick(event);
  });

  it('should render dashboard container', () => {
    expect(tree.find('.main-content').length).toBe(1);
  });

  it('should render MyProfile', () => {
    expect(tree.find('MyProfile').length).toBe(1);
  });

  it('should call viewDetailsHandler', () => {
    const simulateClick = tree.find('MyProfile').prop('onViewDetailsClick');
    simulateClick(event);
    expect(history.push).toBeCalled();
  });

  it('should render Promotions container', () => {
    expect(tree.find('.promotions-list').length).toBe(1);
  });

  it('should render PromotionalBanner', () => {
    expect(tree.find('PromotionalBanner').length).not.toBe(0);
  });

  it('should render QuickAccess', () => {
    expect(tree.find('QuickAccess').length).toBe(1);
  });

  it('should call onQuickAccessClick ', () => {
    const simulateClick = tree.find('QuickAccess').prop('onQuickAccess');
    simulateClick(event);
    expect(accessPage).toBeCalled();
  });
});

describe('HeaderContainer with connected component', () => {
  window.open = jest.fn();
  const state = {
    headerState: {
      cartItemsCount: 1,
    },
    myProfileState: {
      userInfo: {
        email: 'Brain@gaf.com',
        firstName: 'Brain',
        lastName: 'Granger',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
        contractInfo: {
          contractorName: 'Queens Roofing and Tinsmithing Inc',
          contractorImage: 'tinsmiths.jpg',
        },
      },
      rewardsInfo: {
        rewardsPoint: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    },
    whatQualifiesState: {
      spotPromotionsCategory: [
        {
          StartDate: '2018-08-01T16:57:19.0487212+05:30',
          EndDate: '2018-08-011T16:57:19.0487212+05:30',
        },
      ],
    },
    promotionsState: {
      promotionsInfo: [{
        StartDate: '2018-08-01T16:57:19.0487212+05:30',
        EndDate: '2018-08-011T16:57:19.0487212+05:30',
      }],
    },
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <DashboardCont
          actions={{
            accessPage: jest.fn(),
          }}
        />
      </Router>
    </Provider>);

  it('should be defined', () => {
    expect(DashboardCont).toBeDefined();
  });

  it('should render dashboard container', () => {
    expect(tree.find('.main-content').length).toBe(1);
  });

  it('should render MyProfile', () => {
    expect(tree.find('.gaf-myprofile').length).toBe(1);
  });

  it('should render Promotions container', () => {
    expect(tree.find('.promotions-list').length).toBe(1);
  });

  it('should render PromotionalBanner', () => {
    expect(tree.find('PromotionalBanner').length).not.toBe(0);
  });
});
