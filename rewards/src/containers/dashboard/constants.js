const coreFeatures = {
  contractor: [
    { title: 'What Qualifies', url: '', isExternal: false },
    { title: 'Submit Invoices', url: '', isExternal: false },
    { title: 'Redeem Points', url: '/', isExternal: false },
  ],
};

export { coreFeatures };
