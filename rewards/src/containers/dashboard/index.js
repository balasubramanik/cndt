import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { Grid, Col, Row } from 'react-bootstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Helmet } from 'react-helmet';
import MyProfile from '../../components/myProfile';
import QuickAccess from '../../components/quickAccess';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import { getCategoryPromotions, updateSpotPromotionSelectedYear } from '../../actionCreators/WhatQualifies';
import PromotionalBanner from '../../components/promotionalBanner';
import { coreFeatures } from './constants';
import RoleAccessPopup from '../../components/roleAccessPopup';
import { authorization, history } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import * as gtmActionCreators from '../../actionCreators/GTM';
import { pageTitle } from '../../constants/constants';

export class DashboardContainer extends PureComponent {
  constructor(props) {
    super(props);
    const currentYear = moment().year();
    this.state = {
      selectedYear: currentYear,
    };
  }

  componentDidMount() {
    const { selectedYear } = this.state;
    const { contractorId } = this.props;
    if (authorization.userRolesList.length && (authorization.userRolesList.includes('FULL_ACCESS'))) {
      this.props.getPromotions(selectedYear, contractorId, 2);
      this.props.actions.getRewardsDetails();
    }
    if (authorization.userRolesList.length && (authorization.userRolesList.includes('VIEW_POINTS'))) {
      this.props.actions.getRewardsDetails();
    }
    if (authorization.userRolesList.length && (authorization.userRolesList.includes('VIEW_PLAN'))) {
      this.props.getPromotions(selectedYear, contractorId, 2);
    }
  }

  /** @description callback function for NavItem click
   * @param {object} e - triggered event(click)
   * @param {object} link - selected menu item
   */
  onQuickAccessClick = (e, link) => {
    e.preventDefault();
    if (authorization.userRolesAccess && authorization.checkAccess(link)) {
      history.push(link);
      /* Trigger the GTM action */
      if (link === RouteConstants.WHAT_QUALIFIES) {
        this.props.gtmActions.gtmWhatQualifies();
      }
      if (link === RouteConstants.SUBMIT_INVOICE) {
        this.props.gtmActions.gtmSubmitInvoices();
      }
      if (link === RouteConstants.REDEEM_POINTS) {
        this.props.gtmActions.gtmRedeemPoints();
      }
    } else {
      this.props.actions.accessPage(true);
    }
  }

  /** @description callback function for view more button
   * @param {object} e - triggered event(click)
   */
  viewDetailsHandler = (e) => {
    e.preventDefault();
    history.push(RouteConstants.POINTS_HISTORY);
    /* Trigger the GTM action */
    this.props.gtmActions.gtmViewRewardPointsDetails();
  }

  render() {
    const {
      userInfo, rewardsInfo, spotPromotionsCategory, accessInfo,
    } = this.props;
    return (
      <main className="main-content">
        <Helmet>
          <title>{pageTitle.HOME}</title>
        </Helmet>
        <section>
          <Grid>
            <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={this.props.actions.accessPage} />
            <Row className="show-grid">
              <Col xs={12} sm={12} md={4}>
                <MyProfile
                  info={{ userInfo, rewardsInfo }}
                  onViewDetailsClick={this.viewDetailsHandler}
                  isRewardPointAccess={authorization.userRolesList.length > 0 && (authorization.userRolesList.includes('FULL_ACCESS') || authorization.userRolesList.includes('VIEW_POINTS'))}
                />
              </Col>
              <Col xs={12} sm={12} md={8}>
                <QuickAccess
                  coreFeatures={coreFeatures.contractor}
                  onQuickAccess={this.onQuickAccessClick}
                />
              </Col>
            </Row>
          </Grid>
        </section>
        <section className="promotions-list">
          <Grid>
            <Row>
              <Col xs={12} md={12}>
                {spotPromotionsCategory &&
                  spotPromotionsCategory.map((info, i) => (
                    <PromotionalBanner
                      key={i.toString()}
                      index={i}
                      promotionsInfo={info}
                      gtmActions={this.props.gtmActions}
                      updateSelectedYear={this.props.updateSpotPromotionSelectedYear}
                    />
                  ))}
              </Col>
            </Row>
          </Grid>
        </section>
      </main>
    );
  }
}

function mapStateToProps(state) {
  const { myProfileState, whatQualifiesState } = state;
  const { userInfo, rewardsInfo, accessInfo } = myProfileState;
  const { spotPromotionsCategory } = whatQualifiesState;
  return {
    userInfo,
    rewardsInfo,
    accessInfo,
    spotPromotionsCategory,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getPromotions: bindActionCreators(getCategoryPromotions, dispatch),
    actions: bindActionCreators(getProfileDetails, dispatch),
    gtmActions: bindActionCreators(gtmActionCreators, dispatch),
    updateSpotPromotionSelectedYear: bindActionCreators(updateSpotPromotionSelectedYear, dispatch),
  };
}

/** PropTypes:
 * userInfo - object - Contains the information about the user
 * rewardsInfo - object - Contains the information about the user's rewards
 *  accessInfo - object - Contains the information about the user access
 * * getPromotions - func - triggers the get promotions api call
 */
DashboardContainer.propTypes = {
  userInfo: PropTypes.object,
  rewardsInfo: PropTypes.object,
  accessInfo: PropTypes.bool,
  spotPromotionsCategory: PropTypes.array,
  actions: PropTypes.object,
  getPromotions: PropTypes.func.isRequired,
  contractorId: PropTypes.string,
  gtmActions: PropTypes.object,
  updateSpotPromotionSelectedYear: PropTypes.func,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DashboardContainer));
