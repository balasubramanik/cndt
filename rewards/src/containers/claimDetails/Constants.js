const constants = {
  pageTitle: 'Claim Details',
  PageContent: 'Questions?',
  pointEarned: 'Points Earned',
  contactusText: 'Contact us.',
  linkText: 'Claim Status',
};
export { constants };
