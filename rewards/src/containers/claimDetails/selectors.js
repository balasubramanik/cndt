import { createSelector } from 'reselect';
import { ascending, descending, sortByDate } from '../../utils/utils';

export const getInvoiceList = (state) => state.claimDetails.listOfInvoice;
export const getPageNo = (state) => state.claimDetails.pageNumber;
export const showPerPage = (state) => state.claimDetails.showPerPage;
export const isAscending = (state) => state.claimDetails.isAscending;
export const sortField = (state) => state.claimDetails.sortField;
export const getSearchText = (state) => state.claimDetails.searchText.trim();

export const getDefaultSortData = createSelector(
  [getInvoiceList],
  (invoiceList) => {
    const list = [...invoiceList];
    if (list !== []) {
      sortByDate(list, 'descending', 'invoiceDate');
    }
    return list;
  }
);

export const getFilteredList = createSelector(
  [getSearchText, getDefaultSortData],
  (searchText, invoiceList) => {
    if (searchText) {
      const list = [...invoiceList];
      const holder = list.filter((items) => {
        const listItem = (items.productName) ? items.productName.trim().toLowerCase() : items.productName;
        if (listItem) {
          return listItem.indexOf(searchText.toLowerCase()) > -1;
        }
        return false;
      });
      return holder;
    }
    return invoiceList;
  },
);

export const getSortedData = createSelector(
  [getFilteredList, isAscending, sortField],
  (invoiceList, isAsc, field) => {
    if (invoiceList) {
      const data = [...invoiceList];
      if (field) {
        if (isAsc) {
          data.sort(ascending(field));
        } else {
          data.sort(descending(field));
        }
      }
      return data;
    }
    return {};
  },
);

export const getTotalItems = createSelector(
  [getSortedData],
  (invoiceList) => {
    if (invoiceList) {
      return invoiceList.length;
    }
    return 0;
  },
);

export const getInvoiceListByPageNo = createSelector(
  [getSortedData, getPageNo, showPerPage],
  (invoiceList, pageNumber, itemsPerPage) => {
    if (invoiceList) {
      let list = [...invoiceList];
      list = list.slice((pageNumber - 1) * itemsPerPage, pageNumber * itemsPerPage);
      return list;
    }
    return [];
  },
);
