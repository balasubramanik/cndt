import test from 'tape';
import { getInvoiceList, getPageNo, showPerPage, isAscending, sortField, getSearchText, getDefaultSortData, getFilteredList, getSortedData, getInvoiceListByPageNo } from './selectors';

const t = test('test utils', (b) => b);
describe('Claim Details Selectors', () => {
  it('should return InvoiceList', () => {
    const mock = {
      claimDetails: {
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        }],
      },
    };
    t.deepEqual(mock.claimDetails.listOfInvoice, getInvoiceList(mock));
  });

  it('should return page number', () => {
    const mock = {
      claimDetails: {
        pageNumber: 1,
      },
    };
    t.deepEqual(mock.claimDetails.pageNumber, getPageNo(mock));
  });

  it('should return show per page', () => {
    const mock = {
      claimDetails: {
        showPerPage: 25,
      },
    };
    t.deepEqual(mock.claimDetails.showPerPage, showPerPage(mock));
  });
  it('should return isAscending', () => {
    const mock = {
      claimDetails: {
        isAscending: true,
      },
    };
    t.deepEqual(mock.claimDetails.isAscending, isAscending(mock));
  });
  it('should return sortField', () => {
    const mock = {
      claimDetails: {
        sortField: 'productName',
      },
    };
    t.deepEqual(mock.claimDetails.sortField, sortField(mock));
  });
  it('should return getSearchText', () => {
    const mock = {
      claimDetails: {
        searchText: 'Asad hjkjjh',
      },
    };
    t.deepEqual(mock.claimDetails.searchText, getSearchText(mock));
  });
  it('should return default sort order', () => {
    const mock = {
      claimDetails: {
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2017-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Cobra® Snow Country',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: '-32',
        submissionValue: '-2816',
        pointsEarned: '-28.16',
        rate: '3',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2017-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
    ], getDefaultSortData(mock));
  });
  it('should return empty array when invoiceList empty', () => {
    const mock = {
      claimDetails: {
        listOfInvoice: [],
      },
    };
    t.deepEqual([], getDefaultSortData(mock));
  });
  it('should return Filtered List', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
    ], getFilteredList(mock));
  });
  it('should return Filtered List for no search text', () => {
    const mock = {
      claimDetails: {
        searchText: '',
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Cobra® Snow Country',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-32',
      submissionValue: '-2816',
      pointsEarned: '-28.16',
      rate: '3',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    ], getFilteredList(mock));
  });
  it('should return Ascending order Sorted Data according to the key', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        isAscending: true,
        sortField: 'productName',
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
    ], getSortedData(mock));
  });
  it('should return Descending order Sorted Data according to the key', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        isAscending: false,
        sortField: 'productName',
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
    ], getSortedData(mock));
  });
  it('should return Data according to the key when no sortfield is provided', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        isAscending: false,
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    }], getSortedData(mock));
  });
  it('should return empty object when no invoiceList is provided', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        isAscending: false,
        listOfInvoice: [],
      },
    };
    t.deepEqual([], getSortedData(mock));
  });
  it('should return getInvoiceListByPageNo', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        isAscending: true,
        showPerPage: 25,
        pageNumber: 1,
        sortField: 'productName',
        listOfInvoice: [{
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        ],
      },
    };
    t.deepEqual([
      {
        invoiceNumber: '10005698-001',
        invoiceDate: '2018-12-09T09:00:56.6173327Z',
        productName: 'Liberty Self-Adhering Roofing',
        promotionName: '2018 GAF Rewards',
        uom: 'Bundle',
        productQuantity: ' -98',
        submissionValue: '-3724',
        pointsEarned: '-37.24',
        rate: '1',
        detailStatus: 'Approved',
        reasonForDescAudit: '',
      },
    ], getInvoiceListByPageNo(mock));
  });
  it('should return getInvoiceListByPageNo when order list not defined', () => {
    const mock = {
      claimDetails: {
        searchText: 'lib',
        isAscending: true,
        listOfInvoice: [],
        showPerPage: 25,
        pageNumber: 1,
        sortField: 'productName',
      },
    };
    t.deepEqual([], getInvoiceListByPageNo(mock));
  });
});
