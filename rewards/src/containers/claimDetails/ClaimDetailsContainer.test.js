import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import ClaimDetailsCont, { ClaimDetails } from '../../containers/claimDetails';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';

configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('ClaimDetails', () => {
  window.open = jest.fn();
  const getSuggestion = jest.fn();
  const gtmClaimDetilsToClaimStatus = jest.fn();
  const gtmClaimDetailsToContactus = jest.fn();
  const gtmClaimSearch = jest.fn();
  const gtmClaimDetailsCSVExport = jest.fn();
  const getInvoiceListClear = jest.fn();
  const getInvoiceList = jest.fn();
  const updatePageDetails = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<ClaimDetails
    productSerachActions={{ getSuggestion }}
    totalItems={2}
    showPerPage={1}
    pageNumber={1}
    isLoading
    inVoiceList={[{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Cobra® Snow Country',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-32',
      submissionValue: '-2816',
      pointsEarned: '-28.16',
      rate: '3',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    ]}
    inVoiceItems={[{
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Liberty Self-Adhering Roofing',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: ' -98',
      submissionValue: '-3724',
      pointsEarned: '-37.24',
      rate: '1',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    {
      invoiceNumber: '10005698-001',
      invoiceDate: '2018-12-09T09:00:56.6173327Z',
      productName: 'Cobra® Snow Country',
      promotionName: '2018 GAF Rewards',
      uom: 'Bundle',
      productQuantity: '-32',
      submissionValue: '-2816',
      pointsEarned: '-28.16',
      rate: '3',
      detailStatus: 'Approved',
      reasonForDescAudit: '',
    },
    ]}
    claimDetailsActions={{ getInvoiceListClear, getInvoiceList, updatePageDetails }}
    match={{ params: { claimId: '123' } }}
    pointsEarned={1300}
    submittedOn="2018-09-28T21:12:44.0122176+05:30"
    gtmActions={{
      gtmClaimDetilsToClaimStatus,
      gtmClaimDetailsToContactus,
      gtmClaimSearch,
      gtmClaimDetailsCSVExport,
    }}

  />);
  it('should be defined', () => {
    expect(ClaimDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should redirect to Claim Status', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(tree.state().isResetSearch).toBe(true);
    expect(history.push).toBeCalled();
    expect(gtmClaimDetilsToClaimStatus).toBeCalled();
  });

  it('should redirect to Contact Us', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(tree.state().isResetSearch).toBe(true);
    expect(history.push).toBeCalled();
    expect(gtmClaimDetailsToContactus).toBeCalled();
  });

  it('should call paginationHandler', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('Pagination').prop('pageClick');
    simulateClick(event);
    expect(updatePageDetails).toBeCalled();
    expect(tree.state().isResetSearch).toBe(true);
  });

  it('should call showPerPageHandler', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('ShowBy').prop('onChange');
    simulateClick(event);
    expect(updatePageDetails).toBeCalled();
    expect(tree.state().isResetSearch).toBe(true);
  });

  it('should call sortHandler', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('InvoiceList').prop('onSort');
    simulateClick(event);
    expect(updatePageDetails).toBeCalled();
    expect(tree.state().isResetSearch).toBe(true);
  });
  it('should call searchByText', () => {
    const simulateClick = tree.find('SearchBy').prop('handleChange');
    simulateClick('lib', true);
    expect(getSuggestion).toBeCalled();
  });

  it('should call searchByText when bool is false and no value', () => {
    const simulateClick = tree.find('SearchBy').prop('handleChange');
    simulateClick(false);
    expect(updatePageDetails).toBeCalled();
  });
  it('should call searchByProduct', () => {
    const simulateClick = tree.find('SearchBy').prop('handleSubmit');
    simulateClick('Cabela’s', true);
    expect(updatePageDetails).toBeCalled();
    expect(gtmClaimSearch).toBeCalled();
  });
  it('should call searchByProduct when product is not provided and bool is false', () => {
    tree.setState({ isResetSearch: true });
    const simulateClick = tree.find('SearchBy').prop('handleSubmit');
    simulateClick('', false);
    expect(updatePageDetails).toBeCalled();
    expect(gtmClaimSearch).toBeCalled();
    expect(tree.state().isResetSearch).toBe(false);
  });
});

describe('ClaimDetails with connected component', () => {
  window.open = jest.fn();
  const state = {
    searchByState: {
      claimDetailProduct: {},
    },
    claimDetails: {
      searchText: 'lib',
      isAscending: true,
      sortField: 'productName',
      listOfInvoice: [
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Liberty Self-Adhering Roofing',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: ' -98',
          submissionValue: '-3724',
          pointsEarned: '-37.24',
          rate: '1',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
        {
          invoiceNumber: '10005698-001',
          invoiceDate: '2018-12-09T09:00:56.6173327Z',
          productName: 'Cobra® Snow Country',
          promotionName: '2018 GAF Rewards',
          uom: 'Bundle',
          productQuantity: '-32',
          submissionValue: '-2816',
          pointsEarned: '-28.16',
          rate: '3',
          detailStatus: 'Approved',
          reasonForDescAudit: '',
        },
      ],
      totalItems: 2,
      showPerPage: 25,
      pageNumber: 1,
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsRedeemed: '1300',
      isLoading: true,
    },
  };
  const getSuggestion = jest.fn();
  const gtmClaimDetilsToClaimStatus = jest.fn();
  const gtmClaimDetailsToContactus = jest.fn();
  const gtmClaimSearch = jest.fn();
  const gtmClaimDetailsCSVExport = jest.fn();
  const getInvoiceListClear = jest.fn();
  const getInvoiceList = jest.fn();
  const updatePageDetails = jest.fn();
  const store = fakeStore(state);
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <ClaimDetailsCont
        productSerachActions={{ getSuggestion }}
        claimDetailsActions={{ getInvoiceListClear, getInvoiceList, updatePageDetails }}
        match={{ params: { claimId: '123' } }}
        pointsEarned="1300"
        submittedOn="2018-09-28T21:12:44.0122176+05:30"
        gtmActions={{
          gtmClaimDetilsToClaimStatus,
          gtmClaimDetailsToContactus,
          gtmClaimSearch,
          gtmClaimDetailsCSVExport,
        }}

      />
    </Router>
                     </Provider>);
  it('should be defined', () => {
    expect(ClaimDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
