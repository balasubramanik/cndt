import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import { history, authorization } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import InvoiceList from '../../components/invoiceList';
import * as claimDetailsActionCreators from '../../actionCreators/ClaimDetails';
import { constants } from './Constants';
import ShowBy from '../../components/showBy';
import Pagination from '../../components/pagination';
import SearchBy from '../../components/searchBy';
import * as productSearchBy from '../../actionCreators/SearchBy';
import { searchByType, pageTitle } from '../../constants/constants';
import { getInvoiceListByPageNo, getDefaultSortData, getTotalItems } from './selectors';
import Loader from '../../components/loader';
import { formatPoint } from '../../utils/utils';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import RoleAccessPopup from '../../components/roleAccessPopup';
import * as getProfileDetails from '../../actionCreators/MyProfile';

export class ClaimDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchByPlaceholder: 'Search by Product',
      isSearchByIcon: true,
      searchByChar: {
        min: 3,
      },
      isResetSearch: true,
    };
    this.defaultSort = 'invoiceDate';
  }

  componentDidMount() {
    const { claimDetailsActions, match } = this.props;
    const claimID = match.params.claimId;
    claimDetailsActions.updatePageDetails({
      isAscending: true,
      sortField: '',
      searchText: '',
      pageNumber: 1,
      showPerPage: 25,
    });
    claimDetailsActions.getInvoiceListClear();
    claimDetailsActions.getInvoiceList(claimID, this.state.ShowBy);
  }

  searchByText = (value, bool) => {
    const { claimDetailsActions } = this.props;
    if (bool) {
      this.props.productSerachActions.getSuggestion(value, searchByType.ClaimDetailProduct);
    }
    if (!value) {
      claimDetailsActions.updatePageDetails({
        isAscending: true,
        sortField: '',
        searchText: '',
        pageNumber: 1,
      });
    }
  }

  handleSearch = (searchText) => {
    const { inVoiceItems } = this.props;
    if (!inVoiceItems) {
      return [];
    }
    if (!searchText) {
      return inVoiceItems;
    }
    return inVoiceItems.map((list) => (searchText.indexOf(list.productName) === 0));
  }

  searchProduct = (product) => {
    const { claimDetailsActions, gtmActions } = this.props;
    claimDetailsActions.updatePageDetails({
      isAscending: true,
      sortField: '',
      searchText: product,
      pageNumber: 1,
    });
    gtmActions.gtmClaimSearch(product);
    if (!product) {
      this.setState({
        isResetSearch: false,
      });
    }
  }

  handleCSVDownload = () => {
    const { gtmActions } = this.props;
    gtmActions.gtmClaimDetailsCSVExport();
    this.restSearchError();
  }

  showPerPageHandler = (showPerPage) => {
    const { claimDetailsActions } = this.props;
    claimDetailsActions.updatePageDetails({ showPerPage });
    this.restSearchError();
  }

  sortHandler = (sortField, isAscending) => {
    const { claimDetailsActions } = this.props;
    claimDetailsActions.updatePageDetails({ isAscending, sortField, pageNumber: 1 });
    this.restSearchError();
  }

  paginationHandler = (pageNumber) => {
    const { claimDetailsActions } = this.props;
    claimDetailsActions.updatePageDetails({ pageNumber });
    this.restSearchError();
  }

  redirectToClaimStatus = (e) => {
    const { gtmActions, profileDetails } = this.props;
    e.preventDefault();
    if (authorization.userRolesAccess && authorization.checkAccess(RouteConstants.CLAIM_STATUS)) {
      history.push({
        pathname: RouteConstants.CLAIM_STATUS,
      });
    } else {
      profileDetails.accessPage(true);
    }
    this.restSearchError();
    gtmActions.gtmClaimDetilsToClaimStatus();
  };

  contactUsPage = (e) => {
    e.preventDefault();
    this.restSearchError();
    const { match, gtmActions, claimId } = this.props;
    history.push(RouteConstants.CONTACTUS.replace(':claimId', match.params.claimId));
    gtmActions.gtmClaimDetailsToContactus(claimId);
  };

  restSearchError = () => {
    this.setState({
      isResetSearch: true,
    });
  }

  render() {
    const {
      inVoiceList,
      totalItems,
      showPerPage,
      pageNumber,
      pointsEarned,
      isLoading,
      submittedOn,
      claimId,
      accessInfo,
      profileDetails,
    } = this.props;
    return (
      <div>
        <Helmet>
          <title>{pageTitle.CLAIMDETAILS}</title>
        </Helmet>
        <main className="main-content claim-details" id="claim-details">
          <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={profileDetails.accessPage} />
          <Grid>
            <div className="gaf-title-block" id="claim-details-title">
              <Row>
                <Col md={8}>
                  <h2>{constants.pageTitle} - {claimId}</h2>
                  <p>{constants.PageContent} {constants.to} <span><a href="/" onClick={this.contactUsPage}>{constants.contactusText}</a></span></p>
                </Col>
                <Col md={4} className="text-right">
                  <div className="links">
                    <a href="/" onClick={this.redirectToClaimStatus} id="claim-status-link">{constants.linkText}</a>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="gaf-page-info">
              <div className="points-earned" id="cd-points-earned">
                <Row className="show-grid">
                  <Col md={7} sm={6} xs={12}>
                    {authorization.userRolesList.length && (authorization.userRolesList.includes('FULL_ACCESS') || authorization.userRolesList.includes('VIEW_POINTS')) ? <p><span></span>{constants.pointEarned}<span>{formatPoint(pointsEarned)}</span></p> : null}
                    <div className="clearfix"></div>
                  </Col>
                  <Col md={5} sm={6} xs={12}>
                    <SearchBy id={searchByType.ClaimDetailProduct} placeholder={this.state.searchByPlaceholder} typeahead="false" isSearchByIcon={this.state.isSearchByIcon} charLength={this.state.searchByChar} handleChange={this.searchByText} validationErrTxt="" dynamicClass="pull-right" handleSubmit={this.searchProduct} isResetSearch={this.state.isResetSearch} />
                  </Col>
                </Row>
              </div>
              <section className="invoice-list" id="cd-invoice-list">
                {inVoiceList && <InvoiceList data={inVoiceList} onSort={this.sortHandler} CSVHandler={this.handleCSVDownload} exportCSVData={inVoiceList} loading={isLoading} defaultSort={this.defaultSort} submittedOnDate={submittedOn} claimID={claimId} />}
                <div className="gaf-pagination">
                  {totalItems > showPerPage &&
                    <Row>
                      <Col sm={6} md={6} xsHidden>
                        <Form inline id="cd-showby">
                          <ShowBy onChange={this.showPerPageHandler} />
                        </Form>
                      </Col>
                      <Col sm={6} md={6} xs={12}>
                        <Pagination selected={pageNumber} totalRows={totalItems} perPage={showPerPage} pageClick={this.paginationHandler} />
                      </Col>
                    </Row>
                  }
                </div>
              </section>
            </div>
          </Grid>
        </main>
        {isLoading && <Loader />}
      </div>
    );
  }
}

function mapStateToProps(state, match) {
  const { searchByState, claimDetails, myProfileState } = state;
  const { accessInfo } = myProfileState;
  const {
    listOfInvoice,
    showPerPage,
    pageNumber,
    pointsEarned,
    isLoading,
    claimId,
    submittedOn,
  } = claimDetails;
  const { claimDetailProduct = {} } = searchByState;
  const {
    suggestions,
    searchError,
    searchText,
    isSuggestionsLoading,
  } = claimDetailProduct;
  return {
    suggestions,
    searchError,
    searchText,
    isSuggestionsLoading,
    inVoiceItems: getDefaultSortData(state),
    inVoiceList: getInvoiceListByPageNo(state),
    listOfInvoice,
    totalItems: getTotalItems(state),
    showPerPage,
    pageNumber,
    pointsEarned,
    match,
    isLoading,
    claimId,
    submittedOn,
    accessInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    claimDetailsActions: bindActionCreators(claimDetailsActionCreators, dispatch),
    productSerachActions: bindActionCreators(productSearchBy, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
    profileDetails: bindActionCreators(getProfileDetails, dispatch),
  };
}

ClaimDetails.propTypes = {
  productSerachActions: PropTypes.object,
  claimDetailsActions: PropTypes.object,
  inVoiceItems: PropTypes.array,
  inVoiceList: PropTypes.array,
  totalItems: PropTypes.number,
  showPerPage: PropTypes.number,
  pageNumber: PropTypes.number,
  pointsEarned: PropTypes.number,
  isLoading: PropTypes.bool,
  match: PropTypes.object,
  submittedOn: PropTypes.string,
  claimId: PropTypes.string,
  gtmActions: PropTypes.object.isRequired,
  accessInfo: PropTypes.bool,
  profileDetails: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ClaimDetails));
