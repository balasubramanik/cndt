import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import envConfig from 'envConfig'; // eslint-disable-line
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { constants } from './Constants';
import { resendEmail } from '../../actionCreators/EmailVerification';
import { getQueryString } from '../../utils/utils';


export class ConfirmationMessageContainer extends PureComponent {
  constructor(props) {
    super(props);
    const { location } = this.props;
    const { search } = location;
    const id = getQueryString('id', search);
    this.isRegistration = Boolean(id);
  }

  /** @description Callback function for trigger the resend email api
   * @param {object} e - triggered event(click)
   */
  resendEmailHandler = (e) => {
    const { guid } = this.props;
    e.preventDefault();
    this.props.onSubmit({ id: guid }, this.props.resendEmail);
  }

  /** @description Function to render not eligible message */
  renderNotEligibleMessage = () => (
    <section className="verify-email">
      <div className="email-icon">
        <p>
          <span className="icon-check"></span>
        </p>
      </div>
      <div className="email-desc">
        <h4>{constants.NOT_ELIGIBLE_MESSAGE_1}</h4>
        <p>{constants.NOT_ELIGIBLE_MESSAGE_2}</p>
      </div>
    </section>
  );

  /** @description Function to render already register message */
  renderAlreadyVerifiedMessage = () => (
    <section className="verify-email">
      <div className="email-icon">
        <p>
          <span className="icon-check"></span>
        </p>
      </div>
      <div className="email-desc">
        <h4>{constants.ALREADY_VERIFIED_1}</h4>
      </div>
    </section>
  );

  /** @description Function to render email exist message */
  renderEmailExistMessage = () => (
    <section className="verify-email">
      <div className="email-icon">
        <p>
          <span className="icon-email"></span>
        </p>
      </div>
      <div className="email-desc">
        <h3>{constants.EMAIL_EXISTS_1}</h3>
        <p>{constants.PLEASE} <a href={`${envConfig.rewardsBaseUrl}`}><b>{constants.LOGIN}</b></a></p>
      </div>
    </section>
  );

  /** @description Function to render success message */
  renderSuccessMessage = () => {
    const { values } = this.props;
    const { email } = values;
    return (
      <section className="verify-email">
        <div className="email-icon">
          <p>
            <span className="icon-email"></span>
          </p>
        </div>
        <div className="email-desc">
          <h4>{constants.EMAIL_SENT_MESSAGE_1} <strong className="email">{email}</strong></h4>
          <p>{constants.EMAIL_SENT_MESSAGE_2}</p>
          <p><a className="resend-email" href="/" onClick={this.resendEmailHandler}>{constants.CLICKHERE}</a> {constants.RESEND}</p>
        </div>
      </section>
    );
  }

  /** @description Function to render pending registration message */
  renderPendingRegistrationMessage = () => (
    <section className="verify-email">
      <div className="email-icon">
        <p>
          <span className="icon-email"></span>
        </p>
      </div>
      <div className="email-desc">
        <h4>{constants.REGISTRATION_PENDING_1}</h4>
        <p>{constants.REGISTRATION_PENDING_2}</p>
        <p>{constants.QUERY}</p>
      </div>
    </section>
  );

  /** @description Function to render cancel registration message */
  renderCancelMessage = () => (
    <section className="verify-email">
      <div className="email-icon">
        <p>
          <span className="icon-email"></span>
        </p>
      </div>
      <div className="email-desc">
        <h4>{constants.REGISTRATION_CANCELED_1}</h4>
        <p>{constants.REGISTRATION_CANCELED_2}</p>
        <p>{constants.QUERY}</p>
      </div>
    </section>
  );

  /** @description function to render the registration confirmation page */
  renderMessage = () => {
    const {
      values, isEmailExist, status, emailFormStatus,
    } = this.props;
    const regStatus = status ? status.toLowerCase() : '';
    const emailStatus = emailFormStatus ? emailFormStatus.toLowerCase() : '';
    if (!this.isRegistration && values.isRoofingContractor === constants.NO) {
      return this.renderNotEligibleMessage();
    }

    if (!this.isRegistration && isEmailExist) {
      if (emailStatus === constants.PENDING) {
        return this.renderPendingRegistrationMessage();
      }
      if (emailStatus === constants.CANCELED) {
        return this.renderCancelMessage();
      }
      return this.renderEmailExistMessage();
    }

    if (this.isRegistration && regStatus !== constants.EMAILVERIFIED) {
      return this.renderAlreadyVerifiedMessage();
    }
    return this.renderSuccessMessage();
  }

  render() {
    return (
      <div className="confirm-wrapper">
        {this.renderMessage()}
      </div>
    );
  }
}


function mapStateToProps(state) {
  const { form } = state;
  const { emailVerificationForm, registrationForm } = form;
  const { status } = registrationForm;
  const { status: emailFormStatus } = emailVerificationForm;
  const {
    values, isEmailExist, guid,
  } = emailVerificationForm;
  return {
    values,
    status,
    emailFormStatus,
    isEmailExist,
    guid,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    resendEmail: bindActionCreators(resendEmail, dispatch),
  };
}

/** PropTypes:
 * location - object - contains the current location details
 * resendEmail - function - function to trigger resend email api
 * onSubmit - func - callback function for submit form
 * emailFormStatus - string - status of email verification
 * values - object - Contains the values of valid form fields
 * isEmailExist - boolean - represents whether email is present in database
 * status - string - status of registration
 * guid - string - user id
 */
ConfirmationMessageContainer.propTypes = {
  location: PropTypes.object.isRequired,
  resendEmail: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  emailFormStatus: PropTypes.string,
  values: PropTypes.object,
  isEmailExist: PropTypes.bool,
  status: PropTypes.string,
  guid: PropTypes.string,
};

const Component = withRouter(ConfirmationMessageContainer);

export default connect(mapStateToProps, mapDispatchToProps)(Component);
