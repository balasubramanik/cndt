import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import { history } from '../../routes';
import ConfirmationMessageCont, { ConfirmationMessageContainer } from '../../containers/confirmationMessage';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });


describe('ConfirmationMessageContainer with isRoofingContractor yes ', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const resendEmail = jest.fn();
  const onSubmit = jest.fn();
  const tree = shallow(
    <ConfirmationMessageContainer
      location={{
        search: '?id=1234',
      }}
      status="emailVerified"
      resendEmail={resendEmail}
      guid="1234567890"
      onSubmit={onSubmit}
      values={{
        isRoofingContractor: 'yes',
        termsAndConditions: true,
      }}
    />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should be defined', () => {
    expect(ConfirmationMessageContainer).toBeDefined();
  });

  it('should render Description', () => {
    expect(tree.find('.email-desc').length).toBe(1);
  });

  it('should call resend email handler', () => {
    const simulateClick = tree.find('.resend-email').prop('onClick');
    simulateClick(event);
    expect(onSubmit).toBeCalled();
  });
});

describe('ConfirmationMessageContainer with isEmailExist true with cancelled ', () => {
  const tree = shallow(
    <ConfirmationMessageContainer
      location={{
        search: '',
      }}
      isEmailExist={true}
      emailFormStatus="canceled"
      values={{
        isRoofingContractor: 'yes',
        termsAndConditions: true,
      }}
    />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should be defined', () => {
    expect(ConfirmationMessageContainer).toBeDefined();
  });

  it('should render Description', () => {
    expect(tree.find('.email-desc').length).toBe(1);
  });
});

describe('ConfirmationMessageContainer with isEmailExist true with pending ', () => {
  const tree = shallow(
    <ConfirmationMessageContainer
      location={{
        search: '',
      }}
      isEmailExist={true}
      emailFormStatus="pending"
      values={{
        isRoofingContractor: 'yes',
        termsAndConditions: true,
      }}
    />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should be defined', () => {
    expect(ConfirmationMessageContainer).toBeDefined();
  });

  it('should render Description', () => {
    expect(tree.find('.email-desc').length).toBe(1);
  });
});

describe('ConfirmationMessageContainer with isEmailExist true ', () => {
  const tree = shallow(
    <ConfirmationMessageContainer
      location={{
        search: '',
      }}
      isEmailExist={true}
      emailFormStatus="approved"
      values={{
        isRoofingContractor: 'yes',
        termsAndConditions: true,
      }}
    />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should be defined', () => {
    expect(ConfirmationMessageContainer).toBeDefined();
  });

  it('should render Description', () => {
    expect(tree.find('.email-desc').length).toBe(1);
  });
});

describe('ConfirmationMessageContainer with non roofing contractor ', () => {
  const tree = shallow(
    <ConfirmationMessageContainer
      location={{
        search: '',
      }}
      isEmailExist={true}
      emailFormStatus=""
      values={{
        isRoofingContractor: 'no',
      }}
    />);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should be defined', () => {
    expect(ConfirmationMessageContainer).toBeDefined();
  });

  it('should render Description', () => {
    expect(tree.find('.email-desc').length).toBe(1);
  });
});

describe('ConfirmationMessageContainer with connected component', () => {
  window.open = jest.fn();
  const state = {
    form: {
      emailVerificationForm: {
        values: {},
        isEmailExist: true,
      },
      registrationForm: {
        status: 'pending',
      },
    },
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <ConfirmationMessageCont
          location={{
            search: '?id=1234',
          }}
        />
      </Router>
    </Provider>);

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should be defined', () => {
    expect(ConfirmationMessageCont).toBeDefined();
  });

  it('should render Description', () => {
    expect(tree.find('.email-desc').length).toBe(1);
  });
});
