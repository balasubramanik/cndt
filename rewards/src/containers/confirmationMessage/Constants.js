import { inquiryDetails } from '../../constants/constants';

const constants = {
  NOT_ELIGIBLE_MESSAGE_1: 'Thank you for your interest in GAF Rewards.',
  NOT_ELIGIBLE_MESSAGE_2: 'However, this program is only available to those who install roofs.',
  EMAIL_EXISTS_1: 'Your email address is already registered with us.',
  EMAIL_SENT_MESSAGE_1: 'A link has been sent to',
  EMAIL_SENT_MESSAGE_2: 'Please click on the link sent to you to verify your email. If you have not received the email,',
  ALREADY_VERIFIED_1: 'Your email address has already been verified.',
  REGISTRATION_PENDING_1: 'You have already enrolled in the new GAF Rewards program.',
  REGISTRATION_PENDING_2: 'Your enrollment is pending. Please give us a little more time to complete the verification process.',
  REGISTRATION_CANCELED_1: 'You have already enrolled in the new GAF Rewards program.',
  REGISTRATION_CANCELED_2: 'Your enrollment has been canceled.',
  QUERY: `If you have any questions, please contact us at ${inquiryDetails.Phone} ${inquiryDetails.Email} between 8.00 a.m. and 5.00 p.m. ET, Monday through Friday.`,
  PLEASE: 'Please',
  LOGIN: 'Log In',
  CLICKHERE: 'click here',
  RESEND: 'to resend.',
  EMAILVERIFIED: 'emailverified',
  PENDING: 'pending',
  APPROVED: 'approved',
  CANCELED: 'canceled',
  NO: 'no',
};


export { constants };
