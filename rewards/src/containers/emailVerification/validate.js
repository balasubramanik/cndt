import { validate as nameValidation } from '../../components/name/validate';
import { validate as emailValidation } from '../../components/email/validate';
import { validate as roofingCheckValidation } from '../../components/roofingCompanyCheck/validate';

export default function (values, props) {
  return {
    ...nameValidation(values),
    ...emailValidation(values, props),
    ...roofingCheckValidation(values),
  };
}
