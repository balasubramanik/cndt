import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Col } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { reduxForm } from 'redux-form';
import { changeFormState, submitEmailVerification } from '../../actionCreators/EmailVerification';
import { getRegistrationInfo } from '../../actionCreators/Registration';
import Name from '../../components/name';
import Email from '../../components/email';
import { warnings } from '../../components/email/validate';
import RoofingCompanyCheck from '../../components/roofingCompanyCheck';
import { emailVerification } from './Constants';
import { values as options } from '../../components/roofingCompanyCheck/constants';
import { constants } from '../ems/Constants';
import validate from './validate';
import * as EMAILVERIFICATION from '../../actionTypes/EmailVerification';
import * as gtmActionsCreators from '../../actionCreators/GTM';

export class EmailVerificationContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.values = null;
  }

  /** @description Callback function for change roof option
   * @param {string} type
   * @param {string} value
   */
  roofOptionChangeHandler = (type, value) => {
    const { changeState, gtmActions } = this.props;
    if (type === EMAILVERIFICATION.CHANGE_ROOF_OPTIONS) {
      gtmActions.gtmRoofInstaller(value);
    }
    changeState(type, value);
  }

  /** @description Callback function for submit form
   * @param {object} formData
   */
  submitHandler = (formData) => {
    const {
      firstName, lastName, email,
      isRoofingContractor, description, installedOption,
      authorizeeDesignation,
      contractorClassification,
    } = formData;
    const reqBody = {
      contact: {
        emailAddress: email,
        firstName,
        lastName,
      },
      userId: email,
      isRoofingContractor: isRoofingContractor === options.YES,
      contractorClassification: contractorClassification !== options.OTHERS ? contractorClassification : description,
      authorizeeDesignation,
      isCompany: installedOption === options.COMPANY,
    };
    this.values = reqBody;
    this.props.onSubmit(this.values, this.props.submitForm);
  }

  /** @description callback function for successful recaptcha */
  verifyCallback = (response) => {
    this.recaptchaInstance.reset();
    if (response) {
      this.props.submitForm({
        captchaResponse: response,
        ...this.values,
      });
    }
  };

  /** @description function to render the submit button */
  renderSubmitButton = () => (
    <div className="next-btn">
      <Button
        type="submit"
        className="btn-block gaf-btn-primary"
      >
        {emailVerification.NEXT}
      </Button>
    </div>
  );

  /** @description function to render the Terms and condition links */
  renderTermsAndConditions = () => (
    <Col xs={12} md={12}>
      <div className="btn-and-terms clearfix">
        {this.renderSubmitButton()}
        <div className="terms-and-conditions">
          <p>{emailVerification.VIEWTERMS_AND_CONDITIONS}
            <br className="visible-xs" />
            <a
              id="ev-tnc"
              href={constants.TERMS_AND_CONDITIONS_URL}
              target="_blank"
              rel="noopener noreferrer"
            > {emailVerification.GAF_REWARDS}
            </a>
          </p>
        </div>
      </div>
    </Col>
  );

  render() {
    const {
      values, handleSubmit,
    } = this.props;
    return (
      <form
        name="emailVerificationForm"
        onSubmit={handleSubmit(this.submitHandler)}
      >
        <p className="ems-alert">{emailVerification.INDICATES_REQUIRED_FIELD}</p>
        <div className="emailVerification row">
          <Name />
          <Email />
          <RoofingCompanyCheck
            options={values}
            onChange={this.roofOptionChangeHandler}
          />
          {this.renderTermsAndConditions()}
        </div>
      </form>
    );
  }
}

function mapStateToProps(state) {
  const { form } = state;
  const { emailVerificationForm } = form;
  const {
    values,
  } = emailVerificationForm;
  return {
    values,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    changeState: bindActionCreators(changeFormState, dispatch),
    submitForm: bindActionCreators(submitEmailVerification, dispatch),
    getRegistrationInfo: bindActionCreators(getRegistrationInfo, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

/** PropTypes:
 * changeState - func - function for update form state
 * handleSubmit - func - function for submit the form
 * submitForm - func - function to trigger api call for form submit
 * onSubmit - func - callback function for submit form
 * gtmActions - object - actions for google tag manager
 * values - object - Contains the values of valid form fields
 */
EmailVerificationContainer.propTypes = {
  changeState: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitForm: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  gtmActions: PropTypes.object.isRequired,
  values: PropTypes.object,
};

const Component = withRouter(connect(mapStateToProps, mapDispatchToProps)(EmailVerificationContainer));

export default reduxForm({
  form: 'emailVerificationForm',
  validate,
  warn: warnings,
  touchOnBlur: false,
  destroyOnUnmount: false,
})(Component);
