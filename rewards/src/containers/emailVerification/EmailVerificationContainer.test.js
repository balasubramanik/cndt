import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import EmailVerificationCont, { EmailVerificationContainer } from '../../containers/emailVerification';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('EmailVerificationContainer', () => {
  window.open = jest.fn();
  const changeState = jest.fn();
  const handleSubmit = jest.fn();
  const getRegistrationInfo = jest.fn();
  const onSubmit = jest.fn();
  const gtmRoofInstaller = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const syncErrors = {
    email: 'Please enter valid email',
  };
  const submitForm = jest.fn();
  const tree = shallow(<EmailVerificationContainer
    anyTouched
    changeState={changeState}
    values={{}}
    status="pending"
    location={{
      search: '?email=sample@test.com',
    }}
    gtmActions={{
      gtmRoofInstaller,
    }}
    syncErrors={syncErrors}
    handleSubmit={handleSubmit}
    submitForm={submitForm}
    onSubmit={onSubmit}
    getRegistrationInfo={getRegistrationInfo}
  />);
  tree.instance().recaptchaInstance = {
    reset: jest.fn(),
  };
  it('should be defined', () => {
    expect(EmailVerificationContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Email Form', () => {
    expect(tree.find('form[name="emailVerificationForm"]').length).toBe(1);
  });

  it('should render Name', () => {
    expect(tree.find('Name').length).toBe(1);
  });

  it('should render Email', () => {
    expect(tree.find('Email').length).toBe(1);
  });

  it('should render RoofingCompanyCheck', () => {
    expect(tree.find('RoofingCompanyCheck').length).toBe(1);
  });

  it('should render Terms and conditions', () => {
    expect(tree.find('.terms-and-conditions').length).toBe(1);
  });

  it('should render Next button', () => {
    expect(tree.find('.next-btn').length).toBe(1);
  });

  it('should render VerifyCallback', () => {
    const { verifyCallback } = tree.instance();
    verifyCallback('123');
    expect(submitForm).toBeCalled();
  });
  it('should call submitHandler when type is Change roof options', () => {
    const simulateClick = tree.find('RoofingCompanyCheck').prop('onChange');
    simulateClick('CHANGE_ROOF_OPTIONS', 23, event);
    expect(gtmRoofInstaller).toBeCalled();
    expect(changeState).toBeCalled();
  });

  it('should call submitHandler when type is not change roof options', () => {
    const simulateClick = tree.find('RoofingCompanyCheck').prop('onChange');
    simulateClick('CHANGE_ATS', 23, event);
    expect(changeState).toBeCalled();
  });

  // it('should call roofOptionChangeHandler', () => {
  //   const submitHandler = tree.find('form[name="emailVerificationForm"]').prop('onSubmit');
  //   submitHandler({
  //     firstName: 'Adam',
  //     lastName: 'Sandler',
  //     email: 'asand@aol.com',
  //     isRoofingContractor: true,
  //     description: 'ahgghk jhjkhjlkh',
  //     installedOption: 'adadooi jhh',
  //     authorizeeDesignation: 'manager',
  //     contractorClassification: 'others',
  //   }, submitForm);
  //   expect(onSubmit).toBeCalled();
  // });
});

describe('EmailVerificationContainer with connected component', () => {
  window.open = jest.fn();
  const state = {
    form: {
      registrationForm: {
        status: 'completed',
      },
      emailVerificationForm: {
        values: {
          contractorClassification: 'others',
        },
        handleSubmit: jest.fn(),
        changeState: jest.fn(),
        getRegistrationInfo: jest.fn(),
        anyTouched: true,
        syncErrors: {
          confirmEmail: 'invalid confirm email',
          email: 'Please enter valid email',
        },
      },
    },
  };
  const store = fakeStore(state);
  const changeState = jest.fn();
  const submitForm = jest.fn();
  const onSubmit = jest.fn();
  const gtmRoofInstaller = jest.fn();
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <EmailVerificationCont
        location={{
          search: '',
        }}
        changeState={changeState}
        submitForm={submitForm}
        onSubmit={onSubmit}
        gtmActions={{
          gtmRoofInstaller,
        }}
      />
    </Router>
                     </Provider>);

  it('should be defined', () => {
    expect(EmailVerificationCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call roofOptionChangeHandler', () => {
    const routeEle = tree.instance().props.children;
    const emailContainer = routeEle.props.children;
    const submitHandler = tree.find('form[name="emailVerificationForm"]').prop('onSubmit');
    submitHandler();
    expect(emailContainer.props.onSubmit).toBeDefined();
  });
});

describe('EmailVerificationContainer with connected component', () => {
  window.open = jest.fn();
  const state = {
    form: {
      registrationForm: {
        status: 'completed',
      },
      emailVerificationForm: {
        values: {
          contractorClassification: 'XXX',
        },
        handleSubmit: jest.fn(),
        changeState: jest.fn(),
        getRegistrationInfo: jest.fn(),
        anyTouched: true,
        syncErrors: {
          confirmEmail: 'invalid confirm email',
          email: 'Please enter valid email',
        },
      },
    },
  };
  const store = fakeStore(state);
  const changeState = jest.fn();
  const submitForm = jest.fn();
  const onSubmit = jest.fn();
  const gtmRoofInstaller = jest.fn();
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <EmailVerificationCont
        location={{
          search: '',
        }}
        changeState={changeState}
        submitForm={submitForm}
        onSubmit={onSubmit}
        gtmActions={{
          gtmRoofInstaller,
        }}
      />
    </Router>
                     </Provider>);

  it('should be defined', () => {
    expect(EmailVerificationCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });


  it('should call roofOptionChangeHandler', () => {
    const routeEle = tree.instance().props.children;
    const emailContainer = routeEle.props.children;
    const submitHandler = tree.find('form[name="emailVerificationForm"]').prop('onSubmit');
    submitHandler();
    expect(emailContainer.props.onSubmit).toBeDefined();
  });
});
