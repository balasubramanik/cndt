const emailVerification = {
  VIEWTERMS_AND_CONDITIONS: 'View the  Terms and Conditions for ',
  GAF_REWARDS: 'GAF Rewards',
  NEXT: 'NEXT',
  ERROR_MESSAGE: 'Please enter all mandatory fields',
  INDICATES_REQUIRED_FIELD: '* Indicates required field',
};

export { emailVerification };
