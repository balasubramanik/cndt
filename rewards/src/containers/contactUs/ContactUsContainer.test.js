import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import ContactUsContainer, { ContactUs } from '../../containers/contactUs';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';

configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('Contactus Container', () => {
  window.open = jest.fn();
  const submitContactUsSend = jest.fn();
  const handleSubmit = jest.fn();
  const changeformstatehandler = jest.fn();
  const resetcontacthandler = jest.fn();
  const contactusFormSubmit = jest.fn();
  const gtmContactusClaimDetails = jest.fn();
  const gtmContactusClaimStatus = jest.fn();
  const syncErrors = {
    question: 'Please provide your reasons',
  };
  const tree = shallow(<ContactUs
    submitContactUsSend={submitContactUsSend}
    resetContactUs={resetcontacthandler}
    handleSubmit={handleSubmit}
    successContactUS
    isLoading
    syncErrors={syncErrors}
    changeFormState={changeformstatehandler}
    gtmActions={{ contactusFormSubmit, gtmContactusClaimDetails, gtmContactusClaimStatus }}
  />);
  it('should be defined', () => {
    expect(ContactUs).toBeDefined();
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });


  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      successContactUS: false,
    });
    expect(contactusFormSubmit).toBeCalled();
  });
  it('should not call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      successContactUS: true,
    });
    expect(contactusFormSubmit).toBeCalled();
  });
});

describe('Contactus Container with redirect links', () => {
  window.open = jest.fn();
  const submitContactUsSend = jest.fn();
  const handleSubmit = jest.fn();
  const changeformstatehandler = jest.fn();
  const resetcontacthandler = jest.fn();
  const contactusFormSubmit = jest.fn();
  const gtmContactusClaimDetails = jest.fn();
  const gtmContactusClaimStatus = jest.fn();
  const syncErrors = {
    question: 'Please provide your reasons',
  };
  const event = {
    preventDefault: jest.fn(),
    target: {
      value: '',
    },
  };
  const tree = shallow(<ContactUs
    submitContactUsSend={submitContactUsSend}
    resetContactUs={resetcontacthandler}
    handleSubmit={handleSubmit}
    successContactUS={false}
    isLoading
    match={{ params: { claimId: '123' } }}
    syncErrors={syncErrors}
    changeFormState={changeformstatehandler}
    gtmActions={{ contactusFormSubmit, gtmContactusClaimDetails, gtmContactusClaimStatus }}
  />);
  it('should be defined', () => {
    expect(ContactUs).toBeDefined();
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should redirect to claim status', () => {
    const simulateClick = tree.find('a').at(0).prop('onClick');
    simulateClick(event);
    expect(gtmContactusClaimStatus).toBeCalled();
  });
  it('should redirect to claim details', () => {
    const simulateClick = tree.find('a').at(1).prop('onClick');
    simulateClick(event);
    expect(gtmContactusClaimDetails).toBeCalled();
  });
  it('should call Change form state whenever special characters occur', () => {
    const simulateClick = tree.find('Field').prop('restrictFormat');
    simulateClick('AGJGHGHGGKG JHJJHJ%');
    expect(changeformstatehandler).toBeCalled();
  });
});

describe('Contactus Container with connected component', () => {
  window.open = jest.fn();
  const state = {
    contactUsState: {
      contactUsSuccessFlag: true,
      isLoading: false,
    },
    myProfileState: {
      userInfo: {
        contractorName: 'contractorName',
        contractorCertificationNumber: 12222,
      },
    },
    form: {
      contactUsForm: {
        values: {},
        syncErrors: {
          firstName: 'Please provide your reasons',
        },
      },
    },
  };
  const syncErrors = {
    question: 'Please provide your reasons',
  };
  const store = fakeStore(state);
  const submitContactUsForm = jest.fn();
  const handleSubmit = jest.fn();
  const resetcontacthandler = jest.fn();
  const changeformstatehandler = jest.fn();

  const submitContactUsSend = jest.fn();
  const contactusFormSubmit = jest.fn();
  const gtmContactusClaimDetails = jest.fn();
  const gtmContactusClaimStatus = jest.fn();
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <ContactUsContainer
        submitContactUsSend={submitContactUsSend}
        handleSubmit={handleSubmit}
        changeFormState={changeformstatehandler}
        successContactUS
        isLoading
        syncErrors={syncErrors}
        resetContactUs={resetcontacthandler}
        onSubmit={submitContactUsForm}
        gtmActions={{ contactusFormSubmit, gtmContactusClaimDetails, gtmContactusClaimStatus }}
      />
    </Router>
  </Provider>);

  it('should be defined', () => {
    expect(ContactUsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Email Form', () => {
    expect(tree.find('form[name="contactUsForm"]').length).toBe(1);
  });

  it('should render question', () => {
    expect(tree.find('Field[id="question"]').length).toBe(1);
  });

  it('should render button', () => {
    expect(tree.find('.claim-submit-btn').length).toBe(1);
  });
});

describe('Contactus Container with form submit', () => {
  window.open = jest.fn();
  const state = {
    contactUsState: {
      contactUsSuccessFlag: true,
      isLoading: false,
    },
    myProfileState: {
      userInfo: {
        contractorName: 'contractorName',
        contractorCertificationNumber: 12222,
      },
    },
    form: {
      contactUsForm: {
        values: {},
        syncErrors: {
          firstName: 'Please provide your reasons',
        },
      },
    },
  };
  const syncErrors = {
    question: 'Please provide your reasons',
  };
  const store = fakeStore(state);
  const submitContactUsForm = jest.fn();
  const resetcontacthandler = jest.fn();
  const changeformstatehandler = jest.fn();
  const submitContactUsSend = jest.fn();
  const contactusFormSubmit = jest.fn();
  const gtmContactusClaimDetails = jest.fn();
  const gtmContactusClaimStatus = jest.fn();
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <ContactUsContainer
        submitContactUsSend={submitContactUsSend}
        changeFormState={changeformstatehandler}
        successContactUS={false}
        isLoading
        syncErrors={syncErrors}
        resetContactUs={resetcontacthandler}
        onSubmit={submitContactUsForm}
        gtmActions={{ contactusFormSubmit, gtmContactusClaimDetails, gtmContactusClaimStatus }}
      />
    </Router>
  </Provider>);

  it('should be defined', () => {
    expect(ContactUsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should call submit function for form', () => {
    const routeEle = tree.instance().props.children;
    const contactUsContainer = routeEle.props.children;
    const submitHandler = tree.find('form[name="contactUsForm"]').prop('onSubmit');
    submitHandler();
    expect(contactUsContainer.props.onSubmit).toBeDefined();
  });
});

describe('Contactus Container with empty state values', () => {
  const state = {
    contactUsState: {
      contactUsSuccessFlag: true,
      isLoading: false,
    },
    form: {},
  };
  const syncErrors = {
    question: 'Please provide your reasons',
  };
  const store = fakeStore(state);
  const submitContactUsForm = jest.fn();
  const handleSubmit = jest.fn();
  const resetcontacthandler = jest.fn();
  const changeformstatehandler = jest.fn();

  const submitContactUsSend = jest.fn();
  const contactusFormSubmit = jest.fn();
  const gtmContactusClaimDetails = jest.fn();
  const gtmContactusClaimStatus = jest.fn();
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <ContactUsContainer
        submitContactUsSend={submitContactUsSend}
        handleSubmit={handleSubmit}
        changeFormState={changeformstatehandler}
        successContactUS
        isLoading
        syncErrors={syncErrors}
        resetContactUs={resetcontacthandler}
        onSubmit={submitContactUsForm}
        gtmActions={{ contactusFormSubmit, gtmContactusClaimDetails, gtmContactusClaimStatus }}
      />
    </Router>
  </Provider>);

  it('should be defined', () => {
    expect(ContactUsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
