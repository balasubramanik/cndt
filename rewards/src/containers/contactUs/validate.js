const validate = (values) => {
  const errors = {};
  const question = values.question ? values.question.trim() : '';
  if (!question) {
    errors.question = 'Please provide your reasons.';
  }
  return errors;
};

export { validate };
