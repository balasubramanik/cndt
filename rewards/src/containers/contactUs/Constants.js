const constants = {
  pageTitle: 'Contact Us',
  PageContent: 'Please provide details below.',
  ClaimStatus: 'Claim Status',
  ClaimDetails: 'Claim Details',
  SUBMIT: 'SUBMIT',
  CANCEL: 'CANCEL',
  SUCCESSTITLE: 'Success!',
  SUCCESSCONTENT1: 'A customer service representative will be in touch with you shortly. ',
  SUCCESSCONTENT2: 'If you have any other questions, ',
  CALL: 'call',
  EMAIL: 'or  email',
};
export { constants };
