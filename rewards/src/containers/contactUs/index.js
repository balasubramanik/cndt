import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import { withRouter } from 'react-router-dom';
import { reduxForm, Field, change } from 'redux-form';
import { history } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import Loader from '../../components/loader';
import { submitContactUs, resetContactUs } from '../../actionCreators/ContactUs';
import TextArea from '../../components/textArea';
import { constants } from './Constants';
import { validate } from './validate';
import ErrorNotification from '../../components/errorNotification';
import { pageTitle, inquiryDetails } from '../../constants/constants';
import * as gtmActionsCreators from '../../actionCreators/GTM';


export class ContactUs extends PureComponent {
  componentDidMount() {
    this.props.resetContactUs();
  }

  componentDidUpdate(prevProps) {
    const { successContactUS, gtmActions } = this.props;
    if ((prevProps.successContactUS !== successContactUS) && successContactUS) {
      gtmActions.contactusFormSubmit();
    }
  }
  restrictAllowSpecialCharater = (value = '') => {
    const val = value.replace(/[!<>(){}^|£€₹[\]]/g, '');
    this.props.changeFormState('contactUsForm', 'question', val);
    return val;
  }

  redirectToClaimsDetails = (e) => {
    const { match, gtmActions } = this.props;
    e.preventDefault();
    history.push(RouteConstants.CLAIM_DETAILS.replace(':claimId', match.params.claimId));
    gtmActions.gtmContactusClaimDetails();
  }

  redirectToClaimsStatus = (e) => {
    const { gtmActions } = this.props;
    e.preventDefault();
    history.push({
      pathname: RouteConstants.CLAIM_STATUS,
    });
    gtmActions.gtmContactusClaimStatus();
  };


  submitContactUsForm = (data) => {
    const { contractorName, contractorCertificationNumber, match } = this.props;
    const formatValue = { ...data };
    formatValue.Id = match.params.claimId;
    formatValue.contractorName = contractorName.toString();
    formatValue.certification = contractorCertificationNumber;
    this.props.submitContactUsSend(formatValue);
  }

  sucessContactUs = () => (
    <Grid>
      <section className="invoice-submit-success">
        <div className="tick-icon">
          <p>
            <span className="icon-check"></span>
          </p>
        </div>
        <h2>{constants.SUCCESSTITLE}</h2>
        <p>{constants.SUCCESSCONTENT1}</p>
        <p>{constants.SUCCESSCONTENT2}</p>
        <p>{constants.CALL} {inquiryDetails.Phone} {inquiryDetails.Email}.</p>
      </section>
    </Grid>
  );

  /** @description callback function for clear error
   * @param {object} e - triggered event(click)
   * @param {func} action - action for clear error
   */
  clearError = (e, action) => {
    e.preventDefault();
    action();
  }

  /** @description function to render the error message for EMS */
  renderErrorMessage = () => {
    const {
      error,
      submitContactUsSend,
    } = this.props;
    let props = {};
    props = {
      error,
      onClear: (e) => this.clearError(e, submitContactUsSend.clearErrors),
    };
    return (<ErrorNotification {...props} />);
  }

  renderContactUs = () => {
    const { handleSubmit } = this.props;
    return (
      <Grid>
        <div className="gaf-title-block">
          <h2>{constants.pageTitle}</h2>
          <div className="links claim-contactus-links">
            <a href="/" onClick={this.redirectToClaimsStatus}>{constants.ClaimStatus}</a><span className="separator">|</span><a href="/" onClick={this.redirectToClaimsDetails}>{constants.ClaimDetails}</a>
          </div>
        </div>
        <div className="contact-us-response">
          <form
            name="contactUsForm"
            onSubmit={handleSubmit(this.submitContactUsForm)}
          >
            <Row className="Reasons">
              <Col sm={12}>
                <p>{constants.PageContent}<span className="mandatory-icon">*</span></p>
                <Field
                  className="gaf-form"
                  name="question"
                  id="question"
                  pattern="true"
                  restrictFormat={this.restrictAllowSpecialCharater}
                  component={TextArea}
                  minLength={3}
                  maxLength={500}
                  countPosition="text-counter"
                />
                {this.renderSubmitButton()}
              </Col>
            </Row>
          </form>
        </div>
      </Grid>
    );
  }

  renderSubmitButton = () => {
    const { syncErrors } = this.props;
    const btnClass = classNames('btn-block gaf-btn-primary', { 'btn-disabled': syncErrors });
    return (
      <div className="claim-cancel-submit-btn clearfix">
        <div className="claim-cancel-btn ">
          <button className="btn btn-block gaf-btn-secondary" onClick={this.redirectToClaimsDetails}>{constants.CANCEL}</button>
        </div>
        <div className="claim-submit-btn">
          <button type="submit" className={btnClass}>{constants.SUBMIT}</button>
        </div>
      </div>
    );
  }

  render() {
    const { isLoading } = this.props;
    return (
      <div>
        <Helmet>
          <title>{pageTitle.CONTACTUS}</title>
        </Helmet>
        <main className="main-content claim-contactus">
          {
            this.props.successContactUS ?
              this.sucessContactUs() :
              this.renderContactUs()
          }
        </main>
        {isLoading && <Loader />}
        {this.renderErrorMessage()}
      </div>
    );
  }
}

function mapStateToProps(state, match) {
  const { form, myProfileState = {}, contactUsState } = state;
  const { successContactUS = false, isLoading, error } = contactUsState;
  const { contactUsForm = {} } = form;
  const { syncErrors, values } = contactUsForm;
  const { contractorName = '', contractorCertificationNumber = '' } = myProfileState.userInfo || {};
  return {
    successContactUS,
    isLoading,
    contractorName,
    contractorCertificationNumber,
    error,
    match,
    syncErrors,
    values,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    submitContactUsSend: bindActionCreators(submitContactUs, dispatch),
    resetContactUs: bindActionCreators(resetContactUs, dispatch),
    changeFormState: bindActionCreators(change, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

ContactUs.propTypes = {
  submitContactUsSend: PropTypes.func,
  resetContactUs: PropTypes.func,
  syncErrors: PropTypes.object,
  handleSubmit: PropTypes.func,
  isLoading: PropTypes.bool,
  successContactUS: PropTypes.bool,
  contractorName: PropTypes.string,
  contractorCertificationNumber: PropTypes.string,
  error: PropTypes.any,
  match: PropTypes.object,
  changeFormState: PropTypes.func,
  gtmActions: PropTypes.object.isRequired,
};

const Component = connect(mapStateToProps, mapDispatchToProps)(withRouter(ContactUs));

export default reduxForm({
  form: 'contactUsForm',
  validate,
  touchOnBlur: false,
})(Component);

