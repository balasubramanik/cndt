import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Grid, Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import * as getPromotion from '../../actionCreators/spotPromotion';
import { spotpromotion } from './constant';
import SpotPromotionList from '../../components/spotPromotionList';
import Pagination from '../../components/pagination';
import ViewBy from '../../components/viewBy';
import ShowBy from '../../components/showBy';
import SearchBy from '../../components/searchBy';
import Loader from '../../components/loader';
import { searchByType, pageTitle } from '../../constants/constants';
import { searchShowBy } from '../../components/showBy/constants';
import RouteConstants from '../../constants/RouteConstants';
import { generateYearRange } from '../../utils/utils';
import { history } from '../../routes';

const currentYear = moment().year();
class SpotPromotionAdmin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      viewByValue: currentYear,
      isShowByOptSelected: Number(searchShowBy.showBy[0].value),
      sortOrder: '',
      searchPromoTitle: '',
      isResetSearch: false,
      isSearchByIcon: true,
      currentPageNumber: 1,
      isPaginationCall: false,
      isInitialPagination: true,
      isPromoTitleSubmitted: false,
      orderByColumn: 'modifiedOn',
      advPaginationRec: 1,
      isAdvPaginationLoading: false,
      searchByChar: {
        min: 1,
      },
    };
  }

  componentDidMount() {
    this.props.actions.getPromotionListClear();
    this.makingPromotionListAPI();
  }

  componentDidUpdate(prevProps) {
    const { paging, isLoading } = this.props;
    if (this.state.isInitialPagination && paging.totalRecords > this.state.isShowByOptSelected) this.nextPaginationCall(prevProps);
    if (this.state.isAdvPaginationLoading && (prevProps.isLoading !== isLoading) && !isLoading) {
      this.resetLazyLoading();
    }
  }

  /* Pagination Handle for Previous & Next Buttons */
  getPaginationNumber = (pageNo, btnName) => {
    const { spotPromotion, paging } = this.props;
    const { isShowByOptSelected, advPaginationRec } = this.state;
    this.setState({
      currentPageNumber: pageNo,
      isResetSearch: true,
    }, () => {
      if (btnName === 'next') {
        const advPageNo = pageNo + advPaginationRec;
        const advRecordsCount = isShowByOptSelected * advPageNo;
        if (spotPromotion && (Object.keys(spotPromotion).length > 0) && (advRecordsCount <= Math.ceil(paging.totalRecords / isShowByOptSelected) * isShowByOptSelected)) {
          if (!spotPromotion[Number(advPageNo)]) {
            this.setState({
              isPaginationCall: true,
              isAdvPaginationLoading: true,
            }, () => {
              this.makingPromotionListAPI();
            });
          }
        }
      } else {
        const currentRecordsCount = isShowByOptSelected * pageNo;
        if (spotPromotion && (Object.keys(spotPromotion).length > 0) && (currentRecordsCount <= Math.ceil(paging.totalRecords / isShowByOptSelected) * isShowByOptSelected)) {
          if (!spotPromotion[Number(pageNo)]) {
            this.setState({
              isPaginationCall: false,
            }, () => {
              this.makingPromotionListAPI();
            });
          }
        }
      }
    });
  }

  /* Reset the advance pagination loading */
  resetLazyLoading = () => {
    this.setState({
      isAdvPaginationLoading: false,
    });
  }

  /* Set Advance Claims List API call for Pagination lazy loading */
  advancePaginationRec = () => {
    window.setTimeout(() => {
      this.setState({ isInitialPagination: true });
    }, 500);
  }

  /* Creating Advance Pagination API Call for advance pages */
  nextPaginationCall = () => {
    const { spotPromotion } = this.props;
    const listOfPromotionLength = Object.keys(spotPromotion).length;
    if (spotPromotion && listOfPromotionLength > 0) {
      this.setState({
        isInitialPagination: false,
        isPaginationCall: true,
      }, () => {
        this.makingPromotionListAPI();
      });
    }
  }

  /* View By Option Selection */
  viewByOptionSelection = (viewBy) => {
    this.props.actions.getPromotionListClear();
    this.setState({
      viewByValue: viewBy,
      isPaginationCall: false,
      currentPageNumber: 1,
      searchPromoTitle: '',
      isResetSearch: true,
    }, () => {
      this.makingPromotionListAPI();
      this.advancePaginationRec();
    });
  }

  /* Making API call for Promotion List */
  makingPromotionListAPI = () => {
    const request = this.creatingPromoListRequest().promotionList;
    if (this.state.isPaginationCall) {
      request.paging.pageNo = this.state.currentPageNumber + this.state.advPaginationRec;
      this.props.actions.getPromotion(request);
    } else {
      this.props.actions.getPromotion(request);
    }
  }

  /* Show By Option Selection */
  showPageValue = (showPage) => {
    this.props.actions.getPromotionListClear();
    this.setState({
      isShowByOptSelected: Number(showPage),
      currentPageNumber: 1,
      isPaginationCall: false,
      isResetSearch: true,
    }, () => {
      this.makingPromotionListAPI();
      this.advancePaginationRec();
    });
  }

  /* Triggering Action when entering search text */
  searchByText = (value) => {
    this.setState({ isResetSearch: false });
    if (this.state.isPromoTitleSubmitted && !value) {
      this.props.actions.getPromotionListClear();
      this.setState({
        isPaginationCall: false,
        currentPageNumber: 1,
        searchPromoTitle: '',
        isPromoTitleSubmitted: false,
      }, () => {
        this.makingPromotionListAPI();
        this.advancePaginationRec();
      });
    }
  }

  /* Triggering Action when Search Promotion Title Submitted */
  searchPromoTitleSubmitted = (promoTitle, bool) => {
    if (promoTitle && bool) {
      this.props.actions.getPromotionListClear();
      this.setState({
        isPaginationCall: false,
        currentPageNumber: 1,
        searchPromoTitle: promoTitle,
        isPromoTitleSubmitted: true,
      }, () => {
        this.makingPromotionListAPI();
      });
    }
    if (!promoTitle || !bool) {
      this.setState({
        isResetSearch: false,
      });
    }
  }

  /* Sorting Table header coloumn in ascending or descending order */
  sortingDataOrder = (key, type) => {
    this.setState({
      orderByColumn: key.charAt(0).toUpperCase() + key.substr(1),
      sortOrder: type ? 'ASC' : 'DESC',
      isPaginationCall: false,
      currentPageNumber: 1,
      isResetSearch: true,
    }, () => {
      this.props.actions.getPromotionListClear();
      this.makingPromotionListAPI();
      this.advancePaginationRec();
    });
  }

  /* Creating Request for Promotion List API Call */
  creatingPromoListRequest = () => {
    const {
      viewByValue,
      searchPromoTitle,
      sortOrder,
      isShowByOptSelected,
      currentPageNumber,
      orderByColumn,
    } = this.state;
    const request = {
      promotionList: {
        filter: {
          promotionTitle: searchPromoTitle,
          viewBy: viewByValue,
        },
        applyPaging: true,
        paging: {
          pageNo: currentPageNumber,
          pageSize: isShowByOptSelected,
        },
        sortBy: orderByColumn,
        sortOrder,

      },
    };
    return request;
  }

  formatYearRange = () => {
    const years = generateYearRange(currentYear, 4);
    if (!years) {
      return [];
    }
    return years.map((year) => ({
      label: year,
      value: year,
    }));
  }

  CreateNewPromo = () => {
    history.push(RouteConstants.NEW_SPOT_PROMOTION);
  };

  SpotPromotionTitleBlock = () => (
    <Grid>
      <div className="gaf-title-block">
        <h2>{spotpromotion.header.spotPromotion}</h2>
        <p>{spotpromotion.header.spotPromotionText}</p>
      </div>
    </Grid>
  )

  SpotPromotionTableHeader = () => (
    <div className="gaf-table-header">
      <Row className="show-grid">
        <Col xs={8} sm={4} md={4} lg={6}>
          <h4 className="hidden-xs">{spotpromotion.header.tableHeader}</h4>
        </Col>
        <Col xs={12} sm={8} md={8} lg={6}>
          <div className="new-promo-btn pull-right">
            <button className="btn btn-block gaf-btn-primary" onClick={this.CreateNewPromo}>{spotpromotion.header.createNewPromo}</button>
          </div>
        </Col>
      </Row>
    </div>
  )

  render() {
    const { spotPromotion, isLoading, paging } = this.props;
    const {
      currentPageNumber, isPaginationCall, isAdvPaginationLoading, isShowByOptSelected,
    } = this.state;
    return (
      <div>
        <Helmet>
          <title>{pageTitle.SPOTPROMOTION_MANAGEMENT}</title>
        </Helmet>
        {/* Center Body Content */}
        <main className="main-content">
          {this.SpotPromotionTitleBlock()}
          <Grid>
            <section className="gaf-page-info spot-promotion-page-info">
              <div className="spot-promotion-block">
                <Row className="show-grid">
                  <Col xs={12} sm={6} lg={6} >
                    <ViewBy id={searchByType.PromotionTitle} options={this.formatYearRange()} onChange={(viewBy) => this.viewByOptionSelection(viewBy)} />
                  </Col>
                  <Col xs={12} sm={6} lg={6}>
                    <form>
                      <div className="form-group gaf-form pull-right">
                        <SearchBy id={searchByType.PromotionTitle} placeholder={spotpromotion.header.searchByPlaceHolder} handleChange={this.searchByText} handleSubmit={this.searchPromoTitleSubmitted} isResetSearch={this.state.isResetSearch} typeahead="false" isSearchByIcon={this.state.isSearchByIcon} charLength={this.state.searchByChar} />
                      </div>
                    </form>
                  </Col>
                </Row>
              </div>
              {this.SpotPromotionTableHeader()}
              <SpotPromotionList data={spotPromotion[currentPageNumber]} sortingData={this.sortingDataOrder} loading={isLoading} defaultSort={this.state.orderByColumn} />
              <div className="gaf-pagination">
                <Row>
                  <Col md={6} sm={6} xsHidden>
                    {(paging.totalRecords > searchShowBy.showBy[0].value) && <ShowBy onChange={(showPage) => this.showPageValue(showPage)} defaultValue={isShowByOptSelected} />}
                  </Col>
                  <Col md={6} sm={6} xs={12}>
                    {(spotPromotion && Object.keys(spotPromotion).length > 0) && (paging.totalRecords > this.state.isShowByOptSelected) && <Pagination perPage={this.state.isShowByOptSelected} totalRows={paging.totalRecords} pageClick={this.getPaginationNumber} />}
                  </Col>
                </Row>
              </div>
            </section>
            {((isLoading && !isPaginationCall) || (isAdvPaginationLoading && (paging.pageNo < currentPageNumber))) ? <Loader /> : null}
          </Grid>
        </main>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { spotPromotionState } = state;
  const { spotPromotion, isLoading, paging = { totalRecords: 0 } } = spotPromotionState;
  return {
    spotPromotion,
    isLoading,
    paging,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(getPromotion, dispatch),
  };
}

SpotPromotionAdmin.propTypes = {
  spotPromotion: PropTypes.object,
  actions: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  paging: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(SpotPromotionAdmin);
