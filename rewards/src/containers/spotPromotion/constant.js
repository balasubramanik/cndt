const spotpromotion = {
  header: {
    spotPromotion: 'Spot Promotion',
    spotPromotionText: 'View all promotions including current, past, and pending.',
    tableHeader: 'List of Promotions',
    createNewPromo: 'CREATE NEW PROMO',
    searchByPlaceHolder: 'Search by Promo Title',
  },
  table: {
    norecords: 'No matching records found.',
  },
};

export { spotpromotion };
