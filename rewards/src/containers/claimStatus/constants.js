export const searchViewBy = {
  viewBy: [
    { label: 'Last 30 Days', value: 'Last 30 Days' },
    { label: 'Last 60 Days', value: 'Last 60 Days' },
    { label: 'Last 90 Days', value: 'Last 90 Days' },
    { label: 'Custom', value: 'Custom' },
  ],
};
export const matchCriteria = {
  Match: [
    { label: 'All', value: 'All' },
    { label: 'Any', value: 'Any' },
  ],
  UserName: [
    { title: 'Imthiyas', value: 'Imthiyas' },
    { title: 'John', value: 'John' },
    { title: 'Mike', value: 'Mike' },
  ],
  Status: [
    { label: 'Show All', value: 'Show All Claims' },
    { label: 'Completed', value: 'Completed' },
    { label: 'Pending', value: 'Pending' },
    { label: 'Submitted', value: 'Submitted' },
  ],
  ShowAll: [{ label: 'Show All', value: '' }],
};
export const labelName = {
  title: 'Claim Status',
  subTitle: 'From submitted to processed and everything in between.',
  customSearch: 'Custom Search',
  match: 'Match',
  criteria: 'of the following criteria',
  product: 'Product',
  userName: 'User Name',
  status: 'status',
  clear: 'clear',
  apply: 'apply',
  listOfClaims: 'List of Claims',
  fromDate: 'From',
  toDate: 'To',
  toDateValidation: 'Please select "to" date.',
  searchByPlaceholder: 'Search by Claim Number',
  validClaimNo: 'Please enter a valid claim number.',
  validProductName: 'Please enter a valid product name.',
  productLabelName: 'Product',
};
export const GTMEventMessage = {
  expand: 'Expand',
  hide: 'Hide',
  clear: 'Clear Claim Status Filters',
  apply: 'Claim Status Filters Applied',
  CSVExport: 'List of Claims',
};
