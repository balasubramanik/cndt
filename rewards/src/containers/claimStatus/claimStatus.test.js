import React from 'react';
import { configure, mount, shallow } from 'enzyme';
// import { Router } from 'react-router-dom';
// import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import ClaimStatus from '../../containers/claimStatus';
import { history } from '../../routes';
// import { fakeStore } from '../../config/jest/fakeStore';
// import DatePicker from 'react-datepicker';
// import moment from 'moment';
// import ViewBy from '../../components/viewBy';
// import Dropdown from '../../components/dropdown';
// import SearchBy from '../../components/searchBy';

configure({ adapter: new Adapter() });
history.push = jest.fn();
// window.open = jest.fn();
describe('ClaimStatus', () => {
  const getSuggestion = jest.fn();
  const getClaimsList = jest.fn();
  const props = {
    actions: {
      getSuggestion,
      getClaimsList,
      CLAIM_STATUS_NUMBER: {
        claim: 'G0012345',
        user: 'J. Doe',
        submitted: '09/19/2018',
        updated: 'N/A',
        value: 'N/A',
        pts: '10',
        status: 'submitted',
      },
      CLAIM_PRODUCT_DETAILS: {
        claim: 'G0012345',
        user: 'J. Doe',
        submitted: '09/19/2018',
        updated: 'N/A',
        value: 'N/A',
        pts: '10',
        status: 'submitted',
      },
      listOfClaims: {
        claimId: 'G0000004',
        contractorAccountId: '1000247',
        firstName: 'Boyd',
        lastName: 'Melton',
        claimStatus: 'processed',
        createdBy: 'updated@gmail.com',
        createdOn: '2018-08-31',
        submittedOn: '2018-08-31',
        modifiedBy: 'updated@gmail.com',
        modifiedOn: '2018-08-31',
        impersonatedBy: 'updated@gmail.com',
        impersonatedOn: '2018-08-31T19:06:35.6764259+05:30',
        invoices: [{
          fileName: '3402972_14604582.pdf',
          filePath: 'https://rewardsdevuse.blob.core.windows.net/rewards-app-private/Invoices/3402972_14604582.pdf',
        },
        {
          fileName: '3403001_14604682.pdf',
          filePath: 'https://rewardsdevuse.blob.core.windows.net/rewards-app-private/Invoices/3403001_14604682.pdf',
        },
        ],
        invoiceDetails: null,
      },
      user: {
        firstName: 'John',
        lastname: 'Doe',
      },
      isLoading: false,
    },
  };
  const tree = shallow(<ClaimStatus
    {...props}
  />);
  it('should be defined', () => {
    expect(ClaimStatus).toBeDefined(1);
  });
  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
  it('should render ClaimStatus container', () => {
    expect(tree.find('.main-content').length).toBe(1);
  });
  it('should render Dropdown Component', () => {
    expect(tree.find('.filterby').length).toBe(1);
  });
  it('should render Search By Component', () => {
    expect(tree.find('.display-inline').length).toBe(1);
  });
  it('should render DatePicker', () => {
    expect(tree.find('DatePicker').length).toBe(1);
  });
  // it('should call onQuickAccessClick ', () => {
  //   const simulateClick = tree.find('QuickAccess').prop('onQuickAccess');
  //   simulateClick();
  //   expect(window.open).toBeCalled();
  // });
});

// describe('ClaimStaus with connected component', () => {
//   const getSuggestion = jest.fn();
//   const getClaimsList = jest.fn();
// }
//   const store = fakeStore(state);
//   const tree = mount(<Provider store={store}>
//       <Router history={history}>
//         <ClaimStatus />
//       </Router>
//     </Provider>);

//   it('should be defined', () => {
//     expect(ClaimStatus).toBeDefined();
//   });

//   it('should render ClaimStatus container', () => {
//     expect(tree.find('.main-content').length).toBe(1);
//   });

//   it('should render Dropdown Component', () => {
//     expect(tree.find('Dropdown').length).toBe(1);
//   });

//   it('should render ViewBy Component', () => {
//     expect(tree.find('DatePicker').length).toBe(1);
//   });

//   it('should render DatePicker', () => {
//     expect(tree.find('DatePicker').length).toBe(1);
//   });

//   it('should render SearchBy Component', () => {
//     expect(tree.find('SearchBy').length).toBe(1);
//   });
// });
