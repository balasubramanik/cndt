import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Col, Row, Grid, Panel, Button, Form, FormGroup, Modal } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FileSaver from 'file-saver';
import moment from 'moment';
import { Helmet } from 'react-helmet';
import { searchByType, pageTitle } from '../../constants/constants';
import ViewBy from '../../components/viewBy';
import { searchViewBy, matchCriteria, labelName, GTMEventMessage } from './constants';
import { searchShowBy } from '../../components/showBy/constants';
import { claimInvoiceDetails } from '../../components/claimsList/constants';
import DatePickerControl from '../../components/datePickerControl';
import Dropdown from '../../components/dropdown';
import SearchBy from '../../components/searchBy';
import ClaimsList from '../../components/claimsList';
import ShowBy from '../../components/showBy';
import Pagination from '../../components/pagination';
import RoleAccessPopup from '../../components/roleAccessPopup';
import * as typeAHeadActionCreators from '../../actionCreators/SearchBy';
import * as claimsListActionCreators from '../../actionCreators/ClaimsList';
import * as invoiceListActionCreators from '../../actionCreators/ClaimInvoiceDetails';
import * as contractorUserActionsCreators from '../../actionCreators/ContractorUser';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import Loader from '../../components/loader';
import { authentication } from '../../routes';
import { formatDateUTC, ascending, descending, formatDate } from '../../utils/utils';
import Tables from '../../components/tables';
import ErrorNotification from '../../components/errorNotification';

class ClaimStatus extends React.Component {
  constructor(props) {
    super(props);
    this.downloadFileDetails = {};
    this.isExportCSVCliked = false;
    this.claimId = '';
    this.state = {
      isOpenAdvanceSearch: false,
      isViewByOptSelected: searchViewBy.viewBy[0].value,
      searchClaimNumber: '',
      searchProductInfo: '',
      isMatchOptSelected: matchCriteria.Match[0].value,
      isUserNameOptSelected: '',
      startDate: undefined,
      endDate: undefined,
      isStatusOptSelected: matchCriteria.Status[0].value,
      searchByPlaceholder: labelName.searchByPlaceholder,
      isSearchByIcon: true,
      searchByChar: {
        min: 9,
        max: 10,
      },
      isClaimNoSubmitted: false,
      productPlaceholder: '',
      isProductIcon: false,
      productKey: 'description',
      productInfoChar: {
        min: 3,
        max: 100,
      },
      isShowByOptSelected: Number(searchShowBy.showBy[0].value),
      sortOrder: 'DESC',
      orderByColumn: 'ModifiedOn',
      isResetSearch: false,
      isResetProduct: false,
      isShowInvoicePopup: false,
      currentPageNumber: 1,
      token: '',
      advPaginationRec: 1,
      isInitialPagination: true,
      isPaginationCall: false,
      isExportCsv: false,
      invoiceDetailsList: [],
      isApplyBtnClicked: false,
      isAdvPaginationLoading: false,
      downloadCSV: false,
    };
  }

  /* Claims List Actions will trigger when component mounted */
  componentDidMount() {
    const { users, contractorUserActions } = this.props;
    if (!users || users.length === 0) {
      contractorUserActions.getUsers();
    }
    this.props.claimsListActions.getClaimsListClear();
    this.makingClaimsListAPI();
  }

  /* Claims List Actions will trigger to get Advance Pagination Records */
  componentDidUpdate(prevProps) {
    const { paging, listOfClaimsCSV } = this.props;
    if (this.state.isInitialPagination && paging.totalRecords > this.state.isShowByOptSelected) this.nextPaginationCall(prevProps);
    if (this.props.isInvoiceDownloadSuccess) this.downloadInvoiceFile();
    const { isLoading, isInvoiceListLoading } = this.props;
    if ((prevProps.isInvoiceListLoading !== isInvoiceListLoading) && !isInvoiceListLoading) {
      this.setInvoiceDetailsList();
    }
    if (this.isExportCSVCliked && (prevProps.isLoading !== isLoading) && !isLoading) {
      if (listOfClaimsCSV && listOfClaimsCSV.length > 0) {
        window.setTimeout(() => {
          this.updateDownloadState(true);
          this.isExportCSVCliked = false;
          /* Trigger the Event for GTM track */
          this.props.gtmActions.gtmClaimStatusCSVExport(GTMEventMessage.CSVExport);
        }, 500);
      }
    }
    if (this.state.isAdvPaginationLoading && (prevProps.isLoading !== isLoading) && !isLoading) {
      this.resetLazyLoading();
    }
  }

  setInvoiceDetailsList = () => {
    const { claimInvoiceData } = this.props;
    this.setState({
      invoiceDetailsList: claimInvoiceData,
    });
  }

  /* Pagination Handle for Previous & Next Buttons */
  getPaginationNumber = (pageNo, btnName) => {
    const { listOfClaims, paging } = this.props;
    const { isShowByOptSelected, advPaginationRec } = this.state;
    this.setState({
      currentPageNumber: pageNo,
      token: paging.token || '',
      isResetSearch: true,
    }, () => {
      if (btnName === 'next') {
        const advPageNo = pageNo + advPaginationRec;
        const advRecordsCount = isShowByOptSelected * advPageNo;
        if (listOfClaims && (Object.keys(listOfClaims).length > 0) && (advRecordsCount <= Math.ceil(paging.totalRecords / isShowByOptSelected) * isShowByOptSelected)) {
          if ((!listOfClaims[Number(advPageNo)])) {
            this.setState({
              isPaginationCall: true,
              isAdvPaginationLoading: true,
            }, () => {
              this.makingClaimsListAPI();
            });
          }
        }
      } else {
        const currentRecordsCount = isShowByOptSelected * pageNo;
        if (listOfClaims && (Object.keys(listOfClaims).length > 0) && (currentRecordsCount <= Math.ceil(paging.totalRecords / isShowByOptSelected) * isShowByOptSelected)) {
          if (!listOfClaims[Number(pageNo)]) {
            this.setState({
              isPaginationCall: false,
            }, () => {
              this.makingClaimsListAPI();
            });
          }
        }
      }
    });
  }

  updateDownloadState = (downloadCSV) => this.setState({ downloadCSV });

  resetLazyLoading = () => {
    this.setState({
      isAdvPaginationLoading: false,
    });
  }

  /* Download invoice file */
  downloadInvoiceFile = () => {
    const { invoiceFileDetails } = this.props;
    const fileName = this.downloadFileDetails.fileName.toLowerCase();
    const mimeType = {};
    if (fileName.indexOf('pdf') > -1) {
      mimeType.type = 'application/pdf';
    } else if (fileName.indexOf('jpeg') > -1) {
      mimeType.type = 'image/jpeg';
    } else if (fileName.indexOf('png') > -1) {
      mimeType.type = 'application/png';
    }
    mimeType.lastModifiedDate = new Date();
    try {
      const blobAsFile = new File([invoiceFileDetails.data], this.downloadFileDetails.fileName, mimeType);
      FileSaver.saveAs(blobAsFile);
    } catch (err) {
      const textFileAsBlob = new Blob([invoiceFileDetails.data], mimeType);
      window.navigator.msSaveBlob(textFileAsBlob, this.downloadFileDetails.fileName);
    }
    this.props.invoiceListActions.downloadInvoiceClear();
    this.downloadFileDetails = {};
  }

  /* Set Advance Claims List API call for Pagination lazy loading */
  advancePaginationRec = () => {
    window.setTimeout(() => {
      this.setState({ isInitialPagination: true });
    }, 500);
  }

  /* Creating Advance Pagination API Call for advance pages */
  nextPaginationCall = () => {
    const { listOfClaims } = this.props;
    const listOfClaimsLength = Object.keys(listOfClaims).length;
    if (listOfClaims && listOfClaimsLength > 0) {
      this.setState({
        isInitialPagination: false,
        isPaginationCall: true,
      }, () => {
        this.makingClaimsListAPI();
      });
    }
  }

  /* Making API call for Claims List */
  makingClaimsListAPI = () => {
    const request = this.creatingClaimListRequest().claimList;
    if (this.state.isPaginationCall) {
      request.paging.pageNo = this.state.currentPageNumber + this.state.advPaginationRec;
      this.props.claimsListActions.getClaimsList(request);
    } else {
      this.props.claimsListActions.getClaimsList(request);
    }
  }

  /* Creating Request for Claims List API Call */
  creatingClaimListRequest = () => {
    const {
      isOpenAdvanceSearch,
      searchClaimNumber,
      searchProductInfo,
      isMatchOptSelected,
      isUserNameOptSelected,
      isStatusOptSelected,
      startDate,
      endDate,
      isViewByOptSelected,
      isShowByOptSelected,
      sortOrder,
      orderByColumn,
      currentPageNumber,
      token,
      isExportCsv,
    } = this.state;
    const viewByDays = (isViewByOptSelected.indexOf('30') > -1) ? 30 : (isViewByOptSelected.indexOf('60') > -1) ? 60 : 90;  //eslint-disable-line
    const { contractorId } = authentication.getAuthDetails();
    const request = {
      claimList: {
        applyPaging: !isExportCsv,
        filter: {
          contractorAccountId: contractorId,
          advancedSearch: isOpenAdvanceSearch,
          claimNumber: searchClaimNumber ? searchClaimNumber.toUpperCase() : '',
          matchAll: isMatchOptSelected.toLowerCase() === matchCriteria.Match[0].value.toLowerCase(),
          product: searchProductInfo,
          userName: isUserNameOptSelected,
          status: isStatusOptSelected.toLowerCase() === matchCriteria.Status[0].value.toLowerCase() ? '' : isStatusOptSelected,
          fromDate: formatDate(startDate),
          toDate: formatDate(endDate),
          viewBy: viewByDays,
          isExportCsv,
        },
        paging: {
          token: token || '',
          pageNo: currentPageNumber,
          pageSize: isShowByOptSelected,
        },
        sortBy: orderByColumn,
        sortOrder,
      },
      contractorId,
    };
    return request;
  }

  /* Advance Search Handler Toogle */
  advanceSearchHandler = (bool) => {
    this.setState({
      isOpenAdvanceSearch: bool,
      searchClaimNumber: '',
      isResetSearch: true,
    });
    /* Trigger the Event for GTM track */
    this.props.gtmActions.gtmClaimStatusAdvanceSearchToggle(bool ? GTMEventMessage.expand : GTMEventMessage.hide);
  }

  /* View By Option Selection */
  viewByOptionSelection = (viewBytype) => {
    this.setState({
      isApplyBtnClicked: false,
      isViewByOptSelected: viewBytype,
      isPaginationCall: false,
      currentPageNumber: 1,
      searchClaimNumber: '',
      token: '',
      isResetSearch: true,
    }, () => {
      if (viewBytype.toLowerCase() !== searchViewBy.viewBy[searchViewBy.viewBy.length - 1].value.toLowerCase()) {
        this.props.claimsListActions.getClaimsListClear();
        this.makingClaimsListAPI();
        this.advancePaginationRec();
      }
    });
    if (viewBytype.toLowerCase() === searchViewBy.viewBy[searchViewBy.viewBy.length - 1].value.toLowerCase()) {
      this.advanceSearchHandler(true);
    } else {
      this.advanceSearchHandler(false);
    }
    /* Trigger the Event for GTM track */
    this.props.gtmActions.gtmClaimStatusViewBySelection(viewBytype);
  }

  /* Triggering Action when entering search number */
  searchByText = (value) => {
    this.setState({ isApplyBtnClicked: false, isResetSearch: false });
    if (this.state.isClaimNoSubmitted && !value) {
      this.props.claimsListActions.getClaimsListClear();
      this.setState({
        isPaginationCall: false,
        currentPageNumber: 1,
        searchClaimNumber: '',
        token: '',
        isClaimNoSubmitted: false,
      }, () => {
        this.makingClaimsListAPI();
        this.advancePaginationRec();
      });
    }
    // this.advanceSearchHandler(false);
  }

  /* Triggering Action when Search Claim Number Submitted */
  searchClaimNoSubmitted = (claimNumber, bool) => {
    if (claimNumber && bool) {
      this.props.claimsListActions.getClaimsListClear();
      this.setState({
        isPaginationCall: false,
        currentPageNumber: 1,
        searchClaimNumber: claimNumber,
        isClaimNoSubmitted: true,
        token: '',
        isOpenAdvanceSearch: false,
      }, () => {
        this.makingClaimsListAPI();
      });
      /* Trigger the Event for GTM Track */
      this.props.gtmActions.gtmClaimStatusSearchClaimNo(claimNumber);
    }
    if (!claimNumber || !bool) {
      this.setState({
        isResetSearch: false,
      });
    }
  }

  /* Triggering Action when entering product information */
  productInfoText = (value, bool) => {
    if (bool) {
      this.setState({ searchProductInfo: value, isResetProduct: false });
      this.props.typeAHeadActions.getSuggestion(value.trim(), searchByType.ClaimProductDetails);
    } else {
      this.setState({ searchProductInfo: '', isResetProduct: false });
    }
  }

  /* Product Information Option Selection */
  productOptionSelected = (productInfo) => {
    this.setState({ isPaginationCall: false, searchProductInfo: productInfo });
  }

  /* Match Option Selection */
  matchHandler = (value) => {
    this.setState({ isMatchOptSelected: value });
  }

  /* User Name Option Selection */
  userNameHandler = (value) => {
    this.setState({ isUserNameOptSelected: value });
  }

  /* Start Date Selection */
  startDateChange = (date) => {
    this.setState({ startDate: date });
    const endDate = moment(this.state.endDate);
    if (!date || date > endDate) this.setState({ endDate: '' });
  }

  /* End Date Selection */
  endDateChange = (date) => {
    this.setState({ endDate: date });
  }

  /* Status Option Selection */
  statusHandler = (value) => {
    this.setState({ isStatusOptSelected: value });
  }

  /* Clear the data when Clear button clicked */
  clearHandler = () => {
    this.setState({
      startDate: undefined,
      endDate: undefined,
      isMatchOptSelected: matchCriteria.Match[0].value,
      isUserNameOptSelected: '',
      isStatusOptSelected: matchCriteria.Status[0].value,
      isResetSearch: true,
      isResetProduct: true,
      isPaginationCall: false,
      currentPageNumber: 1,
      token: '',
      searchProductInfo: '',
      isOpenAdvanceSearch: false,
    }, () => {
      this.advanceSearchHandler(false);
      this.makingClaimsListAPI();
      this.advancePaginationRec();
    });
    /* Trigger the Event for GTM Track */
    this.props.gtmActions.gtmClaimStatusClearBtnClicked(GTMEventMessage.clear);
  }

  /* Dispatching Claims List Action when Apply Handler Clicked */
  applyHandler = () => {
    this.setState({ isApplyBtnClicked: true });
    if (this.state.startDate && !this.state.endDate) {
      return false;
    }
    this.props.claimsListActions.getClaimsListClear();
    this.setState({
      isPaginationCall: false,
      currentPageNumber: 1,
      token: '',
      isResetSearch: true,
    }, () => {
      this.advancePaginationRec();
      this.makingClaimsListAPI();
    });
    /* Trigger the Event for GTM track */
    this.triggerGTMEvent();
    return true;
  }

  /* Show By Option Selection */
  showperPage = (selectedshowperPage) => {
    this.props.claimsListActions.getClaimsListClear();
    this.setState({
      isShowByOptSelected: Number(selectedshowperPage),
      currentPageNumber: 1,
      isPaginationCall: false,
      token: '',
      isResetSearch: true,
    }, () => {
      this.makingClaimsListAPI();
      this.advancePaginationRec();
    });
  }

  /* Sorting Table header coloumn in ascending or descending order */
  sortingDataOrder = (key, type) => {
    this.setState({
      orderByColumn: key.charAt(0).toUpperCase() + key.substr(1),
      sortOrder: type ? 'ASC' : 'DESC',
      isPaginationCall: false,
      isResetSearch: true,
      currentPageNumber: 1,
    }, () => {
      this.props.claimsListActions.getClaimsListClear();
      this.makingClaimsListAPI();
      this.advancePaginationRec();
    });
  }

  /* Format the user name to show the last name followed by first name */
  formatUsers = () => {
    const { users } = this.props;
    if (!users || users.length === 0) {
      return matchCriteria.ShowAll;
    }
    const formattedusers = users.map((user) => ({
      ...user,
      label: `${user.firstName} ${user.lastName}`,
      value: user.emailAddress,
    }));
    return matchCriteria.ShowAll.concat(formattedusers);
  }

  /* Show/Hide Invoice Details Popup */
  isToggleInvoicePopup = (e, bool, claimID, id) => {
    e.preventDefault();
    this.setState({ isShowInvoicePopup: bool }, () => {
      if (bool) {
        this.claimID = id;
        this.props.invoiceListActions.getClaimsInvoiceListClear();
        this.props.invoiceListActions.getClaimsInvoiceList(this.claimID);
      }
    });
  }

  /* Sorting Invoice Details Data */
  sortingInvoiceDetailsData = (key, isAsc) => {
    const { invoiceDetailsList } = this.state;
    this.setState({ invoiceDetailsList: invoiceDetailsList.sort(isAsc ? ascending(key) : descending(key)) });
  }

  /* Format the Claims List Table data */
  formatData = (records) => {
    if (!records) return [];
    return records.map((record) => (
      {
        ...record,
        fileName: record.fileName ? <a href="/" className="text-underline" onClick={(event) => this.downloadHandler(event, record)}>{record.fileName.substring(record.fileName.lastIndexOf('\\') + 1)}</a> : '',
        submittedOn: record.submittedOn ? formatDateUTC(record.submittedOn) : '',
      }
    ));
  };

  /* Triggering the action to download the file */
  downloadHandler = (event, fileDetails) => {
    event.preventDefault();
    this.downloadFileDetails = { ...fileDetails, fileName: fileDetails.fileName.substring(fileDetails.fileName.lastIndexOf('/') + 1) };
    this.props.invoiceListActions.downloadInvoice({ ...fileDetails, Id: this.claimID });
  }

  /* Opening Invoice Details Popup */
  openInvoiceDetails = (e, claimID, id) => {
    this.isToggleInvoicePopup(e, true, claimID, id);
    /* Trigger the Event for GTM track */
    this.props.gtmActions.gtmClaimStatusViewInvoice(claimID);
  }

  /* Making API call to download Export CSV file */
  handleCSVDownload = () => {
    this.setState({
      isExportCsv: true,
      isPaginationCall: false,
      downloadCSV: false,
    }, () => {
      const request = this.creatingClaimListRequest().claimList;
      this.props.claimsListActions.getClaimsList(request, this.state.isExportCsv);
      this.isExportCSVCliked = true;
      this.setState({ isExportCsv: false });
    });
  }

  /** @description callback function for clear error
   * @param {object} e - triggered event(click)
   * @param {func} action - action for clear error
   */
  clearError = (e, action) => {
    e.preventDefault();
    action();
  }

  /* Trigger the Event for GTM track */
  triggerGTMEvent = () => {
    const {
      searchProductInfo,
      isUserNameOptSelected,
      isMatchOptSelected,
      isStatusOptSelected,
      startDate,
      endDate,
    } = this.state;
    this.props.gtmActions.gtmClaimStatusApplyBtnClicked(GTMEventMessage.apply);
    if (searchProductInfo) this.props.gtmActions.gtmClaimStatusSearchProduct(searchProductInfo);
    if (isUserNameOptSelected) this.props.gtmActions.gtmClaimStatusUserNameSelection(isUserNameOptSelected);
    if (isMatchOptSelected) this.props.gtmActions.gtmClaimStatusMatchItemSelection(isMatchOptSelected);
    if (isStatusOptSelected) this.props.gtmActions.gtmClaimStatusStatusItemSelection(isStatusOptSelected);
    if (startDate) this.props.gtmActions.gtmClaimStatusStartDateSelection(startDate);
    if (endDate) this.props.gtmActions.gtmClaimStatusEndDateSelection(endDate);
  }

  /* Trigger the Event when View Button Clicked in Claims List Table */
  viewClaimDetails = (claimID) => {
    /* Trigger the event for track */
    this.props.gtmActions.gtmClaimStatusViewClaimDetails(claimID);
  }

  /* Rendering ViewBy Component & SearchBy Component */
  renderViewBySearchBy = () => {
    const isShowAdvanceIcon = classNames({ 'open-advance-search': this.state.isOpenAdvanceSearch });
    return (
      <div className="gaf-search" id="cs-search">
        <Row>
          <Col md={4} xs={10} test={this.state.test}>
            <ViewBy id="claim-viewBy" options={searchViewBy.viewBy} onChange={this.viewByOptionSelection} />
          </Col>
          <Col md={3} mdPush={5} xs={2}>
            <div className="ad-search-filter-icon" id="cs-searchicon-wrapper">
              <a href="/" className="icon-filter visible-sm visible-xs" id="cs-search-icon" onClick={(e) => { e.preventDefault(); this.advanceSearchHandler(!this.state.isOpenAdvanceSearch); }}></a>
            </div>
            <Button id="cs-search-button" className={`btn-block ad-search-btn visible-md visible-lg ${isShowAdvanceIcon}`} onClick={(e) => { e.preventDefault(); this.advanceSearchHandler(!this.state.isOpenAdvanceSearch); }}>
              {labelName.customSearch} <i className="icon-arrow-up" id="cs-arrow-icon"></i>
            </Button>
          </Col>
          <Col md={4} mdOffset={1} mdPull={3} xs={12}>
            <SearchBy id={searchByType.ClaimStatusNumber} placeholder={this.state.searchByPlaceholder} isSearchByIcon={this.state.isSearchByIcon} typeahead="false" charLength={this.state.searchByChar} handleChange={this.searchByText} handleSubmit={this.searchClaimNoSubmitted} validationErrTxt={labelName.validClaimNo} isResetSearch={this.state.isResetSearch} />
          </Col>
        </Row>
      </div>
    );
  }

  /* Rendring MatchBy Component, Product Component & User Name Component */
  renderMatchProductUserName = () => {
    const { CLAIM_PRODUCT_DETAILS } = this.props;
    const { users } = this.props;
    const isDisableMatch = true;
    const formattedUsers = this.formatUsers();
    return (
      <Col md={12}>
        <div className="filterby" id="cs-advsearch-filterby">
          <Form inline id="cs-ads-inline">
            <FormGroup className="gaf-form match-criteria" id="cs-ads-matchcriteria">
              <span className="hidden-xs hidden-sm">{labelName.match}</span>
              <span className="visible-xs visible-sm">{labelName.match} <span className="input-short-text">({labelName.criteria})</span></span>
              <Dropdown className="display-inline" id="claimStatusMatch" menuItems={matchCriteria.Match} onChangeHandler={this.matchHandler} value={this.state.isMatchOptSelected} readOnly={isDisableMatch} />
              <span className="input-short-text hidden-xs hidden-sm">{labelName.criteria}</span>
            </FormGroup>
            <FormGroup className="gaf-form product-input" id="cs-ads-product">
              <label>{labelName.product}</label>
              <SearchBy id={searchByType.ClaimProductDetails} placeholder={this.state.productPlaceholder} typeahead="true" isSearchByIcon={this.state.isProductIcon} searchByKey={this.state.productKey} isSuggestionsLoading={CLAIM_PRODUCT_DETAILS.isSuggestionsLoading} suggestions={CLAIM_PRODUCT_DETAILS.suggestions} searchError={CLAIM_PRODUCT_DETAILS.searchError} optionSelected={this.productOptionSelected} charLength={this.state.productInfoChar} handleChange={this.productInfoText} validationErrTxt={labelName.validProductName} isResetSearch={this.state.isResetProduct} label={labelName.productLabelName} />
            </FormGroup>
            <FormGroup className="gaf-form select-user" id="cs-ads-selectuser">
              <label>{labelName.userName}</label>
              <Dropdown
                className="display-inline"
                id="claimStatusUserName"
                menuItems={formattedUsers}
                onChangeHandler={this.userNameHandler}
                value={this.state.isUserNameOptSelected}
                readOnly={!(users && users.length > 0)}
              />
            </FormGroup>
            {/* visible in mobile view */}
            <FormGroup className="gaf-form select-claims visible-xs visible-sm" id="cs-ads-selectclaims">
              <label>{labelName.status}</label>
              <Dropdown className="display-inline" id="claimStatusStatus" menuItems={matchCriteria.Status} onChangeHandler={this.statusHandler} value={this.state.isStatusOptSelected} />
            </FormGroup>
          </Form>
        </div>
      </Col>
    );
  }

  /* Rendering Date Picker Component, Status Component, Clear & Apply Buttons */
  renderDatePickerStatus = () => {
    const { startDate, endDate } = this.state;
    return (
      <Col md={12}>
        <div className="filterby calender-section" id="cs-ads-calendersection">
          <Form inline>
            <FormGroup className="gaf-form calender-input" id="cs-calender-start">
              <DatePickerControl
                id="fromDate"
                min={moment().subtract(24, 'months')}
                max={moment()}
                onChange={(date) => this.startDateChange(date)}
                value={this.state.startDate}
                placeholder={labelName.fromDate}
              />
            </FormGroup>
            <FormGroup className="gaf-form calender-input" id="cs-calender-end">
              <DatePickerControl
                id="toDate"
                min={moment(new Date(this.state.startDate))}
                max={moment()}
                onChange={(date) => this.endDateChange(date)}
                value={this.state.endDate}
                placeholder={labelName.toDate}
                disabled={!this.state.startDate}
              />
              {this.state.isApplyBtnClicked && startDate && !endDate && <span className="text-danger input-error-msg">{labelName.toDateValidation}</span>}
            </FormGroup>
            {/* visible in Desktop view */}
            <FormGroup className="gaf-form select-claims hidden-xs hidden-sm" id="cs-ads-selectclaims1">
              <label>{labelName.status}</label>
              <Dropdown className="display-inline" id="claimStatusStatus" menuItems={matchCriteria.Status} onChangeHandler={this.statusHandler} value={this.state.isStatusOptSelected} />
            </FormGroup>
            <div className="btn-actions" id="cs-ads-buttons">
              <Button className="btn gaf-btn-secondary" onClick={this.clearHandler}>{labelName.clear}</Button>
              <Button className="btn gaf-btn-primary" onClick={this.applyHandler}>{labelName.apply}</Button>
            </div>
          </Form>
        </div>
      </Col>
    );
  };

  /* Rendering Claims List Table */
  renderClaimsListTable = () => {
    const {
      listOfClaims,
      isLoading,
      paging,
      listOfClaimsCSV,
    } = this.props;
    const { downloadCSV, isShowByOptSelected } = this.state;
    return (
      <div className="claim-list" id="cs-claimlist">
        <ClaimsList
          data={listOfClaims[this.state.currentPageNumber]}
          sortingData={this.sortingDataOrder}
          invoiceDetails={(e, claimID, id) => this.openInvoiceDetails(e, claimID, id)}
          loading={isLoading}
          isAsyncExportCSV="true"
          CSVHandler={this.handleCSVDownload}
          exportCSVData={listOfClaimsCSV}
          onRef={(ref) => { this.child = ref; }}
          defaultSort={this.state.orderByColumn}
          viewClaimDetails={this.viewClaimDetails}
          triggerDownload={downloadCSV}
        />
        <div className="gaf-pagination" id="cs-pagination">
          <Row>
            <Col sm={6} md={6} xsHidden>
              <Form inline>
                {paging.totalRecords > searchShowBy.showBy[0].value && <ShowBy id="claim-showBy" onChange={this.showperPage} defaultValue={isShowByOptSelected} />}
              </Form>
            </Col>
            <Col sm={6} md={6} xs={12}>
              {(listOfClaims && Object.keys(listOfClaims).length > 0) && (paging.totalRecords > this.state.isShowByOptSelected) && <Pagination totalRows={paging.totalRecords} perPage={this.state.isShowByOptSelected} pageClick={this.getPaginationNumber} />}
            </Col>
          </Row>
        </div>
      </div>
    );
  }

  /* Rendering Invoice Details Popup */
  renderInvoiceDetails = () => {
    const { isInvoiceListLoading } = this.props;
    const { invoiceDetailsList } = this.state;
    return invoiceDetailsList && invoiceDetailsList.length > 0 && (
      <div>
        <Modal className="gaf-model-popup" show={this.state.isShowInvoicePopup} onHide={(e) => this.isToggleInvoicePopup(e, false)}>
          <Modal.Header>
            <a href="/" onClick={(e) => this.isToggleInvoicePopup(e, false)}>
              <span className="icon-close" />
            </a>
            <Modal.Title>Invoice Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="gaf-table invoice-details-popup">
              <Tables config={claimInvoiceDetails.claimInvoiceListTableHeder} data={this.formatData(invoiceDetailsList)} isLoading={isInvoiceListLoading} onSortingClick={(key, isAsc) => this.sortingInvoiceDetailsData(key, isAsc)} />
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  };

  /** @description function to render the error message for Claim Status */
  renderErrorMessage = () => {
    const { claimsListError, viewInvoiceError, downloadInvoiceError } = this.props;
    const props = {
      error: claimsListError || viewInvoiceError || downloadInvoiceError,
      onClear: (e) => this.clearError(e, (claimsListError) ? this.props.claimsListActions.claimsListClearError : this.props.invoiceListActions.claimsInvoiceClearError),
    };
    return (<ErrorNotification {...props} />);
  }

  render() {
    const {
      // CLAIM_STATUS_NUMBER,
      isLoading,
      isInvoiceListLoading,
      isInvoiceDownloading,
      accessInfo,
      paging,
    } = this.props;
    const { isPaginationCall, isAdvPaginationLoading, currentPageNumber } = this.state;
    return (
      <main className="main-content claim-status" id="claim-status">
        <Helmet>
          <title>{pageTitle.CLAIMSTATUS}</title>
        </Helmet>
        <section id="cs-section">
          <Grid>
            <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={this.props.actions.accessPage} />
            <div className="gaf-title-block">
              <h2>{labelName.title}</h2>
              <p>{labelName.subTitle}</p>
            </div>
            <div className="gaf-page-info" id="cs-page-info">
              <div className="gaf-search-area">
                {this.renderViewBySearchBy()}
                <div className="gaf-advance-search" id="cs-advance-search">
                  <Panel id="collapsible-panel-example-1" className="gaf-ad-search-panel" expanded={this.state.isOpenAdvanceSearch} onToggle={() => { }}>
                    <Panel.Collapse>
                      <Panel.Body>
                        <Row>
                          {this.renderMatchProductUserName()}
                          {this.renderDatePickerStatus()}
                        </Row>
                      </Panel.Body>
                    </Panel.Collapse>
                  </Panel>
                </div>
              </div>
              {this.renderClaimsListTable()}
            </div>
          </Grid>
        </section>
        {this.state.isShowInvoicePopup && this.renderInvoiceDetails()}
        {((isLoading && !isPaginationCall) || isInvoiceListLoading || isInvoiceDownloading || (isAdvPaginationLoading && (paging.pageNo < currentPageNumber))) ? <Loader /> : null}
        {this.renderErrorMessage()}
      </main>
    );
  }
}

function mapStateToProps(state) {
  const {
    searchByState,
    claimsListState,
    contractorUserState,
    claimInvoiceState,
    myProfileState,
  } = state;
  const { CLAIM_STATUS_NUMBER = {}, CLAIM_PRODUCT_DETAILS = {} } = searchByState;
  const {
    listOfClaims = {},
    listOfClaimsCSV = [],
    isLoading,
    filter,
    claimsListError,
    paging = { token: '', totalRecords: 0 },
  } = claimsListState;
  const { users } = contractorUserState;
  const {
    isInvoiceListLoading,
    viewInvoiceError,
    claimInvoiceData = [],
    isInvoiceDownloading,
    downloadInvoiceError,
    invoiceFileDetails,
    isInvoiceDownloadSuccess,
  } = claimInvoiceState;
  const { accessInfo } = myProfileState;
  return {
    CLAIM_STATUS_NUMBER,
    CLAIM_PRODUCT_DETAILS,
    listOfClaims,
    listOfClaimsCSV,
    users,
    isLoading,
    filter,
    paging,
    claimsListError,
    isInvoiceListLoading,
    viewInvoiceError,
    claimInvoiceData,
    isInvoiceDownloading,
    invoiceFileDetails,
    downloadInvoiceError,
    isInvoiceDownloadSuccess,
    accessInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    contractorUserActions: bindActionCreators(contractorUserActionsCreators, dispatch),
    typeAHeadActions: bindActionCreators(typeAHeadActionCreators, dispatch),
    claimsListActions: bindActionCreators(claimsListActionCreators, dispatch),
    invoiceListActions: bindActionCreators(invoiceListActionCreators, dispatch),
    actions: bindActionCreators(getProfileDetails, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

ClaimStatus.propTypes = {
  // CLAIM_STATUS_NUMBER: PropTypes.object,
  CLAIM_PRODUCT_DETAILS: PropTypes.object,
  typeAHeadActions: PropTypes.object,
  claimsListActions: PropTypes.object,
  invoiceListActions: PropTypes.object,
  listOfClaims: PropTypes.object,
  users: PropTypes.array,
  isLoading: PropTypes.bool,
  isInvoiceListLoading: PropTypes.bool,
  claimInvoiceData: PropTypes.array,
  paging: PropTypes.object,
  isInvoiceDownloadSuccess: PropTypes.bool,
  listOfClaimsCSV: PropTypes.array,
  invoiceFileDetails: PropTypes.array,
  isInvoiceDownloading: PropTypes.bool,
  contractorUserActions: PropTypes.object.isRequired,
  claimsListError: PropTypes.string,
  viewInvoiceError: PropTypes.string,
  downloadInvoiceError: PropTypes.string,
  accessInfo: PropTypes.bool,
  actions: PropTypes.object,
  gtmActions: PropTypes.object.isRequired,
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClaimStatus));
