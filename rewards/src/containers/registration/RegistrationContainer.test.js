import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import RegistrationCont, { RegistrationContainer } from '../../containers/registration';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('RegistrationContainer', () => {
  const selectServices = jest.fn();
  const selectAddress = jest.fn();
  const changeAddress = jest.fn();
  const registrationOption = jest.fn();
  const registerNow = jest.fn();
  const updateRegistrationOption = jest.fn();
  const onSubmit = jest.fn();
  const props = {
    statesActions: {
      getStates: jest.fn(),
    },
    gtmActions: {
      gtmBusinessResidentialPercent: jest.fn(),
      gtmBusinessCommercialPercent: jest.fn(),
      gtmAnnualRoofInstall: jest.fn(),
      gtmMaterialBuyingOutlet: jest.fn(),
      gtmPrimaryBusinessType: jest.fn(),
      gtmServicesOffered: jest.fn(),
      gtmGafProductsBought: jest.fn(),
    },
    actions: {
      selectServices,
      selectAddress,
      changeAddress,
      registrationOption,
      registerNow,
      updateRegistrationOption,
    },
    states: [{ name: 'New Jersey', code: 'NJ' }],
    handleSubmit: jest.fn(),
    anyTouched: true,
    done: true,
    syncErrors: {
      firstName: 'Please enter valid firstname',
    },
    values: {
      hasHeadQuatersAddress: true,
      hasMailingAddress: true,
    },
    businessType: [{ title: 'sample', value: 'sample' }],
    materialsBuyingOutlet: [{ title: 'sample', value: 'sample' }],
    services: [{ title: 'sample', value: 'sample' }],
    gafProductsBought: [{ title: 'sample', value: 'sample' }],
  };
  const tree = shallow(<RegistrationContainer
    {...props}
  />);
  tree.instance().recaptchaInstance = {
    reset: jest.fn(),
  };
  it('should be defined', () => {
    expect(RegistrationContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Registration Form', () => {
    expect(tree.find('form[name="registrationForm"]').length).toBe(1);
  });

  it('should render Name', () => {
    expect(tree.find('Name').length).toBe(1);
  });

  it('should render Email', () => {
    expect(tree.find('Email').length).toBe(1);
  });

  it('should render Address', () => {
    expect(tree.find('FieldArray[name="address"]').length).toBe(1);
  });

  it('should call addressSelectHandler', () => {
    const addressSelectHandler = tree.find('FieldArray[name="address"]').prop('onAddressSelect');
    addressSelectHandler({ address_components: [] });
    expect(selectAddress).toBeCalled();
  });


  it('should call addressChangeHandler', () => {
    const addressChangeHandler = tree.find('FieldArray[name="address"]').prop('onAddressChange');
    addressChangeHandler();
    expect(changeAddress).toBeCalled();
  });

  it('should render RangeSlider', () => {
    expect(tree.find('RangeSlider').length).toBe(2);
  });

  it('should call handleRangeSlide for below 50', () => {
    const handleRangeSlide = tree.find('RangeSlider').at(0).prop('onChange');
    handleRangeSlide(10);
    expect(tree.state().lowSlope).toBe(10);
    expect(tree.state().steepSlope).toBe(90);
  });

  it('should call handleRangeSlide for above 50', () => {
    const handleRangeSlide = tree.find('RangeSlider').at(0).prop('onChange');
    handleRangeSlide(90);
    expect(tree.state().steepSlope).toBe(10);
    expect(tree.state().lowSlope).toBe(90);
  });

  it('should call handleRangeSlide', () => {
    const handleRangeSlide = tree.find('RangeSlider').at(1).prop('onChange');
    handleRangeSlide(10);
    expect(tree.state().annualInstallations).toBe(10);
  });

  it('should call handleOptionChange with checked true', () => {
    const handleOptionChange = tree.find('Field[name="Gutters"]').at(0).prop('onChange');
    handleOptionChange({ target: { checked: true, name: 'servicesOffered' } });
    expect(selectServices).toBeCalled();
  });

  it('should call handleOptionChange with checked false', () => {
    const handleOptionChange = tree.find('Field[name="Gutters"]').at(0).prop('onChange');
    handleOptionChange({ target: { checked: false, name: 'servicesOffered' } });
    expect(selectServices).toBeCalled();
  });

  it('should call handleOptionChange with none checked false', () => {
    const handleOptionChange = tree.find('Field[name="Gutters"]').at(0).prop('onChange');
    handleOptionChange({ target: { checked: false, name: 'None' } }, 'serviceOffered');
    expect(updateRegistrationOption).not.toBeCalled();
  });

  it('should call handleOptionChange with none checked true', () => {
    const handleOptionChange = tree.find('Field[name="Gutters"]').at(0).prop('onChange');
    handleOptionChange({ target: { checked: true, name: 'None' } });
    expect(updateRegistrationOption).toBeCalled();
  });

  it('should render Registration options', () => {
    expect(tree.find('.primary-purchase-point').length).not.toBe(0);
  });

  it('should render Next button', () => {
    expect(tree.find('.register-now').length).toBe(1);
  });
});

describe('RegistrationContainer with connected component', () => {
  const selectServices = jest.fn();
  const selectAddress = jest.fn();
  const changeAddress = jest.fn();
  const registrationOption = jest.fn();
  const updateRegistrationOption = jest.fn();
  const state = {
    states: {
      states: null,
    },
    form: {
      registrationForm: {
        values: {
          hasHeadQuatersAddress: false,
          hasMailingAddress: false,
          Gutter: true,
          Coatings: true,
          isCompany: true,
        },
        actions: {
          selectServices,
          selectAddress,
          changeAddress,
          registrationOption,
          updateRegistrationOption,
        },
        handleSubmit: jest.fn(),
        syncErrors: {
          firstName: 'Please enter valid firstname',
        },
      },
    },
  };
  const store = fakeStore(state);
  const submitForm = jest.fn();
  const onSubmit = jest.fn();
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <RegistrationCont
          submitFailed={true}
          onSubmit={onSubmit}
          submitForm={submitForm}
          statesActions={{
            getStates: jest.fn(),
          }}
          gtmActions={{
            gtmBusinessResidentialPercent: jest.fn(),
            gtmBusinessCommercialPercent: jest.fn(),
            gtmAnnualRoofInstall: jest.fn(),
            gtmMaterialBuyingOutlet: jest.fn(),
            gtmPrimaryBusinessType: jest.fn(),
            gtmServicesOffered: jest.fn(),
            gtmGafProductsBought: jest.fn(),
          }}
        />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(RegistrationCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Name', () => {
    expect(tree.find('Name').length).toBe(1);
  });

  it('should render Email', () => {
    expect(tree.find('Email').length).toBe(1);
  });

  it('should call submitHandler', () => {
    const routeEle = tree.instance().props.children;
    const regContainer = routeEle.props.children;
    const submitHandler = tree.find('form[name="registrationForm"]').prop('onSubmit');
    submitHandler();
    expect(regContainer.props.submitForm).toBeDefined();
  });
});
