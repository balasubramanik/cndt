import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button, Col, Row } from 'react-bootstrap';
import { reduxForm, FieldArray, Field } from 'redux-form';
import * as registrationActionCreators from '../../actionCreators/Registration';
import * as statesActionsCreators from '../../actionCreators/States';
import Checkbox from '../../components/checkbox';
import Radio from '../../components/radio';
import ToolTip from '../../components/toolTip';
import RangeSlider from '../../components/rangeSlider';
import Name from '../../components/name';
import Email from '../../components/email';
import Address from '../../components/address';
import Company from '../../components/company';
import Toast from '../../components/toast';
import { registration, registrationOptions } from './Constants';
import { constants, errorMessages } from '../ems/Constants';
import validate from './validate';
import { formatAddress, allowOnlyNumber } from '../../utils/utils';
import * as gtmActionsCreators from '../../actionCreators/GTM';

const isShowError = true;

const INITIAL_RANGE = 50;
export class RegistrationContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.values = null;
    this.state = {
      steepSlope: INITIAL_RANGE,
      lowSlope: INITIAL_RANGE,
      annualInstallations: 0,
    };
  }

  /** @description React Life cycle method
   * it will invoke, when component is mounted.
   */
  componentDidMount() {
    const { statesActions } = this.props;
    statesActions.getStates();
  }

  /** @description Callback function for address change
   * @param {string} value - location value
   * @param {string} name - name of the field
   */
  addressChangeHandler = (value, name) => {
    this.props.actions.changeAddress(name, value);
  }

  /** @description Callback function for address select
   * @param {string} value - location value
   * @param {string} name - name of the field
   */
  addressSelectHandler = (value, name) => {
    const address = formatAddress(value.address_components);
    this.props.actions.selectAddress(name, address);
  }

  /** @description function to format states for address
   */
  formatStates = () => {
    const { states } = this.props;
    if (states) {
      return states.map((item) => ({
        label: item.name,
        value: item.code,
      }));
    }
    return [];
  }

  /** @description function to format data for trigger service call
   * @param {object} values - form values
   */
  formatFormData = (values) => {
    const services = [];
    const products = [];
    const { servicesOffered, gafProductsBought } = registrationOptions;
    const { steepSlope, lowSlope, annualInstallations } = this.state;
    const {
      firstName, lastName, email, company, dba,
      address1br, address2br, citybr, zipbr, workPhonebr, statebr, countryCode,
      address1hq, address2hq, cityhq, ziphq, workPhonehq, statehq,
      address1ml, address2ml, cityml, zipml, workPhoneml, stateml,
      businessType, materialsBuyingOutlet, date,
      hasHeadQuatersAddress, hasMailingAddress,
    } = values;
    let eSignature = `${firstName} ${lastName}`;
    if (eSignature.length >= 40) {
      eSignature = `${firstName}${lastName}`;
    }
    servicesOffered.options.forEach((item) => {
      if (values[item.value]) services.push(item.value);
    });
    gafProductsBought.options.forEach((item) => {
      if (values[item.value]) products.push(item.value);
    });
    const branchAddress = {
      address1: address1br,
      address2: address2br,
      city: citybr,
      state: statebr,
      postal: zipbr,
      country: countryCode,
      phoneNumber: allowOnlyNumber(workPhonebr),
      addressType: 0,
    };
    const addresses = [branchAddress];
    if (!hasMailingAddress) {
      const mailingAddress = {
        address1: address1ml,
        address2: address2ml,
        city: cityml,
        state: stateml,
        postal: zipml,
        country: countryCode,
        ...workPhoneml ? { phoneNumber: allowOnlyNumber(workPhoneml) } : {},
        addressType: 1,
      };
      addresses.push(mailingAddress);
    }
    if (!hasHeadQuatersAddress) {
      const headQuatersAddress = {
        address1: address1hq,
        address2: address2hq,
        city: cityhq,
        state: statehq,
        postal: ziphq,
        country: countryCode,
        phoneNumber: allowOnlyNumber(workPhonehq),
        addressType: 2,
      };
      addresses.push(headQuatersAddress);
    }
    return {
      contact: {
        firstName,
        lastName,
        emailAddress: email,
      },
      company: {
        legalName: company,
        dbaName: dba,
        addresses,
        isSameAsHeadQuarters: hasHeadQuatersAddress,
        isSameAsMailingAddress: hasMailingAddress,
      },
      residentialPercentage: String(steepSlope),
      commercialPercentage: String(lowSlope),
      annualInstallations: String(annualInstallations),
      businessType,
      servicesOffered: services,
      materialsBuyingOutlet,
      gafProductsBought: products,
      isTnCAccepted: true,
      signedBy: eSignature,
      signedDate: date,
      userId: email,
    };
  }

  /** @description Callback function for submit form
   * @param {object} values
   */
  submitHandler = (values) => {
    this.values = this.formatFormData(values);
    this.props.onSubmit(this.values, this.props.actions.registerNow);
  }

  /** @description Callback function for change the Other services, Gaf Products
   * @param {object} e - triggered event(change)
   * @param {array} key - state key to update
   * @param {array} data - registrataion option
   */
  handleRegistrationOptionChange = (e, key) => {
    const { target } = e;
    const { value } = target;
    if (key === 'materialsBuyingOutlet') {
      this.props.gtmActions.gtmMaterialBuyingOutlet(value);
    }
    if (key === 'businessType') {
      this.props.gtmActions.gtmPrimaryBusinessType(value);
    }
  }

  /** @description callback function for change the range slider
   * @param {string} key - key to update the state
   * @param {string} value - value of selected range slider
   */
  handleRangeSlide = (key, value) => {
    if (key === 'residentialPercentage') {
      let steepSlope = 0;
      let lowSlope = 0;
      if (value <= 50) {
        steepSlope = 100 - value;
        lowSlope = 100 - steepSlope;
      } else {
        lowSlope = value;
        steepSlope = 100 - lowSlope;
      }
      this.setState({ steepSlope, lowSlope });
      this.props.gtmActions.gtmBusinessResidentialPercent(steepSlope);
      this.props.gtmActions.gtmBusinessCommercialPercent(lowSlope);
      return;
    }
    this.props.gtmActions.gtmAnnualRoofInstall(value);
    this.setState({ [key]: value });
  };

  /** @description Callback function for change the Other services, Gaf Products
   * @param {object} e - triggered event(change)
   * @param {array} key - state key to update
   * @param {array} data - registrataion option
   */
  handleOptionChange = (e, key, data) => {
    const { checked, name } = e.target;
    let fields = {};
    if (!this[key]) {
      this[key] = [];
    }
    const optionName = name ? name.toLowerCase() : '';
    if (optionName === registration.NONE) {
      this[key] = [];
      if (checked) {
        this.setState({ [`${key}Disabled`]: true });
        data.forEach((item) => {
          fields = {
            ...fields,
            ...(item.value.toLowerCase() !== registration.NONE) ? { [item.value]: false } : {},
          };
        });
        this[key].push(name);
        this.props.actions.updateRegistrationOption(fields);
      } else {
        this.setState({ [`${key}Disabled`]: false });
      }
    } else if (checked) {
      this[key].push(name);
    } else {
      this[key].splice(this[key].indexOf(name), 1);
    }
    const value = this[key].join(',');
    if (key === 'servicesOffered') {
      this.props.gtmActions.gtmServicesOffered(value);
    }
    if (key === 'gafProductsBought') {
      this.props.gtmActions.gtmGafProductsBought(value);
    }
    this.props.actions.selectServices(`has${key}`, this[key].length > 0);
  }

  /** @description function to generate labels for business labels */
  generateBusinessLabels = () => {
    const { steepSlope, lowSlope } = this.state;
    return {
      0: <div><span className="center-line-bold">|</span><span className="number-range">{steepSlope}%</span></div>,
      10: <span className="adjust">|</span>,
      20: <span className="adjust">|</span>,
      30: <span className="adjust">|</span>,
      40: <span className="adjust">|</span>,
      50: <span className="center-line-bold">|</span>,
      60: <span className="adjust">|</span>,
      70: <span className="adjust">|</span>,
      80: <span className="adjust">|</span>,
      90: <span className="adjust">|</span>,
      100: <div><span className="center-line-bold">|</span><span className="number-range">{lowSlope}%</span></div>,
    };
  };

  /** @description function to generate labels for annual roof installed */
  generateAnnualRoofLabels = () => ({
    10: <div><span className="center-line-bold">|</span><span className="number-range ten">&lt;10</span></div>,
    50: <div><span className="adjust">|</span><span className="number-range fifty">50</span></div>,
    100: <div><span className="center-line-bold">|</span><span className="number-range hundred">100</span></div>,
    150: <div><span className="adjust">|</span><span className="number-range one-fifty">150</span></div>,
    200: <div><span className="center-line-bold">|</span><span className="number-range two-hundred">200+</span></div>,
  });

  /** @description Function to render the Checkbox group
   * @param {array} data - contains the array of options to render
   * @param {array} key - state key to update
   */
  renderCheckBoxGroup = (data, key) => {
    const { values, submitFailed } = this.props;
    return (
      <div>
        {submitFailed && !values[`has${key}`] && <span className="error-message">{errorMessages.REQUIRED_QUESTIONS}</span>}
        <Row>
          {data && data.map((item, i) => {
            const isNoneSelected = this.state[`${key}Disabled`] && item.value && item.value.toLowerCase() !== registration.NONE;
            const disabledClass = classNames({ disabled: isNoneSelected });
            return (
              <Col md={4} key={i.toString()}>
                <Field
                  key={i.toString()}
                  checked={values[item.value]}
                  component={Checkbox}
                  name={item.value}
                  option={item}
                  className={disabledClass}
                  disabled={isNoneSelected}
                  onChange={(e) => this.handleOptionChange(e, key, data)}
                />
              </Col>
            );
          })
          }
        </Row>
      </div>
    );
  }

  /** @description function to render the Other services */
  renderCheckBoxes = (obj, key) => (
    <div className="other-services clearfix" key={key}>
      <Col md={12} className="gab-product error-question">
        <p>{obj.question}<sup>*</sup></p>
        {this.renderCheckBoxGroup(obj.options, key)}
      </Col>
    </div>
  );

  /** @description function to render the Radio buttons */
  renderRadioButtons = (obj, key) => (
    <div className="primary-purchase-point clearfix marBtm15" key={key}>
      <Col md={12}>
        <p>{obj.question}<sup>*</sup></p>
        <Row className="error-question">
          <Field
            className="gaf-radio col-md-4"
            component={Radio}
            showError={isShowError}
            name={key}
            options={obj.options}
            onChange={(e) => this.handleRegistrationOptionChange(e, key)}
          />
        </Row>
      </Col>
    </div>
  );

  /** @description function to render the Registration options */
  renderRegistrationOptions = () => {
    const { values } = this.props;
    const { isCompany } = values;
    if (registrationOptions && Object.keys(registrationOptions).length > 0) {
      return Object.keys(registrationOptions).map((key) => {
        if (registrationOptions[key].selection === 'single') {
          if (key === 'businessType' && !isCompany) return null;
          return this.renderRadioButtons(registrationOptions[key], key);
        }
        return this.renderCheckBoxes(registrationOptions[key], key);
      });
    }
    return null;
  }

  /** @description function to render the submit button */
  renderSubmitButton = () => (
    <Col md={12}>
      <div className="register-or-go-back">
        <div className="register-now">
          <Button
            type="submit"
            className="btn-block gaf-btn-primary"
          >
            {registration.REGISTERNOW}
          </Button>
        </div>
      </div>
    </Col>
  );

  /** @description function to render the Range slider for business percentage  */
  renderBusinessPercentRangeSlider = () => (
    <Col md={12} className="marTop25">
      <p className="ranger-text">
        {registration.BUSINESS_PERCENTAGE_DESCRIPTION}
        <ToolTip id="business-percentage" text={registration.BUSINESS_PERCENTAGE_TOOLTIP}>
          <span className="icon-question" role="button" />
        </ToolTip>
      </p>
      <Col md={9} className="padNone">
        <div className="residential-commercial">
          <p>{registration.STEEP_SLOPE}
            <span className="ranger-commercial">
              {registration.LOW_SLOPE}
            </span>
          </p>
        </div>
      </Col>
      <RangeSlider
        labels={this.generateBusinessLabels()}
        onChange={(value) => this.handleRangeSlide('residentialPercentage', value)}
        value={INITIAL_RANGE}
      />
    </Col>
  );

  /** @description function to render the Range slider for annual roof installed  */
  renderRoofInstalledRangeSlider = () => {
    const isShowToolTip = true;
    return (
      <Col md={12}>
        <p className="install-annually">{registration.ROOF_INSTALLED_DESCRIPTION}
          <ToolTip id="install-annually" text={registration.BUSINESS_PERCENTAGE_TOOLTIP}>
            <span className="icon-question" role="button" />
          </ToolTip>
        </p>
        <RangeSlider
          min={10}
          max={200}
          labels={this.generateAnnualRoofLabels()}
          onChange={(value) => this.handleRangeSlide('annualInstallations', value)}
          tooltip={isShowToolTip}
        />
      </Col>
    );
  }

  renderTermsAndConditionCheckbox = (fields) => {
    const { submitFailed } = this.props;
    const { meta } = fields;
    const { error } = meta;
    return (
      <div>
        <Checkbox
          {...fields}
          option={{
            title: `${registration.ACCEPT_TERMS_AND_CONDITIONS}
                  <a
                    id="reg-tnc"
                    href="${constants.TERMS_AND_CONDITIONS_URL}"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                  ${registration.GAF_REWARDS}
                  </a><sup>*</sup>`,
          }}
        />
        {submitFailed && error && <p><span className="error-message">{errorMessages.TERMS_AND_CONDITIONS_REQUIRED}</span></p>}
      </div>
    );
  }

  /** @description function to render the terms and conditions  */
  renderTermsAndCondition = () => {
    const { values } = this.props;
    const { termsAndConditionsEnabled } = values;
    return (
      <div className="tc-error error-question">
        <Field
          checked={termsAndConditionsEnabled}
          component={this.renderTermsAndConditionCheckbox}
          name="termsAndConditionsEnabled"
        />
      </div>
    );
  };

  /** @description function to render the Esignature  */
  renderEsignature = () => (
    <Col md={12}>
      <Row className="terms-and-cond">
        <Col md={12}>
          {this.renderTermsAndCondition()}
        </Col>
      </Row>
    </Col>
  );

  render() {
    const isEditable = false;
    const {
      values, handleSubmit, hqLocationText, mlLocationText, brLocationText, actions,
    } = this.props;
    const { hasMailingAddress, hasHeadQuatersAddress, isCompany } = values;
    return (
      <form
        name="registrationForm"
        noValidate
        onSubmit={handleSubmit(this.submitHandler)}
      >
        <Toast icon="icon-check" text={registration.TOAST_MESSAGE} />
        <p className="ems-alert">{registration.INDICATES_REQUIRED_FIELD}</p>
        <Row className="reg-container">

          <Name />
          <Email editable={isEditable} isCompany={isCompany} />
          {isCompany && <Company />}
          <FieldArray
            name="address"
            locationText={{ brLocationText, hqLocationText, mlLocationText }}
            states={this.formatStates()}
            values={{ hasHeadQuatersAddress, hasMailingAddress }}
            component={Address}
            onAddressChange={this.addressChangeHandler}
            onAddressSelect={this.addressSelectHandler}
            onPhoneNumberChange={actions.selectServices}
          />
          {this.renderBusinessPercentRangeSlider()}
          {this.renderRoofInstalledRangeSlider()}
          {this.renderRegistrationOptions()}
          {this.renderEsignature()}
          {this.renderSubmitButton()}
        </Row>
      </form>
    );
  }
}

function mapStateToProps(state) {
  const { form, states } = state;
  const { registrationForm } = form;
  const {
    values, syncErrors, brLocationText, hqLocationText, mlLocationText, submitFailed,
  } = registrationForm;
  const { states: stateValues } = states;
  return {
    submitFailed,
    values,
    brLocationText,
    hqLocationText,
    mlLocationText,
    syncErrors,
    states: stateValues,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(registrationActionCreators, dispatch),
    statesActions: bindActionCreators(statesActionsCreators, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

/** PropTypes:
 * actions - object - contains the collection of actions
 * handleSubmit - func - function for submit the form
 * onSubmit - func - callback function for submit form
 * gtmActions - object - actions for google tag manager
 * submitFailed - boolean - represents whether form submit is success or not.
 * syncErrors - object - Contains the error messages for invalid form fields
 * values - object - Contains the values of valid form fields
 * brLocationText - string - branch address location text
 * hqLocationText - string - head quaters address location text
 * mlLocationText - string - mailing address location text
 * states - array - contains the state list
 */
RegistrationContainer.propTypes = {
  actions: PropTypes.object.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  statesActions: PropTypes.object.isRequired,
  gtmActions: PropTypes.object.isRequired,
  submitFailed: PropTypes.bool,
  syncErrors: PropTypes.object,
  values: PropTypes.object,
  brLocationText: PropTypes.string,
  hqLocationText: PropTypes.string,
  mlLocationText: PropTypes.string,
  states: PropTypes.array,
};

const Component = connect(mapStateToProps, mapDispatchToProps)(RegistrationContainer);

export default reduxForm({
  form: 'registrationForm',
  validate,
  touchOnBlur: false,
})(Component);
