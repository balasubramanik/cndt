const registration = {
  REGISTERNOW: 'REGISTER NOW',
  ERROR_MESSAGE: 'Please enter all mandatory fields',
  STEEP_SLOPE: 'Steep Slope',
  LOW_SLOPE: 'Low Slope',
  BUSINESS_PERCENTAGE_DESCRIPTION: 'Tell us what percentage of your business is steep slope versus low slope.',
  BUSINESS_PERCENTAGE_TOOLTIP: 'So your experience can be as personalized as possible.',
  ROOF_INSTALLED_DESCRIPTION: 'Approximate number of roofs installed annually.',
  PRIMARY_BUSINESS_TYPE: 'Primary Business type',
  OTHER_SERVICES: 'What other services do you offer?',
  GAF_PRODUCTS: 'What GAF Products are you buying (select all that apply)?',
  DISTRIBUTORS: 'Where do you primarily buy your roofing materials?',
  ACCEPT_TERMS_AND_CONDITIONS: 'I accept the Terms & Conditions for',
  GAF_REWARDS: 'GAF Rewards',
  ESIGNATURE: 'Electronic Signature',
  DATE: 'Date',
  TOAST_MESSAGE: 'Your email address has been verified',
  INDICATES_REQUIRED_FIELD: '* Indicates required field',
  NONE: 'none',
};

const registrationOptions = {
  businessType: {
    question: 'Primary Business Type',
    selection: 'single',
    options: [
      {
        title: 'Roofing',
        value: 'Roofing',
      },
      {
        title: 'General Contractor',
        value: 'General Contractor',
      },
      {
        title: 'Remodeler',
        value: 'Remodeler',
      },
      {
        title: 'National Builder',
        value: 'National Builder',
      },
      {
        title: 'Regional Builder',
        value: 'Regional Builder',
      },
    ],
  },
  servicesOffered: {
    question: 'What other services do you offer?',
    selection: 'multiple',
    options: [
      {
        title: 'Gutters',
        value: 'Gutters',
      },
      {
        title: 'Siding',
        value: 'Siding',
      },
      {
        title: 'Windows/Doors',
        value: 'Windows/Doors',
      },
      {
        title: 'Interior Remodel',
        value: 'Interior Remodel',
      },
      {
        title: 'Landscaping',
        value: 'Landscaping',
      },
      {
        title: 'Asphalt/Paving',
        value: 'Asphalt/Paving',
      },
      {
        title: 'Solar',
        value: 'Solar',
      },
      {
        title: 'None',
        value: 'None',
      },
    ],
  },
  materialsBuyingOutlet: {
    question: 'Where do you primarily buy your roofing materials?',
    selection: 'single',
    options: [
      {
        title: 'Distributor',
        value: 'Distributor',
      },
      {
        title: 'Hardware Store',
        value: 'Hardware Store',
      },
      {
        title: 'Lumberyard',
        value: 'Lumberyard',
      },
      {
        title: 'Big Box Store',
        value: 'Big Box Store',
      },
    ],
  },
  gafProductsBought: {
    question: 'What GAF products are you buying (select all that apply)?',
    selection: 'multiple',
    options: [
      {
        title: 'Architectural Shingles',
        value: 'Architectural Shingles',
      },
      {
        title: 'Designer Shingles',
        value: 'Designer Shingles',
      },
      {
        title: '3-Tab Shingles',
        value: '3-Tab Shingles',
      },
      {
        title: 'Coatings',
        value: 'Coatings',
      },
      {
        title: 'Roof Deck Protection',
        value: 'Roof Deck Protection',
      },
      {
        title: 'Leak Barriers',
        value: 'Leak Barriers',
      },
      {
        title: 'Ventilation and Attic Fans',
        value: 'Ventilation and Attic Fans',
      },
      {
        title: 'Hip & Ridge Cap Shingles',
        value: 'Hip & Ridge Cap Shingles',
      },
      {
        title: 'Starter Strip Shingles',
        value: 'Starter Strip Shingles',
      },
    ],
  },
};

export {
  registration,
  registrationOptions,
};
