import { validate as nameValidation } from '../../components/name/validate';
import { validate as emailValidation } from '../../components/email/validate';
import { validate as companyValidataion } from '../../components/company/validate';
import { validate as addressValidataion } from '../../components/address/validate';
import { registrationOptions } from './Constants';
import { errorMessages } from '../ems/Constants';

export const validateRegForm = (values) => {
  const errors = {};
  if (registrationOptions && Object.keys(registrationOptions).length > 0) {
    Object.keys(registrationOptions).forEach((key) => {
      if (registrationOptions[key].selection === 'single' && !values[key]) {
        if (!(key === 'businessType' && !values.isCompany)) {
          errors[key] = errorMessages.REQUIRED_QUESTIONS;
        }
      }
      if (registrationOptions[key].selection === 'multiple' && !values[`has${key}`]) {
        if (registrationOptions[key].options &&
          registrationOptions[key].options.length > 0) {
          const selectedOption = registrationOptions[key].options[0];
          errors[selectedOption.value] = errorMessages.REQUIRED_QUESTIONS;
        }
      }
    });
  }
  if (!values.termsAndConditionsEnabled) {
    errors.termsAndConditionsEnabled = errorMessages.REQUIRED_FIELD;
  }
  return errors;
};

export default function (values, props) {
  return {
    ...nameValidation(values),
    ...emailValidation(values, props),
    ...companyValidataion(values),
    ...addressValidataion(values),
    ...validateRegForm(values),
  };
}
