import { validateRegForm } from './validate';
import { errorMessages } from '../ems/Constants';

describe('validate', () => {
  it('should be equal to invalid termsAndConditionsEnabled', () => {
    const errors = validateRegForm({});
    expect(errors.termsAndConditionsEnabled).toBe(errorMessages.REQUIRED_FIELD);
  });

  it('should be equal to valid termsAndConditionsEnabled', () => {
    const errors = validateRegForm({ termsAndConditionsEnabled: true });
    expect(errors.termsAndConditionsEnabled).toBe(undefined);
  });
});
