import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Button, Form, FormGroup, Panel } from 'react-bootstrap';
import classNames from 'classnames';
import { Helmet } from 'react-helmet';
import { authentication } from '../../routes';
import { texts, showAll } from './constants';
import Dropdown from '../../components/dropdown';
import CSV from '../../components/csv';
import ContractorList from '../../components/contractorList';
import { getContractorList, achToggle, closeTogglePopup, submitDetailedReport, closeDetailedPopup, updatePageDetails, resetInitialState } from '../../actionCreators/ContractorList';
import ShowBy from '../../components/showBy';
import Pagination from '../../components/pagination';
import Dialog from '../../components/dialog';
import Loader from '../../components/loader';
import { getToggleData, getcontractorListByPageNo } from './selector';
import * as statesActionsCreators from '../../actionCreators/States';
import SearchBy from '../../components/searchBy';
import { searchByType, pageTitle } from '../../constants/constants';
import { searchShowBy } from '../../components/showBy/constants';
import { formatDateUTC } from '../../utils/utils';

class ContractorManagement extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      downloadCSV: false,
      isOpenAdvanceSearch: false,
      contractorNameOrId: '',
      currentPageNumber: 1,
      isShowByOptSelected: Number(searchShowBy.showBy[0].value),
      sortOrder: 'ASC',
      sortBy: 'ContractorName',
      showDetailedReport: false,
      onchangeDropDown: moment().year(),
      isContractorNameSubmitted: false,
      isResetSearch: false,
      isCityResetSearch: false,
      isDetailedReportError: false,
      advanceSearch: {
        matchAll: texts.matchAll,
        redeemCheck: null,
        planType: '',
        contractorType: '',
        state: '',
        city: '',
      },
      cities: {
        isSearchByIcon: false,
        searchByChar: {
          min: 0,
        },
        // disabled: true,
      },
      searchByContractorNameOrId: {
        isSearchByIcon: true,
        placeholder: texts.SearchByText,
        searchByChar: {
          min: 3,
        },
      },
    };
    this.isDetailedReportClicked = false;
  }

  componentDidMount() {
    this.makingContractorListAPI();
    const { statesActions } = this.props;
    statesActions.getStates();
  }

  componentDidUpdate(prevProps) {
    const { detailedReportData, isDetailedReportLoading } = this.props;
    if (this.isDetailedReportClicked && (prevProps.isDetailedReportLoading !== isDetailedReportLoading) && !isDetailedReportLoading) {
      if (detailedReportData && detailedReportData.length > 0) {
        window.setTimeout(() => {
          this.updateDownloadCSVState(true);
          this.isDetailedReportClicked = false;
          this.handleDetailedReportClose();
        }, 500);
      } else {
        this.detailedReportErrorUpdate();
      }
    }
  }
  /**  Page Reset Initial State */
  componentWillUnmount() {
    this.props.resetInitialState();
  }

  /** @description Detailed Report Click Handler */
  onDetailedReportClick = (e) => {
    e.preventDefault();
    this.setState({ showDetailedReport: true });
  }

  /** @description Submit Report Click Handler */
  onSubmitDetailedReport = (e) => {
    e.preventDefault();
    this.props.submitDetailedReport(this.state.onchangeDropDown);
  }

  getDetailedReportBody = () => {
    const { downloadCSV, isDetailedReportError } = this.state;
    const DetailedCSVRecords = this.createDetailedReportCSVFile();
    const isAsync = true;
    return (
      <div className="details-report clearfix">
        <p><i className="icon-info"></i>{texts.DETAILED_REPORT.chooseParameter}</p>
        <div className="form-inline-left">
          <form className="form-inline">
            <div className="gaf-form gaf-select-box form-group">
              <label className="control-label">{texts.DETAILED_REPORT.viewBy}</label>
              <Dropdown
                id="cm-detailedReport-dropDown"
                menuItems={texts.DETAILED_REPORT.selectYear}
                onChangeHandler={this.handleDetailedReportDropDown}
              />
            </div>
            {isDetailedReportError ? <div className="text-danger">{texts.detailedReportError}</div> : null}
          </form>
        </div>
        <div className="details-report-btn">
          <CSV
            filename={texts.detailedReportfilename}
            config={texts.DetailedCSVConfig}
            data={DetailedCSVRecords}
            onClick={() => this.DetailedReportCSVFile()}
            isAsync={isAsync}
            triggerDownload={downloadCSV}
          >
            <div className="btn btn-block gaf-btn-primary">{texts.DETAILED_REPORT.submit}</div>
          </CSV>
        </div>
      </div>);
  };

  getTogglePopupBody = () => (<div className="details-report-msg">{texts.ToggleText} {this.props.ToggleData.isAchEnabled ? 'enabled' : 'disabled'} {texts.ToggleText1} <span><strong>{this.props.ToggleData.contractorName}</strong></span></div>);

  detailedReportErrorUpdate = () => {
    this.setState({
      isDetailedReportError: true,
    });
  }

  updateDownloadCSVState = (downloadCSV) => this.setState({ downloadCSV });

  createDetailedReportCSVFile = () => {
    const csv = [];
    const { detailedReportData } = this.props;
    if (detailedReportData) {
      detailedReportData.forEach((record) => {
        const data = { ...record };
        const {
          customerId, planType, category, product, value, allowanceType, uom, startDate, endDate,
        } = data;
        const exportField = {
          customerId: customerId || 'N/A',
          planType: planType || 'N/A',
          category: category || 'N/A',
          product: product || 'N/A',
          value: value || value === 0 ? value : 'N/A',
          allowanceType: allowanceType || 'N/A',
          uom: uom || 'N/A',
          startDate: formatDateUTC(startDate),
          endDate: formatDateUTC(endDate),
        };
        csv.push(exportField);
      });
    }
    return csv;
  }

  DetailedReportCSVFile = () => {
    this.setState({
      downloadCSV: false,
    }, () => {
      this.isDetailedReportClicked = true;
      this.props.submitDetailedReport(this.state.onchangeDropDown);
    });
  }
  /* Dispatching Claims List Action when Apply Handler Clicked */
  applyHandler = () => {
    this.setState({
      currentPageNumber: 1,
    }, () => {
      this.makingContractorListAPI();
    });
    this.resetSearchByError();
  }

  handleToggleAction = (selectedRow, index) => {
    this.resetSearchByError();
    const { email } = authentication.getAuthDetails();
    this.props.achToggle(index, selectedRow, email);
  }

  closePopUpToggle = (e) => {
    e.preventDefault();
    this.props.closeTogglePopup();
  }

  pageTitle = () => (
    <div className="gaf-title-block">
      <h2>{texts.label.header}</h2>
      <p>{texts.t1.text}</p>
    </div>
  );

  detailedReportElement = () => (
    <Dialog
      show={this.state.showDetailedReport}
      onCloseClick={(ev) => this.handleDetailedReportClose(ev)}
      title={texts.DETAILED_REPORT.title}
      body={this.getDetailedReportBody()}
      className="gaf-model-popup details-report"
    />
  );

  toggleModelElement = () => (
    <Dialog
      show={this.props.successToggleFlag}
      title={texts.TogglePopupTitle}
      body={this.getTogglePopupBody()}
      className="gaf-model-popup"
      onCloseClick={this.closePopUpToggle}
    />
  );

  showPerPageHandler = (showPerPage) => {
    this.resetSearchByError();
    this.props.updatePageDetails({ pageNumber: 1, showPerPage });
  }

  sortHandler = (sortField, isAscending) => {
    this.resetSearchByError();
    this.props.updatePageDetails({ isAscending, sortField, pageNumber: 1 });
  }

  paginationHandler = (pageNumber) => {
    this.resetSearchByError();
    this.props.updatePageDetails({ pageNumber });
  }


  showByAndPagniation = () => {
    const { totalItems, showPerPage, pageNumber } = this.props;
    return (
      <div className="gaf-pagination">
        {totalItems > showPerPage &&
          <Row>
            <Col sm={6} md={6} xsHidden>
              <Form inline id="cd-showby">
                <ShowBy onChange={this.showPerPageHandler} defaultValue={this.state.isShowByOptSelected} />
              </Form>
            </Col>
            <Col sm={6} md={6} xs={12}>
              <Pagination selected={pageNumber} totalRows={totalItems} perPage={showPerPage} pageClick={this.paginationHandler} />
            </Col>
          </Row>
        }
      </div>);
  };


  handleDetailedReportClose(e) {
    if (e) {
      e.preventDefault();
    }
    this.setState({
      showDetailedReport: false,
      onchangeDropDown: moment().year(),
      isDetailedReportError: false,
    });
    this.props.closeDetailedPopup();
  }

  createContractorListCSV = () => ([]);

  stateNameChange = (ListOfstates) => {
    if (!ListOfstates) return [];
    const formattedStates = ListOfstates.map((recard) => ({
      ...recard,
      label: recard.code,
      value: recard.code,
    }));
    return showAll.concat(formattedStates);
  }

  /* Clear the data when Clear button clicked */
  clearHandler = () => {
    const advanceSearchObj = {
      matchAll: texts.matchAll,
      redeemCheck: null,
      planType: '',
      contractorType: '',
      state: '',
      city: '',
    };
    const cities = {
      isSearchByIcon: false,
      searchByChar: {
        min: 0,
      },
      // disabled: true,
    };
    this.setState({
      advanceSearch: advanceSearchObj,
      cities,
      isResetSearch: true,
      currentPageNumber: 1,
      isCityResetSearch: true,
      isOpenAdvanceSearch: false,
    }, () => {
      window.setTimeout(() => {
        this.setState({ isCityResetSearch: false });
      }, 500);
      this.makingContractorListAPI();
    });
  }

  searchByOptionSelected = (citiyName) => {
    const { advanceSearch } = this.state;
    const advanceSearchObj = { ...advanceSearch };
    advanceSearchObj.city = citiyName.name;
    this.setState({ advanceSearch: advanceSearchObj });
  }

  stateOnchange = (dropdownState, changeValue) => {
    const { advanceSearch } = this.state;
    const advanceSearchObj = { ...advanceSearch };
    advanceSearchObj[dropdownState] = changeValue;
    this.setState({ advanceSearch: advanceSearchObj });
    const cities = Object.assign({}, this.state.cities);
    // if (changeValue !== '') {
    //   cities.disabled = false;
    // } else {
    //   cities.disabled = true;
    // }
    this.setState({ cities });
  }

  handleDetailedReportDropDown = (data) => {
    this.setState({ onchangeDropDown: data, isDetailedReportError: false });
  }

  advanceSearchOnChange = (typeOfDropDown, changeValue) => {
    const { advanceSearch } = this.state;
    const advanceSearchObj = { ...advanceSearch };
    advanceSearchObj[typeOfDropDown] = changeValue;
    this.setState({ advanceSearch: advanceSearchObj });
  }

  /* Creating Request for contractor List API Call */
  creatingContractorListRequest = () => {
    const {
      isOpenAdvanceSearch,
      contractorNameOrId,
      advanceSearch,
      currentPageNumber,
      isShowByOptSelected,
      sortBy,
      sortOrder,
    } = this.state;
    const {
      matchAll,
      redeemCheck,
      planType,
      contractorType,
      state,
      city,
    } = advanceSearch;
    const redeemCheckCondition = redeemCheck ? (redeemCheck === texts.redeemCheckValue) : null;
    const request = {
      contractorList: {
        filter: {
          advancedSearch: isOpenAdvanceSearch,
          contractorNameOrId,
          redeemCheck: redeemCheckCondition,
          planType,
          contractorType,
          state,
          city,
          matchAll: matchAll === texts.matchAll,
        },
        applyPaging: true,
        paging: {
          pageNo: currentPageNumber,
          pageSize: isShowByOptSelected,
          sortBy,
          sortOrder,
        },
      },
    };
    return request;
  }

  /* Making API call for contractor List */
  makingContractorListAPI = () => {
    const request = this.creatingContractorListRequest().contractorList;
    this.props.getContractorList(request);
  }

  /* Advance Search Handler Toogle */
  advanceSearchHandler = (bool) => {
    this.setState({
      isOpenAdvanceSearch: bool,
      isResetSearch: true,
      isCityResetSearch: false,
    });
  }

  /* Show By Option Selection */
  showperPage = (selectedshowperPage) => {
    this.setState({
      isShowByOptSelected: Number(selectedshowperPage),
      currentPageNumber: 1,
      isResetSearch: true,
    }, () => {
      this.makingContractorListAPI();
    });
  }


  searchByCities = (value) => {
    const { advanceSearch } = this.state;
    const advanceSearchObj = { ...advanceSearch };
    advanceSearchObj.city = value;
    this.setState({
      advanceSearch: advanceSearchObj,
      currentPageNumber: 1,
    });
  }

  advanceSearchElement = () => {
    const {
      states,
    } = this.props;
    const {
      advanceSearch,
    } = this.state;
    const isDisableMatch = true;
    const formattedStates = this.stateNameChange(states);
    return (
      <Panel id="collapsible-panel-example-1" className="gaf-ad-search-panel" expanded={this.state.isOpenAdvanceSearch} onToggle={() => { }}>
        <Panel.Collapse>
          <Panel.Body>
            <Row>
              <Col md={12}>
                <div className="filterby">
                  <Form inline>
                    <FormGroup className="gaf-form match-criteria">
                      <span className="hidden-xs hidden-sm">{texts.matchCriteria.title}</span>
                      <span className="visible-xs visible-sm">{texts.matchCriteria.title}<span className="input-short-text">({texts.matchCriteria.ofCriteria})</span></span>
                      <Dropdown
                        id="cm-matchAll"
                        menuItems={texts.match}
                        onChangeHandler={(e) => this.advanceSearchOnChange('matchAll', e)}
                        value={advanceSearch.matchAll}
                        readOnly={isDisableMatch}
                      />
                      <span className="input-short-text hidden-xs hidden-sm">{texts.matchCriteria.ofCriteria}</span>
                    </FormGroup>{' '}
                    <FormGroup className="gaf-form redeem-check">
                      <label>{texts.advanceSearch.redeemCheck}</label>
                      <Dropdown
                        id="cm-rdeemCheck"
                        menuItems={texts.redeemCheck}
                        onChangeHandler={(e) => this.advanceSearchOnChange('redeemCheck', e)}
                        value={advanceSearch.redeemCheck}
                      />
                    </FormGroup>{' '}
                    <FormGroup className="gaf-form plan-type hidden-xs  hidden-sm">
                      <label>{texts.advanceSearch.planType}</label>
                      <Dropdown
                        id="cm-planType"
                        menuItems={texts.planType}
                        onChangeHandler={(e) => this.advanceSearchOnChange('planType', e)}
                        value={advanceSearch.planType}
                      />
                    </FormGroup>{' '}
                    {/* visible in mobile view */}
                    <FormGroup className="gaf-form contractor-type hidden-xs hidden-sm hidden-lg hidden-md">
                      <label>{texts.advanceSearch.contractorType}</label>
                      <Dropdown
                        id="cm-contractorType"
                        menuItems={texts.contractorType}
                        placeholder="Select"
                        onChangeHandler={(e) => this.advanceSearchOnChange('contractorType', e)}
                        value={advanceSearch.contractorType}
                      />
                    </FormGroup>{' '}
                  </Form>
                </div>
              </Col>
              <Col md={12}>
                <div className="filterby calender-section">
                  <Form inline>
                    <FormGroup className="city-input gaf-form">
                      <label>{texts.advanceSearch.city}</label>
                      <SearchBy id={searchByType.CityList} typeahead="false" isSearchByIcon={this.state.cities.isSearchByIcon} charLength={this.state.cities.searchByChar} handleChange={this.searchByCities} validationErrTxt="" dynamicClass="pull-right" isResetSearch={this.state.isCityResetSearch} label={texts.cityLabelName} />
                    </FormGroup>{' '}
                    <FormGroup className="gaf-form state ">
                      <label>{texts.advanceSearch.state}</label>
                      {formattedStates && formattedStates.length > 0 &&
                        <Dropdown
                          id="cm-state"
                          menuItems={this.stateNameChange(states)}
                          onChangeHandler={(e) => this.stateOnchange('state', e)}
                          value={advanceSearch.state}
                        />}
                    </FormGroup>{' '}
                    {/* visible in Desktop view */}
                    <FormGroup className="gaf-form contractor-type">
                      <label>{texts.advanceSearch.contractorType}</label>
                      <Dropdown
                        id="cm-mobilecontractorType"
                        menuItems={texts.contractorType}
                        onChangeHandler={(e) => this.advanceSearchOnChange('contractorType', e)}
                        value={advanceSearch.contractorType}
                      />
                    </FormGroup>{' '}
                    {/* plan mobile view */}
                    <FormGroup className="gaf-form plan-type hidden-lg hidden-md">
                      <label>{texts.advanceSearch.planType}</label>
                      <Dropdown
                        id="cm-mobileplanType"
                        menuItems={texts.planType}
                        placeholder="Select"
                        onChangeHandler={(e) => this.advanceSearchOnChange('planType', e)}
                        value={advanceSearch.planType}
                      />
                    </FormGroup>{' '}
                    {/* plan mobile view end */}
                    <div className="btn-actions">
                      <Button className="btn gaf-btn-secondary" onClick={this.clearHandler}>{texts.advanceSearch.clear}</Button>
                      <Button className="btn gaf-btn-primary" onClick={this.applyHandler}>{texts.advanceSearch.apply}</Button>
                    </div>
                  </Form>
                </div>
              </Col>
            </Row>
          </Panel.Body>
        </Panel.Collapse>
      </Panel>);
  };

  /* Triggering Action when clear or empty by serach box */
  searchByNameOrID = (value) => {
    this.setState({ isResetSearch: false, isContractorNameSubmitted: false });
    if (this.state.isContractorNameSubmitted && !value) {
      this.setState({
        currentPageNumber: 1,
        contractorNameOrId: '',
      }, () => {
        this.makingContractorListAPI();
      });
    } else {
      this.setState({
        currentPageNumber: 1,
        contractorNameOrId: value,
      });
    }
  }

  /* Triggering Action when Search Contractor Number Submitted */
  searchContractorNameOrIDSubmitted = (value, bool) => {
    if (value && bool) {
      const advanceSearchObj = {
        matchAll: texts.matchAll,
        redeemCheck: null,
        planType: '',
        contractorType: '',
        state: '',
        city: '',
      };
      const cities = {
        isSearchByIcon: false,
        searchByChar: {
          min: 0,
        },
      };
      this.setState({
        isOpenAdvanceSearch: false,
        advanceSearch: advanceSearchObj,
        cities,
        currentPageNumber: 1,
        contractorNameOrId: value,
        isContractorNameSubmitted: true,
        isCityResetSearch: true,
      }, () => {
        this.makingContractorListAPI();
      });
    }
    if (!value || !bool) {
      this.setState({
        isResetSearch: false,
      });
    }
  }

  createInvoiceListCSV = () => {
    const csv = [];
    const { contractorAllRows } = this.props;
    if (contractorAllRows) {
      contractorAllRows.forEach((record) => {
        const data = { ...record };
        const {
          displayContractorId, contractorName, address: { stateOrProvince, city }, contractorType, planType,
        } = data;
        const exportField = {
          displayContractorId: displayContractorId || 'N/A',
          contractorName: contractorName || 'N/A',
          state: stateOrProvince || 'N/A',
          city: city || 'N/A',
          contractorType: contractorType || 'N/A',
          planType: planType || 'N/A',
          isAchEnabled: record.isAchEnabled ? 'Enabled' : 'Disabled',
        };
        csv.push(exportField);
      });
    }
    return csv;
  }

  resetSearchByError = () => {
    this.setState({
      isResetSearch: true,
    });
  }

  tableHeder = () => (
    <div>
      {this.renderCSVLink()}
    </div>
  );

  searchByContractorOrIDElement = () => (
    <Col md={6} lg={5} lgOffset={4} lgPull={3} mdOffset={3} mdPull={3} xs={12}>
      <div className="form-group gaf-form">
        <SearchBy
          id={searchByType.ContractorNameOrId}
          placeholder={this.state.searchByContractorNameOrId.placeholder}
          typeahead="false"
          isSearchByIcon={this.state.searchByContractorNameOrId.isSearchByIcon}
          charLength={this.state.searchByContractorNameOrId.searchByChar}
          handleChange={this.searchByNameOrID}
          handleSubmit={this.searchContractorNameOrIDSubmitted}
          validationErrTxt={texts.searchValidChar}
          isResetSearch={this.state.isResetSearch}
        />
      </div>
    </Col>
  );

  renderCSVLink = () => {
    const { contractorAllRows } = this.props;
    const CSVRecords = this.createInvoiceListCSV();
    return (
      <div className="gaf-export-block pull-right">
        <CSV
          filename={texts.heading.filename}
          config={texts.csvConfig}
          data={CSVRecords}
          disabled={!contractorAllRows || contractorAllRows.length === 0}
        >
          <div className="pull-right">
            <span><i className="icon-Export-Icon"></i><span>{texts.heading.label}</span></span>
          </div>
        </CSV>
      </div>);
  };

  render() {
    const {
      contractorData,
      isToggled,
      isLoading,
      isDetailedReportLoading,
    } = this.props;
    const isShowAdvanceIcon = classNames({ 'open-advance-search': this.state.isOpenAdvanceSearch });
    return (
      <div>
        {/* Center Body Content */}
        <Helmet>
          <title>{pageTitle.CONTRACTOR_MANAGEMENT}</title>
        </Helmet>
        <main className="main-content contractor-management">
          <section>
            <Grid>
              {/* GAF - pageTitle in contractorManagement */}
              {this.pageTitle()}
              {/* GAF - Page details */}
              <div className="gaf-page-info">
                <div className="gaf-search-area">
                  <div className="gaf-search">
                    <Row>
                      <Col md={3} mdPush={9} lg={3} lgPush={9} xs={12}>
                        <div className="ad-search-filter-icon">
                          <a href="/" className="icon-filter visible-sm visible-xs" onClick={(e) => { e.preventDefault(); this.advanceSearchHandler(!this.state.isOpenAdvanceSearch); }}></a>
                        </div>
                        <Button className={`btn-block ad-search-btn visible-md visible-lg ${isShowAdvanceIcon}`} onClick={(e) => { e.preventDefault(); this.advanceSearchHandler(!this.state.isOpenAdvanceSearch); }}>
                          {texts.advanceSearch.title}<i className="icon-arrow-up"></i>
                        </Button>
                      </Col>
                      {this.searchByContractorOrIDElement()}
                    </Row>
                  </div>
                  <div className="gaf-advance-search">
                    {this.advanceSearchElement()}
                  </div>
                </div>
                {/* GAF - Page Details Ends */}
                {/* List of Contractor Details */}
                <div className="claim-list">
                  <div className="gaf-table-header">
                    <div className="clearfix">
                      <div className="contractor-table-title">
                        <h4>{texts.contractorList.title}</h4>
                      </div>
                      <div className="contractor-icon-block gaf-export-block">
                        {this.detailedReportElement()}
                        {this.tableHeder()}
                        {this.toggleModelElement()}
                        <a href="/" onClick={(e) => this.onDetailedReportClick(e)}><span className="download-report"><i className="icon-download"></i><span>{texts.DETAILED_REPORT.title}</span></span></a>
                        <div className="clearfix"></div>
                      </div>
                    </div>
                  </div>
                  <ContractorList contractorListInfo={contractorData} onSort={this.sortHandler} toggleAction={this.handleToggleAction} loading={isLoading} />
                  {this.showByAndPagniation()}
                </div>
              </div>
            </Grid>
          </section>
          {(isLoading || isToggled || isDetailedReportLoading) ? <Loader /> : null}
        </main>
      </div >
    );
  }
}

function mapStateToProps(state) {
  const { contractorListState, states } = state;
  const {
    totalItems,
    showPerPage,
    pageNumber,
    isLoading,
    isToggled,
    successToggleFlag,
    contractorListInfo,
    detailedReportData,
    isDetailedReportLoading,
  } = contractorListState;
  const { ListOfstates } = contractorListState;
  const { states: stateValues } = states;
  return {
    ListOfstates,
    isToggled,
    isLoading,
    successToggleFlag,
    ToggleData: getToggleData(state) || {},
    states: stateValues,
    totalItems,
    showPerPage,
    pageNumber,
    contractorAllRows: contractorListInfo,
    contractorData: getcontractorListByPageNo(state),
    detailedReportData,
    isDetailedReportLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getContractorList: bindActionCreators(getContractorList, dispatch),
    achToggle: bindActionCreators(achToggle, dispatch),
    closeTogglePopup: bindActionCreators(closeTogglePopup, dispatch),
    submitDetailedReport: bindActionCreators(submitDetailedReport, dispatch),
    closeDetailedPopup: bindActionCreators(closeDetailedPopup, dispatch),
    statesActions: bindActionCreators(statesActionsCreators, dispatch),
    updatePageDetails: bindActionCreators(updatePageDetails, dispatch),
    resetInitialState: bindActionCreators(resetInitialState, dispatch),
  };
}

ContractorManagement.propTypes = {
  getContractorList: PropTypes.func,
  achToggle: PropTypes.func,
  isToggled: PropTypes.bool,
  isLoading: PropTypes.bool,
  successToggleFlag: PropTypes.bool,
  closeTogglePopup: PropTypes.func,
  ToggleData: PropTypes.object,
  submitDetailedReport: PropTypes.func,
  closeDetailedPopup: PropTypes.func,
  statesActions: PropTypes.object.isRequired,
  states: PropTypes.array,
  contractorData: PropTypes.array,
  totalItems: PropTypes.number,
  showPerPage: PropTypes.number,
  pageNumber: PropTypes.number,
  updatePageDetails: PropTypes.func,
  contractorAllRows: PropTypes.array,
  resetInitialState: PropTypes.func,
  detailedReportData: PropTypes.array,
  isDetailedReportLoading: PropTypes.bool,
};


export default connect(mapStateToProps, mapDispatchToProps)(ContractorManagement);
