import { createSelector } from 'reselect';

export const contractorListInfo = (state) => state.contractorListState.contractorListInfo;
export const toggleIndex = (state) => state.contractorListState.toggleIndex;
export const getPageNo = (state) => state.contractorListState.pageNumber;
export const showPerPage = (state) => state.contractorListState.showPerPage;
export const isAscending = (state) => state.contractorListState.isAscending;
export const sortField = (state) => state.contractorListState.sortField;

export const getToggleData = createSelector(
  [contractorListInfo, toggleIndex],
  (getContractorList, index) => {
    const list = [...getContractorList];
    if (list.length > 0) {
      if (index !== undefined && index !== '') {
        const data = list[index];
        const { contractorName = '', isAchEnabled } = data;
        return {
          contractorName,
          isAchEnabled,
        };
      }
    }
    const contractorName = '';
    const isAchEnabled = '';
    return {
      contractorName,
      isAchEnabled,
    };
  }
);


export const getcontractorListByPageNo = createSelector(
  [contractorListInfo, getPageNo, showPerPage],
  (contractorList, pageNumber, itemsPerPage) => {
    if (contractorList) {
      let list = [...contractorList];
      list = list.slice((pageNumber - 1) * itemsPerPage, pageNumber * itemsPerPage);
      return list;
    }
    return [];
  },
);
