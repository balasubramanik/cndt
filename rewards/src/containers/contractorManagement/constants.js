import moment from 'moment';
const showAll = [{ label: 'Show All', value: '' }];
const texts = {
  match: [
    { label: 'All', value: 'All' },
    { label: 'Any', value: 'Any' },
  ],
  matchAll: 'All',
  redeemCheck: [
    ...showAll,
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' },
  ],
  redeemCheckValue: 'Yes',
  contractorType: [
    ...showAll,
    { label: 'Certified', value: 'Certified' },
    { label: 'Master Elite', value: 'Master Elite' },
    { label: 'Non-Certified', value: 'Non-Certified' },
  ],
  report: {
    title: 'List of Rewards Contractor',
    filename: 'ContractorList.csv',
  },
  matchCriteria: {
    title: 'Match',
    ofCriteria: 'of the following criteria',
  },
  advanceSearch: {
    title: 'Custom Search',
    clear: 'Clear',
    apply: 'Apply',
    redeemCheck: 'Redeem ACH',
    planType: 'Plan Type',
    contractorType: 'Contractor Type',
    city: 'City',
    state: 'State',
  },
  csvConfig: [
    { colName: 'CUSTOMER ID #', key: 'displayContractorId' },
    { colName: 'CONTRACTOR NAME', key: 'contractorName' },
    { colName: 'CITY', key: 'city' },
    { colName: 'STATE', key: 'state' },
    { colName: 'CONTRACTOR TYPE', key: 'contractorType' },
    { colName: 'PLAN TYPE', key: 'planType' },
    { colName: 'REDEEM ACH', key: 'isAchEnabled' },
  ],
  DetailedCSVConfig: [
    { colName: 'CUSTOMER ID #', key: 'customerId' },
    { colName: 'PLAN TYPE', key: 'planType' },
    { colName: 'CATEGOTRY', key: 'category' },
    { colName: 'PRODUCT', key: 'product' },
    { colName: 'ALLOWANCE TYPE', key: 'allowanceType' },
    { colName: 'UOM', key: 'uom' },
    { colName: 'VALUE', key: 'value' },
    { colName: 'STARTDATE', key: 'startDate' },
    { colName: 'ENDDATE', key: 'endDate' },
  ],
  detailedReportfilename: 'contractors_detail_report.csv',
  planType: [
    ...showAll,
    { label: 'Standard', value: 'Standard' },
    { label: 'Custom', value: 'Custom' },
  ],
  label: {
    header: 'Contractor Management',
  },
  contractorList:
  {
    title: 'List of Contractors',
  },
  t1:
    { text: 'View, manage, and pull reporting on all GAF Rewards contractors.' },
  EXPORT: 'Export',
  DETAILED_REPORT: {
    title: 'Detailed Report',
    chooseParameter: 'Choose your parameter below.',
    viewBy: 'View by',
    selectYear: [
      { label: 'Current Year', value: moment().year() },
      { label: 'Previous Year', value: moment().year() - 1 },
    ],
    submit: 'Submit',
  },
  TogglePopupTitle: 'Redeem ACH',
  SearchByText: 'Search by Contractor Name / Customer ID',
  DOWNLOAD: {
    downloadTxt: '  Please note that a link to this report will be sent to the registered email.',
  },
  ToggleText: 'ACH option has been successfully',
  ToggleText1: 'for ',
  heading: {
    filename: 'ListOfContractors.csv',
    label: 'Export',
  },
  searchValidChar: 'Please enter a Valid Character.',
  cityLabelName: 'City',
  applyError: 'Please select a value for at least one field to apply this filter.',
  detailedReportError: 'No Records Found.',
};

export { texts, showAll };
