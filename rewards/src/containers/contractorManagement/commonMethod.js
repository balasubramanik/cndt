
import { ascending, descending } from '../../utils/utils';

export const getSortedData = (contractorList, state) => {
  if (contractorList) {
    const contractorListData = [...contractorList];
    const data = [];
    contractorListData.forEach((record) => {
      const row = { ...record };
      row.state = record.address.stateOrProvince;
      row.city = record.address.city;
      data.push(row);
    });
    if (state.sortField) {
      if (state.isAscending) {
        data.sort(ascending(state.sortField));
      } else {
        data.sort(descending(state.sortField));
      }
    }
    return data;
  }
  return [];
};
