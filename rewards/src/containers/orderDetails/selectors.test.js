import test from 'tape';
import { getOrderList, getPageNo, showPerPage, isAscending, sortField, getSearchText, pointEarned, getDefaultSortData, getFilteredList, getSortedData, getOrderListByPageNo } from './selectors';

const t = test('test utils', (b) => b);
describe('Order Details Selectors', () => {
  it('should return OrderList', () => {
    const mock = {
      orderDetailsState: {
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual(mock.orderDetailsState.listOfOrder, getOrderList(mock));
  });

  it('should return page number', () => {
    const mock = {
      orderDetailsState: {
        pageNumber: 1,
      },
    };
    t.deepEqual(mock.orderDetailsState.pageNumber, getPageNo(mock));
  });

  it('should return show per page', () => {
    const mock = {
      orderDetailsState: {
        showPerPage: 25,
      },
    };
    t.deepEqual(mock.orderDetailsState.showPerPage, showPerPage(mock));
  });
  it('should return isAscending', () => {
    const mock = {
      orderDetailsState: {
        isAscending: true,
      },
    };
    t.deepEqual(mock.orderDetailsState.isAscending, isAscending(mock));
  });
  it('should return sortField', () => {
    const mock = {
      orderDetailsState: {
        sortField: 'productName',
      },
    };
    t.deepEqual(mock.orderDetailsState.sortField, sortField(mock));
  });
  it('should return getSearchText', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'Asad hjkjjh',
      },
    };
    t.deepEqual(mock.orderDetailsState.searchText, getSearchText(mock));
  });
  it('should return pointEarned', () => {
    const mock = {
      orderDetailsState: {
        pointEarned: '1300',
      },
    };
    t.deepEqual(mock.orderDetailsState.pointEarned, pointEarned(mock));
  });
  it('should return default sort order', () => {
    const mock = {
      orderDetailsState: {
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
          orderDate: '2018-11-30T09:10:56.6173327Z',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
          orderDate: '2018-11-29T09:10:56.6173327Z',
        },
        ],
      },
    };
    t.deepEqual([{
      productName: 'Jiffy Lube', serviceFeeType: '$', serviceFeeRate: '0.0', amountRedeemed: '2000', cardValue: '5000', orderDate: '2018-11-30T09:10:56.6173327Z',
    }, {
      productName: 'Cabela’s', serviceFeeType: '$', serviceFeeRate: '2.0', amountRedeemed: '1000', cardValue: '5000', orderDate: '2018-11-29T09:10:56.6173327Z',
    }], getDefaultSortData(mock));
  });
  it('should return empty array when orderList empty', () => {
    const mock = {
      orderDetailsState: {
        listOfOrder: [],
      },
    };
    t.deepEqual([], getDefaultSortData(mock));
  });
  it('should return Filtered List', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual([
      {
        productName: 'Cabela’s',
        serviceFeeType: '$',
        serviceFeeRate: '2.0',
        amountRedeemed: '1000',
        cardValue: '5000',
      },
    ], getFilteredList(mock));
  });
  it('should return Filtered List for no search text', () => {
    const mock = {
      orderDetailsState: {
        searchText: '',
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual([{
      productName: 'Jiffy Lube',
      serviceFeeType: '$',
      serviceFeeRate: '0.0',
      amountRedeemed: '2000',
      cardValue: '5000',
    },
    {
      productName: 'Cabela’s',
      serviceFeeType: '$',
      serviceFeeRate: '2.0',
      amountRedeemed: '1000',
      cardValue: '5000',
    },
    ], getFilteredList(mock));
  });
  it('should return Ascending order Sorted Data according to the key', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        isAscending: true,
        sortField: 'productName',
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        {
          productName: 'Cabdet',
          serviceFeeType: '$',
          serviceFeeRate: '1.3',
          amountRedeemed: '3000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual([
      {
        productName: 'Cabdet',
        serviceFeeType: '$',
        serviceFeeRate: '1.3',
        amountRedeemed: '3000',
        cardValue: '5000',
      },
      {
        productName: 'Cabela’s',
        serviceFeeType: '$',
        serviceFeeRate: '2.0',
        amountRedeemed: '1000',
        cardValue: '5000',
      },
    ], getSortedData(mock));
  });
  it('should return Descending order Sorted Data according to the key', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        isAscending: false,
        sortField: 'productName',
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        {
          productName: 'Cabdet',
          serviceFeeType: '$',
          serviceFeeRate: '1.3',
          amountRedeemed: '3000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual([
      {
        productName: 'Cabela’s',
        serviceFeeType: '$',
        serviceFeeRate: '2.0',
        amountRedeemed: '1000',
        cardValue: '5000',
      },
      {
        productName: 'Cabdet',
        serviceFeeType: '$',
        serviceFeeRate: '1.3',
        amountRedeemed: '3000',
        cardValue: '5000',
      },
    ], getSortedData(mock));
  });
  it('should return Data according to the key when no sortfield is provided', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        isAscending: false,
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        {
          productName: 'Cabdet',
          serviceFeeType: '$',
          serviceFeeRate: '1.3',
          amountRedeemed: '3000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual([{
      productName: 'Cabela’s', serviceFeeType: '$', serviceFeeRate: '2.0', amountRedeemed: '1000', cardValue: '5000',
    }, {
      productName: 'Cabdet', serviceFeeType: '$', serviceFeeRate: '1.3', amountRedeemed: '3000', cardValue: '5000',
    }], getSortedData(mock));
  });
  it('should return empty object when no orderList is provided', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        isAscending: false,
        listOfOrder: [],
      },
    };
    t.deepEqual([], getSortedData(mock));
  });
  it('should return getOrderListByPageNo', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        isAscending: true,
        showPerPage: 25,
        pageNumber: 1,
        sortField: 'productName',
        listOfOrder: [{
          productName: 'Jiffy Lube',
          serviceFeeType: '$',
          serviceFeeRate: '0.0',
          amountRedeemed: '2000',
          cardValue: '5000',
        },
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
        {
          productName: 'Cabdet',
          serviceFeeType: '$',
          serviceFeeRate: '1.3',
          amountRedeemed: '3000',
          cardValue: '5000',
        },
        ],
      },
    };
    t.deepEqual([
      {
        productName: 'Cabdet',
        serviceFeeType: '$',
        serviceFeeRate: '1.3',
        amountRedeemed: '3000',
        cardValue: '5000',
      },
      {
        productName: 'Cabela’s',
        serviceFeeType: '$',
        serviceFeeRate: '2.0',
        amountRedeemed: '1000',
        cardValue: '5000',
      },
    ], getOrderListByPageNo(mock));
  });
  it('should return getOrderListByPageNo when order list not defined', () => {
    const mock = {
      orderDetailsState: {
        searchText: 'cab',
        isAscending: true,
        listOfOrder: [],
        showPerPage: 25,
        pageNumber: 1,
        sortField: 'productName',
      },
    };
    t.deepEqual([], getOrderListByPageNo(mock));
  });
});
