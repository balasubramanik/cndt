import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import OrderDetailsCont, { OrderDetails } from '../../containers/orderDetails';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';

configure({ adapter: new Adapter() });
history.push = jest.fn();

describe('OrderDetails', () => {
  window.open = jest.fn();
  const getSuggestion = jest.fn();
  const gtmOrderDetailsToPointsHistory = jest.fn();
  const gtmOrderDetailsSearch = jest.fn();
  const gtmOrderDetailsCSVExport = jest.fn();
  const getProductClear = jest.fn();
  const getProduct = jest.fn();
  const updatePageDetails = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const tree = shallow(<OrderDetails
    productSerachActions={{ getSuggestion }}
    totalItems={2}
    showPerPage={1}
    pageNumber={1}
    isLoading
    orderList={[{
      productName: 'Jiffy Lube',
      serviceFeeType: '$',
      serviceFeeRate: '0.0',
      amountRedeemed: '2000',
      cardValue: '5000',
    },
    {
      productName: 'Cabela’s',
      serviceFeeType: '$',
      serviceFeeRate: '2.0',
      amountRedeemed: '1000',
      cardValue: '5000',
    },
    ]}
    orderItems={[{
      productName: 'Jiffy Lube',
      serviceFeeType: '$',
      serviceFeeRate: '0.0',
      amountRedeemed: '2000',
      cardValue: '5000',
    },
    {
      productName: 'Cabela’s',
      serviceFeeType: '$',
      serviceFeeRate: '2.0',
      amountRedeemed: '1000',
      cardValue: '5000',
    },
    ]}
    orderActions={{ getProductClear, getProduct, updatePageDetails }}
    match={{ params: { orderId: '123' } }}
    pointsRedeemed="1300"
    modifiedOn="2018-09-28T21:12:44.0122176+05:30"
    gtmActions={{
      gtmOrderDetailsToPointsHistory,
      gtmOrderDetailsSearch,
      gtmOrderDetailsCSVExport,
    }}

  />);
  it('should be defined', () => {
    expect(OrderDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should redirect to PointsHistory', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('a').prop('onClick');
    simulateClick(event);
    expect(tree.state().isResetSearch).toBe(true);
    expect(history.push).toBeCalled();
    expect(gtmOrderDetailsToPointsHistory).toBeCalled();
  });

  it('should call paginationHandler', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('Pagination').prop('pageClick');
    simulateClick(event);
    expect(updatePageDetails).toBeCalled();
    expect(tree.state().isResetSearch).toBe(true);
  });

  it('should call showPerPageHandler', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('ShowBy').prop('onChange');
    simulateClick(event);
    expect(updatePageDetails).toBeCalled();
    expect(tree.state().isResetSearch).toBe(true);
  });

  it('should call sortHandler', () => {
    tree.setState({ isResetSearch: false });
    const simulateClick = tree.find('ProductList').prop('onSort');
    simulateClick(event);
    expect(updatePageDetails).toBeCalled();
    expect(tree.state().isResetSearch).toBe(true);
  });
  it('should call searchByText', () => {
    const simulateClick = tree.find('SearchBy').prop('handleChange');
    simulateClick('cab', true);
    expect(getSuggestion).toBeCalled();
  });

  it('should call searchByText when bool is false and no value', () => {
    const simulateClick = tree.find('SearchBy').prop('handleChange');
    simulateClick(false);
    expect(updatePageDetails).toBeCalled();
  });
  it('should call searchByProduct', () => {
    const simulateClick = tree.find('SearchBy').prop('handleSubmit');
    simulateClick('Cabela’s', true);
    expect(updatePageDetails).toBeCalled();
    expect(gtmOrderDetailsSearch).toBeCalled();
  });
  it('should call searchByProduct when product is not provided and bool is false', () => {
    tree.setState({ isResetSearch: true });
    const simulateClick = tree.find('SearchBy').prop('handleSubmit');
    simulateClick('', false);
    expect(updatePageDetails).toBeCalled();
    expect(gtmOrderDetailsSearch).toBeCalled();
    expect(tree.state().isResetSearch).toBe(false);
  });
});

describe('OrderDetails with connected component', () => {
  window.open = jest.fn();
  const state = {
    orderDetailsState: {
      searchText: 'cab',
      isAscending: true,
      sortField: 'productName',
      listOfOrder: [
        {
          productName: 'Cabela’s',
          serviceFeeType: '$',
          serviceFeeRate: '2.0',
          amountRedeemed: '1000',
          cardValue: '5000',
        },
      ],
      totalItems: 2,
      showPerPage: 25,
      pageNumber: 1,
      modifiedOn: '2018-09-28T21:12:44.0122176+05:30',
      pointsRedeemed: '1300',
      isLoading: true,
    },
  };
  const getSuggestion = jest.fn();
  const gtmOrderDetailsToPointsHistory = jest.fn();
  const gtmOrderDetailsSearch = jest.fn();
  const gtmOrderDetailsCSVExport = jest.fn();
  const getProductClear = jest.fn();
  const getProduct = jest.fn();
  const updatePageDetails = jest.fn();
  const store = fakeStore(state);
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <OrderDetailsCont
        productSerachActions={{ getSuggestion }}
        orderActions={{ getProductClear, getProduct, updatePageDetails }}
        match={{ params: { orderId: '123' } }}
        modifiedOn="2018-09-28T21:12:44.0122176+05:30"
        gtmActions={{
          gtmOrderDetailsToPointsHistory,
          gtmOrderDetailsSearch,
          gtmOrderDetailsCSVExport,
        }}

      />
    </Router>
  </Provider>);
  it('should be defined', () => {
    expect(OrderDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });
});
