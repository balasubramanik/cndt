import { createSelector } from 'reselect';
import { ascending, descending, sortByDate } from '../../utils/utils';

export const getOrderList = (state) => state.orderDetailsState.listOfOrder;
export const getPageNo = (state) => state.orderDetailsState.pageNumber;
export const showPerPage = (state) => state.orderDetailsState.showPerPage;
export const isAscending = (state) => state.orderDetailsState.isAscending;
export const sortField = (state) => state.orderDetailsState.sortField;
export const getSearchText = (state) => state.orderDetailsState.searchText.trim();
export const pointEarned = (state) => state.orderDetailsState.pointEarned;

export const getDefaultSortData = createSelector(
  [getOrderList],
  (orderList) => {
    const list = [...orderList];
    if (list !== []) {
      sortByDate(list, 'descending', 'modifiedOn');
    }
    return list;
  }
);

export const getFilteredList = createSelector(
  [getSearchText, getOrderList],
  (searchText, orderList) => {
    if (searchText) {
      const list = [...orderList];
      const holder = list.filter((items) => {
        const listItem = items.productName.trim().toLowerCase();
        return listItem.indexOf(searchText.toLowerCase()) > -1;
      });
      return holder;
    }
    return orderList;
  },
);

export const getSortedData = createSelector(
  [getFilteredList, isAscending, sortField],
  (orderList, isAsc, field) => {
    if (orderList) {
      const data = [...orderList];
      if (field) {
        if (isAsc) {
          data.sort(ascending(field));
        } else {
          data.sort(descending(field));
        }
      }
      return data;
    }
    return {};
  },
);

export const getTotalItems = createSelector(
  [getSortedData],
  (orderList) => {
    if (orderList) {
      return orderList.length;
    }
    return 0;
  },
);


export const getOrderListByPageNo = createSelector(
  [getSortedData, getPageNo, showPerPage],
  (orderList, pageNumber, itemsPerPage) => {
    if (orderList) {
      let list = [...orderList];
      list = list.slice((pageNumber - 1) * itemsPerPage, pageNumber * itemsPerPage);
      return list;
    }
    return [];
  },
);
