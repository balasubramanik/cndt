import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid, Row, Col, Form } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import RouteConstants from '../../constants/RouteConstants';
import { history } from '../../routes';
import { formatPoint } from '../../utils/utils';
import { texts } from './constants';
import * as orderActionCreators from '../../actionCreators/OrderDetails';
import * as productSearchBy from '../../actionCreators/SearchBy';
import ProductList from '../../components/productList';
import Pagination from '../../components/pagination';
import ShowBy from '../../components/showBy';
import SearchBy from '../../components/searchBy';
import { getOrderListByPageNo, getDefaultSortData, getTotalItems } from './selectors';
import { searchByType, pageTitle } from '../../constants/constants';
import Loader from '../../components/loader';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import RoleAccessPopup from '../../components/roleAccessPopup';
import * as getProfileDetails from '../../actionCreators/MyProfile';

export class OrderDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchByPlaceholder: texts.text.description,
      isSearchByIcon: true,
      searchByChar: {
        min: 3,
      },
      isResetSearch: true,
    };
  }

  componentDidMount() {
    const { orderActions, match } = this.props;
    const orderID = match.params.orderId;
    orderActions.updatePageDetails({
      isAscending: true,
      sortField: '',
      searchText: '',
      pageNumber: 1,
      showPerPage: 25,
    });
    orderActions.getProductClear();
    orderActions.getProduct(orderID, this.state.ShowBy);
  }


  redirectToPointsHistory = (e) => {
    const { gtmActions } = this.props;
    e.preventDefault();
    this.restSearchError();
    history.push({
      pathname: RouteConstants.POINTS_HISTORY,
    });
    gtmActions.gtmOrderDetailsToPointsHistory();
  };


  searchByText = (value, bool) => {
    const { orderActions } = this.props;
    if (bool) {
      this.props.productSerachActions.getSuggestion(value, searchByType.OrderListProduct);
    }
    if (!value) {
      orderActions.updatePageDetails({
        isAscending: true,
        sortField: '',
        searchText: '',
        pageNumber: 1,
      });
    }
  }

  searchByProduct = (product) => {
    const { orderActions, gtmActions } = this.props;
    orderActions.updatePageDetails({
      isAscending: true,
      sortField: '',
      searchText: product,
      pageNumber: 1,
    });
    gtmActions.gtmOrderDetailsSearch(product);
    if (!product) {
      this.setState({
        isResetSearch: false,
      });
    }
  }

  handleCSVDownload = () => {
    const { gtmActions } = this.props;
    gtmActions.gtmOrderDetailsCSVExport();
    this.restSearchError();
  }

  showPerPageHandler = (showPerPage) => {
    const { orderActions } = this.props;
    orderActions.updatePageDetails({ showPerPage });
    this.restSearchError();
  }

  sortHandler = (sortField, isAscending) => {
    const { orderActions } = this.props;
    orderActions.updatePageDetails({ isAscending, sortField, pageNumber: 1 });
    this.restSearchError();
  }

  paginationHandler = (pageNumber) => {
    const { orderActions } = this.props;
    orderActions.updatePageDetails({ pageNumber });
    this.restSearchError();
  }

  restSearchError = () => {
    this.setState({
      isResetSearch: true,
    });
  }

  render() {
    const {
      orderList,
      orderId,
      totalItems,
      showPerPage,
      pageNumber,
      isLoading,
      pointsRedeemed,
      accessInfo,
      profileDetails,
    } = this.props;
    return (
      <div>
        <Helmet>
          <title>{pageTitle.ORDER_DETAILS}</title>
        </Helmet>
        <main className="main-content points-history-details">
          <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={profileDetails.accessPage} />
          <Grid>
            <div className="gaf-title-block">
              <Row>
                <Col md={8}>
                  <h2>{texts.label.header} - {orderId}</h2>
                </Col>
                <Col md={4} className="text-right">
                  <div className="links">
                    <a href="/" onClick={this.redirectToPointsHistory}>{texts.text.Link}</a>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="gaf-page-info">
              <div className="points-earned">
                <Row className="show-grid">
                  <Col md={7} sm={6} xs={12}>
                    <p><span>{texts.text.points}</span><span>{formatPoint(pointsRedeemed)}</span></p>
                    <div className="clearfix"></div>
                  </Col>
                  <Col md={5} sm={6} xs={12}>
                    <SearchBy id={searchByType.OrderListProduct} placeholder={this.state.searchByPlaceholder} typeahead="false" isSearchByIcon={this.state.isSearchByIcon} charLength={this.state.searchByChar} handleChange={this.searchByText} validationErrTxt="" dynamicClass="pull-right" handleSubmit={this.searchByProduct} isResetSearch={this.state.isResetSearch} />
                  </Col>
                </Row>
              </div>
              <section className="invoice-list">
                {orderList &&
                  <ProductList data={orderList} onSort={this.sortHandler} CSVHandler={this.handleCSVDownload} isloading={isLoading} />}
                <div className="gaf-pagination">
                  {totalItems > showPerPage &&
                    <Row>
                      <Col sm={6} md={6} xsHidden>
                        <Form inline>
                          <ShowBy onChange={this.showPerPageHandler} />
                        </Form>
                      </Col>
                      <Col sm={6} md={6} xs={12}>
                        <Pagination selected={pageNumber} totalRows={totalItems} perPage={showPerPage} pageClick={this.paginationHandler} />
                      </Col>
                    </Row>
                  }
                </div>
              </section>
            </div>
          </Grid>
        </main>
        {isLoading && <Loader />}
      </div>
    );
  }
}

function mapStateToProps(state, match) {
  const { orderDetailsState, myProfileState } = state;
  const { accessInfo } = myProfileState;
  const {
    listOfOrder,
    showPerPage,
    pageNumber,
    pointsRedeemed,
    isLoading,
    orderId,
  } = orderDetailsState;
  return {
    listOfOrder,
    totalItems: getTotalItems(state),
    showPerPage,
    pageNumber,
    isLoading,
    pointsRedeemed,
    orderItems: getDefaultSortData(state),
    orderList: getOrderListByPageNo(state),
    match,
    orderId,
    accessInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    orderActions: bindActionCreators(orderActionCreators, dispatch),
    productSerachActions: bindActionCreators(productSearchBy, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
    profileDetails: bindActionCreators(getProfileDetails, dispatch),
  };
}

OrderDetails.propTypes = {
  productSerachActions: PropTypes.object,
  totalItems: PropTypes.number,
  showPerPage: PropTypes.number,
  pageNumber: PropTypes.number,
  isLoading: PropTypes.bool,
  orderList: PropTypes.array,
  orderActions: PropTypes.object,
  match: PropTypes.object,
  pointsRedeemed: PropTypes.string,
  gtmActions: PropTypes.object.isRequired,
  orderId: PropTypes.string,
  accessInfo: PropTypes.bool,
  profileDetails: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(OrderDetails));
