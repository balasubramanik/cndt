const texts = {
  label: {
    header: 'Order Details',
  },
  text: {
    Link: 'Point History',
    points: 'Points Redeemed',
    description: 'Search by Product',
  },
};

export { texts };
