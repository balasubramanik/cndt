import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import CartSummaryContainer, { CartSummary } from '../../containers/cartSummary';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });
history.push = jest.fn();
describe('CartSummary', () => {
  const handleOrderSubmit = jest.fn();
  const getRewardsDetails = jest.fn();
  const cartResetData = jest.fn();
  const clearErrors = jest.fn();
  const listItemSelectHandler = jest.fn();
  const selectedCardValue = jest.fn();
  const handleDeleteItem = jest.fn();
  const onUpdateDisableSubmit = jest.fn();
  const handleQuantityInput = jest.fn();
  const handleItemUpdate = jest.fn();
  const handleSelectedAddress = jest.fn();
  const onUpdateShippingAddress = jest.fn();
  const gtmUpdateShippingAddress = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const updatedItemDebit = {
    index: 0,
    value: '111',
    unitValue: '$25',
    giftCardValue: 25,
    quantity: 2,
    feeValue: 0.63,
    variantId: 'MER100148-1',
    isItemOrQuantityUpdate: true,
  };

  const updatedItemACH = {
    index: 2,
    value: '111',
    unitValue: '$25',
    giftCardValue: 25,
    quantity: 2,
    feeValue: 0.63,
    variantId: 'MER100148-1',
    isItemOrQuantityUpdate: true,
  };

  const handleQtyDecrement = {
    qty: 1,
    action: 'decrement',
    index: 1,
    feeRate: 1.25,
    unitValue: '$99',
  };

  const handleQtyIncrement = {
    qty: 2,
    action: 'increment',
    index: 1,
    feeRate: 1.25,
    unitValue: '$99',
  };

  const handleQtyIncWithDecrement = {
    qty: 2,
    action: 'decrement',
    index: 1,
    feeRate: 1.25,
    unitValue: '$99',
  };

  const selectCartData = {
    val: '25',
    qty: 99,
    index: 0,
  };

  const props = {
    cartData: {
      contractorAccountId: '1000689',
      createdBy: 'ben@gaf-uat1stchoiceroofing.com',
      createdOn: '2018-12-10T20:05:09.3035105Z',
      feeTotal: 4.5,
      firstName: 'Ben',
      id: 'c9069bd3-708c-449c-b4ff-8bf3f1250af7',
      impersonatedBy: null,
      impersonatedOn: null,
      impersonatorId: null,
      itemTotal: 1100,
      items: [{
        cardType: 'Physical Card',
        cardValue: '100.00',
        category: 'debit',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: false,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
        shippingAddress: {
          address1: '10311 Berea Rd',
          addressType: '1',
          city: 'Cleveland',
          country: 'USA',
        },
      },
      {
        cardType: 'Physical Card',
        cardValue: '25.00',
        category: 'giftcards',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: true,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
      },
      {
        cardType: 'e-card',
        cardValue: '100.00',
        category: 'check',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: true,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
      }],
      userInfo: {
        createdBy: 'BATCH',
        createdOn: '2018-03-05T00:00:00',
        modifiedBy: 'BATCH',
        modifiedOn: '2018-03-05T00:00:00',
      },
      status: 'Open',
      userId: 'ben@gaf-uat1stchoiceroofing.com',
      done: false,
      isCartSubmitAlready: false,
      isDebitCardAvail: true,
      isGetCartSuccess: true,
      isItemDelete: false,
      isLoading: false,
    },
    gtmActions: {
      gtmCardValue: jest.fn(),
      gtmCartDelete: jest.fn(),
      gtmCartQuantity: jest.fn(),
      gtmSubmitOrderTotal: jest.fn(),
      gtmRedemptionDetails: jest.fn(),
      gtmSubmitOrderFeeTotal: jest.fn(),
      gtmUpdateShippingAddress: jest.fn(),
      gtmSubmitOrderProductTotal: jest.fn(),
    },
    cartSummaryActions: {
      cartResetData: jest.fn(),
      getCartSummary: jest.fn(),
      updateCartSummary: jest.fn(),
      deleteCartSummary: jest.fn(),
      submitCartSummary: jest.fn(),
      cartInitialState: jest.fn(),
    },
    redeemPointsActions: {
      getGiftCatalogue: jest.fn(),
    },
    contractorUserActions: {
      getUsers: jest.fn(),
    },
    getProfileDetailsActions: {
      getRewardsDetails,
    },
    userInfo: {
      firstName: 'steven',
      lastName: 'smith',
    },
    rewardsInfo: {
      rewardPoints: '1000',
    },
    giftCardDetails: {},
    users: [{
      emailAddress: 'Tom@GAF-UAT1stchoiceroofing.com',
      firstName: 'Heather',
      lastName: 'Michellto',
    }],
    getCartCount: jest.fn(),
    onCancelToastMessage: jest.fn(),
    radioFlag: true,
    done: true,
    isLoading: true,
    isItemDelete: true,
    isDisableOrder: false,
    isDebitCardAvail: false,
    isGetCartSuccess: true,
    isCartSubmitAlready: true,
    orderNumber: 'O12345',
    giftCards: {
      variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
      productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
      variantID: 'MER1001513-2',
      variants: {
        listprice: '0.0000',
        cardType: 'E card',
        feeType: '$',
        feeValue: '1.0',
        giftCardValue: '25.0000',
        feeDescription: 'Aéropostale - $26',
        imageicon: null,
        max: '100.0',
        min: '1.0',
        minmaxtype: 'QTY',
        Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
        DisplayName: 'Aéropostale - $26',
      },
    },
    cartDeleteFailure: {
      error: 'Internal Server Error',
    },
    getCartSummaryError: {
      error: 'Internal Server Error',
    },
    updateCartSummaryError: {
      error: 'Internal Server Error',
    },
    submitCartSummaryError: {
      errors: [{
        code: '03023',
      }],
      error: 'Internal Server Error',
    },
    addressData: [{
      address1: '328 Greenwich St',
      address2: '',
      addressType: '1',
      city: 'New York',
      country: 'USA',
      phone: '258/741-0268',
      stateOrProvince: 'NY',
    }],
  };

  const tree = shallow(<CartSummary
    {...props}
    handleOrderSubmit={handleOrderSubmit}
    handleSelectedAddress={handleSelectedAddress}
    onUpdateShippingAddress={onUpdateShippingAddress}
    handleUpdate={handleItemUpdate}
  />);

  it('should be defined', () => {
    expect(CartSummary).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call handleItemUpdate Debit', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleUpdate');
    simulateChange(updatedItemDebit);
    expect(handleItemUpdate).toBeCalled();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleDelete');
    simulateChange(0, 'Universal Visa Prepaid Card (Reloadable)');
    expect(handleDeleteItem).toBeCalled();
  });

  it('should call handleDelete', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('updateCardAmount');
    simulateChange({});
    expect(selectedCardValue).toBeCalled();
  });

  it('should call onDisableOrderSubmit', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('onDisableOrderSubmit');
    simulateChange(false);
    expect(onUpdateDisableSubmit).toBeCalled();
  });

  it('should call handleQtyDecrement', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleQuantityInputValue');
    simulateChange(handleQtyDecrement);
    expect(handleQuantityInput).toBeCalled();
  });

  it('should call handleQtyIncrement', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleQuantityInputValue');
    simulateChange(handleQtyIncrement);
    expect(handleQuantityInput).toBeCalled();
  });

  it('should call handleQtyIncrement Else Part', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleQuantityInputValue');
    simulateChange(handleQtyIncWithDecrement);
    expect(handleQuantityInput).toBeCalled();
  });

  it('should call handleItemSelectHandler', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleItemSelectHandler');
    simulateChange(selectCartData);
    expect(listItemSelectHandler).toBeCalled();
  });

  it('should call handleItemUpdate for ACH', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleUpdate');
    simulateChange(updatedItemACH);
    expect(handleItemUpdate).toBeCalled();
  });

  it('should call handleOrderSubmit', () => {
    const simulateChange = tree.find('CartSummaryOrder').at(0).prop('handleOrderSubmit');
    simulateChange(true);
    expect(handleOrderSubmit).toBeCalled();
  });

  it('should call redemptionOrderDetails', () => {
    const simulateChange = tree.find('CartSummaryAddress').at(0).prop('updateShippingAddress');
    simulateChange('Branch');
    expect(gtmUpdateShippingAddress).toBeCalled();
  });

  it('should call selectShippingAddress', () => {
    const simulateChange = tree.find('CartSummaryAddress').at(0).prop('selectShippingAddress');
    simulateChange({
      address1: '10311 Berea Rd',
      addressType: '1',
      city: 'Cleveland',
      country: 'USA',
    });
    expect(handleSelectedAddress).toBeCalled();
  });

  tree.setState({
    isShowDebitModal: true,
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      cartData: {
        contractorAccountId: '1000687',
        feeTotal: 4.5,
        firstName: 'Beny',
        itemTotal: 1100,
        items: [{
          cardType: 'Physical Card',
          cardValue: '1001.00',
          category: 'debit',
          debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
          debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
          feeRate: 2.5,
          feeType: '%',
          feeValue: 2.5,
          img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
          isAvailable: true,
          isModified: false,
          isReloadableDebitCard: true,
          max: 99999,
          min: 25,
          minmaxType: '$',
          productId: 'MER1001481',
          productName: 'Universal Visa Prepaid Card (Reloadable)',
          quantity: 1,
          totalValue: 102.5,
          variantId: 'MER100148-1',
          orderTotal: 1104.5,
          shippingAddress: {
            address1: '10311 Berea Rd',
            addressType: '1',
            city: 'Cleveland',
            country: 'USA',
          },
        }],
      },
      done: false,
      isReloadDebitCardAlert: true,
      isCartSubmitAlready: false,
      isDebitCardAvail: true,
      isGetCartSuccess: false,
      isItemDelete: false,
    });
    expect(getRewardsDetails).toBeCalled();
  });

  it('should call clearError', () => {
    const simulateClick = tree.find('ErrorNotification').prop('onClear');
    simulateClick(event);
    expect(clearErrors).toBeCalled();
  });

  it('should call componentWillUnmount', () => {
    const instance = tree.instance();
    instance.componentWillUnmount({
      cartData: {},
    });
    expect(cartResetData).toBeCalled();
  });
});

describe('CartSummaryContainer', () => {
  const getCartCount = jest.fn();
  const getUsers = jest.fn();
  const getCartSummary = jest.fn();
  const getGiftCatalogue = jest.fn();
  const getRewardsDetails = jest.fn();
  const redeemPointsActions = {
    getGiftCatalogue,
  };
  const cartSummaryActions = {
    getCartSummary,
  };
  const contractorUserActions = {
    getUsers,
  };
  const getProfileDetailsActions = {
    getRewardsDetails,
  };

  const state = {
    contractorUserState: {
      users: [],
    },
    cartSummaryState: {
      contractorAccountId: '1000689',
      createdBy: 'ben@gaf-uat1stchoiceroofing.com',
      createdOn: '2018-12-10T20:05:09.3035105Z',
      feeTotal: 4.5,
      firstName: 'Ben',
      id: 'c9069bd3-708c-449c-b4ff-8bf3f1250af7',
      impersonatedBy: null,
      impersonatedOn: null,
      impersonatorId: null,
      itemTotal: 1100,
      items: [{
        cardType: 'Physical Card',
        cardValue: '100.00',
        category: 'debit',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: true,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
        shippingAddress: {
          address1: '10311 Berea Rd',
          addressType: '1',
          city: 'Cleveland',
          country: 'USA',
        },
      },
      {
        cardType: 'Physical Card',
        cardValue: '100.00',
        category: 'check',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: true,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
        shippingAddress: {
          address1: '10311 Berea Rd',
          addressType: '1',
          city: 'Cleveland',
          country: 'USA',
        },
      }],
      userInfo: {
        createdBy: 'BATCH',
        createdOn: '2018-03-05T00:00:00',
        modifiedBy: 'BATCH',
        modifiedOn: '2018-03-05T00:00:00',
      },
      status: 'Open',
      userId: 'ben@gaf-uat1stchoiceroofing.com',
      done: false,
      isCartSubmitAlready: false,
      isDebitCardAvail: true,
      isGetCartSuccess: true,
      isItemDelete: false,
      isLoading: false,
    },
    myProfileState: {
      rewardsInfo: {
        rewardPoints: '1000',
      },
      userInfo: {},
    },
    redeemPointsState: {
      giftcards: [{
        categories: null,
        categoryId: '2c60baca-93ed-4b3b-a046-d15473d8e692',
        createdBy: null,
        createdOn: '0001-01-01T00:00:00',
        description: 'RewardsCategoryDef',
        displayName: 'Apparels & Shoes',
        id: null,
        impersonatedBy: null,
        impersonatedOn: null,
        modifiedBy: null,
        products: [{
          productImage: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{2bd235ca-c19a-499d-8ce7-a89eed71d930}.png',
          cardTypes: ['Physical Card', 'e-Card'],
        }],
      }],
    },
  };
  const gtmActions = {
    gtmCardValue: jest.fn(),
    gtmCartDelete: jest.fn(),
    gtmCartQuantity: jest.fn(),
    gtmSubmitOrderTotal: jest.fn(),
    gtmRedemptionDetails: jest.fn(),
    gtmSubmitOrderFeeTotal: jest.fn(),
    gtmUpdateShippingAddress: jest.fn(),
    gtmSubmitOrderProductTotal: jest.fn(),
  };
  const store = fakeStore(state);
  const tree = mount(<Provider store={store}>
    <Router history={history}>
      <CartSummaryContainer
        gtmActions={gtmActions}
        getCartCount={getCartCount}
        cartSummaryActions={cartSummaryActions}
        redeemPointsActions={redeemPointsActions}
        getProfileDetailsActions={getProfileDetailsActions}
        contractorUserActions={contractorUserActions}
      />
    </Router>
  </Provider>);

  it('should be defined', () => {
    expect(CartSummaryContainer).toBeDefined();
  });

  it('should render cartSummary container', () => {
    expect(tree.find('#cart-summary-main-content').length).toBe(1);
  });
});


describe('CartSummary Handle Input For Same Quantity Check', () => {
  const handleOrderSubmit = jest.fn();
  const getRewardsDetails = jest.fn();
  const clearErrors = jest.fn();
  const handleQuantityInput = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };

  const handleQtyDecrement = {
    qty: 1,
    action: 'decrement',
    index: 1,
    feeRate: 1.25,
    unitValue: '$99',
  };

  const handleQtyIncrement = {
    qty: 2,
    action: 'increment',
    index: 1,
    feeRate: 1.25,
    unitValue: '$99',
  };

  const handleQtyIncWithDecrement = {
    qty: 2,
    action: 'decrement',
    index: 1,
    feeRate: 1.25,
    unitValue: '$99',
  };

  const props = {
    cartData: {
      contractorAccountId: '1000689',
      createdBy: 'ben@gaf-uat1stchoiceroofing.com',
      createdOn: '2018-12-10T20:05:09.3035105Z',
      feeTotal: 4.5,
      firstName: 'Ben',
      id: 'c9069bd3-708c-449c-b4ff-8bf3f1250af7',
      impersonatedBy: null,
      impersonatedOn: null,
      impersonatorId: null,
      itemTotal: 1100,
      items: [{
        cardType: 'Physical Card',
        cardValue: '100.00',
        category: 'debit',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: false,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
        shippingAddress: {
          address1: '10311 Berea Rd',
          addressType: '1',
          city: 'Cleveland',
          country: 'USA',
        },
      },
      {
        cardType: 'Physical Card',
        cardValue: '25.00',
        category: 'giftcards',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: true,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
      },
      {
        cardType: 'e-card',
        cardValue: '100.00',
        category: 'check',
        debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
        debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
        feeRate: 2.5,
        feeType: '%',
        feeValue: 2.5,
        img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
        isAvailable: true,
        isModified: false,
        isReloadableDebitCard: true,
        max: 99999,
        min: 25,
        minmaxType: '$',
        productId: 'MER1001481',
        productName: 'Universal Visa Prepaid Card (Reloadable)',
        quantity: 1,
        totalValue: 102.5,
        variantId: 'MER100148-1',
        orderTotal: 1104.5,
      }],
      userInfo: {
        createdBy: 'BATCH',
        createdOn: '2018-03-05T00:00:00',
        modifiedBy: 'BATCH',
        modifiedOn: '2018-03-05T00:00:00',
      },
      status: 'Open',
      userId: 'ben@gaf-uat1stchoiceroofing.com',
      done: false,
      isCartSubmitAlready: false,
      isDebitCardAvail: true,
      isGetCartSuccess: true,
      isItemDelete: false,
      isLoading: false,
    },
    gtmActions: {
      gtmCardValue: jest.fn(),
      gtmCartDelete: jest.fn(),
      gtmCartQuantity: jest.fn(),
      gtmSubmitOrderTotal: jest.fn(),
      gtmRedemptionDetails: jest.fn(),
      gtmSubmitOrderFeeTotal: jest.fn(),
      gtmUpdateShippingAddress: jest.fn(),
      gtmSubmitOrderProductTotal: jest.fn(),
    },
    cartSummaryActions: {
      cartResetData: jest.fn(),
      getCartSummary: jest.fn(),
      updateCartSummary: jest.fn(),
      deleteCartSummary: jest.fn(),
      submitCartSummary: jest.fn(),
      cartInitialState: jest.fn(),
    },
    redeemPointsActions: {
      getGiftCatalogue: jest.fn(),
    },
    contractorUserActions: {
      getUsers: jest.fn(),
    },
    getProfileDetailsActions: {
      getRewardsDetails,
    },
    userInfo: {
      firstName: 'steven',
      lastName: 'smith',
    },
    rewardsInfo: {
      rewardPoints: '1000',
    },
    giftCardDetails: {},
    users: [{
      emailAddress: 'Tom@GAF-UAT1stchoiceroofing.com',
      firstName: 'Heather',
      lastName: 'Michellto',
    }],
    getCartCount: jest.fn(),
    onCancelToastMessage: jest.fn(),
    radioFlag: true,
    done: true,
    isLoading: true,
    isItemDelete: true,
    isDisableOrder: false,
    isDebitCardAvail: false,
    isGetCartSuccess: true,
    isCartSubmitAlready: true,
    orderNumber: 'O12345',
    giftCards: {
      variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
      productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
      variantID: 'MER1001513-2',
      variants: {
        listprice: '0.0000',
        cardType: 'E card',
        feeType: '$',
        feeValue: '1.0',
        giftCardValue: '25.0000',
        feeDescription: 'Aéropostale - $26',
        imageicon: null,
        max: '100.0',
        min: '1.0',
        minmaxtype: 'QTY',
        Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
        DisplayName: 'Aéropostale - $26',
      },
    },
    addressData: [{
      address1: '328 Greenwich St',
      address2: '',
      addressType: '1',
      city: 'New York',
      country: 'USA',
      phone: '258/741-0268',
      stateOrProvince: 'Chicago',
    }],
  };

  const tree = shallow(<CartSummary
    {...props}
    handleOrderSubmit={handleOrderSubmit}
  />);

  it('should be defined', () => {
    expect(CartSummary).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call handleQtyDecrement', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleQuantityInputValue');
    simulateChange(handleQtyDecrement);
    expect(handleQuantityInput).toBeCalled();
  });

  it('should call handleQtyIncrement', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleQuantityInputValue');
    simulateChange(handleQtyIncrement);
    expect(handleQuantityInput).toBeCalled();
  });

  it('should call handleQtyIncrement Else Part', () => {
    const simulateChange = tree.find('CartSummaryList').at(0).prop('handleQuantityInputValue');
    simulateChange(handleQtyIncWithDecrement);
    expect(handleQuantityInput).toBeCalled();
  });

  it('should call handleOrderSubmit', () => {
    const simulateChange = tree.find('CartSummaryOrder').at(0).prop('handleOrderSubmit');
    simulateChange(false);
    expect(handleOrderSubmit).toBeCalled();
  });

  tree.setState({
    isShowDebitModal: true,
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      cartData: {
        contractorAccountId: '1000687',
        feeTotal: 4.5,
        firstName: 'Beny',
        itemTotal: 1100,
        items: [{
          cardType: 'Physical Card',
          cardValue: '1001.00',
          category: 'debit',
          debitCardIssuedTo: 'Tom@GAF-UAT1stchoiceroofing.com',
          debitCardUserName: 'Tom@GAF-UAT1stchoiceroofing.com',
          feeRate: 2.5,
          feeType: '%',
          feeValue: 2.5,
          img: 'https://gafweb-uat.azureedge.net/sitecore-media-library/rewardscatalogitemimages/{f0d5d602-f459-4156-a6bc-9eba2e340d76}.png',
          isAvailable: true,
          isModified: false,
          isReloadableDebitCard: true,
          max: 99999,
          min: 25,
          minmaxType: '$',
          productId: 'MER1001481',
          productName: 'Universal Visa Prepaid Card (Reloadable)',
          quantity: 1,
          totalValue: 102.5,
          variantId: 'MER100148-1',
          orderTotal: 1104.5,
          shippingAddress: {
            address1: '10311 Berea Rd',
            addressType: '1',
            city: 'Cleveland',
            country: 'USA',
          },
        }],
      },
      done: false,
      isReloadDebitCardAlert: true,
      isCartSubmitAlready: false,
      isDebitCardAvail: true,
      isGetCartSuccess: false,
      isItemDelete: false,
    });
    expect(getRewardsDetails).toBeCalled();
  });

  it('should call clearError', () => {
    const simulateClick = tree.find('ErrorNotification').prop('onClear');
    simulateClick(event);
    expect(clearErrors).toBeCalled();
  });
});

