import { createSelector } from 'reselect';

export const myProfileAddressInfo = (state) => state.myProfileState;

export const getProfileStateInfo = createSelector(
  [myProfileAddressInfo],
  (myProfileState) => myProfileState.userInfo
);

export const getShippingAddress = createSelector(
  [getProfileStateInfo],
  (userInfo) => {
    if (userInfo) {
      const { contractorInfo } = userInfo;
      const { addresses } = contractorInfo || {};
      return addresses;
    }
    return [];
  },
);

export const getTrimedAddress = createSelector(
  [getShippingAddress],
  (addressDetails) => {
    const addrData = [];
    if (addressDetails) {
      addressDetails.map((addr) =>
        addrData.push((`${addr.address1}${addr.address2}${addr.city}${addr.stateOrProvince}${addr.zipOrPostal}${addr.country}`).replace(/\s/g, '')));
    }
    return addrData;
  }
);

export const getDuplicateAddressIndex = createSelector(
  [getTrimedAddress],
  (duplicateAddIndex) => {
    const data = [];
    if (duplicateAddIndex) {
      duplicateAddIndex.filter((value, index, array) => {
        if (array.indexOf(value) !== index) {
          data.push(index);
        }
        return data;
      });
    }
    return data;
  }
);

export const getUniqueShippingAddress = createSelector(
  [getDuplicateAddressIndex, getShippingAddress],
  (dupAddIndx, shippingAddress) => {
    if (dupAddIndx) {
      dupAddIndx.reverse().forEach((index) => {
        shippingAddress.splice(index, 1);
      });
    }
    return shippingAddress;
  }
);

