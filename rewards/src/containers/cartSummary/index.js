import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Grid, Row, Button } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import { getCartCount } from '../../actionCreators/Header';
import { constants as RedeemConstants } from '../redeemPoints/constants';
import { cartSummaryConstants } from './Constants';
import { pageTitle } from '../../constants/constants';
import { authentication } from '../../routes';
import { getUniqueShippingAddress } from './selector';
import { getGiftCardDetails } from '../../components/giftCardRedemption/selectors';
import { cartSummaryListtext } from '../../components/cartSummaryDebit/constants';
import { roundToTwoDecimal, formatCurrency, calculateFee, removeSpecialChar, convertStringToNumber, deviceOrientationChange, deviceOrientationClear } from '../../utils/utils';
import Loader from '../../components/loader';
import Dialog from '../../components/dialog';
import ErrorNotification from '../../components/errorNotification';
import OrderConfirmation from '../../components/orderConfirmation';
import CartSummaryBalance from '../../components/cartSummaryBalance';
import CartSummaryAddress from '../../components/cartSummaryAddress';
import CartSummaryList from '../../components/cartSummaryList';
import CartSummaryOrder from '../../components/cartSummaryOrder';
import * as contractorUserActionsCreators from '../../actionCreators/ContractorUser';
import * as redeemPointsActionsCreators from '../../actionCreators/RedeemPoints';
import * as cartSummaryActionCreators from '../../actionCreators/cartSummary';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import * as gtmActionsCreators from '../../actionCreators/GTM';

const TOAST_MESSAGE_TIMEOUT = 2000;

export class CartSummary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isSubmitOrder: false,
      isCloseToastMessage: false,
      isToastMessage: '',
      isShowDebitModal: false,
      isDisableOrderSubmit: '',
      cartInfo: {},
      cardValueAmount: {},
    };
    this.shippingAddress = null;
    this.nonReloadableArr = [];
    this.reloadableArr = [];
    this.deletedProductName = null;
    this.cardValueRatio = null;
    this.submitOrderTotal = null;
    this.submitOrderFees = null;
    this.orderProductValue = null;
    this.initialCartData = null;
    this.cardQuantity = null;
    this.isInitialCartData = false;
    this.debitCardValue = null;
    this.achValue = null;
    this.debitCardUsername = '';
    this.isReloadDebitCardAlert = false;
  }

  componentDidMount() {
    const {
      users,
      cartData,
      cartSummaryActions,
      redeemPointsActions,
      contractorUserActions,
    } = this.props;
    const { getGiftCatalogue } = redeemPointsActions;
    cartSummaryActions.getCartSummary();
    this.props.getCartCount();
    this.props.getProfileDetailsActions.getRewardsDetails();
    if (!users || users.length === 0) {
      contractorUserActions.getUsers();
    }
    getGiftCatalogue(RedeemConstants.TYPE.GIFT_CARDS, RedeemConstants.GIFT_CARDS);
    if (!cartData || Object.keys(cartData).length > 0) {
      this.updateCartState(cartData);
    }
    deviceOrientationChange();
  }

  componentDidUpdate(prevProps) {
    const {
      done,
      cartData,
      gtmActions,
      isItemDelete,
      isDisableOrder,
      isGetCartSuccess,
      isDebitCardAvail,
      cartSummaryActions,
      isCartSubmitAlready,
      submitCartSummaryError = {},
    } = this.props;

    if (JSON.stringify(prevProps.cartData) !== JSON.stringify(cartData)) {
      this.updateCartState(cartData);
    }

    if (prevProps.done !== done && done) {
      this.props.getProfileDetailsActions.getRewardsDetails();
      gtmActions.gtmCardValue(this.cardValueRatio);
      gtmActions.gtmCartQuantity(this.cardQuantity);
      gtmActions.gtmSubmitOrderTotal(this.submitOrderTotal);
      gtmActions.gtmSubmitOrderFeeTotal(this.submitOrderFees);
      gtmActions.gtmSubmitOrderProductTotal(this.orderProductValue);
      this.updateState('isSubmitOrder', true);
    }

    if (prevProps.isItemDelete !== isItemDelete && isItemDelete) {
      gtmActions.gtmCartDelete(this.deletedProductName);
      this.handleToastMessage('isToastMessage', cartSummaryConstants.DELETE_FINISHED);
      this.onUpdateDisableSubmit(isDisableOrder);
    }

    if (this.state.isCloseToastMessage) {
      setTimeout(() => {
        this.onCancelToastMessage();
      }, TOAST_MESSAGE_TIMEOUT);
    }

    if (!this.isInitialCartData && cartData.items && cartData.items.length > 0) {
      this.isInitialCartData = true;
      this.initialCartData = {
        ...cartData,
        items: [...cartData.items],
      };

      const { items } = this.initialCartData;
      items.forEach((itm) => {
        if (itm.category === cartSummaryConstants.DEBIT) {
          this.debitCardValue = itm.cardValue;
        } else if (itm.category === cartSummaryConstants.ACH_CARD) {
          this.achValue = itm.cardValue;
        }
      });
    }

    if (prevProps.isDebitCardAvail !== isDebitCardAvail && !isDebitCardAvail) {
      cartSummaryActions.getCartSummary();
      this.isReloadDebitCardAlert = true;
    }

    if (prevProps.isGetCartSuccess !== isGetCartSuccess && isGetCartSuccess && this.isReloadDebitCardAlert) {
      const data = { ...cartData };
      const items = [...data.items];
      this.debitCardIssuedCheck(items);
      cartSummaryActions.cartInitialState();
      if (this.nonReloadableArr.length > 0) {
        this.showDebitReloadModal();
      }
      this.isReloadDebitCardAlert = false;
    }

    if (prevProps.isCartSubmitAlready !== isCartSubmitAlready && isCartSubmitAlready) {
      if (Object.keys(submitCartSummaryError).length > 0) {
        const { errors = [] } = submitCartSummaryError;
        if (errors.length > 0) {
          errors.forEach((itm) => {
            if (itm.code === cartSummaryConstants.CART_ALREADY_SUBMITTED_CODE) {
              cartSummaryActions.cartInitialState();
              this.props.getCartCount();
              cartSummaryActions.getCartSummary();
            }
          });
        }
      }
    }
  }

  componentWillUnmount() {
    const { cartSummaryActions } = this.props;
    cartSummaryActions.cartResetData();
    deviceOrientationClear();
  }

  onCancelToastMessage = () => {
    this.setState({
      isCloseToastMessage: false,
    });
  }

  onCancelDebitReloadModal = (e) => {
    e.preventDefault();
    this.setState({
      isShowDebitModal: false,
    });
  }

  onUpdateDisableSubmit(value) {
    this.setState({
      isDisableOrderSubmit: value,
    });
  }

  onUpdateShippingAddress(val) {
    const { gtmActions } = this.props;
    gtmActions.gtmUpdateShippingAddress(val);
  }

  redemptionOrderDetails(val) {
    const { gtmActions } = this.props;
    gtmActions.gtmRedemptionDetails(val);
  }

  updateCartState = (cartData) => {
    const cartlist = { ...cartData };
    this.setState({
      cartInfo: cartlist,
    });
  }

  showDebitReloadModal = () => {
    this.setState({
      isShowDebitModal: true,
    });
  }

  updateState = (key, value) => {
    this.setState({ [key]: value });
    this.orderProductValue = null;
    this.submitOrderFees = null;
    this.submitOrderTotal = null;
    this.cardQuantity = null;
    this.cardValueRatio = null;
  }

  /** @description callback function for clear error
   * @param {object} e - triggered event(click)
   * @param {func} action - action for clear error
   */
  clearError = (e, action) => {
    e.preventDefault();
    action();
  }

  /** @description function to remove image url from request body
   * @param {object} cardData
   */
  removeCardImageFromRequest = (cardData) => {
    const data = { ...cardData };
    const items = [...data.items].map((list) => ({ ...list, img: null }));
    return {
      ...data,
      items,
    };
  }

  /**
   * Dispatch action for updating the cart item value
   * @param updatedItem
   */

  handleItemUpdate = (updatedItem) => {
    const { cartInfo } = this.state;
    const { cartSummaryActions, userInfo } = this.props;

    const { contractorId, email } = authentication.getAuthDetails();
    const { firstName, lastName } = userInfo;
    const cartItem = { ...cartInfo.items[updatedItem.index] };
    const cartObj = { ...cartInfo };

    if (updatedItem.value) {
      cartItem.totalValue = Number(updatedItem.value);
    }

    if (updatedItem.unitValue) {
      const amount = (/(\d{0,})[^.]*((?:\.\d{0,2})?)/g).exec(updatedItem.unitValue.toString().replace(/[^\d.]/g, ''));
      cartItem.cardValue = amount[1] + amount[2];
    }

    if (updatedItem.giftCardValue) {
      cartItem.cardValue = updatedItem.giftCardValue;
    }

    if (updatedItem.quantity) {
      cartItem.quantity = updatedItem.quantity;
    }

    if (updatedItem.feeValue) {
      cartItem.feeValue = Number(roundToTwoDecimal(updatedItem.feeValue));
    }

    if (updatedItem.variantId) {
      cartItem.variantId = updatedItem.variantId;
    }

    cartItem.isModified = true;
    cartItem.modifiedBy = email;
    cartObj.shippingAddress = this.shippingAddress;
    cartObj.items[updatedItem.index] = cartItem;

    cartObj.itemTotal = Number(this.computeItemTotal(cartObj, cartSummaryConstants.CARDVALUE));
    cartObj.feeTotal = Number(this.computeFeeTotal(cartObj));

    cartObj.orderTotal = (Number(roundToTwoDecimal(cartObj.itemTotal)) + Number(roundToTwoDecimal(cartObj.feeTotal)));

    cartObj.contractorId = contractorId;
    cartObj.firstName = firstName;
    cartObj.lastName = lastName;
    cartObj.modifiedBy = email;
    cartObj.userId = email;
    cartObj.createdBy = email;

    this.updateCartState(cartObj);

    if (cartItem.isAvailable && updatedItem.isItemOrQuantityUpdate) {
      const reqBody = this.removeCardImageFromRequest(cartObj);

      if (cartItem.category === cartSummaryConstants.DEBIT) {
        this.debitCardValue = removeSpecialChar(updatedItem.unitValue);
      } else if (cartItem.category === cartSummaryConstants.ACH_CARD) {
        this.achValue = removeSpecialChar(updatedItem.unitValue);
      }
      const items = this.checkCardValueRange(reqBody);
      cartSummaryActions.updateCartSummary(items);
    }
  }

  checkCardValueRange = (reqBody) => {
    const data = { ...reqBody };
    const cardItem = [...data.items].map((item) => {
      const isCardValueFallOnRange = item.min <= convertStringToNumber(item.cardValue) && convertStringToNumber(item.cardValue) <= item.max;
      if (item.category === cartSummaryConstants.DEBIT) {
        return {
          ...item,
          cardValue: isCardValueFallOnRange ? convertStringToNumber(item.cardValue) : this.debitCardValue,
        };
      } else if (item.category === cartSummaryConstants.ACH_CARD) {
        return {
          ...item,
          cardValue: isCardValueFallOnRange ? convertStringToNumber(item.cardValue) : this.achValue,
        };
      }
      return item;
    });
    return {
      ...data,
      items: cardItem,
    };
  }

  handleOrderSubmit = (insufficientBalance) => {
    const { cartInfo } = this.state;
    const { cartSummaryActions, cartData } = this.props;
    const cardTotal = cartData.itemTotal;
    const cartobj = { ...cartInfo };
    const { items } = cartobj;
    const initialCartItemsData = this.initialCartData;
    const isAvailableCheck = this.handleDisableCheckout();
    this.submitOrderTotal = cartobj.orderTotal;
    this.submitOrderFees = cartobj.feeTotal;
    this.orderProductValue = cartobj.itemTotal;

    const intialQuantity = initialCartItemsData.items.reduce((a, b) => ({ quantity: a.quantity + b.quantity }));
    const updatedQuantity = cartobj.items.reduce((a, b) => ({ quantity: a.quantity + b.quantity }));

    if (intialQuantity.quantity === updatedQuantity.quantity) {
      this.cardQuantity = cartSummaryConstants.CARDVALUE_SAME;
    } else {
      this.cardQuantity = `${intialQuantity.quantity > updatedQuantity.quantity ? cartSummaryConstants.CARDVALUE_DECREASED : cartSummaryConstants.CARDVALUE_INCREASED}`;
    }

    if (cardTotal === cartobj.itemTotal) {
      this.cardValueRatio = cartSummaryConstants.CARDVALUE_SAME;
    } else {
      this.cardValueRatio = `${cardTotal > cartobj.itemTotal ? cartSummaryConstants.CARDVALUE_DECREASED : cartSummaryConstants.CARDVALUE_INCREASED}`;
    }
    this.debitCardIssuedCheck(items);
    const cartList = {
      ...cartobj,
      shippingAddress: this.shippingAddress,
    };

    if (this.nonReloadableArr.length > 0) {
      this.showDebitReloadModal();
    } else if (!insufficientBalance || items.length !== 0 || isAvailableCheck.length === 0) {
      const reqBody = this.removeCardImageFromRequest(cartList);
      cartSummaryActions.submitCartSummary(reqBody);
    }
  }

  debitCardIssuedCheck = (items) => {
    this.nonReloadableArr = [];
    this.reloadableArr = [];
    items.forEach((item) => {
      if (item.category === cartSummaryListtext.DEBIT && !item.isReloadableDebitCard && item.debitCardIssuedTo) {
        this.nonReloadableArr.push(item);
        this.findDebitCardUserName(item.debitCardIssuedTo);
      } else {
        this.reloadableArr.push(item);
      }
    });
  }

  findDebitCardUserName = (email) => {
    const userName = this.props.users.find((usr) => usr.emailAddress === email);
    if (userName) {
      this.debitCardUsername = `${userName.firstName} ${userName.lastName}`;
    }
  }

  handleDebitReload = () => {
    const { cartInfo } = this.state;
    const { cartSummaryActions } = this.props;
    const cartObj = { ...cartInfo };

    this.nonReloadableArr.forEach((part, index) => {
      this.nonReloadableArr[index].isReloadableDebitCard = true;
    });
    const item = [...this.nonReloadableArr, ...this.reloadableArr];
    cartObj.items = item;
    const reqBody = this.removeCardImageFromRequest(cartObj);
    cartSummaryActions.updateCartSummary(reqBody);
    this.setState({
      isShowDebitModal: false,
    });
  }

  handleDebitRemoveItem = () => {
    const { cartInfo } = this.state;
    const { cartSummaryActions } = this.props;
    const cartObj = { ...cartInfo };

    const item = [...this.reloadableArr];
    cartObj.items = item;
    const reqBody = this.removeCardImageFromRequest(cartObj);
    cartSummaryActions.deleteCartSummary(reqBody);
    this.setState({
      isShowDebitModal: false,
    });
  }

  computeItemTotal = (updatedCartData, key) => {
    const cartData = [];
    const cartListitem = updatedCartData.items;

    cartListitem.forEach((item) => {
      if (item.isAvailable) {
        cartData.push(item[key] * item.quantity);
      }
    });
    const Totals = cartData.map(roundToTwoDecimal);
    return Totals.reduce((sum, val) => parseFloat(sum) + parseFloat(val), 0).toFixed(2);
  }

  computeFeeTotal = (updatedCartData) => {
    const cartListitem = updatedCartData.items;
    let feeTotal = 0;

    cartListitem.forEach((item) => {
      let fee = 0;
      if (item.category === cartSummaryListtext.GIFT_CARD) {
        fee = calculateFee(item.quantity, item.feeRate, item.feeType);
      } else {
        fee = calculateFee(item.cardValue, item.feeRate, item.feeType, Number(`${!item.debitCardIssuedTo ? 3 : 0}`));
      }
      feeTotal += Number(fee);
    });
    return feeTotal;
  }

  /**
   * Dispatch action to delete cart item
   *
   * @param  index
   *
   */
  handleDeleteItem = (index, productName) => {
    const {
      cartSummaryActions,
    } = this.props;

    const { cartInfo } = this.state;
    const data = {
      ...cartInfo,
      items: [...cartInfo.items],
    };

    const reqBody = this.removeCardImageFromRequest(data);
    const checkCardRange = this.checkCardValueRange(reqBody);
    checkCardRange.items.splice(index, 1);
    cartSummaryActions.deleteCartSummary(checkCardRange);
    this.deletedProductName = productName;
  }

  handleToastMessage(key, value) {
    const { cartSummaryActions } = this.props;
    this.setState({
      isCloseToastMessage: true,
      [key]: value,
    });
    this.deletedProductName = null;
    cartSummaryActions.cartInitialState();
  }

  handleInitiallAddress = (addressData) => {
    const checkedAddress = addressData.map((item) => item.addressType);
    this.shippingAddress = checkedAddress.indexOf('1') > -1 ? addressData[checkedAddress.indexOf('1')] : null;
    return checkedAddress.indexOf('1');
  }

  disableRadioFlag = () => {
    const { cartData } = this.props;
    const { items } = cartData;
    let radioFlag = false;
    if (items) {
      items.forEach((item) => {
        if (item.category === cartSummaryListtext.DEBIT && !item.debitCardIssuedTo) {
          radioFlag = true;
        }
      });
    }
    return radioFlag;
  }

  handleSelectedAddress = (obj) => {
    this.shippingAddress = obj;
  }

  handleDisableCheckout = () => {
    const { cartData } = this.props;
    const isAvailable = [];
    const { items } = cartData;

    if (items) {
      items.forEach((item) => {
        if (!item.isAvailable) {
          isAvailable.push(item.isAvailable);
        }
      });
    }
    return isAvailable;
  }

  calculateTotal = () => {
    const { cartInfo } = this.state;
    const { rewardsInfo } = this.props;
    const cartObj = { ...cartInfo };
    const { items } = cartObj;

    let productTotal = 0;
    let feeTotal = 0;
    let orderTotal = 0;
    let balance = 0;
    if (cartObj && items) {
      items.forEach((item) => {
        let fee = 0;
        if (item.category === cartSummaryListtext.GIFT_CARD) {
          fee = calculateFee(item.quantity, item.feeRate, item.feeType);
        } else {
          fee = calculateFee(item.cardValue, item.feeRate, item.feeType, Number(`${!item.debitCardIssuedTo ? 3 : 0}`));
        }
        const cardValue = (item.cardValue * item.quantity);
        productTotal += cardValue;
        feeTotal += Number(fee);
        orderTotal += cardValue + item.feeValue;
      });
      balance += Number(rewardsInfo.rewardPoints) - orderTotal;
    }
    return {
      productTotal,
      feeTotal,
      orderTotal,
      balance,
    };
  }

  handleQuantityInput = (inputObj) => {
    const {
      qty,
      action,
      index,
      feeRate,
      unitValue,
    } = inputObj;
    const updatedItem = {};
    let quantity = qty;

    if (quantity === 1 && action === cartSummaryConstants.DECREMENT) {
      return false;
    }

    if (action === cartSummaryConstants.INCREMENT) {
      quantity += 1;
    } else {
      quantity -= 1;
    }

    updatedItem.quantity = quantity;
    updatedItem.value = roundToTwoDecimal(quantity * unitValue);
    updatedItem.feeValue = roundToTwoDecimal(quantity * feeRate);
    updatedItem.index = index;
    updatedItem.isItemOrQuantityUpdate = true;
    this.handleItemUpdate(updatedItem);
    return null;
  }

  listItemSelectHandler = (selectObj) => {
    const {
      val,
      qty,
      index,
    } = selectObj;
    const updatedItem = {};
    updatedItem.value = roundToTwoDecimal(qty * Number(val));
    updatedItem.giftCardValue = Number(val);
    updatedItem.index = index;
    updatedItem.isItemOrQuantityUpdate = true;
    updatedItem.variantId = selectObj.selectedVariantId;
    this.handleItemUpdate(updatedItem);
    return null;
  }

  selectedCardValue(amtObj) {
    const { cardValueAmount } = this.state;
    this.setState({
      cardValueAmount: {
        ...cardValueAmount,
        [amtObj.productId]: amtObj.amount,
        variant: {
          ...cardValueAmount.variant,
          [amtObj.productId]: amtObj.variantId,
        },
      },
    });
  }

  /** @description function to render the error message for cartSummary */
  renderErrorMessage = () => {
    const {
      cartDeleteFailure,
      cartSummaryActions,
      getCartSummaryError,
      submitCartSummaryError,
      updateCartSummaryError,
    } = this.props;
    const props = {
      error: cartDeleteFailure || submitCartSummaryError || updateCartSummaryError || getCartSummaryError,
      onClear: (e) => this.clearError(e, cartSummaryActions.clearError),
    };
    return (<ErrorNotification {...props} />);
  }

  renderReloadModalFooter() {
    return (
      <div className="cart-footer">
        <Button className="gaf-btn-primary" onClick={() => this.handleDebitReload()}>{cartSummaryListtext.OK}</Button>
        <Button className="gaf-btn-secondary" onClick={() => this.handleDebitRemoveItem()}>{cartSummaryListtext.CANCEL}</Button>
      </div>
    );
  }

  renderCartSummaryList() {
    const { cartInfo, cardValueAmount } = this.state;
    const cartObj = { ...cartInfo };
    const { items } = cartObj;
    const {
      cartData,
      giftCards,
      redeemPointsActions,
      giftCardDetails,
    } = this.props;
    return (
      <CartSummaryList
        cartlist={cartData.items}
        giftCardList={items || {}}
        giftCards={giftCards}
        cardDetails={giftCardDetails}
        action={redeemPointsActions}
        cardAmount={cardValueAmount}
        handleUpdate={(updatedObj) => this.handleItemUpdate(updatedObj)}
        handleDelete={(index, productName) => this.handleDeleteItem(index, productName)}
        updateCardAmount={(amtObj) => this.selectedCardValue(amtObj)}
        onDisableOrderSubmit={(value) => this.onUpdateDisableSubmit(value)}
        handleItemSelectHandler={(selectObj) => this.listItemSelectHandler(selectObj)}
        handleQuantityInputValue={(inputObj) => this.handleQuantityInput(inputObj)}
      />
    );
  }

  renderAddressComponent() {
    const { addressData } = this.props;
    return (
      <CartSummaryAddress
        addressData={addressData}
        isCheck={this.handleInitiallAddress(addressData)}
        disableRadio={this.disableRadioFlag()}
        selectShippingAddress={(obj) => this.handleSelectedAddress(obj)}
        updateShippingAddress={(val) => this.onUpdateShippingAddress(val)}
      />
    );
  }

  renderOrderSummaryComponent() {
    const { isDisableOrderSubmit } = this.state;
    const {
      cartData,
      rewardsInfo,
    } = this.props;
    const {
      productTotal,
      feeTotal,
      orderTotal,
      balance,
    } = this.calculateTotal();
    return (
      <CartSummaryOrder
        cartDataItems={cartData.items}
        orderTotal={formatCurrency(roundToTwoDecimal(orderTotal))}
        feeTotal={formatCurrency(roundToTwoDecimal(feeTotal))}
        productTotal={formatCurrency(roundToTwoDecimal(productTotal))}
        availBalance={roundToTwoDecimal(Number(balance))}
        handleCheckout={this.handleDisableCheckout()}
        handleOrderSubmit={(value) => this.handleOrderSubmit(value)}
        rewardsPoints={roundToTwoDecimal(rewardsInfo.rewardPoints)}
        onDisableOrder={isDisableOrderSubmit}
      />
    );
  }

  renderEmptyCart() {
    return (
      <div className="empty-message">
        <h3>{cartSummaryConstants.EMPTY_CART}</h3>
      </div>
    );
  }

  renderModal = () => {
    const { isCloseToastMessage } = this.state;
    const toastBody = (
      <div className="gaf-toast-dialog">
        <span className="icon-check"></span>
        <p>{this.state.isToastMessage}</p>
      </div>);
    return (
      <Dialog
        show={isCloseToastMessage}
        body={toastBody}
        className="gaf-model-popup cart-summary-toast-modal gaf-toast-main"
      />
    );
  }


  render() {
    const { isSubmitOrder, isShowDebitModal } = this.state;
    const {
      cartData,
      giftCards,
      isLoading,
      orderNumber,
      rewardsInfo,
    } = this.props;
    const balance = formatCurrency(roundToTwoDecimal(rewardsInfo.rewardPoints));
    const availbleBalance = balance || roundToTwoDecimal(0);
    return (
      <main className="main-content cart-summary" id="cart-summary-main-content">
        <Helmet>
          <title>{pageTitle.CARTSUMMARY}</title>
        </Helmet>
        <Dialog
          show={isShowDebitModal}
          title={cartSummaryListtext.TITLE}
          body={cartSummaryConstants.RELOAD_MESSAGE.replace('{{name}}', this.debitCardUsername)}
          onCloseClick={this.onCancelDebitReloadModal}
          footer={this.renderReloadModalFooter()}
          className="gaf-model-popup cart-summary-debit-alert"
        />
        <section>
          {isSubmitOrder ?
            <OrderConfirmation
              orderNumber={orderNumber}
              orderReceived={(val) => this.redemptionOrderDetails(val)}
            /> :
            <Grid>
              <CartSummaryBalance
                balance={availbleBalance}
              />
              <div className="cart-main">
                {cartData.items && giftCards && this.renderCartSummaryList()}
                {(cartData.items && cartData.items.length > 0) ?
                  <Row className="show-grid">
                    {this.renderAddressComponent()}
                    {this.renderOrderSummaryComponent()}
                  </Row>
                  :
                  !isLoading &&
                  <Row className="show-grid">
                    {this.renderEmptyCart()}
                  </Row>
                }
              </div>
            </Grid>}
          {isLoading && <Loader />}
          {this.renderErrorMessage()}
        </section>
        {this.renderModal()}
      </main>
    );
  }
}

function mapStateToProps(state) {
  const {
    cartSummaryState,
    myProfileState,
    redeemPointsState,
    contractorUserState,
  } = state;
  const { users } = contractorUserState;
  const { userInfo, rewardsInfo } = myProfileState;
  const { giftCards } = redeemPointsState;
  const {
    isLoading,
    done,
    getCartSummaryError,
    submitCartSummaryError,
    updateCartSummaryError,
    cartSummaryData,
    orderNumber,
    isItemDelete,
    isDisableOrder,
    cartDeleteFailure,
    isDebitCardAvail,
    isCartSubmitAlready,
    isGetCartSuccess,
  } = cartSummaryState;
  return {
    giftCards,
    giftCardDetails: getGiftCardDetails(state),
    cartData: cartSummaryData || {},
    isLoading,
    cartDeleteFailure,
    userInfo,
    rewardsInfo,
    addressData: getUniqueShippingAddress(state),
    orderNumber,
    isItemDelete,
    isDisableOrder,
    done,
    users,
    getCartSummaryError,
    isGetCartSuccess,
    isDebitCardAvail,
    isCartSubmitAlready,
    submitCartSummaryError,
    updateCartSummaryError,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getCartCount: bindActionCreators(getCartCount, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
    getProfileDetailsActions: bindActionCreators(getProfileDetails, dispatch),
    cartSummaryActions: bindActionCreators(cartSummaryActionCreators, dispatch),
    redeemPointsActions: bindActionCreators(redeemPointsActionsCreators, dispatch),
    contractorUserActions: bindActionCreators(contractorUserActionsCreators, dispatch),
  };
}

CartSummary.propTypes = {
  done: PropTypes.bool,
  users: PropTypes.array,
  isLoading: PropTypes.bool,
  userInfo: PropTypes.object,
  giftCards: PropTypes.array,
  cartData: PropTypes.object,
  addressData: PropTypes.array,
  isItemDelete: PropTypes.bool,
  rewardsInfo: PropTypes.object,
  orderNumber: PropTypes.string,
  isDisableOrder: PropTypes.bool,
  isDebitCardAvail: PropTypes.bool,
  isGetCartSuccess: PropTypes.bool,
  cartDeleteFailure: PropTypes.any,
  shippingAddress: PropTypes.array,
  giftCardDetails: PropTypes.object,
  getCartSummaryError: PropTypes.any,
  isCartSubmitAlready: PropTypes.bool,
  submitCartSummaryError: PropTypes.any,
  updateCartSummaryError: PropTypes.any,
  getCartCount: PropTypes.func.isRequired,
  gtmActions: PropTypes.object.isRequired,
  contractorUserActions: PropTypes.object.isRequired,
  cartSummaryActions: PropTypes.object.isRequired,
  redeemPointsActions: PropTypes.object.isRequired,
  getProfileDetailsActions: PropTypes.object.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartSummary);
