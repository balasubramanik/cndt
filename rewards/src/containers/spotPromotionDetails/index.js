import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Grid, Row, Col } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import RouteConstants from '../../constants/RouteConstants';
import { history, authorization } from '../../routes';
import { getDetails, getDetailsClear } from '../../actionCreators/SpotPromotionDetails';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import ImageBanner from '../../components/imageBanner';
import Dialog from '../../components/dialog';
import { details } from './constants';
import { pageTitle } from '../../constants/constants';
import RoleAccessPopup from '../../components/roleAccessPopup';
import Loader from '../../components/loader';
import { updateSpotPromotionPlanID, updateSpotPromotionSelectedYear } from '../../actionCreators/WhatQualifies';

/** @description Class component to render the Spot Promotion Details */
export class SpotPromotionDetails extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isViewMore: true,
    };
  }

  componentDidMount() {
    const { match, location } = this.props;
    const { planId } = match.params;
    const { state } = location;
    const contractorId = state ? state.contractorId : null;
    this.props.getDetails(planId, contractorId);
  }

  componentWillUnmount() {
    this.props.getDetailsClear();
  }

  /** @description Callback function for view more button
   * @param {object} e - triggered event(click)
   */
  viewMoreHandler = (e) => {
    e.preventDefault();
    const { isViewMore } = this.state;
    this.setState({ isViewMore: !isViewMore });
  }

  clickHandler = (e) => {
    const { promotionsInfo } = this.props;
    e.preventDefault();
    if (authorization.userRolesAccess && authorization.checkAccess(RouteConstants.SUBMIT_INVOICE)) {
      history.push({
        pathname: RouteConstants.SUBMIT_INVOICE,
      });
    } else {
      this.props.gtmActions.gtmSubmitInvoiceLink(promotionsInfo.title);
      this.props.actions.accessPage(true);
    }
  };

  handleClose(e) {
    e.preventDefault();
    history.push('/');
  }

  approvePopupContent = () => (
    <div className="gaf-toast-dialog error-dialog">
      <span className="icon-alert-danger"></span>
      <p>{details.label.ACCESS_DENIED}</p>
    </div>
  );


  render() {
    const { isViewMore } = this.state;
    const {
      promotionsInfo,
      isLoading,
      selectedYear,
      accessInfo,
    } = this.props;
    const viewMoreClass = classNames('gaf-description-block s-p-detail', { 'view-less-content': isViewMore, 'view-more-content': !isViewMore });
    return (
      <div>
        <main className="spot-promotion-details main-content">
          <Helmet>
            <title>{pageTitle.SPOTPROMOTIONDETAILS}</title>
          </Helmet>
          <Grid>
            <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={this.props.actions.accessPage} />
            <Row>
              <Col xs={12}>
                {promotionsInfo &&
                  <ImageBanner
                    selectedYear={selectedYear}
                    updateSelectedYear={this.props.updateSpotPromotionSelectedYear}
                    updatePlanId={this.props.updateSpotPromotionPlanID}
                    promotionsInfo={promotionsInfo}
                    gtmActions={this.props.gtmActions}
                    isLoading={isLoading}
                  />
                }
              </Col>
              <Col xs={12}>
                <div className={viewMoreClass}>
                  <Row>
                    <Col xs={12}>
                      <h4>{details.label.header}</h4>
                      <div className="gaf-paragraph">
                        {promotionsInfo && <p>{promotionsInfo.description}</p>}
                      </div>
                      <div className="view-more visible-xs">
                        <a href="/" onClick={this.viewMoreHandler}>
                          {isViewMore ? details.label.VIEW_MORE : details.label.HIDE_MORE}
                          <i className="icon-down-arrow-circle" />
                        </a>
                      </div>
                    </Col>
                  </Row>
                </div>
              </Col>
              {!promotionsInfo &&
                <Dialog
                  show
                  onCloseClick={(e) => this.handleClose(e)}
                  body={this.approvePopupContent()}
                  className="gaf-model-popup gaf-toast-main"
                />
              }
              {!authorization.hasAdminAccess ?
                <Col xs={12}>
                  <div className="gaf-description-block">
                    <Row>
                      <Col md={9} sm={12} xs={12}>
                        <p>{details.t1.text}</p>
                      </Col>
                      <Col md={3} sm={12} xs={12}>
                        <button className="btn gaf-btn-primary btn-block" onClick={this.clickHandler}>{details.label.submitInvoice}</button>
                      </Col>
                    </Row>
                  </div>
                </Col> : null}
              <Col xs={12}>
                <div className="gaf-description-block gaf-paragraph">
                  {promotionsInfo && <p>{details.t2.text}{promotionsInfo.disclaimer}</p>}
                </div>
              </Col>
            </Row>
          </Grid>
          {isLoading && <Loader />}
        </main>
      </div>
    );
  }
}

function mapStateToProps(state, match) {
  const { spotPromotionDetailsState, whatQualifiesState, myProfileState } = state;
  const { selectedYear } = whatQualifiesState;
  const { accessInfo } = myProfileState;
  const { promotionsInfo, isLoading } = spotPromotionDetailsState;
  return {
    promotionsInfo,
    isLoading,
    match,
    selectedYear,
    accessInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getDetails: bindActionCreators(getDetails, dispatch),
    actions: bindActionCreators(getProfileDetails, dispatch),
    getDetailsClear: bindActionCreators(getDetailsClear, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
    updateSpotPromotionPlanID: bindActionCreators(updateSpotPromotionPlanID, dispatch),
    updateSpotPromotionSelectedYear: bindActionCreators(updateSpotPromotionSelectedYear, dispatch),
  };
}

/** PropTypes:
 * promotionsInfo - data provided for the spot promotion details page
 * getDetails - replace with the services
 */

SpotPromotionDetails.propTypes = {
  promotionsInfo: PropTypes.object.isRequired,
  gtmActions: PropTypes.func,
  getDetails: PropTypes.func.isRequired,
  getDetailsClear: PropTypes.func,
  updateSpotPromotionPlanID: PropTypes.func,
  updateSpotPromotionSelectedYear: PropTypes.func,
  isLoading: PropTypes.bool,
  match: PropTypes.object,
  selectedYear: PropTypes.number,
  accessInfo: PropTypes.bool,
  actions: PropTypes.object,
  location: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SpotPromotionDetails));
