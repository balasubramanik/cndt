import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import SpotPromotionDetailsCont, { SpotPromotionDetails } from '../../containers/whatQualifies';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();
window.open = jest.fn();
describe('SpotPromotionDetails', () => {
  const event = {
    preventDefault: jest.fn(),
  };
  const getDetails = jest.fn();
  const tree = shallow(<SpotPromotionDetails
    getCategory={getDetails}
    promotionsInfo={[{
      startDate: '2018-08-01T16:57:19.0487212+05:30',
      endDate: '2018-08-11T16:57:19.0487212+05:30',
      submissionDueDate: '2018-08-11T16:57:19.0487212+05:30',
      states: ['NM', 'NY'],
      headerDesc: 'Timberline Ultra HD Promotion',
      image: 'images/banner-image.jpg',
    }]}
  />);
  it('should be defined', () => {
    expect(SpotPromotionDetails).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render SpotPromotionDetails container', () => {
    expect(tree.find('.spot-promotion-details main-content').length).toBe(1);
  });

  it('should render Image Banner component', () => {
    expect(tree.find('ImageBanner').length).toBe(1);
  });

  it('should call openPopup', () => {
    const simulateClick = tree.find('ImageBanner').prop('onClick');
    simulateClick(event);
  });

  it('should call closePopUp', () => {
    const simulateClick = tree.find('ImageBanner').prop('onCloseClick');
    simulateClick(event);
  });

  it('should call redirectToSpotPromotions', () => {
    const simulateClick = tree.find('ImageBanner').prop('onClick');
    simulateClick(event);
  });

  it('should call unmount', () => {
    tree.unmount();
  });
});

describe('SpotPromotionDetails with connected component', () => {
  window.open = jest.fn();
  const state = {
    getDetails: jest.fn(),
  };

  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <SpotPromotionDetailsCont />
      </Router>
    </Provider>);

  it('should be defined', () => {
    expect(SpotPromotionDetailsCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render ImageBanner component', () => {
    expect(tree.find('ImageBanner').length).toBe(1);
  });
});
