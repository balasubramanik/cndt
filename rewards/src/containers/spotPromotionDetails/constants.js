const details = {
  t1:
      { text: 'You only have to submit your qualifying invoices once! Earn the correct amount of GAF Reward Points for the current promotions for which the invoice qualifies.' },
  t2:
      { text: 'Additional Details: ' },

  label: {
    header: 'Details',
    submitInvoice: 'SUBMIT INVOICES',
    VIEW_MORE: 'View More',
    HIDE_MORE: 'Hide More',
    ACCESS_DENIED: 'Sorry! This promotion is not available in your area. Check out "What Qualifies" to view your current available promos.',
  },
};

export { details };
