import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import HeaderCont, { HeaderContainer } from '../../containers/header';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('HeaderContainer', () => {
  window.open = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const getProfileDetails = jest.fn();
  const tree = shallow(<HeaderContainer
    getProfileDetails={getProfileDetails}
  />);
  it('should be defined', () => {
    expect(HeaderContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render header', () => {
    expect(tree.find('header').length).toBe(1);
  });

  it('should render TopHeader', () => {
    expect(tree.find('TopHeader').length).toBe(1);
  });

  it('should call viewDetailsHandler', () => {
    const simulateClick = tree.find('TopHeader').prop('onViewDetailsClick');
    simulateClick(event);
    expect(history.push).toBeCalled();
  });

  it('should call profileHandler', () => {
    const simulateClick = tree.find('TopHeader').prop('onProfileClick');
    simulateClick(event, true);
    expect(tree.state().isShowOverlay).toBe(true);
  });

  it('should call signOutHandler', () => {
    const simulateClick = tree.find('TopHeader').prop('onSignOutClick');
    simulateClick();
  });

  it('should call cartHandler', () => {
    const simulateClick = tree.find('TopHeader').prop('onCartClick');
    simulateClick(event);
    // TODO: Assertion will add, Once the functionality is done.
  });

  it('should call myaccountHandler', () => {
    const simulateClick = tree.find('TopHeader').prop('onMyAccountClick');
    simulateClick();
    expect(window.open).toBeCalled();
  });

  it('should render HeaderMenu', () => {
    expect(tree.find('HeaderMenu').length).toBe(1);
  });

  it('should call navigationHandler with internal link', () => {
    const simulateClick = tree.find('HeaderMenu').prop('onNavigationClick');
    simulateClick(event, { url: '/dashboard', isExternal: false });
    expect(history.push).toBeCalled();
  });

  it('should call navigationHandler with external link', () => {
    const simulateClick = tree.find('HeaderMenu').prop('onNavigationClick');
    simulateClick(event, { url: 'http://test.com', isExternal: true });
    expect(window.open).toBeCalled();
  });

  // TODO: Below case will be uncommented, Once the functionality is done.

  // it('should render IdleTimePopup', () => {
  //   expect(tree.find('IdleTimePopup').length).toBe(1);
  // });

  // it('should call logoutHandler', () => {
  //   const simulateClick = tree.find('IdleTimePopup').prop('onLogout');
  //   simulateClick();
  //   // TODO: Assertion will add, Once the functionality is done.
  // });

  // it('should call redirectToGafRewards', () => {
  //   const simulateClick = tree.find('IdleTimePopup').prop('onConfirmClick');
  //   simulateClick();
  //   expect(window.open).toBeCalled();
  // });

  it('should call documentHandler', () => {
    tree.setState({ isShowOverlay: true });
    const e = {
      target: {
        closest: () => false,
      },
    };
    const treeInstance = tree.instance();
    treeInstance.documentHandler(e);
    expect(tree.state().isShowOverlay).toBe(false);
  });

  it('should render Dialog', () => {
    expect(tree.find('Dialog').length).toBe(1);
  });

  it('should call closePopupHandler', () => {
    const simulateClick = tree.find('Dialog').prop('onCloseClick');
    simulateClick();
    expect(tree.state().isShowLogoutDialog).toBe(false);
  });

  it('should call unmount', () => {
    tree.unmount();
  });
});

describe('HeaderContainer with connected component', () => {
  window.open = jest.fn();
  const state = {
    headerState: {
      cartItemsCount: 1,
    },
    myProfileState: {
      userInfo: {
        email: 'Brain@gaf.com',
        firstName: 'Brain',
        lastName: 'Granger',
        contractorAccountNumber: '1234567890',
        contractorCertificationNumber: 'ME08560',
        contractInfo: {
          contractorName: 'Queens Roofing and Tinsmithing Inc',
          contractorImage: 'tinsmiths.jpg',
        },
      },
      rewardsInfo: {
        rewardsPoint: '2500',
        expiryDate: '2018-08-09T16:57:19.0487212+05:30',
      },
    },
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <HeaderCont />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(HeaderCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render header', () => {
    expect(tree.find('header').length).toBe(1);
  });

  it('should render TopHeader', () => {
    expect(tree.find('.header-top-logo-nav').length).toBe(1);
  });

  it('should render HeaderMenu', () => {
    expect(tree.find('.navbar-section').length).toBe(1);
  });
});
