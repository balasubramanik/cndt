import envConfig from 'envConfig'; //eslint-disable-line
import RouteConstants from '../../constants/RouteConstants';
const links = {
  rewardsPageUrl: '/rewards',
  cczMyAccountUrl: '/CompanyProfile/home',
};

const header = {
  navlinks: {
    contractor: [
      { title: 'Home', url: '/', isExternal: false },
      { title: 'What Qualifies', url: RouteConstants.WHAT_QUALIFIES, isExternal: false },
      { title: 'Submit Invoices', url: RouteConstants.SUBMIT_INVOICE, isExternal: false },
      { title: 'Redeem Points', url: RouteConstants.REDEEM_POINTS, isExternal: false },
      { title: 'Claim Status', url: RouteConstants.CLAIM_STATUS, isExternal: false },
      { title: 'Points History', url: RouteConstants.POINTS_HISTORY, isExternal: false },
      { title: 'Contractor Zone', url: `${envConfig.cczBaseUrl}`, isExternal: true },
    ],
    admin: [
      { title: 'Contractor Management', url: RouteConstants.CONTRACTORMANAGEMENT, isExternal: false },
      { title: 'Spot Promotion', url: RouteConstants.SPOTPROMOTION, isExternal: false },
      { title: 'Contractor Zone', url: `${envConfig.cczBaseUrl}`, isExternal: true },
    ],
  },
  labels: {
    logoutConfirmation: 'Are you sure you want to logout?',
    yes: 'Yes',
    no: 'No',
  },
};

export { header, links };
