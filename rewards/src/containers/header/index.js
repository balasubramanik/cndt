import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import envConfig from 'envConfig'; //eslint-disable-line
import TopHeader from '../../components/topHeader';
import HeaderMenu from '../../components/headerMenu';
import IdleTimePopup from '../../components/idleTimePopup';
import Dialog from '../../components/dialog';
import { getCartCount } from '../../actionCreators/Header';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import { header, links } from './Constants';
import { authentication, authorization, history } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import * as gtmActionCreators from '../../actionCreators/GTM';

export class HeaderContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.isRedirect = false;
    this.state = {
      isShowOverlay: false,
      isShowLogoutDialog: false,
    };
  }

  /** @description React life cycle method
   * it will invoke only when component is mounted
   */
  componentDidMount() {
    const { location } = this.props;
    const { pathname } = location;
    if (!authorization.hasAdminAccess) {
      if (authorization.userRolesList.length && (authorization.userRolesList.includes('FULL_ACCESS') || authorization.userRolesList.includes('VIEW_POINTS'))) {
        if (pathname !== '/'
          && pathname !== RouteConstants.POINTS_HISTORY
          && pathname !== RouteConstants.REDEEM_POINTS
          && pathname !== RouteConstants.CARTSUMMARY
        ) {
          this.props.actions.getRewardsDetails();
        }
        if (authorization.checkAccess(RouteConstants.REDEEM_POINTS) || authorization.checkAccess(RouteConstants.CARTSUMMARY)) {
          this.props.getCartCount();
        }
      }
    }
  }

  componentDidUpdate() {
    const { userInfo, adminUserInfo } = this.props;
    if (Object.keys(userInfo).length === 0 && !authorization.hasAdminAccess) {
      this.props.actions.getProfileDetails();
    }
    if (Object.keys(adminUserInfo).length === 0 && authorization.hasAdminAccess) {
      this.props.actions.getAdminProfileDetails();
    }
  }

  /** @description Function to redirect gaf rewards page
   */
  redirectToGafRewards = () => {
    window.open(`${envConfig.gafBaseUrl}${links.rewardsPageUrl}`, '_self');
  }

  /** @description function to close the logout confirmation dialog
   */
  closePopupHandler = () => {
    this.setState({ isShowLogoutDialog: false });
  }

  /** @description callback function for cart icon
  * @param {object} e - triggered event(click)
  */
  cartHandler = (e) => {
    e.preventDefault();
    if (authorization.checkAccess(RouteConstants.REDEEM_POINTS) || authorization.checkAccess(RouteConstants.CARTSUMMARY)) {
      history.push(RouteConstants.CARTSUMMARY);
    } else {
      this.props.actions.accessPage(true);
    }
  }

  /** @description callback function for signout button */
  signOutHandler = () => {
    const url = `${envConfig.cczBaseUrl}`;
    authentication.clearAuthSession();
    authentication.logout(url);
  }

  /** @description callback function for view more button
   * @param {object} e - triggered event(click)
   */
  viewDetailsHandler = (e) => {
    e.preventDefault();
    history.push(RouteConstants.POINTS_HISTORY);
  }

  /** @description callback function for myaccount button */
  myaccountHandler = () => {
    const url = `${envConfig.cczBaseUrl}${links.cczMyAccountUrl}`;
    authentication.clearAuthSession();
    window.open(url, '_self');
  }

  /** @description callback function for NavItem click
   * @param {object} e - triggered event(click)
   * @param {object} link - selected menu item
   */
  navigationHandler = (e, link) => {
    e.preventDefault();
    if (!link.isExternal) {
      if (authorization.userRolesAccess && authorization.userRolesAccess.includes(link.url)) {
        history.push(link.url);
        /* Trigger the GTM action */
        this.props.gtmActions.gtmHeaderMenuItem(link.title);
      } else {
        this.props.actions.accessPage(true);
      }
      return;
    }
    authentication.clearAuthSession();
    window.open(link.url, '_self');
  };

  /** @description Function to render the logout confirmation dialog controls
   */
  renderLogoutDialogFooter = () => (
    <div>
      <Button onClick={this.closePopupHandler}>{header.labels.no}</Button>
      <Button
        onClick={this.redirectToGafRewards}
        bsStyle="primary"
      >
        {header.labels.yes}
      </Button>
    </div>
  );

  render() {
    const {
      cartItemsCount, userInfo, rewardsInfo, adminUserInfo, location,
    } = this.props;
    const { isShowOverlay, isShowLogoutDialog } = this.state;
    return (
      <header>
        <TopHeader
          profileInfo={{ userInfo, rewardsInfo, adminUserInfo }}
          cartCount={cartItemsCount}
          showOverlay={isShowOverlay}
          onCartClick={this.cartHandler}
          onSignOutClick={this.signOutHandler}
          onViewDetailsClick={this.viewDetailsHandler}
          onMyAccountClick={this.myaccountHandler}
          isAdmin={authorization.hasAdminAccess}
        />
        <HeaderMenu
          currentLocation={location.pathname}
          navLinks={authorization.hasAdminAccess ? header.navlinks.admin : header.navlinks.contractor}
          onNavigationClick={this.navigationHandler}
        />
        <IdleTimePopup
          onConfirmClick={() => authentication.renewToken()}
          onLogout={this.signOutHandler}
        />
        <Dialog
          title={header.labels.logoutConfirmation}
          footer={this.renderLogoutDialogFooter()}
          onCloseClick={this.closePopupHandler}
          show={isShowLogoutDialog}
        />
      </header>
    );
  }
}

function mapStateToProps(state) {
  const { headerState, myProfileState } = state;
  const { cartItemsCount } = headerState;
  const { userInfo, rewardsInfo, adminUserInfo } = myProfileState;
  return {
    cartItemsCount,
    userInfo,
    rewardsInfo,
    adminUserInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getCartCount: bindActionCreators(getCartCount, dispatch),
    actions: bindActionCreators(getProfileDetails, dispatch),
    gtmActions: bindActionCreators(gtmActionCreators, dispatch),
  };
}

/** PropTypes:
 * getCartCount - func - triggers the get cart count api call
 * getProfileDetails - func - triggers the get profile api call
 * cartItemsCount - number - no of cart items
 * userInfo - object - Contains the information about the user
 * rewardsInfo - object - Contains the information about the user's rewards
 */
HeaderContainer.propTypes = {
  getCartCount: PropTypes.func.isRequired,
  actions: PropTypes.object,
  cartItemsCount: PropTypes.number,
  userInfo: PropTypes.object,
  rewardsInfo: PropTypes.object,
  adminUserInfo: PropTypes.object,
  location: PropTypes.object,
  gtmActions: PropTypes.object,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderContainer));
