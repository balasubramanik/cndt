import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import { Grid, Row, Col, Button, Form, Panel, FormGroup } from 'react-bootstrap';
import moment from 'moment';
import { Helmet } from 'react-helmet';
import { texts, GTMEventMessage } from './constants';
import ViewBy from '../../components/viewBy';
import TransactionList from '../../components/transactionList';
import { roundToTwoDecimal, formatCurrency, formatDate } from '../../utils/utils';
import Pagination from '../../components/pagination';
import ShowBy from '../../components/showBy';
import DatePickerControl from '../../components/datePickerControl';
import { searchShowBy } from '../../components/showBy/constants';
import Dropdown from '../../components/dropdown';
import Loader from '../../components/loader';
import { searchByType, pageTitle } from '../../constants/constants';
import SearchBy from '../../components/searchBy';
import RoleAccessPopup from '../../components/roleAccessPopup';
import { authentication } from '../../routes';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import * as pointsHistoryActionCreators from '../../actionCreators/PointsHistory';
import * as typeAHeadActionCreators from '../../actionCreators/SearchBy';
import * as contractorUserActionsCreators from '../../actionCreators/ContractorUser';
import ErrorNotification from '../../components/errorNotification';

const type = {
  Earned: 1,
  Redeemed: 2,
  'Show All': 3,
};

export class PointsHistory extends React.Component {
  constructor(props) {
    super(props);
    this.isExportCSVCliked = false;
    this.state = {
      isOpenAdvanceSearch: false,
      isViewByOptSelected: texts.viewBy[0].value,
      searchOrderNumber: '',
      isMatchOptSelected: texts.matchCriteria.Match[0].value,
      isUserNameOptSelected: '',
      startDate: undefined,
      endDate: undefined,
      isTypeOptSelected: texts.matchCriteria.Status[0].value,
      searchByPlaceholder: texts.t2.search,
      isSearchByIcon: true,
      sortOrder: 'DESC',
      searchByChar: {
        min: 9,
        max: 10,
      },
      isOrderNoSubmitted: false,
      isShowByOptSelected: Number(searchShowBy.showBy[0].value),
      orderBy: 'Date',
      isResetSearch: false,
      currentPageNumber: 1,
      token: '',
      advPaginationRec: 1,
      isInitialPagination: true,
      isPaginationCall: false,
      isExportCsv: false,
      isApplyBtnClicked: false,
      isAdvPaginationLoading: false,
      downloadCSV: false,
    };
  }

  /* Transaction List Actions will trigger when component mounted */
  componentDidMount() {
    const { users, contractorUserActions } = this.props;
    this.props.actions.getRewardsDetails();
    if (!users || users.length === 0) {
      contractorUserActions.getUsers();
    }
    this.props.pointsHistoryAction.getTransactionListClear();
    this.makingTransactionListAPI();
  }

  /* Transaction List Actions will trigger to get Advance Pagination Records */
  componentDidUpdate(prevProps) {
    const { paging, listOfTransactionCSV } = this.props;
    if (this.state.isInitialPagination && paging.totalRecords > this.state.isShowByOptSelected) this.nextPaginationCall(prevProps);
    const { isLoading } = this.props;
    if (this.isExportCSVCliked && (prevProps.isLoading !== isLoading) && !isLoading) {
      if (listOfTransactionCSV && listOfTransactionCSV.length > 0) {
        window.setTimeout(() => {
          this.updateDownloadCSVState(true);
          this.isExportCSVCliked = false;
          /* Trigger the Event for GTM track */
          this.props.gtmActions.gtmPointsHistoryCSVExport(GTMEventMessage.CSVExport);
        }, 500);
      }
    }
    if (this.state.isAdvPaginationLoading && (prevProps.isLoading !== isLoading) && !isLoading) {
      this.resetLazyLoading();
    }
  }

  /* Pagination Handle for Previous & Next Buttons */
  getPaginationNumber = (pageNo, btnName) => {
    const { listOfTransactions, paging } = this.props;
    const { isShowByOptSelected, advPaginationRec } = this.state;
    this.setState({
      currentPageNumber: pageNo,
      token: paging.token || '',
      isResetSearch: true,
    }, () => {
      if (btnName === 'next') {
        const advPageNo = pageNo + advPaginationRec;
        const advRecordsCount = isShowByOptSelected * advPageNo;
        if (listOfTransactions && Object.keys(listOfTransactions).length > 0 && (advRecordsCount <= Math.ceil(paging.totalRecords / isShowByOptSelected) * isShowByOptSelected)) {
          if (!listOfTransactions[Number(advPageNo)]) {
            this.setState({
              isPaginationCall: true,
              isAdvPaginationLoading: true,
            }, () => {
              this.makingTransactionListAPI();
            });
          }
        }
      } else {
        const currentRecordsCount = isShowByOptSelected * pageNo;
        if (listOfTransactions && (Object.keys(listOfTransactions).length > 0) &&
          (currentRecordsCount <= Math.ceil(paging.totalRecords / isShowByOptSelected) * isShowByOptSelected)) {
          if (!listOfTransactions[Number(pageNo)]) {
            this.setState({
              isPaginationCall: false,
            }, () => {
              this.makingTransactionListAPI();
            });
          }
        }
      }
    });
  }

  updateDownloadCSVState = (downloadCSV) => this.setState({ downloadCSV });

  resetLazyLoading = () => {
    this.setState({
      isAdvPaginationLoading: false,
    });
  }

  /* Set Advance Transaction List API call for Pagination lazy loading */
  advancePaginationRec = () => {
    window.setTimeout(() => {
      this.setState({ isInitialPagination: true });
    }, 500);
  }

  /* Creating Advance Pagination API Call for next pages */
  nextPaginationCall = () => {
    const { listOfTransactions } = this.props;
    const listOfTransactionsLength = Object.keys(listOfTransactions).length;
    if (listOfTransactions && listOfTransactionsLength > 0) {
      this.setState({
        isInitialPagination: false,
        isPaginationCall: true,
      }, () => {
        this.makingTransactionListAPI();
      });
    }
  }

  /* Making API call to download Export CSV file */
  handleCSVDownload = () => {
    this.setState({
      isExportCsv: true,
      isPaginationCall: false,
      downloadCSV: false,
    }, () => {
      const reqBody = this.creatingPointsHistoryRequest().transactionList;
      this.props.pointsHistoryAction.getTransactions(reqBody, this.state.isExportCsv);
      this.isExportCSVCliked = true;
      this.setState({ isExportCsv: false });
    });
  }

  /* Making API call for Transaction List */
  makingTransactionListAPI = () => {
    const reqBody = this.creatingPointsHistoryRequest().transactionList;
    if (this.state.isPaginationCall) {
      reqBody.paging.pageNo = this.state.currentPageNumber + this.state.advPaginationRec;
      this.props.pointsHistoryAction.getTransactions(reqBody);
    } else {
      this.props.pointsHistoryAction.getTransactions(reqBody);
    }
  }

  /* Creating Request for Transactions List API Call */
  creatingPointsHistoryRequest = () => {
    const {
      searchOrderNumber,
      isOpenAdvanceSearch,
      isMatchOptSelected,
      isUserNameOptSelected,
      startDate,
      endDate,
      isViewByOptSelected,
      isTypeOptSelected,
      isShowByOptSelected,
      sortOrder,
      orderBy,
      currentPageNumber,
      token,
      isExportCsv,
    } = this.state;
    const viewByDays = (isViewByOptSelected.indexOf('30') > -1) ? 30 : (isViewByOptSelected.indexOf('60') > -1) ? 60 : 90;  //eslint-disable-line
    const { contractorId } = authentication.getAuthDetails();
    const reqBody = {
      transactionList: {
        applyPaging: !isExportCsv,
        sortBy: orderBy,
        sortOrder,
        filter: {
          advancedSearch: isOpenAdvanceSearch,
          contractorId,
          transactionNumber: searchOrderNumber ? searchOrderNumber.toUpperCase() : '',
          matchAll: isMatchOptSelected.toLowerCase() === texts.matchCriteria.Match[0].value.toLowerCase(),
          userName: isUserNameOptSelected || null,
          fromDate: formatDate(startDate) || null,
          toDate: formatDate(endDate) || null,
          viewBy: viewByDays,
          transactionType: type[isTypeOptSelected],
          isExportCsv,
        },
        paging: {
          token: token || '',
          pageNo: currentPageNumber,
          pageSize: isShowByOptSelected,
        },
      },
      contractorId,
    };
    return reqBody;
  }

  /* Advance Search Handler Toogle */
  advanceSearchHandler = (bool) => {
    this.setState({
      isOpenAdvanceSearch: bool,
      searchOrderNumber: '',
      isResetSearch: true,
    });
    /* Trigger the Event for GTM track */
    this.props.gtmActions.gtmPointsHistoryAdvanceSearchToggle(bool ? GTMEventMessage.expand : GTMEventMessage.hide);
  }

  /* View By Option Selection */
  viewByOptionSelection = (viewBytype) => {
    this.setState({
      isApplyBtnClicked: false,
      isViewByOptSelected: viewBytype,
      isPaginationCall: false,
      currentPageNumber: 1,
      searchOrderNumber: '',
      token: '',
      isResetSearch: true,
    }, () => {
      if (viewBytype.toLowerCase() !== texts.viewBy[texts.viewBy.length - 1].value.toLowerCase()) {
        this.props.pointsHistoryAction.getTransactionListClear();
        this.makingTransactionListAPI();
        this.advancePaginationRec();
      }
    });
    if (viewBytype.toLowerCase() === texts.viewBy[texts.viewBy.length - 1].value.toLowerCase()) {
      this.advanceSearchHandler(true);
    } else {
      this.advanceSearchHandler(false);
    }
    /* Trigger the Event for GTM track */
    this.props.gtmActions.gtmPointsHistoryViewBySelection(viewBytype);
  }

  /* Triggering Action when entering search number */
  searchByText = (value) => {
    this.setState({ isApplyBtnClicked: false, isResetSearch: false });
    if (this.state.isOrderNoSubmitted && !value) {
      this.props.pointsHistoryAction.getTransactionListClear();
      this.setState({
        isPaginationCall: false,
        currentPageNumber: 1,
        searchOrderNumber: '',
        token: '',
        isOrderNoSubmitted: false,
      }, () => {
        this.makingTransactionListAPI();
        this.advancePaginationRec();
      });
    }
  }

  /* Triggering Action when Search Claim Number Submitted */
  searchOrderNoSubmitted = (transactionNumber, bool) => {
    if (transactionNumber && bool) {
      this.props.pointsHistoryAction.getTransactionListClear();
      this.setState({
        isPaginationCall: false,
        currentPageNumber: 1,
        searchOrderNumber: transactionNumber,
        isOrderNoSubmitted: true,
        token: '',
        isOpenAdvanceSearch: false,
      }, () => {
        this.makingTransactionListAPI();
      });
      /* Trigger the Event for GTM Track */
      this.props.gtmActions.gtmPointsHistorySearchTransactionNo(transactionNumber);
    }
    if (!transactionNumber || !bool) {
      this.setState({
        isResetSearch: false,
      });
    }
  }

  matchHandler = (value) => {
    this.setState({ isMatchOptSelected: value });
  }

  userNameHandler = (value) => {
    this.setState({ isUserNameOptSelected: value });
  }

  startDateChange = (date) => {
    this.setState({ startDate: date });
    const endDate = moment(this.state.endDate);
    if (date > endDate) this.setState({ endDate: moment() });
  }

  endDateChange = (date) => {
    this.setState({ endDate: date });
  }

  typeHandler = (value) => {
    this.setState({ isTypeOptSelected: value });
  }

  clearHandler = () => {
    this.setState({
      startDate: undefined,
      endDate: undefined,
      isUserNameOptSelected: '',
      isMatchOptSelected: texts.matchCriteria.Match[0].value,
      isTypeOptSelected: texts.matchCriteria.Status[0].value,
      isResetSearch: true,
      isPaginationCall: false,
      currentPageNumber: 1,
      token: '',
      isOpenAdvanceSearch: false,
    }, () => {
      this.advanceSearchHandler(false);
      this.makingTransactionListAPI();
      this.advancePaginationRec();
    });
    /* Trigger the Event for GTM Track */
    this.props.gtmActions.gtmPointsHistoryClearBtnClicked(GTMEventMessage.clear);
  }

  /* Dispatching Transaction List Action when Apply Handler Clicked */
  applyHandler = () => {
    this.setState({ isApplyBtnClicked: true });
    if (this.state.startDate && !this.state.endDate) {
      return false;
    }
    this.props.pointsHistoryAction.getTransactionListClear();
    this.setState({
      // searchOrderNumber: null,
      isPaginationCall: false,
      currentPageNumber: 1,
      token: '',
      isResetSearch: true,
    }, () => {
      this.advancePaginationRec();
      this.makingTransactionListAPI();
    });
    /* Trigger the Event for GTM track */
    this.triggerGTMEvent();
    return true;
  }

  /* Show By Option Selection */
  showperPage = (selectedshowperPage) => {
    this.props.pointsHistoryAction.getTransactionListClear();
    this.setState({
      isShowByOptSelected: Number(selectedshowperPage),
      currentPageNumber: 1,
      isPaginationCall: false,
      token: '',
      isResetSearch: true,
    }, () => {
      this.makingTransactionListAPI();
      this.advancePaginationRec();
    });
  }

  /* Sorting Table header coloumn in ascending or descending order */
  sortingDataOrder = (key, types) => {
    this.setState({
      orderBy: key.charAt(0).toUpperCase() + key.substr(1),
      sortOrder: types ? 'ASC' : 'DESC',
      isPaginationCall: false,
      isResetSearch: true,
      currentPageNumber: 1,
    }, () => {
      this.props.pointsHistoryAction.getTransactionListClear();
      this.makingTransactionListAPI();
      this.advancePaginationRec();
    });
  }

  /* Format the user name to show the last name followed by first name */
  formatUsers = () => {
    const { users } = this.props;
    if (!users || users.length === 0) {
      return texts.matchCriteria.ShowAll;
    }
    const formattedusers = users.map((user) => ({
      ...user,
      label: `${user.firstName} ${user.lastName}`,
      value: user.emailAddress,
    }));
    return texts.matchCriteria.ShowAll.concat(formattedusers);
  }

  /** @description callback function for clear error
   * @param {object} e - triggered event(click)
   * @param {func} action - action for clear error
   */
  clearError = (e, action) => {
    e.preventDefault();
    action();
  }

  /* Trigger the Event for GTM track */
  triggerGTMEvent = () => {
    const {
      isUserNameOptSelected,
      isMatchOptSelected,
      isTypeOptSelected,
      startDate,
      endDate,
    } = this.state;
    this.props.gtmActions.gtmPointsHistoryApplyBtnClicked(GTMEventMessage.apply);
    if (isUserNameOptSelected) this.props.gtmActions.gtmPointsHistoryUserNameSelection(isUserNameOptSelected);
    if (isMatchOptSelected) this.props.gtmActions.gtmPointsHistoryMatchItemSelection(isMatchOptSelected);
    if (isTypeOptSelected) this.props.gtmActions.gtmPointsHistoryTypeSelection(isTypeOptSelected);
    if (startDate) this.props.gtmActions.gtmPointsHistoryStartDateSelection(startDate);
    if (endDate) this.props.gtmActions.gtmPointsHistoryEndDateSelection(endDate);
  }

  /* Trigger the Event when View Button Clicked in Claims List Table */
  viewTransactionDetails = (transactionID) => {
    /* Trigger the event for track */
    this.props.gtmActions.gtmPointsHistoryViewTransactionDetails(transactionID);
  }

  /* Rendering ViewBy Component & SearchBy Component */
  renderViewBySearchBy = () => {
    const isShowAdvanceIcon = classNames({ 'open-advance-search': this.state.isOpenAdvanceSearch });
    return (
      <div className="gaf-search">
        <Row>
          <div className="col-md-4 col-xs-10" id="ph-viewby">
            <ViewBy id="pointhistory-viewBy" onChange={this.viewByOptionSelection} options={texts.viewBy} />
          </div>
          <Col md={3} xs={2} mdPush={5}>
            <div className="ad-search-filter-icon">
              <a
                href="/"
                id="ph-filter-icon"
                className="icon-filter visible-sm visible-xs"
                onClick={(e) => { e.preventDefault(); this.advanceSearchHandler(!this.state.isOpenAdvanceSearch); }}
              >
              </a>
            </div>
            <Button
              id="ph-search-btn"
              className={`btn-block ad-search-btn visible-md visible-lg ${isShowAdvanceIcon}`}
              onClick={(e) => { e.preventDefault(); this.advanceSearchHandler(!this.state.isOpenAdvanceSearch); }}
            >
              {texts.t2.customSearch}
              <i className="icon-arrow-up">
              </i>
            </Button>
          </Col>
          <Col md={4} xs={12} mdOffset={1} mdPull={3}>
            <SearchBy
              id={searchByType.PointsHistoryClaimNumber}
              placeholder={this.state.searchByPlaceholder}
              isSearchByIcon={this.state.isSearchByIcon}
              typeahead="false"
              charLength={this.state.searchByChar}
              handleChange={this.searchByText}
              validationErrTxt={texts.t2.errorTxt}
              handleSubmit={this.searchOrderNoSubmitted}
              isResetSearch={this.state.isResetSearch}
            />
          </Col>
        </Row>
      </div>
    );
  }


  /* Rendring MatchBy Component, Product Component & User Name Component */
  renderMatchUserNameType = () => {
    const isDisableMatch = true;
    const { users } = this.props;
    const formattedUsers = this.formatUsers();
    return (
      <Col md={12}>
        <div className="filterby">
          <Form inline>
            <FormGroup className="gaf-form match-criteria" id="ph-match-criteria">
              <span className="hidden-xs hidden-sm">{texts.t2.match}</span>
              <span className="visible-xs visible-sm">{texts.t2.match}<span className="input-short-text">({texts.t2.criteria})</span></span>
              <Dropdown
                className="display-inline"
                id="Match"
                menuItems={texts.matchCriteria.Match}
                onChangeHandler={this.matchHandler}
                value={this.state.isMatchOptSelected}
                readOnly={isDisableMatch}
              />
              <span className="input-short-text hidden-xs hidden-sm">{texts.t2.criteria}</span>
            </FormGroup>
            <FormGroup className="gaf-form select-user" id="ph-select-user">
              <label>{texts.t2.username}</label>
              <Dropdown
                className="display-inline"
                id="UserName"
                menuItems={formattedUsers}
                onChangeHandler={this.userNameHandler}
                value={this.state.isUserNameOptSelected}
                readOnly={!(users && users.length > 0)}
              />
            </FormGroup>
          </Form>
        </div>
      </Col>
    );
  }

  /* Rendering Date Picker Component, Status Component, Clear & Apply Buttons */
  renderDatePickerType = () => {
    const { startDate, endDate } = this.state;
    return (
      <Col md={12}>
        <div className="filterby calender-section">
          <Form inline>
            <FormGroup className="gaf-form calender-input">
              <DatePickerControl
                id="fromDate"
                min={moment().subtract(24, 'months')}
                max={moment()}
                onChange={(date) => this.startDateChange(date)}
                value={this.state.startDate}
                placeholder={texts.t2.fromDate}
              />
            </FormGroup>
            <FormGroup className="gaf-form calender-input">
              <DatePickerControl
                id="toDate"
                min={moment(new Date(this.state.startDate))}
                max={moment()}
                onChange={(date) => this.endDateChange(date)}
                value={this.state.endDate}
                placeholder={texts.t2.toDate}
                disabled={!this.state.startDate}
              />
              {this.state.isApplyBtnClicked && startDate && !endDate && <span className="text-danger input-error-msg">{texts.t2.message}</span>}
            </FormGroup>
            <FormGroup className="gaf-form select-claims">
              <label>{texts.t2.type}</label>
              <Dropdown className="display-inline" id="Status" menuItems={texts.matchCriteria.Status} onChangeHandler={this.typeHandler} value={this.state.isTypeOptSelected} />
            </FormGroup>
            <div className="btn-actions">
              <Button className="btn gaf-btn-secondary" id="ph-clear-btn" onClick={this.clearHandler}>{texts.t2.clear}</Button>
              <Button className="btn gaf-btn-primary" id="ph-apply-btn" onClick={this.applyHandler}>{texts.t2.apply}</Button>
            </div>
          </Form>
        </div>
      </Col>
    );
  };


  /* Rendering Claims List Table */
  renderTransactionListTable = () => {
    const {
      listOfTransactions,
      isLoading,
      paging,
      listOfTransactionCSV,
    } = this.props;
    const { downloadCSV, isShowByOptSelected } = this.state;
    return (
      <div className="pointshistory-list" id="ph-listdata">
        <TransactionList
          data={listOfTransactions[this.state.currentPageNumber]}
          sortingData={this.sortingDataOrder}
          isloading={isLoading}
          isAsyncExportCSV="true"
          CSVHandler={this.handleCSVDownload}
          exportCSVData={listOfTransactionCSV}
          defaultSort={this.state.orderBy}
          viewTransactionDetails={this.viewTransactionDetails}
          accessPage={this.props.actions.accessPage}
          triggerDownload={downloadCSV}
        />
        <div className="gaf-pagination">
          <Row>
            <Col sm={6} md={6} xsHidden>
              <Form inline>
                {paging.totalRecords > searchShowBy.showBy[0].value && <ShowBy onChange={this.showperPage} defaultValue={isShowByOptSelected} />}
              </Form>
            </Col>
            <Col sm={6} md={6} xs={12}>
              {(listOfTransactions && Object.keys(listOfTransactions).length > 0) &&
                (paging.totalRecords > this.state.isShowByOptSelected) &&
                <Pagination totalRows={paging.totalRecords} perPage={this.state.isShowByOptSelected} pageClick={this.getPaginationNumber} />}
            </Col>
          </Row>
        </div>
      </div>
    );
  }

  /** @description function to render the error message for Claim Status */
  renderErrorMessage = () => {
    const { transactionListError, pointsHistoryAction } = this.props;
    const props = {
      error: transactionListError,
      onClear: (e) => this.clearError(e, pointsHistoryAction.transactionListClearError),
    };
    return (<ErrorNotification {...props} />);
  }

  render() {
    const {
      rewardsInfo,
      accessInfo,
      paging,
      isLoading,
    } = this.props;
    const { isPaginationCall, isAdvPaginationLoading, currentPageNumber } = this.state;
    const balance = formatCurrency(roundToTwoDecimal(rewardsInfo.rewardPoints));
    const availbleBalance = balance || roundToTwoDecimal(0);
    return (
      <main className="main-content points-history" id="points-history">
        <Helmet>
          <title>{pageTitle.POINTSHISTORY}</title>
        </Helmet>
        <section>
          <Grid>
            <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={this.props.actions.accessPage} />
            <div className="gaf-title-block" id="ph-title-block">
              <Row>
                <Col xs={12} md={6}>
                  <h2>{texts.label.header}</h2>
                </Col>
                <Col xs={12} md={6}>
                  <div className="available-balance-points">
                    <p>{texts.t1.text} <span>{availbleBalance}</span></p>
                  </div>
                </Col>
              </Row>
            </div>
            <div className="gaf-page-info">
              <div className="gaf-search-area">
                {this.renderViewBySearchBy()}
                <div className="gaf-advance-search points-history-ad-search">
                  <Panel id="collapsible-panel-example-1" className="gaf-ad-search-panel" expanded={this.state.isOpenAdvanceSearch} onToggle={() => { }}>
                    <Panel.Collapse>
                      <Panel.Body>
                        <Row>
                          {this.renderMatchUserNameType()}
                          {this.renderDatePickerType()}
                        </Row>
                      </Panel.Body>
                    </Panel.Collapse>
                  </Panel>
                </div>
              </div>
              {this.renderTransactionListTable()}
            </div>
          </Grid>
        </section>
        {((isLoading && !isPaginationCall) || (isAdvPaginationLoading && (paging.pageNo < currentPageNumber))) ? <Loader /> : null}
        {this.renderErrorMessage()}
      </main>
    );
  }
}

function mapStateToProps(state) {
  const {
    pointsHistoryState,
    searchByState,
    contractorUserState,
    myProfileState,
  } = state;
  const {
    listOfTransactions = {},
    listOfTransactionCSV = [],
    isLoading,
    filter,
    transactionListError,
    paging = { token: '', totalRecords: 0 },
  } = pointsHistoryState;
  const { POINT_HISTORY_CLAIM_NUMBER = {} } = searchByState;
  const { users } = contractorUserState;
  const { rewardsInfo, accessInfo } = myProfileState;
  return {
    POINT_HISTORY_CLAIM_NUMBER,
    listOfTransactions,
    listOfTransactionCSV,
    isLoading,
    users,
    filter,
    paging,
    transactionListError,
    rewardsInfo,
    accessInfo,
  };
}


function mapDispatchToProps(dispatch) {
  return {
    contractorUserActions: bindActionCreators(contractorUserActionsCreators, dispatch),
    pointsHistoryAction: bindActionCreators(pointsHistoryActionCreators, dispatch),
    typeAHeadActions: bindActionCreators(typeAHeadActionCreators, dispatch),
    actions: bindActionCreators(getProfileDetails, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

PointsHistory.propTypes = {
  listOfTransactions: PropTypes.object,
  getTransactions: PropTypes.func,
  pointsHistoryAction: PropTypes.object.isRequired,
  users: PropTypes.array,
  listOfTransactionCSV: PropTypes.array,
  isLoading: PropTypes.bool,
  rewardsInfo: PropTypes.object,
  accessInfo: PropTypes.bool,
  paging: PropTypes.object,
  actions: PropTypes.object,
  contractorUserActions: PropTypes.object.isRequired,
  transactionListError: PropTypes.string,
  gtmActions: PropTypes.object.isRequired,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PointsHistory));
