const texts = {
  label: {
    header: 'Points History',
  },
  t1: { text: 'Available Balance ' },
  t2: {
    search: 'Search by Claim/Order Number',
    description: 'description',
    customSearch: 'Custom Search',
    match: 'Match',
    criteria: 'of the following criteria',
    username: 'User Name',
    type: 'Type',
    clear: 'clear',
    apply: 'apply',
    errorTxt: 'Please enter a valid claim or order number.',
    select: 'Select',
    fromDate: 'From',
    toDate: 'To',
    message: 'Please select To Date',
  },
  viewBy: [
    { label: 'Last 30 Days', value: 'Last 30 Days' },
    { label: 'Last 60 Days', value: 'Last 60 Days' },
    { label: 'Last 90 Days', value: 'Last 90 Days' },
    { label: 'Custom', value: 'Custom' },
  ],
  matchCriteria: {
    Match: [
      { label: 'All', value: 'All' },
      { label: 'Any', value: 'Any' },
    ],
    UserName: [
      { title: 'Imthiyas', value: 'Imthiyas' },
      { title: 'John', value: 'John' },
      { title: 'Mike', value: 'Mike' },
    ],
    Status: [
      { label: 'Show All', value: 'Show All' },
      { label: 'Earned', value: 'Earned' },
      { label: 'Redeemed', value: 'Redeemed' },
    ],
    ShowAll: [{ label: 'Show All', value: '' }],
  },
};

export const GTMEventMessage = {
  expand: 'Expand',
  hide: 'Hide',
  clear: 'Clear Points History Filters',
  apply: 'Points History Filters Applied',
  CSVExport: 'List of Transactions',
};

export { texts };
