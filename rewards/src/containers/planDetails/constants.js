const texts = {
  planDetails: {
    title: 'Plan Details -',
    customerID: 'Corporate Customer ID:',
    contractorName: 'Contractor Name:',
    address: 'Address:',
    linkToContractorManagement: 'Contractor Management',
  },
};

export { texts };
