import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Col, Row } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { history } from '../../routes';
import { texts } from './constants';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import RouteConstants from '../../constants/RouteConstants';
import PlanAndPromotion from '../planAndPromotion';
import RoleAccessPopup from '../../components/roleAccessPopup';

export class PlanDetails extends React.Component {
  redirectToContractorManagement = (e) => {
    e.preventDefault();
    history.push({
      pathname: RouteConstants.CONTRACTORMANAGEMENT,
    });
  };

  render() {
    const { accessInfo, location } = this.props;
    const { state } = location;
    return (
      <main className="main-content what-qualifies plan-details" id="plan-details">
        <section>
          <Grid>
            <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={this.props.actions.accessPage} />
            <div className="gaf-title-block">
              <Row className="show-grid">
                <Col md={6}>
                  <h2>{texts.planDetails.title}  {state.parentContractorId}</h2>
                  <table>
                    <tbody>
                      <tr>
                        <td>{texts.planDetails.customerID}</td>
                        <td>{state.displayContractorId}</td>
                      </tr>
                      <tr>
                        <td>{texts.planDetails.contractorName}</td>
                        <td>{state.contractorName}</td>
                      </tr>
                      <tr>
                        <td>{texts.planDetails.address}</td>
                        <td>
                          {state.address.address1},
                          {state.address.address2 !== '' ? `${state.address.address3},` : ''}
                          {state.address.address3 !== '' ? `${state.address.address3},` : ''}
                          {state.address.city},
                          {state.address.stateOrProvince}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </Col>
                <Col md={6}>
                  <p className="links"><a href="/" onClick={this.redirectToContractorManagement}>{texts.planDetails.linkToContractorManagement}</a></p>
                </Col>
              </Row>
            </div>
            <PlanAndPromotion contractorId={state.contractorId} />
          </Grid>
        </section>
      </main>
    );
  }
}

function mapStateToProps(state) {
  const { myProfileState } = state;
  const { accessInfo } = myProfileState;
  return {
    accessInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(getProfileDetails, dispatch),
  };
}

PlanDetails.propTypes = {
  location: PropTypes.object,
  accessInfo: PropTypes.bool,
  actions: PropTypes.object,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanDetails));
