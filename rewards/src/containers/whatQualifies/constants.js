const texts = {
  t1:
    { text: 'Valid in the U.S. only. GAF reserves the right to audit all redemption claims. Offer void where prohibited, taxed, or restricted by law. Ineligible, incomplete, and illegible claims or claims that do not comply with these terms and the terms of the GAF Rewards will be rejected. This offer cannot be combined with any other promotion or offer. GAF is not responsible for lost, late, mutilated, misdirected, or postage-due submissions or any computer, hardware, software, network, telephone, or human errors that may occur. Return of qualifying merchandise invalidates offer.' },
  t2:
    { text: 'What products can earn you points? How many points can you earn? And when? All your answers are here.' },
  label: {
    header: 'What Qualifies',
  },
};

export { texts };
