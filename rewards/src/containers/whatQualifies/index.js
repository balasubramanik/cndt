import React from 'react';
import PropTypes from 'prop-types';
import { Grid } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { bindActionCreators } from 'redux';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import { pageTitle } from '../../constants/constants';
import { texts } from './constants';
import RoleAccessPopup from '../../components/roleAccessPopup';
import PlanAndPromotion from '../planAndPromotion';

let whatQualifiesContainer = null;
const scrollIntoView = (top) => {
  if (whatQualifiesContainer) {
    window.scrollTo(0, top - whatQualifiesContainer.offsetTop);
  }
};

/** @description Class component to render the What Qualifies Container */
const WhatQualifies = (props) => {
  const {
    accessInfo,
  } = props;
  return (
    <main className="main-content what-qualifies comment" id="what-qualifies" ref={(ele) => { whatQualifiesContainer = ele; }}>
      <Helmet>
        <title>{pageTitle.WHATQUALIFIES}</title>
      </Helmet>
      <section>
        <Grid>
          <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={props.actions.accessPage} />
          {/* GAF - Qualifies Title Block */}
          <div className="gaf-title-block" id="wq-titleblock">
            <h2>{texts.label.header}</h2>
            <p>{texts.t2.text}</p>
          </div>
          {/* GAF - Qualifies Title Block  Ends */}
          <PlanAndPromotion scrollIntoView={scrollIntoView} />
          {/* GAF - Description Block */}
          <div className="gaf-description-block" id="wq-description-block">
            <p>{texts.t1.text}</p>
          </div>
          { /* GAF - Description Block End */}
        </Grid>
      </section>
    </main >
  );
};

function mapStateToProps(state) {
  const { myProfileState } = state;
  const { accessInfo } = myProfileState;
  return {
    accessInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(getProfileDetails, dispatch),
  };
}

/** PropTypes:
 * getMyPlans - replace with the services
 * getSpotPromotions - replace with the services
 * myPlanCategory - data for the my paln category
 * spotPromotionsCategory - data for the spot promotion category
 * location - location prop
 */

WhatQualifies.propTypes = {
  accessInfo: PropTypes.bool,
  actions: PropTypes.object,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WhatQualifies));
