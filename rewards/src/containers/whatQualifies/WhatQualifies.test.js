import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import WhatQualifiesCont, { WhatQualifies } from '../../containers/whatQualifies';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();
window.open = jest.fn();
describe('WhatQualifies', () => {
  const getCategory = jest.fn();
  const tree = shallow(<WhatQualifies
    getCategory={getCategory}
  />);
  it('should be defined', () => {
    expect(WhatQualifies).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render what Qualifies container', () => {
    expect(tree.find('.main-content').length).toBe(1);
  });

  it('should render Title section', () => {
    expect(tree.find('.gaf-title-block').length).toBe(1);
  });

  it('should render my plan component', () => {
    expect(tree.find('MyPlan').length).toBe(1);
  });

  it('should render spot promotions component', () => {
    expect(tree.find('SpotPromotions').length).toBe(1);
  });

  it('should call unmount', () => {
    tree.unmount();
  });
});

describe('WhatQualifies with connected component', () => {
  window.open = jest.fn();
  const state = {
    myPlanCategory: jest.fn(),
  };

  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <WhatQualifiesCont />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(WhatQualifiesCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Title section', () => {
    expect(tree.find('.gaf-title-block').length).toBe(1);
  });

  it('should render my plan component', () => {
    expect(tree.find('MyPlan').length).toBe(1);
  });

  it('should render spot promotions component', () => {
    expect(tree.find('SpotPromotions').length).toBe(1);
  });
});
