const texts = {
  label: {
    myPlan: 'My Plan',
    spotPromotions: 'Spot Promotions',
    myPlanCSV: 'myPlan',
    spotPromotionCSV: 'spotPromotions',
  },
};

export { texts };
