import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Tabs, Tab } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCategory, getCategoryPromotions, clearErrors, updateMyPlanID, updateSpotPromotionPlanID, updateSpotPromotionSelectedYear } from '../../actionCreators/WhatQualifies';
import * as gtmActionsCreators from '../../actionCreators/GTM';
import { texts } from './constants';
import MyPlan from '../../components/myPlan';
import SpotPromotions from '../../components/spotPromotions';
import Loader from '../../components/loader';
import ErrorNotification from '../../components/errorNotification';

export class PlanAndPromotion extends React.Component {
  constructor(props, context) {
    super(props, context);
    const { id, selectedYear } = props;
    this.currentYear = moment().year();
    const year = id && selectedYear;
    this.state = {
      selectedYear: year || this.currentYear,
      activeTabIndex: id ? 2 : 1,
    };
  }
  componentDidMount() {
    const { activeTabIndex } = this.state;
    const { contractorId } = this.props;
    this.getPlanCategories(activeTabIndex, contractorId);
  }

  onChange = (value) => {
    const { activeTabIndex } = this.state;
    const { contractorId } = this.props;
    this.props.gtmActions.gtmCategoryFilter(value);
    this.setState({ selectedYear: value }, () => {
      this.props.updateSpotPromotionSelectedYear(this.state.selectedYear);
      this.props.updateMyPlanID();
      this.getPlanCategories(activeTabIndex, contractorId);
    });
  }

  getPlanCategories = (key) => {
    const { getMyPlans, getSpotPromotions, contractorId } = this.props;
    const { selectedYear } = this.state;
    if (key === 1) {
      getMyPlans(selectedYear, contractorId);
    } else {
      getSpotPromotions(selectedYear, contractorId);
    }
  }

  handleCSVDownload = (type) => {
    this.props.gtmActions.gtmWhatQualifiesCSVExport(type);
  }

  /** @description Function to handle the selection of tabs */
  tabChangeHandler = (key) => {
    const { activeTabIndex, selectedYear } = this.state;
    this.currentYear = moment().year();
    if (activeTabIndex === 1) {
      this.props.gtmActions.gtmMyPlan();
    }
    if (activeTabIndex === 2) {
      this.props.gtmActions.gtmSpotPromotions();
    }
    if (activeTabIndex !== key) {
      this.setState({ selectedYear: this.currentYear, activeTabIndex: key }, () => {
        this.getPlanCategories(key);
      });
    }
    this.props.updateSpotPromotionPlanID();
    this.props.updateMyPlanID();
    this.props.updateSpotPromotionSelectedYear(selectedYear);
  }

  /** @description function to render the error message for EMS */
  renderErrorMessage = () => {
    const { activeTabIndex } = this.state;
    const {
      isMyPlanError, isSpotPromotionError,
    } = this.props;
    let props = {};
    if (activeTabIndex === 0 || activeTabIndex === 1) {
      props = {
        error: isMyPlanError,
        onClear: (e) => this.clearError(e, clearErrors),
      };
    } else {
      props = {
        error: isSpotPromotionError,
        onClear: (e) => this.clearError(e, clearErrors),
      };
    }

    return (<ErrorNotification {...props} />);
  }

  render() {
    const { activeTabIndex, selectedYear } = this.state;
    const {
      myPlanCategory,
      spotPromotionsCategory,
      isLoading,
      scrollIntoView,
      id,
      myPlanId,
    } = this.props;
    return (
      <div>
        <div className="gaf-tabs table-border-mob" id="gaf-tabs">
          <Tabs defaultActiveKey={activeTabIndex} animation={false} id={1} onSelect={this.tabChangeHandler}>
            <Tab eventKey={1} title={texts.label.myPlan}>
              <MyPlan
                selectedPlanId={myPlanId}
                updatePlanId={this.props.updateMyPlanID}
                selectedYear={selectedYear}
                activeIndex={activeTabIndex}
                data={myPlanCategory}
                isAsyncExportCSV="false"
                onRef={(ref) => { this.myPlanChild = ref; }}
                CSVHandler={() => { this.handleCSVDownload(texts.label.myPlanCSV); }}
                onChange={this.onChange}
                loading={isLoading}
              />
            </Tab>
            <Tab eventKey={2} title={texts.label.spotPromotions}>
              <SpotPromotions
                selectedPlanId={id}
                scrollIntoView={scrollIntoView}
                selectedYear={selectedYear}
                activeIndex={activeTabIndex}
                data={spotPromotionsCategory}
                isAsyncExportCSV="false"
                onRef={(ref) => { this.spotPromotionChild = ref; }}
                CSVHandler={() => { this.handleCSVDownload(texts.label.spotPromotionCSV); }}
                onChange={this.onChange}
                loading={isLoading}
                updatePlanId={this.props.updateSpotPromotionPlanID}
                updateSelectedYear={this.props.updateSpotPromotionSelectedYear}
              />
            </Tab>
          </Tabs>
        </div>
        {isLoading && <Loader />}
        {this.renderErrorMessage()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { whatQualifiesState } = state;
  const {
    myPlanCategory, isLoading, spotPromotionsCategory, id, selectedYear, myPlanId,
  } = whatQualifiesState;
  return {
    myPlanCategory,
    spotPromotionsCategory,
    isLoading,
    id,
    selectedYear,
    myPlanId,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getMyPlans: bindActionCreators(getCategory, dispatch),
    getSpotPromotions: bindActionCreators(getCategoryPromotions, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
    updateSpotPromotionPlanID: bindActionCreators(updateSpotPromotionPlanID, dispatch),
    updateMyPlanID: bindActionCreators(updateMyPlanID, dispatch),
    updateSpotPromotionSelectedYear: bindActionCreators(updateSpotPromotionSelectedYear, dispatch),
  };
}

PlanAndPromotion.propTypes = {
  location: PropTypes.object,
  getMyPlans: PropTypes.func.isRequired,
  getSpotPromotions: PropTypes.func.isRequired,
  gtmActions: PropTypes.object,
  myPlanCategory: PropTypes.array,
  spotPromotionsCategory: PropTypes.array,
  isMyPlanError: PropTypes.any,
  isSpotPromotionError: PropTypes.any,
  isLoading: PropTypes.bool,
  contractorId: PropTypes.string,
  scrollIntoView: PropTypes.func,
  updateSpotPromotionSelectedYear: PropTypes.func,
  id: PropTypes.string,
  myPlanId: PropTypes.string,
  selectedYear: PropTypes.number,
  updateSpotPromotionPlanID: PropTypes.func,
  updateMyPlanID: PropTypes.func,
};

PlanAndPromotion.defaultProps = {
  scrollIntoView: () => { },
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PlanAndPromotion));
