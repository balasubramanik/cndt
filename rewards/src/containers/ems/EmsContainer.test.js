import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import EmsCont, { EmsContainer } from '../../containers/ems';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();

describe('EmsContainer', () => {
  const getRegistrationInfo = jest.fn();
  const props = {
    location: {
      search: '',
    },
    registrationActions: {
      getRegistrationInfo,
    },
    gtmActions: {
      gtmRegistrationSuccess: jest.fn(),
      gtmEmailVerificationSuccess: jest.fn(),
    },
    isEmailFormLoading: true,
    status: 'pending',
    isEmailVerificationSuccess: true,
    isRegistrationSuccess: true,
  };
  const tree = shallow(<EmsContainer
    {...props}
  />);
  it('should be defined', () => {
    expect(EmsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Ems container', () => {
    expect(tree.find('.ems-container').length).toBe(1);
  });

  it('should render StepWizard', () => {
    expect(tree.find('StepWizard').length).toBe(1);
  });

  it('should render EmailVerificationContainer', () => {
    tree.setState({ activeTabIndex: 0 });
    expect(tree.find('.ems-emailverification').length).toBe(1);
  });

  it('should render RegistrationContainer', () => {
    tree.setState({ activeTabIndex: 1 });
    expect(tree.find('.ems-registration').length).toBe(1);
  });

  it('should render Validation', () => {
    tree.setState({ activeTabIndex: 2 });
    expect(tree.find('.ems-validation').length).toBe(1);
  });

  it('should render RegistrationMessageContainer', () => {
    tree.setState({ activeTabIndex: 3 });
    expect(tree.find('.ems-registration-message').length).toBe(1);
  });

  it('should not render component', () => {
    tree.setState({ activeTabIndex: 4 });
    expect(tree.find('.ems-registration-message').length).toBe(0);
    expect(tree.find('.ems-validation').length).toBe(0);
    expect(tree.find('.ems-registration').length).toBe(0);
    expect(tree.find('.ems-emailverification').length).toBe(0);
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isEmailVerificationSuccess: false,
      isRegistrationSuccess: false,
      status: '',
    });
    expect(tree.state().activeTabIndex).toBe(3);
  });

  it('should render Loader', () => {
    expect(tree.find('Loader').length).toBe(1);
  });
});

describe('EmsContainer with status pending', () => {
  const getRegistrationInfo = jest.fn();
  const clearErrors = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const props = {
    location: {
      search: '',
    },
    emailVerificationActions: {
      clearErrors,
    },
    registrationActions: {
      getRegistrationInfo,
      clearErrors,
    },
    gtmActions: {
      gtmRegistrationSuccess: jest.fn(),
      gtmEmailVerificationSuccess: jest.fn(),
    },
    status: 'emailverified',
    isEmailVerificationSuccess: true,
    isRegistrationSuccess: true,
  };
  const tree = shallow(<EmsContainer
    {...props}
  />);

  it('should be defined', () => {
    expect(EmsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Ems container', () => {
    expect(tree.find('.ems-container').length).toBe(1);
  });

  it('should call clearErrors', () => {
    const simulateClick = tree.find('ErrorNotification').prop('onClear');
    simulateClick(event);
    expect(clearErrors).toBeCalled();
  });

  it('should call clearErrors for registration', () => {
    tree.setState({ activeTabIndex: 1 });
    const simulateClick = tree.find('ErrorNotification').prop('onClear');
    simulateClick(event);
    expect(clearErrors).toBeCalled();
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isEmailVerificationSuccess: false,
      isRegistrationSuccess: false,
      status: '',
    });
    expect(tree.state().activeTabIndex).toBe(1);
  });
});

describe('EmsContainer with state params', () => {
  const getRegistrationInfo = jest.fn();
  const props = {
    location: {
      search: '?id=sample@test.com',
    },
    registrationActions: {
      getRegistrationInfo,
    },
    gtmActions: {
      gtmRegistrationSuccess: jest.fn(),
      gtmEmailVerificationSuccess: jest.fn(),
    },
    status: 'pending',
    isEmailVerificationSuccess: true,
    isRegistrationSuccess: true,
  };
  const tree = shallow(<EmsContainer
    {...props}
  />);

  it('should be defined', () => {
    expect(EmsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should call getRegistrationInfo', () => {
    expect(getRegistrationInfo).toBeCalled();
  });
});

describe('EmsContainer with connected component', () => {
  const getRegistrationInfo = jest.fn();
  const state = {
    form: {
      emailVerificationForm: {
        done: true,
        values: {}
      },

      registrationActions: {
        getRegistrationInfo,
      },
      registrationForm: {
        done: true,
        isLoading: true,
        status: 'completed',
      },
    },
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <EmsCont
          location={{
            search: '',
          }}
          gtmActions={{
            gtmRegistrationSuccess: jest.fn(),
            gtmEmailVerificationSuccess: jest.fn(),
          }}
        />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(EmsCont).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Ems container', () => {
    expect(tree.find('.ems-container')).toBeDefined();
  });

  it('should render StepWizard', () => {
    expect(tree.find('StepWizard').length).toBe(1);
  });

  it('should render EmailVerificationContainer', () => {
    tree.setState({ activeTabIndex: 0 });
    expect(tree.find('.ems-emailverification').length).toBeDefined();
  });

  it('should render RegistrationContainer', () => {
    tree.setState({ activeTabIndex: 1 });
    expect(tree.find('.ems-registration').length).toBeDefined();
  });

  it('should render Validation', () => {
    tree.setState({ activeTabIndex: 2 });
    expect(tree.find('.ems-validation').length).toBeDefined();
  });

  it('should render RegistrationMessageContainer', () => {
    tree.setState({ activeTabIndex: 3 });
    expect(tree.find('.ems-registration-message').length).toBeDefined();
  });

  it('should render Loader', () => {
    expect(tree.find('Loader').length).toBe(1);
  });
});
