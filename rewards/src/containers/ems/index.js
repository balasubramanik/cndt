import React, { PureComponent } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Col, Grid, Row } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import ReCAPTCHA from 'react-google-recaptcha';
import envConfig from 'envConfig'; //eslint-disable-line
import * as emailVerificationActionsCreators from '../../actionCreators/EmailVerification';
import * as registrationActionsCreators from '../../actionCreators/Registration';
import { constants, metaTags } from './Constants';
import { pageTitle } from '../../constants/constants';
import RouteConstants from '../../constants/RouteConstants';
import StepWizard from '../../components/stepWizard';
import Loader from '../../components/loader';
import EmailVerificationContainer from '../../containers/emailVerification';
import RegistrationContainer from '../../containers/registration';
import ConfirmationMessageContainer from '../../containers/confirmationMessage';
import Validation from '../../components/validation';
import ErrorNotification from '../../components/errorNotification';
import EmsHeader from '../../components/emsHeader';
import Footer from '../../components/footer';
import { getQueryString, scrollToTop } from '../../utils/utils';
import { loadAsyncScript } from '../../components/loadAsyncScript';
import * as gtmActionsCreators from '../../actionCreators/GTM';

export class EmsContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: null,
    };
  }

  /** @description React Life cycle method
   * it will invoke, when component is mounted.
   */
  componentDidMount() {
    const { location, registrationActions } = this.props;
    const { search } = location;
    const id = getQueryString('id', search);
    if (id) {
      const decodeId = decodeURIComponent(id);
      registrationActions.getRegistrationInfo(decodeId);
    } else {
      this.updateState('activeTabIndex', 0);
    }
  }

  /** @description React Life cycle method
   * it will invoke, when component is updated.
   * @param {object} prevProps - previous props of component
   */
  componentDidUpdate(prevProps, prevState) {
    const { activeTabIndex } = this.state;
    const {
      status, isEmailVerificationSuccess, isRegistrationSuccess,
    } = this.props;
    const regStatus = status ? status.toLowerCase() : '';
    /** Condition for redirect to confirmation message */
    if (prevProps.isEmailVerificationSuccess !== isEmailVerificationSuccess
      && isEmailVerificationSuccess) {
      this.updateState('activeTabIndex', 3);
      this.props.gtmActions.gtmEmailVerificationSuccess();
    }

    /** Condition for redirect to validtion */
    if (prevProps.isRegistrationSuccess !== isRegistrationSuccess
      && isRegistrationSuccess) {
      this.updateState('activeTabIndex', 2);
      this.props.gtmActions.gtmRegistrationSuccess();
    }

    /** Condition for redirect to registration */
    if (prevProps.status !== status && status) {
      if (regStatus === constants.EMAILVERIFIED) {
        this.updateState('activeTabIndex', 1);
      } else {
        this.updateState('activeTabIndex', 3);
      }
    }

    /** Condition for change the scroll top for tab change */
    if (prevState.activeTabIndex !== activeTabIndex) {
      setTimeout(() => {
        scrollToTop();
      }, 100);
    }
  }

  /** @description callback function for clear error
   * @param {object} e - triggered event(click)
   * @param {func} action - action for clear error
   */
  clearError = (e, action) => {
    e.preventDefault();
    action();
  }

  verifyCallback = (response) => {
    this.recaptchaInstance.reset();
    if (response && this.callback) {
      this.callback({
        captchaResponse: response,
        ...this.values,
      });
      this.values = null;
      this.callback = null;
    }
  }

  submitHandler = (values, callback) => {
    const { disableCaptcha } = envConfig;
    const isDisableRecaptcha = JSON.parse(disableCaptcha);
    if (isDisableRecaptcha && callback) {
      callback(values);
      return;
    }
    this.values = values;
    this.callback = callback;
    this.recaptchaInstance.execute();
  }

  /** @description function to update local state value based on paramaters
   * @param {string} key - key to update
   * @param {string} value - value of current state
   */
  updateState = (key, value) => this.setState({ [key]: value });

  renderForm = () => {
    const { activeTabIndex } = this.state;
    switch (activeTabIndex) {
      case 0:
        return (
          <EmailVerificationContainer
            className="ems-emailverification"
            onSubmit={this.submitHandler}
          />);
      case 1:
        return (
          <RegistrationContainer
            className="ems-registration"
            onSubmit={this.submitHandler}
          />);
      case 2:
        return (
          <Validation
            className="ems-validation"
          />);
      case 3:
        return (
          <ConfirmationMessageContainer
            className="ems-registration-message"
            onSubmit={this.submitHandler}
          />);
      default:
        return null;
    }
  }

  /** @description function to render the error message for EMS */
  renderErrorMessage = () => {
    const { activeTabIndex } = this.state;
    const {
      isEmailFormError, isRegFormError, emailVerificationActions, registrationActions,
    } = this.props;
    let props = {};
    if (activeTabIndex === 0 || activeTabIndex === 3) {
      // props for email verification errors
      props = {
        error: isEmailFormError,
        onClear: (e) => this.clearError(e, emailVerificationActions.clearErrors),
      };
    } else {
      // props for registration errors
      props = {
        error: isRegFormError,
        onClear: (e) => this.clearError(e, registrationActions.clearErrors),
      };
    }

    return (<ErrorNotification {...props} />);
  }

  /** @description function to render the information text based on active tab */
  renderInformationText = () => {
    const { activeTabIndex } = this.state;
    switch (activeTabIndex) {
      case 0:
        return constants.EMAIL_VERIFICATION_INFORMATION_TEXT;
      case 1:
        return constants.REGISTRATION_INFORMATION_TEXT;
      default:
        return null;
    }
  }

  renderRecaptcha = () => (
    <ReCAPTCHA
      ref={(e) => { this.recaptchaInstance = e; }}
      size="invisible"
      sitekey={envConfig.googleRecaptchaApiKeys}
      onChange={this.verifyCallback}
    />
  );

  render() {
    const { disableCaptcha } = envConfig;
    const { isEmailFormLoading, isRegFormLoading } = this.props;
    const { activeTabIndex } = this.state;
    const isLoading = isEmailFormLoading || isRegFormLoading;
    const emsContainerClass = classNames('show-grid', { 'final-wizard': activeTabIndex === 2 });
    const colStyle = activeTabIndex !== 3 ? { lg: 8, lgOffset: 2 } : { xs: 12, md: 10, mdOffset: 1 };
    const informationText = this.renderInformationText();
    const isDisableRecaptcha = JSON.parse(disableCaptcha);
    return (
      <React.Fragment>
        <Helmet>
          <title>{pageTitle.SIGN_UP}</title>
          <meta name="robots" content="follow, index" />
          <meta name="url" content={RouteConstants.SIGNUP} />
          <meta name="language" content={metaTags.LANGUAGE} />
          <meta name="keywords" content={metaTags.KEYWORD} />
          <meta name="description" content={metaTags.DESCRIPTION} />
          <meta property="og:url" content={`${envConfig.rewardsBaseUrl}${RouteConstants.SIGNUP}`} />
          <link rel="canonical" href={`${envConfig.rewardsBaseUrl}${RouteConstants.SIGNUP}`} />
          <meta property="og:title" content={pageTitle.SIGN_UP} />
          <meta property="og:site_name" content={envConfig.rewardsBaseUrl} />
        </Helmet>
        <EmsHeader />
        <main className="ems-wrapper">
          {activeTabIndex !== null &&
            <Grid className="ems-container">
              <Row className={emsContainerClass}>
                <Col {...colStyle}>
                  <StepWizard step={activeTabIndex} />
                  {informationText && <p className="verification-process">{informationText}</p>}
                  {this.renderForm()}
                </Col>
              </Row>
            </Grid>}
          {isLoading && <Loader />}
          {this.renderErrorMessage()}
          {!isDisableRecaptcha && this.renderRecaptcha()}
        </main>
        <Footer />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { form } = state;
  const { registrationForm, emailVerificationForm } = form;
  const {
    done: isEmailVerificationSuccess,
    isLoading: isEmailFormLoading,
    error: isEmailFormError,
    status: emailFormStatus,
  } = emailVerificationForm;
  const {
    status,
    done: isRegistrationSuccess,
    isLoading: isRegFormLoading,
    error: isRegFormError,
  } = registrationForm;
  return {
    status,
    emailFormStatus,
    isEmailVerificationSuccess,
    isRegistrationSuccess,
    isEmailFormLoading,
    isRegFormLoading,
    isEmailFormError,
    isRegFormError,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    emailVerificationActions: bindActionCreators(emailVerificationActionsCreators, dispatch),
    registrationActions: bindActionCreators(registrationActionsCreators, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

/** PropTypes:
 * location - object - contains information about current route
 * registrationActions - object - actions for registration
 * emailVerificationActions - object - actions for email verification
 * gtmActions - object - actions for google tag manager
 * status - string - status of registration
 * isEmailVerificationSuccess - boolean - represents the verification api is success
 * isRegistrationSuccess - boolean - represents the registration api is success
 * isEmailFormLoading - boolean - flag to denotes the email verification is loading or not
 * isRegFormLoading - boolean - flag to denotes the registraion is loading or not
 * isEmailFormError - any - contains the error message for emailverification
 * isRegFormError - any - contains the error message for registration
 */
EmsContainer.propTypes = {
  location: PropTypes.object.isRequired,
  registrationActions: PropTypes.object.isRequired,
  emailVerificationActions: PropTypes.object.isRequired,
  gtmActions: PropTypes.object.isRequired,
  status: PropTypes.string,
  isEmailVerificationSuccess: PropTypes.bool,
  isRegistrationSuccess: PropTypes.bool,
  isEmailFormLoading: PropTypes.bool,
  isRegFormLoading: PropTypes.bool,
  isEmailFormError: PropTypes.any,
  isRegFormError: PropTypes.any,
};

const Component = withRouter(loadAsyncScript(
  EmsContainer,
  [
    `https://maps.googleapis.com/maps/api/js?key=${envConfig.googlePlaceApiKeys}&libraries=places&region=US`,
  ]
));

export default connect(mapStateToProps, mapDispatchToProps)(Component);
