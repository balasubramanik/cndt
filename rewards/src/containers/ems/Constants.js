const constants = {
  EMAIL_VERIFICATION_INFORMATION_TEXT: 'Please fill out the required fields to confirm your eligibility.',
  REGISTRATION_INFORMATION_TEXT: 'Please fill out the required fields to complete the registration process.',
  TERMS_AND_CONDITIONS_URL: 'https://webcsauthor.gaf.com/rewards-terms-and-conditions',
  EMAILVERIFIED: 'emailverified',
};

const errorMessages = {
  REQUIRED_FIELD: 'This field is required',
  REQUIRED_QUESTIONS: 'This question is required',
  INVALID_CONFIRM_EMAIL: 'Your emails do not match',
  INVALID_EMAIL_WITH_GAF_DOMAIN: 'Sorry, you can\'t sign up for a GAF Rewards account from a GAF email address. Please enter a valid email.',
  INVALID_EMAIL: 'Please enter a valid email address',
  INVALID_MOBILE_NUMBER: 'Please enter a valid phone number',
  TERMS_AND_CONDITIONS_REQUIRED: 'Please accept the Terms & Conditions',
};

const metaTags = {
  KEYWORD: 'GAF rewards sign up',
  DESCRIPTION: 'Sign up for free to join the GAF Rewards program today! Gain access to exclusive promotions and redeem your points for gift cards.',
  LANGUAGE: 'en-US',
};

export { constants, errorMessages, metaTags };
