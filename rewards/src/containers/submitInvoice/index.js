import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Grid, PanelGroup } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { history } from '../../routes';
import { submitInvoice } from './Constants';
import Accordion from '../../components/accordion';
import RoleAccessPopup from '../../components/roleAccessPopup';
import * as getProfileDetails from '../../actionCreators/MyProfile';
import UploadInvoice from '../../components/uploadInvoice';
import DistributorSubmission from '../../components/distributorSubmission';
import MailInvoice from '../../components/mailInvoice';
import RouteConstants from '../../constants/RouteConstants';
import { pageTitle } from '../../constants/constants';
import * as gtmActionCreators from '../../actionCreators/GTM';
import { deviceOrientationChange, deviceOrientationClear } from '../../utils/utils';

export class SubmitInvoice extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      uploadInvoiceSuccessFlag: false,
      activeAccordionKey: 1,
    };
  }

  componentDidMount() {
    deviceOrientationChange();
  }

  componentWillUnmount() {
    deviceOrientationClear();
  }

  onClaimStatusClick = (e) => {
    e.preventDefault();
    history.push({ pathname: RouteConstants.CLAIM_STATUS });
    /* Trigger the GTM action */
    this.props.gtmActions.gtmSuccessfulInvoice();
  }

  successCall = () => {
    this.setState({
      uploadInvoiceSuccessFlag: true,
    });
  }

  navigateCall = () => {
    this.setState({ activeAccordionKey: 1 });
  }

  handleAccordionSelect = (activeAccordionKey) => {
    this.setState({ activeAccordionKey });
    /* Trigger the GTM action */
    let accordian = '';
    switch (activeAccordionKey) {
      case 1:
        accordian = 'Upload Invoice';
        break;
      case 2:
        accordian = 'Mail Invoice';
        break;
      case 3:
        accordian = 'Distributor Submission';
        break;
      default:
        accordian = null;
    }
    this.props.gtmActions.gtmUploadInvoiceOptionsMenu(accordian);
  }

  confirmationMessage = () => (
    <Grid>
      <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={this.props.accessInfo} accessText={this.props.actions.accessPage} />
      <section className="invoice-submit-success">
        <div className="tick-icon">
          <p>
            <span className="icon-check"></span>
          </p>
        </div>
        <h2>{submitInvoice.SUCCESSTITLE}</h2>
        <h3>{submitInvoice.SUCCESSCONTENT1}</h3>
        <p>{submitInvoice.SUCCESSCONTENT3}<a href="/" onClick={(e) => this.onClaimStatusClick(e)}>{submitInvoice.LINKFORCLAIM}</a>{submitInvoice.SUCCESSCONTENT4}</p>
      </section>
    </Grid>
  );

  submitInvoiceRender = () => {
    const { userInfo } = this.props;
    return (
      <div>
        <Grid>
          <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={this.props.accessInfo} accessText={this.props.actions.accessPage} />
          <div className="gaf-title-block">
            <h2>{submitInvoice.TITLE}</h2>
            <p>{submitInvoice.CONTENT}</p>
            <p>{submitInvoice.PLEASENOTE}</p>
          </div>
          <div className="submit-invoice-accordian">
            <div className="gaf-accordian">
              <PanelGroup
                accordion
                id="accordion-controlled-example"
                defaultActiveKey={1}
                activeKey={this.state.activeAccordionKey}
                onSelect={this.handleAccordionSelect}
              >
                <Accordion
                  key="1"
                  index={1}
                  title={submitInvoice.ACCORDIONTITLE1}
                  body={
                    <UploadInvoice callback={this.successCall} userInfo={userInfo} isOpened={this.state.activeAccordionKey === 1} gtmUploadInvoiceActions={this.props.gtmActions}></UploadInvoice>
                  }
                >
                </Accordion>
                <Accordion
                  key="2"
                  index={2}
                  title={submitInvoice.ACCORDIONTITLE2}
                  body={
                    <MailInvoice callback={this.navigateCall} gtmUploadInvoiceActions={this.props.gtmActions}></MailInvoice>
                  }
                >
                </Accordion>
                <Accordion
                  key="3"
                  title={submitInvoice.ACCORDIONTITLE3}
                  body={
                    <DistributorSubmission gtmUploadInvoiceActions={this.props.gtmActions}></DistributorSubmission>
                  }
                  index={3}
                >
                </Accordion>
              </PanelGroup>
            </div>
          </div>
        </Grid>
      </div>
    );
  }

  render() {
    return (
      <main className="main-content">
        <Helmet>
          <title>{pageTitle.SUBMITINVOICE}</title>
        </Helmet>
        {
          this.state.uploadInvoiceSuccessFlag ?
            this.confirmationMessage() :
            this.submitInvoiceRender()
        }
      </main>
    );
  }
}

function mapStateToProps(state) {
  const { myProfileState } = state;
  const { accessInfo, userInfo } = myProfileState;
  return {
    accessInfo,
    userInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(getProfileDetails, dispatch),
    gtmActions: bindActionCreators(gtmActionCreators, dispatch),
  };
}

/** PropTypes:
 * accessInfo - bool for the Toast message
 */

SubmitInvoice.propTypes = {
  accessInfo: PropTypes.bool,
  actions: PropTypes.object,
  userInfo: PropTypes.object,
  gtmActions: PropTypes.object,
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SubmitInvoice));
