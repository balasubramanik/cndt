import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import SubmitInvoice from '../submitInvoice';

configure({ adapter: new Adapter() });

describe('SubmitInvoice', () => {
  const tree = shallow(<SubmitInvoice />);

  it('should be defined', () => {
    expect(SubmitInvoice).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
    tree.instance().successCall();
    tree.instance().navigateCall();
    tree.instance().handleSelect(1);
  });
});
