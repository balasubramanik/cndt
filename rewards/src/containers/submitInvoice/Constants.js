const submitInvoice = {
  TITLE: 'Submit Invoices',
  CONTENT: 'You only have to submit your qualifying invoices once! Earn the correct amount of GAF Reward Points for the current promotions for which the invoice qualifies.',
  PLEASENOTE: 'Please note: All invoices must be submitted within 90 days of purchase date. Invoices older than 90 days will not be accepted.',
  SUCCESSTITLE: 'Success!',
  SUCCESSCONTENT1: 'Your invoice(s) have been successfully submitted for processing.',
  SUCCESSCONTENT3: 'Check the ',
  SUCCESSCONTENT4: ' page for details.',
  LINKFORCLAIM: 'Claim Status',
  ACCORDIONTITLE1: 'Upload Invoice',
  ACCORDIONTITLE2: 'Mail Invoice',
  ACCORDIONTITLE3: 'Distributor Submission',
};

export { submitInvoice };
