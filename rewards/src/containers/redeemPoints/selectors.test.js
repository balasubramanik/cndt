import test from 'tape';
import { getRewardsInfo, getCartBalance, getAvailableBalance } from './selectors';

const t = test('test utils', (b) => b);

describe('Redemption form Selectors', () => {
  it('should return RedeemPointsState', () => {
    const mock = {
      myProfileState: {
        userInfo: {},
        rewardsInfo: {
          rewardPoints: '1200',
        },
      },
    };
    t.deepEqual(mock.myProfileState.rewardsInfo, getRewardsInfo(mock));
  });

  it('should return cart balance', () => {
    const mock = {
      headerState: {
        availableBalance: 1000,
      },
    };
    t.deepEqual(mock.headerState.availableBalance, getCartBalance(mock));
  });

  it('should return availableBalance', () => {
    const mock = {
      myProfileState: {
        userInfo: {},
        rewardsInfo: {
          rewardPoints: '1200',
        },
      },
      headerState: {
        availableBalance: 1000,
      },
    };
    t.deepEqual(200, getAvailableBalance(mock));
  });
});
