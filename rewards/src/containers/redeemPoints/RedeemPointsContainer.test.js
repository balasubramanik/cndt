import React from 'react';
import { configure, mount, shallow } from 'enzyme';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import RedeemPointsCont, { RedeemPointsContainer } from '../../containers/redeemPoints';
import { history } from '../../routes';
import { fakeStore } from '../../config/jest/fakeStore';


configure({ adapter: new Adapter() });

history.push = jest.fn();
jest.useFakeTimers();
describe('RedeemPointsContainer', () => {
  const addToCart = jest.fn();
  const closePopup = jest.fn();
  const clearErrors = jest.fn();
  const getUsers = jest.fn();
  const getGiftCatalogue = jest.fn();
  const event = {
    preventDefault: jest.fn(),
  };
  const props = {
    headerActions: {
      updateCartItem: jest.fn(),
      getCartCount: jest.fn(),
    },
    myprofileActions: {
      accessPage: jest.fn(),
      getRewardsDetails: jest.fn(),
    },
    userInfo: {
      isAchEnabled: true,
    },
    rewardsInfo: {
      rewardPoints: '1000',
    },
    gtmActions: {
      gtmProceedToCheckout: jest.fn(),
      gtmProceedToCheckoutItemCount: jest.fn(),
      gtmProceedToCheckoutOrderTotal: jest.fn(),
      gtmGiftCardCategory: jest.fn(),
    },
    debitCardInfo: [{
      listprice: '0.0000',
      cardType: 'E card',
      feeType: '$',
      feeValue: '1.0',
      giftCardValue: '25.0000',
      feeDescription: 'Aéropostale - $26',
      imageicon: null,
      max: '100.0',
      min: '1.0',
      minmaxtype: 'QTY',
      Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
      DisplayName: 'Aéropostale - $26',
    }],
    contractorUserActions: {
      getUsers,
    },
    redeemPointsActions: {
      addToCart,
      closePopup,
      clearErrors,
      getGiftCatalogue,
    },
    values: {},
    availableBalance: 1000,
    isLoading: true,
    isShowPopup: true,
    debitCardInfoError: {
      error: 'Internal Server Error',
    },
    giftCards: {
      variantExternalID: '639e9d4d-7453-4593-9339-8604122d68e0',
      productExternalID: '19e95c3a-6968-4b26-8952-5d8731ff032d',
      variantID: 'MER1001513-2',
      variants: {
        listprice: '0.0000',
        cardType: 'E card',
        feeType: '$',
        feeValue: '1.0',
        giftCardValue: '25.0000',
        feeDescription: 'Aéropostale - $26',
        imageicon: null,
        max: '100.0',
        min: '1.0',
        minmaxtype: 'QTY',
        Variant_Images: 'https://dmyxigrg1v9vl.cloudfront.net/images/merchant-cards/aero.png',
        DisplayName: 'Aéropostale - $26',
      },
    },
    achInfoError: {
      error: 'Internal Server Error',
    },
  };
  const tree = shallow(<RedeemPointsContainer
    {...props}
  />);
  it('should be defined', () => {
    expect(RedeemPointsContainer).toBeDefined();
  });

  it('should render correctly', () => {
    expect(tree).toMatchSnapshot();
  });

  it('should render Redeem Points container', () => {
    expect(tree.find('.redeem-points-main').length).toBe(1);
  });

  it('should render DebitCard', () => {
    expect(tree.find('.debit-card').length).toBe(1);
  });

  it('should render Ach', () => {
    expect(tree.find('.ach').length).toBe(1);
  });

  it('should render Loader', () => {
    expect(tree.find('Loader').length).toBe(1);
  });

  it('should call clearError', () => {
    const simulateClick = tree.find('ErrorNotification').prop('onClear');
    simulateClick(event);
    expect(clearErrors).toBeCalled();
  });

  it('should call addToCartHandler for debitcard', () => {
    const simulateClick = tree.find('.debit-card').prop('onSubmit');
    simulateClick('debit', 'debitCardInfo', {});
    expect(addToCart).toBeCalled();
  });

  it('should call addToCartHandler for ach', () => {
    const simulateClick = tree.find('.ach').prop('onSubmit');
    simulateClick('check', 'achInfo', {});
    expect(addToCart).toBeCalled();
  });

  it('should redirect to cartsummary', () => {
    const simulateClick = tree.find('Button').prop('onClick');
    simulateClick();
    expect(history.push).toBeCalled();
  });

  it('should call component did update', () => {
    const instance = tree.instance();
    instance.componentDidUpdate({
      isShowPopup: false,
    });
    jest.runAllTimers();
    expect(closePopup).toBeCalled();
  });
});

describe('RedeemPointsContainer', () => {
  const addToCart = jest.fn();
  const closePopup = jest.fn();
  const clearErrors = jest.fn();
  const getUsers = jest.fn();
  const getGiftCatalogue = jest.fn();
  const redeemPointsActions = {
    addToCart,
    closePopup,
    clearErrors,
    getGiftCatalogue,
  };
  const myprofileActions = {
    accessPage: jest.fn(),
    getRewardsDetails: jest.fn(),
  };
  const headerActions = {
    updateCartItem: jest.fn(),
    getCartCount: jest.fn(),
  };
  const contractorUserActions = {
    getUsers,
  };
  const state = {
    contractorUserState: {
      users: [],
    },
    myProfileState: {
      rewardsInfo: {
        rewardPoints: '1000',
      },
      userInfo: {},
    },
    headerState: {
      availableBalance: 100,
    },
    form: {
      achForm: {},
      debitCardForm: {},
    },
    redeemPointsState: {
      cardHolderName: 'john',
      availableBalance: 1000,
      debitCardInfo: [{
        products: [{
          productSpecification: {
            descriptiontypeecard: 'description1',
            termsconditionecard: 'description2',
            termsconditionphysical: 'description3',
          },
          variants: [{
            variants: {
              listprice: '0.000',
              cardType: 'Physical Card',
              feeType: '$',
              feeValue: '2.5',
              giftCardValue: null,
              imageicon: null,
              max: '99,999.00',
              min: '25.00',
              minmaxtype: '$',
              Variant_Images: null,
              DisplayName: 'VISA Reloadble Debit Card',
            },
          }],
        }],
      }],
      isLoading: true,
      isShowPopup: true,
      achInfoError: {
        error: 'Internal Server Error',
      },
    },
  };
  const gtmActions = {
    gtmProceedToCheckout: jest.fn(),
    gtmProceedToCheckoutItemCount: jest.fn(),
    gtmProceedToCheckoutOrderTotal: jest.fn(),
    gtmGiftCardCategory: jest.fn(),
  };
  const store = fakeStore(state);
  const tree = mount(
    <Provider store={store}>
      <Router history={history}>
        <RedeemPointsCont
          headerActions={headerActions}
          redeemPointsActions={redeemPointsActions}
          contractorUserActions={contractorUserActions}
          myprofileActions={myprofileActions}
          gtmActions={gtmActions}
        />
      </Router>
    </Provider>
  );

  it('should be defined', () => {
    expect(RedeemPointsCont).toBeDefined();
  });

  it('should render Redeem Points container', () => {
    expect(tree.find('.redeem-points-main').length).toBe(1);
  });
});
