import { createSelector } from 'reselect';
import { convertStringToNumber } from '../../utils/utils';

export const getRewardsInfo = (state) => state.myProfileState.rewardsInfo;
export const getCartBalance = (state) => state.headerState.availableBalance;

export const getAvailableBalance = createSelector(
  [getRewardsInfo, getCartBalance],
  (rewardsInfo, cartBalance) => {
    const rewardPoints = convertStringToNumber(rewardsInfo.rewardPoints);
    const availableBalance = rewardPoints - cartBalance;
    return availableBalance;
  }
);
