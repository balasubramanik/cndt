const constants = {
  REDEEM_POINTS: 'Redeem Points',
  AVAILABLE_POINTS: 'Available Points',
  ADD_TO_CART_SUCCESS: 'Product has been added to your cart successfully',
  TYPE: {
    DEBIT: 'debit',
    CHECK: 'check',
    GIFT_CARDS: 'giftcards',
  },
  DEBIT_CARD_INFO: 'debitCardInfo',
  ACH_INFO: 'achInfo',
  GIFT_CARDS: 'giftCards',
  PROCEED_TO_CHECKOUT: 'Proceed to Checkout',
  DATE_FORMAT: 'MM/DD/YYYY',
  TIME_STAMP_FORMAT: 'x',
};

export { constants };
