import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Helmet } from 'react-helmet';
import { Col, Grid, Row, Button } from 'react-bootstrap';
import * as redeemPointsActionsCreators from '../../actionCreators/RedeemPoints';
import * as contractorUserActionsCreators from '../../actionCreators/ContractorUser';
import * as headerActionsCreators from '../../actionCreators/Header';
import * as myprofileActionsCreators from '../../actionCreators/MyProfile';
import { constants } from './constants';
import { pageTitle } from '../../constants/constants';
import Loader from '../../components/loader';
import ErrorNotification from '../../components/errorNotification';
import Ach from '../../components/ach';
import DebitCard from '../../components/debitCard';
import Dialog from '../../components/dialog';
import GiftCards from '../../components/giftCards';
import RoleAccessPopup from '../../components/roleAccessPopup';
import { formatCurrency, roundToTwoDecimal, createListener, removeListener, formatUSCurrency } from '../../utils/utils';
import { authentication, history } from '../../routes';
import RouteConstants from '../../constants/RouteConstants';
import { getCategories, getGiftCardDetails } from '../../components/giftCardRedemption/selectors';
import * as gtmActionsCreators from '../../actionCreators/GTM';

const SUCCESS_TIMEOUT = 2000;

export class RedeemPointsContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.cartItems = null;
    this.selectedProduct = null;
    this.state = {
      isBottomReached: false,
    };
  }

  /** @description React Life cycle method
   * it will invoke, when component is mounted.
   */
  componentDidMount() {
    const {
      users, redeemPointsActions, contractorUserActions, userInfo, headerActions, myprofileActions,
    } = this.props;
    const { getGiftCatalogue } = redeemPointsActions;
    if (!users || users.length === 0) {
      contractorUserActions.getUsers();
    }
    if (userInfo.isAchEnabled) {
      getGiftCatalogue(constants.TYPE.CHECK, constants.ACH_INFO);
    }
    redeemPointsActions.clearErrors();
    myprofileActions.getRewardsDetails();
    getGiftCatalogue(constants.TYPE.DEBIT, constants.DEBIT_CARD_INFO);
    getGiftCatalogue(constants.TYPE.GIFT_CARDS, constants.GIFT_CARDS);
    headerActions.getCartCount();
    createListener(null, ['scroll'], this.scrollHandler);
  }

  /** @description React Life cycle method
   * it will invoke, when component is updated.
   * @param {object} prevProps - previous props of component
   */
  componentDidUpdate(prevProps) {
    const { isShowPopup } = this.props;
    if (prevProps.isShowPopup !== isShowPopup && isShowPopup) {
      this.closePopupHandler();
    }
  }

  /** @description React Life cycle method
   * it will invoke, when component is unmounted.
   */
  componentWillUnmount() {
    removeListener(null, ['scroll'], this.scrollHandler);
  }

  /** @description Function to format the debit card details
   * @param {object} userDetails - user information
   * @param {object} values - form data
   */
  formatDebitCardDetails = (userDetails, values, type) => {
    const { cartItems } = this.props;
    const { impersonatedBy, email } = authentication.getAuthDetails();
    const { items = [] } = cartItems || {};
    const {
      productId, productName,
      productType, feeType,
      cardTypeDesc, feeTypeDesc,
      cardValue, fee,
      debitCardUserName,
      debitCardIssuedTo,
      quantity,
      feeValue,
      min,
      max,
      isReloadable,
      variantId,
      minMaxType,
    } = values;
    const totalPoints = (cardValue * quantity) + fee;
    this.selectedProduct = {
      min,
      max: type === constants.TYPE.GIFT_CARDS ? max : formatUSCurrency(max),
      category: type === constants.TYPE.GIFT_CARDS ? type : '',
    };

    const cartData = [...items].map((list) => ({ ...list, img: null }));

    const addedItems = cartData.concat([{
      productId,
      variantId,
      feeType,
      productName,
      cardType: productType,
      cardTypeDesc,
      cardValue,
      quantity,
      feeRate: feeValue,
      feeValue: fee,
      min,
      max,
      minmaxType: minMaxType,
      feeTypeDesc,
      category: type,
      totalValue: totalPoints,
      debitCardUserName,
      debitCardIssuedTo,
      isReloadableDebitCard: isReloadable,
      createdBy: email,
      modifiedBy: impersonatedBy || email,
      impersonatedBy,
    }]);
    return {
      ...cartItems,
      ...userDetails,
      items: addedItems,
    };
  }

  /** @description callback function for scroll event */
  scrollHandler = () => {
    const { isBottomReached } = this.state;
    const scrollTop = document.body.scrollTop || document.documentElement.scrollTop;
    const offset = scrollTop + window.innerHeight;
    const height = this.redeemPointsContainer.offsetTop + this.redeemPointsContainer.offsetHeight + this.proceedToCheckout.offsetHeight;
    let isBottom = false;
    if (offset >= height) {
      isBottom = true;
    }
    if (isBottom !== isBottomReached) {
      removeListener(null, ['scroll'], this.scrollHandler);
      this.setState({ isBottomReached: isBottom }, () => {
        createListener(null, ['scroll'], this.scrollHandler);
      });
    }
  }

  /** @description Function to format the debit card details
   * @param {object} userDetails - user information
   * @param {object} values - form data
   */
  formatRequestBody = (type, values) => {
    const { userInfo } = this.props;
    const { contractorId } = authentication.getAuthDetails();
    const userDetails = {
      firstName: userInfo.firstName,
      lastName: userInfo.lastName,
      contractorAccountId: contractorId,
    };
    this.cartItems = this.formatDebitCardDetails(userDetails, values, type);
    return this.cartItems;
  }

  /** @description callback function for proceed to checkout */
  proceedToCheckoutHandler = () => {
    const { cartItemsCount, gtmActions, cartItems } = this.props;
    const { orderTotal } = cartItems;
    gtmActions.gtmProceedToCheckout();
    gtmActions.gtmProceedToCheckoutItemCount(cartItemsCount);
    gtmActions.gtmProceedToCheckoutOrderTotal(orderTotal);
    history.push(RouteConstants.CARTSUMMARY);
  }

  /** @description Callback function for add to cart button
   * @param {string} type - category (debit, ach, gift cards)
   * @param {string} key
   * @param {object} values - values of particular form
   */
  addToCartHandler = (type, key, values) => {
    const { redeemPointsActions } = this.props;
    const reqBody = this.formatRequestBody(type, values);
    redeemPointsActions.addToCart(type, reqBody);
  }

  /** @description callback function for close success popup
   */
  closePopupHandler = () => {
    const { redeemPointsActions } = this.props;
    setTimeout(() => {
      redeemPointsActions.closePopup();
    }, SUCCESS_TIMEOUT);
  }

  /** @description callback function for clear error
   * @param {object} e - triggered event(click)
   * @param {func} action - action for clear error
   */
  clearError = (e, action) => {
    e.preventDefault();
    action();
  }

  /** @description function to render the error message for Redeem points */
  renderErrorMessage = () => {
    const {
      debitCardInfoError, achInfoError, addToCartError, redeemPointsActions, detailsError,
    } = this.props;
    const props = {
      error: debitCardInfoError || achInfoError || addToCartError || detailsError,
      onClear: (e) => this.clearError(e, redeemPointsActions.clearErrors),
      data: this.selectedProduct,
    };
    return (<ErrorNotification {...props} />);
  }

  /** @description function to render the success message popup */
  renderModal = () => {
    const { isShowPopup } = this.props;
    const toastBody = (
      <div className="gaf-toast-dialog">
        <span className="icon-check"></span>
        <p>{constants.ADD_TO_CART_SUCCESS}</p>
      </div>);
    return (
      <Dialog
        className="gaf-model-popup redeempoints-success gaf-toast-main"
        body={toastBody}
        show={isShowPopup}
      />
    );
  }

  /** @description function to render the proceed to checkout button */
  renderProceedToCheckout = () => {
    const { isBottomReached } = this.state;
    const { cartItemsCount } = this.props;
    const proceedToCheckoutClass = classNames('proceed-btn', { 'p-btn-fixed': !isBottomReached });
    return (
      <div ref={(ele) => { this.proceedToCheckout = ele; }} className={proceedToCheckoutClass}>
        <Grid>
          <Row>
            <Col md={12}>
              <Button
                id="proceed-to-checkout"
                className="btn gaf-btn-primary"
                disabled={cartItemsCount === 0}
                onClick={this.proceedToCheckoutHandler}
              >
                {constants.PROCEED_TO_CHECKOUT}
              </Button>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

  /** @description function to render the information text with available balance */
  renderInformationText = () => {
    const { rewardsInfo } = this.props;
    const balance = formatCurrency(roundToTwoDecimal(rewardsInfo.rewardPoints));
    const availbleBalance = balance || roundToTwoDecimal(0);
    return (
      <div className="gaf-title-block">
        <Row className="show-grid redeem-points-block">
          <Col md={8}>
            <h2>{constants.REDEEM_POINTS}</h2>
          </Col>
          <Col md={4}>
            <p className="avail-balance" id="avail-balance-text">{constants.AVAILABLE_POINTS} <span id="points">{availbleBalance}</span></p>
          </Col>
        </Row>
      </div>
    );
  }

  render() {
    const {
      isLoading, giftCards, userInfo, debitCardInfo, accessInfo, myprofileActions,
      redeemPointsActions, giftCardDetails, gtmActions,
    } = this.props;
    return (
      <main className="main-content redeem-points-main">
        <Helmet>
          <title>{pageTitle.REDEEMPOINTS}</title>
        </Helmet>
        <section>
          <div ref={(ele) => { this.redeemPointsContainer = ele; }}>
            <RoleAccessPopup icon="icon-check-circle check-icon" className="toast-info-icon" show={accessInfo} accessText={myprofileActions.accessPage} />
            <Grid>
              <Row className="show-grid">
                <Col xs={12}>
                  {this.renderInformationText()}
                  {debitCardInfo && debitCardInfo.length > 0 &&
                    <DebitCard className="debit-card" onSubmit={this.addToCartHandler} />}
                  {userInfo.isAchEnabled &&
                    <Ach className="ach" onSubmit={this.addToCartHandler} />}
                  {giftCards &&
                    <GiftCards
                      categories={giftCards}
                      details={giftCardDetails}
                      getDetails={redeemPointsActions.getGiftCardDetails}
                      onSubmit={this.addToCartHandler}
                      gtmActions={gtmActions}
                    />}
                </Col>
              </Row>
            </Grid>
          </div>
          {this.renderProceedToCheckout()}
          {isLoading && <Loader />}
          {this.renderErrorMessage()}
          {this.renderModal()}
        </section>
      </main>

    );
  }
}

function mapStateToProps(state) {
  const {
    redeemPointsState, myProfileState, headerState, contractorUserState,
  } = state;
  const {
    debitCardInfoError,
    achInfoError,
    addToCartError,
    isLoading,
    isShowPopup,
    redemptionInfo,
    debitCardInfo,
    detailsError,
  } = redeemPointsState;
  const { users, isLoading: isUserDataLoading } = contractorUserState;
  const { userInfo, rewardsInfo, accessInfo } = myProfileState;
  const { cartItemsCount, cartItems } = headerState;
  return {
    debitCardInfoError,
    achInfoError,
    addToCartError,
    detailsError,
    isLoading: isLoading || isUserDataLoading,
    isShowPopup,
    userInfo,
    giftCards: getCategories(state),
    giftCardDetails: getGiftCardDetails(state),
    redemptionInfo,
    users,
    cartItemsCount,
    rewardsInfo,
    cartItems,
    accessInfo,
    debitCardInfo,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    redeemPointsActions: bindActionCreators(redeemPointsActionsCreators, dispatch),
    contractorUserActions: bindActionCreators(contractorUserActionsCreators, dispatch),
    headerActions: bindActionCreators(headerActionsCreators, dispatch),
    myprofileActions: bindActionCreators(myprofileActionsCreators, dispatch),
    gtmActions: bindActionCreators(gtmActionsCreators, dispatch),
  };
}

/** PropTypes:
 * redeemPointsActions - object - actions for redeem points
 * contractorUserActions - object - actions for contractor user
 * getRewardsInfo - func - function to trigger the rewards info api call
 * headerActions - object - actions for header
 * myprofileActions - object - actions for myprofile
 * gtmActions - object - actions for google tag manager
 * isLoading - boolean - flag to denotes the redeem points is loading or not
 * debitCardInfoError - any - contains the error message for debit card
 * achInfoError - any - contains the error message for ach
 * detailsError - any  - contains the error message for gift card details
 * addToCartError - any - contains the error message for add to cart
 * isShowPopup - boolean - flag to show/hide the success popup
 * giftCards - object - contains the gift card details
 * giftCardDetails - object - contains the gift card redemption details
 * redemptionInfo - object - contains the information about redemption
 * userInfo - object - contains the information about loggedin user
 * cartItemsCount - number - contains the added cart item count
 * cartItems - object - contains the cart items
 * accessInfo - object - Contains the information about the user access
 * debitCardInfo - array - contains the debitcard information
 */
RedeemPointsContainer.propTypes = {
  redeemPointsActions: PropTypes.object.isRequired,
  contractorUserActions: PropTypes.object.isRequired,
  headerActions: PropTypes.object.isRequired,
  myprofileActions: PropTypes.object.isRequired,
  gtmActions: PropTypes.object.isRequired,
  isLoading: PropTypes.bool,
  debitCardInfoError: PropTypes.any,
  achInfoError: PropTypes.any,
  addToCartError: PropTypes.any,
  detailsError: PropTypes.any,
  isShowPopup: PropTypes.bool,
  giftCards: PropTypes.array,
  giftCardDetails: PropTypes.object,
  redemptionInfo: PropTypes.object,
  userInfo: PropTypes.object,
  users: PropTypes.array,
  cartItemsCount: PropTypes.number,
  rewardsInfo: PropTypes.object,
  cartItems: PropTypes.object,
  accessInfo: PropTypes.bool,
  debitCardInfo: PropTypes.array,
};

export default connect(mapStateToProps, mapDispatchToProps)(RedeemPointsContainer);
