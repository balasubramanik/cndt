import { headerWatchers } from './sagas/Header';
import { myProfileWatchers } from './sagas/MyProfile';
import { emailVerificationWatchers } from './sagas/EmailVerification';
import { registrationWatchers } from './sagas/Registration';
import { whatQualifiesWatcher } from './sagas/WhatQualifies';
import { spotPromotionWatcher } from './sagas/SpotPromotion';
import { spotPromotionDetailsWatcher } from './sagas/SpotPromotionDetails';
import { termsAndConditionsWatcher } from './sagas/TermsAndCondition';
import { contractorUserWatcher } from './sagas/ContractorUser';
import { redeemPointsWatchers } from './sagas/RedeemPoints';
import { searchByWatcher } from './sagas/SearchBy';
import { SpotPromotionManagementWatchers } from './sagas/SpotPromotionManagement';
import { cartSummaryWatcher } from './sagas/cartSummary';
import { pointsHistoryWatcher } from './sagas/PointsHistory';
import { orderDetailsWatcher } from './sagas/OrderDetails';
import { claimDetailsWatcher } from './sagas/ClaimDetails';
import { claimsListWatcher } from './sagas/ClaimsList';
import { contactUsWatcher } from './sagas/ContactUs';
import { claimInvoiceDetailsWatcher } from './sagas/ClaimInvoiceDetails';
import { contractorListWatchers } from './sagas/ContractorList';
import { statesWatcher } from './sagas/States';

export default function* rootWatchers() {
  yield [
    headerWatchers(),
    myProfileWatchers(),
    emailVerificationWatchers(),
    registrationWatchers(),
    whatQualifiesWatcher(),
    spotPromotionWatcher(),
    spotPromotionDetailsWatcher(),
    termsAndConditionsWatcher(),
    contractorUserWatcher(),
    redeemPointsWatchers(),
    searchByWatcher(),
    SpotPromotionManagementWatchers(),
    cartSummaryWatcher(),
    pointsHistoryWatcher(),
    orderDetailsWatcher(),
    claimDetailsWatcher(),
    claimsListWatcher(),
    contactUsWatcher(),
    claimInvoiceDetailsWatcher(),
    contractorListWatchers(),
    statesWatcher(),
  ];
}
