import moment from 'moment';

const events = ['mousemove', 'keydown', 'wheel', 'DOMMouseScroll', 'mouseWheel', 'mousedown', 'touchstart', 'touchmove', 'MSPointerDown', 'MSPointerMove'];

export const createListener = (ele, eType, callBack) => {
  const element = ele || window;
  const eventType = eType || events;
  eventType.forEach((e) => element.addEventListener(e, callBack));
};

export const removeListener = (ele, eType, callBack) => {
  const element = ele || window;
  const eventType = eType || events;
  eventType.forEach((e) => element.removeEventListener(e, callBack));
};

export const formatUSNumber = (value) => {
  if (!value) {
    return '';
  }
  return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const formatDateUTC = (value) => {
  if (!value) {
    return '';
  }
  return moment.utc(value).format('MM/DD/YYYY');
};

export const formatDate = (value) => {
  if (!value) {
    return '';
  }
  return moment(value).format('MM/DD/YYYY');
};

export const formatPhoneNumber = (value) => {
  if (!value) {
    return '';
  }
  let str = value.replace(/\D/g, '');
  str = (/(\d{0,10})/g).exec(str.replace(/[^\d.]/g, ''));
  return str[0] || '';
};

export const formatUSPhoneNumber = (value = '') => {
  const val = formatPhoneNumber(value);
  const formatPhoneNo = val.match(/^(\d{3})(\d{3})(\d{4})$/);
  return (!formatPhoneNo) ? val : `(${formatPhoneNo[1]}) ${formatPhoneNo[2]}-${formatPhoneNo[3]}`;
};

export const formatPoint = (point) => {
  let value = point;
  if (!value) {
    value = 0;
  }
  return Number(parseFloat(value).toFixed(2)).toLocaleString('en', {
    minimumFractionDigits: 2,
  });
};

export const ascending = (key) =>
  (a, b) => {
    if (a[key] === null) return 1;
    else if (b[key] === null) return -1;
    else if (a[key].toString().toLowerCase() < b[key].toString().toLowerCase()) return -1;
    else if (a[key].toString().toLowerCase() > b[key].toString().toLowerCase()) return 1;
    return 0;
  };

export const descending = (key) =>
  (a, b) => {
    if (a[key] === null) return 1;
    else if (b[key] === null) return -1;
    else if (a[key].toString().toLowerCase() < b[key].toString().toLowerCase()) return 1;
    else if (a[key].toString().toLowerCase() > b[key].toString().toLowerCase()) return -1;
    return 0;
  };

export const formatAddress = (addressComponent) => {
  const address = {};
  if (!addressComponent) {
    return address;
  }
  addressComponent.forEach((value) => {
    const { types } = value;
    const longName = value.long_name;
    const shortName = value.short_name;
    const isStreetName = types.indexOf('street_number') > -1;
    const isRoute = types.indexOf('route') > -1;
    const subLocality = types.indexOf('sublocality_level_1') > -1;
    const isNeighborhoodAddress = types.indexOf('neighborhood') > -1;
    const isLocality = types.indexOf('locality') > -1;
    const isState = types.indexOf('administrative_area_level_1') > -1;
    const isPostalCode = types.indexOf('postal_code') > -1;

    if (isStreetName) address.address1 = longName;
    if (isRoute) address.address1 = `${address.address1 || ''} ${longName}`;
    if (isNeighborhoodAddress) address.neighborhood = longName;
    if (subLocality) address.subLocality = longName;
    if (isLocality) address.locality = longName;
    if (isState) address.state = shortName;
    if (isPostalCode) address.zip = longName.substring(0, 5);
  });
  const { neighborhood, subLocality, locality } = address;
  return {
    ...address,
    city: subLocality || neighborhood || locality,
  };
};

export const getQueryString = (field, url) => {
  const reg = new RegExp(`[?&]${field}=([^&#]*)`, 'i');
  const string = reg.exec(url);
  return string ? string[1] : null;
};

export const roundToTwoDecimal = (value) => {
  let amount = value;
  if (!amount) {
    amount = 0;
  }
  return Number(amount).toFixed(2);
};

export const formatCurrency = (value) => {
  if (!value) {
    return '';
  }
  const amount = (/(\d{0,})[^.]*((?:\.\d{0,2})?)/g).exec(value.toString().replace(/[^\d.]/g, ''));
  const str = value.toString();
  const minusSymbol = str.indexOf('-') === 0 && str.length > 1 ? '-' : '';
  return formatUSNumber(minusSymbol + amount[1] + amount[2]);
};

export const checkPOAddress = (address) => {
  if (!address) {
    return '';
  }
  const addressRegex = new RegExp(/^ *((.*p[ \\.]? ?(o|0)[-. \\/\\]? *-?((box|bin)|b|(#|num)?\d+))|(p(ost)? *(o(ff(ice)?)?)? *((box|bin)|b)? *\d+)|(p *-?\/?(o)? *-?box)|post office box)/igm);
  return addressRegex.test(address);
};

export const formatUSCurrency = (value) => {
  if (!value) {
    return '';
  }
  const amount = formatCurrency(value);
  if (amount) {
    if (amount < 0) {
      return `-$${Math.abs(amount)}`;
    }
    return `$${amount}`;
  }
  return '';
};

export const removeSpecialChar = (value) => {
  if (!value) {
    return '';
  }
  return value.replace(/[\s$%,]/g, '');
};

export const convertStringToNumber = (value) => {
  if (!value) {
    return 0;
  }
  const val = formatCurrency(value);
  return Number(removeSpecialChar(val));
};

export const allowOnlyNumber = (value) => {
  if (!value) {
    return '';
  }
  return value.replace(/[^\d]/g, '');
};

export const truncateZero = (value) => {
  if (!value) {
    return '';
  }
  let val = Number(value.toFixed(2)).toString();
  if (val.indexOf('.') === -1) {
    val += '.0';
  }
  return val;
};

export const calculateFee = (value, feeValue, feeType, additionalFee = 0) => {
  let fee = Number(value);
  switch (feeType) {
    case '%':
      fee *= (feeValue / 100);
      break;
    case '$':
      fee *= feeValue;
      break;
    default:
      fee = feeValue;
  }
  if (fee) {
    fee += additionalFee;
  }
  return roundToTwoDecimal(fee);
};

export const sortByDate = (list, orderType, dateFiled) => {
  if (orderType === 'ascending') {
    return list.sort((a, b) => new Date(a[dateFiled]) - new Date(b[dateFiled]));
  }
  return list.sort((a, b) => new Date(b[dateFiled]) - new Date(a[dateFiled]));
};

export const setContractorInfo = (search) => {
  const customerID = getQueryString('customerID', search);
  const impersonatedBy = getQueryString('impersonatedBy', search);
  const impersonatedTo = getQueryString('impersonatedTo', search);
  if (customerID) {
    sessionStorage.setItem('contractorId', customerID);
  }
  if (impersonatedBy) {
    sessionStorage.setItem('impersonatedBy', impersonatedBy);
  }
  if (impersonatedTo) {
    sessionStorage.setItem('impersonatedTo', impersonatedTo);
  }
};

export const removeContractorInfo = () => {
  sessionStorage.removeItem('contractorId');
  sessionStorage.removeItem('impersonatedBy');
  sessionStorage.removeItem('impersonatedTo');
};

export const getContractorId = () => {
  const contractorId = sessionStorage.getItem('contractorId');
  if (!contractorId || hasAdminEntry()) {
    return false;
  }
  return contractorId;
};

export const getImpersonatedBy = () => {
  const impersonatedBy = sessionStorage.getItem('impersonatedBy');
  if (!impersonatedBy || hasAdminEntry()) {
    return false;
  }
  return impersonatedBy;
};

export const getImpersonatedTo = () => {
  const impersonatedTo = sessionStorage.getItem('impersonatedTo');
  if (!impersonatedTo) {
    return false;
  }
  return impersonatedTo;
};

export const getAccessToken = () => {
  const accessToken = sessionStorage.getItem('access_token');
  if (!accessToken) {
    return false;
  }
  return accessToken;
};

export const getIdToken = () => {
  const idToken = sessionStorage.getItem('id_token');
  if (!idToken) {
    return false;
  }
  return idToken;
};

export const hasAdminEntry = () => {
  const isAdmin = sessionStorage.getItem('isAdmin');
  return Boolean(isAdmin);
};

export const checkUserEntry = (urlQueryString) => {
  const hasContractorQuery = getQueryString('customerID', urlQueryString);
  const contractorId = getContractorId();
  if (contractorId) {
    return true;
  } else if (hasContractorQuery) {
    setContractorInfo(urlQueryString);
    return true;
  }
  return false;
};

export const checkImpersonatorEntry = (urlQueryString) => {
  if (getContractorId() && getImpersonatedBy() && getImpersonatedTo()) {
    return true;
  } else if ((/customerID/.test(urlQueryString)) && (/impersonatedBy/.test(urlQueryString)) && (/impersonatedTo/.test(urlQueryString))) {
    setContractorInfo(urlQueryString);
    return true;
  }
  return false;
};

export const checkAdminEntry = (location) => {
  const isAdmin = sessionStorage.getItem('isAdmin');
  if (/admin/.test(location.pathname) || (isAdmin && /spotpromotiondetails/.test(location.pathname))) {
    sessionStorage.setItem('isAdmin', true);
    return true;
  }
  sessionStorage.removeItem('isAdmin');
  return false;
};

export const checkCZAuthentication = ({ location }) => {
  const urlQueryString = location.search;
  if (checkAdminEntry(location) || checkImpersonatorEntry(urlQueryString) || checkUserEntry(urlQueryString)) {
    return true;
  }
  return false;
};

export const generateYearRange = (endYear, range) => {
  const startYear = endYear - (range - 1);
  const getRange = (start, end) => {
    if (start === end) {
      return [start];
    }
    return [start, ...getRange(start - 1, end)];
  };
  return getRange(endYear, startYear);
};

export const scrollToTop = () => {
  window.scrollTo(0, 0);
};

export const trim = (str) => {
  let value = str;
  if (!value) {
    value = '';
  }
  return value.trim();
};

export const formatValue = (value) => {
  if (!value) {
    return '';
  }
  // regex format value example = 22.22
  const match = (/(\d{0,2})[^.]*((?:\.\d{0,2})?)/g).exec(value.replace(/[^\d.]/g, ''));
  return match[1] + match[2];
};

export const dateDifference = (startDate, endDate) => {
  const startingDate = moment(startDate);
  const endingDate = moment(endDate);
  let dateDiff;
  if (startDate !== undefined && endDate !== undefined) {
    dateDiff = startingDate.diff(endingDate, 'days');
    return dateDiff;
  }
  return dateDiff;
};

export const roundDecimal = (value) => {
  if (!value) return '';
  return Math.round(value * 1000) / 100;
};

export const allowSpecialCharacter = (value) => {
  const val = value.match(/[,.\-()&^%$#@! a-zA-Z0-9/"]/g, '');
  if (val) {
    return val.join('');
  }
  return '';
};

export const restrictSpecialChar = (value) => {
  if (!value) {
    return '';
  }
  const val = value.replace(/`|~|!|@|#|\$|%|\^|\*|\+|=|\[|\{|\]|\}|\||\\|<|>|\?|"|"|;|:/g, '');
  return val;
};

export const formatRateValue = (rate) => {
  if (rate.indexOf('%') > -1) {
    let formatRate = rate.toString().replace('%', '');
    formatRate = truncateZero(Number(formatRate));
    return `${formatRate}%`;
  }
  return rate;
};

export const deviceOrientationChange = () => {
  window.addEventListener('orientationchange', scrollToTop, false);
};

export const deviceOrientationClear = () => {
  window.removeEventListener('orientationchange', scrollToTop, false);
};
