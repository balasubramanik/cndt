import auth0 from 'auth0-js';
import jwtDecode from 'jwt-decode';
import envConfig from 'envConfig'; //eslint-disable-line
import {
  removeContractorInfo,
  getAccessToken,
  getIdToken,
  getContractorId,
  getImpersonatedTo,
  getImpersonatedBy,
} from '../utils/utils';

const {
  authConfig, cczBaseUrl, rewardsBaseUrl,
} = envConfig;

export default class Auth {
  SSOdefaultScope = 'openid email';
  SSOScope = '';
  redirectUri = rewardsBaseUrl;
  adminScope = `${this.SSOdefaultScope} Contractor:GetAdminProfile`;
  nonAdminScope = `${this.SSOdefaultScope} Contractor:GetContractorProfile`;
  auth0 = new auth0.WebAuth({
    domain: authConfig.domain,
    clientID: authConfig.clientId,
    redirectUri: this.redirectUri,
    audience: authConfig.audience,
    responseType: 'token id_token',
    scope: this.SSOdefaultScope,
  });
  tokenRenewalTimeout;

  constructor() {
    this.scheduleRenewal();
  }

  login() {
    window.location.href = `${cczBaseUrl}?redirectURL=${window.location.href}`;
  }

  setAuthSession(authResult) {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    sessionStorage.setItem('access_token', authResult.accessToken);
    sessionStorage.setItem('id_token', authResult.idToken);
    sessionStorage.setItem('expires_at', expiresAt);

    // schedule a token renewal
    this.scheduleRenewal();
  }

  clearAuthSession() {
    // Clear access token and ID token from session storage
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('id_token');
    sessionStorage.removeItem('expires_at');
    removeContractorInfo();
    localStorage.clear();
    clearTimeout(this.tokenRenewalTimeout);
  }

  logout(url) {
    this.auth0.logout({
      returnTo: url,
      client_id: authConfig.clientId,
    });
  }

  isAuthenticated() {
    const expiresAt = JSON.parse(sessionStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  checkSessionCb = (err, result, promise = {}) => {
    if (err) {
      if (err.error === 'login_required') {
        this.auth0.authorize({ redirectUri: this.redirectUri });
      } else {
        this.login();
        if (promise.reject) {
          promise.reject(false);
        }
      }
    } else {
      this.setAuthSession(result);
      if (promise.resolve) {
        promise.resolve(true);
      }
    }
  };

  renewToken(promise = {}) {
    this.auth0.checkSession({ scope: this.SSOScope }, (err, result) => this.checkSessionCb(err, result, promise));
  }

  scheduleRenewal() {
    const expiresAt = JSON.parse(sessionStorage.getItem('expires_at'));
    const delay = expiresAt - Date.now();
    if (delay > 0) {
      this.tokenRenewalTimeout = setTimeout(() => {
        this.renewToken();
      }, delay);
    }
  }

  getProfile() {
    if (getIdToken()) {
      return jwtDecode(getIdToken());
    }
    return false;
  }

  getEmail() {
    const { email = false } = this.getProfile();
    return email;
  }

  getAuthDetails() {
    const accessToken = getAccessToken() || '';
    const tokenId = getIdToken() || '';
    const contractorId = decodeURIComponent(getContractorId() || '');
    let email = this.getEmail() || '';
    email = email.toLowerCase();
    let impersonatedBy = '';
    if (getImpersonatedBy()) {
      email = decodeURIComponent(getImpersonatedTo() || '');
      impersonatedBy = this.getEmail();
    }
    impersonatedBy = impersonatedBy.toLowerCase();
    return {
      accessToken,
      contractorId,
      email,
      impersonatedBy,
      tokenId,
    };
  }

  hasSSOAuthenticated = () => new Promise((resolve, reject) => {
    if (this.isAuthenticated()) {
      resolve(true);
    } else {
      this.renewToken({ resolve, reject });
    }
  });
}
