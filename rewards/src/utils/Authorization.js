import RoleConstants from '../constants/RoleConstants';
import RouteConstants from '../constants/RouteConstants';
export default class Authorization {
  userRolesList = [];
  userRolesAccess = [];
  impersonatorRolesList = [];
  hasRewardAccess = false;
  hasAdminAccess = false;
  hasContractorExist = true;
  hasServiceError = false;
  setUserRoles = (data) => {
    if (typeof data === 'object' && data.userRoles) {
      data.userRoles.forEach((roles) => {
        if (RoleConstants[roles.roleName]) {
          this.userRolesList.push(roles.roleName);
          this.userRolesAccess = this.userRolesAccess.concat(RoleConstants[roles.roleName]);
        }
      });
    }
    if (RoleConstants[data]) {
      this.userRolesList.push(data);
      this.userRolesAccess = this.userRolesAccess.concat(RoleConstants[data]);
    }
    if (this.impersonatorRolesList.includes('TM') && this.userRolesAccess.length) {
      this.userRolesAccess = this.userRolesAccess.filter((url) => !((url === RouteConstants.REDEEM_POINTS) || (url === RouteConstants.CARTSUMMARY)));
    }
  };

  setImpersonatorRoles = (data) => {
    if (typeof data === 'object' && data.impersonatorRoles) {
      data.impersonatorRoles.forEach((roles) => {
        this.impersonatorRolesList.push(roles.roleName);
      });
    }
  };

  setRewardAccess = (bool) => { this.hasRewardAccess = bool; };
  setContractorExist = (bool) => { this.hasContractorExist = bool; };
  setAdminAccess = (bool) => { this.hasAdminAccess = bool; };

  checkAccess = (route) => {
    let isValid = false;
    this.userRolesAccess.forEach((item) => {
      const url = item.replace(/:[\s\S]*/, '(.*)');
      const regex = new RegExp(`^${url}$`);
      if (regex.test(route)) {
        isValid = true;
      }
    });
    return isValid;
  }
}
