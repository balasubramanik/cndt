import envConfig from 'envConfig'; //eslint-disable-line
import { authentication, authorization } from '../routes';

const { baseUrl } = envConfig;
const baseUrlWithProtocol = baseUrl;
const TIMEOUT = 300000;

const headers = (additionalHeaders = {}) => {
  const { accessToken, tokenId } = authentication.getAuthDetails();
  return {
    'Content-Type': 'application/json; charset=UTF-8',
    Accept: 'application/json; charset=UTF-8',
    'Access-Control-Allow-Origin': '*',
    Authorization: `Bearer ${accessToken}`,
    Authorization2: tokenId,
    ...additionalHeaders,
  };
};

function getUrlWithAuthDetails(url) {
  const {
    contractorId,
    email,
    impersonatedBy,
  } = authentication.getAuthDetails();
  const { hasAdminAccess } = authorization;
  let urlWithAuthDetails = url.indexOf('?') > -1 ? `${url}&` : `${url}?`;
  urlWithAuthDetails = `${urlWithAuthDetails}contractorId=${contractorId}&impersonatorId=${impersonatedBy}&userId=${email}&isAdminRequest=${hasAdminAccess}`;
  return urlWithAuthDetails;
}
/**
* @description function to handle the time out error
* @param  {Promise} promise - promise
* @param  {number} timeout - millseconds
* @param  {string} error - errorcode
* @return
*/
function timeoutPromise(promise, timeout, error) {
  return new Promise((resolve, reject) => {
    const clearTimeOut = setTimeout(() => {
      const err = { status: error };
      reject(err);
    }, timeout);
    promise.then((data) => {
      clearTimeout(clearTimeOut);
      resolve(data);
    }, (data) => {
      clearTimeout(clearTimeOut);
      reject(data);
    });
  });
}

export const statusCheck = (data) => {
  const { status } = data;
  return data.json().then((res) => {
    const resData = { status, ...res };
    if (data.status >= 200 && data.status < 300) {
      return Promise.resolve(resData);
    }
    return Promise.reject(resData);
  }).catch((error) => {
    let resData = { status };
    if (error && error.errors) {
      resData = { status, ...error };
    }
    return Promise.reject(resData);
  });
};

export const statusCheckBlob = (data) => {
  if (data.status >= 200 && data.status < 300) {
    return Promise.resolve(data.blob());
  }
  return Promise.reject(data);
};

/** @description calls a native fetch method and returns a promise Object
 * @param {string} url
 * @param {string} urlPrefix
 * @returns {Promise}
 */
export const fetchURL = (url, urlPrefix = baseUrlWithProtocol) => {
  const urlWithAuthDetails = getUrlWithAuthDetails(url);
  return timeoutPromise(fetch(
    urlPrefix.concat(urlWithAuthDetails),
    Object.assign({}, {
      headers: headers(),
    }),
  ), TIMEOUT, 504);
};

/** @description Sending a GET request to JSON API.
 * doGet method resolves or rejects the promise that is obtained
 * from the fetchURl method
 * @param {string} url
 * @param {string} urlPrefix
 * @returns {object}
 */
export const doGet = (url, urlPrefix = baseUrlWithProtocol) => {
  const fetchData = fetchURL(url, urlPrefix).then(statusCheck);
  return fetchData;
};

/** @description Sending a POST request.
 * @param {string} url
 * @param {object} body
 * @param {string} urlPrefix
 * @returns {Promise}
 */
export const doPost = (url, body, urlPrefix = baseUrlWithProtocol) => {
  const {
    email, impersonatedBy, contractorId,
  } = authentication.getAuthDetails();
  const { hasAdminAccess } = authorization;
  const reqBody = {
    contractorId,
    impersonatorId: impersonatedBy,
    userId: email,
    isAdminRequest: hasAdminAccess,
    ...body,
  };
  return timeoutPromise(fetch(
    urlPrefix.concat(url),
    Object.assign({}, {
      method: 'post',
      headers: headers(),
      body: JSON.stringify(reqBody),
    }),
  ), TIMEOUT, 504)
    .then(statusCheck);
};

/** @description Sending a POST request to Blob.
 * @param {string} url
 * @param {object} body
 * @param {string} urlPrefix
 * @returns {Promise}
 */
export const doPostBlob = (url, body, urlPrefix = baseUrlWithProtocol) => {
  const {
    email, impersonatedBy, contractorId,
  } = authentication.getAuthDetails();
  const { hasAdminAccess } = authorization;
  const reqBody = {
    contractorId,
    impersonatorId: impersonatedBy,
    userId: email,
    isAdminRequest: hasAdminAccess,
    ...body,
  };
  return timeoutPromise(fetch(
    urlPrefix.concat(url),
    Object.assign({}, {
      method: 'post',
      headers: headers({
        'Content-Disposition': 'inline',
        responseType: 'arraybuffer',
      }),
      body: JSON.stringify(reqBody),
    }),
  ), TIMEOUT, 504)
    .then(statusCheckBlob);
};

/** @description Sending a PUT request.
 * @param {string} url
 * @param {object} body
 * @param {string} urlPrefix
 * @returns {Promise}
 */
export const doPut = (url, body, urlPrefix = baseUrlWithProtocol) => {
  const urlWithAuthDetails = getUrlWithAuthDetails(url);
  return timeoutPromise(fetch(
    urlPrefix.concat(urlWithAuthDetails),
    Object.assign({}, {
      method: 'put',
      headers: headers(),
      credentials: 'include',
      body: JSON.stringify(body),
    }),
  ), TIMEOUT, 504)
    .then(statusCheck);
};

/** @description Sending a DELETE request.
 * @param {string} url
 * @param {object} body
 * @param {string} urlPrefix
 * @returns {Promise}
 */
export const doDelete = (url, body, urlPrefix = baseUrlWithProtocol) => {
  const urlWithAuthDetails = getUrlWithAuthDetails(url);
  return timeoutPromise(fetch(
    urlPrefix.concat(urlWithAuthDetails),
    Object.assign({}, {
      method: 'delete',
      headers: headers(),
      credentials: 'include',
      body: JSON.stringify(body),
    }),
  ), TIMEOUT, 504)
    .then(statusCheck);
};
