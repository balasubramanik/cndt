import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import header from './reducers/Header';
import myProfile from './reducers/MyProfile';
import emailVerification from './reducers/EmailVerification';
import registration from './reducers/Registration';
import whatQualifies from './reducers/WhatQualifies';
import spotPromotion from './reducers/SpotPromotion';
import spotPromotionDetails from './reducers/SpotPromotionDetails';
import TermsAndConditions from './reducers/TermsAndConditions';
import contractorUser from './reducers/ContractorUser';
import redeemPoints from './reducers/RedeemPoints';
import SpotPromotionManagement from './reducers/SpotPromotionManagement';
import searchBy from './reducers/SearchBy';
import cartSummary from './reducers/cartSummary';
import PointsHistory from './reducers/PointsHistory';
import OrderDetails from './reducers/OrderDetails';
import ClaimDetails from './reducers/ClaimDetails';
import claimsList from './reducers/ClaimsList';
import contactUs from './reducers/ContactUs';
import claimInvoiceDetails from './reducers/ClaimInvoiceDetails';
import contractorList from './reducers/ContractorList';
import states from './reducers/States';

const rootReducer = combineReducers({
  routing: routerReducer,
  headerState: header,
  myProfileState: myProfile,
  whatQualifiesState: whatQualifies,
  spotPromotionState: spotPromotion,
  spotPromotionDetailsState: spotPromotionDetails,
  TermsAndConditionsState: TermsAndConditions,
  contractorUserState: contractorUser,
  redeemPointsState: redeemPoints,
  orderDetailsState: OrderDetails,
  pointsHistoryState: PointsHistory,
  form: formReducer.plugin({
    emailVerificationForm: emailVerification,
    registrationForm: registration,
    spotPromotionForm: SpotPromotionManagement,
  }),
  searchByState: searchBy,
  cartSummaryState: cartSummary,
  claimDetails: ClaimDetails,
  claimsListState: claimsList,
  contactUsState: contactUs,
  claimInvoiceState: claimInvoiceDetails,
  contractorListState: contractorList,
  states,
});

export default rootReducer;
