export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Latitude: any;
  Longitude: any;
  DateTime: any;
  DateTimeCustom: any;
  DateTimeISO: any;
  LocalDate: any;
  JSON: any;
  PhoneNumber: any;
};

export type Account = {
   __typename?: 'Account';
  accountId?: Maybe<Scalars['String']>;
  encryptedAccountId?: Maybe<Scalars['String']>;
  accountDetails?: Maybe<AccountDetails>;
  mailingAddress?: Maybe<Address>;
  persons?: Maybe<Array<Maybe<Person>>>;
  premises?: Maybe<Array<Maybe<PremiseDetails>>>;
  billingInfo?: Maybe<BillPayInfo>;
  viewBillDetails?: Maybe<ViewBillInfo>;
  payment?: Maybe<PaymentInfo>;
  autoPay?: Maybe<AutoPayEnrollStatus>;
  nextBill?: Maybe<NextBillInfo>;
  chargeSummary?: Maybe<ChargeSummary>;
  fieldActivity?: Maybe<AccountFieldActivity>;
  collectionActivity?: Maybe<AccountCollectionActivity>;
  retainService?: Maybe<AccountRetainService>;
  tpa?: Maybe<Tpa>;
  case?: Maybe<Case>;
  billHistory?: Maybe<Array<BillingHistory>>;
  paymentHistory?: Maybe<Array<PaymentHistory>>;
  servicesEligibility?: Maybe<ServicesEligibility>;
  paymentExtension?: Maybe<PaymentExtensionInfo>;
};


export type AccountPaymentArgs = {
  encryptedPersonId: Scalars['String'];
};


export type AccountBillHistoryArgs = {
  startDate: Scalars['DateTimeISO'];
  endDate: Scalars['DateTimeISO'];
};


export type AccountPaymentHistoryArgs = {
  startDate: Scalars['DateTimeISO'];
  endDate: Scalars['DateTimeISO'];
};


export type AccountPaymentExtensionArgs = {
  encryptedPersonId: Scalars['String'];
};

export type AccountAlert = {
   __typename?: 'AccountAlert';
  description?: Maybe<Scalars['String']>;
  enabled?: Maybe<Scalars['Boolean']>;
};

export type AccountCollectionActivity = {
   __typename?: 'AccountCollectionActivity';
  fifteenDayNoticeServed: Scalars['Boolean'];
  noticeDueDate?: Maybe<Scalars['DateTimeISO']>;
};

export type AccountCustomer = {
   __typename?: 'AccountCustomer';
  language: Scalars['String'];
  email: Scalars['String'];
  uid: Scalars['String'];
  personName?: Maybe<Scalars['String']>;
  personId: Scalars['String'];
  timestamp: Scalars['DateTimeCustom'];
  encryptedPersonId: Scalars['String'];
  totalAccounts: Scalars['Int'];
  groups?: Maybe<Array<Maybe<Group>>>;
  contactDetails?: Maybe<Array<Maybe<PersonContact>>>;
  lastConfirmedDate?: Maybe<Scalars['LocalDate']>;
};


export type AccountCustomerGroupsArgs = {
  groupInfoParams?: Maybe<GroupInfoParams>;
};

export type AccountDetail = {
   __typename?: 'AccountDetail';
  accountNumber: Scalars['String'];
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  isDefault: Scalars['Boolean'];
  isLoggedInUserOnAccount: Scalars['Boolean'];
  description?: Maybe<Scalars['String']>;
  relationType: Scalars['String'];
  accountType: AccountType;
  mainCustomerName: Scalars['String'];
  coCustomerNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  /** @deprecated use Premise Details, which has SP/SA details included */
  serviceAddresses?: Maybe<Array<Maybe<ServiceAddress>>>;
  premiseInfo?: Maybe<Array<Maybe<PremiseInfo>>>;
  mailingAddress?: Maybe<ServiceAddress>;
  paymentEligibility?: Maybe<PaymentEligibility>;
  paymentAlerts?: Maybe<PaymentAlerts>;
  currentCharges?: Maybe<CurrentCharges>;
  autoPay?: Maybe<AutoPayInfo>;
  preferredDueDateInfo?: Maybe<PreferredDueDateInfoResponse>;
  preferredDueDate?: Maybe<PreferredDueDateDetails>;
  isPaperlessBillEnrolled?: Maybe<IsPaperlessBillEnrolledResponse>;
  equalpay?: Maybe<PaymentPlanTypeResponse>;
  billInfo?: Maybe<BillInfo>;
  peaktimeRebate?: Maybe<PeakTimeRebateProgramStatus>;
  renewableEnrollment?: Maybe<RenewableEnrollment>;
  pendingDisconnect?: Maybe<PendingDisconnectStatus>;
  isActive: Scalars['Boolean'];
  alertDetails?: Maybe<AlertDetails>;
  isEnrolled?: Maybe<Scalars['Boolean']>;
  peakTimeRebateGiveBack?: Maybe<PeakTimeRebateGiveBackDetails>;
  timeOfDayInfo?: Maybe<TimeOfDayInfo>;
  nextBill?: Maybe<NextBillInfo>;
  tpa?: Maybe<Tpa_AccountDetail>;
  commPreferences?: Maybe<Array<Maybe<CommPreference>>>;
};


export type AccountDetailTpaArgs = {
  channel?: Maybe<Channel>;
};


export type AccountDetailCommPreferencesArgs = {
  commPreferencesParams: CommPreferencesParams;
};

export type AccountDetailList = {
   __typename?: 'AccountDetailList';
  totalCount: Scalars['Int'];
  timestamp: Scalars['DateTimeCustom'];
  accounts?: Maybe<Array<Maybe<AccountDetail>>>;
};

export type AccountDetailListParams = {
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
  groupId?: Maybe<Scalars['ID']>;
};

export type AccountDetailParams = {
  accountNumberList: Array<AccountParams>;
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
};

export type AccountDetails = {
   __typename?: 'AccountDetails';
  accountType?: Maybe<AccountType>;
  accountStatus: SaStatus;
  statusDate?: Maybe<Scalars['DateTime']>;
};

export type AccountDetailsInput = {
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  groupId: Scalars['String'];
};

export type AccountDetailsRequest = {
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type AccountDetailsResponse = {
  accountNumber?: Maybe<Scalars['String']>;
  relationType?: Maybe<Scalars['String']>;
  accountType?: Maybe<Scalars['String']>;
  mainCustomerName?: Maybe<Scalars['String']>;
  coCustomerNames?: Maybe<Array<Maybe<Scalars['String']>>>;
  serviceAddresses?: Maybe<Array<Maybe<ServiceAddressResponse>>>;
  mailingAddress?: Maybe<ServiceAddressResponse>;
};

export type AccountFieldActivity = {
   __typename?: 'AccountFieldActivity';
  scheduledDisconnectDate?: Maybe<Scalars['DateTimeISO']>;
  pendingReconnect?: Maybe<Scalars['Boolean']>;
  disconnected?: Maybe<Scalars['Boolean']>;
};

export type AccountGroup = {
   __typename?: 'AccountGroup';
  groupId: Scalars['String'];
  groupCode?: Maybe<Scalars['String']>;
  groupName?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  accountNumbers?: Maybe<Array<Maybe<Scalars['String']>>>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  groupType?: Maybe<GroupType>;
};

export type AccountGroupParams = {
  encryptedaccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type AccountGroupResponse = ResponseBase & {
   __typename?: 'AccountGroupResponse';
  group?: Maybe<Group>;
  success: Scalars['Boolean'];
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
  groupStatus: GroupStatus;
};

export enum AccountGroupVerificationType {
  DateOfBirth = 'DateOfBirth',
  EmployeeIdentificationNumber = 'EmployeeIdentificationNumber',
  PrimaryNotificationPhone = 'PrimaryNotificationPhone'
}

export type AccountInfoParams = {
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
  groupId?: Maybe<Scalars['String']>;
};

export type AccountLookupDetails = {
   __typename?: 'AccountLookupDetails';
  accountID?: Maybe<Scalars['String']>;
  customerClass?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
  premiseCount?: Maybe<Scalars['String']>;
  listOfPersonIDs?: Maybe<Array<Maybe<Person>>>;
  zipCode?: Maybe<Scalars['String']>;
  customerName?: Maybe<Scalars['String']>;
  personCount?: Maybe<Scalars['String']>;
  streetName?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  premiseDetails?: Maybe<Array<Maybe<PremiseDetails>>>;
};

export enum AccountLookUpType {
  Account = 'ACCOUNT',
  Phone = 'PHONE'
}

export type AccountNumberParams = {
  accountNumber: Scalars['String'];
};

export type AccountParams = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type AccountPerson = {
   __typename?: 'AccountPerson';
  encryptedPersonId?: Maybe<Scalars['String']>;
  fullName?: Maybe<Scalars['String']>;
  personRelationshipType?: Maybe<RelationshipType>;
};

export type AccountPersonInput = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  fullName?: Maybe<Scalars['String']>;
  personRelationshipType?: Maybe<RelationshipType>;
  changeReason?: Maybe<Scalars['String']>;
  employerName?: Maybe<Scalars['String']>;
};

export type AccountRemoveAccountsResponse = ResponseBase & {
   __typename?: 'AccountRemoveAccountsResponse';
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
  groupStatus: GroupStatus;
};

export type AccountResponse = {
  personName?: Maybe<Scalars['String']>;
  personId?: Maybe<Scalars['String']>;
  userName?: Maybe<Scalars['String']>;
  accounts?: Maybe<Array<Maybe<AccountDetailsResponse>>>;
};

export type AccountRetainService = {
   __typename?: 'AccountRetainService';
  minimumToRetainService?: Maybe<Scalars['Float']>;
};

export enum AccountSort {
  Default = 'DEFAULT',
  Accountnumber = 'ACCOUNTNUMBER',
  Serviceaddress = 'SERVICEADDRESS',
  Nickname = 'NICKNAME',
  Duedate = 'DUEDATE'
}

export type AccountSummary = {
   __typename?: 'AccountSummary';
  accountID: Scalars['ID'];
  premises?: Maybe<Array<Maybe<PremiseSummary>>>;
  payment?: Maybe<PaymentInfo>;
  nextBill?: Maybe<NextBillInfo>;
  chargeSummary?: Maybe<ChargeSummary>;
};


export type AccountSummaryPaymentArgs = {
  encryptedPersonId: Scalars['String'];
};

export enum AccountType {
  Res = 'RES',
  Com = 'COM',
  State = 'STATE',
  Statecom = 'STATECOM',
  Stateres = 'STATERES',
  Non = 'NON',
  Sum = 'SUM',
  Eba = 'EBA',
  Ret = 'RET',
  Summary = 'SUMMARY',
  Self = 'SELF'
}

export type AddAccountGroupInput = {
  groupName?: Maybe<Scalars['String']>;
  groupId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['String']>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  groupType?: Maybe<GroupType>;
  isPrimary?: Maybe<Scalars['Boolean']>;
};

export type AddGroupRequest = {
  userName?: Maybe<Scalars['String']>;
  groupCode?: Maybe<Scalars['String']>;
  groupName?: Maybe<Scalars['String']>;
  groupType?: Maybe<GroupTypeRequest>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type AddGroupResponse = {
  group_id?: Maybe<Scalars['String']>;
};

export type AdditionalInfo = {
   __typename?: 'AdditionalInfo';
  dateOfBirth?: Maybe<Scalars['String']>;
  primaryPhone?: Maybe<Scalars['String']>;
  emailAddress?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  socialSecurityNumber?: Maybe<Scalars['String']>;
  alternatePhoneInfo?: Maybe<AlternatePhone>;
  mailingAddress?: Maybe<Address>;
  previousAddress?: Maybe<Address>;
  federalInformation?: Maybe<FederalInfo>;
  stateInformation?: Maybe<StateInfo>;
  employmentInformation?: Maybe<EmploymentInfo>;
  hasSocialSecurityInfo?: Maybe<Scalars['Boolean']>;
  hasStateInfo?: Maybe<Scalars['Boolean']>;
  hasFederalInfo?: Maybe<Scalars['Boolean']>;
  mailingAndServiceAddressesSame?: Maybe<Scalars['Boolean']>;
  registerForOnlineAccess?: Maybe<Scalars['Boolean']>;
  hasExistingPrimaryIdentification?: Maybe<Scalars['Boolean']>;
};

export type AdditionalInfoInput = {
  dateOfBirth?: Maybe<Scalars['String']>;
  primaryPhone?: Maybe<Scalars['String']>;
  emailAddress?: Maybe<Scalars['String']>;
  password?: Maybe<Scalars['String']>;
  socialSecurityNumber?: Maybe<Scalars['String']>;
  alternatePhoneInfo?: Maybe<AlternatePhoneInput>;
  mailingAddress?: Maybe<AddressInput>;
  previousAddress?: Maybe<AddressInput>;
  federalInformation?: Maybe<FederalInfoInput>;
  stateInformation?: Maybe<StateInfoInput>;
  employmentInformation?: Maybe<EmploymentInfoInput>;
  hasSocialSecurityInfo?: Maybe<Scalars['Boolean']>;
  hasStateInfo?: Maybe<Scalars['Boolean']>;
  hasFederalInfo?: Maybe<Scalars['Boolean']>;
  mailingAndServiceAddressesSame?: Maybe<Scalars['Boolean']>;
  registerForOnlineAccess?: Maybe<Scalars['Boolean']>;
  hasExistingPrimaryIdentification?: Maybe<Scalars['Boolean']>;
};

export type AddRemoveAccountInput = {
  accountNumber: Scalars['String'];
  encryptedBusinessPersonId?: Maybe<Scalars['String']>;
  action: AddRemoveOperation;
};

export type AddRemoveAccountRequest = {
  businessPersonId?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  isActive?: Maybe<Scalars['Boolean']>;
  action?: Maybe<AddRemoveOperation>;
};

export type AddRemoveAccountsInput = {
  groupId: Scalars['String'];
  accounts: Array<Maybe<AddRemoveAccountInput>>;
};

export type AddRemoveAccountsRequest = {
  accounts?: Maybe<Array<Maybe<AddRemoveAccountRequest>>>;
};

export type AddRemoveAccountsResponse = {
  result?: Maybe<Scalars['Boolean']>;
};

export enum AddRemoveOperation {
  Add = 'ADD',
  Remove = 'REMOVE'
}

export type Address = {
   __typename?: 'Address';
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export type AddressInput = {
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
};

export type Alert = {
   __typename?: 'Alert';
  description?: Maybe<Scalars['String']>;
  type?: Maybe<NotificationType>;
  sequence?: Maybe<Scalars['Float']>;
  originalValue?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  isEmail?: Maybe<Scalars['Boolean']>;
  encryptedEmailPrefId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText?: Maybe<Scalars['Boolean']>;
  encryptedTextPrefId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type AlertDetails = {
   __typename?: 'AlertDetails';
  phoneNumber?: Maybe<Scalars['String']>;
  phoneSequence?: Maybe<Scalars['Float']>;
  notEnrolled?: Maybe<Scalars['Boolean']>;
  alerts?: Maybe<Array<Maybe<Alert>>>;
};

export type Alertinput = {
  description?: Maybe<Scalars['String']>;
  type?: Maybe<NotificationType>;
  sequence?: Maybe<Scalars['Float']>;
  originalValue?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  isEmail?: Maybe<Scalars['Boolean']>;
  encryptedEmailPrefId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText?: Maybe<Scalars['Boolean']>;
  encryptedTextPrefId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export enum AlertType {
  Email = 'EMAIL',
  Sms = 'SMS'
}

export type AlternatePhone = {
   __typename?: 'AlternatePhone';
  alternatePhoneNumber?: Maybe<Scalars['String']>;
  alternatePhoneType?: Maybe<PhoneType>;
  alternatePhoneText?: Maybe<Scalars['String']>;
};

export type AlternatePhoneInfoInput = {
  alternatePhoneExt?: Maybe<Scalars['String']>;
  alternatePhoneNumber?: Maybe<Scalars['String']>;
  alternatePhoneType?: Maybe<PhoneType>;
};

export type AlternatePhoneInput = {
  alternatePhoneNumber?: Maybe<Scalars['String']>;
  alternatePhoneType?: Maybe<PhoneType>;
  alternatePhoneText?: Maybe<Scalars['String']>;
};

export type AniEligibility = {
   __typename?: 'aniEligibility';
  isEligible?: Maybe<Scalars['String']>;
  reason?: Maybe<Scalars['String']>;
};

export type AutoPay = {
   __typename?: 'AutoPay';
  isEnrolled?: Maybe<Scalars['Boolean']>;
};

export type AutoPayCancelInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type AutoPayCancelIvaInput = {
  referenceId: Scalars['String'];
  accountNumber: Scalars['String'];
  personId: Scalars['String'];
  serviceAddressLine1?: Maybe<Scalars['String']>;
  userName?: Maybe<Scalars['String']>;
};

export type AutoPayEnrollDetails = {
   __typename?: 'AutoPayEnrollDetails';
  tokenId: Scalars['String'];
  type: Scalars['String'];
  referenceNumber: Scalars['String'];
};

export type AutoPayEnrollInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  tokenId: Scalars['String'];
};

export type AutoPayEnrollStatus = {
   __typename?: 'AutoPayEnrollStatus';
  isEnrolled: Scalars['Boolean'];
  isSameDayEnrolled: Scalars['Boolean'];
};

export enum AutoPayErrorReason {
  Unknown = 'Unknown',
  NotEnrolled = 'NotEnrolled',
  Canceled = 'CANCELED'
}

export type AutoPayInfo = {
   __typename?: 'AutoPayInfo';
  isEnrolled: Scalars['Boolean'];
  isSameDayEnrolled: Scalars['Boolean'];
  startDate?: Maybe<Scalars['DateTimeCustom']>;
  enrollDetails?: Maybe<AutoPayEnrollDetails>;
};

export type AutoPaymentDownloadPdfRequest = {
  name?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<Scalars['String']>;
  bankAccountRoutingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  confirmationNumber?: Maybe<Scalars['String']>;
};

export type AutoPaymentPdfResponse = {
   __typename?: 'AutoPaymentPdfResponse';
  success?: Maybe<Scalars['Boolean']>;
  pdf?: Maybe<Scalars['String']>;
};

export type BillableAccountInfo = {
   __typename?: 'BillableAccountInfo';
  isAccountBillable?: Maybe<Scalars['Boolean']>;
  isServiceDesignationElectric?: Maybe<Scalars['Boolean']>;
  isAccountOnFlexPricePlan?: Maybe<Scalars['Boolean']>;
  isNonSpo?: Maybe<Scalars['Boolean']>;
};

export type BillInfo = {
   __typename?: 'BillInfo';
  amountDue?: Maybe<Scalars['Float']>;
  balanceDueDate?: Maybe<Scalars['String']>;
  dueDate?: Maybe<Scalars['String']>;
  lastPaymentAmount?: Maybe<Scalars['Float']>;
  lastPaymentDate?: Maybe<Scalars['String']>;
  isAccountPayable?: Maybe<Scalars['Boolean']>;
  isNewAccount?: Maybe<Scalars['Boolean']>;
  futurePaymentDate?: Maybe<Scalars['String']>;
  oneTimeFuturePaymentScheduled?: Maybe<Scalars['Boolean']>;
  multipleFuturePaymentsScheduled?: Maybe<Scalars['Boolean']>;
  enrolledInTPA?: Maybe<Scalars['Boolean']>;
};

export type BillInfoParams = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export enum BillingAndPaymentDelimiterType {
  Unknown = 'Unknown',
  Csv = 'Csv',
  Pipe = 'Pipe'
}

export type BillingAndPaymentHistoryDetailsDownloadResponse = {
   __typename?: 'BillingAndPaymentHistoryDetailsDownloadResponse';
  byteString?: Maybe<Scalars['String']>;
};

export type BillingAndPaymentHistoryDetailsInput = {
  encryptedAccountNumbers: Array<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTimeCustom']>;
  endDate?: Maybe<Scalars['DateTimeCustom']>;
  sortedByDueDate?: Maybe<Scalars['Boolean']>;
  reportType?: Maybe<BillingAndPaymentReportType>;
  delimeterType?: Maybe<BillingAndPaymentDelimiterType>;
};

export type BillingAndPaymentHistoryDetailsResponse = {
   __typename?: 'BillingAndPaymentHistoryDetailsResponse';
  accountNumbers?: Maybe<Array<Maybe<Scalars['String']>>>;
  billingSummaries?: Maybe<Array<Maybe<BillingAndPaymentHistorySummary>>>;
  totalAmount?: Maybe<Scalars['Float']>;
  totalAccounts?: Maybe<Scalars['Float']>;
};

export type BillingAndPaymentHistorySummary = {
   __typename?: 'BillingAndPaymentHistorySummary';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  encryptedBillingId?: Maybe<Scalars['String']>;
  totalKwh?: Maybe<Scalars['Float']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTimeCustom']>;
  billDate?: Maybe<Scalars['DateTimeCustom']>;
  details?: Maybe<Array<Maybe<BillingAndPaymentSummaryDetail>>>;
};

export enum BillingAndPaymentReportType {
  Unknown = 'Unknown',
  LongForm = 'LongForm',
  ShortForm = 'ShortForm',
  Accounting = 'Accounting'
}

export type BillingAndPaymentSummaryDetail = {
   __typename?: 'BillingAndPaymentSummaryDetail';
  amount?: Maybe<Scalars['Float']>;
  kwh?: Maybe<Scalars['Float']>;
  serviceAddress?: Maybe<Scalars['String']>;
};

export type BillingHistory = {
   __typename?: 'BillingHistory';
  billDate: Scalars['DateTimeISO'];
  billingPeriodStartDate: Scalars['DateTimeISO'];
  billingPeriodEndDate: Scalars['DateTimeISO'];
  billAmount: Scalars['Float'];
  usageInKwh: Scalars['Float'];
};

export type BillingPeriod = {
   __typename?: 'BillingPeriod';
  averageTemperature?: Maybe<Scalars['Int']>;
  date?: Maybe<Scalars['String']>;
  totalCost?: Maybe<Scalars['Int']>;
  totalKwh?: Maybe<Scalars['Int']>;
};

export type BillNotFoundDownloadPdf = {
   __typename?: 'BillNotFoundDownloadPdf';
  pdf?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type BillPayInfo = {
   __typename?: 'BillPayInfo';
  amountDue?: Maybe<Scalars['Float']>;
  balanceDueDate?: Maybe<Scalars['String']>;
  dueDate?: Maybe<Scalars['DateTimeISO']>;
  lastPaymentAmount?: Maybe<Scalars['Float']>;
  lastPaymentDate?: Maybe<Scalars['DateTimeISO']>;
  isAccountPayable?: Maybe<Scalars['Boolean']>;
  isNewAccount?: Maybe<Scalars['Boolean']>;
  futurePaymentDate?: Maybe<Scalars['DateTimeISO']>;
  oneTimeFuturePaymentScheduled?: Maybe<Scalars['Boolean']>;
  multipleFuturePaymentsScheduled?: Maybe<Scalars['Boolean']>;
  enrolledInTPA?: Maybe<Scalars['Boolean']>;
};

export type BusinessPersonRequest = {
  businessPersonId?: Maybe<Scalars['String']>;
  accountNumberList?: Maybe<Array<Maybe<Scalars['String']>>>;
};

export type CancelAutoPay = {
   __typename?: 'CancelAutoPay';
  isSuccess: Scalars['Boolean'];
  errorReason?: Maybe<AutoPayErrorReason>;
  autoPay?: Maybe<AutoPayInfo>;
};

export enum CardType {
  Amex = 'AMEX',
  Disc = 'DISC',
  Mc = 'MC',
  Visa = 'VISA'
}

export type Case = {
   __typename?: 'CASE';
  FiveDayVerbalDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type ChangeEmailRequest = {
  newEmail: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type ChangeEmailResponse = {
   __typename?: 'ChangeEmailResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type ChangePasswordInfo = {
   __typename?: 'ChangePasswordInfo';
  hasPerson?: Maybe<Scalars['Boolean']>;
  sameAsOldPassword?: Maybe<Scalars['Boolean']>;
};

export type ChangePasswordInfoRequest = {
  password?: Maybe<Scalars['String']>;
};

export type ChangePasswordInfoResponse = {
   __typename?: 'ChangePasswordInfoResponse';
  hasPerson?: Maybe<HasPersonIdResponse>;
  sameAsOldPassword?: Maybe<SameAsOldPasswordResponse>;
};

export type ChangePasswordUpdateRequest = {
  password: Scalars['String'];
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type ChangePasswordUpdateResponse = {
   __typename?: 'ChangePasswordUpdateResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type ChangePinUpdateRequest = {
  newPinCode: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type ChangePinUpdateResponse = {
   __typename?: 'ChangePinUpdateResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export enum Channel {
  Csweb = 'CSWEB',
  Iva = 'IVA',
  Ivr = 'IVR',
  Mobapp = 'MOBAPP'
}

export type Characteristic = {
  type: Scalars['String'];
  value: Scalars['String'];
  action: Scalars['String'];
};

export type ChargeSummary = {
   __typename?: 'ChargeSummary';
  balanceForward?: Maybe<Scalars['Float']>;
  paymentAdjustments?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTimeISO']>;
  amountDue?: Maybe<Scalars['Float']>;
};

export type CifDetails = {
   __typename?: 'CIFDetails';
  accountNumber: Scalars['String'];
  amountDue: Scalars['Float'];
  dueDate: Scalars['String'];
  Segment: Scalars['String'];
  zipcode: Scalars['String'];
  paymentType?: Maybe<Scalars['String']>;
  invoiceNumber?: Maybe<Scalars['String']>;
  amountPastDue?: Maybe<Scalars['Float']>;
  authenticationToken1?: Maybe<Scalars['String']>;
  authenticationToken2?: Maybe<Scalars['String']>;
  authenticationToken3?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  dayPhone?: Maybe<Scalars['String']>;
  dayExt?: Maybe<Scalars['String']>;
  eveningPhone?: Maybe<Scalars['String']>;
  eveningExt?: Maybe<Scalars['String']>;
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  paperSuppressed?: Maybe<Scalars['Boolean']>;
  daysPastDue?: Maybe<Scalars['Int']>;
  minAmount?: Maybe<Scalars['Float']>;
  maxAmount?: Maybe<Scalars['Float']>;
  maxPostingDate?: Maybe<Scalars['String']>;
  maxPostingDate2?: Maybe<Scalars['String']>;
  webChannelBlock?: Maybe<Scalars['Boolean']>;
  adChannelBlock?: Maybe<Scalars['Boolean']>;
  mobileChannelBlock?: Maybe<Scalars['Boolean']>;
  ivrChannelBlock?: Maybe<Scalars['Boolean']>;
  delinquent?: Maybe<Scalars['Boolean']>;
  securityToken?: Maybe<Scalars['String']>;
};

export type CifDetailsParams = {
  accountNumber: Scalars['String'];
};

export type CoCustomerForAccountRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type CoCustomerInfo = {
   __typename?: 'CoCustomerInfo';
  encryptedPersonId?: Maybe<Scalars['String']>;
  personalIdentificationType?: Maybe<PersonalIdentificationType>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
};

export enum CocustomerRelationshipType {
  None = 'None',
  Spouse = 'Spouse',
  DomesticPartner = 'DomesticPartner',
  Relative = 'Relative',
  Roommate = 'Roommate'
}

export type CollectionActivity = {
   __typename?: 'CollectionActivity';
  FifteenDayNoticeServed: Scalars['Boolean'];
  NoticeDueDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type CommercialAccountValidation = {
   __typename?: 'CommercialAccountValidation';
  isValidAccountNumber: Scalars['Boolean'];
  isValidBusinessAccount: Scalars['Boolean'];
  shouldUseBookkeeperRegistration: Scalars['Boolean'];
};

export type CommercialContactInfoInput = {
  bookKeeperName?: Maybe<Scalars['String']>;
  bookKeeperPhone?: Maybe<Scalars['String']>;
  bookKeeperPhoneExt?: Maybe<Scalars['String']>;
  contactName?: Maybe<Scalars['String']>;
  primaryPhone?: Maybe<Scalars['String']>;
  primaryPhoneExt?: Maybe<Scalars['String']>;
};

export type CommercialRegistrationRequest = {
  AccountNumber?: Maybe<Scalars['String']>;
  EmailAddress?: Maybe<Scalars['String']>;
  Password?: Maybe<Scalars['String']>;
  FirstName?: Maybe<Scalars['String']>;
  MiddleName?: Maybe<Scalars['String']>;
  LastName?: Maybe<Scalars['String']>;
  VerificationType?: Maybe<VerificationType>;
  VerificationValue?: Maybe<Scalars['String']>;
  IsCoApplicantOfCommercialAccount?: Maybe<Scalars['Boolean']>;
};

export type CommPreference = {
   __typename?: 'CommPreference';
  notificationType: NotificationType;
  deliveryTypeDetails?: Maybe<Array<Maybe<DeliveryTypeDetail>>>;
};

export type CommPreferencesParams = {
  notificationTypes: Array<NotificationType>;
};

export type ContactDetail = {
   __typename?: 'ContactDetail';
  contactId?: Maybe<Scalars['ID']>;
  encryptedContactId: Scalars['String'];
  value?: Maybe<Scalars['String']>;
  encryptedPreferenceId?: Maybe<Scalars['String']>;
  isSelected: Scalars['Boolean'];
};

export enum ContactInfoConfirmationOption {
  InfoCorrect = 'INFO_CORRECT',
  UpdateInfo = 'UPDATE_INFO'
}

export type ContactInfoConfirmationRequest = {
  contactInfoConfirmationOption: ContactInfoConfirmationOption;
};

export type ContactInfoConfirmationResponse = {
   __typename?: 'ContactInfoConfirmationResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type ContactInput = {
  encryptedContactId: Scalars['String'];
  value: Scalars['String'];
  encryptedPreferenceId?: Maybe<Scalars['String']>;
  isSelected: Scalars['Boolean'];
};

export type ContactlogPersonLeavingBehindRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type Coordinates = {
   __typename?: 'Coordinates';
  lng: Scalars['Float'];
  lat: Scalars['Float'];
  other?: Maybe<Scalars['Float']>;
};

export type CreateAccountGroupInput = {
  accountNumber: Scalars['String'];
  businessName: Scalars['String'];
  verificationValue: Scalars['String'];
  verificationType?: Maybe<AccountGroupVerificationType>;
};

export type CreateGroupAccountsInput = {
  accountNumber: Scalars['String'];
  encryptedBusinessPersonId?: Maybe<Scalars['String']>;
  action: AddRemoveOperation;
};

export type CreateGroupInput = {
  groupCode: Scalars['String'];
  groupName: Scalars['String'];
  accounts: Array<CreateGroupAccountsInput>;
};

export type CurrentBillDetailsResponse = {
  accountNumber?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
  isAccountPayable?: Maybe<Scalars['Boolean']>;
  isNewAccount?: Maybe<Scalars['Boolean']>;
  lastPaymentDate?: Maybe<Scalars['String']>;
  futurePaymentDate?: Maybe<Scalars['String']>;
  lastPaymentAmount?: Maybe<Scalars['Float']>;
  oneTimeFuturePaymentScheduled?: Maybe<Scalars['Boolean']>;
  multipleFuturePaymentsScheduled?: Maybe<Scalars['Boolean']>;
  enrolledInTPA?: Maybe<Scalars['Boolean']>;
};

export type CurrentCharges = {
   __typename?: 'CurrentCharges';
  amountDue: Scalars['Float'];
  dueDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type CustomerContactInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  contactClass: Scalars['String'];
  contactType: Scalars['String'];
  longDescription: Scalars['String'];
  characteristics?: Maybe<Array<Maybe<Characteristic>>>;
  shouldPrintLetter: Scalars['Boolean'];
};

export type CustomerContactResponse = {
   __typename?: 'CustomerContactResponse';
  success: Scalars['Boolean'];
};

export type CustomerInfoInput = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  personalIdentificationType?: Maybe<PersonalIdentificationType>;
  additionalInformation?: Maybe<AdditionalInfoInput>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  middleName?: Maybe<Scalars['String']>;
  spouseOrRegisteredDomesticPartnerType?: Maybe<CocustomerRelationshipType>;
};

export type CustomerPreferencesAccountResponse = {
  businessPersonId?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type CustomerPreferencesResponse = {
  language?: Maybe<Scalars['String']>;
  hasGroups?: Maybe<Scalars['Boolean']>;
  accounts?: Maybe<Array<Maybe<CustomerPreferencesAccountResponse>>>;
  lastConfirmedDate?: Maybe<Scalars['LocalDate']>;
  prefLanguage?: Maybe<Scalars['String']>;
};

export type CustomerServiceDisconnectedParams = {
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
};

export type CustomerServiceDisconnectedResponse = {
   __typename?: 'CustomerServiceDisconnectedResponse';
  isServiceDisconnected: Scalars['Boolean'];
};

export type CustomGroupInput = {
  groupName?: Maybe<Scalars['String']>;
  groupId?: Maybe<Scalars['String']>;
  id?: Maybe<Scalars['String']>;
  createdDate?: Maybe<Scalars['String']>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  groupType?: Maybe<GroupType>;
  isPrimary?: Maybe<Scalars['Boolean']>;
  groupCode?: Maybe<Scalars['String']>;
  nickname?: Maybe<Scalars['String']>;
  active?: Maybe<Scalars['Boolean']>;
  checked?: Maybe<Scalars['Boolean']>;
};




export type DefaultAccountInfo = {
   __typename?: 'DefaultAccountInfo';
  accountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type DeleteGroupInput = {
  groups: Array<DeleteGroupListInput>;
};

export type DeleteGroupListInput = {
  groupId: Scalars['String'];
};

export type DeleteGroupResponse = ResponseBase & {
   __typename?: 'DeleteGroupResponse';
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
  groupStatus: GroupStatus;
};

export type DeleteProfileError = {
   __typename?: 'DeleteProfileError';
  error?: Maybe<Array<Maybe<KeyValue>>>;
};

export type DeleteProfileRequest = {
  ownerId: Scalars['String'];
  token: Scalars['String'];
};

export type DeleteProfileResponse = {
   __typename?: 'DeleteProfileResponse';
  profileResponse?: Maybe<ProfileResponse>;
};

export enum DeliveryType {
  Email = 'EMAIL',
  Sms = 'SMS',
  Push = 'PUSH'
}

export type DeliveryTypeDetail = {
   __typename?: 'DeliveryTypeDetail';
  deliveryType: DeliveryType;
  contactDetails?: Maybe<Array<Maybe<ContactDetail>>>;
};

export type DeliveryTypeInput = {
  deliveryType: DeliveryType;
  contacts: Array<ContactInput>;
};

export type Description = {
   __typename?: 'Description';
  description?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
};

export enum Direction {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type EmploymentInfo = {
   __typename?: 'EmploymentInfo';
  employmentOption?: Maybe<EmploymentType>;
  employerName?: Maybe<Scalars['String']>;
};

export type EmploymentInfoInput = {
  employmentOption?: Maybe<EmploymentType>;
  employerName?: Maybe<Scalars['String']>;
};

export enum EmploymentType {
  None = 'None',
  Employed = 'Employed',
  SelfEmployed = 'SelfEmployed',
  Retired = 'Retired',
  Unemployed = 'Unemployed'
}

export type EnergyTrackerData = {
   __typename?: 'EnergyTrackerData';
  detailsAvailable: Scalars['Boolean'];
  showEnergyTracker?: Maybe<Scalars['Boolean']>;
  hasMoreThan15DaysOfData?: Maybe<Scalars['Boolean']>;
  encryptedIdentifiers?: Maybe<Array<Maybe<Scalars['String']>>>;
  details?: Maybe<EnergyTrackerDetails>;
  serviceProvider?: Maybe<EnergyTrackerServiceProvider>;
};

export type EnergyTrackerDataParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
};

export type EnergyTrackerDetails = {
   __typename?: 'EnergyTrackerDetails';
  billingCycleDay?: Maybe<Scalars['Int']>;
  numberOfBillingDays?: Maybe<Scalars['Int']>;
  lastReadDate?: Maybe<Scalars['String']>;
  billToDateAmount?: Maybe<Scalars['Float']>;
  projectedAmount?: Maybe<Scalars['Float']>;
  minProjectedAmount?: Maybe<Scalars['Float']>;
  maxProjectedAmount?: Maybe<Scalars['Float']>;
  billingProgress?: Maybe<Scalars['Int']>;
};

export type EnergyTrackerLinkInput = {
   __typename?: 'EnergyTrackerLinkInput';
  LinkType?: Maybe<EnergyTrackerLinkInputLinkType>;
  ServiceProvider?: Maybe<EnergyTrackerLinkInputServiceProvider>;
};

export enum EnergyTrackerLinkInputLinkType {
  None = 'None',
  MyUsage = 'MyUsage',
  CompareBills = 'CompareBills',
  WaysToSave = 'WaysToSave'
}

export enum EnergyTrackerLinkInputServiceProvider {
  None = 'None',
  OPower = 'OPower',
  FirstFuel = 'FirstFuel'
}

export enum EnergyTrackerServiceProvider {
  None = 'None',
  OPower = 'OPower',
  FirstFuel = 'FirstFuel'
}

export type EnergyTrackerUserInfo = {
   __typename?: 'EnergyTrackerUserInfo';
  mainAccountPersonId?: Maybe<Scalars['String']>;
  premiseIds?: Maybe<Array<Maybe<Scalars['String']>>>;
  provider?: Maybe<EnergyTrackerServiceProvider>;
};

export type EnrollAutoPay = {
   __typename?: 'EnrollAutoPay';
  isSuccess: Scalars['Boolean'];
  confirmationId?: Maybe<Scalars['String']>;
  errorReason?: Maybe<AutoPayErrorReason>;
  autoPay?: Maybe<AutoPayInfo>;
};

export type EnrolledInstallmentDetails = {
   __typename?: 'EnrolledInstallmentDetails';
  currentAccountBalance: Scalars['Float'];
  totalMonths: Scalars['Int'];
  payOffAmount: Scalars['Float'];
  recurringChargeAmount: Scalars['Float'];
  currentTpaMonthBalance?: Maybe<Scalars['Float']>;
  monthsMatrix: Array<Tpa_MonthsMatrix>;
};

export type Enrollments = {
   __typename?: 'Enrollments';
  descriptions?: Maybe<Array<Maybe<Description>>>;
  enrolled?: Maybe<Scalars['Boolean']>;
};

export type EnrollToPaymentExtensionInput = {
  accountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  currentPayPlanId?: Maybe<Scalars['String']>;
  currentEnrolledInOption3?: Maybe<Scalars['Boolean']>;
  scheduledAmount?: Maybe<Scalars['Float']>;
  scheduleDate?: Maybe<Scalars['DateTimeISO']>;
  paymentMode?: Maybe<Scalars['String']>;
  source?: Maybe<Scalars['String']>;
  payPlanType?: Maybe<PaymentExtensionOptionType>;
};

export enum ErrorReason {
  GroupNameAlreadyExists = 'GroupNameAlreadyExists',
  GroupCodeAlreadyExists = 'GroupCodeAlreadyExists',
  AccountGroupAlreadyExists = 'AccountGroupAlreadyExists',
  DefaultGroup = 'DefaultGroup',
  DefaultAccountNumber = 'DefaultAccountNumber',
  AccountGroupVerificationFailed = 'AccountGroupVerificationFailed',
  AccountGroupPersonNotFound = 'AccountGroupPersonNotFound',
  AccountGroupAccountNotFound = 'AccountGroupAccountNotFound',
  LoggedOnUserAutoGroup = 'LoggedOnUserAutoGroup',
  AccountAlreadyExistsInGroup = 'AccountAlreadyExistsInGroup',
  CannotRemoveDefaultAccount = 'CannotRemoveDefaultAccount'
}

export type ExpiryDate = {
   __typename?: 'ExpiryDate';
  month?: Maybe<Scalars['String']>;
  year?: Maybe<Scalars['String']>;
};

export enum FederalIdentificationType {
  UsPassport = 'USPassport',
  UsStudentVisa = 'USStudentVisa',
  UsMilitaryId = 'USMilitaryID',
  UsTemporaryVisa = 'USTemporaryVisa',
  UsImmigration = 'USImmigration',
  OregonTribalId = 'OregonTribalID'
}

export type FederalInfo = {
   __typename?: 'FederalInfo';
  federalIDType?: Maybe<FederalIdentificationType>;
  federalIDNumber?: Maybe<Scalars['String']>;
};

export type FederalInfoInput = {
  federalIDType?: Maybe<FederalIdentificationType>;
  federalIDNumber?: Maybe<Scalars['String']>;
};

export type FieldActivity = {
   __typename?: 'FieldActivity';
  ScheduledDisconnectDate?: Maybe<Scalars['DateTimeISO']>;
  PendingReconnect: Scalars['Boolean'];
  Disconnected: Scalars['Boolean'];
};

export type FilterParams = {
  filterBy?: Maybe<Scalars['String']>;
  operator: Operator;
};

export enum ForgotPasswordErrorType {
  None = 'None',
  NoValidWebUser = 'NoValidWebUser',
  NoMatchFound = 'NoMatchFound',
  NoPersonFound = 'NoPersonFound',
  NoPhoneFound = 'NoPhoneFound',
  SpecialHandling = 'SpecialHandling'
}

export type ForgotPasswordRequest = {
  emailAddress?: Maybe<Scalars['String']>;
  phoneNumber?: Maybe<Scalars['String']>;
  lastFourDigitSSN?: Maybe<Scalars['String']>;
  lastFourDigitStateOrDriverID?: Maybe<Scalars['String']>;
  pinCode?: Maybe<Scalars['String']>;
  birthDate?: Maybe<Scalars['DateTimeCustom']>;
  lastFourDigitEIN?: Maybe<Scalars['String']>;
  identificationType?: Maybe<PersonalIdentificationType>;
  lastFourDigitITIN?: Maybe<Scalars['String']>;
};

export type ForgotPasswordResponse = {
   __typename?: 'ForgotPasswordResponse';
  forgotPasswordError?: Maybe<ForgotPasswordErrorType>;
  resetPasswordFlag?: Maybe<Scalars['Boolean']>;
  securityToken?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  signInToken?: Maybe<Scalars['String']>;
};

export enum FromSidEnum {
  Csshortcode1 = 'CSSHORTCODE1',
  Cslongcode1 = 'CSLONGCODE1'
}

export type GetAlertDetailsRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
};

export type Group = {
   __typename?: 'Group';
  groupName: Scalars['String'];
  groupId: Scalars['ID'];
  groupCode: Scalars['String'];
  numberOfAccounts?: Maybe<Scalars['Int']>;
  isDefault: Scalars['Boolean'];
  type: GroupType;
  isDefaultAccountExists: Scalars['Boolean'];
  isLoggedOnUserAutoGroup: Scalars['Boolean'];
  defaultAccount?: Maybe<DefaultAccountInfo>;
  accounts?: Maybe<Array<Maybe<AccountDetail>>>;
};


export type GroupAccountsArgs = {
  accountInfoParams?: Maybe<AccountInfoParams>;
};

export type GroupDetailsRequest = {
  groupCode: Scalars['String'];
  groupName: Scalars['String'];
  isDefault: Scalars['Boolean'];
};

export type GroupInfoParams = {
  filter?: Maybe<FilterParams>;
  paging?: Maybe<PagingParams>;
  sort?: Maybe<SortParams>;
};

export type GroupResponse = {
  groupid?: Maybe<Scalars['String']>;
  groupcode?: Maybe<Scalars['String']>;
  groupname?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
  groupType?: Maybe<GroupTypeRequest>;
  accounts?: Maybe<Array<Maybe<CustomerPreferencesAccountResponse>>>;
};

export enum GroupSort {
  Default = 'DEFAULT',
  Grouptype = 'GROUPTYPE',
  Numberofaccounts = 'NUMBEROFACCOUNTS'
}

export type GroupsResponse = {
  groups?: Maybe<Array<Maybe<GroupResponse>>>;
};

export enum GroupStatus {
  Deleted = 'Deleted',
  Preserved = 'Preserved',
  Created = 'Created',
  NotCreated = 'NotCreated'
}

export enum GroupType {
  Automatic = 'Automatic',
  Custom = 'Custom',
  Virtual = 'Virtual'
}

export enum GroupTypeRequest {
  Custom = 'CUSTOM',
  Auto = 'AUTO'
}

export type GuestAccountDetails = {
   __typename?: 'GuestAccountDetails';
  isFound: Scalars['Boolean'];
  foundMultiple: Scalars['Boolean'];
};

export type GuestOutageDetail = {
   __typename?: 'GuestOutageDetail';
  isEligibleToReport: Scalars['Boolean'];
  isAccountExists?: Maybe<Scalars['Boolean']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPremiseId?: Maybe<Scalars['String']>;
  encryptedServicePointId?: Maybe<Scalars['String']>;
  hasKnownOutage?: Maybe<Scalars['Boolean']>;
  houseNumber?: Maybe<Scalars['String']>;
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested?: Maybe<Scalars['Boolean']>;
  estimatedTimeOut?: Maybe<Scalars['String']>;
  estimatedTimeOn?: Maybe<Scalars['String']>;
  numberOfCustomersAffected?: Maybe<Scalars['Int']>;
  numberOfReports?: Maybe<Scalars['Int']>;
  crewDispatchedStatus?: Maybe<Scalars['String']>;
  isCrewOnsite?: Maybe<Scalars['Boolean']>;
  cause?: Maybe<Scalars['String']>;
  restorePowerAlert?: Maybe<OutageAlert>;
  ineligibleReason?: Maybe<Scalars['String']>;
  multipleAccountList?: Maybe<Array<Maybe<OutageMultipleAccountDetail>>>;
};

export type GuestPayInfo = {
   __typename?: 'GuestPayInfo';
  encryptedAccountNumber: Scalars['String'];
  lastFourdDigitAccount: Scalars['String'];
  amountDue: Scalars['Float'];
  houseNumber?: Maybe<Scalars['String']>;
};

export type GuestPayInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  paymentAmount: Scalars['Float'];
  tokenId: Scalars['String'];
  customerName: Scalars['String'];
  guestEmailAddress?: Maybe<Scalars['String']>;
  guestTextPhone?: Maybe<Scalars['String']>;
};

export type GuestPayInputForIva = {
  accountNumber?: Maybe<Scalars['String']>;
  paymentAmount: Scalars['Float'];
  tokenId: Scalars['String'];
  customerName: Scalars['String'];
  guestEmailAddress?: Maybe<Scalars['String']>;
  guestTextPhone?: Maybe<Scalars['String']>;
};

export type GuestPaymentAccountInput = {
  zipCode: Scalars['String'];
  phoneNumber?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  identificationType: IdentificationType;
};

export type GuestPaymentConfirmation = {
   __typename?: 'GuestPaymentConfirmation';
  lastFourdDigitAccount: Scalars['String'];
  confirmationId?: Maybe<Scalars['String']>;
  isSuccess: Scalars['Boolean'];
  paymentAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['String']>;
  cardType?: Maybe<Scalars['String']>;
  lastFourDigitsCardNumber?: Maybe<Scalars['String']>;
  errorReason?: Maybe<PaymentErrorReason>;
  errorMessage?: Maybe<PaymentErrorMessage>;
};

export type GuestPaymentDetails = {
   __typename?: 'GuestPaymentDetails';
  guestAccountDetails: GuestAccountDetails;
  eligibility?: Maybe<GuestPaymentEligibility>;
  paymentInfo?: Maybe<GuestPayInfo>;
};

export type GuestPaymentDownloadPdfInput = {
  confirmationNumber?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  paymentDate?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['String']>;
};

export type GuestPaymentEligibility = {
   __typename?: 'GuestPaymentEligibility';
  isEligible?: Maybe<Scalars['Boolean']>;
};

export type GuestPaymentPdf = {
   __typename?: 'GuestPaymentPdf';
  success?: Maybe<Scalars['Boolean']>;
  pdf?: Maybe<Scalars['String']>;
};

export type GuestPaymentVerify = {
   __typename?: 'GuestPaymentVerify';
  success?: Maybe<Scalars['Boolean']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type HasPersonIdResponse = {
   __typename?: 'HasPersonIdResponse';
  hasPerson?: Maybe<Scalars['Boolean']>;
};

export enum IdentificationType {
  PhoneNumber = 'phoneNumber',
  AccountNumber = 'accountNumber'
}

export type IdentityInput = {
  value: Scalars['String'];
  type: IdentityType;
};

export enum IdentityType {
  Dl = 'DL',
  Ssn = 'SSN',
  Dob = 'DOB'
}

export type IFrameInfoRequest = {
  postMessagePmDetailsOrigin: Scalars['String'];
  pmCategory: PaymentCategory;
  token?: Maybe<Scalars['String']>;
  ownerId?: Maybe<Scalars['String']>;
  lang?: Maybe<Scalars['String']>;
  firstName?: Maybe<Scalars['String']>;
  lastName?: Maybe<Scalars['String']>;
  email?: Maybe<Scalars['String']>;
  cvv?: Maybe<Scalars['Boolean']>;
  ignoreCase?: Maybe<Scalars['Boolean']>;
  nickname?: Maybe<Scalars['Boolean']>;
  primaryPM?: Maybe<Scalars['Boolean']>;
};

export type IFrameInfoResponse = {
   __typename?: 'IFrameInfoResponse';
  src: Scalars['String'];
};

export type IsEmailExistResponse = {
   __typename?: 'IsEmailExistResponse';
  exist?: Maybe<Scalars['Boolean']>;
};

export type IsPaperlessBillEnrolledRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type IsPaperlessBillEnrolledResponse = {
   __typename?: 'IsPaperlessBillEnrolledResponse';
  result?: Maybe<Scalars['Boolean']>;
};

export type IsPersonalIdValidParams = {
  encryptedPersonId: Scalars['String'];
  idNumber: Scalars['String'];
};


export type KeyValue = {
   __typename?: 'KeyValue';
  key?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
};


export type ListProfilesRequest = {
  loginId: Scalars['String'];
};

export type ListProfilesResponse = {
   __typename?: 'ListProfilesResponse';
  listProfilesResponse: PaymentProfileList;
};



export type MailingAddress = {
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  state: Scalars['String'];
  postal: Scalars['String'];
  country: Scalars['String'];
};

export type MeterAccessInput = {
  hasSecurityGate?: Maybe<Scalars['Boolean']>;
  hasDog?: Maybe<Scalars['Boolean']>;
  meterAccessMessage?: Maybe<Scalars['String']>;
};

export type MonthlyUsage = {
   __typename?: 'MonthlyUsage';
  monthName?: Maybe<Scalars['String']>;
  totalKwh?: Maybe<Scalars['Int']>;
  year?: Maybe<Scalars['Int']>;
};

export type MoveServiceEligibilityRequest = {
  encryptedAccountNumber: Scalars['String'];
  serviceAddress: AddressInput;
};

export type MoveServiceEligibilityResponse = {
   __typename?: 'MoveServiceEligibilityResponse';
  eligibleForMoveService: Scalars['Boolean'];
  eligibleForGreenResource: Scalars['Boolean'];
  isCurrentlyPaperlessBilling: Scalars['Boolean'];
};

export type MoveServiceRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  anyoneRemainingAtProperty?: Maybe<Scalars['Boolean']>;
  currentServiceAddress?: Maybe<AddressInput>;
  customerInformation?: Maybe<CustomerInfoInput>;
  eligibleForGreenResource?: Maybe<Scalars['Boolean']>;
  eligibleForMoveService?: Maybe<Scalars['Boolean']>;
  heatSourceType?: Maybe<Scalars['String']>;
  isCurrentlyPaperlessBilling?: Maybe<Scalars['Boolean']>;
  isElectricVehicleSelected?: Maybe<Scalars['Boolean']>;
  isPaperlessBillingSelected?: Maybe<Scalars['Boolean']>;
  isRenewablesSelected?: Maybe<Scalars['Boolean']>;
  isSmartThermostatSelected?: Maybe<Scalars['Boolean']>;
  livesAtPremise?: Maybe<Scalars['Boolean']>;
  meterAccessInfo?: Maybe<MeterAccessInput>;
  moveToServiceAddress?: Maybe<AddressInput>;
  selectedPremiseType?: Maybe<PremiseType>;
  startDate?: Maybe<Scalars['String']>;
  stopDate?: Maybe<Scalars['String']>;
  waterHeaterType?: Maybe<Scalars['String']>;
  coCustomersInformation?: Maybe<Array<Maybe<CustomerInfoInput>>>;
  preferredDueDate?: Maybe<Scalars['String']>;
  isAddressExactMatch?: Maybe<Scalars['Boolean']>;
  premiseId?: Maybe<Scalars['String']>;
};

export type MoveServiceSubmitResponse = {
   __typename?: 'MoveServiceSubmitResponse';
  isSuccessful: Scalars['Boolean'];
};

export type MoveToServiceAddressEligibilityRequest = {
  encryptedAccountNumber: Scalars['String'];
  serviceAddress: AddressInput;
  selectedPremiseType: PremiseType;
};

export type MoveToServiceAddressEligibilityResponse = {
   __typename?: 'MoveToServiceAddressEligibilityResponse';
  isEligible: Scalars['Boolean'];
  isAddressExactMatch: Scalars['Boolean'];
  isPeakTimeRebateEligible: Scalars['Boolean'];
  premiseId: Scalars['String'];
  serviceAddressEligibilityType?: Maybe<ServiceAddressEligibilityType>;
};

export type MultiPaymentDetail = {
   __typename?: 'MultiPaymentDetail';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  confirmationNumber?: Maybe<Scalars['String']>;
};

export type Mutation = {
   __typename?: 'Mutation';
  root?: Maybe<Scalars['String']>;
  addRemoveAccountsToGroup: AccountRemoveAccountsResponse;
  createAccountGroup: AccountGroupResponse;
  createCustomGroup: AccountGroupResponse;
  deleteGroup: DeleteGroupResponse;
  updateGroup: UpdateGroupResponse;
  updateAccountDetails: UpdateAccountDetailsResponse;
  updateServiceAgreementNickname: UpdateServiceAgreementResponse;
  updateContactInfoConfirmation: ContactInfoConfirmationResponse;
  enrollTpa: Tpa_EnrollmentConfirmation;
  updateAlerts?: Maybe<UpdateAlertsResponse>;
  updatePaperlessBill?: Maybe<UpdatePaperlessBillResponse>;
  submitCommPreferences: SubmitCommPreferencesResponse;
  sendActivationCode?: Maybe<SendActivationCodeResponse>;
  verifyEqualsEncryptedValue?: Maybe<VerifyEqualsEncryptedValueResponse>;
  createCustomerContact: CustomerContactResponse;
  sendSmsText: Scalars['Boolean'];
  moveServiceSubmit?: Maybe<MoveServiceSubmitResponse>;
  reportOutageBySpList?: Maybe<ReportOutageResponse>;
  reportOutageBySpListGuest?: Maybe<ReportOutageResponse>;
  outage?: Maybe<Scalars['Boolean']>;
  reportOutage?: Maybe<ReportOutageResponse>;
  reportOutageByGuest?: Maybe<ReportOutageResponse>;
  enrollAutoPay: EnrollAutoPay;
  cancelAutoPay: CancelAutoPay;
  postBackAutoPayEnroll: Scalars['Boolean'];
  cancelAutoPayIVA: Scalars['Boolean'];
  submitGuestPayment: GuestPaymentConfirmation;
  submitGuestPaymentForIVA: GuestPaymentConfirmation;
  submitOnecheckPayment?: Maybe<OnecheckPaymentSubmitResponse>;
  enrollPaymentExtension?: Maybe<PaymentmentExtensionEnrollResponse>;
  enrollToPaymentExtension?: Maybe<PaymentmentExtensionEnrollResponse>;
  makePayment: PaymentConfirmationList;
  postBackPayment: PostBackPaymentConfirmation;
  postBackPaymentForIVA: PostBackPaymentConfirmationForIva;
  addPreferredDueDate?: Maybe<PreferredDueDateAddResponse>;
  submitQuickPayment: QuickPayConfirmation;
  deleteProfile?: Maybe<DeleteProfileResponse>;
  enrollPeakTimeRebate?: Maybe<PeaktimeRebateSuccessResponse>;
  updatePeakTimeRebate?: Maybe<PeaktimeRebateSuccessResponse>;
  cancelPeakTimeRebate?: Maybe<PeaktimeRebateSuccessResponse>;
  submitPeakTimeRebateGiveBack?: Maybe<PeakTimeRebateGiveBack>;
  createRenewablesEnrollmentTodo?: Maybe<Scalars['Boolean']>;
  updateRenewables?: Maybe<Scalars['Boolean']>;
  updateProfilePhone: Scalars['Boolean'];
  updateMailingAddress: Scalars['Boolean'];
  registerResidential?: Maybe<RegistrationResponse>;
  registerCommercial?: Maybe<RegistrationResponse>;
  unregister?: Maybe<UnregisterResponse>;
  startServiceCreateIneligibilityLog?: Maybe<Scalars['Boolean']>;
  startServiceSubmit?: Maybe<StartServiceResponse>;
  createPersonLeavingBehindContactLog?: Maybe<Scalars['Boolean']>;
  stopServiceSubmit?: Maybe<Scalars['String']>;
  stopServiceCreate?: Maybe<Scalars['String']>;
  submitTimeOfDayEnrollment: TimeOfDayEnrollmentConfirmation;
  submitTimeOfDayUnenrollment: TimeOfDayUnenrollmentConfirmation;
  updateAccountInfo?: Maybe<Scalars['Boolean']>;
  updateAccountInformation?: Maybe<Scalars['Boolean']>;
  updateEmail?: Maybe<ChangeEmailResponse>;
  updatePassword?: Maybe<ChangePasswordUpdateResponse>;
  updatePin?: Maybe<ChangePinUpdateResponse>;
  validateAndUpdateTempPassword?: Maybe<ForgotPasswordResponse>;
};


export type MutationAddRemoveAccountsToGroupArgs = {
  payload: AddRemoveAccountsInput;
};


export type MutationCreateAccountGroupArgs = {
  payload: CreateAccountGroupInput;
};


export type MutationCreateCustomGroupArgs = {
  payload: CreateGroupInput;
};


export type MutationDeleteGroupArgs = {
  payload: DeleteGroupInput;
};


export type MutationUpdateGroupArgs = {
  payload: UpdateGroupInput;
};


export type MutationUpdateAccountDetailsArgs = {
  payload: AccountDetailsInput;
};


export type MutationUpdateServiceAgreementNicknameArgs = {
  payload: ServiceAgreementNicknameInput;
};


export type MutationUpdateContactInfoConfirmationArgs = {
  payload: ContactInfoConfirmationRequest;
};


export type MutationEnrollTpaArgs = {
  payload: Tpa_EnrollInput;
};


export type MutationUpdateAlertsArgs = {
  payload?: Maybe<UpdateAlertsRequest>;
};


export type MutationUpdatePaperlessBillArgs = {
  payload?: Maybe<UpdatePaperlessBillRequest>;
};


export type MutationSubmitCommPreferencesArgs = {
  payload: SubmitCommPreferencesRequest;
};


export type MutationSendActivationCodeArgs = {
  payload?: Maybe<SendActivationCodeRequest>;
};


export type MutationVerifyEqualsEncryptedValueArgs = {
  payload?: Maybe<VerifyEqualsEncryptedValueRequest>;
};


export type MutationCreateCustomerContactArgs = {
  params: CustomerContactInput;
};


export type MutationSendSmsTextArgs = {
  input: SendSmsTextInput;
};


export type MutationMoveServiceSubmitArgs = {
  payload?: Maybe<MoveServiceRequest>;
};


export type MutationReportOutageBySpListArgs = {
  payload: ReportOutageBySpListRequest;
};


export type MutationReportOutageBySpListGuestArgs = {
  payload: ReportOutageBySpListGuestRequest;
};


export type MutationOutageArgs = {
  payload?: Maybe<OutageReport>;
};


export type MutationReportOutageArgs = {
  payload?: Maybe<ReportOutageRequest>;
};


export type MutationReportOutageByGuestArgs = {
  payload?: Maybe<ReportOutageByGuestRequest>;
};


export type MutationEnrollAutoPayArgs = {
  payload: AutoPayEnrollInput;
};


export type MutationCancelAutoPayArgs = {
  payload: AutoPayCancelInput;
};


export type MutationPostBackAutoPayEnrollArgs = {
  payload: PostAutoPayEnrollInput;
};


export type MutationCancelAutoPayIvaArgs = {
  payload: AutoPayCancelIvaInput;
};


export type MutationSubmitGuestPaymentArgs = {
  payload: GuestPayInput;
};


export type MutationSubmitGuestPaymentForIvaArgs = {
  payload: GuestPayInputForIva;
};


export type MutationSubmitOnecheckPaymentArgs = {
  payload?: Maybe<OnecheckPaymentSubmitRequest>;
};


export type MutationEnrollPaymentExtensionArgs = {
  payload?: Maybe<PaymentExtensionEnrollInput>;
};


export type MutationEnrollToPaymentExtensionArgs = {
  payload?: Maybe<EnrollToPaymentExtensionInput>;
};


export type MutationMakePaymentArgs = {
  payload: PaymentInputList;
};


export type MutationPostBackPaymentArgs = {
  payload: PostBackPaymentInput;
};


export type MutationPostBackPaymentForIvaArgs = {
  payload: PostBackPaymentForIvaInput;
};


export type MutationAddPreferredDueDateArgs = {
  payload?: Maybe<PreferredDueDateAddRequest>;
};


export type MutationSubmitQuickPaymentArgs = {
  payload: QuickPayInput;
};


export type MutationDeleteProfileArgs = {
  payload?: Maybe<DeleteProfileRequest>;
};


export type MutationEnrollPeakTimeRebateArgs = {
  payload: PeaktimeRebateUpsertInput;
};


export type MutationUpdatePeakTimeRebateArgs = {
  payload: PeaktimeRebateUpsertInput;
};


export type MutationCancelPeakTimeRebateArgs = {
  payload: PeakTimeRebateParams;
};


export type MutationSubmitPeakTimeRebateGiveBackArgs = {
  payload: PeakTimeRebateGiveBackDetailsInput;
};


export type MutationCreateRenewablesEnrollmentTodoArgs = {
  payload?: Maybe<RenewablesEnrollmentToDoRequest>;
};


export type MutationUpdateRenewablesArgs = {
  payload?: Maybe<RenewablePowerInput>;
};


export type MutationUpdateProfilePhoneArgs = {
  payload: UpdateProfilePhoneInput;
};


export type MutationUpdateMailingAddressArgs = {
  payload: UpdateMailingAddressInput;
};


export type MutationRegisterResidentialArgs = {
  payload: ResidentialRegistrationRequest;
};


export type MutationRegisterCommercialArgs = {
  payload: CommercialRegistrationRequest;
};


export type MutationStartServiceCreateIneligibilityLogArgs = {
  payload?: Maybe<StartServiceIneligibilityLogRequest>;
};


export type MutationStartServiceSubmitArgs = {
  payload?: Maybe<StartServiceRequest>;
};


export type MutationCreatePersonLeavingBehindContactLogArgs = {
  payload?: Maybe<ContactlogPersonLeavingBehindRequest>;
};


export type MutationStopServiceSubmitArgs = {
  payload?: Maybe<StopServiceSubmitRequest>;
};


export type MutationStopServiceCreateArgs = {
  payload?: Maybe<StopServiceCreateInput>;
};


export type MutationSubmitTimeOfDayEnrollmentArgs = {
  payload: TimeOfDayEnrollmentInput;
};


export type MutationSubmitTimeOfDayUnenrollmentArgs = {
  payload: TimeOfDayUnenrollmentInput;
};


export type MutationUpdateAccountInfoArgs = {
  payload?: Maybe<SubmitUpdateInfoInput>;
};


export type MutationUpdateAccountInformationArgs = {
  payload?: Maybe<SubmitUpdateInformationInput>;
};


export type MutationUpdateEmailArgs = {
  payload?: Maybe<ChangeEmailRequest>;
};


export type MutationUpdatePasswordArgs = {
  payload?: Maybe<ChangePasswordUpdateRequest>;
};


export type MutationUpdatePinArgs = {
  payload?: Maybe<ChangePinUpdateRequest>;
};


export type MutationValidateAndUpdateTempPasswordArgs = {
  payload?: Maybe<ForgotPasswordRequest>;
};

export type MutiPaymentSubmitResponse = {
   __typename?: 'MutiPaymentSubmitResponse';
  confirmationNumbers?: Maybe<Array<Maybe<MultiPaymentDetail>>>;
};

export type NextBillInfo = {
   __typename?: 'NextBillInfo';
  billDate?: Maybe<Scalars['DateTimeISO']>;
};

export enum NotificationType {
  Unknown = 'Unknown',
  Out = 'OUT',
  Webpyrcv = 'WEBPYRCV',
  Webpda = 'WEBPDA',
  Webdisc = 'WEBDISC',
  Webpyrem = 'WEBPYREM',
  Webuse = 'WEBUSE',
  Webexc = 'WEBEXC',
  Paperless = 'PAPERLESS',
  Rest = 'REST'
}

export type NotificationTypeInput = {
  notificationType: NotificationType;
  deliveryTypes: Array<DeliveryTypeInput>;
};

export enum OnecheckDisplayOption {
  AmountDue = 'AmountDue',
  LastBilledAmount = 'LastBilledAmount'
}

export type OnecheckPaymentAccount = {
   __typename?: 'OnecheckPaymentAccount';
  accountNumber?: Maybe<Scalars['String']>;
  amountDue?: Maybe<Scalars['Float']>;
  addresses?: Maybe<Array<Maybe<Scalars['String']>>>;
  lastBilledDate?: Maybe<Scalars['String']>;
  lastBilledAmount?: Maybe<Scalars['Float']>;
  billDueDate?: Maybe<Scalars['String']>;
  downloadBillUrl?: Maybe<Scalars['String']>;
};

export type OnecheckPaymentAccountRequest = {
  accountNumber: Scalars['String'];
  amountDue: Scalars['Float'];
  payment: Scalars['Float'];
  addresses?: Maybe<Array<Maybe<Scalars['String']>>>;
  paymentOriginal: Scalars['Float'];
  lastBilledDate?: Maybe<Scalars['String']>;
  lastBilledAmount?: Maybe<Scalars['Float']>;
  billDueDate?: Maybe<Scalars['String']>;
};

export type OnecheckPaymentDownloadRemittanceFormResponse = {
   __typename?: 'OnecheckPaymentDownloadRemittanceFormResponse';
  pdf?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type OnecheckPaymentGroupInfoRequest = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  customGroupName?: Maybe<Scalars['String']>;
};

export type OnecheckPaymentGroupInfoResponse = {
   __typename?: 'OnecheckPaymentGroupInfoResponse';
  numberOfAccounts?: Maybe<Scalars['Int']>;
  accounts?: Maybe<Array<Maybe<OnecheckPaymentAccount>>>;
  totalAmountDue?: Maybe<Scalars['Float']>;
};

export type OnecheckPaymentInfoResponse = {
   __typename?: 'OnecheckPaymentInfoResponse';
  amountDueDetails?: Maybe<OnecheckPaymentGroupInfoResponse>;
  lastBilledAmountDetails?: Maybe<OnecheckPaymentGroupInfoResponse>;
};

export type OnecheckPaymentSubmitRequest = {
  accountGroup?: Maybe<Scalars['String']>;
  encryptedPersonId: Scalars['String'];
  displayOption?: Maybe<OnecheckDisplayOption>;
  numberOfAccounts?: Maybe<Scalars['Int']>;
  totalAmountDue: Scalars['Float'];
  oneCheckTotal: Scalars['Float'];
  todaysDate: Scalars['String'];
  remittanceId?: Maybe<Scalars['String']>;
  isCustomGroup?: Maybe<Scalars['Boolean']>;
  accounts: Array<OnecheckPaymentAccountRequest>;
};

export type OnecheckPaymentSubmitResponse = {
   __typename?: 'OnecheckPaymentSubmitResponse';
  success?: Maybe<Scalars['Boolean']>;
  remittanceId?: Maybe<Scalars['String']>;
};

export type OnetimePaymentDownloadPdfRequest = {
  name?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<Scalars['String']>;
  bankAccountRoutingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  confirmationNumber?: Maybe<Scalars['String']>;
  paymentAmount?: Maybe<Scalars['String']>;
  paymentDate?: Maybe<Scalars['String']>;
  isFutureDatedPayment?: Maybe<Scalars['Boolean']>;
};

export type OnetimePaymentPdfResponse = {
   __typename?: 'OnetimePaymentPdfResponse';
  success?: Maybe<Scalars['Boolean']>;
  pdf?: Maybe<Scalars['String']>;
};

export enum OnlineAccountType {
  Unknown = 'Unknown',
  PgeResidentialAcct = 'PGEResidentialAcct',
  PgeCommercialAcct = 'PGECommercialAcct',
  PgeAgencyAcct = 'PGEAgencyAcct',
  PgeEssAcct = 'PGEEssAcct'
}

export enum Operator {
  Startswith = 'STARTSWITH'
}

export enum Organization {
  None = 'None',
  CentralCityConcern = 'CentralCityConcern',
  CommunityEnergyProject = 'CommunityEnergyProject',
  WorkingTheoryFarm = 'WorkingTheoryFarm'
}

export type Outage = {
   __typename?: 'Outage';
  name?: Maybe<Scalars['String']>;
  coords: Coordinates;
  cause?: Maybe<Scalars['String']>;
  affected?: Maybe<Scalars['Int']>;
  calls?: Maybe<Scalars['Int']>;
  estimatedTimeOn?: Maybe<Scalars['String']>;
};

export type OutageAlert = {
   __typename?: 'OutageAlert';
  isEmail: Scalars['Boolean'];
  encryptedEmailNotificationId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText: Scalars['Boolean'];
  encryptedTextNotificationId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type OutageByCountyParams = {
  county?: Maybe<Scalars['String']>;
};

export type OutageDetail = {
   __typename?: 'OutageDetail';
  isEligibleToReport?: Maybe<Scalars['Boolean']>;
  accountNumber?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPremiseId?: Maybe<Scalars['String']>;
  encryptedServicePointId?: Maybe<Scalars['String']>;
  hasKnownOutage?: Maybe<Scalars['Boolean']>;
  serviceAddress?: Maybe<OutageServiceAddress>;
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested?: Maybe<Scalars['Boolean']>;
  estimatedTimeOut?: Maybe<Scalars['String']>;
  estimatedTimeOn?: Maybe<Scalars['String']>;
  numberOfCustomersAffected?: Maybe<Scalars['Int']>;
  numberOfReports?: Maybe<Scalars['Int']>;
  crewDispatchedStatus?: Maybe<Scalars['String']>;
  isCrewOnsite?: Maybe<Scalars['Boolean']>;
  cause?: Maybe<Scalars['String']>;
  restorePowerAlert?: Maybe<OutageAlert>;
  ineligibleReason?: Maybe<Scalars['String']>;
  multipleAccountList?: Maybe<Array<Maybe<OutageMultipleAccountDetail>>>;
};

export type OutageDetailByGuestParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['String']>;
  lookUpType: OutageLookUpType;
};

export type OutageDetailParams = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type OutageDetailsByCounty = {
   __typename?: 'OutageDetailsByCounty';
  totalCustomersImpacted: Scalars['Int'];
  outages: Scalars['Int'];
  county: Scalars['String'];
  zipCodeInfo: Array<ZipCodeInfo>;
};

export type OutageDetailsByZipCode = {
   __typename?: 'OutageDetailsByZipCode';
  zipCode: Scalars['String'];
  cause: Scalars['String'];
  totalCustomersImpacted: Scalars['Int'];
  estimatedRestorationOn?: Maybe<Scalars['DateTimeISO']>;
  estimatedRestorationOut?: Maybe<Scalars['DateTimeISO']>;
  lat?: Maybe<Scalars['Latitude']>;
  long?: Maybe<Scalars['Longitude']>;
};

export type OutageDetailsByZipCodeParams = {
  zipCode: Scalars['String'];
};

export type OutageEvent = {
   __typename?: 'OutageEvent';
  eventId: Scalars['ID'];
  lat: Scalars['Latitude'];
  long: Scalars['Longitude'];
  totalCustomersImpacted: Scalars['Int'];
  polygon?: Maybe<Array<Maybe<OutagePolygon>>>;
};

export type OutageEventDetails = {
   __typename?: 'OutageEventDetails';
  totalCustomersImpacted: Scalars['Int'];
  crewDispatchedStatus?: Maybe<Scalars['String']>;
  estimatedTimeOut?: Maybe<Scalars['DateTimeISO']>;
  estimatedTimeOn?: Maybe<Scalars['DateTimeISO']>;
  cause?: Maybe<Scalars['String']>;
  lat?: Maybe<Scalars['Latitude']>;
  long?: Maybe<Scalars['Longitude']>;
  isStorm?: Maybe<Scalars['Boolean']>;
  noOfReports?: Maybe<Scalars['Int']>;
};

export type OutageEventParams = {
  outageEventId: Scalars['ID'];
};

export type OutageEvents = {
   __typename?: 'OutageEvents';
  events?: Maybe<Array<Maybe<OutageEvent>>>;
  lastUpdateTime?: Maybe<Scalars['DateTimeISO']>;
};

export type OutageInfo = {
   __typename?: 'OutageInfo';
  numberOfReports?: Maybe<Scalars['Int']>;
  crewDispatchedStatus?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  estimatedTimeOn?: Maybe<Scalars['DateTimeISO']>;
  hasKnownOutage?: Maybe<Scalars['Boolean']>;
  estimatedTimeOut?: Maybe<Scalars['DateTimeISO']>;
  outageDueToStorm?: Maybe<Scalars['Boolean']>;
  callbackRequested?: Maybe<Scalars['Boolean']>;
  registeredPhone?: Maybe<Scalars['String']>;
  numberOfCustomersAffected?: Maybe<Scalars['Int']>;
  cause?: Maybe<Scalars['String']>;
  outageAlreadyReported?: Maybe<Scalars['Boolean']>;
};

export type OutageLookupMultipleOption = {
   __typename?: 'OutageLookupMultipleOption';
  encryptedValue: Scalars['String'];
  houseNumber: Scalars['String'];
  last4CharAddress?: Maybe<Scalars['String']>;
};

export type OutageLookupParams = {
  value?: Maybe<Scalars['String']>;
  lookupType: OutageLookUpType;
  encryptedValue?: Maybe<Scalars['String']>;
};

export type OutageLookupResponse = {
   __typename?: 'OutageLookupResponse';
  isAccountExists: Scalars['Boolean'];
  isEligibleToReport: Scalars['Boolean'];
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPremiseId?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
  multiplePremiseList?: Maybe<Array<OutageLookupMultipleOption>>;
  outages?: Maybe<Array<ServicePointOutageDetails>>;
};

export enum OutageLookUpType {
  Account = 'ACCOUNT',
  Phone = 'PHONE',
  Premise = 'PREMISE'
}

export type OutageMultipleAccountDetail = {
   __typename?: 'OutageMultipleAccountDetail';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
};

export type OutagePolygon = {
   __typename?: 'OutagePolygon';
  lat: Scalars['Latitude'];
  long: Scalars['Longitude'];
};

export type OutageReport = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  encryptedServicePointId: Scalars['String'];
  source: OutageSource;
  phoneNumber?: Maybe<Scalars['String']>;
};

export type OutageRestoredAlert = {
  type: AlertType;
  encryptedContactId: Scalars['String'];
  encryptedNotificationId?: Maybe<Scalars['String']>;
  contactValue: Scalars['String'];
  isSelected: Scalars['Boolean'];
};

export type OutageServiceAddress = {
   __typename?: 'OutageServiceAddress';
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export type OutagesList = {
   __typename?: 'OutagesList';
  outages?: Maybe<OutagesListDetail>;
};

export type OutagesListCounty = {
   __typename?: 'OutagesListCounty';
  attr?: Maybe<OutagesListCountyAttr>;
  zip?: Maybe<Array<Maybe<OutagesListZip>>>;
};

export type OutagesListCountyAttr = {
   __typename?: 'OutagesListCountyAttr';
  name?: Maybe<Scalars['String']>;
  outages?: Maybe<Scalars['String']>;
  affected?: Maybe<Scalars['String']>;
};

export type OutagesListDetail = {
   __typename?: 'OutagesListDetail';
  timestamp?: Maybe<Array<Maybe<Scalars['DateTimeCustom']>>>;
  qualifying_value?: Maybe<Array<Maybe<Scalars['String']>>>;
  county?: Maybe<Array<Maybe<OutagesListCounty>>>;
};

export type OutagesListOutage = {
   __typename?: 'OutagesListOutage';
  attr?: Maybe<OutagesListOutageDetailAttr>;
};

export type OutagesListOutageDetailAttr = {
   __typename?: 'OutagesListOutageDetailAttr';
  cause?: Maybe<Scalars['String']>;
  eto?: Maybe<Scalars['DateTimeCustom']>;
  customers?: Maybe<Scalars['String']>;
  calls?: Maybe<Scalars['String']>;
  etr?: Maybe<Scalars['DateTimeCustom']>;
  outage_status?: Maybe<Scalars['String']>;
  crew_status?: Maybe<Scalars['String']>;
  planned_outage?: Maybe<Scalars['String']>;
};

export type OutagesListZip = {
   __typename?: 'OutagesListZip';
  attr?: Maybe<OutagesListZipAttr>;
  outage?: Maybe<Array<Maybe<OutagesListOutage>>>;
};

export type OutagesListZipAttr = {
   __typename?: 'OutagesListZipAttr';
  zip?: Maybe<Scalars['String']>;
};

export enum OutageSource {
  Ivr = 'IVR',
  Web = 'WEB'
}

export type PagingParams = {
  limit: Scalars['Int'];
  offset: Scalars['Int'];
};

export type PaymentAlerts = {
   __typename?: 'PaymentAlerts';
  isFutureDated?: Maybe<Scalars['Boolean']>;
  isMultipleSameDayPayment?: Maybe<Scalars['Boolean']>;
  isSingleSameDayPayment?: Maybe<Scalars['Boolean']>;
  lastPaidAmount?: Maybe<Scalars['Float']>;
  /** @deprecated use totalFutureDatedPaymentAmount, which is sum of all future dated payments */
  futureDatedPaymentAmount?: Maybe<Scalars['Float']>;
  totalFutureDatedPaymentAmount?: Maybe<Scalars['Float']>;
};

export type PaymentBankInfoResponse = {
   __typename?: 'PaymentBankInfoResponse';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  routingNumber?: Maybe<Scalars['String']>;
  bankAccountNumber?: Maybe<Scalars['String']>;
  isPaymentDueInNextThreeDays?: Maybe<Scalars['Boolean']>;
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['String']>;
};

export enum PaymentCategory {
  Cc = 'CC',
  Dc = 'DC',
  Dd = 'DD',
  AmazonPay = 'AMAZON_PAY',
  PaypalCredit = 'PAYPAL_CREDIT',
  PaypalAccount = 'PAYPAL_ACCOUNT'
}

export type PaymentConfirmation = {
   __typename?: 'PaymentConfirmation';
  accountNumber: Scalars['String'];
  confirmationId?: Maybe<Scalars['String']>;
  isSuccess: Scalars['Boolean'];
  paymentAmount: Scalars['Float'];
  paymentMethod: Scalars['String'];
  cardType: Scalars['String'];
  lastFourDigitsCardNumber: Scalars['String'];
  digitalWalletEmail?: Maybe<Scalars['String']>;
  pendingDisconnect?: Maybe<PendingDisconnectStatus>;
  errorReason?: Maybe<PaymentErrorReason>;
  errorMessage?: Maybe<PaymentErrorMessage>;
};

export type PaymentConfirmationList = {
   __typename?: 'PaymentConfirmationList';
  paymentConfirmation: Array<PaymentConfirmation>;
  isPartialSucceed: Scalars['Boolean'];
};

export type PaymentEligibility = {
   __typename?: 'PaymentEligibility';
  isCashOnly: Scalars['Boolean'];
  isNonBillableNoBalance: Scalars['Boolean'];
};

export type PaymentEligibilityParams = {
  accountNumberList: Array<AccountNumberParams>;
};

export enum PaymentErrorMessage {
  Unknown = 'UNKNOWN',
  Invalidexpiration = 'INVALIDEXPIRATION',
  Invalidtransaction = 'INVALIDTRANSACTION',
  Pickup = 'PICKUP',
  Invalidamount = 'INVALIDAMOUNT',
  Noanswer = 'NOANSWER',
  Invalideffective = 'INVALIDEFFECTIVE',
  Invalidccnumber = 'INVALIDCCNUMBER',
  Issuerunavailable = 'ISSUERUNAVAILABLE',
  Processordecline = 'PROCESSORDECLINE',
  Notonfile = 'NOTONFILE',
  Callvoicecenter = 'CALLVOICECENTER',
  Defaultcall = 'DEFAULTCALL',
  Loststolen = 'LOSTSTOLEN',
  Cardisexpired = 'CARDISEXPIRED',
  Donothonor = 'DONOTHONOR',
  Duplicatepayment = 'DUPLICATEPAYMENT',
  Expirydatepast = 'EXPIRYDATEPAST',
  Connectiontimeout = 'CONNECTIONTIMEOUT',
  Greaterthanmaxamount = 'GREATERTHANMAXAMOUNT',
  Lessthanminamount = 'LESSTHANMINAMOUNT'
}

export enum PaymentErrorReason {
  Unknown = 'UNKNOWN',
  Accepted = 'ACCEPTED',
  Approved = 'APPROVED',
  Declined = 'DECLINED',
  Failed = 'FAILED',
  Scheduled = 'SCHEDULED'
}

export enum PaymentExtensionApeEligibilityCheck {
  Eligible = 'Eligible',
  NonResIneligibility = 'NonResIneligibility',
  EmployeeIneligibility = 'EmployeeIneligibility',
  MedicalCertificateIneligibility = 'MedicalCertificateIneligibility',
  IncativeAccountIneligibility = 'IncativeAccountIneligibility',
  AutopayEnrolledIneligibility = 'AutopayEnrolledIneligibility',
  EqualPayEnrolledIneligibility = 'EqualPayEnrolledIneligibility',
  PaymentExistsIneligibility = 'PaymentExistsIneligibility',
  UnsatisfiedDepositIneligibility = 'UnsatisfiedDepositIneligibility',
  AccountBalanceIneligibility = 'AccountBalanceIneligibility',
  CanceledPaymentIneligibility = 'CanceledPaymentIneligibility',
  DisconnectIneligibility = 'DisconnectIneligibility',
  UnknownIneligibility = 'UnknownIneligibility'
}

export type PaymentExtensionCurrentPlan = {
   __typename?: 'PaymentExtensionCurrentPlan';
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTimeCustom']>;
  planId?: Maybe<Scalars['String']>;
  enrolledInOption3?: Maybe<Scalars['Boolean']>;
};

export type PaymentExtensionCurrentPlanInfo = {
   __typename?: 'PaymentExtensionCurrentPlanInfo';
  amountDue?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTimeISO']>;
  planId?: Maybe<Scalars['String']>;
  enrolledInOption3?: Maybe<Scalars['Boolean']>;
};

export type PaymentExtensionDetailInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type PaymentExtensionDetailResponse = {
   __typename?: 'PaymentExtensionDetailResponse';
  eligibility?: Maybe<PaymentExtensionEligibility>;
  currentPlan?: Maybe<PaymentExtensionCurrentPlan>;
  paymentExtensionOptions?: Maybe<Array<Maybe<PaymentExtensionOption>>>;
};

export enum PaymentExtensionEligibility {
  Unknown = 'Unknown',
  Ineligible = 'Ineligible',
  EligibleWithCurrentPlan = 'EligibleWithCurrentPlan',
  EligibleWithoutCurrentPlan = 'EligibleWithoutCurrentPlan',
  EligibleWithPastDuePlan = 'EligibleWithPastDuePlan'
}

export type PaymentExtensionEligibilityResponse = {
   __typename?: 'PaymentExtensionEligibilityResponse';
  tpa?: Maybe<PaymentExtensionTpaEligibilityCheck>;
  ape?: Maybe<PaymentExtensionApeEligibilityCheck>;
};

export type PaymentExtensionEnrollInput = {
  serviceAddress?: Maybe<PaymentExtensionServiceAddress>;
  hasPastDueAmount?: Maybe<Scalars['Boolean']>;
  currentPlandId?: Maybe<Scalars['String']>;
  enrolledInOption3?: Maybe<Scalars['Boolean']>;
  selectedPaymentExtensionOption?: Maybe<SelectedPaymentExtensionOptions>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type PaymentExtensionInfo = {
   __typename?: 'PaymentExtensionInfo';
  eligibility?: Maybe<PaymentExtensionEligibilityResponse>;
  currentPlan?: Maybe<PaymentExtensionCurrentPlanInfo>;
  paymentExtensionOptions?: Maybe<Array<Maybe<PaymentExtensionOptionInfo>>>;
};

export type PaymentExtensionInfoResponse = {
   __typename?: 'PaymentExtensionInfoResponse';
  details?: Maybe<PaymentExtensionDetailResponse>;
  isEnrolled?: Maybe<Scalars['Boolean']>;
};

export type PaymentExtensionInput = {
  accountNumber?: Maybe<Scalars['String']>;
};

export type PaymentExtensionOption = {
   __typename?: 'PaymentExtensionOption';
  paymentExtensionOptionType?: Maybe<PaymentExtensionOptionType>;
  amount?: Maybe<Scalars['Float']>;
  planDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type PaymentExtensionOptionInfo = {
   __typename?: 'PaymentExtensionOptionInfo';
  paymentExtensionOptionType?: Maybe<PaymentExtensionOptionType>;
  amount?: Maybe<Scalars['Float']>;
  planDate?: Maybe<Scalars['DateTimeISO']>;
};

export enum PaymentExtensionOptionType {
  Option1 = 'Option1',
  Option2 = 'Option2',
  Option3 = 'Option3'
}

export type PaymentExtensionResponse = {
   __typename?: 'PaymentExtensionResponse';
  eligibility?: Maybe<PaymentExtensionEligibilityResponse>;
  currentPlan?: Maybe<PaymentExtensionCurrentPlan>;
  paymentExtensionOptions?: Maybe<Array<Maybe<PaymentExtensionOption>>>;
};

export type PaymentExtensionServiceAddress = {
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  houseType?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  county?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  pmbMailstop?: Maybe<Scalars['String']>;
  inCareOf?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
};

export type PaymentExtensionTimePaymentArrangementResponse = {
   __typename?: 'PaymentExtensionTimePaymentArrangementResponse';
  isEnrolled?: Maybe<Scalars['Boolean']>;
};

export enum PaymentExtensionTpaEligibilityCheck {
  Eligible = 'Eligible',
  Ineligible = 'Ineligible'
}

export type PaymentHistory = {
   __typename?: 'PaymentHistory';
  paymentDate: Scalars['DateTimeISO'];
  amountPaid: Scalars['Float'];
  paymentType: PaymentHistoryStatus;
};

export enum PaymentHistoryStatus {
  Payment = 'Payment',
  PendingPayment = 'PendingPayment',
  Agency = 'Agency'
}

export type PaymentInfo = {
   __typename?: 'PaymentInfo';
  amountDue?: Maybe<Scalars['String']>;
  isPendingDisconnect?: Maybe<Scalars['Boolean']>;
  isCardOnly?: Maybe<Scalars['Boolean']>;
  isNonBillableNoBalance?: Maybe<Scalars['Boolean']>;
  /** @deprecated Field no longer supported */
  autoPay?: Maybe<AutoPay>;
};

export type PaymentInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  tokenId: Scalars['String'];
  paymentAmount: Scalars['Float'];
  paymentDate: Scalars['DateTimeCustom'];
};

export type PaymentInputList = {
  payment: Array<PaymentInput>;
  tpaConfirmationNumber?: Maybe<Scalars['String']>;
};

export type PaymentmentExtensionEnrollResponse = {
   __typename?: 'PaymentmentExtensionEnrollResponse';
  confirmationNumber?: Maybe<Scalars['String']>;
};

export enum PaymentPlanType {
  Unknown = 'Unknown',
  RegularPay = 'RegularPay',
  EqualPay = 'EqualPay'
}

export type PaymentPlanTypeResponse = {
   __typename?: 'PaymentPlanTypeResponse';
  paymentPlanType?: Maybe<PaymentPlanType>;
};

export type PaymentProfile = {
   __typename?: 'PaymentProfile';
  token: Scalars['String'];
  type?: Maybe<Scalars['String']>;
  accountNumber?: Maybe<Scalars['String']>;
  creditCardExpiryDate?: Maybe<ExpiryDate>;
  cardHolderName?: Maybe<Scalars['String']>;
  default?: Maybe<Scalars['Boolean']>;
  usedForAutopay?: Maybe<Scalars['Boolean']>;
  hasDeferredPayments?: Maybe<Scalars['Boolean']>;
};

export type PaymentProfileList = {
   __typename?: 'PaymentProfileList';
  profile: Array<PaymentProfile>;
};

export enum PaymentType {
  Chq = 'CHQ',
  Sav = 'SAV',
  Cc = 'CC'
}

export enum PeakTimeEnrollmentStatus {
  Enrolled = 'Enrolled',
  Unenrolled = 'Unenrolled',
  YetToEnroll = 'YetToEnroll'
}

export type PeakTimeRebaseWidgetResponse = {
   __typename?: 'PeakTimeRebaseWidgetResponse';
  url?: Maybe<Scalars['String']>;
};

export type PeaktimeRebateCancelInput = {
  encryptedAccountNumber: Scalars['String'];
};

export type PeakTimeRebateContact = {
   __typename?: 'PeakTimeRebateContact';
  email?: Maybe<Scalars['String']>;
  mobilePhoneNumber?: Maybe<Scalars['String']>;
};

export type PeakTimeRebateGiveBack = {
   __typename?: 'PeakTimeRebateGiveBack';
  success?: Maybe<Scalars['Boolean']>;
};

export type PeakTimeRebateGiveBackDetails = {
   __typename?: 'PeakTimeRebateGiveBackDetails';
  isEligible?: Maybe<Scalars['Boolean']>;
  peakTimeSeason?: Maybe<PeakTimeSeason>;
  organizationName?: Maybe<Organization>;
};

export type PeakTimeRebateGiveBackDetailsInput = {
  encryptedAccountNumber: Scalars['String'];
  organizationName: Organization;
  peakTimeSeason: PeakTimeSeason;
};

export type PeakTimeRebateGiveBackInput = {
  accountNumber: Scalars['String'];
};

export type PeakTimeRebateParams = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type PeakTimeRebateProgramInfoResponse = {
   __typename?: 'PeakTimeRebateProgramInfoResponse';
  isEnrolled?: Maybe<Scalars['Boolean']>;
  isEligible?: Maybe<Scalars['Boolean']>;
  peakTimeRebateContact?: Maybe<PeakTimeRebateContact>;
};

export type PeakTimeRebateProgramStatus = {
   __typename?: 'PeakTimeRebateProgramStatus';
  hasActiveSA: Scalars['Boolean'];
  peakTimeRebateEnrollmentStatus: PeakTimeEnrollmentStatus;
  hasRebates: Scalars['Boolean'];
};

export type PeakTimeRebateProgramStatusResponse = {
  hasActiveSa?: Maybe<Scalars['Boolean']>;
  peakTimeRebateEnrollmentStatus?: Maybe<PeakTimeEnrollmentStatus>;
  hasRebates?: Maybe<Scalars['Boolean']>;
};

export type PeaktimeRebateSuccessResponse = {
   __typename?: 'PeaktimeRebateSuccessResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type PeaktimeRebateUpsertInput = {
  mobilePhoneNumber?: Maybe<Scalars['String']>;
  emailAddress?: Maybe<Scalars['String']>;
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export enum PeakTimeSeason {
  Winter = 'Winter',
  NextPeakEvent = 'NextPeakEvent',
  NotToDonate = 'NotToDonate'
}

export type PendingDisconnect = {
   __typename?: 'PendingDisconnect';
  isPendingDisconnect?: Maybe<Scalars['Boolean']>;
};

export type PendingDisconnectStatus = {
   __typename?: 'PendingDisconnectStatus';
  isPendingDisconnect: Scalars['Boolean'];
};

export type PendingFinancialTransactions = {
   __typename?: 'PendingFinancialTransactions';
  saId?: Maybe<Scalars['String']>;
  saStatusFlg?: Maybe<Scalars['String']>;
  saTypeCd?: Maybe<Scalars['String']>;
  curAmt?: Maybe<Scalars['Int']>;
  ftId?: Maybe<Scalars['String']>;
  cisDivision?: Maybe<Scalars['String']>;
  creDttm?: Maybe<Scalars['DateTimeCustom']>;
  glDivision?: Maybe<Scalars['String']>;
  ftTypeFlg?: Maybe<Scalars['String']>;
  billId?: Maybe<Scalars['String']>;
};

export type PendingFinancialTransactionsParams = {
  accountNumbers: Array<Maybe<Scalars['String']>>;
  saStatusFlag?: Maybe<Scalars['String']>;
};

export type Person = {
   __typename?: 'Person';
  encryptedPersonId?: Maybe<Scalars['String']>;
  personId?: Maybe<Scalars['String']>;
  personName?: Maybe<Scalars['String']>;
  mainCustomer?: Maybe<Scalars['Boolean']>;
  availableIdentityTypes: Array<IdentityType>;
};

export type PersonAccountRequest = {
  username?: Maybe<Scalars['String']>;
  accountNumberList?: Maybe<Array<Maybe<Scalars['String']>>>;
  businessPersonAccountList?: Maybe<Array<Maybe<BusinessPersonRequest>>>;
};

export enum PersonalIdentificationType {
  None = 'NONE',
  Ssn = 'SSN',
  Dl = 'DL',
  Ortrib = 'ORTRIB',
  Visa = 'VISA',
  Military = 'MILITARY',
  Pssprt = 'PSSPRT',
  Resalien = 'RESALIEN',
  Pincode = 'PINCODE',
  Dob = 'DOB',
  Ein = 'EIN',
  Itin = 'ITIN'
}

export type PersonalIdentificationTypeParams = {
  encryptedPersonId: Scalars['String'];
};

export type PersonContact = {
   __typename?: 'PersonContact';
  encryptedContactId: Scalars['String'];
  contactType: Scalars['String'];
  contactValue: Scalars['String'];
  isPrimary: Scalars['Boolean'];
};

export type PersonDetail = {
   __typename?: 'PersonDetail';
  encryptedPersonId: Scalars['String'];
  contactDetails?: Maybe<Array<Maybe<PersonContact>>>;
  profileUpdateEligibility?: Maybe<ProfileUpdateEligibility>;
};

export type PgeEnergyTrackerData = {
   __typename?: 'PgeEnergyTrackerData';
  showEnergyTracker?: Maybe<Scalars['Boolean']>;
  identifiers?: Maybe<Array<Maybe<Scalars['String']>>>;
  serviceProvider?: Maybe<EnergyTrackerServiceProvider>;
  billableAccountDetails?: Maybe<BillableAccountInfo>;
};

export type PgeEnergyTrackerDataParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export enum PhoneType {
  None = 'None',
  Home = 'Home',
  Mobile = 'Mobile',
  Work = 'Work',
  Alternate = 'Alternate',
  PrimaryNotificationPhone = 'PrimaryNotificationPhone'
}

export type PostAutoPayEnrollInput = {
  referenceId: Scalars['String'];
  tokenId: Scalars['String'];
  paymentType: PaymentType;
  cardType?: Maybe<CardType>;
  accountNumber: Scalars['String'];
  lastFourNumber: Scalars['String'];
  customerName: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  source: Source;
  amountDue?: Maybe<Scalars['Float']>;
};

export type PostBackPaymentConfirmation = {
   __typename?: 'PostBackPaymentConfirmation';
  success: Scalars['Boolean'];
  successs: Scalars['Boolean'];
};

export type PostBackPaymentConfirmationForIva = {
   __typename?: 'PostBackPaymentConfirmationForIVA';
  success: Scalars['Boolean'];
};

export type PostBackPaymentForIvaInput = {
  accountNumber: Scalars['String'];
  source: Scalars['String'];
  customerFirstName: Scalars['String'];
  customerLastName: Scalars['String'];
  referenceId: Scalars['String'];
  paymentType: Scalars['String'];
  cardType: Scalars['String'];
  last4DigitCardNumber: Scalars['String'];
  paymentDate: Scalars['LocalDate'];
  paymentAmount: Scalars['Float'];
  tokenId: Scalars['String'];
  personId: Scalars['String'];
};

export type PostBackPaymentInput = {
  accountNumber: Scalars['String'];
  source: Scalars['String'];
  customerFirstName: Scalars['String'];
  customerLastName: Scalars['String'];
  referenceId: Scalars['String'];
  paymentType: Scalars['String'];
  cardType: Scalars['String'];
  last4DigitCardNumber: Scalars['String'];
  paymentDate: Scalars['LocalDate'];
  paymentAmount: Scalars['Float'];
};

export type PotentialAddress = {
   __typename?: 'PotentialAddress';
  moniker?: Maybe<Scalars['String']>;
  address?: Maybe<Scalars['String']>;
  isFullAddress?: Maybe<Scalars['Boolean']>;
  isMultipleAddress?: Maybe<Scalars['Boolean']>;
  partialAddress?: Maybe<Scalars['String']>;
};

export type PowerPortfolio = {
   __typename?: 'PowerPortfolio';
  lastUpdateTime: Scalars['DateTime'];
  load?: Maybe<Scalars['Float']>;
  temp?: Maybe<Scalars['Float']>;
  supplyTypeDetails?: Maybe<Array<Maybe<SupplyTypeDetail>>>;
  supplyHistory?: Maybe<Array<Maybe<SupplyHistory>>>;
};


export type PowerPortfolioSupplyHistoryArgs = {
  supplyHistoryParams: SupplyHistoryParams;
};

export type PreferredDueDate = {
   __typename?: 'PreferredDueDate';
  preferredDueDate?: Maybe<Scalars['Int']>;
  status?: Maybe<PreferredDueDateStatus>;
  effectiveDate?: Maybe<Scalars['String']>;
};

export type PreferredDueDateAddRequest = {
  encryptedAccountNumber: Scalars['String'];
  preferredDueDate?: Maybe<Scalars['Int']>;
};

export type PreferredDueDateAddResponse = {
   __typename?: 'PreferredDueDateAddResponse';
  preferredDueDate?: Maybe<Scalars['Int']>;
  status?: Maybe<PreferredDueDateStatus>;
  effectiveDate?: Maybe<Scalars['String']>;
};

export type PreferredDueDateDetails = {
   __typename?: 'PreferredDueDateDetails';
  dueDate?: Maybe<PreferredDueDate>;
  eligibility?: Maybe<PreferredDueDateEligibility>;
};

export type PreferredDueDateEligibility = {
   __typename?: 'PreferredDueDateEligibility';
  reasonCode?: Maybe<Scalars['String']>;
  isEligible?: Maybe<Scalars['Boolean']>;
};

export type PreferredDueDateEligibilityResponse = {
   __typename?: 'PreferredDueDateEligibilityResponse';
  reasonCode?: Maybe<Scalars['String']>;
  isEligible?: Maybe<Scalars['Boolean']>;
};

export type PreferredDueDateInfoResponse = {
   __typename?: 'PreferredDueDateInfoResponse';
  preferredDueDate?: Maybe<Scalars['Int']>;
  status?: Maybe<PreferredDueDateStatus>;
  effectiveDate?: Maybe<Scalars['String']>;
};

export type PreferredDueDateRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type PreferredDueDateResponse = {
   __typename?: 'PreferredDueDateResponse';
  dueDateInfo?: Maybe<PreferredDueDateInfoResponse>;
  eligibility?: Maybe<PreferredDueDateEligibilityResponse>;
};

export enum PreferredDueDateStatus {
  Found = 'Found',
  NotFound = 'NotFound',
  Added = 'Added',
  NotAdded = 'NotAdded',
  ChangedToday = 'ChangedToday',
  NotChanged = 'NotChanged',
  Error = 'Error',
  NotEligible = 'NotEligible'
}

export type PremiseDetails = {
   __typename?: 'PremiseDetails';
  encryptedPremiseId?: Maybe<Scalars['String']>;
  streetName?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  houseNumber?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  zipCode?: Maybe<Scalars['String']>;
  servicePoints?: Maybe<Array<Maybe<ServicePoint>>>;
};


export type PremiseDetailsServicePointsArgs = {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
};

export type PremiseInfo = {
   __typename?: 'PremiseInfo';
  encryptedPremiseId: Scalars['String'];
  addressLine1: Scalars['String'];
  addressLine2?: Maybe<Scalars['String']>;
  houseType?: Maybe<Scalars['String']>;
  city: Scalars['String'];
  county?: Maybe<Scalars['String']>;
  state: Scalars['String'];
  postal: Scalars['String'];
  country?: Maybe<Scalars['String']>;
  servicePointDetails?: Maybe<Array<Maybe<ServicePointDetail>>>;
  saDetails?: Maybe<Array<Maybe<SaDetail>>>;
};

export type PremiseSummary = {
   __typename?: 'PremiseSummary';
  houseNumber?: Maybe<Scalars['String']>;
  streetName?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  zipCode?: Maybe<Scalars['String']>;
};

export enum PremiseType {
  None = 'None',
  Own = 'Own',
  Rent = 'Rent'
}

export type PrimaryEmailDetails = {
   __typename?: 'PrimaryEmailDetails';
  isPrimaryEmailExists: Scalars['Boolean'];
};

export type ProfilePreferencesRequest = {
  language?: Maybe<Scalars['String']>;
};

export type ProfileResponse = {
   __typename?: 'ProfileResponse';
  status?: Maybe<Scalars['String']>;
  token?: Maybe<Scalars['String']>;
  errors?: Maybe<DeleteProfileError>;
};

export type ProfileUpdateEligibility = {
   __typename?: 'ProfileUpdateEligibility';
  isEligible: Scalars['Boolean'];
};

export type PromotionalOpportunity = {
   __typename?: 'PromotionalOpportunity';
  isUsingCompanyName?: Maybe<Scalars['Boolean']>;
  companyName?: Maybe<Scalars['String']>;
};

export type PromotionalOpportunityInput = {
  isUsingCompanyName?: Maybe<Scalars['Boolean']>;
  companyName?: Maybe<Scalars['String']>;
};

export type QasSearchResult = {
   __typename?: 'QasSearchResult';
  verifyLevel?: Maybe<VerifyLevel>;
  isAddressTruncated?: Maybe<Scalars['Boolean']>;
  suggestedAddress?: Maybe<SuggestedAddress>;
  potentialAddresses?: Maybe<Array<Maybe<PotentialAddress>>>;
};

export type Query = {
   __typename?: 'Query';
  root?: Maybe<Scalars['String']>;
  getAccountGroups?: Maybe<Array<Maybe<Group>>>;
  getCustomGroups?: Maybe<Array<Maybe<Group>>>;
  getAccountInfo?: Maybe<AccountCustomer>;
  getAccountDetails?: Maybe<Array<Maybe<AccountDetail>>>;
  accountExists: Scalars['Boolean'];
  account?: Maybe<Account>;
  accounts?: Maybe<Array<Maybe<AccountSummary>>>;
  getAccountDetailList?: Maybe<AccountDetailList>;
  getEnergyTrackerData?: Maybe<EnergyTrackerData>;
  getEnergyTrackerUserInfo?: Maybe<EnergyTrackerUserInfo>;
  getServiceAgreementNicknames?: Maybe<Array<Maybe<ServiceAgreementNickname>>>;
  getPgeEnergyTrackerData?: Maybe<PgeEnergyTrackerData>;
  getAlertDetails?: Maybe<AlertDetails>;
  getBillingAndPaymentHistoryDetails?: Maybe<BillingAndPaymentHistoryDetailsResponse>;
  downloadBillingAndPaymentHistoryDetails?: Maybe<BillingAndPaymentHistoryDetailsDownloadResponse>;
  getBillInfo?: Maybe<BillInfo>;
  isPaperlessBillEnrolled?: Maybe<IsPaperlessBillEnrolledResponse>;
  getViewBillInfo?: Maybe<ViewBillInfoResponse>;
  viewBillDownloadPdf?: Maybe<ViewBillDownloadPdfResponse>;
  getViewPaymentHistoryChargeSummary?: Maybe<ViewPaymentHistoryResponse>;
  getViewPaymentHistoryDetail?: Maybe<ViewPaymentHistoryDetailResponse>;
  getViewPaymentHistoryUsage?: Maybe<ViewPaymentHistoryBillingUsageResponse>;
  getViewPaymentHistoryDetailFromCloud?: Maybe<ViewPaymentHistoryDetailResponse>;
  validateAccountExists?: Maybe<ValidateAccountExistsResponse>;
  isPersonalIdentificationValid?: Maybe<Scalars['Boolean']>;
  isCustomerServiceDisconnected?: Maybe<CustomerServiceDisconnectedResponse>;
  getCoCustomerPersonDetails?: Maybe<AdditionalInfo>;
  getMainCustomerPersonDetails?: Maybe<AdditionalInfo>;
  getCoCustomersForAccount?: Maybe<Array<Maybe<CoCustomerInfo>>>;
  isEmailExist?: Maybe<IsEmailExistResponse>;
  getMailingAddressForAccount?: Maybe<Address>;
  getPnpPhone?: Maybe<Scalars['String']>;
  getPersonsForAccount?: Maybe<Array<Maybe<AccountPerson>>>;
  getUserAccountPremiseInfo?: Maybe<UserAccountPremiseInfoResponse>;
  getPersonPrimaryIdentificationType: PersonalIdentificationType;
  qasSearchAddress?: Maybe<QasSearchResult>;
  qasSearchAddressById?: Maybe<QasSearchResult>;
  qasRefineAddressSearch?: Maybe<QasSearchResult>;
  searchServiceAddress?: Maybe<SearchServiceAddressResponse>;
  verifyIdentity: Array<VerifiedIdentity>;
  moveServiceEligibility?: Maybe<MoveServiceEligibilityResponse>;
  moveToServiceAddressEligibility?: Maybe<MoveToServiceAddressEligibilityResponse>;
  getAllOutageEvents?: Maybe<Scalars['JSON']>;
  getOutageEventDetails?: Maybe<OutageEventDetails>;
  getOutagesByCounty?: Maybe<Array<Scalars['JSON']>>;
  getOutageDetailsByZipCode?: Maybe<OutageDetailsByZipCode>;
  getOutageLookup: OutageLookupResponse;
  getOutageDetail?: Maybe<OutageDetail>;
  getOutageDetailByGuest?: Maybe<GuestOutageDetail>;
  getOutages?: Maybe<Array<Maybe<Outage>>>;
  getOutageList?: Maybe<OutagesList>;
  getDownloadAutoPaymentPdf?: Maybe<AutoPaymentPdfResponse>;
  getCIFDetails: CifDetails;
  getGuestPaymentInfo: GuestPaymentDetails;
  getDownloadGuestPaymentPdf?: Maybe<GuestPaymentPdf>;
  getGroupAmountDetails?: Maybe<OnecheckPaymentInfoResponse>;
  getDownloadOnecheckRemittanceFormPdf?: Maybe<OnecheckPaymentDownloadRemittanceFormResponse>;
  getDownloadOnetimePaymentPdf?: Maybe<OnetimePaymentPdfResponse>;
  getPaymentExtensionInfo?: Maybe<PaymentExtensionInfoResponse>;
  getPreferredDueDateInfo?: Maybe<PreferredDueDateResponse>;
  getQuickPaymentDetails: QuickPayDetails;
  getIFrameInfo?: Maybe<IFrameInfoResponse>;
  listProfiles?: Maybe<ListProfilesResponse>;
  billNotFoundPdfCsLegacy?: Maybe<BillNotFoundDownloadPdf>;
  billNotFoundPdfBizLegacy?: Maybe<BillNotFoundDownloadPdf>;
  getPeakTimeRebateStatus?: Maybe<PeakTimeRebateProgramStatus>;
  getPeakTimeRebateProgramInfo?: Maybe<PeakTimeRebateProgramInfoResponse>;
  getPeakTimeRebateWidgetUrl?: Maybe<PeakTimeRebaseWidgetResponse>;
  person?: Maybe<PersonDetail>;
  renewablesAccountEligibility?: Maybe<RenewablesAccountEligibleResponse>;
  getRenewables?: Maybe<RenewablePower>;
  getRenewableEnrollmentStatus?: Maybe<RenewableEnrollment>;
  getPowerPortfolio?: Maybe<PowerPortfolio>;
  validateCommercialAccountNumber?: Maybe<CommercialAccountValidation>;
  startServiceAddressEligibility?: Maybe<ServiceAddressEligibility>;
  startServiceEligibility?: Maybe<StartServiceEligibilityResponse>;
  stopServiceEligibility?: Maybe<Scalars['Boolean']>;
  hasPerson?: Maybe<HasPersonIdResponse>;
  sameAsOldPassword?: Maybe<SameAsOldPasswordResponse>;
};


export type QueryGetAccountGroupsArgs = {
  params?: Maybe<AccountGroupParams>;
};


export type QueryGetAccountDetailsArgs = {
  params: AccountDetailParams;
};


export type QueryAccountExistsArgs = {
  accountId: Scalars['String'];
};


export type QueryAccountArgs = {
  accountId: Scalars['String'];
};


export type QueryAccountsArgs = {
  phoneNumber: Scalars['String'];
};


export type QueryGetAccountDetailListArgs = {
  params: AccountDetailListParams;
};


export type QueryGetEnergyTrackerDataArgs = {
  params?: Maybe<EnergyTrackerDataParams>;
};


export type QueryGetEnergyTrackerUserInfoArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QueryGetServiceAgreementNicknamesArgs = {
  params: ServiceAgreementNicknamesParams;
};


export type QueryGetPgeEnergyTrackerDataArgs = {
  params?: Maybe<PgeEnergyTrackerDataParams>;
};


export type QueryGetAlertDetailsArgs = {
  params?: Maybe<GetAlertDetailsRequest>;
};


export type QueryGetBillingAndPaymentHistoryDetailsArgs = {
  payload: BillingAndPaymentHistoryDetailsInput;
};


export type QueryDownloadBillingAndPaymentHistoryDetailsArgs = {
  payload: BillingAndPaymentHistoryDetailsInput;
};


export type QueryGetBillInfoArgs = {
  params?: Maybe<BillInfoParams>;
};


export type QueryIsPaperlessBillEnrolledArgs = {
  payload?: Maybe<IsPaperlessBillEnrolledRequest>;
};


export type QueryGetViewBillInfoArgs = {
  payload: ViewBillDetailsRequest;
};


export type QueryViewBillDownloadPdfArgs = {
  payload?: Maybe<ViewBillDownloadPdfRequest>;
};


export type QueryGetViewPaymentHistoryChargeSummaryArgs = {
  payload?: Maybe<ViewPaymentHistoryInput>;
};


export type QueryGetViewPaymentHistoryDetailArgs = {
  payload?: Maybe<ViewPaymentHistoryDetailInput>;
};


export type QueryGetViewPaymentHistoryUsageArgs = {
  payload?: Maybe<ViewPaymentHistoryBillingUsageInput>;
};


export type QueryGetViewPaymentHistoryDetailFromCloudArgs = {
  payload?: Maybe<ViewPaymentHistoryDetailInput>;
};


export type QueryValidateAccountExistsArgs = {
  payload?: Maybe<ValidateAccountExistsRequest>;
};


export type QueryIsPersonalIdentificationValidArgs = {
  params?: Maybe<IsPersonalIdValidParams>;
};


export type QueryIsCustomerServiceDisconnectedArgs = {
  payload?: Maybe<CustomerServiceDisconnectedParams>;
};


export type QueryGetCoCustomerPersonDetailsArgs = {
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export type QueryGetMainCustomerPersonDetailsArgs = {
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export type QueryGetCoCustomersForAccountArgs = {
  params: CoCustomerForAccountRequest;
};


export type QueryIsEmailExistArgs = {
  email?: Maybe<Scalars['String']>;
};


export type QueryGetMailingAddressForAccountArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QueryGetPnpPhoneArgs = {
  encryptedPersonId?: Maybe<Scalars['String']>;
};


export type QueryGetPersonsForAccountArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QueryGetUserAccountPremiseInfoArgs = {
  payload: UserAccountPremiseInfo;
};


export type QueryGetPersonPrimaryIdentificationTypeArgs = {
  params: PersonalIdentificationTypeParams;
};


export type QueryQasSearchAddressArgs = {
  params?: Maybe<QuickAddressSearchParams>;
};


export type QueryQasSearchAddressByIdArgs = {
  params?: Maybe<QuickAddressSearchParams>;
};


export type QueryQasRefineAddressSearchArgs = {
  params?: Maybe<QuickAddressSearchParams>;
};


export type QuerySearchServiceAddressArgs = {
  payload?: Maybe<SearchServiceAddressRequest>;
};


export type QueryVerifyIdentityArgs = {
  input: Array<VerifyIdentityInput>;
};


export type QueryMoveServiceEligibilityArgs = {
  payload?: Maybe<MoveServiceEligibilityRequest>;
};


export type QueryMoveToServiceAddressEligibilityArgs = {
  payload?: Maybe<MoveToServiceAddressEligibilityRequest>;
};


export type QueryGetOutageEventDetailsArgs = {
  params: OutageEventParams;
};


export type QueryGetOutagesByCountyArgs = {
  params?: Maybe<OutageByCountyParams>;
};


export type QueryGetOutageDetailsByZipCodeArgs = {
  params: OutageDetailsByZipCodeParams;
};


export type QueryGetOutageLookupArgs = {
  params: OutageLookupParams;
};


export type QueryGetOutageDetailArgs = {
  params?: Maybe<OutageDetailParams>;
};


export type QueryGetOutageDetailByGuestArgs = {
  payload: OutageDetailByGuestParams;
};


export type QueryGetDownloadAutoPaymentPdfArgs = {
  payload?: Maybe<AutoPaymentDownloadPdfRequest>;
};


export type QueryGetCifDetailsArgs = {
  payload: CifDetailsParams;
};


export type QueryGetGuestPaymentInfoArgs = {
  params: GuestPaymentAccountInput;
};


export type QueryGetDownloadGuestPaymentPdfArgs = {
  payload?: Maybe<GuestPaymentDownloadPdfInput>;
};


export type QueryGetGroupAmountDetailsArgs = {
  payload?: Maybe<OnecheckPaymentGroupInfoRequest>;
};


export type QueryGetDownloadOnecheckRemittanceFormPdfArgs = {
  payload?: Maybe<OnecheckPaymentSubmitRequest>;
};


export type QueryGetDownloadOnetimePaymentPdfArgs = {
  payload?: Maybe<OnetimePaymentDownloadPdfRequest>;
};


export type QueryGetPaymentExtensionInfoArgs = {
  payload?: Maybe<PaymentExtensionDetailInput>;
};


export type QueryGetPreferredDueDateInfoArgs = {
  payload?: Maybe<PreferredDueDateRequest>;
};


export type QueryGetQuickPaymentDetailsArgs = {
  encryptedAccountInfo: Scalars['String'];
};


export type QueryGetIFrameInfoArgs = {
  payload?: Maybe<IFrameInfoRequest>;
};


export type QueryListProfilesArgs = {
  payload?: Maybe<ListProfilesRequest>;
};


export type QueryGetPeakTimeRebateStatusArgs = {
  params: PeakTimeRebateParams;
};


export type QueryGetPeakTimeRebateProgramInfoArgs = {
  payload: PeakTimeRebateParams;
};


export type QueryGetPeakTimeRebateWidgetUrlArgs = {
  payload: PeakTimeRebateParams;
};


export type QueryPersonArgs = {
  encryptedPersonId: Scalars['String'];
};


export type QueryRenewablesAccountEligibilityArgs = {
  payload?: Maybe<RenewablesAccountEligibleRequest>;
};


export type QueryGetRenewablesArgs = {
  payload?: Maybe<RenewableInput>;
};


export type QueryGetRenewableEnrollmentStatusArgs = {
  payload?: Maybe<RenewableEnrollmentStatusParams>;
};


export type QueryValidateCommercialAccountNumberArgs = {
  accountNumber: Scalars['String'];
};


export type QueryStartServiceAddressEligibilityArgs = {
  payload?: Maybe<StartServiceAddressEligibilityRequest>;
};


export type QueryStartServiceEligibilityArgs = {
  payload?: Maybe<StartServiceEligibilityRequest>;
};


export type QueryStopServiceEligibilityArgs = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};


export type QuerySameAsOldPasswordArgs = {
  payload?: Maybe<SameAsOldPasswordRequest>;
};

export type QuickAddressSearchParams = {
  state?: Maybe<Scalars['String']>;
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  fullAddress?: Maybe<Scalars['String']>;
  isMailingAddress?: Maybe<Scalars['Boolean']>;
  moniker?: Maybe<Scalars['String']>;
};

export type QuickPayConfirmation = {
   __typename?: 'QuickPayConfirmation';
  lastFourdDigitAccount: Scalars['String'];
  confirmationId?: Maybe<Scalars['String']>;
  isSuccess: Scalars['Boolean'];
  paymentAmount?: Maybe<Scalars['Float']>;
  paymentMethod?: Maybe<Scalars['String']>;
  cardType?: Maybe<Scalars['String']>;
  lastFourDigitsCardNumber?: Maybe<Scalars['String']>;
  errorReason?: Maybe<PaymentErrorReason>;
  errorMessage?: Maybe<PaymentErrorMessage>;
};

export type QuickPayDetails = {
   __typename?: 'QuickPayDetails';
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  paymentEligibility: PaymentEligibility;
  currentCharges: CurrentCharges;
  autoPay: AutoPayInfo;
  serviceAddress?: Maybe<Scalars['String']>;
  paymentDate: Scalars['DateTimeCustom'];
  emailId: Scalars['String'];
  paymentProfile: PaymentProfile;
};

export type QuickPayInput = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  tokenId: Scalars['String'];
  emailId: Scalars['String'];
  paymentAmount: Scalars['Float'];
};

export type RegistrationResponse = {
   __typename?: 'RegistrationResponse';
  success?: Maybe<Scalars['Boolean']>;
  signinToken?: Maybe<Scalars['String']>;
  uid?: Maybe<Scalars['String']>;
};

export enum RelationshipType {
  Main = 'Main',
  CoApplicant = 'CoApplicant',
  Responsible = 'Responsible'
}

export enum RenewableEligibility {
  Unknown = 'Unknown',
  Eligible = 'Eligible',
  IneligibleNoActiveServiceAgreement = 'IneligibleNoActiveServiceAgreement',
  IneligibleHasDisconnect = 'IneligibleHasDisconnect',
  IneligibleHasMultipleServiceAgreement = 'IneligibleHasMultipleServiceAgreement'
}

export type RenewableEnrollment = {
   __typename?: 'RenewableEnrollment';
  renewableEnrollmentStatus?: Maybe<RenewableEnrollmentStatus>;
};

export enum RenewableEnrollmentStatus {
  Unknown = 'Unknown',
  Enrolled = 'Enrolled',
  NotEnrolled = 'NotEnrolled',
  Canceled = 'Canceled',
  Changed = 'Changed'
}

export type RenewableEnrollmentStatusParams = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type RenewableInput = {
  encryptedServiceAgreementId: Scalars['String'];
};

export type RenewableOption = {
   __typename?: 'RenewableOption';
  greenSourceEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindEnrolled?: Maybe<Scalars['Boolean']>;
  greenFutureSolarEnrolled?: Maybe<Scalars['Boolean']>;
  supportingHabitatEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindChargeBlock?: Maybe<Scalars['Int']>;
  greenFutureSolarBlock?: Maybe<Scalars['Int']>;
  cleanWindStartDate?: Maybe<Scalars['String']>;
  cleanWindEndDate?: Maybe<Scalars['String']>;
  supportHabitatStartDate?: Maybe<Scalars['String']>;
  supportHabitatEndDate?: Maybe<Scalars['String']>;
  greenSourceStartDate?: Maybe<Scalars['String']>;
  greenSourceEndDate?: Maybe<Scalars['String']>;
  cleanWindQuantityEffectiveDate?: Maybe<Scalars['String']>;
};

export type RenewableOptionInput = {
  greenSourceEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindEnrolled?: Maybe<Scalars['Boolean']>;
  greenFutureSolarEnrolled?: Maybe<Scalars['Boolean']>;
  supportingHabitatEnrolled?: Maybe<Scalars['Boolean']>;
  cleanWindChargeBlock?: Maybe<Scalars['Int']>;
  greenFutureSolarBlock?: Maybe<Scalars['Int']>;
  totalGreenFutureSolarBlock?: Maybe<Scalars['Float']>;
  totalCleanWindChargeBlock?: Maybe<Scalars['Float']>;
  cleanWindStartDate?: Maybe<Scalars['String']>;
  cleanWindEndDate?: Maybe<Scalars['String']>;
  supportHabitatStartDate?: Maybe<Scalars['String']>;
  supportHabitatEndDate?: Maybe<Scalars['String']>;
  greenSourceStartDate?: Maybe<Scalars['String']>;
  greenSourceEndDate?: Maybe<Scalars['String']>;
  cleanWindQuantityEffectiveDate?: Maybe<Scalars['String']>;
};

export type RenewablePower = {
   __typename?: 'RenewablePower';
  renewableOption?: Maybe<RenewableOption>;
  eligibility?: Maybe<RenewableEligibility>;
  isSmallBusiness?: Maybe<Scalars['Boolean']>;
  isIndustrial?: Maybe<Scalars['Boolean']>;
  howDidYouHearOption?: Maybe<Scalars['String']>;
  keyWord?: Maybe<Scalars['String']>;
  promotionalOpportunity?: Maybe<PromotionalOpportunity>;
  originalRenewableOption?: Maybe<RenewableOption>;
  username?: Maybe<Scalars['String']>;
  contactName?: Maybe<Scalars['String']>;
  primaryPhoneNumber?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};

export type RenewablePowerInput = {
  username?: Maybe<Scalars['String']>;
  renewableOption?: Maybe<RenewableOptionInput>;
  originalRenewableOption?: Maybe<RenewableOptionInput>;
  howDidYouHearOption?: Maybe<Scalars['String']>;
  keyWord?: Maybe<Scalars['String']>;
  promotionalOpportunity?: Maybe<PromotionalOpportunityInput>;
  isSmallBusiness?: Maybe<Scalars['Boolean']>;
  isIndustrial?: Maybe<Scalars['Boolean']>;
  eligibility?: Maybe<RenewableEligibility>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  contactName?: Maybe<Scalars['String']>;
  primaryPhoneNumber?: Maybe<Scalars['String']>;
  encryptedPersonId: Scalars['String'];
};

export type RenewablesAccountEligibleRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  state: Scalars['String'];
  postal: Scalars['String'];
};

export type RenewablesAccountEligibleResponse = {
   __typename?: 'RenewablesAccountEligibleResponse';
  eligibility?: Maybe<RenewableEligibility>;
  isIndustrial?: Maybe<Scalars['Boolean']>;
  isSmallBusiness?: Maybe<Scalars['Boolean']>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};

export type RenewablesEnrollmentToDoRequest = {
  contactName?: Maybe<Scalars['String']>;
  primaryPhoneNumber?: Maybe<Scalars['String']>;
  username?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedServiceAgreementId?: Maybe<Scalars['String']>;
};

export type ReportOutageByGuestRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPremiseId: Scalars['String'];
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested: Scalars['Boolean'];
  previouslyEnrolledInCallbackPhone: Scalars['Boolean'];
};

export type ReportOutageBySpListGuestRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedServicePointList: Array<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  source: ReportOutageSource;
};

export type ReportOutageBySpListRequest = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  encryptedPremiseId: Scalars['String'];
  encryptedServicePointList: Array<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  outageRestoredAlertList?: Maybe<Array<OutageRestoredAlert>>;
  source: ReportOutageSource;
};

export type ReportOutageRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  encryptedPremiseId: Scalars['String'];
  registeredPhone?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  callbackRequested: Scalars['Boolean'];
  restorePowerAlert?: Maybe<ReportOutageRequest_Alert>;
  previouslyEnrolledInCallbackPhone: Scalars['Boolean'];
  previouslyEnrolledInAlerts: Scalars['Boolean'];
};

export type ReportOutageRequest_Alert = {
  isEmail: Scalars['Boolean'];
  encryptedEmailNotificationId?: Maybe<Scalars['String']>;
  encryptedEmailContactId?: Maybe<Scalars['String']>;
  isText: Scalars['Boolean'];
  encryptedTextNotificationId?: Maybe<Scalars['String']>;
  encryptedTextContactId?: Maybe<Scalars['String']>;
};

export type ReportOutageResponse = {
   __typename?: 'ReportOutageResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export enum ReportOutageSource {
  Web = 'WEB',
  Ivr = 'IVR',
  Mob = 'MOB'
}

export type ResidentialRegistrationRequest = {
  AccountNumber?: Maybe<Scalars['String']>;
  EmailAddress: Scalars['String'];
  Password: Scalars['String'];
  PhoneNumber?: Maybe<Scalars['String']>;
  Last4DigitsOfSSN?: Maybe<Scalars['String']>;
  Last4DigitsOfDriversLicenseOrStateId?: Maybe<Scalars['String']>;
  PinCode?: Maybe<Scalars['String']>;
  IsPaperlessBillingSelected?: Maybe<Scalars['Boolean']>;
};

export type ResponseBase = {
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
};

export type RetainService = {
   __typename?: 'RetainService';
  minimumToRetainService?: Maybe<Scalars['Float']>;
};

export type SaDetail = {
   __typename?: 'SADetail';
  encryptedSAId: Scalars['String'];
  saStatus: SaStatus;
  startDate: Scalars['DateTime'];
  endDate?: Maybe<Scalars['DateTime']>;
  nickName?: Maybe<Scalars['String']>;
};

export type SameAsOldPasswordRequest = {
  password?: Maybe<Scalars['String']>;
};

export type SameAsOldPasswordResponse = {
   __typename?: 'SameAsOldPasswordResponse';
  sameAsOldPassword?: Maybe<Scalars['Boolean']>;
};

export enum SartServicePremiseTypeParam {
  None = 'None',
  Own = 'Own',
  Rent = 'Rent'
}

export enum SaStatus {
  Active = 'ACTIVE',
  Pendingstart = 'PENDINGSTART',
  Pendingstop = 'PENDINGSTOP',
  Stopped = 'STOPPED',
  Reactivated = 'REACTIVATED',
  Closed = 'CLOSED',
  Cancelled = 'CANCELLED',
  Incomplete = 'INCOMPLETE',
  Multiple = 'MULTIPLE'
}

export enum SaType {
  Rartpa = 'RARTPA'
}

export type SearchServiceAddress = {
   __typename?: 'SearchServiceAddress';
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  postal: Scalars['String'];
  premiseIds?: Maybe<Array<Scalars['String']>>;
};

export type SearchServiceAddressRequest = {
  match?: Maybe<Scalars['String']>;
};

export type SearchServiceAddressResponse = {
   __typename?: 'SearchServiceAddressResponse';
  addresses?: Maybe<Array<Maybe<SearchServiceAddress>>>;
};

export type SelectedPaymentExtensionOptions = {
  paymentExtensionOptionType?: Maybe<PaymentExtensionOptionType>;
  amount?: Maybe<Scalars['Float']>;
  planDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type SendActivationCodeRequest = {
  phoneNumber: Scalars['String'];
};

export type SendActivationCodeResponse = {
   __typename?: 'SendActivationCodeResponse';
  encryptedActivationCode: Scalars['String'];
};

export type SendSmsTextInput = {
  accountId?: Maybe<Scalars['String']>;
  personId?: Maybe<Scalars['String']>;
  notificationType: Scalars['String'];
  toNumber: Scalars['PhoneNumber'];
  messageBody: Scalars['String'];
  source: Scalars['String'];
  fromSid: FromSidEnum;
};

export type ServiceAddress = {
   __typename?: 'ServiceAddress';
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  houseType?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  county?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  pMBMailstop?: Maybe<Scalars['String']>;
  fullServiceAddress?: Maybe<Scalars['String']>;
  fullStreetAddress1?: Maybe<Scalars['String']>;
  fullStreetAddress2?: Maybe<Scalars['String']>;
  inCareOf?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
};

export type ServiceAddressEligibility = {
   __typename?: 'ServiceAddressEligibility';
  isEligible?: Maybe<Scalars['Boolean']>;
  isAddressExactMatch?: Maybe<Scalars['Boolean']>;
  isPeakTimeRebateEligible?: Maybe<Scalars['Boolean']>;
  premiseId?: Maybe<Scalars['String']>;
  serviceAddressEligibilityType?: Maybe<StartServiceAddressEligibilityType>;
};

export enum ServiceAddressEligibilityType {
  None = 'None',
  AddressNotFound = 'AddressNotFound',
  CommercialBuilding = 'CommercialBuilding'
}

export type ServiceAddressResponse = {
  addressLine1?: Maybe<Scalars['String']>;
  addressLine2?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  county?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
};

export type ServiceAgreementNickname = {
   __typename?: 'ServiceAgreementNickname';
  encryptedId: Scalars['String'];
  streetAddress: Scalars['String'];
  cityStateZip: Scalars['String'];
  nickname?: Maybe<Scalars['String']>;
};

export type ServiceAgreementNicknameInput = {
  encryptedId: Scalars['String'];
  streetAddress: Scalars['String'];
  cityStateZip: Scalars['String'];
  nickname?: Maybe<Scalars['String']>;
};

export type ServiceAgreementNicknameResponse = {
   __typename?: 'ServiceAgreementNicknameResponse';
  serviceAgreements?: Maybe<Array<Maybe<ServiceAgreementNickname>>>;
};

export type ServiceAgreementNicknamesParams = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export enum ServiceEligibilityReason {
  Eligible = 'ELIGIBLE',
  NeedsManualStopIneligible = 'NEEDS_MANUAL_STOP_INELIGIBLE',
  NonElectricIneligible = 'NON_ELECTRIC_INELIGIBLE',
  ProtectedAddressIneligible = 'PROTECTED_ADDRESS_INELIGIBLE',
  ServiceIntegrationIneligible = 'SERVICE_INTEGRATION_INELIGIBLE'
}

export type ServicePoint = {
   __typename?: 'ServicePoint';
  servicePointID?: Maybe<Scalars['String']>;
  encryptedServicePointID?: Maybe<Scalars['String']>;
  outage?: Maybe<OutageInfo>;
};

export type ServicePointDetail = {
   __typename?: 'ServicePointDetail';
  encryptedServicePointId: Scalars['String'];
  lat?: Maybe<Scalars['Latitude']>;
  long?: Maybe<Scalars['Longitude']>;
  outageInfo?: Maybe<ServicePointOutageDetails>;
};

export type ServicePointOutageDetails = {
   __typename?: 'ServicePointOutageDetails';
  encryptedServicePointId?: Maybe<Scalars['String']>;
  totalReports?: Maybe<Scalars['Int']>;
  crewDispatchStatus?: Maybe<Scalars['String']>;
  callbackPhone?: Maybe<Scalars['String']>;
  estimatedTimeOn?: Maybe<Scalars['DateTime']>;
  hasKnownOutage?: Maybe<Scalars['Boolean']>;
  estimatedTimeOut?: Maybe<Scalars['DateTime']>;
  isDueToStorm?: Maybe<Scalars['Boolean']>;
  callbackRequested?: Maybe<Scalars['Boolean']>;
  registeredPhone?: Maybe<Scalars['String']>;
  customersAffected?: Maybe<Scalars['Int']>;
  cause?: Maybe<Scalars['String']>;
  isReported?: Maybe<Scalars['Boolean']>;
};

export type ServicesEligibility = {
   __typename?: 'ServicesEligibility';
  moveService?: Maybe<ServiceEligibilityReason>;
  stopService?: Maybe<ServiceEligibilityReason>;
  startService?: Maybe<ServiceEligibilityReason>;
};

export type SortParams = {
  sort: AccountSort;
  direction: Direction;
};

export enum Source {
  Iva = 'IVA'
}

export type StartServiceAddressEligibilityRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  serviceAddress: StartServiceAddressParam;
  selectedPremiseType: SartServicePremiseTypeParam;
};

export enum StartServiceAddressEligibilityType {
  None = 'None',
  AddressNotFound = 'AddressNotFound',
  CommercialBuilding = 'CommercialBuilding'
}

export type StartServiceAddressParam = {
  addressLine1: Scalars['String'];
  city: Scalars['String'];
  state: Scalars['String'];
  postal: Scalars['String'];
};

export type StartServiceEligibilityRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type StartServiceEligibilityResponse = {
   __typename?: 'StartServiceEligibilityResponse';
  isEligible?: Maybe<Scalars['Boolean']>;
};

export type StartServiceIneligibilityLogRequest = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type StartServiceRequest = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  channel?: Maybe<Scalars['String']>;
  coCustomerInformation?: Maybe<CustomerInfoInput>;
  currentServiceAddress?: Maybe<AddressInput>;
  customerInformation?: Maybe<CustomerInfoInput>;
  eligibleForGreenResource?: Maybe<Scalars['Boolean']>;
  heatSourceType?: Maybe<Scalars['String']>;
  isCoCustomerSelected?: Maybe<Scalars['Boolean']>;
  isCurrentlyPaperlessBilling?: Maybe<Scalars['Boolean']>;
  isElectricVehicleSelected?: Maybe<Scalars['Boolean']>;
  isPaperlessBillingSelected?: Maybe<Scalars['Boolean']>;
  peakTimeRebateEmail?: Maybe<Scalars['String']>;
  peakTimeRebateMobilePhone?: Maybe<Scalars['String']>;
  isRenewablesSelected?: Maybe<Scalars['Boolean']>;
  isSmartThermostatSelected?: Maybe<Scalars['Boolean']>;
  isUserLoggedIn?: Maybe<Scalars['Boolean']>;
  livesAtPremise?: Maybe<Scalars['Boolean']>;
  meterAccessInfo?: Maybe<MeterAccessInput>;
  selectedPremiseType?: Maybe<PremiseType>;
  serviceAddress?: Maybe<AddressInput>;
  startDate?: Maybe<Scalars['String']>;
  submitDate?: Maybe<Scalars['String']>;
  waterHeaterType?: Maybe<Scalars['String']>;
  preferredDueDateSelected?: Maybe<Scalars['Int']>;
  isAddressExactMatch?: Maybe<Scalars['Boolean']>;
  premiseId?: Maybe<Scalars['String']>;
};

export type StartServiceResponse = {
   __typename?: 'StartServiceResponse';
  isStartServiceSuccessful?: Maybe<Scalars['Boolean']>;
  isPreferredDueDateSuccessful?: Maybe<Scalars['Boolean']>;
};

export type State = {
   __typename?: 'State';
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export enum StateIdType {
  DriverLicense = 'DriverLicense',
  StateIdCard = 'StateIDCard'
}

export type StateInfo = {
   __typename?: 'StateInfo';
  stateIdentificationType?: Maybe<StateIdType>;
  stateIDState?: Maybe<State>;
  stateIDNumber?: Maybe<Scalars['String']>;
};

export type StateInfoInput = {
  stateIdentificationType?: Maybe<StateIdType>;
  stateIDState?: Maybe<StateInput>;
  stateIDNumber?: Maybe<Scalars['String']>;
};

export type StateInput = {
  name?: Maybe<Scalars['String']>;
  code?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
};

export type StopServiceAddressInput = {
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  qasVerified?: Maybe<Scalars['Boolean']>;
  inCareOf?: Maybe<Scalars['String']>;
};

export type StopServiceCreateInput = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  channel?: Maybe<Channel>;
  stopDate: Scalars['LocalDate'];
};

export type StopServiceSubmitRequest = {
  encryptedAccountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  phoneNumber?: Maybe<Scalars['String']>;
  serviceAddress?: Maybe<AddressInput>;
  anyoneRemainingAtProperty?: Maybe<Scalars['Boolean']>;
  stopDate?: Maybe<Scalars['String']>;
  finalBillAddress?: Maybe<StopServiceAddressInput>;
  phoneExt?: Maybe<Scalars['String']>;
  channel?: Maybe<Channel>;
};

export type SubmitCommPreferencesRequest = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  notificationTypes: Array<NotificationTypeInput>;
};

export type SubmitCommPreferencesResponse = {
   __typename?: 'SubmitCommPreferencesResponse';
  success: Scalars['Boolean'];
};

export type SubmitUpdateInfoInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  originalUpdateAccountInfo?: Maybe<UpdateAccountInfoInput>;
  updateInfo?: Maybe<UpdateAccountInfoInput>;
  serviceAddress?: Maybe<AddressInput>;
  accountType?: Maybe<OnlineAccountType>;
  channel?: Maybe<Channel>;
};

export type SubmitUpdateInformationInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  encryptedPersonId?: Maybe<Scalars['String']>;
  originalUpdateAccountInfo?: Maybe<UpdateAccountInformationInput>;
  updateInfo?: Maybe<UpdateAccountInformationInput>;
  serviceAddress?: Maybe<AddressInput>;
  accountType?: Maybe<OnlineAccountType>;
};

export type SuggestedAddress = {
   __typename?: 'SuggestedAddress';
  addressLine1?: Maybe<Scalars['String']>;
  city?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  postal?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  isGeneralAddress?: Maybe<Scalars['Boolean']>;
  isHighwayContractRoute?: Maybe<Scalars['Boolean']>;
};

export type SupplyHistory = {
   __typename?: 'SupplyHistory';
  intervalDateTime: Scalars['DateTime'];
  supplyTypeDetails?: Maybe<Array<Maybe<SupplyTypeDetail>>>;
};

export type SupplyHistoryParams = {
  supplyDate: Scalars['DateTime'];
  interval: Scalars['Int'];
};

export enum SupplyType {
  Solar = 'SOLAR',
  Hydro = 'HYDRO',
  Thermal = 'THERMAL',
  Wind = 'WIND',
  Baoload = 'BAOLOAD'
}

export type SupplyTypeDetail = {
   __typename?: 'SupplyTypeDetail';
  type: SupplyType;
  supply: Scalars['Float'];
};

export type TimeOfDayEligibility = {
   __typename?: 'TimeOfDayEligibility';
  isEligible: Scalars['Boolean'];
  ineligibilityType?: Maybe<Array<TimeOfDayEligibilityType>>;
};

export enum TimeOfDayEligibilityType {
  UnenrolledLast_12Months = 'UNENROLLED_LAST_12_MONTHS',
  CustomerOrCoCustomer = 'CUSTOMER_OR_CO_CUSTOMER',
  SaStatusNone = 'SA_STATUS_NONE',
  SaStatusMultiple = 'SA_STATUS_MULTIPLE',
  Residential_07 = 'RESIDENTIAL_07',
  EqualPay = 'EQUAL_PAY',
  SaCharNetSpo = 'SA_CHAR_NET_SPO',
  PendingFinancialTransactions = 'PENDING_FINANCIAL_TRANSACTIONS'
}

export type TimeOfDayEnrollmentConfirmation = {
   __typename?: 'TimeOfDayEnrollmentConfirmation';
  referenceId: Scalars['String'];
  isSuccess: Scalars['Boolean'];
  exceptions?: Maybe<Array<Maybe<TimeOfDayEnrollmentException>>>;
};

export type TimeOfDayEnrollmentException = {
   __typename?: 'TimeOfDayEnrollmentException';
  type: TimeOfDayEnrollmentExceptionType;
  value: Scalars['Boolean'];
};

export enum TimeOfDayEnrollmentExceptionType {
  Meter = 'METER'
}

export type TimeOfDayEnrollmentInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export enum TimeOfDayEnrollmentStatus {
  PendingUnenrolled = 'PendingUnenrolled',
  PendingEnrolled = 'PendingEnrolled',
  PendingUnenroll = 'PendingUnenroll',
  Unenrolled = 'Unenrolled',
  PendingEnroll = 'PendingEnroll',
  Enrolled = 'Enrolled',
  CancelledEnrollement = 'cancelledEnrollement',
  CancelledUnenrollement = 'cancelledUnenrollement'
}

export type TimeOfDayInfo = {
   __typename?: 'TimeOfDayInfo';
  enrollmentStatus: TimeOfDayEnrollmentStatus;
  eligibility?: Maybe<TimeOfDayEligibility>;
};

export type TimeOfDayUnenrollmentConfirmation = {
   __typename?: 'TimeOfDayUnenrollmentConfirmation';
  referenceId: Scalars['String'];
  isSuccess: Scalars['Boolean'];
};

export type TimeOfDayUnenrollmentInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type Tpa = {
   __typename?: 'TPA';
  payAgreementAmount?: Maybe<Scalars['Float']>;
  payAgreementMonths?: Maybe<Scalars['Int']>;
  payAgreementDownPayment?: Maybe<Scalars['Float']>;
  installmentFullBillAmount?: Maybe<Scalars['Float']>;
  installmentAmount?: Maybe<Scalars['Float']>;
};

export type Tpa_AccountDetail = {
   __typename?: 'TPA_AccountDetail';
  isEligible: Scalars['Boolean'];
  isEnrolled: Scalars['Boolean'];
  accountBalance?: Maybe<Scalars['Float']>;
  recommendedPaymentMonths?: Maybe<Scalars['Int']>;
  enrolledInstallmentDetails?: Maybe<EnrolledInstallmentDetails>;
};

export type Tpa_EnrollInput = {
  accountId: Scalars['String'];
  personId: Scalars['String'];
  channel: Channel;
  arrangementAmount: Scalars['Float'];
  downPaymentAmount: Scalars['Float'];
  installments: Scalars['Int'];
  saType: SaType;
  allowSaTypes: Scalars['Boolean'];
};

export type Tpa_EnrollmentConfirmation = {
   __typename?: 'TPA_EnrollmentConfirmation';
  confirmationNumber: Scalars['String'];
  enrolledInstallmentDetails: EnrolledInstallmentDetails;
};

export type Tpa_MonthsMatrix = {
   __typename?: 'TPA_MonthsMatrix';
  monthNumber: Scalars['Int'];
  isPaymentCompleted: Scalars['Boolean'];
  doesBillExist: Scalars['Boolean'];
  paymentDate: Scalars['DateTime'];
  monthlyAmount: Scalars['Float'];
};

export type UnregisterResponse = {
   __typename?: 'UnregisterResponse';
  success?: Maybe<Scalars['Boolean']>;
};

export type UpdateAccountDetailsResponse = ResponseBase & {
   __typename?: 'UpdateAccountDetailsResponse';
  AccountDetail?: Maybe<AccountDetail>;
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
};

export type UpdateAccountInfoInput = {
  primaryPhone?: Maybe<Scalars['String']>;
  accountType?: Maybe<OnlineAccountType>;
  altPhoneInfo?: Maybe<AlternatePhoneInfoInput>;
  commContactInfo?: Maybe<CommercialContactInfoInput>;
  mailingAddress?: Maybe<AddressInput>;
  meterAccessInfo?: Maybe<MeterAccessInput>;
  persons?: Maybe<Array<Maybe<AccountPersonInput>>>;
};

export type UpdateAccountInformationInput = {
  primaryPhone?: Maybe<Scalars['String']>;
  mobilePhone?: Maybe<Scalars['String']>;
  alternatePhone?: Maybe<Scalars['String']>;
  accountType?: Maybe<OnlineAccountType>;
  commContactInfo?: Maybe<CommercialContactInfoInput>;
  mailingAddress?: Maybe<AddressInput>;
  persons?: Maybe<Array<Maybe<AccountPersonInput>>>;
};

export enum UpdateAlertsError {
  None = 'None',
  PhoneAttachedToActiveAccount = 'PhoneAttachedToActiveAccount',
  Other = 'Other',
  ContactExists = 'ContactExists'
}

export type UpdateAlertsRequest = {
  encryptedPersonId?: Maybe<Scalars['String']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  newPhoneNumber?: Maybe<Scalars['String']>;
  originalMobilePhoneNumber?: Maybe<Scalars['String']>;
  originalMobilePhoneSequence?: Maybe<Scalars['Float']>;
  originallyNotEnrolled?: Maybe<Scalars['Boolean']>;
  alerts?: Maybe<Array<Maybe<Alertinput>>>;
};

export type UpdateAlertsResponse = {
   __typename?: 'UpdateAlertsResponse';
  updateAlertsError?: Maybe<UpdateAlertsError>;
};

export type UpdateGroupInput = {
  groupId: Scalars['String'];
  groupName?: Maybe<Scalars['String']>;
  groupCode?: Maybe<Scalars['String']>;
  isDefault?: Maybe<Scalars['Boolean']>;
};

export type UpdateGroupResponse = ResponseBase & {
   __typename?: 'UpdateGroupResponse';
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
  groupStatus: GroupStatus;
  group?: Maybe<Group>;
};

export type UpdateMailingAddressInput = {
  accountNumber: Scalars['String'];
  encryptedPersonId: Scalars['String'];
  mailingAddress: MailingAddress;
};

export type UpdatePaperlessBillRequest = {
  encryptedAccountNumber: Scalars['String'];
  isPaperless: Scalars['Boolean'];
  email: Scalars['String'];
  encryptedPersonId: Scalars['String'];
};

export type UpdatePaperlessBillResponse = {
   __typename?: 'UpdatePaperlessBillResponse';
  success: Scalars['Boolean'];
};

export type UpdateProfilePhoneInput = {
  encryptedPersonId: Scalars['String'];
  phoneNumber: Scalars['PhoneNumber'];
  isPrimary: Scalars['Boolean'];
  phoneType: PhoneType;
};

export type UpdateServiceAgreementResponse = ResponseBase & {
   __typename?: 'UpdateServiceAgreementResponse';
  serviceAgreementNickname?: Maybe<ServiceAgreementNickname>;
  success?: Maybe<Scalars['Boolean']>;
  code?: Maybe<Scalars['Int']>;
  message?: Maybe<Scalars['String']>;
  errorReason?: Maybe<ErrorReason>;
};

export enum UserAccountPremiseAccountType {
  Res = 'RES',
  Com = 'COM',
  Self = 'SELF',
  State = 'STATE',
  Summary = 'SUMMARY',
  Ret = 'RET',
  Eba = 'EBA',
  Unknown = 'UNKNOWN'
}

export type UserAccountPremiseInfo = {
  email?: Maybe<Scalars['String']>;
};

export type UserAccountPremiseInfoDetail = {
   __typename?: 'UserAccountPremiseInfoDetail';
  accountNumber?: Maybe<Scalars['String']>;
  mainPersonId?: Maybe<Scalars['String']>;
  premiseId?: Maybe<Scalars['String']>;
};

export type UserAccountPremiseInfoResponse = {
   __typename?: 'UserAccountPremiseInfoResponse';
  userAccountPremiseList?: Maybe<Array<Maybe<UserAccountPremiseInfoDetail>>>;
};

export type ValidateAccountExistsRequest = {
  value: Scalars['String'];
  lookUpType: AccountLookUpType;
};

export type ValidateAccountExistsResponse = {
   __typename?: 'ValidateAccountExistsResponse';
  result: Scalars['Boolean'];
};

export enum VerificationType {
  Ein = 'EIN',
  Phone = 'PHONE',
  Dob = 'DOB'
}

export type VerifiedIdentity = {
   __typename?: 'VerifiedIdentity';
  personId: Scalars['String'];
  verified: Scalars['Boolean'];
};

export type VerifyAccountGroupDetailsReponse = {
   __typename?: 'VerifyAccountGroupDetailsReponse';
  reason?: Maybe<VerifyAccountGroupReason>;
  personId?: Maybe<Scalars['String']>;
};

export type VerifyAccountGroupDetailsRequest = {
  accountNumber: Scalars['String'];
  businessName: Scalars['String'];
  verificationValue: Scalars['String'];
  verificationType: AccountGroupVerificationType;
};

export enum VerifyAccountGroupReason {
  VerificationFailed = 'VerificationFailed',
  PersonFound = 'PersonFound',
  PersonNotFound = 'PersonNotFound',
  AccountNotFound = 'AccountNotFound'
}

export type VerifyEqualsEncryptedValueRequest = {
  clearTextValue: Scalars['String'];
  encryptedValue: Scalars['String'];
};

export type VerifyEqualsEncryptedValueResponse = {
   __typename?: 'VerifyEqualsEncryptedValueResponse';
  result: Scalars['Boolean'];
};

export type VerifyIdentityInput = {
  personId: Scalars['String'];
  identity: IdentityInput;
};

export enum VerifyLevel {
  None = 'None',
  Verified = 'Verified',
  InteractionRequired = 'InteractionRequired',
  Multiple = 'Multiple',
  PremisesPartial = 'PremisesPartial',
  StreetPartial = 'StreetPartial'
}

export type ViewBillAverageTemperature = {
   __typename?: 'ViewBillAverageTemperature';
  temperatureSource?: Maybe<Scalars['String']>;
  currentBillingPeriod?: Maybe<ViewBillAverageTemperatureBillingPeriod>;
  previousBillingPeriod?: Maybe<ViewBillAverageTemperatureBillingPeriod>;
};

export type ViewBillAverageTemperatureBillingPeriod = {
   __typename?: 'ViewBillAverageTemperatureBillingPeriod';
  date?: Maybe<Scalars['DateTimeCustom']>;
  averageTemperature?: Maybe<Scalars['Float']>;
  totalCost?: Maybe<Scalars['Float']>;
  totalKwh?: Maybe<Scalars['Float']>;
};

export type ViewBillDetails = {
   __typename?: 'ViewBillDetails';
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  downloadBillUrl?: Maybe<Scalars['String']>;
  billStatus?: Maybe<Scalars['String']>;
  billDate?: Maybe<Scalars['DateTimeCustom']>;
  dueDate?: Maybe<Scalars['DateTimeCustom']>;
  amountDue?: Maybe<Scalars['Float']>;
  previousBalance?: Maybe<Scalars['Float']>;
  totalAdjustments?: Maybe<Scalars['Float']>;
  totalCurrentCharges?: Maybe<Scalars['Float']>;
  totalBalanceAfterBill?: Maybe<Scalars['Float']>;
  hasBills?: Maybe<Scalars['Boolean']>;
  encryptedBillId?: Maybe<Scalars['String']>;
  kwh?: Maybe<Scalars['Float']>;
  billingPeriodStartDate?: Maybe<Scalars['DateTimeCustom']>;
  billingPeriodEndDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type ViewBillDetailsRequest = {
  encryptedAccountNumber: Scalars['String'];
};

export type ViewBillDownloadPdfRequest = {
  encryptedBillId?: Maybe<Scalars['String']>;
};

export type ViewBillDownloadPdfResponse = {
   __typename?: 'ViewBillDownloadPdfResponse';
  pdf?: Maybe<Scalars['String']>;
  success?: Maybe<Scalars['Boolean']>;
};

export type ViewBillInfo = {
   __typename?: 'ViewBillInfo';
  billStatus?: Maybe<Scalars['String']>;
  billDate?: Maybe<Scalars['DateTimeISO']>;
  dueDate?: Maybe<Scalars['DateTimeISO']>;
  amountDue?: Maybe<Scalars['Float']>;
  previousBalance?: Maybe<Scalars['Float']>;
  totalAdjustments?: Maybe<Scalars['Float']>;
  totalCurrentCharges?: Maybe<Scalars['Float']>;
  totalBalanceAfterBill?: Maybe<Scalars['Float']>;
  kwh?: Maybe<Scalars['Float']>;
  billingPeriodStartDate?: Maybe<Scalars['DateTimeISO']>;
  billingPeriodEndDate?: Maybe<Scalars['DateTimeISO']>;
};

export type ViewBillInfoResponse = {
   __typename?: 'ViewBillInfoResponse';
  viewBillDetails?: Maybe<ViewBillDetails>;
  viewBillAverageTemperature?: Maybe<ViewBillAverageTemperature>;
  viewBillMonthUsage?: Maybe<ViewBillMonthlyUsage>;
};

export type ViewBillMonthlyUsage = {
   __typename?: 'ViewBillMonthlyUsage';
  usages?: Maybe<Array<Maybe<ViewBillMonthlyUsageDetails>>>;
};

export type ViewBillMonthlyUsageDetails = {
   __typename?: 'ViewBillMonthlyUsageDetails';
  monthName?: Maybe<Scalars['String']>;
  year?: Maybe<Scalars['String']>;
  totalKwh?: Maybe<Scalars['Float']>;
};

export enum ViewPaymentHistoryBillingAndPaymentType {
  Unknown = 'Unknown',
  Bill = 'Bill',
  Payment = 'Payment',
  PendingPayment = 'PendingPayment',
  Agency = 'Agency'
}

export type ViewPaymentHistoryBillingUsageInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type ViewPaymentHistoryBillingUsageResponse = {
   __typename?: 'ViewPaymentHistoryBillingUsageResponse';
  billingUsages?: Maybe<Array<Maybe<ViewPaymentHistoryDetail>>>;
  totalDetailsRecords?: Maybe<Scalars['Int']>;
};

export type ViewPaymentHistoryDetail = {
   __typename?: 'ViewPaymentHistoryDetail';
  date?: Maybe<Scalars['DateTimeCustom']>;
  billingPeriodStartDate?: Maybe<Scalars['DateTimeCustom']>;
  billingPeriodEndDate?: Maybe<Scalars['DateTimeCustom']>;
  amountDue?: Maybe<Scalars['Float']>;
  kwh?: Maybe<Scalars['Float']>;
  amountPaid?: Maybe<Scalars['Float']>;
  encryptedBillId?: Maybe<Scalars['String']>;
  type?: Maybe<ViewPaymentHistoryBillingAndPaymentType>;
};

export type ViewPaymentHistoryDetailInput = {
  pageIndex?: Maybe<Scalars['Int']>;
  noOfDisplay?: Maybe<Scalars['Int']>;
  encryptedAccountNumber?: Maybe<Scalars['String']>;
};

export type ViewPaymentHistoryDetailResponse = {
   __typename?: 'ViewPaymentHistoryDetailResponse';
  paymentHistoryDetails?: Maybe<Array<Maybe<ViewPaymentHistoryDetail>>>;
  totalDetailsRecords?: Maybe<Scalars['Int']>;
};

export type ViewPaymentHistoryInput = {
  encryptedAccountNumber?: Maybe<Scalars['String']>;
  startDate?: Maybe<Scalars['DateTimeCustom']>;
  endDate?: Maybe<Scalars['DateTimeCustom']>;
};

export type ViewPaymentHistoryResponse = {
   __typename?: 'ViewPaymentHistoryResponse';
  amountDue?: Maybe<Scalars['Float']>;
  paymentAdjustments?: Maybe<Scalars['Float']>;
  balanceForward?: Maybe<Scalars['Float']>;
  dueDate?: Maybe<Scalars['DateTimeCustom']>;
  billingAndPaymentDetails?: Maybe<Array<Maybe<ViewPaymentHistoryDetail>>>;
  totalDetailsRecords?: Maybe<Scalars['Float']>;
};

export type ZipCodeInfo = {
   __typename?: 'ZipCodeInfo';
  zipCode: Scalars['String'];
  lat?: Maybe<Scalars['Latitude']>;
  long?: Maybe<Scalars['Longitude']>;
};
