const express = require('express');
const fs = require("fs");
const bodyParser = require('body-parser');
const app = express();
const routes = require('./routes');
const path = require("path");
const URLS = require("./utils/hspAPIs.json");
const SESSION_PATH = "./data/session.json";
const _ = require("lodash");
function setNewSession(isLogout, userdata) {
    try {
        fs.readFile(SESSION_PATH, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                throw err;
            }
            if (data) {
                let sessionUsers = JSON.parse(data);
                sessionUsers = _.reject(sessionUsers, function (obj) { return (obj.userID === userdata.userID); });
                if (!isLogout) {
                    sessionUsers.push(userdata);
                }
                json = JSON.stringify(sessionUsers);
                fs.writeFile(SESSION_PATH, json, 'utf8', (err) => {
                    if (err) {
                        console.log(err);
                        throw err;
                    }
                });
            }

        });
    } catch (e) {
        console.log(e.message)

    }
}
function getSession(userdata, callback) {
    try {
        fs.readFile(SESSION_PATH, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                throw err;
            }
            if (data) {
                let sessionUsers = JSON.parse(data);
                const sessionUser = _.filter(sessionUsers, function (obj) { return (obj.userID === userdata.userID); });
                callback(sessionUser)
            }

        });
    } catch (e) {
        console.log(e.message)

    }

}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});
app.use(URLS.base + '/api', routes);
app.use("/", express.static(__dirname + '/build'));
var port = process.env.PORT || 5000;
var server = require("http").createServer(app);
var io = require('socket.io')(server, { path: URLS.base + "/socket.io" });

io.on('connection', function (socket) {
    // if (socket.handshake.query && socket.handshake.query.userID && socket.handshake.query.sessionId) {
    //     sessionUsers = _.reject(sessionUsers, function (obj) { return (obj.userID === socket.handshake.query.userID); });
    //     const user = {
    //         sessionId: socket.handshake.query.sessionId,
    //         userID: socket.handshake.query.userID
    //     }
    //     sessionUsers.push(user);
    // }
    socket.on("SET_NEW_SESSION", (userdata) => {


        if (userdata.force) {
            getSession(userdata, (session) => {

                if (session.length > 0) {
                    socket.broadcast.emit("REMOVE_OLD_SESSION", session[0]);

                }
                setNewSession(false, userdata);
            });
        }
        else {
            setNewSession(false, userdata);
        }


    });
    socket.on("disconnect", function () {

    });
    /**
    * Logout the user
    */
    socket.on('logout', (data) => {
        socket.disconnect();
        setNewSession(true, data);
    });
});
server.listen(port, () => console.log('Orch app listening on port ' + port));
app.get('/*', function (req, res, next) {
    res.sendFile(path.resolve(__dirname + '/build/index.html'));
});
module.exports = {
    app
}