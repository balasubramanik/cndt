/*
* login activity Nodejs and Socket.io
* @author Birendranath Maity
*/
const _ = require("lodash");
let sessionUsers = [];
class Socket {

    constructor(socket) {
        this.io = socket;
    }
    socketEvents() {

        this.io.on('connection', (socket) => {

            if (socket.handshake.query && socket.handshake.query.userID && socket.handshake.query.sessionId) {
                sessionUsers = _.reject(sessionUsers, function (obj) { return (obj.userID === socket.handshake.query.userID); });
                const user = {
                    sessionId: socket.handshake.query.sessionId,
                    userID: socket.handshake.query.userID
                }
                sessionUsers.push(user);
            }
            socket.on("SET_NEW_SESSION", (userdata) => {
                if (userdata.force) {
                    const filteredData = _.filter(sessionUsers, (obj) => obj.userID == userdata.userID);
                    if (filteredData.length > 0) {
                        socket.broadcast.emit("REMOVE_OLD_SESSION", filteredData[0]);

                    }
                    sessionUsers = _.reject(sessionUsers, function (obj) { return (obj.userID === userdata.userID); });
                }
                sessionUsers.push(userdata);

            });
            socket.on("disconnect", function () {

            });
            /**
            * Logout the user
            */
            socket.on('logout', (data) => {
                sessionUsers = _.reject(sessionUsers, function (obj) { return (obj.userID === data.userID); });
                 socket.disconnect();
            });

            /**
            * sending the disconnected user to all socket users. 
            */
            socket.on('disconnect', () => {
                // console.log(socket.id)
                //  socket.broadcast.emit('ADMIN_SIDE_USER_CONNECT');
            });

        });

    }

    socketConfig() {

        this.io.use(function (socket, next) {
            return next();
            // return the result of next() to accept the connection.
            // if (socket.handshake.query && socket.handshake.query.userID) {

            //     return next();
            // }

            // // call next() with an Error if you need to reject the connection.
            // next(new Error('Authentication error'));
        });

        this.socketEvents();
    }
}
module.exports = Socket;