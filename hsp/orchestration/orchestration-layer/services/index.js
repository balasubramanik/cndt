// wrapper/orchestration for business logic service calls

const controltypeservice = require("./controltypes.service");
const { xmlBrowserService ,GetUserReportsService,GetReferenceCodesService} = require('./xmlBrowser.service');

module.exports = {
    controltypeservice,
    xmlBrowserService,
    GetUserReportsService,
    GetReferenceCodesService
}