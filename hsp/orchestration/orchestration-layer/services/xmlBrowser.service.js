const fetch = require("node-fetch");
const https = require("https");
const agent = new https.Agent({
  rejectUnauthorized: false
});

const xmlBrowserService = (url, body) => {
  return fetch(url, {
    agent,
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: body
  });
};
const GetUserReportsService = (url, body) => {
  console.log(url)
  return fetch(url, {
    agent,
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: body
  });
};
const GetReferenceCodesService = (url, body) => {
  console.log(url)
  return fetch(url, {
    agent,
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: body
  });
};

module.exports = {
  xmlBrowserService,
  GetUserReportsService,
  GetReferenceCodesService
};
