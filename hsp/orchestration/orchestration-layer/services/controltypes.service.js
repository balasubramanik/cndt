// Keep all Business logic here
const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');

const agent = new https.Agent({
    rejectUnauthorized: false
});

let CallArray = [];

const executeStoreProcedure = (procName, args, callback) => {
    CallArray.push(procName);
    let url = hspAPIs.baseURI;
    try {
        const lowerName = procName.toLowerCase();
        if (lowerName === "getbenefitstructures") {
            url = `${url}${hspAPIs.getBenefitStructuresAPI}`;
        } else if (lowerName === "getreferencecodes_v2") {
            url = `${url}${hspAPIs.getReferenceCodesAPI}`;
        } else if (lowerName === "getoffices_v2") {
            url = `${url}${hspAPIs.findOfficesAPI}`;
        } else if (lowerName === "getdemographicsfromzips") {
            url = `${url}${hspAPIs.getDemographicsAPI}`;
        }

        const response = fetch(url, {
            agent,
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(args)
        }).then(res => {
            return res.json();
        }).then(json => {
            return callback(json);
        })
    } catch (e) {
        console.log("error>>", e.message)
        return callback(e);
    }
    return;
}

const constructProcedureData = (req, resp, callback) => {
    if (resp.findControls && resp.findControls.length) {
        resp.findControls.map((control, index) => {
            const controlType = control.controlType;
            if ((controlType === "ToolkitCombobox" && control.linkTo !== null && control.linkFrom === null && control.comboBoxSql) ||
                (controlType === "ToolkitCombobox" && control.linkTo === null && control.linkFrom === null && control.comboBoxSql) ||
                controlType === "ToolkitMultiColumnCombobox"
            ) {
                const paramsObj = {};
                const stmnt = control.comboBoxSql.split(" ");
                const procName = stmnt[1].slice("3");
                const usageArray = stmnt.filter((str) => str.includes("Usage"))
                if (usageArray && usageArray.length) {
                    paramsObj.usage = usageArray[0].match(/'([^']+)'/)[1];
                }

                const typeArray = stmnt.filter((str) => str.includes("Type"))
                if (typeArray && typeArray.length) {
                    paramsObj.type = typeArray[0].match(/'([^']+)'/)[1];
                }
                paramsObj.sessionId = req.body.sessionId;

                executeStoreProcedure(procName, paramsObj, (data) => {
                    control.comboData = data[Object.keys(data)[1]];
                    CallArray.pop();
                    if (CallArray.length === 0) {
                        return callback(resp);
                    }
                });
            }
        });
    }
    else{
        return callback(resp); 
    }
}
module.exports = {
    constructProcedureData
}