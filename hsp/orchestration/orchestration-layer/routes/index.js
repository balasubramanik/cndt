// Keep all routes here
const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');
const hspAPIs = require('../utils/hspAPIs.json');
const https = require('https');
const agent = new https.Agent({
    rejectUnauthorized: false
});
const { controltypes, contracts, office, profiles, search, connectuser, disconnectuser, getsessionid,
    getreferencecodes, getcountries, getdemographics, getlistviewprofilesettings, dynamicSearchParameter,
    setUserOptions, getUserOptions, getXmlStyleSheet, controllerGetUserReport, controllerGetReferenceCodes, lastsearch, setUserListViewSetting, getPermission, findCommand } = require('../controllers');

const router = express.Router();
router.use(cors());
const setSession = (req, res, next) => {

    if (req.headers.sessionid) {
        req.body.sessionId = req.headers.sessionid;
    }
    if (req.headers.userid) {
        req.body.userID = req.headers.userid;
    }

    next();

}
router.use(setSession)
router.post('/connectUser', connectuser.connectUser);
router.post('/findControls', controltypes.getFindControls);
router.post('/office', office.findOffices);
router.post('/contract', contracts.findContracts);
router.post('/profiles', profiles.findProfiles);
router.post('/getSearchData', search.getSearch);
router.post('/setSearchData', search.setSearch);
router.post('/disConnectUser', disconnectuser.disConnectUser);
router.post('/getSessionID', getsessionid.getSessionID);
router.post('/getrefcodes', getreferencecodes.getRefCodeData);
router.post('/getcountries', getcountries.getCountries);
router.post('/getdemographics', getdemographics.getDemographics);
router.post('/getlistviewprofilesettings', getlistviewprofilesettings.getListViewProfileSettings);
router.post('/dynamicsearch', dynamicSearchParameter.getDynamicSearchParameter);
router.post('/setUserOptions', setUserOptions.setUserOptions);
router.post('/getUserOptions', getUserOptions.getUserOptions);
router.post('/getxmlstylesheet', getXmlStyleSheet);
router.post('/getuserreports', controllerGetUserReport);
router.post('/getreferencecodes', controllerGetReferenceCodes);
router.post('/getrecentsearch', lastsearch.getLastSearch);
router.post('/setrecentsearch', lastsearch.setLastSearch);
router.post('/setUserListViewSetting', setUserListViewSetting.setUserListViewSetting);
router.post('/getUserListViewSetting', setUserListViewSetting.getUserListViewSetting);
router.post('/getUserListViewSetting', setUserListViewSetting.getUserListViewSetting);
router.post('/getPermission', getPermission.getPermission);
router.post('/findCommands', findCommand.findCommands);
module.exports = router;
