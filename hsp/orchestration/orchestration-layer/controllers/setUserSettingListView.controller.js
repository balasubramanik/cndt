const fs = require("fs");
const moment = require("moment");
const _ = require("lodash");
const setUserListViewSetting = (req, res, next) => {
  let dataPath = req.body.type === 'Contract' ? './data/contract.json' : './data/office.json';
  const userId = req.body.userID;
  const itemType = req.body.type;
  try {
    fs.readFile(dataPath, 'utf8', (err, data) => {
      if (err) {
        console.log(err);
        throw err;
      }
      let jsonData = [];
      if (data) {
        jsonData = JSON.parse(data)
      } else {
        jsonData = [];
      }
      const isExtis = _.filter(jsonData, (obj) => obj.userID == userId && obj.type === itemType);
      if (isExtis.length > 0) {
        jsonData = _.reject(jsonData, function (obj) { return (obj.userID === userId && obj.type === itemType); });

      }
      req.body.addedTime = moment().toISOString(true);
      jsonData.push(req.body);

      json = JSON.stringify(jsonData);

      fs.writeFile(dataPath, json, 'utf8', (err) => {
        if (err) {
          console.log(err);
          throw err;
        }
        res.sendStatus(200) && next();
      });
    });
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500) && next(e)
  }
}
const getUserListViewSetting = (req, res, next) => {
  let dataPath = req.body.type === 'Contract' ? './data/contract.json' : './data/office.json';
  const userId = req.body.userID;
  const itemType = req.body.type;
  try {
    fs.readFile(dataPath, (err, data) => {
      if (err) {
        console.log(err);
        throw err;
      }

      if (data) {
        const parsedData = JSON.parse(data);
        const filteredData = _.filter(parsedData, (obj) => obj.userID == userId && obj.type === itemType);

        res.send(filteredData[0]);
      }
      else {
        res.send([]);
      }
      next();
    });
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500) && next(e)
  }
}

module.exports = {
  setUserListViewSetting,
  getUserListViewSetting
}