const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');
const agent = new https.Agent({
  rejectUnauthorized: false
});

const findCommands = async (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.getFindCommandsAPI;
  try {
    const response = await fetch(url, {
      agent,
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        SessionId: req.body.sessionId,
        Usage: "|All|"
      })
    });
    const findCommands = await response.json();
    res.send({findCommands: findCommands});
    next();
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500) && next(e)
  }
}

module.exports = {
    findCommands
}