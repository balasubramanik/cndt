const fs = require("fs");
const moment = require("moment");
const _ = require("lodash");
const dataPath = "./data/lastsearch.json";

const getLastSearch = (req, res, next) => {
    const userId = req.body.userID;
    const itemType = req.body.itemType;
    try {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                throw err;
            }
            const parsedData = JSON.parse(data);
            const filteredData = _.filter(parsedData, (obj) => obj.userID == userId && obj.itemType === itemType);
            const sortedArray = _.orderBy(filteredData, ['addedTime'], ['desc']);
            res.send(sortedArray[0]);
            next();
        });
    } catch (e) {
        console.log(e.message)
        res.sendStatus(500) && next(e)
    }
}

const setLastSearch = (req, res, next) => {
    const userId = req.body.userID;
    const itemType = req.body.itemType;
    try {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                throw err;
            }
            let jsonData = [];
            if (data) {
                jsonData = JSON.parse(data)
            } else {
                jsonData = [];
            }
            const isExtis = _.filter(jsonData, (obj) => obj.userID == userId && obj.itemType === itemType);
            if (isExtis.length > 0) {
                jsonData = _.reject(jsonData, function (obj) { return (obj.userID === userId && obj.itemType === itemType); });

            }
            req.body.addedTime = moment().toISOString(true);
            jsonData.push(req.body);
            json = JSON.stringify(jsonData);

            fs.writeFile(dataPath, json, 'utf8', (err) => {
                if (err) {
                    console.log(err);
                    throw err;
                }
                res.sendStatus(200) && next();
            });
        });
    } catch (e) {
        console.log(e.message)
        res.sendStatus(500) && next(e)
    }
}

module.exports = {
    getLastSearch,
    setLastSearch
}