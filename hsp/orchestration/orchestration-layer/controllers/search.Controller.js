const fs = require("fs");
const moment = require("moment");
const _ = require("lodash");
//const dataPath = "./data/search.json";

const getSearch = (req, res, next) => {
    let dataPath = req.body.itemType === 'ContractSearch' ? './data/lastten_contract.json' : './data/lastten_office.json';
    const userId = req.body.userID;
    const itemType = req.body.itemType;
    try {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                throw err;
            }
            if (data) {
                const parsedData = JSON.parse(data);
                const filteredData = _.filter(parsedData, (obj) => obj.userID == userId && obj.itemType === itemType);
                const sortedArray = _.orderBy(filteredData, ['addedTime'], ['desc']);
                if (req.body.lastTen) {
                    res.send(sortedArray.slice(0, 10));
                } else {
                    res.send(sortedArray[0]);
                }
            }
            else {
                res.send([]);
            }
            next();
        });
    } catch (e) {
        console.log(e.message)
        res.sendStatus(500) && next(e)
    }
}

const setSearch = (req, res, next) => {
    const userId = req.body.userID;
    const itemType = req.body.itemType;
    const ID = req.body.itemType === 'ContractSearch' ? req.body.itemValue.contractId : req.body.itemValue.officeID;
    const filterID = req.body.itemType === 'ContractSearch' ? 'contractId' : 'officeID';
    let dataPath = req.body.itemType === 'ContractSearch' ? './data/lastten_contract.json' : './data/lastten_office.json';
    try {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                console.log(err);
                throw err;
            }
            let jsonData = [];
            if (data) {
                jsonData = JSON.parse(data)
            }
            const existArr = _.filter(jsonData, (obj) => obj.userID == userId && obj.itemType === itemType && obj.itemValue[filterID] === ID);
            if (existArr.length === 0) {

                const filteredData = _.filter(jsonData, (obj) => obj.userID == userId && obj.itemType === itemType);
                if (filteredData.length === 10) {

                    jsonData = _.reject(jsonData, function (obj) { return (obj.userID === filteredData[0].userID && obj.itemType === itemType && obj.itemValue[filterID] === filteredData[0].itemValue[filterID]); });

                }
                req.body.addedTime = moment().toISOString(true);
                jsonData.push(req.body);

            }
            else {
                _.map(jsonData, (obj, key) => {
                    if (obj.userID == userId && obj.itemType === itemType && obj.itemValue[filterID] === ID) {
                        jsonData[key].addedTime = moment().toISOString(true);
                    }

                });

            }



            json = JSON.stringify(jsonData);

            fs.writeFile(dataPath, json, 'utf8', (err) => {
                if (err) {
                    console.log(err);
                    throw err;
                }
                res.sendStatus(200) && next();
            });
        });
    } catch (e) {
        console.log(e.message)
        res.sendStatus(500) && next(e)
    }
}

module.exports = {
    getSearch,
    setSearch
}
