const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');
const agent = new https.Agent({
  rejectUnauthorized: false
});

const getCountries = async (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.getCountriesAPI;

  try {
    const response = await fetch(url, {
      agent,
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        req.body
      )
    });
    const multiComboData = await response.json();
    res.send(multiComboData);
    next();
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500) && next(e)
  }
}

module.exports = {
  getCountries
}