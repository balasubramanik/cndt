const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');
const { controltypes } = require("../services");
const agent = new https.Agent({
  rejectUnauthorized: false
});

const getRefCodeData = async (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.getReferenceCodesAPI;

  try {
    const response = await fetch(url, {
      agent,
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        req.body
      )
    });
    const resData = await response.json();
    res.send(resData);
    next();
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500) && next(e)
  }
}

module.exports = {
  getRefCodeData
}