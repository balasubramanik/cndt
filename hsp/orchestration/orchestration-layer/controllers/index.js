// wrapper/orchestration for call hsp APIS
const controltypes = require("./controltypes.controller");
const office = require("./office.controller");
const contracts = require("./contracts.controller");
const profiles = require("./profiles.controller");
const connectuser = require("./connectuser.controller");
const search = require("./search.Controller");
const disconnectuser = require("./disconnectuser.controller");
const getsessionid = require("./getsessionid.controller");
const getreferencecodes = require("./getreferencecodes.controller");
const getcountries = require("./getcountries.controller");
const getdemographics = require("./getdemographics.controller");
const getlistviewprofilesettings = require("./listviewprofilesettings.controller");
const dynamicSearchParameter = require('./dynamicSearchParameter.controller');
const setUserOptions = require('./setUserOptions.controller');
const getUserOptions = require('./getUserOptions.controller');
const { getXmlStyleSheet, controllerGetUserReport,controllerGetReferenceCodes } = require('./getxmlstylesheet.controller');
const lastsearch = require('./lastsearch.controller');
const setUserListViewSetting= require("./setUserSettingListView.controller");
const getUserListViewSetting= require("./getUserSettingListView.controller");
const getPermission =require("./getPermission");
const findCommand =require("./findCommand.controller");

module.exports = {
    controltypes,
    office,
    contracts,
    profiles,
    connectuser,
    disconnectuser,
    search,
    getsessionid,
    getreferencecodes,
    getcountries,
    getdemographics,
    getlistviewprofilesettings,
    dynamicSearchParameter,
    setUserOptions,
    getUserOptions,
    getXmlStyleSheet,
    controllerGetUserReport,
    controllerGetReferenceCodes,
    lastsearch,
    setUserListViewSetting,
    getUserListViewSetting,
    getPermission,
    findCommand
}
