const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');
const agent = new https.Agent({
  rejectUnauthorized: false
});

const findContracts = async (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.findContractsAPI;

  try {
    const response = await fetch(url, {
      agent,
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        req.body
      )
    });
    const findContracts = await response.json();
    res.send({countMetadata:findContracts.metaData,contracts:findContracts.contractsDTO});
    next();
  } catch (e) {
    console.log(e.message)
    res.sendStatus(500) && next(e)
  }
}

module.exports = {
  findContracts
}