const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');
const each = require("./sync");
const agent = new https.Agent({
    rejectUnauthorized: false
});

const getPermission = (req, res, next) => {
    const url = hspAPIs.baseURI + hspAPIs.getPermisstion;
    const providers = ['Contracts', 'Offices'];
    let Obj = {
        permissions: {
            office: false,
            contract: false
        }
    };
    each(providers,
        async (item, next) => {
            let mainreq = {
                "SessionId": req.body.sessionId,
                "EntityType": item,
                "Usage": 'USAGE1',
                "PermissionName": 'View',
                "ProductName": 'Meditrac',
            }
            const body = JSON.stringify(mainreq);
            try {
                const response = await fetch(url, {
                    agent,
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: body
                });
                const permission = await response.json();
                if (permission && permission.checkPermission && permission.checkPermission.length > 0) {
                    if (item === 'Contracts') {
                        Obj.permissions.contract = permission.checkPermission[0].permission;
                    }
                    else if (item === 'Offices') {
                        Obj.permissions.office = permission.checkPermission[0].permission;
                    }
                }
                next();
            } catch (e) {
                res.send(Obj);
            }
        },
        function (err, updatedItems) {
            res.send(Obj);
        }
    )
};
module.exports = {
    getPermission
};