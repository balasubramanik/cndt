const fetch = require('node-fetch');
const https = require('https');
const hspAPIs = require('../utils/hspAPIs.json');
const { controltypeservice } = require("../services");

const agent = new https.Agent({
  rejectUnauthorized: false
});

const getFindControls = async (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.getFindControlsAPI;
  try {
    const response = await fetch(url, {
      agent,
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        req.body
      )
    });
    const controlsData = await response.json();
    controltypeservice.constructProcedureData(req, controlsData, (resData) => {
      res.send(resData);
      next();
    });

  } catch (e) {
    res.sendStatus(500) && next(e)
  }
}

module.exports = {
  getFindControls
}