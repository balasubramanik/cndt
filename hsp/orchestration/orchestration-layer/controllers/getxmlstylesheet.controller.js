const hspAPIs = require("../utils/hspAPIs.json");
const { xmlBrowserService, GetUserReportsService, GetReferenceCodesService } = require("./../services");
const each = require("./sync");
const getXMLResponse = (respObj, req) => {
  const obj = respObj;
  const base = /(<base.[^(><.)]+>)/g;
  let { htmlResult } = obj;
  let strData = htmlResult.split(base);
  let css = strData[2].replace('styles.css', hspAPIs.base + '/assets/stylesheets/styles.css');
  let img = css.replace('HSPLogo.jpg', hspAPIs.base + '/assets/hsp-logo-stylesheet.png');
  img = img.replace('<img', '<img alt="HSP Logo Stylesheet"');
  strData = strData[0] + img;
  respObj.htmlResult = strData;
  return respObj;
};



const getXmlStyleSheet = async (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.getXmlStyleSheet;
  const body = JSON.stringify(req.body.xmlreqbody);
  try {
    const response = await xmlBrowserService(url, body);
    const respData = await response.json();
    const respXmlData = getXMLResponse(respData, req);
    res.send(respXmlData);
    next();
  } catch (e) {
    console.log(e);
    res.sendStatus(500) && next(e);
  }
};
const controllerGetUserReport = async (req, res, next) => {

  const url = hspAPIs.baseURI + hspAPIs.GetUserReports;
  const body = JSON.stringify(req.body);
  try {
    const response = await GetUserReportsService(url, body);
    const respData = await response.json();
    // const respXmlData = getXMLResponse(respData);
    res.send(respData);
    next();
  } catch (e) {
    console.log(e);
    res.sendStatus(500) && next(e);
  }
};
const controllerGetReferenceCodes = (req, res, next) => {
  const url = hspAPIs.baseURI + hspAPIs.GetReferenceCodes;
  var types = req.body.Type;
  console.log(req.body.SessionId)
  var Obj = {};
  each(types,
    async (item, next) => {
      var mainreq = {
        "sessionID": req.body.sessionId,
        "type": item.type,
        // "subType": item.subType ? item.subType : "",
        "Usage": req.body.Usage
      }
      const body = JSON.stringify(mainreq);
      try {
        const response = await GetReferenceCodesService(url, body);
        const respData = await response.json();
        Obj[item.type] = respData.referenceCodes_V2DTOs;
        next();
      } catch (e) {
        console.log(e);
        res.sendStatus(500) && next(e);
      }
    },
    function (err, updatedItems) {
      res.send(Obj);
    }
  )
};
module.exports = {
  getXmlStyleSheet,
  controllerGetUserReport,
  controllerGetReferenceCodes
};