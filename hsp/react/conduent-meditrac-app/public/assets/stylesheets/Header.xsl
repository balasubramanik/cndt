<?xml version="1.0" encoding="utf-8"?>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:html="http://www.w3.org/TR/html4/" exclude-result-prefixes="html"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:user="urn:my-scripts">
    <!--HEADER-->
    <!--Modify ONLY the HTML within the following <xsl:template> to change the HEADER for each XSL Stylesheet.-->
    <xsl:template name="header">
      <table width="100%">
        <tr>
          <td width="100px">
            <img src="HSPLogo.jpg" />
          </td>
          <td valign="bottom">
            <h4>Standard Embedded Dashboard</h4>
          </td>

        </tr>
      </table>
    </xsl:template>
    <!--END HEADER-->
</xsl:stylesheet>
