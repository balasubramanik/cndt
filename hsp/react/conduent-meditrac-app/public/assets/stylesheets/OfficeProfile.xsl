<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:variable name="donePath" select="concat(substring-before(/Office/HSPStandardInfo/DonePath,'DONE'),'Style Sheets\')" />
  <xsl:include href="Header.xsl"/>
  <xsl:include href="Footer.xsl"/>
  <xsl:output method = "html" encoding="Windows-1252" />
  <xsl:template match="/">

    <html>
      <head>
        <base href="{$donePath}" />
        <link rel="stylesheet" type="text/css" href="styles.css"/>

        <xsl:text disable-output-escaping="yes">

        <![CDATA[

        <script language="javascript">



          function hideAllGroup()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

                

		            for (i=0;i < locl.length;i++)
		            {
			            if (locl[i].id.indexOf('Group_') > -1)
				      {
                
					      locl[i].style.display = 'none';
					      toggleCaption(locl[i].id + '_caption','+')
				      }
		            }

		            return false;
		         }

          function showAllGroup()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

		            for (i=0;i<locl.length;i++)
		            {
			            if (locl[i].id.indexOf('Group_') > -1)
				      {
					      locl[i].style.display = '';
					      toggleCaption(locl[i].id + '_caption','-')
				      }
		            }

		            return false;
		         }

         
		      function hideAll()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

		            for (i=0;i < locl.length;i++)
		            {
			            if (locl[i].id.indexOf('_nav') > -1)
				      {
					      locl[i].style.display = 'none';
					      toggleCaption(locl[i].id + '_caption','+')
				      }
		            }

		            return false;
		         }

		         function showAll()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

		            for (i=0;i<locl.length;i++)
		            {
			            if (locl[i].id.indexOf('_nav') > -1)
				      {
					      locl[i].style.display = '';
					      toggleCaption(locl[i].id + '_caption','-')
				      }
		            }

		            return false;
		         }

		          function getItem(id)
		          {
			      var itm = false;
			      if(document.getElementById)
			          itm = document.getElementById(id);
			      else if(document.all)
			          itm = document.all[id];
			      else if(document.layers)
			          itm = document.layers[id];

			      return itm;
		          }

		          function toggleItem(id)
		          {
			      itm = getItem(id);

			      if(!itm)
			          return false;

			      if(itm.style.display == 'none')
			      {
			          itm.style.display = '';
			          toggleCaption(id + '_caption','-')
			      }
			      else
			      {
			          itm.style.display = 'none';
			          toggleCaption(id + '_caption','+')
			      }

			      return false;
		          }

		          function toggleCaption(id, v)
		          {
			      itm = getItem(id);

			      if(!itm)
			          return false;
      			
			      itm.innerHTML = v;

			      return false;
		          }

              
      </script>
  
      ]]>

      </xsl:text>


      </head>
      <title>
        Office Information
      </title>


      <body onload="hideAll();">

        <xsl:call-template name="header"/>


        <xsl:for-each select = "//Office">
          <!--Main Table-->
          <table width="100%" cellpadding="2">

            <!--Header Table-->
            <table width="100%">
              <!--Office Information-->
              <td width="70%" class="singleborder">
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="bottomdotted" colspan ="2">
                      <b class="header">Office Information</b>
                    </td>
                  </tr>

                  <tr>
                    <td style="vertical-align:top" width="30%">
                      <table cellpadding="0" cellspacing="0">
                        <tr>
                          <td colspan="2">
                            <tr>
                              <b>
                                <xsl:value-of select="OfficeInfo/@OfficeName" />&#160;
                                (<xsl:value-of select="OfficeInfo/@OfficeNumber" />)
                              </b>
                            </tr>
                            <tr>
                              <td>
                                <xsl:value-of select="OfficeInfo/@Address1" />
                              </td>
                            </tr>
                            <tr>
                              <td colspan="2">
                                <xsl:if test="OfficeInfo/@Address2!=''">
                                  <xsl:value-of select="OfficeInfo/@Address2"/>
                                </xsl:if>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <xsl:value-of select="OfficeInfo/@City"/>&#160;
                                <xsl:value-of select="OfficeInfo/@State"/>,&#160;
                                <xsl:value-of select="OfficeInfo/@Zip"/>&#160;
                                <xsl:value-of select="OfficeInfo/@CountryCode"/>&#160;
                              </td>
                            </tr>
                          </td>
                        </tr>
                      </table>
                    </td>

                
                    <td class="leftdotted">

                      <xsl:for-each select="OfficeInfo">

                        <table   cellpadding="0" cellspacing="0" align="top">

                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Contact Name'"/>
                            <xsl:with-param name="Value" select="@ContactName"/>
                            <xsl:with-param name="Position" select="1"/>
                          </xsl:call-template>

                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Contact Email'"/>
                            <xsl:with-param name="Value" select="@ContactEmail"/>
                            <xsl:with-param name="Position" select="2"/>
                          </xsl:call-template>
                          <xsl:choose>
                            <xsl:when test="@ContactPhone!=''">
                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Contact Phone'"/>
                            <xsl:with-param name="Value" select="concat('(',substring(@ContactPhone,1,3),') ',substring(@ContactPhone,4,3),'-',substring(@ContactPhone,7,4),' x ',(@ContactExt))"/>
                            <xsl:with-param name="Position" select="3"/>
                          </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:call-template name="InfoRow">
                                <xsl:with-param name="Header" select="'Contact Phone'"/>
                                <xsl:with-param name="Value" select="''"/>
                                <xsl:with-param name="Position" select="3"/>
                              </xsl:call-template>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:choose>
                            <xsl:when test="@ContactFax!=''">
                              <xsl:call-template name="InfoRow">
                                <xsl:with-param name="Header" select="'Contact Fax'"/>
                                <xsl:with-param name="Value" select="concat('(',substring(@ContactFax,1,3),') ',substring(@ContactFax,4,3),'-',substring(@ContactFax,7,4))"/>
                                <xsl:with-param name="Position" select="4"/>
                              </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:call-template name="InfoRow">
                                <xsl:with-param name="Header" select="'Contact Fax'"/>
                                <xsl:with-param name="Value" select="''"/>
                                <xsl:with-param name="Position" select="4"/>
                              </xsl:call-template>
                            </xsl:otherwise>
                          </xsl:choose>
                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Number Of Physicians'"/>
                            <xsl:with-param name="Value" select="@NumberOfPhysicians"/>
                            <xsl:with-param name="Position" select="5"/>
                          </xsl:call-template>


                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Access Code'"/>
                            <xsl:with-param name="Value" select="@AccessCode"/>
                            <xsl:with-param name="Position" select="6"/>
                          </xsl:call-template>

                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Wheelchair Access'"/>
                            <xsl:with-param name="Value" select="@WheelchairAccess"/>
                            <xsl:with-param name="Position" select="7"/>
                          </xsl:call-template>

                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Available After Hours'"/>
                            <xsl:with-param name="Value" select="@AvailableAfterHours"/>
                            <xsl:with-param name="Position" select="8"/>
                          </xsl:call-template>

                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Number Of Physicians Extendors'"/>
                            <xsl:with-param name="Value" select="@NumberOfPhysicians"/>
                            <xsl:with-param name="Position" select="9"/>
                          </xsl:call-template>

                          <xsl:call-template name="InfoRow">
                            <xsl:with-param name="Header" select="'Facility Operating Number'"/>
                            <xsl:with-param name="Value" select="@FacilityOperatingNumber"/>
                            <xsl:with-param name="Position" select="10"/>
                          </xsl:call-template>

                        </table>
                      </xsl:for-each>

                    </td>

                    <!--Office Hours Table-->
                    <td  width="30%" class="singleborder">
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="bottomdotted">
                            <b class="header">Office Hours</b>
                          </td>
                        </tr>
                        <xsl:for-each select="OfficeInfo">
                          <tr>
                            <td>
                              <b> Monday: </b><xsl:value-of select="@MondayStart"/> - <xsl:value-of select="@MondayEnd"/>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b> Tuesday: </b><xsl:value-of select="@TuesdayStart"/> - <xsl:value-of select="@TuesdayEnd"/>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b> Wednesday: </b><xsl:value-of select="@WednesdayStart"/> - <xsl:value-of select="@WednesdayEnd"/>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b> Thursday: </b><xsl:value-of select="@ThursdayStart"/> - <xsl:value-of select="@ThursdayEnd"/>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b> Friday: </b><xsl:value-of select="@FridayStart"/> - <xsl:value-of select="@FridayEnd"/>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b> Saturday: </b><xsl:value-of select="@SaturdayStart"/> - <xsl:value-of select="@SaturdayEnd"/>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <b> Sunday: </b><xsl:value-of select="@SundayStart"/> - <xsl:value-of select="@SundayEnd"/>
                            </td>
                          </tr>

                        </xsl:for-each>
                      </table>
                    </td>
                    <!--End Office Hours Table-->


                  </tr>
                </table>
              </td>

            </table>

            <!-- Navigation Bar -->
            <table align="center">
              <tr class="navigationborder">
                <td>
                  <a href="# " class="hover" title="Collapse All" onclick="hideAll();return false;"> [ Collapse All ]</a>
                </td>
                <td>
                  <a href="#" class="hover" title="Expand All" onclick="showAll();return false;"> [ Expand All ]</a>
                </td>
              </tr>
            </table>


       
            <!-- Mapped Providers Loop-->
            <xsl:variable name="MappedProvidersBody"  select="string(concat('MappedProvidersBody_','_nav'))"/>

            <!-- MappedProviders Table -->
            <xsl:if test="MappedProvider/MappedProviderInfo/@ProviderID !=''">
              <br></br>
              <table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <!-- Attribute header -->
                  <td colspan="3" style="padding-bottom: 3px">
                    <b class="detailsheader">
                      <a id='{$MappedProvidersBody}_caption' class="navigate" title="Collapse/Expand Mapped Providers Info" onclick="toggleItem('{$MappedProvidersBody}')">-</a>
                      Mapped Providers
                    </b>
                  </td>
                </tr>

                <!-- Collapse / Expand area for Standard FIlters-->
                <tbody id="{$MappedProvidersBody}">
                  <tr style="text-align: center;">
                    <td class="bottomdouble">
                      Last Name
                    </td>
                    <td class="bottomdouble">
                      First Name
                    </td>
                    <td class="bottomdouble">
                      Number
                    </td>
					<td class="bottomdouble">
                      NPI
                    </td>
					<td class="bottomdouble">
                      Monday 
                    </td>
					<td class="bottomdouble">
                      Tuesday 
                    </td>
					<td class="bottomdouble">
                      Wednesday 
                    </td>
					<td class="bottomdouble">
                      Thursday 
                    </td>
					<td class="bottomdouble">
                      Friday 
                    </td>
					<td class="bottomdouble">
                      Saturday 
                    </td>
					<td class="bottomdouble">
                      Sunday 
                    </td> 
					<td class="bottomdouble">
                      Total Office Hours
					</td>
          <td class="bottomdouble">
                      Primary Language
          </td>
                  </tr>



                  <xsl:for-each select="MappedProvider/MappedProviderInfo">

                    <xsl:variable name="HighlightColor">
                      <xsl:if test="position() mod 2 = 1">
                        <text>alternating1</text>
                      </xsl:if>
                      <xsl:if test="position() mod 2 = 0">
                        <text>alternating2</text>
                      </xsl:if>
                    </xsl:variable>

                    <tr class="{$HighlightColor}" style="text-align:center">
                      <td>
                        <xsl:value-of select="@ProviderLastName"/>
                      </td>
                      <td class ="leftdotted">
                        <xsl:value-of select="@ProviderFirstName"/>
                      </td>
                      <td class ="leftdotted">
                        <xsl:value-of select="@ProviderNumber"/>
                      </td> 
					  <td class ="leftdotted">
                        <xsl:value-of select="@NPI"/>
                      </td>
                      <td class ="leftdotted">
                        <xsl:value-of select="@ProviderMondayStart"/> - <xsl:value-of select="@ProviderMondayEnd"/>
                      </td>
					  <td class ="leftdotted">
                        <xsl:value-of select="@ProviderTuesdayStart"/> - <xsl:value-of select="@ProviderTuesdayEnd"/>
                      </td>
					  <td class ="leftdotted">
                        <xsl:value-of select="@ProviderWednesdayStart"/> - <xsl:value-of select="@ProviderWednesdayEnd"/>
                      </td>
					  <td class ="leftdotted">
                        <xsl:value-of select="@ProviderThursdayStart"/> - <xsl:value-of select="@ProviderThursdayEnd"/>
                      </td>
					  <td class ="leftdotted">
                        <xsl:value-of select="@ProviderFridayStart"/> - <xsl:value-of select="@ProviderFridayEnd"/>
                      </td>
					  <td class ="leftdotted">
                        <xsl:value-of select="@ProviderSaturdayStart"/> - <xsl:value-of select="@ProviderSaturdayEnd"/>
                      </td>
					  <td class ="leftdotted">
                        <xsl:value-of select="@ProviderSundayStart"/> - <xsl:value-of select="@ProviderSundayEnd"/>
                      </td>
					  <td class ="leftdotted"> 
					   <xsl:choose>
					    <xsl:when test="@ProviderTotalOfficeHours = ''">
					     -
					    </xsl:when>
					    <xsl:otherwise>
						 <xsl:value-of select="@ProviderTotalOfficeHours"/>
						</xsl:otherwise>
					   </xsl:choose>
                      </td>    
					  <td class ="leftdotted">
                        <xsl:value-of select="../Languages/Language/@Language"/>
                        <xsl:if test="../Languages/Language/@Spoken!='' or ../Languages/Language/@Written!=''">:</xsl:if>
                        <xsl:if test="../Languages/Language/@Spoken!=''"> Spoken=<xsl:value-of select="../Languages/Language/@Spoken"/></xsl:if>
                        <xsl:if test="../Languages/Language/@Written!=''"> Written=<xsl:value-of select="../Languages/Language/@Written"/></xsl:if>
                      </td>                         
                    </tr>
					
                  </xsl:for-each>
                </tbody>
              </table>
            </xsl:if>

            <!-- end: Mapped Providers Loop -->



            <!-- Languages Loop-->
            <xsl:variable name="LanguagesBody"  select="string(concat('LanguagesBody_','_nav'))"/>
            <xsl:if test="LanguageList/Language/@LanguageName !=''">
              <!-- Languages Table -->

              <br></br>
              <table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <!-- Attribute header -->
                  <td colspan="2" style="padding-bottom: 3px">
                    <b class="detailsheader">
                      <a id='{$LanguagesBody}_caption' class="navigate" title="Collapse/Expand Language Info" onclick="toggleItem('{$LanguagesBody}')">-</a>
                      Languages
                    </b>
                  </td>

                </tr>

                <!-- Collapse / Expand area for Languages-->



                <tbody id="{$LanguagesBody}">
                  <tr style="text-align: center;">
                    <td class="bottomdouble">
                      Name
                    </td>
                    <td class="bottomdouble">
                      Use
                    </td>
                    <td class="bottomdouble">
                      Spoken
                    </td>
                    <td class="bottomdouble">
                      Written
                    </td>
                    <td class="bottomdouble">
                      Level Of Proficiency
                    </td>                    
                  </tr>



                  <xsl:for-each select="LanguageList/Language">

                    <xsl:variable name="HighlightColor">
                      <xsl:if test="position() mod 2 = 1">
                        <text>alternating1</text>
                      </xsl:if>
                      <xsl:if test="position() mod 2 = 0">
                        <text>alternating2</text>
                      </xsl:if>
                    </xsl:variable>

                    <tr class="{$HighlightColor}" style="text-align:center">
                      <td>
                        <xsl:value-of select="@LanguageName"/>
                      </td>
                      <td>
                        <xsl:value-of select="@LanguageUse"/>
                      </td>
                      <td>
                        <xsl:value-of select="@Spoken"/>
                      </td>
                      <td>
                        <xsl:value-of select="@Written"/>
                      </td>
                      <td>
                        <xsl:value-of select="@LevelOfProficiency"/>
                      </td>
                    </tr>

                  </xsl:for-each>
                </tbody>
              </table>
            </xsl:if>
            <!-- end: Languages Loop -->

            <!-- Fulfillments Loop-->
            <xsl:variable name="FulfillmentsBody"  select="string(concat('FulfillmentsBody_','_nav'))"/>
            <xsl:if test="FulfillmentList/Fulfillment/@DocumentTypeName !=''">
              <!-- Fulfillments Table -->

              <br></br>
              <table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <!-- Attribute header -->
                  <td colspan="2" style="padding-bottom: 3px">
                    <b class="detailsheader">
                      <a id='{$FulfillmentsBody}_caption' class="navigate" title="Collapse/Expand Fulfillment Info" onclick="toggleItem('{$FulfillmentsBody}')">-</a>
                      Fulfillments
                    </b>
                  </td>

                </tr>

                <!-- Collapse / Expand area for Fulfillments-->



                <tbody id="{$FulfillmentsBody}">
                  <tr style="text-align: center;">
                    <td class="bottomdouble">
                      Document Type
                    </td>
                    <td class="bottomdouble">
                      Method
                    </td>
                    <td class="bottomdouble">
                      Fax Number
                    </td>
                  </tr>



                  <xsl:for-each select="FulfillmentList/Fulfillment">

                    <xsl:variable name="HighlightColor">
                      <xsl:if test="position() mod 2 = 1">
                        <text>alternating1</text>
                      </xsl:if>
                      <xsl:if test="position() mod 2 = 0">
                        <text>alternating2</text>
                      </xsl:if>
                    </xsl:variable>

                    <tr class="{$HighlightColor}" style="text-align:center">
                      <td>
                        <xsl:value-of select="@DocumentTypeName"/>
                      </td>
                      <td>
                        <xsl:value-of select="@MethodName"/>
                      </td>
                     <td>
						<xsl:if test="@FaxNumber ">
							<xsl:value-of select="concat('(',substring(@FaxNumber,1,3),') ',substring(@FaxNumber,4,3),'-',substring(@FaxNumber,7,4))"/>
						</xsl:if>
                     </td>
                   </tr>

                  </xsl:for-each>
                </tbody>
              </table>
            </xsl:if>
            <!-- end: Fulfillments Loop -->


            <!-- Additional Services Loop-->
            <xsl:variable name="AdditionalServicesBody"  select="string(concat('AdditionalServicesBody_','_nav'))"/>

            <!-- Additional Services Table -->
            <xsl:if test="ServiceList/AdditionalService/@ServiceName!=''">
              <br></br>
              <table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <!-- Attribute header -->
                  <td colspan="2" style="padding-bottom: 3px">
                    <b class="detailsheader">
                      <a id='{$AdditionalServicesBody}_caption' class="navigate" title="Collapse/Expand Language Info" onclick="toggleItem('{$AdditionalServicesBody}')">-</a>
                      Additional Services
                    </b>
                  </td>

                </tr>

                <!-- Collapse / Expand area for Additional Services-->
			


                <tbody id="{$AdditionalServicesBody}">
                  <tr style="text-align: center;">
                    <td class="bottomdouble">
						Service Name
                    </td>
					<td class="bottomdouble">
						Effective From - To
                    </td>
                  </tr>

                  <xsl:for-each select="ServiceList/AdditionalService">

                    <xsl:variable name="HighlightColor">
                      <xsl:if test="position() mod 2 = 1">
                        <text>alternating1</text>
                      </xsl:if>
                      <xsl:if test="position() mod 2 = 0">
                        <text>alternating2</text>
                      </xsl:if>
                    </xsl:variable>

                    <tr class="{$HighlightColor}" style="text-align:center">
					  <td>
                        <xsl:value-of select="@ServiceName"/>
                      </td>
                      <td nowrap="true" class = "leftdotted">
						<xsl:value-of select="@ServiceEffectiveDate"/> - <xsl:value-of select="@ServiceExpirationDate"/>
					  </td>
                    </tr>
					
                  </xsl:for-each>
                </tbody>
              </table>
            </xsl:if>
            <!-- end: Additional Services Loop -->





            <!-- Custom Attributes Loop-->
            <xsl:variable name="CustomAttributesBody"  select="string(concat('CustomAttributesBody_','_nav'))"/>

            <!-- Custom Attributes Table -->
            <xsl:if test="CustomAttributes/CustomAttribute/@AttributeName!=''">
              <br></br>
              <table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <!-- Attribute header -->
                  <td colspan="2" style="padding-bottom: 3px">
                    <b class="detailsheader">
                      <a id='{$CustomAttributesBody}_caption' class="navigate" title="Collapse/Expand Custom Attributes" onclick="toggleItem('{$CustomAttributesBody}')">-</a>
                      Custom Attributes
                    </b>
                  </td>

                </tr>

                <!-- Collapse / Expand area for Additional Services-->



                <tbody id="{$CustomAttributesBody}">
                  <tr style="text-align: center;">
                    <td class="bottomdouble">
                      Attribute Name
                    </td>
                    <td class="bottomdouble">
                      Attribute Value
                    </td>

                  </tr>



                  <xsl:for-each select="CustomAttributes/CustomAttribute">

                    <xsl:variable name="HighlightColor">
                      <xsl:if test="position() mod 2 = 1">
                        <text>alternating1</text>
                      </xsl:if>
                      <xsl:if test="position() mod 2 = 0">
                        <text>alternating2</text>
                      </xsl:if>
                    </xsl:variable>

                    <tr class="{$HighlightColor}" style="text-align:center">
                      <td>
                        <xsl:value-of select="@AttributeName"/>
                      </td>
                      <td>
                        <xsl:value-of select="@AttributeValue"/>
                      </td>
                    </tr>

                  </xsl:for-each>
                </tbody>
              </table>
            </xsl:if>
            <!-- end: Custom Attributes Loop -->




            <!-- Additional Contacts Loop-->
            <xsl:variable name="AdditionalContactsBody"  select="string(concat('AdditionalContactsBody_','_nav'))"/>
            <xsl:if test="AdditionalContactList/AdditionalContact/@Name !=''">
              <!-- Additional Contacts Table -->

              <br></br>
              <table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
                <tr>
                  <!-- Attribute header -->
                  <td colspan="2" style="padding-bottom: 3px">
                    <b class="detailsheader">
                      <a id='{$AdditionalContactsBody}_caption' class="navigate" title="Collapse/Expand Additional Contact Info" onclick="toggleItem('{$AdditionalContactsBody}')">-</a>
                      Additional Contacts 
                    </b>
                  </td>

                </tr>

                <!-- Collapse / Expand area for Additional Contacts-->



                <tbody id="{$AdditionalContactsBody}">
                  <tr style="text-align: center;">
                    <td class="bottomdouble">
                      Name
                    </td>
                    <td class="bottomdouble">
                      Class
                    </td>
                    <td class="bottomdouble">
                      Subclass
                    </td>
                    <td class="bottomdouble">
                      Title
                    </td>
                    <td class="bottomdouble">
                      Department
                    </td>       
                    <td class="bottomdouble">
                      Phone
                    </td> 
		    <td class="bottomdouble">
                      Extension
                    </td> 
                       <td class="bottomdouble">
                      Fax
                    </td>  
                    <td class="bottomdouble">
                      Email
                    </td>  
		     <td class="bottomdouble">
                      Description
                    </td> 
                    <td class="bottomdouble">
                      EffectiveDate
                    </td>        
                    <td class="bottomdouble">
                      ExpirationDate
                    </td>  
                  </tr>



                  <xsl:for-each select="AdditionalContactList/AdditionalContact">

                    <xsl:variable name="HighlightColor">
                      <xsl:if test="position() mod 2 = 1">
                        <text>alternating1</text>
                      </xsl:if>
                      <xsl:if test="position() mod 2 = 0">
                        <text>alternating2</text>
                      </xsl:if>
                    </xsl:variable>

                    <tr class="{$HighlightColor}" style="text-align:center">
                      <td>
                        <xsl:value-of select="@Name"/>
                      </td>
                      <td>
                        <xsl:value-of select="@ClassName"/>
                      </td>
                      <td>
                        <xsl:value-of select="@SubclassName"/>
                      </td>
                      <td>
                        <xsl:value-of select="@TitleName"/>
                      </td>
                      <td>
                        <xsl:value-of select="@DepartmentName"/>
                      </td>
                   	<td>
                        <xsl:value-of select="@Phone"/>
                      </td>
		     <td>
                        <xsl:value-of select="@Extension"/>
                      </td>
                   	<td>
                        <xsl:value-of select="@Fax"/>
                      </td>
                   	<td>
                        <xsl:value-of select="@Email"/>
                      </td>
                  	<td>
                        <xsl:value-of select="@Description"/>
                      </td>
                   	<td>
                        <xsl:value-of select="@EffectiveDate"/>
                      </td>
                   	<td>
                        <xsl:value-of select="@ExpirationDate"/>
                      </td>
                    </tr>

                  </xsl:for-each>
                </tbody>
              </table>
            </xsl:if>
            <!-- end: Additional ContactsLoop -->



          </table>
        </xsl:for-each>



        <xsl:call-template name="footer"/>

      </body>
    </html>

  </xsl:template>

  <xsl:template name="InfoRow">
    <xsl:param name="Header" />
    <xsl:param name="Value" />
    <xsl:param name="Position" />

    <xsl:variable name="HighlightColor">
      <xsl:if test="$Position mod 2 = 1">
        <text>alternating1</text>
      </xsl:if>
      <xsl:if test="$Position mod 2 = 0">
        <text>alternating2</text>
      </xsl:if>
    </xsl:variable>


    <tr class="{$HighlightColor}">
      <td align="right" width="10%" nowrap="true">
        <b>
          <xsl:value-of select="$Header" />:
        </b>
      </td>
      <td>
        <xsl:value-of select="$Value" />
      </td>
    </tr>


  </xsl:template>
  
  
  
</xsl:stylesheet>





 

      
    













        

