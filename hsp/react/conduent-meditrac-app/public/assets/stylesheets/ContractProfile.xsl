<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:html="http://www.w3.org/TR/html4/" exclude-result-prefixes="html"
        xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:user="urn:my-scripts">

  <xsl:output method="html" indent="yes"
				doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
				doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
				/>
  
  <xsl:variable name="donePath" select="concat(substring-before(/ContractProfile/Contract/ContractInfo/HSPStandardInfo/DonePath,'DONE'),'Style Sheets\')" />
  <xsl:include href="Header.xsl"/>
  <xsl:include href="Footer.xsl"/>


  <xsl:template match="/ContractProfile/Contract/ContractInfo">

    <html>
      <head>
        <title>
          HSP Contract Profile for: <xsl:value-of select="@ContractInfo/ContractName" />
        </title>

        <base href="{$donePath}" />
        <link rel="stylesheet" type="text/css" href="styles.css"/>
        <xsl:text disable-output-escaping="yes">

        <![CDATA[

        <script language="javascript">



          function hideAllFeeSched()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

                

		            for (i=0;i < locl.length;i++)
		            {
			            if (locl[i].id.indexOf('FeeScheduleMap_') > -1)
				      {
                
					      locl[i].style.display = 'none';
					      toggleCaption(locl[i].id + '_caption','+')
				      }
		            }

		            return false;
		         }

          function showAllFeeSched()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

		            for (i=0;i<locl.length;i++)
		            {
			            if (locl[i].id.indexOf('FeeScheduleMap_') > -1)
				      {
					      locl[i].style.display = '';
					      toggleCaption(locl[i].id + '_caption','-')
				      }
		            }

		            return false;
		         }

         
		      function hideAll()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

		            for (i=0;i < locl.length;i++)
		            {
			            if (locl[i].id.indexOf('_nav') > -1)
				      {
					      locl[i].style.display = 'none';
					      toggleCaption(locl[i].id + '_caption','+')
				      }
		            }

		            return false;
		         }

		         function showAll()
		         {
      			
		            locl = document.getElementsByTagName('tbody');

		            for (i=0;i<locl.length;i++)
		            {
			            if (locl[i].id.indexOf('_nav') > -1)
				      {
					      locl[i].style.display = '';
					      toggleCaption(locl[i].id + '_caption','-')
				      }
		            }

		            return false;
		         }

		          function getItem(id)
		          {
			      var itm = false;
			      if(document.getElementById)
			          itm = document.getElementById(id);
			      else if(document.all)
			          itm = document.all[id];
			      else if(document.layers)
			          itm = document.layers[id];

			      return itm;
		          }

		          function toggleItem(id)
		          {
			      itm = getItem(id);

			      if(!itm)
			          return false;

			      if(itm.style.display == 'none')
			      {
			          itm.style.display = '';
			          toggleCaption(id + '_caption','-')
			      }
			      else
			      {
			          itm.style.display = 'none';
			          toggleCaption(id + '_caption','+')
			      }

			      return false;
		          }

		          function toggleCaption(id, v)
		          {
			      itm = getItem(id);

			      if(!itm)
			          return false;
      			
			      itm.innerHTML = v;

			      return false;
		          }
              
      function Hello(a)
      {
          alert(a);
          return false;
      }

      function ExpandScientific(numToExpand, decPlaces)
      {
     
        if(isNaN(numToExpand) || numToExpand == '')
        {
          return '';
        }
        else
        {
          return parseFloat(numToExpand).toFixed(decPlaces);
        }

      }

              
      </script>
  
      ]]>

      </xsl:text>

      </head>

      <body onload="hideAll();">
        <xsl:call-template name="header"/>

          <!-- Main Table-->
          <table>
            <tr>
              <!-- Header table-->
              <table width ="100%">
                <tr>
                  <!-- Contract Info-->
                  <td width="50%" class="singleborder">
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="bottomdotted" colspan="2">
                          <b class="header">Contract Info</b>
                        </td>
                      </tr>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Name'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@ContractName"/>
                        <xsl:with-param name="Position" select="'1'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Contract #'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@ContractNumber"/>
                        <xsl:with-param name="Position" select="'2'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Contract Type'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@ContractType"/>
                        <xsl:with-param name="Position" select="'3'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Effective Date'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@EffectiveDate"/>
                        <xsl:with-param name="Position" select="'4'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Expiration Date'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@ExpirationDate"/>
                        <xsl:with-param name="Position" select="'5'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Description'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@ContractDescription"/>
                        <xsl:with-param name="Position" select="'6'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Notes'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@ContractNotes"/>
                        <xsl:with-param name="Position" select="'7'"/>
                      </xsl:call-template>

                      <xsl:call-template name="InfoRow">
                        <xsl:with-param name="Header" select="'Pricing Mode'"/>
                        <xsl:with-param name="Value" select="ContractInfo/@PricingModeName"/>
                        <xsl:with-param name="Position" select="'8'"/>
                      </xsl:call-template>

                    </table>
                  </td>
                  <!-- Denial Categories-->
                  <td width="50%" class="singleborder">
                    <table width="100%">
                      <tr>
                        <td class="bottomdotted" width="30%">
                          <b class="header">Denial Categories</b>
                        </td>
                        <td class="bottomdotted" >
                          Denial Category
                        </td>
                        <td class="bottomdotted">
                          Denial Action
                        </td>
                      </tr>
                      <xsl:for-each select="/ContractProfile/Contract/DenialCategories/DenialCategory">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Denial Category'"/>
                            <xsl:with-param name="Value1" select="@DenialCategoryName"/>
                            <xsl:with-param name="Value2" select="@DenialActionName"/>
                            <xsl:with-param name="Position" select="position()"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:for-each>
                    </table>
                  </td>

                </tr>
              </table>
            </tr>
          </table>


          <!-- Navigation Bar -->
        <center>
          <table>
            <tr style="border: 0px;">
              <td>
                <a href="# " class="hover" title="Collapse All" onclick="hideAll();return false;"> [ Collapse All ]</a>
              </td>
              <td>
                <a href="#" class="hover" title="Expand All" onclick="showAll();return false;"> [ Expand All ]</a>
              </td>
              <td>
                <a href="# " class="hover" title="Collapse Fee Schedule Info" onclick="hideAllFeeSched();return false;"> [ Collapse Fee Schedule Info ]</a>
              </td>
              <td>
                <a href="#" class="hover" title="Expand Fee Schedule Info" onclick="showAllFeeSched();return false;"> [ Expand Fee Schedule Info ]</a>
              </td>
            </tr>
          </table>
        </center>

          <!-- Attribute Loop-->
          <xsl:variable name="CABody"  select="concat('CustomAttributes_','_nav')"/>

          <!-- Attribute Table -->
          <xsl:if test="count(/ContractProfile/Contract/CustomAttributes/CustomAttributes) > 0">
            <br></br>
            <table width="100%" class="singleborder1" cellpadding="0" cellspacing="0">
              <tr>
                <!-- Attribute header -->
                <td colspan="2" style="padding-bottom: 3px">
                  <b class="detailsheader">
                    <a id='{$CABody}_caption' class="navigate" title="Collapse/Expand Attribute Info" onclick="toggleItem('{$CABody}')">-</a>
                    Custom Attributes
                  </b>
                </td>

              </tr>

              <!-- Collapse / Expand area for Custom Attributes-->
              <tbody id="{$CABody}">
                <xsl:for-each select="/ContractProfile/Contract/CustomAttributes/CustomAttributes">
                  <xsl:call-template name="InfoRow">
                    <xsl:with-param name="Header" select="@AttributeName"/>
                    <xsl:with-param name="Value" select="@AttributeValue"/>
                    <xsl:with-param name="Position" select="position()"/>
                  </xsl:call-template>
                </xsl:for-each>
              </tbody>
            </table>
          </xsl:if>
          <!-- end: Attribute Loop -->

          <!-- Attribute Loop-->
          <xsl:variable name="RMBBody"  select="concat('Reimbursements_','_nav')"/>

          <!-- Attribute Table -->
          <xsl:if test="count(/ContractProfile/Contract/Reimbursements/Reimbursement) > 0">
            <br></br>
            <table width="100%" class="singleborder2" cellpadding="0" cellspacing="0">
              <tr>
                <!-- Attribute header -->
                <td colspan="2" style="padding-bottom: 3px">
                  <b class="detailsheader">
                    <a id='{$RMBBody}_caption' class="navigate" title="Collapse/Expand Reimbursements" onclick="toggleItem('{$RMBBody}')">-</a>
                    Reimbursements
                  </b>
                </td>

              </tr>
              
              <!-- Collapse / Expand area for Reimbursements-->
              <tbody id="{$RMBBody}">
                <tr>
                  <td class="bottomdouble">Name</td>
                  <td class="bottomdouble">Description</td>
                  <td class="bottomdouble">Who To Pay</td>
                  <td class="bottomdouble">Fee Schedule</td>
                  <td class="bottomdouble">Effective Date</td>
                  <td class="bottomdouble">Expiration Date</td>
                </tr>
                <xsl:for-each select="/ContractProfile/Contract/Reimbursements/Reimbursement">
                  
                  <xsl:variable name="HighlightColor">
                    <xsl:if test="position() mod 2 = 1">
                      <text>alternating1</text>
                    </xsl:if>
                    <xsl:if test="position() mod 2 = 0">
                      <text>alternating2</text>
                    </xsl:if>
                  </xsl:variable>

                  <tr class="{$HighlightColor}">
                    <td>
                      <xsl:value-of select="@ReimbursementName" />
                    </td>
                    <td>
                      <xsl:value-of select="@ReimbursementDescription" />
                    </td>
                    <td>
                      <xsl:value-of select="@WhoToPayName" />
                    </td>
                    <td>
                      <xsl:value-of select="@FeeScheduleName" />
                    </td>
                    <td>
                      <xsl:value-of select="@EffectiveDate" />
                    </td>
                    <td>
                      <xsl:value-of select="@ExpirationDate" />
                    </td>

                  </tr>
                </xsl:for-each>
              </tbody>
            </table>
          </xsl:if>
          <!-- end: Attribute Loop -->


          <!-- Fee Schedule Mappings Loop-->
          <xsl:for-each select="/ContractProfile/Contract/FeeScheduleMaps/FeeScheduleMap/FeeScheduleContractMap">
            <br></br>
            <xsl:variable name="TBody"  select="concat('FeeScheduleMap_',@ContractFeeScheduleMapID,'_nav')"/>

            <!-- Fee Schedule Mapping Table -->
            <table width="100%" class="detailsborder">
              <tr>
                <!-- Primary Fee Schedule Name -->
                <td width="50%">
                  <b class="detailsheader">
                    <a id='{$TBody}_caption' class="navigate" title="Collapse/Expand Fee Schedule Info" onclick="toggleItem('{$TBody}')">-</a>
                    <xsl:value-of select="@FeeScheduleName"/>
                    <br></br>
                    <br></br>
                  </b>
                  Primary Fee Schedule Description: 
                  <xsl:value-of select="@FeeScheduleDescription"/>
                  <xsl:if test="@FeeScheduleMultiplier != 0">
                    <br />
                    Primary Fee Schedule Multiplier:
                    <xsl:value-of select="@FeeScheduleMultiplier"/>
                  </xsl:if>
                </td>

                <!-- Mapping Effective Dates and Claim Category -->
                <td width="50%">
                    <li></li>
                    Effective Date: <xsl:value-of select="@EffectiveDate"/>
                    <li></li>
                    Expiration Date: <xsl:value-of select="@ExpirationDate"/>
                    <li></li>
                    Claim Category: <xsl:value-of select="@ClaimCategory"/>
                </td>

              </tr>

              <!-- Collapse / Expand area for Fee Schedule Mappings -->
              <tbody id="{$TBody}">
                <tr>
                  <!-- Mapping Details -->
                  <td width="50%" class="singleborder">
                    <table width="100%"  >
                      <tr>
                        <td class="bottomdotted">
                          <b class="header">Details</b>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table cellpadding ="0" cellspacing="0" align="top">
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Precedence'"/>
                              <xsl:with-param name="Value" select="@Precedence"/>
                              <xsl:with-param name="Position" select="'1'"/>
                            </xsl:call-template>
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Region'"/>
                              <xsl:with-param name="Value" select="@RegionName"/>
                              <xsl:with-param name="Position" select="'2'"/>
                            </xsl:call-template>
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Line Of Business'"/>
                              <xsl:with-param name="Value" select="@LineOfBusinessName"/>
                              <xsl:with-param name="Position" select="'3'"/>
                            </xsl:call-template>
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Place Of Service'"/>
                              <xsl:with-param name="Value" select="@PlaceOfServiceName"/>
                              <xsl:with-param name="Position" select="'4'"/>
                            </xsl:call-template>
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Action Code'"/>
                              <xsl:with-param name="Value" select="@ActionCodeName"/>
                              <xsl:with-param name="Position" select="'5'"/>
                            </xsl:call-template>
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Provider/Office Global Fee'"/>
                              <xsl:with-param name="Value" select="@GlobalFeeProviderOfficeName"/>
                              <xsl:with-param name="Position" select="'6'"/>
                            </xsl:call-template>
                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Modifier Reduction Schedule'"/>
                              <xsl:with-param name="Value" select="@ModifierReductionScheduleName"/>
                              <xsl:with-param name="Position" select="'7'"/>
                            </xsl:call-template>
                            <!-- Determine the string to show since fields are conditional -->
                            
                            <xsl:variable name="ProcNotFoundParam">
                              <xsl:if test="@ActionForProcedureNotFound = 'Pend'">
                                <text>Pend</text>
                              </xsl:if>
                              <xsl:if test="@ActionForProcedureNotFound = 'Deny'">
                                <text>Deny</text>
                              </xsl:if>
                              <xsl:if test="@ActionForProcedureNotFound = 'Pay Percent of Total Charges'">
                                <text>
                                  <xsl:value-of select="concat('Pay ', @PayPercentOfTotalCharges, '% Of Total Charges')"/>
                                </text>
                              </xsl:if>
                           
                            </xsl:variable>

                            <xsl:call-template name="InfoRow">
                              <xsl:with-param name="Header" select="'Procedure Not Found Action'"/>
                              <xsl:with-param name="Value" select="$ProcNotFoundParam"/>
                              <xsl:with-param name="Position" select="'8'"/>
                            </xsl:call-template>

                            <xsl:if test="$ProcNotFoundParam != 'Pend'">
                              <xsl:call-template name="InfoRow">
                                <xsl:with-param name="Header" select="'Procedure Not Found Explanation'"/>
                                <xsl:with-param name="Value" select="@DenialExplanationAbbreviation"/>
                                <xsl:with-param name="Position" select="'8'"/>
                              </xsl:call-template>
                            </xsl:if>

                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <!-- Fee Schedules -->
                  <td width="50%" class="singleborder">
                    <table width="100%">
                      <tr>
                        <td class="bottomdotted">
                          <b class="header">Secondary Fee Schedules</b>
                        </td>
                        <td class="bottomdotted">
                          Schedule Name
                        </td>
                        <td class="bottomdotted">
                          Multiplier
                        </td>
                      </tr>

                      <xsl:if test="@SecondaryFeeSchedule1Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 1'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule1Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule1Percent"/>
                            <xsl:with-param name="Position" select="'1'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>

                      <xsl:if test="@SecondaryFeeSchedule2Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 2'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule2Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule2Percent"/>
                            <xsl:with-param name="Position" select="'2'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>

                      <xsl:if test="@SecondaryFeeSchedule3Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 3'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule3Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule3Percent"/>
                            <xsl:with-param name="Position" select="'3'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>

                      <xsl:if test="@SecondaryFeeSchedule4Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 4'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule4Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule4Percent"/>
                            <xsl:with-param name="Position" select="'4'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>

                      <xsl:if test="@SecondaryFeeSchedule5Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 5'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule5Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule5Percent"/>
                            <xsl:with-param name="Position" select="'5'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>
					  
					  <xsl:if test="@SecondaryFeeSchedule6Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 6'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule6Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule6Percent"/>
                            <xsl:with-param name="Position" select="'6'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>
					  
					  <xsl:if test="@SecondaryFeeSchedule7Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 7'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule7Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule7Percent"/>
                            <xsl:with-param name="Position" select="'7'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>
					  
					  <xsl:if test="@SecondaryFeeSchedule8Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 8'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule8Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule8Percent"/>
                            <xsl:with-param name="Position" select="'8'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>
					  
					  <xsl:if test="@SecondaryFeeSchedule9Name != ''">
                        <tr>
                          <xsl:call-template name="InfoRowDual">
                            <xsl:with-param name="Header" select="'Secondary Fee Schedule 9'"/>
                            <xsl:with-param name="Value1" select="@SecondaryFeeSchedule9Name"/>
                            <xsl:with-param name="Value2" select="@SecondaryFeeSchedule9Percent"/>
                            <xsl:with-param name="Position" select="'9'"/>
                          </xsl:call-template>
                        </tr>
                      </xsl:if>

                    </table>
                  </td>

                </tr>
                <tr>
                </tr>
              </tbody>

              <!-- ASG Details Area-->
              <xsl:variable name="ASGBody"  select="concat('FeeScheduleMapASG_',@ContractFeeScheduleMapID,'_nav')"/>
              <tr>
                <td colspan="4">
                  <!-- Check For ASG Details Existance -->
                  <xsl:if test="@FeeSchedulePricing = 'ASG'">

                    <table width="100%" class="singleborder3" cellpadding="0" cellspacing="0">
                      <tr>
                        <!-- ASG header -->
                        <td colspan="2" style="padding-bottom: 3px">
                          <b class="detailsheader">
                            <a id='{$ASGBody}_caption' class="navigate" title="Collapse/Expand ASG Details" onclick="toggleItem('{$ASGBody}')">-</a>
                            Ambulatory Surgery Details
                          </b>
                        </td>

                      </tr>

                      <!-- Collapse / Expand area for Contracts-->
                      <tbody id="{$ASGBody}">
                        <tr align="center">
                          <td class="bottomdouble" >Surgical Payment Method</td>
                          <td class="bottomdouble" >Surgical Procedure Ranking</td>
                          <td class="bottomdouble">Surgical Reduction Type</td>
                          <td class="bottomdouble">Reduction1 %</td>
                          <td class="bottomdouble">Reduction2 %</td>
                          <td class="bottomdouble">Reduction3 %</td>
                          <td class="bottomdouble">Reduction4 %</td>
                          <td class="bottomdouble">Reduction5 %</td>
                          <td class="bottomdouble">Reduction6 %</td>
                        </tr>
                        
                          <tr style="background:#FFFFFF;text-align:center">
                            <td>
                              <xsl:value-of select="@SurgicalPaymentMethod"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalProcedureRankingMethod"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReductionTypeName"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReduction1"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReduction2"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReduction3"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReduction4"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReduction5"/>
                            </td>
                            <td>
                              <xsl:value-of select="@SurgicalReductionRemaining"/>
                            </td>
                          </tr>
                      </tbody>
                    </table>
                  </xsl:if>
                  <!-- end: Attribute Loop -->
                </td>
              </tr>

            </table>
          </xsl:for-each>
          <!-- end: for each Fee Schedule Map -->

          <!-- Attribute Loop-->
          <xsl:variable name="CRBody"  select="concat('CapitationRate','_nav')"/>

          <!-- Attribute Table -->
          <xsl:if test="/ContractProfile/Contract/ContractInfo/ContractInfo/@CapitationRateName != ''">
            <br></br>
            <table width="100%" class="singleborder3" cellpadding="0" cellspacing="0">
              <tr>
                <!-- Attribute header -->
                <td colspan="2" style="padding-bottom: 3px">
                  <b class="detailsheader">
                    <a id='{$CRBody}_caption' class="navigate" title="Collapse/Expand Capitation Rate Info" onclick="toggleItem('{$CRBody}')">-</a>
                    Capitation Rates
                  </b>
                </td>

              </tr>

              <!-- Collapse / Expand area for Capitation Rates-->
              <tbody id="{$CRBody}">
                <tr>
                  <td class="bottomdouble">Name</td>
                  <td class="bottomdouble">Description</td>
                  <td class="bottomdouble">Last Posted Date</td>
                  <td class="bottomdouble">Retro Add Limit</td>
                  <td class="bottomdouble">Retro Term Limit</td>
                  <td class="bottomdouble">Posting Cutoff Day</td>
                </tr>
                <tr>
                  <td class="gray">
                    <xsl:value-of select="/ContractProfile/Contract/ContractInfo/ContractInfo/@CapitationRateName" />
                  </td>
                  <td class="gray">
                    <xsl:value-of select="/ContractProfile/Contract/ContractInfo/ContractInfo/@CapitationRateDescription" />
                  </td>
                  <td class="gray">
                    <xsl:value-of select="/ContractProfile/Contract/ContractInfo/ContractInfo/@LastPostedDate" />
                  </td>
                  <td class="gray">
                    <xsl:value-of select="/ContractProfile/Contract/ContractInfo/ContractInfo/@RetroactiveAddLimit" />
                  </td>
                  <td class="gray">
                    <xsl:value-of select="/ContractProfile/Contract/ContractInfo/ContractInfo/@RetroactiveTermLimit" />
                  </td>
                  <td class="gray">
                    <xsl:value-of select="/ContractProfile/Contract/ContractInfo/ContractInfo/@PostingCutoffDay" />
                  </td>
                </tr>
              </tbody>
            </table>
          </xsl:if>
          <!-- end: Attribute Loop -->
          
        <xsl:call-template name="footer"/>
      
      </body>
    </html>


  </xsl:template>



  <xsl:template name="InfoRow">
    <xsl:param name="Header" />
    <xsl:param name="Value" />
    <xsl:param name="Position" />

    <xsl:variable name="HighlightColor">
      <xsl:if test="$Position mod 2 = 1">
        <text>alternating1</text>
      </xsl:if>
      <xsl:if test="$Position mod 2 = 0">
        <text>alternating2</text>
      </xsl:if>
    </xsl:variable>


    <tr class="{$HighlightColor}">
      <td align="right" width="10%" nowrap="true">
        <b>
          <xsl:value-of select="$Header" />:
        </b>
      </td>
      <td class="leftdotted">
        <xsl:value-of select="$Value" />
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template name="InfoRowDual">
    <xsl:param name="Header" />
    <xsl:param name="Value1" />
    <xsl:param name="Value2" />
    <xsl:param name="Position" />

    <xsl:variable name="HighlightColor">
      <xsl:if test="$Position mod 2 = 1">
        <text>alternating1</text>
      </xsl:if>
      <xsl:if test="$Position mod 2 = 0">
        <text>alternating2</text>
      </xsl:if>
    </xsl:variable>


    <tr class="{$HighlightColor}">
      <td align="right" width="10%" nowrap="true">
        <b>
          <xsl:value-of select="$Header" />:
        </b>
      </td>
      <td class="leftdotted">
        <xsl:value-of select="$Value1" />
      </td>
      <td class="leftdotted">
        <xsl:value-of select="$Value2" />
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template name="FormatDate">

    <xsl:param name="DateTime" />
    <xsl:variable name="mo">
      <xsl:value-of select="substring($DateTime,6,2)" />
    </xsl:variable>
    <xsl:variable name="day">
      <xsl:value-of select="substring($DateTime,9,2)" />
    </xsl:variable>
    <xsl:variable name="year">
      <xsl:value-of select="substring($DateTime,1,4)" />
    </xsl:variable>
    <xsl:if test="$DateTime != ''">
      <xsl:value-of select="$mo"/>/<xsl:value-of select="$day"/>/<xsl:value-of select="$year"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
