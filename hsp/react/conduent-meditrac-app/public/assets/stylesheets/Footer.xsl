<?xml version="1.0" encoding="utf-8"?>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:html="http://www.w3.org/TR/html4/" exclude-result-prefixes="html"
				xmlns:msxsl="urn:schemas-microsoft-com:xslt"
				xmlns:user="urn:my-scripts">
    <!--FOOTER-->
    <!--Modify ONLY the HTML within the following <xsl:template> to change the FOOTER for each XSL Stylesheet.-->
    <xsl:template name="footer">
      <table width="100%">
	<tr><td><br/><br/><hr/></td></tr>
        <tr>
          <td>
            <center><h6>Conduent</h6></center>
          </td>
        </tr>
      </table>
    </xsl:template>
    <!--END HEADER-->
</xsl:stylesheet>
