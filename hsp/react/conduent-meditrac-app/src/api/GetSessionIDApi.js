import axios from "axios";
import apiList from "./apiList";
export const getSessionID = paramsObj => {
  // Orchestration layer connect call to get sessionID of the connected user
  const url = `${apiList.orchestrationHost}${apiList.getSessionIDAPI}`;
  return axios.post(url, paramsObj);
};
