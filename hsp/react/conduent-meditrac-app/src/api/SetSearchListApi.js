import axios from "axios";
import apiList from "./apiList";
export const setSearchData = paramsObj => {
  // Orchestration layer connect call to set searched item in last search list
  const url = `${apiList.orchestrationHost}${apiList.setSearchListAPI}`;
  return axios.post(url, paramsObj);
};
