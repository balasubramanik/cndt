import axios from "axios";
import apiList from "./apiList";
export const getControls = paramsObj => {
  // Orchestration layer connect call for getControls
  const url = `${apiList.orchestrationHost}${apiList.getControlsAPI}`;
  return axios.post(url, paramsObj);
};
