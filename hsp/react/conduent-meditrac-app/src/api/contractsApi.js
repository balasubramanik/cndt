import axios from "axios";
import apiList from "./apiList";
export const findContracts = paramsObj => {
  // Orchestration layer connect call for findContracts
  const url = `${apiList.orchestrationHost}${apiList.findContractsAPI}`;
  return axios.post(url, paramsObj);
};
