import axios from "axios";
import apiList from "./apiList";
export const getPermission = paramsObj => {
    // Orchestration layer connect get permission

    const url = `${apiList.orchestrationHost}${apiList.getPermission}`;
    return axios.post(url, paramsObj);
};
