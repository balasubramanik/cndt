import axios from "axios";
import apiList from "./apiList";
export const getDemographics = paramsObj => {
  // Orchestration layer connect call for get demographics like county and all
  const url = `${apiList.orchestrationHost}${apiList.getDemographicsAPI}`;
  return axios.post(url, paramsObj);
};
