import axios from "axios";
import apiList from "./apiList";
export const getDisConnectUser = paramsObj => {
  // Orchestration layer connect call to disconnect user
  const url = `${apiList.orchestrationHost}${apiList.getDisConnectUserAPI}`;
  return axios.post(url, paramsObj);
};
