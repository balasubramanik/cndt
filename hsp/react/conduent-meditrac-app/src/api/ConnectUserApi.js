import axios from "axios";
import apiList from "./apiList";
export const getConnectUser = paramsObj => {
  // Orchestration layer connect call for Connect User
  const url = `${apiList.orchestrationHost}${apiList.getConnectUserAPI}`;
  return axios.post(url, paramsObj);
};
