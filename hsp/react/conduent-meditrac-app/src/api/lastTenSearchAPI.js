import axios from "axios";
import apiList from "./apiList";
export const setLastTensearch = paramsObj => {
  // Orchestration layer connect set recent provider search

  const url = `${apiList.orchestrationHost}${apiList.setLastTen}`;
  return axios.post(url, paramsObj);
};
export const getLastTensearch = paramsObj => {
  // Orchestration layer connect get last recent provider search

  const url = `${apiList.orchestrationHost}${apiList.getLastTen}`;
  return axios.post(url, paramsObj);
};
