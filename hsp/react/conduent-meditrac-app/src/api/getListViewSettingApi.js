import axios from "axios";
import apiList from "./apiList";
export const getListViewSetting = paramsObj => {
  // Orchestration layer connect call for get reference codes
  const url = `${apiList.orchestrationHost}${apiList.getListViewSettingAPI}`;
  return axios.post(url, paramsObj);
};
