import axios from "axios";
import apiList from "./apiList";
export const getProfileSettings = paramsObj => {
  // Orchestration layer connect call to get profile settings
  const url = `${apiList.orchestrationHost}${apiList.getProfileSettingsAPI}`;
  return axios.post(url, paramsObj);
};
