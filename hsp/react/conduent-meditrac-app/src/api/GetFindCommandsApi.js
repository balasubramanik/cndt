import axios from "axios";
import apiList from "./apiList";
export const getFindCommands = () => {
  // Orchestration layer call for FindCommands
  const url = `${apiList.orchestrationHost}${apiList.getFindCommandsAPI}`;
  return axios.post(url);
};
