import axios from "axios";
import apiList from "./apiList";
export const getRefCodes = paramsObj => {
  // Orchestration layer connect call for get reference codes
  const url = `${apiList.orchestrationHost}${apiList.getRefCodesAPI}`;
  return axios.post(url, paramsObj);
};
