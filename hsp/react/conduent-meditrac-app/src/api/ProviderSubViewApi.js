/*
 *  Orchestration layer calls to get User Reports, StyleSheets and reference codes to display
 *  for XML Browser / Style Sheet
 *
 */
import axios from "axios";
import apiList from "./apiList";
export const fetchUserReportsStyles = paramsObj => {
  const url = `${apiList.orchestrationHost}${apiList.getUserReports}`;
  return axios.post(url, paramsObj);
};
export const fetchXMLBrowser = paramsObj => {
  const url = `${apiList.orchestrationHost}${apiList.getXMLBrowser}`;
  return axios.post(url, paramsObj);
};
export const fetchRefcodes = paramsObj => {
  const url = `${apiList.orchestrationHost}${apiList.getRefcodes}`;
  return axios.post(url, paramsObj);
};
