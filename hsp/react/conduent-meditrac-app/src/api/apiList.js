/* List of api used in the applications those are registered in orchestration layer */
const baseURL = process.env.PUBLIC_URL;
const LOCAL = {
  apiURLOrigin: "http://localhost:5000",
  SESSION_TIMEOUT: 300
};
const DEV = {
  apiURLOrigin: "https://ghs-hsp-dotnet-w-service01-demoslot.azurewebsites.net",
  SESSION_TIMEOUT: 300
};
const QA1 = {
  apiURLOrigin: "https://ghs-hsp-njs-w-service01-qaslot.azurewebsites.net",
  SESSION_TIMEOUT: 300
};
const QA2 = {
  apiURLOrigin: "https://ghs-hsp-njs-w-service01-qaslot.azurewebsites.net",
  SESSION_TIMEOUT: 300
};
const APIURLS = {
  getFindCommandsAPI: "/api/findCommands",
  getControlsAPI: "/api/findControls",
  findOfficesAPI: "/api/office",
  findContractsAPI: "/api/contract",
  findProfileAPI: "/api/profiles",
  getConnectUserAPI: "/api/connectUser",
  getDisConnectUserAPI: "/api/disConnectUser",
  getSessionIDAPI: "/api/getSessionID",
  getRefCodesAPI: "/api/getrefcodes",
  getProfileSettingsAPI: "/api/getlistviewprofilesettings",
  getDemographicsAPI: "/api/getdemographics",
  getRecentSearchAPI: "/api/getrecentsearch",
  setSearchListAPI: "/api/setrecentsearch",
  getUserReports: "/api/getuserreports",
  getXMLBrowser: "/api/getxmlstylesheet",
  setLastTen: "/api/setSearchData",
  getLastTen: "/api/getSearchData",
  getRefcodes: "/api/getreferencecodes",
  saveListViewSettingAPI: "/api/setUserListViewSetting",
  getListViewSettingAPI: "/api/getUserListViewSetting",
  userLoginAPI: "/api/connectUser",
  getPermission: "/api/getPermission",
  getCountries: "/api/getcountries"
};
let config;
if (process.env.REACT_APP_HSP === 'LOCAL') {
  config = LOCAL;
} else if (process.env.REACT_APP_HSP === 'DEV') {
  config = DEV;
}
else if (process.env.REACT_APP_HSP === 'QA1') {
  config = QA1;
}
else if (process.env.REACT_APP_HSP === 'QA2') {
  config = QA2;
}

export default {
  ...config,
  orchestrationHost: config.apiURLOrigin + baseURL,
  socketPath: baseURL + "/socket.io",
  baseURL: baseURL,
  ...APIURLS

};