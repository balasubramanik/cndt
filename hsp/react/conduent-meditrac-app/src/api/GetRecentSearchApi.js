import axios from "axios";
import apiList from "./apiList";
export const getRecentSearch = paramsObj => {
  // Orchestration layer connect call to get recent search details
  const url = `${apiList.orchestrationHost}${apiList.getRecentSearchAPI}`;
  return axios.post(url, paramsObj);
};
