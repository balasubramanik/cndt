import axios from "axios";
import apiList from "./apiList";
export const getCountries = paramsObj => {
  // Orchestration layer connect call to get sessionID of the connected user
  const url = `${apiList.orchestrationHost}${apiList.getCountries}`;
  return axios.post(url, paramsObj);
};