import axios from "axios";
import apiList from "./apiList";
export const saveListViewSetting = paramsObj => {
  // Orchestration layer connect call for get reference codes
  const url = `${apiList.orchestrationHost}${apiList.saveListViewSettingAPI}`;
  return axios.post(url, paramsObj);
};
