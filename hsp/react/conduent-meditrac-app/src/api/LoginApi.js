import axios from "axios";
import apiList from "./apiList";

export const userLogin = paramsObj => {
  // Orchestration layer connect call for findProfile
  const url = `${apiList.orchestrationHost}${apiList.getConnectUserAPI}`;
  return axios.post(url, paramsObj);
};

export const userDisconnect = paramsObj => {
  // Orchestration layer connect call for findProfile
  const url = `${apiList.orchestrationHost}${apiList.getDisConnectUserAPI}`;
  return axios.post(url, paramsObj);
};
