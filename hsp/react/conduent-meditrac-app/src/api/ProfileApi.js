import axios from "axios";
import apiList from "./apiList";
export const findProfile = paramsObj => {
  // Orchestration layer connect call for findProfile
  const url = `${apiList.orchestrationHost}${apiList.findProfileAPI}`;
  return axios.post(url, paramsObj);
};
