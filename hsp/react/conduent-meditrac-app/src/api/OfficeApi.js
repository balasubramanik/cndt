import axios from "axios";
import apiList from "./apiList";
export const findOffice = paramsObj => {
  // Orchestration layer connect call for findOffice
  const url = `${apiList.orchestrationHost}${apiList.findOfficesAPI}`;
  return axios.post(url, paramsObj);
};
