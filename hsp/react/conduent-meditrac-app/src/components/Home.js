/* Sample Home page after loggin or hitting meditrac URL */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import ACTION_TYPES from './../constants/ACTION_TYPES';
class Home extends Component {
  componentDidMount() {
    this.props.getFindCommandsData();
  }
  render() {
    return (
      <div className="homepage">
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    findCommands: state.findCommandsAction.findCommands
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getFindCommandsData: () => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FIND_COMMANDS
      });
    }
  };
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
