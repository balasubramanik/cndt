/**
 * User_story: US3_Provider_Office_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of a record After a particular
 * office is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import "./provider/provider.css";
import { ButtonBar } from "conduent-component-library";
import Office from "./provider/Office/Office";
import { withRouter } from "react-router";
import NavBar from "./provider/NavBar";
import { Button, ModalHeader, Modal, ModalBody, ModalFooter } from "reactstrap";
const routes = require("../../data/routes.json").OFFICE;
class ProviderSubContainerOffice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "",
      content: null,
      modalMessage: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modalMessage: !prevState.modalMessage
    }));
  }
  getRouteConfig(find, type) {
    const childRoutes = routes[0].children;
    let routeReturn;
    childRoutes.map((route, key) => {
      const { children } = route;
      children.map((childRoute, key) => {

        if (childRoute[find] === type) {
          routeReturn = childRoute;

        }
      });
    });
    return routeReturn;
  }
  onSelected(ID) {
    let targetURL = this.getRouteConfig('id', ID)?.link;
    if (!targetURL) {
      this.setState({ modalMessage: true });
      return;
    }
    var config = {
      pathname: `/officesubview/${this.state.content.officeID}/` + targetURL,
      state: { detail: this.state.content }
    };
    this.props.history.push(config);
  }

  componentDidMount() {
    const { ViewType } = this.props.match.params;
    let content = {};
    let ID = this.getRouteConfig('link', ViewType)?.id;
    const original = JSON.parse(localStorage.getItem("CurrentOffice"));
    if (
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.detail
    ) {
      content = this.props.location.state.detail;
      this.setState({ content: content, selected: ID });
      return;
    } else if (original) {
      content = original;
      this.setState({ content: content, selected: ID });
      return;
    }
    this.props.history.push(`/provider/Office`);
  }

  render() {
    if (this.state.content) {
      return (
        <div>
          <NavBar type="OfficeSearch" content={this.state.content} />
          <div>
            <div id="sidebar-wrapper">
              <ButtonBar
                onSelected={s => this.onSelected(s)}
                container={false}
                selected={this.state.selected}
                routes={routes}
              />
            </div>

            <div id="page-content-wrapper1">
              <Office
                content={this.state.content}
                selected={this.state.selected}
              />
            </div>
          </div>
          <Modal
            backdrop="static"
            isOpen={this.state.modalMessage}
            toggle={this.toggle}
            className="common-popup-size common-popup-position"
          >
            <ModalHeader className="common-popup-header">Meditrac</ModalHeader>
            <ModalBody>
              <div className="d-inline"><img src={process.env.PUBLIC_URL + "/assets/caution_icon.png"} /></div>
              <div className="d-inline common-popup-body">Out of scope</div>
            </ModalBody>
            <ModalFooter>
              <Button className="common-popup-button" onClick={this.toggle}>
                Ok
          </Button>
            </ModalFooter>
          </Modal>
        </div>
      );
    } else {
      return null;
    }
  }
}
export default withRouter(ProviderSubContainerOffice);