/*
 * Main Container Layout to include Find Contract and Dind Office with List View.
 * As User, when logged in and select Provider Office/Contract then this container
 * will load with Find Controller and ListView of selected provider type
 *
 */
import React, { Component } from "react";
import FindControlView from "../views/FindControlView";
import OfficeListView from "../views/OfficeListView";
import ContractListView from "../views/ContractListView";
import PageLevelToolbar from "../PageLevelToolbar";
import { connect } from "react-redux";
import ACTION_TYPES from "../../constants/ACTION_TYPES";
import { Constants } from "../../constants/VIEW_TYPES";

class FindContainer extends Component {
  constructor(props) {
    super(props);
    this.containerRef = React.createRef();
    this.state = {
      providerType: props.match.params.providerType,
      isNavExpanded: true,
      ListViewVisible: "",
      searchPerformed: false
    };
    this.toggleExpandNav = this.toggleExpandNav.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params.providerType !== this.props.match.params.providerType) {
      this.setState({ searchPerformed: false })
    }
  }

  toggleExpandNav() {
    this.setState({
      isNavExpanded: !this.state.isNavExpanded
    });
  }

  DisplayListView = isClear => {
    if (isClear === "false") {
      this.setState({ searchPerformed: false });
      this.props.clearContract();
      this.props.clearOffice();
    }
  };

  render() {
    const providerType = this.props.match.params.providerType;
    const { displayName } = this.props.findCommandsData.find(
      obj => obj.name === this.props.location.pathname.split("/").pop()
    );
    return (
      <React.Fragment>
        <div className="pageToolBar">
          <PageLevelToolbar
            type={
              providerType === Constants.CONTRACT ? Constants.CONTRACT_SEARCH : Constants.OFFICE_SEARCH
            }
            content=""
            providerType={providerType}
          />
        </div>
        <div className="container-fluid findContainer" ref={this.containerRef}>
          <div className="row">
            <div
              className={
                this.state.isNavExpanded ? "col-sm-3" : "col-sm-1 pr-0"
              }
            >
              <FindControlView
                searchPerformed={(value) => { this.setState({ searchPerformed: value }) }}
                providerType={providerType}
                toggleExpandNav={this.toggleExpandNav}
                findListViewId="listViewId"
                clearIndex={this.DisplayListView}
                findCommandsData={this.props.findCommandsData}
              />
            </div>
            <div
              style={{ zIndex: 0 }}
              className={
                this.state.isNavExpanded
                  ? "col-sm-9"
                  : "col-sm-11 pl-0 left-neg-25"
              }
            >
              {/* {this.state.ListViewVisible ? providerType === "Contract" ? <ContractListView /> : <OfficeListView /> : ''} */}
              {providerType === Constants.CONTRACT ? (
                <ContractListView searchPerformed={this.state.searchPerformed} listViewTitle={displayName} />
              ) : (
                <OfficeListView searchPerformed={this.state.searchPerformed} listViewTitle={displayName} />
                )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = state => {
  return {
    findCommandsData: state.findCommandsAction.findCommands
  };
}
const mapDispatchToProps = dispatch => {
  return {
    clearContract: () => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_CLEAR_CONTRACT
      });
    },
    clearOffice: () => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_CLEAR_OFFICE
      });
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(FindContainer);
