/**
 * User_story: US26_Provider_RC_XML_Browser
 *
 * Description:
 * ================================================================
 * As a User, I want to create XML Browser as a reusable component
 * so that it can display stylesheet on screens and can be customized
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ToolBar, XMLBrowser } from "conduent-component-library";

import "react-sticky-header/styles.css";
import StickyHeader from "react-sticky-header";
import ReactPanZoom from "./zoom";
import ACTION_TYPES from '../../../constants/ACTION_TYPES';
class LoadXMLBrowser extends Component {
  isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      html: {},
      printRef: {},
      zoom: 1,
      styles: [],
      values: [],
      ReferenceCodes: {},
      selectedZoom: null
    };
    this.myRef = React.createRef();
  }

  onFetchSuccessXMLBrowser = browser => {
    this.setState({ html: browser });
  };
  getXMLData(config) {
    this.props.fetchXMLBrowser({
      xmlreqbody: {
        SPName: config.storedProcedureName,
        XSLTPath: config.reportPath,
        officeid: this.props.content.officeID,
        returnStatus: CONFIG.OFFICE_MODULE.OFFICE.XMLBROWSER_RETURN_STATUS
      }
    }, this.onFetchSuccessXMLBrowser);
  }

  onFetchSuccess = data => {
    this.getXMLData(data.userReportsDTO[0]);
  };

  componentDidMount() {
    this.isMounted = true;
    if (this.isMounted) {
      this.props.fetchReports(
        {
          usage: CONFIG.USAGE_OBJ.ALL_BY_CATEGORYCODE,
          categoryCode: CONFIG.CODE.CATEGORY_CODE.OFFICE
        },
        this.onFetchSuccess
      );
    }
  }

  onRefresh(v) {
    this.setState({ html: {} });
    this.getXMLData(v[0]);
  }

  componentWillUnmount() {
    this.isMounted = false;
  }

  render() {
    return (
      <div className="ccl--xml-wrapper">
        <StickyHeader
          header={
            <div className="ccl--toolbar">
              <ToolBar
                selectedZoom={this.state.selectedZoom}
                onRefresh={v => {
                  this.onRefresh(v);
                }}
                styles={this.props.styles}
                selectedStyle={this.props.values}
                onSelectStyle={v => {
                  this.setState({ values: v });
                }}
                onZoom={val => {
                  this.setState({ zoom: val });
                }}
                printRef={this.myRef}
              />
            </div>
          }
        />
        <section
          style={{
            paddingTop: 0,
            paddingLeft: 0
          }}
        >
          <ReactPanZoom zoom={this.state.zoom}>
            <XMLBrowser ref={el => (this.myRef = el)} data={this.state.html} />
          </ReactPanZoom>
        </section>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    styles: state.officeSubViewResult?.styles?.userReportsDTO,
    values:
      state.officeSubViewResult?.styles &&
        state.officeSubViewResult?.styles.userReportsDTO.length
        ? [state.officeSubViewResult.styles.userReportsDTO[0]]
        : [],
    html: state.officeSubViewResult?.xmlbrowser
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetchReports: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES_OFC,
        paramsObj,
        callback
      });
    },
    fetchXMLBrowser: (paramsObj,callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER_OFC,
        paramsObj,
        callback
      });
    }
  };
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(LoadXMLBrowser)
);
