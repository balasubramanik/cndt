/**
 * User_story: US9_Provider_Stylesheet_Toolbar
 *
 * Description:
 * ================================================================
 * As a User, I want to display Stylesheet Expanded View, After User
 * makes a selection of record on the List View of office/contracts page
 * Ths is to provide zoom option in the toolbar
 * ================================================================
 *
 * Author: Birendranath
 *
 */ import * as React from "react";

export default class ReactPanZoom extends React.PureComponent {
  // In strict null checking setting default props doesn't seem to work. Hence the non-null assertion.
  // :crossedfingers: it shouldn't be deprecated. Or the very least support defaultProps semantics as proposed
  // in this PR: https://github.com/Microsoft/TypeScript/issues/23812
  //   defaultProps= {
  //     enablePan: true,
  //     onPan: () => undefined,
  //     pandx: 0,
  //     pandy: 0,
  //     zoom: 1,
  //   };
  getInitialState = () => {
    const { pandx, pandy, zoom } = this.props;
    const defaultDragData = {
      dx: pandx,
      dy: pandy,
      x: 0,
      y: 0
    };
    return {
      dragData: defaultDragData,
      dragging: false,
      matrixData: [
        zoom,
        0,
        0,
        zoom,
        pandx,
        pandy // [zoom, skew, skew, zoom, dx, dy]
      ]
    };
  };

  // Used to set cursor while moving.
  panWrapper;
  // Used to set transform for pan.
  panContainer;
  state = this.getInitialState();

  onMouseDown = e => {
    if (!this.props.enablePan) {
      return;
    }
    const { matrixData, dragData } = this.state;
    const offsetX = matrixData[4];
    const offsetY = matrixData[5];
    const newDragData = {
      dx: offsetX,
      dy: offsetY,
      x: e.pageX,
      y: e.pageY
    };
    this.setState({
      dragData: newDragData,
      dragging: true
    });
    if (this.panWrapper) {
      this.panWrapper.style.cursor = "move";
    }
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    e.preventDefault();
  };

  componentWillReceiveProps(nextProps) {
    const { matrixData } = this.state;
    const {
      zoom = matrixData[0],
      pandx = matrixData[4],
      pandy = matrixData[5]
    } = nextProps;
    const newMatrixData = [...this.state.matrixData];
    if (matrixData[0] !== zoom) {
      newMatrixData[0] = zoom || newMatrixData[0];
      newMatrixData[3] = zoom || newMatrixData[3];
    }
    if (matrixData[4] !== pandx) {
      newMatrixData[4] = pandx;
    }
    if (matrixData[5] !== pandy) {
      newMatrixData[5] = pandy;
    }
    this.setState({
      matrixData: newMatrixData
    });
  }

  onMouseUp = () => {
    this.setState({
      dragging: false
    });
    if (this.panWrapper) {
      this.panWrapper.style.cursor = "";
    }
    if (this.props.onPan) {
      this.props.onPan(this.state.matrixData[4], this.state.matrixData[5]);
    }
  };

  getNewMatrixData = (x, y) => {
    const { dragData, matrixData } = this.state;
    const deltaX = dragData.x - x;
    const deltaY = dragData.y - y;
    matrixData[4] = dragData.dx - deltaX;
    matrixData[5] = dragData.dy - deltaY;
    return matrixData;
  };

  onMouseMove = e => {
    if (this.state.dragging) {
      const matrixData = this.getNewMatrixData(e.pageX, e.pageY);
      this.setState({
        matrixData
      });
      if (this.panContainer) {
        this.panContainer.style.transform = `matrix(${this.state.matrixData.toString()})`;
      }
    }
  };

  render() {
    return (
      <div
        className={`pan-container ${this.props.className || ""}`}
        onMouseDown={this.onMouseDown}
        onMouseUp={this.onMouseUp}
        onMouseMove={this.onMouseMove}
        style={{
          height: this.props.height,
          userSelect: "none",
          width: this.props.width
        }}
        ref={ref => (this.panWrapper = ref)}
      >
        <div
          ref={ref => (ref ? (this.panContainer = ref) : null)}
          style={{
            transform: `matrix(${this.state.matrixData.toString()})`
          }}
        >
          {this.props.children}
        </div>
      </div>
    );
  }
}
ReactPanZoom.defaultProps = {
  enablePan: true,
  onPan: () => undefined,
  pandx: 0,
  pandy: 0,
  zoom: 1
};
