/**
 * User_story: US9_Provider_Stylesheet_Toolbar
 * 
 * Description:  
 * ================================================================
 * As a User, I want to display Stylesheet Expanded View, After User 
 * makes a selection of record on the List View of office/contracts page
 * ================================================================
 * 
 * Author: Birendranath
 * 
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { LastTenSearch } from 'conduent-component-library';
import { withRouter } from 'react-router-dom';
import { Link } from "react-router-dom";
import ACTION_TYPES from '../../../constants/ACTION_TYPES';
import Constants from '../../../constants/CONFIG';
class NavBar extends Component {
    isMounted = false;
    constructor() {
        super();
        this.child = React.createRef();
        this.state = {
            popupVisible: false,
            list: []
        };
        this.handleClick = this.handleClick.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.recentSearch = this.recentSearch.bind(this);
    }
    recentSearch() {
        this.child.current.toggleLast10();
    }
    handleClick() {
        if (!this.state.popupVisible) {
            // attach/remove event handler
            document.addEventListener('click', this.handleOutsideClick, false);
        } else {
            document.removeEventListener('click', this.handleOutsideClick, false);
        }

        this.setState(prevState => ({
            popupVisible: !prevState.popupVisible,
        }));
    }

    handleOutsideClick(e) {

        if (this.node.contains(e.target)) {
            return;
        }

        this.handleClick();
    }
    onSuccess = (data) => {
        this.setState({ list: data })
    }
    onSetLastTen = () => {
        this.props.getLastTenSearchData({
            "itemType": this.props.type,
            "lastTen": true
        }, this.onSuccess);
    }
    componentDidMount() {
        this.isMounted = true;
        if (Object.keys(this.props.content).length > 0 && this.isMounted) {
            const reqParams = {
                "itemType": this.props.type,
                "itemValue": this.props.content,
                "addedTime": new Date()
            }
            this.props.setLastTen(reqParams, this.onSetLastTen)
        }


    }
    componentWillUnmount() {
        this.isMounted = false;

    }
    goBack() {
        let backURL;
        if (this.props.type === Constants.ITEM_TYPE.OFFICE) {
            localStorage.setItem("viewDetails", this.props.type)
            backURL = "/provider/Office";
            this.props.history.push(backURL);

        } else if (this.props.type === Constants.ITEM_TYPE.CONTRACT) {
            localStorage.setItem("viewDetails", this.props.type)
            backURL = "/provider/Contract";
            this.props.history.push(backURL);

        }

    }

    gotoFind() {
        let backURL;
        if (this.props.type === Constants.ITEM_TYPE.OFFICE) {
            backURL = "/provider/Office";
            this.props.history.push(backURL);
        } else if (this.props.type === Constants.ITEM_TYPE.CONTRACT) {
            backURL = "/provider/Contract";
            this.props.history.push(backURL);
        }

    }
    render() {

        const { list } = this.state;
        let newData

        if (this.props.type === Constants.ITEM_TYPE.CONTRACT) {
            newData = list.map(item => ({ ...item, text: item.itemValue.contractNumber + " - " + item.itemValue.contractName, time: item.addedTime }));

        } else if (this.props.type === Constants.ITEM_TYPE.OFFICE) {
            newData = list.map(item => ({ ...item, text: item.itemValue.officeNumber + " - " + item.itemValue.officeName, time: item.addedTime }));
        }


        const setRecord = (clickedObj, e) => {
            const URL = this.props.match.url.trim().split("/");

            if (this.props.type === Constants.ITEM_TYPE.CONTRACT) {
                URL[2] = clickedObj.itemValue.contractId;

            }
            if (this.props.type === Constants.ITEM_TYPE.OFFICE) {
                URL[2] = clickedObj.itemValue.officeID;


            }

            const config = {
                pathname: URL.join("/"),
                state: { detail: clickedObj.itemValue }
            }
            this.props.history.push(config);
            this.props.history.go(config)
        };


        const providerNameDetails = () => {
            if (this.props.type && this.props.type === Constants.ITEM_TYPE.OFFICE) {
                return (
                    <a className="nav-link textBredcumb p-0 w-100">{this.props.content.officeName} #{this.props.content.officeNumber}</a>
                )
            }
            if (this.props.type && this.props.type === Constants.ITEM_TYPE.CONTRACT) {
                return (
                    <a className="nav-link textBredcumb p-0 w-100">{this.props.content.contractName} #{this.props.content.contractNumber}</a>
                )
            }
            return null;
        }
        return (

            <nav className="navbar navbar-expand-lg   navbar-light bg-white providerNavbar">
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item">

                            <a className="pl-0 pr-0 nav-link backBtn hand" onClick={() => this.goBack()}><img src={process.env.PUBLIC_URL + "/assets/back.png"} alt="Go back to previous search"/></a>
                        </li>
                        <li className="nav-item pl-3">
                            <div className="nav-link Bread w-100 bredcumText1">Provider / <Link className="bredcumText2" to="#" onClick={() => { this.goBack() }}>{this.props.type === Constants.ITEM_TYPE.CONTRACT ? 'Contracts' : 'Offices'}</Link></div>
                            {providerNameDetails()}
                        </li>
                    </ul>
                    <div className="actions-part">
                        <a className="hand" >

                            <LastTenSearch
                                newSearchRequired={false}
                                newSearchText="New Search"
                                data={newData}
                                gotoFind={() => this.gotoFind()}
                                setRecord={setRecord}
                                ref={this.child}
                                position="right"
                            />
                        </a>

                        <a title="Out of scope"><img src={process.env.PUBLIC_URL + "/assets/action-code.png"} alt="Action code" /> </a>
                        <a title="Out of scope"> <img src={process.env.PUBLIC_URL + "/assets/custom-attributes.png"} alt="Custom Attributes"/></a>
                        <a title="Actions" ref={node => { this.node = node; }} onClick={this.handleClick} className={this.state.popupVisible ? 'dropdown show actions hand' : 'dropdown actions hand'}>
                            <img src={process.env.PUBLIC_URL + "/assets/actions.png"} alt="ActionS in details" />
                            <div className={this.state.popupVisible ? 'dropdown-menu show' : 'dropdown-menu'}>
                                <div className="dropdown-item m-0 nohand" title="Out of scope" style={{ paddingLeft: 28 }}><img src={process.env.PUBLIC_URL + "/assets/normal-copy-28.png"} alt="Normal Copy"/> Show explanation screen</div>
                                <div className="dropdown-item m-0 s-child nohand" title="Out of scope"><img src={process.env.PUBLIC_URL + "/assets/notes_png.png"} alt="Notes"/> Show notes screen</div>
                                <div className="dropdown-item m-0 nohand" title="Out of scope"><img src={process.env.PUBLIC_URL + "/assets/web-account-for-offices.png"} alt="Web Account for Offies"/> View web account for offices</div>
                            </div>
                        </a>
                    </div>
                </div>
            </nav>

        );
    }

}



const mapDispatchToProps = (dispatch) => {
    return {
        getLastTenSearchData: (paramsObj, callback) => {

            dispatch({
                type: ACTION_TYPES.UI_ACTION.REQUEST_GET_LAST_TEN_SEARCH,
                paramsObj,
                callback
            })
        },
        setLastTen: (paramsObj, callback) => {

            dispatch({
                type: ACTION_TYPES.UI_ACTION.REQUEST_SET_LAST_TEN_SEARCH,
                paramsObj,
                callback
            })
        }
    };
};
export default withRouter(connect(null, mapDispatchToProps)(NavBar));
