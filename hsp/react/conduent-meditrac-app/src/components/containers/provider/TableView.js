/**
 * User_story: US26_Provider_RC_XML_Browser
 *
 * Description:
 * ================================================================
 * As a User, I want to create XML Browser as a reusable component
 * so that it can display stylesheet on screens and can be customized
 * to display in the table view format
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import moment from "moment";
import CONFIG from './../../../constants/CONFIG';
class TableView extends Component {
  render() {
    const { content, fields, icon } = this.props;
    function getProjectId(referenceCodes, id, valueField) {
      id = id.toUpperCase();
      if (referenceCodes && referenceCodes.length > 0) {
        const refValue = referenceCodes.filter(el => el[valueField ? valueField : 'code'] === id);
        if (refValue && refValue.length > 0) {
          return refValue[0];
        }
        else {
          return {
            name: id
          };
        }

      } else {
        return {
          name: id
        };
      }
    }
    return (
      <table className="table table-responsive table-borderless providerSubTableNoBorder">
        <thead>
          <tr>
            {fields.map((col, i) => {
              {/** 
                here default value for column width is set to 280
                If object has a width property, that will be applied
                otherwise 280 will be used.
              */}
              return (
                <th
                  style={{ width: col.width ? col.width : CONFIG.STYLE.DEFAULT_WIDTH }}
                  scope="col"
                  key={i}
                >
                  {" "}
                  {icon && content[col.field] && content[col.subfield] ? (
                    <img
                      src={process.env.PUBLIC_URL + "/assets/checkbox.png"}
                      alt="Checkbox"
                    />
                  ) : (
                      icon && (
                        <img
                          src={process.env.PUBLIC_URL + "/assets/Crossmark.png"}
                          alt="Crossmark"
                        />
                      )
                    )}{" "}
                  {col.name}
                </th>
              );
            })}
          </tr>
        </thead>

        <tbody>
          <tr>
            {fields.map((col, i) => {
              if (
                col.referenceName &&
                Object.keys(col.referenceCodes).length > 0
              ) {
                if (content[col.field]) {
                  {/** 
                    CONFIG.STYLE.DEFAULT_WIDTH = 280
                    here default value for column width is set to 280
                    If object has a width property, that will be applied
                    otherwise 280 will be used, served from config.
                  */}
                  return (
                    <td style={{ width: col.width ? col.width : CONFIG.STYLE.DEFAULT_WIDTH }} key={i}>
                      {
                        col?.appendField ? content[col?.appendField] + " " : ''
                      }
                      {
                        getProjectId(
                          col.referenceCodes[col.referenceName],
                          content[col.field], col?.valueField
                        )[col?.labelField ? col?.labelField : 'name']
                      }
                    </td>
                  );
                } else {
                  {/** 
                    CONFIG.STYLE.DEFAULT_WIDTH = 280
                    here default value for column width is set to 280
                    If object has a width property, that will be applied
                    otherwise 280 will be used, served from config.
                  */}
                  return (
                    <td style={{ width: col.width ? col.width : CONFIG.STYLE.DEFAULT_WIDTH }} key={i}>
                      {col.empty ? col.empty : <p className="emptyField" />}
                    </td>
                  );
                }
              } else {
                {/** 
                    CONFIG.STYLE.PADDING_LEFT_WITH_ICON = 20
                    here if icon is available then left padding of 20
                    will be applied otherwise it will be 0.
                  */}
                return (
                  <td style={{ paddingLeft: icon ? CONFIG.STYLE.PADDING_LEFT_WITH_ICON : 0 }} key={i}>
                    {content[col.field] ? (
                      col.date ? (
                        moment(content[col.field]).format("MM/DD/YYYY")
                      ) : (
                          content[col.field]
                        )
                    ) : (
                        col.empty ? col.empty : <p className="emptyField" />
                      )}
                    {content[col.subfield] ? " - " + content[col.subfield] : ""}
                  </td>
                );
              }
            })}
          </tr>
        </tbody>
      </table>
    );
  }
}

export default TableView;
