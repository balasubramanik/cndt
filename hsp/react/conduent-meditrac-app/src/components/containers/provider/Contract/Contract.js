/**
 * User_story: US31_Provider_Contract_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of Contract After a particular
 * contract is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ToolBar, XMLBrowser } from "conduent-component-library";

import Resize from "../resize/Resize";
import ResizeVertical from "../resize/ResizeVertical";
import moment from "moment";
import "react-sticky-header/styles.css";
import StickyHeader from "react-sticky-header";
import Details from "./Details";
import AdditionalDetails from "./AdditionalDetails";
import ReactPanZoom from "./../zoom";
import CONFIG from './../../../../constants/CONFIG';
import ACTION_TYPES from '../../../../constants/ACTION_TYPES';
class Contract extends Component {
  isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      html: {},
      printRef: {},
      zoom: 1,
      styles: [],
      values: [],
      ReferenceCodes: {},
      selectedZoom: null
    };
    this.myRef = React.createRef();
  }
  getXMLData(config) {
    this.props.fetchXMLBrowser({
      xmlreqbody: {
        SPName: config.storedProcedureName,
        XSLTPath: config.reportPath,
        contractid: this.props.content.contractId
      }
    }, this.onFetchSuccessXMLBrowser);
  }

  onFetchSuccess = data => {
    this.getXMLData(data.userReportsDTO[0]);
  };
  onFetchSuccessXMLBrowser = browser => {
    this.setState({ html: browser });
  };

  componentDidMount() {
    this.isMounted = true;
    if (this.isMounted) {
      this.props.fetchReports(
        {
          usage: CONFIG.USAGE_OBJ.ALL_BY_CATEGORYCODE,
          categoryCode: CONFIG.CODE.CATEGORY_CODE.CONTRACT
        },
        this.onFetchSuccess
      );
    }
  }

  onRefresh(v) {
    this.setState({ html: {} });
    this.getXMLData(v[0]);
  }

  componentWillUnmount() {
    this.isMounted = false;
  }
  render() {
    const { content } = this.props;
    const { ViewType } = this.props.match.params;
    return (
      <Resize handleWidth="5px" handleColor="#777">
        <ResizeVertical height="350px" className="contractViewBelow">
          <div className="ccl--xml-wrapper">
            <StickyHeader
              header={
                <div className="ccl--toolbar">
                  <ToolBar
                    selectedZoom={this.state.selectedZoom}
                    onRefresh={v => {
                      this.onRefresh(v);
                    }}
                    styles={this.props.styles}
                    selectedStyle={this.props.values}
                    onSelectStyle={v => {
                      this.setState({ values: v });
                    }}
                    onZoom={val => {
                      this.setState({ zoom: val });
                    }}
                    printRef={this.myRef}
                  />
                </div>
              }
            />
            <section
              style={{
                paddingTop: 0,
                paddingLeft: 0
              }}
            >
              <ReactPanZoom zoom={this.state.zoom}>
                <XMLBrowser
                  ref={el => (this.myRef = el)}
                  data={this.state.html}
                />
              </ReactPanZoom>
            </section>
          </div>
        </ResizeVertical>
        <ResizeVertical height="500px" className="contractViewBelow">
          <a
            title="Out of scope"
            className="mt-3 nav-item d-block float-right editText"
          >
            <img src={process.env.PUBLIC_URL + "/assets/edit_box.png"} alt="Edit Contract Information" /> Edit
          </a>
          {this.isMounted && ViewType === "details" && (
            <Details content={content} />
          )}
          {this.isMounted && ViewType === "additionalDetails" && (
            <AdditionalDetails content={content} />
          )}

          {this.isMounted && ViewType === "outOfScope" && (
            <div className="m-3">
              <p className="headerLabel">Out of scope</p>
            </div>
          )}
          <p className="m-3 mentSubText">
            Last updated by {content.lastUpdatedBy} at{" "}
            {moment(content.lastUpdatedAt).format("MM/DD/YYYY, h:mm:ss a")}
          </p>
        </ResizeVertical>
      </Resize>
    );
  }
}

const mapStateToProps = state => {
  return {
    styles: state.providerSubViewResult?.styles?.userReportsDTO,
    values: [state.providerSubViewResult?.styles?.userReportsDTO[0]]
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetchReports: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES,
        paramsObj,
        callback
      });
    },
    fetchXMLBrowser: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER,
        paramsObj,
        callback
      });
    }
  };
};
export default
  connect(mapStateToProps, mapDispatchToProps)(withRouter(Contract));
