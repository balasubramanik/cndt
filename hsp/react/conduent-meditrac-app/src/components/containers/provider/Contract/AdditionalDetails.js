/**
 * User_story: US8_Provider_Contract_Additional Details
 *
 * Description:
 * ================================================================
 * As a User, I want to create Web UI for View Additional Details Page After User makes a selection
 * of View Additional Details Sub Button in the View details Page. Claims Posting,
 * Capitation, Claims Adjudication, and PCP Options should be displayed on the right hand side
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import TableView from "./../TableView";
import { connect } from "react-redux";
import CONFIG from './../../../../constants/CONFIG';
import ACTION_TYPES from '../../../../constants/ACTION_TYPES';
class AdditionalDetails extends Component {
  isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      ReferenceCodes: {}
    };
  }

  getreferencecodes() {
    const { 
      PAYMENT_CLASS, 
      YES_NO, 
      PROVIDER_INHERIT_MEMBER_PAYMENT, 
      TO_PAY,
      UNIT_TYPE,
      APPLY_COPAY_PER_SCHEDULE,
      NEGOTIATED,
      MONEY_LIMIT_PROCESSING_ORDER,
      PANEL_SIZE_ENFORCEMENT,
      PANEL_SIZE_CALC_METHOD,
      PANEL_OVERRIDE_ALLOWANCE, 
    } = CONFIG.ADDITIONAL_DETAILS;
    const reqBody = {
      Type: [
        { type: PAYMENT_CLASS },
        { type: YES_NO },
        { type: PROVIDER_INHERIT_MEMBER_PAYMENT },
        { type: TO_PAY },
        { type: UNIT_TYPE },
        { type: APPLY_COPAY_PER_SCHEDULE },
        { type: NEGOTIATED },
        { type: MONEY_LIMIT_PROCESSING_ORDER },
        { type: PANEL_SIZE_ENFORCEMENT },
        { type: PANEL_SIZE_CALC_METHOD },
        { type: PANEL_OVERRIDE_ALLOWANCE }
      ],
      Usage: CONFIG.USAGE_OBJ.USAGE2
    };

    this.props.fetchRefcodes(reqBody, this.onFetchSuccess);
  }

  onFetchSuccess = data => {
    localStorage.setItem("refAddContract", JSON.stringify(data));
    this.setState({ ReferenceCodes: data });
  };

  componentDidMount() {
    this.isMounted = true;
    if (this.isMounted) {
      if (localStorage.getItem("refAddContract")) {
        const data = JSON.parse(localStorage.getItem("refAddContract"));
        this.setState({ ReferenceCodes: data });
      } else {
        this.getreferencecodes();
      }
    }
  }

  componentWillUnmount() {
    this.isMounted = false;
  }

  render() {
    const { content } = this.props;
    return (
      <div className="m-3">
        <p className="headerLabel">Claims Posting</p>
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PAYMENT_CLASS.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PAYMENT_CLASS.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PAYMENT_CLASS.FIELD
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.SUPPRESS_PAYMENT.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.SUPPRESS_PAYMENT.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.SUPPRESS_PAYMENT.NAME
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PROVIDER_INHERITS_MEMBER_PAYMENT.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PROVIDER_INHERITS_MEMBER_PAYMENT.NAME,
              width: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PROVIDER_INHERITS_MEMBER_PAYMENT.WIDTH,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PROVIDER_INHERITS_MEMBER_PAYMENT.FIELD
            }
          ]}
        />
        <p className="headerLabel">Capitation</p>
        <TableView
          content={content}
          fields={[
            { name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.CAPITATION_RATE.NAME, field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.CAPITATION_RATE.FIELD },
            { date: true, name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.LAST_POSTED.NAME, field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.LAST_POSTED.FIELD },
            { name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.POSTING_CUTOFF_DAY.NAME, field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.POSTING_CUTOFF_DAY.FIELD}
          ]}
        />
        <TableView
          content={content}
          fields={[
            {
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.RETRO_TERM_LIMIT.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.RETRO_TERM_LIMIT.FIELD
            },
            { name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.RETRO_ADD_LIMIT.NAME, field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.RETRO_ADD_LIMIT.FIELD }
          ]}
        />
        <p className="headerLabel">Claims Adjudication</p>
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.TO_PAY.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.TO_PAY.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.TO_PAY.FIELD
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.PRE_ESTIMATE_EXP.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.PRE_ESTIMATE_EXP.NAME,
              appendField: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.PRE_ESTIMATE_EXP.APPEND_FIELD,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.PRE_ESTIMATE_EXP.FIELD

            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.APPLY_COPAY_PER_SCHEDULE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.APPLY_COPAY_PER_SCHEDULE.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.APPLY_COPAY_PER_SCHEDULE.FIELD
            }
          ]}
        />
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.NEGOTIATED.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.NEGOTIATED.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.NEGOTIATED.FIELD
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.MONEY_LIMIT_PROCESSING_ORDER.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.MONEY_LIMIT_PROCESSING_ORDER.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.MONEY_LIMIT_PROCESSING_ORDER.FIELD
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.USE_HCPCS_FEE_SCHEDULE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.USE_HCPCS_FEE_SCHEDULE.NAME,
              width: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.USE_HCPCS_FEE_SCHEDULE.WIDTH,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.USE_HCPCS_FEE_SCHEDULE.FIELD
            }
          ]}
        />
        <p className="headerLabel"> PCP Options</p>
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.PANEL_SIZE_ENFORCEMENT.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.PANEL_SIZE_ENFORCEMENT.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.PANEL_SIZE_ENFORCEMENT.FIELD
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.PANEL_SIZE_CALC_METHOD.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.PANEL_SIZE_CALC_METHOD.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.PANEL_SIZE_CALC_METHOD.FIELD
            },
            {
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.FUTURE_PCP_LOOKBACK_PERIOD.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.FUTURE_PCP_LOOKBACK_PERIOD.FIELD
            }
          ]}
        />
        <TableView
          content={content}
          fields={[
            {
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.PRIMARY_PCP_LOOKBACK_PERIOD.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.PRIMARY_PCP_LOOKBACK_PERIOD.FIELD
            },
            {
              referenceName: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.PANEL_OVERRIDE_ALLOWANCE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.PANEL_OVERRIDE_ALLOWANCE.NAME,
              field: CONFIG.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.PANEL_OVERRIDE_ALLOWANCE.FIELD
            }
          ]}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchRefcodes: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_REF_CODES,
        paramsObj,
        callback
      });
    }
  };
};
export default connect(null, mapDispatchToProps)(AdditionalDetails);
