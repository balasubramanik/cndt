/**
 * User_story: US31_Provider_Contract_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of Contract After a particular
 * contract is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import TableView from "../TableView";
import { connect } from "react-redux";
import CONFIG from './../../../../constants/CONFIG';
import ACTION_TYPES from '../../../../constants/ACTION_TYPES';
class Details extends Component {
  isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      ReferenceCodes: {}
    };
  }

  getreferencecodes() {
    const reqBody = {
      Type: [
        { type: CONFIG.DETAILS.GET_REF_CODES.CONTRACT_TYPE },
        { type: CONFIG.DETAILS.GET_REF_CODES.PRICING_MODE },
        { type: CONFIG.DETAILS.GET_REF_CODES.FEE_SCHEDULE_SEARCH_CRITERIA }
      ],
      Usage: CONFIG.USAGE_OBJ.USAGE2
    };

    this.props.fetchRefcodes(reqBody, this.onFetchSuccess);
  }

  onFetchSuccess = data => {
    localStorage.setItem("refDetailsContract", JSON.stringify(data));
    this.setState({ ReferenceCodes: data });
  };

  componentWillMount() {
    this.isMounted = true;
    if (this.isMounted) {
      if (localStorage.getItem("refDetailsContract")) {
        const data = JSON.parse(localStorage.getItem("refDetailsContract"));
        this.setState({ ReferenceCodes: data });
      } else {
        this.getreferencecodes();
      }
    }
  }

  componentWillUnmount() {
    this.isMounted = false;
  }

  render() {
    const { content } = this.props;

    return (
      <div className="m-3">
        <p className="headerLabel">Contract Information</p>
        <TableView
          content={content}
          fields={[
            { name: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_NAME.NAME, field: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_NAME.FIELD },
            {
              referenceName: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_TYPE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_TYPE.NAME,
              field: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_TYPE.FIELD,
              empty: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_TYPE.EMPTY
            },
            { name: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_NUMBER.NAME, field: CONFIG.DETAILS.TABLE_VIEW_1.FIELDS.CONTRACT_NUMBER.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            { date: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.EFFECTIVE_DATE.DATE, name: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.EFFECTIVE_DATE.NAME, field: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.EFFECTIVE_DATE.FIELD },
            { date: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.EXP_DATE.DATE, name: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.EXP_DATE.NAME, field: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.EXP_DATE.FIELD },
            { name: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.CONTRACT_DESC.NAME, field: CONFIG.DETAILS.TABLE_VIEW_2.FIELDS.CONTRACT_DESC.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.DETAILS.TABLE_VIEW_3.FIELDS.PRICING_MODE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.DETAILS.TABLE_VIEW_3.FIELDS.PRICING_MODE.NAME,
              field: CONFIG.DETAILS.TABLE_VIEW_3.FIELDS.PRICING_MODE.FIELD
            },
            {
              referenceName: CONFIG.DETAILS.TABLE_VIEW_3.FIELDS.FEE_SCHEDULE_SEARCH_CRITERIA.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.DETAILS.TABLE_VIEW_3.FIELDS.FEE_SCHEDULE_SEARCH_CRITERIA.NAME,
              field: CONFIG.DETAILS.TABLE_VIEW_3.FIELDS.FEE_SCHEDULE_SEARCH_CRITERIA.FIELD
            }
          ]}
        />
        <TableView
          content={content}
          fields={[{ name: CONFIG.DETAILS.TABLE_VIEW_4.FIELDS.CONTRACT_NOTES.NAME, field: CONFIG.DETAILS.TABLE_VIEW_4.FIELDS.CONTRACT_NOTES.FIELD }]}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchRefcodes: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_REF_CODES,
        paramsObj,
        callback
      });
    }
  };
};
export default connect(null, mapDispatchToProps)(Details);
