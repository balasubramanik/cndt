/**
 * User_story: US4_Provider_Office_Additional Details
 *
 * Description:
 * ================================================================
 * To get the required list of demographics liske county and all to
 * display in details page
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import TableView from "../TableView";
import { connect } from "react-redux";
import CONFIG from "./../../../../constants/CONFIG";
import ACTION_TYPES from '../../../../constants/ACTION_TYPES';
class Demographics extends Component {
  isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      ReferenceCodes: {},
      Countries: []
    };
  }

  getreferencecodes() {
    const reqBody = {
      Type: [{ type: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.GET_REF_CODES.STATE }],
      Usage: CONFIG.USAGE_OBJ.USAGE2
    };

    this.props.fetchRefcodes(reqBody, this.onFetchSuccess);
  }

  onFetchGetCountries = ({ countries }) => {
    localStorage.setItem("Countries", JSON.stringify(countries))
    this.setState({ Countries: countries });
  }
  GetCountries() {
    const reqBody = {
      "usage": CONFIG.USAGE_OBJ.FOR_COMBOBOX
    };

    this.props.fetchGetCountries(reqBody, this.onFetchGetCountries);
  }
  onFetchSuccess = data => {
    localStorage.setItem("refDemographics", JSON.stringify(data));
    this.setState({ ReferenceCodes: data });
  };

  componentDidMount() {
    const Countries = JSON.parse(localStorage.getItem("Countries"))
    this.isMounted = true;
    if (this.isMounted) {
      if (localStorage.getItem("refDemographics")) {
        const data = JSON.parse(localStorage.getItem("refDemographics"));
        if (Countries) {
          this.setState({ Countries: Countries, ReferenceCodes: data });
        }
        else {
          this.setState({ ReferenceCodes: data });
          this.getreferencecodes();

        }

      }
    }

    if (Countries) {

    }
    this.GetCountries()
  }

  componentWillUnmount() {
    this.isMounted = false;
  }

  render() {
    const { content } = this.props;
    return (
      <div className="m-3">
        <p className="headerLabel">Demographics</p>
        <TableView
          content={content}
          fields={[
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_1.FIELDS.OFFICE_NUM.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_1.FIELDS.OFFICE_NUM.FIELD },
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_1.FIELDS.OFFICE_NAME.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_1.FIELDS.OFFICE_NAME.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_2.FIELDS.STREET_ADD_1.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_2.FIELDS.STREET_ADD_1.FIELD },
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_2.FIELDS.STREET_ADD_2.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_2.FIELDS.STREET_ADD_2.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.CITY.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.CITY.FIELD },
            {
              referenceName: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.STATE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.STATE.NAME,
              field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.STATE.FIELD
            },
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.ZIP.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_3.FIELDS.ZIP.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTY.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTY.FIELD },
            {
              referenceName: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTRIES.REF_NAME,
              referenceCodes: { Countries: this.state.Countries || [] },
              valueField: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTRIES.VALUE_FIELD,
              name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTRIES.NAME,
              labelField: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTRIES.LABEL_FIELD,
              field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_4.FIELDS.COUNTRIES.FIELD
            }

          ]}
        />
        <hr />
        <p className="headerLabel">Contact Information</p>
        <TableView
          content={content}
          fields={[
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.CONTACT_NAME.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.CONTACT_NAME.FIELD },
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.CONTACT_PHONE.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.CONTACT_PHONE.FIELD },
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.Ext.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.Ext.FIELD },
            { name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.FAX.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_5.FIELDS.FAX.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[{ name: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_6.FIELDS.CONTACT_EMAIL.NAME, field: CONFIG.OFFICE_MODULE.DEMOGRAPHICS.TABLE_VIEW_6.FIELDS.CONTACT_EMAIL.FIELD }]}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchRefcodes: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_REF_CODES,
        paramsObj,
        callback
      });
    },
    fetchGetCountries: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_COUNTRIES,
        paramsObj,
        callback
      });
    }
  };
};
export default connect(null, mapDispatchToProps)(Demographics);
