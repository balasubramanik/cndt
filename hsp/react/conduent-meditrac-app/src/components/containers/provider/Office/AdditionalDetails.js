/**
 * User_story: US4_Provider_Office_Additional Details
 *
 * Description:
 * ================================================================
 * As a User, I want to create Web UI for View Additional Details Page
 * After User makes a selection of View Additional Details Sub Button in the View details,
 * View Additional Office Details and Office Hours to be displayed on the right side
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import TableView from "../TableView";
import { connect } from "react-redux";
import CONFIG from "./../../../../constants/CONFIG";
import ACTION_TYPES from '../../../../constants/ACTION_TYPES';
class AdditionalDetails extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      ReferenceCodes: {}
    };
  }

  getreferencecodes() {
    const reqBody = {
      Type: [
        { type: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.GET_REF_CODES.YES_NO },
        { type: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.GET_REF_CODES.AVAILABLE_AFTER_HRS },
        { type: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.GET_REF_CODES.WHEELCHAIR_ACCESS },
        { type: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.GET_REF_CODES.AFTER_HRS_CONTACT_METHOD },
        { type: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.GET_REF_CODES.ALLOW_RA_ACCESS_ON_WEBOFFICE }
      ],
      Usage: CONFIG.USAGE_OBJ.USAGE2
    };

    this.props.fetchRefcodes(reqBody, this.onFetchSuccess);
  }

  onFetchSuccess = data => {
    localStorage.setItem("refAddOfc", JSON.stringify(data));
    this.setState({ ReferenceCodes: data });
  };

  componentDidMount() {
    this._isMounted = true;
    if (this._isMounted) {
      if (localStorage.getItem("refAddOfc")) {
        const data = JSON.parse(localStorage.getItem("refAddOfc"));
        this.setState({ ReferenceCodes: data });
      } else {
        this.getreferencecodes();
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { content } = this.props;
    return (
      <div className="m-3">
        <p className="headerLabel">Additional Office Details</p>

        <TableView
          content={content}
          fields={[
            {
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.FACILITY_OP_NUM.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.FACILITY_OP_NUM.FIELD
            },
            {
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PERM_FACILITY_ID.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.PERM_FACILITY_ID.FIELD
            },
            { name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.NPI.NAME, field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_1.FIELDS.NPI.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            { name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.NUM_OF_PHYSICIANS.NAME, field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.NUM_OF_PHYSICIANS.FIELD },
            { name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.CONTACTING_PROVIDER.NAME, empty: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.CONTACTING_PROVIDER.EMPTY, field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.CONTACTING_PROVIDER.FIELD },
            { name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.ACCESS_CODE.NAME, field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_2.FIELDS.ACCESS_CODE.FIELD }
          ]}
        />
        <TableView
          content={content}
          fields={[
            { 
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.TOT_OFFICE_HRS.NAME, 
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.TOT_OFFICE_HRS.FIELD
            },
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.AFTER_HRS_CONTACT_METHOD.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.AFTER_HRS_CONTACT_METHOD.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.AFTER_HRS_CONTACT_METHOD.FIELD
            },
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.WHEELCHAIR_ACCESS.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.WHEELCHAIR_ACCESS.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_3.FIELDS.WHEELCHAIR_ACCESS.FIELD
            }
          ]}
        />
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.ALLOW_RA_ACCESS_ON_WEBOFFICE.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.ALLOW_RA_ACCESS_ON_WEBOFFICE.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.ALLOW_RA_ACCESS_ON_WEBOFFICE.FIELD
            },
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.SHOW_IN_WEB_DIR.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.SHOW_IN_WEB_DIR.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.SHOW_IN_WEB_DIR.FIELD
            },
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.ALLOW_INVALID_NPI.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.ALLOW_INVALID_NPI.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_4.FIELDS.ALLOW_INVALID_NPI.FIELD
            }
          ]}
        />
        <TableView
          content={content}
          fields={[
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.AVAILABLE_AFTER_HRS.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.AVAILABLE_AFTER_HRS.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.AVAILABLE_AFTER_HRS.FIELD
            },
            {
              referenceName: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.OVERRIDE_DUP_ADDRESS.REF_NAME,
              referenceCodes: this.state.ReferenceCodes,
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.OVERRIDE_DUP_ADDRESS.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_5.FIELDS.OVERRIDE_DUP_ADDRESS.FIELD
            }
          ]}
        />
        <hr />
        <p className="headerLabel"> Office Hours</p>
        <TableView
          content={content}
          icon
          fields={[
            { 
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.MONDAY.NAME, 
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.MONDAY.FIELD, 
              subfield: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.MONDAY.SUBFIELD 
            },
            { 
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.TUESDAY.NAME, 
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.TUESDAY.FIELD, 
              subfield: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.TUESDAY.SUBFIELD
            },
            {
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.WEDNESDAY.NAME, 
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.WEDNESDAY.FIELD, 
              subfield: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.WEDNESDAY.SUBFIELD
            },
            {
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.THURSDAY.NAME, 
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.THURSDAY.FIELD, 
              subfield: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.THURSDAY.SUBFIELD
            },
            { 
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.FRIDAY.NAME, 
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.FRIDAY.FIELD, 
              subfield: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_6.FIELDS.FRIDAY.SUBFIELD
            }
          ]}
        />
        <TableView
          content={content}
          icon
          fields={[
            {
              name:CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.SATURDAY.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.SATURDAY.FIELD,
              subfield:CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.SATURDAY.SUBFIELD 
            },
            { 
              name: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.SUNDAY.NAME,
              field: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.SUNDAY.FIELD, 
              subfield: CONFIG.OFFICE_MODULE.ADDITIONAL_DETAILS.TABLE_VIEW_7.FIELDS.SUNDAY.SUBFIELD 
            }
          ]}
        />
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    fetchRefcodes: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_REF_CODES,
        paramsObj,
        callback
      });
    }
  };
};
export default connect(null, mapDispatchToProps)(AdditionalDetails);
