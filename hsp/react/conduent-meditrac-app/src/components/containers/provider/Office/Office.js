/**
 * User_story: US3_Provider_Office_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of a record After a particular
 * office is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ToolBar, XMLBrowser } from "conduent-component-library";

import Resize from "../resize/Resize";
import ResizeVertical from "../resize/ResizeVertical";
import moment from "moment";
import "react-sticky-header/styles.css";
import StickyHeader from "react-sticky-header";
import Demographics from "./Demographics";
import AdditionalDetails from "./AdditionalDetails";
import ReactPanZoom from "./../zoom";
import CONFIG from "./../../../../constants/CONFIG";
import ACTION_TYPES from '../../../../constants/ACTION_TYPES';
class Office extends Component {
  isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      html: {},
      printRef: {},
      zoom: 1,
      styles: [],
      values: [],
      ReferenceCodes: {},
      selectedZoom: null
    };
    this.myRef = React.createRef();
  }

  onFetchSuccessXMLBrowser = browser => {
    this.setState({ html: browser });
  };

  getXMLData(config) {
    this.props.fetchXMLBrowser({
      xmlreqbody: {
        SPName: config.storedProcedureName,
        XSLTPath: config.reportPath,
        officeid: this.props.content.officeID,
        returnStatus: CONFIG.OFFICE_MODULE.OFFICE.XMLBROWSER_RETURN_STATUS
      }
    }, this.onFetchSuccessXMLBrowser);
  }

  onFetchSuccess = data => {
    this.getXMLData(data.userReportsDTO[0]);
  };

  componentDidMount() {
    this.isMounted = true;
    if (this.isMounted) {
      this.props.fetchReports(
        {
          usage: CONFIG.USAGE_OBJ.ALL_BY_CATEGORYCODE,
          categoryCode: CONFIG.CODE.CATEGORY_CODE.OFFICE
        },
        this.onFetchSuccess
      );
    }
  }

  onRefresh(v) {
    this.setState({ html: {} });
    this.getXMLData(v[0]);
  }

  componentWillUnmount() {
    this.isMounted = false;
  }
  render() {
    const { content } = this.props;
    const { ViewType } = this.props.match.params;
    return (
      <Resize handleWidth="5px" handleColor="#777">
        <ResizeVertical height="350px" className="contractViewBelow">
          <div className="ccl--xml-wrapper">
            <StickyHeader
              header={
                <div className="ccl--toolbar">
                  <ToolBar
                    onRefresh={v => {
                      this.onRefresh(v);
                    }}
                    styles={this.props.styles}
                    selectedStyle={this.props.values}
                    onSelectStyle={v => {
                      this.setState({ values: v });
                    }}
                    onZoom={val => {
                      this.setState({ zoom: val });
                    }}
                    selectedZoom={this.state.selectedZoom}
                    printRef={this.myRef}
                  />
                </div>
              }
            />
            <section
              style={{
                paddingTop: 0,
                paddingLeft: 0
              }}
            >
              <ReactPanZoom zoom={this.state.zoom}>
                <XMLBrowser
                  ref={el => (this.myRef = el)}
                  data={this.state.html}
                />
              </ReactPanZoom>
            </section>
          </div>
        </ResizeVertical>
        <ResizeVertical height="500px" className="contractViewBelow">
          <a
            title="Out of scope"
            className="mt-3 nav-item d-block float-right editText"
          >
            <img src={process.env.PUBLIC_URL + "/assets/edit_box.png"} alt="Edit Office Information" /> Edit
          </a>
          {this.isMounted && ViewType === "demographics" && (
            <Demographics content={content} />
          )}
          {this.isMounted && ViewType === "additionalDetails" && (
            <AdditionalDetails content={content} />
          )}
          {this.isMounted && ViewType === "outOfScope" && (
            <div className="m-3">
              <p className="headerLabel">Out of scope</p>
            </div>
          )}

          <p className="m-3 mentSubText">
            Last updated by {content.lastUpdatedBy} at{" "}
            {moment(content.lastUpdatedAt).format("MM/DD/YYYY, h:mm:ss a")}
          </p>
        </ResizeVertical>
      </Resize>
    );
  }
}

const mapStateToProps = state => {
  return {
    styles: state.officeSubViewResult?.styles?.userReportsDTO,
    values: [state.officeSubViewResult?.styles?.userReportsDTO[0]]
  };
};
const mapDispatchToProps = dispatch => {
  return {
    fetchReports: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES_OFC,
        paramsObj,
        callback
      });
    },
    fetchXMLBrowser: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER_OFC,
        paramsObj,
        callback
      });
    }
  };
};
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Office));
