/**
 * User_story: US32_HSP_Log_In
 *
 * Description:
 * ============================================================================
 * As a User, I want to Log In on the Portal with Username and Password.
 * Main Container Layout for User Login.
 * Will display if user is not logged-in
 * =============================================================================
 *
 * Author: Bala,Sandeep
 *
 */
import React, { Component } from "react";
import { Redirect } from 'react-router-dom';
import { ComboBox, TextField, CheckBox } from "conduent-component-library";
import { connect } from "react-redux";
import { Alert, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import TermsAndConditions from './TermsAndConditions'
import moment from 'moment';
import io from 'socket.io-client';
import api from './../../api/apiList';
import ACTION_TYPES from "../../constants/ACTION_TYPES";
import CONFIG from './../../constants/CONFIG'
let isDisconnected = false;
let socket;
class LoginContainer extends Component {
  constructor(props) {
    super(props);

    const remData = JSON.parse(localStorage.getItem("RememberData"));

    this.state = {
      userName: (remData && remData.username) || "",
      password: "",
      appName: (remData && remData.appname && remData.appname[0]['value']) || "",
      selectedApp: (remData && remData.appname) || [],
      passwordBox: CONFIG.LOGIN.PASSWORD_BOX,
      rememberMe: (remData && remData.rememberMe) || false,
      isLoginScs: false,
      loginFailed: false,
      loginFailMsg: "",
      loginProcessing: false,
      isOpenedSess: false,
      connectedUser: {},
      loggedUserInfo: {},
      validationErrors: {}
    };
    this.controlValidations = {
      userName: {
        validation: {
          max: CONFIG.LOGIN.VALIDATION.MAXLENGTH
        },
        messages: {
          max: CONFIG.LOGIN.VALIDATION.MSG
        }
      },
      AsOfDate: {
        validation: {
          dateRange: {
            start: CONFIG.LOGIN.VALIDATION.DATE_RANGE.START,
            end: CONFIG.LOGIN.VALIDATION.DATE_RANGE.END
          }
        },
        messages: {
          dateRange: CONFIG.LOGIN.VALIDATION.DATE_RANGE.DATE_RANGE_MSG
        }
      }
    }
  }

  loginSuccess() {
    if (this.state.isLoginScs) {
      return <Redirect to='/' />
    }
    else if (JSON.parse(localStorage.getItem("GetData"))) {
      return <Redirect to='/' />
    }
  }

  setUserName(event) {
    this.setState({ userName: event.target.value })
  }

  setPassword(event) {
    this.setState({ password: event.target.value })
  }

  validateLogin() {
    let isValid = true;
    const validationErrors = {};
    if (!this.state.appName.trim()) {
      isValid = false;
      validationErrors["appName"] = CONFIG.LOGIN.VALIDATION.MESSAGES.APP_NAME
    }
    if (!this.state.userName.trim()) {
      isValid = false;
      validationErrors["userName"] = CONFIG.LOGIN.VALIDATION.MESSAGES.USER_NAME
    }
    if (!this.state.password.trim()) {
      isValid = false;
      validationErrors["password"] = CONFIG.LOGIN.VALIDATION.MESSAGES.PASSWORD
    }
    this.setState({ validationErrors });
    return isValid;
  }

  setSelectedApp(value) {
    if (value.length) {
      this.setState({
        selectedApp: value,
        appName: value[0]['value']
      })
    }
  }

  userLogin(forceLogin) {
    if (!this.validateLogin()) {
      return false;
    }

    let usage = CONFIG.USAGE_OBJ.USAGE1;
    if (forceLogin) {
      usage = CONFIG.USAGE_OBJ.USAGE3;
    }
    isDisconnected = forceLogin;
    if (this.state.rememberMe) {
      const rememberMe = {
        appname: this.state.selectedApp,
        username: this.state.userName,
        rememberMe: this.state.rememberMe
      }
      localStorage.setItem("RememberData", JSON.stringify(rememberMe))
    } else {
      localStorage.removeItem("RememberData")
    }
    const payload = {
      "username": this.state.userName,
      "password": this.state.password,
      "usage": usage,
      "roleName": "",
      "appName": this.state.appName,
      "version": "10.07.0.HSP.0.0.173",
      "computerName": "",
      "licenseMode": "db",
      "numberOfLicensedSeats": "1"
    }
    this.props.userLogin(payload);
    this.setState({ loginProcessing: true })
    socket = io.connect(api.apiURLOrigin, { path: api.socketPath });
  }
  
  componentWillUnmount() {
    if (socket) {
      socket.disconnect();
    }

  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.userData && nextProps.userData.status) {
      const resStatus = nextProps.userData.status.status;
      if (resStatus === "0") {
        localStorage.setItem('GetData', JSON.stringify(nextProps.userData.status));
        socket.emit("SET_NEW_SESSION", { force: isDisconnected, sessionId: nextProps.userData.status.sessionId, userID: nextProps.userData.status.userId });
        this.setState({
          isLoginScs: true,
          loginProcessing: false
        })
      } else if (resStatus === "1" && nextProps.userData.connectUser.length) {
        this.setState({
          isOpenedSess: true,
          loginProcessing: false,
          connectedUser: nextProps.userData.connectUser[0],
          loggedUserInfo: nextProps.userData.status
        })

      } else if (resStatus === "1") {
        this.setState({
          loginFailed: true,
          loginProcessing: false,
          loginFailMsg: nextProps.userData.outputMessages
        })
      }
    }
  }

  closePopup() {
    this.setState({ isOpenedSess: false })
  }

  render() {
    const { rememberMe, loginFailed, loginFailMsg, loginProcessing, isOpenedSess, connectedUser, validationErrors, toggleTerms } = this.state;
    let errorMessage = loginFailMsg.userMessage;
    if (loginFailMsg.userMessage === CONFIG.LOGIN.VALIDATION.USER_LOGINERROR_MSGS.INVALID.RESP_ERRROR_MSG) {
      errorMessage = CONFIG.LOGIN.VALIDATION.USER_LOGINERROR_MSGS.INVALID.ERROR_MSG;
    } else if (loginFailMsg.userMessage === CONFIG.LOGIN.VALIDATION.USER_LOGINERROR_MSGS.LOCKED.RESP_ERRROR_MSG) {
      errorMessage = CONFIG.LOGIN.VALIDATION.USER_LOGINERROR_MSGS.LOCKED.ERROR_MSG;
    }
    return (
      <React.Fragment>
        {this.loginSuccess()}
        <div className="pageToolBar">
        </div>
        <div className="container-fluid findContainer">
          <div className="row">
            <div className="login-container col-md-4">
              <div className="row">
                <div className="col-md-12">
                  <div className="login-logo">
                    <img src="/meditrac/assets/HSP-logo.PNG" alt="HSP logo" />
                  </div>
                </div>
              </div>
              <div className="row row-input">
                <div className="col-md-12 input-col">
                  <ComboBox
                    options={[{ value: 'Meditrac', title: 'Meditrac' }]}
                    label={"Choose Application"}
                    id="hspApplication"
                    controlName="appName"
                    onChange={(value) => this.setSelectedApp(value)}
                    required={"Y"}
                    placeholder='Please select application...'
                    validationErrors={validationErrors}
                    valueField={'value'}
                    labelField={'title'}
                    className="form-control"
                    values={this.state.selectedApp || []}
                    Autofocus={false}
                  />
                </div>
              </div>
              <div className="row row-input">
                <div className="col-md-12 input-col">
                  <TextField
                    type="text"
                    id="userName"
                    label={"Username"}
                    required={"Y"}
                    controlValidations={this.controlValidations}
                    controlName="userName"
                    className="col-md-12 form-control"
                    value={this.state.userName}
                    validationErrors={validationErrors}
                    dataType={"String"}
                    onKeyPress={(event) => {
                      if (event.charCode === 13) {
                        this.userLogin()
                      }
                    }}
                    autoComplete="off"
                    onChange={(event) => this.setUserName(event)}
                  />
                </div>
              </div>
              <div className="row row-input">
                <div className={(validationErrors['password'] ? "invalid-control " : "") + "col-md-12 input-col password-input"}>
                  <label htmlFor="userPassword">Password <span className="input-required">*</span></label>
                  <span className="forgot-password">
                    <a href="#" className="forgot-password">Forgot Password?</a>
                  </span>
                  <TextField
                    type="password"
                    passwordInput={true}
                    id="userPassword"
                    controlName="password"
                    className="col-md-12 form-control"
                    required={true}
                    value={this.state.password}
                    validationErrors={validationErrors}
                    dataType={"String"}
                    autoComplete="off"
                    onKeyPress={(event) => {
                      if (event.charCode === 13) {
                        this.userLogin()
                      }
                    }}
                    onChange={(event) => this.setPassword(event)}
                  />
                </div>
              </div>
              <div className="row row-input">
                <div className="col-md-12 input-col">
                  <span className="remember-me">
                    <CheckBox type="checkbox" id="rememberMe" checked={rememberMe} className="form-checkbox" label="Remember Me"
                      onChange={(event) => {event.preventDefault();this.setState({ rememberMe: !rememberMe })}}
                    />
                  </span>
                </div>
              </div>
              <button type="button" onClick={() => this.userLogin(false)} className="btn login-btn">
                {(loginProcessing) ? "Please Wait..." : "Sign In"}
              </button>
              <div className="alert-container">
                {
                  loginFailed ? <Alert className="login-error-container" color="danger">
                    <div className="error-icon">
                      <span><img src="/meditrac/assets/error.png" alt="error-icon" /></span>
                    </div>
                    <div className="error-message">
                      {errorMessage || ""}
                    </div>
                  </Alert> : ""
                }
              </div>
            </div>
          </div>
          <div className="row">
            <div className="powered-by-container col-md-4">
              <div className="powered-by-logo">
                <span className="powered-by-text">Powered by</span>
                <img src="/meditrac/assets/Conduent-logo.png" alt="conduent logo" />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="terms-conditions col-md-12">
              <div className="powered-by-logo">
                <span className="powered-by-text">By continuing you agree to our
                <a href="#" onClick={() => { this.setState({ toggleTerms: true }) }}>Terms & Conditions</a> and <a href="#" onClick={() => { this.setState({ toggleTerms: true }) }}>Privacy Policy</a></span>
              </div>
            </div>
          </div>
          <div>
            <Modal isOpen={isOpenedSess} toggle={() => this.closePopup()} centered>
              <ModalHeader toggle={() => this.closePopup()}></ModalHeader>
              <ModalBody>
                <div className="popup-msg">
                  A Different User is Logged In With The Same Username. Disconnect This User?
                </div>
                <div className="container open-session-detail">
                  <div className="row row-header">
                    <div className="col-md-3 row-col">Full Name</div>
                    <div className="col-md-4 row-col">Computer Name</div>
                    <div className="col-md-5 row-col">Connect Time</div>
                  </div>
                  <div className="row row-body">
                    <div className="col-md-3 row-col">{connectedUser.fullName}</div>
                    <div className="col-md-4 row-col">{connectedUser.computerName}</div>
                    <div className="col-md-5 row-col">{connectedUser.connectTime && moment(connectedUser.connectTime).format('MM/DD/YYYY hh:mm a')}</div>
                  </div>
                </div>
              </ModalBody>
              <ModalFooter>
                <Button className="btn cancel-session-btn" onClick={() => this.closePopup()}>Cancel</Button>
                <Button color="primary" className="disconnect-btn" onClick={() => this.userLogin(true)}>Disconnect</Button>
              </ModalFooter>
            </Modal>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    userLogin: (data) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_USER_LOGIN,
        payload: data
      });
    }
  };
};

const mapStateToProps = state => {
  return {
    userData: state.loginAction && state.loginAction.userData
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
