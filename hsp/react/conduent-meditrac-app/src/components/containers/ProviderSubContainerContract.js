/**
 * User_story: US31_Provider_Contract_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of Contract After a particular
 * contract is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
import React, { Component } from "react";
import { withRouter } from "react-router";
import { ButtonBar } from "conduent-component-library";
import Contract from "./provider/Contract/Contract";
import NavBar from "./provider/NavBar";
import { Button, ModalHeader, Modal, ModalBody, ModalFooter } from "reactstrap";
import "./provider/provider.css";
const routes = require("../../data/routes.json").CONTRACT;
class ProviderSubContainerContract extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "",
      content: null,
      modalMessage: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modalMessage: !prevState.modalMessage
    }));
  }
  getRouteConfig(find, type) {
    const childRoutes = routes[0].children;
    let routeReturn;
    childRoutes.map((route, key) => {
      const { children } = route;
      children.map((childRoute, key) => {

        if (childRoute[find] === type) {
          routeReturn = childRoute;

        }
      });
    });
    return routeReturn;
  }
  onSelected(ID) {
    let targetURL = this.getRouteConfig('id', ID)?.link;
    if (!targetURL) {
      this.setState({ modalMessage: true });
      return;
    }
    var config = {
      pathname: `/contractsubview/${this.state.content.contractId}/` + targetURL,
      state: { detail: this.state.content }
    };
    this.props.history.push(config);
  }

  componentDidMount() {
    const { ViewType } = this.props.match.params;
    let ID = this.getRouteConfig('link', ViewType)?.id;
    const original = JSON.parse(localStorage.getItem("CurrentContract"));
    let content = {};
    if (
      this.props.location &&
      this.props.location.state &&
      this.props.location.state.detail
    ) {
      content = this.props.location.state.detail;
      this.setState({ content: content, selected: ID });
      return;
    } else if (original) {
      content = original;
      this.setState({ content: content, selected: ID });
      return;
    }
    this.props.history.push(`/provider/Contract`);

  }

  render() {
    if (this.state.content) {
      return (
        <div>
          <NavBar type="ContractSearch" content={this.state.content} />
          <div id="sidebar-wrapper">
            <ButtonBar
              onSelected={s => this.onSelected(s)}
              container={false}
              selected={this.state.selected}
              routes={routes}
            />
          </div>
          <div id="page-content-wrapper1">
            <Contract
              content={this.state.content}
              selected={this.state.selected}
            />
          </div>
          <Modal
            backdrop="static"
            isOpen={this.state.modalMessage}
            toggle={this.toggle}
            className="common-popup-size common-popup-position"
          >
            <ModalHeader className="common-popup-header">Meditrac</ModalHeader>
            <ModalBody>
              <div className="d-inline"><img src={process.env.PUBLIC_URL + "/assets/caution_icon.png"} /></div>
              <div className="d-inline common-popup-body">Out of scope</div>
            </ModalBody>
            <ModalFooter>
              <Button className="common-popup-button" onClick={this.toggle}>
                Ok
          </Button>
            </ModalFooter>
          </Modal>
        </div>
      );
    } else {
      return null;
    }
  }
}
export default withRouter(ProviderSubContainerContract);
