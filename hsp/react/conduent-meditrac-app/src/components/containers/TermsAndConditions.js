import React from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

const TermsAndConditions = (props) => {
    return (
        <Modal size={"lg"} isOpen={props.toggleTerms} toggle={() => props.hideTerms()}>
            <ModalHeader toggle={() => props.hideTerms()}>Terms & Conditions</ModalHeader>
            <ModalBody>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dictum est non tortor accumsan rhoncus. Phasellus ultricies erat nec urna iaculis, eget finibus magna eleifend. Nunc quis ipsum nunc. Duis aliquet vitae ex id consequat. Vestibulum cursus id quam non hendrerit. Duis eu est aliquet, dignissim odio eu, fermentum magna. Maecenas orci lectus, commodo sit amet rhoncus et, ultrices eu leo. Quisque tristique pharetra metus id convallis. Vivamus eu erat ut lorem ultrices viverra. Maecenas nec massa felis. Phasellus convallis nunc metus, sit amet faucibus turpis sollicitudin at. Nam ac ex sed sem laoreet maximus. Donec ex risus, egestas at rhoncus eu, semper vel nulla. Aliquam vel volutpat odio. Proin non lectus urna.

                Etiam dictum, nisi eu accumsan condimentum, elit ex blandit libero, sed molestie turpis turpis vel felis. Fusce gravida faucibus tortor et consectetur. Nunc semper tincidunt finibus. Pellentesque lobortis massa urna, vitae mollis massa dignissim eget. Nam ultricies, velit vitae dapibus scelerisque, felis ante commodo augue, nec rhoncus dui risus ut mauris. Sed vel diam ex. Vivamus felis augue, iaculis at consequat nec, tempus eu mauris. Nam molestie tempus efficitur. In neque enim, condimentum consequat finibus id, dapibus id velit. Nullam dapibus aliquam magna, ac blandit est. Cras hendrerit ex condimentum lacus consequat fringilla. Ut rhoncus sem eget purus egestas ultrices. Fusce placerat auctor risus sed tincidunt. Aenean tincidunt ligula vel nulla pulvinar pulvinar. Fusce mattis nisi nunc, sed fringilla nisl commodo quis. Morbi vitae ultrices felis.
            </ModalBody>
        </Modal>
    )
}

export default TermsAndConditions;