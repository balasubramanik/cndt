/**
 * User_story: US19_Provider_Find_Control_(Main ToolStrip)
 *
 * Description:
 * ============================================================================
 * As a User, I want a Find Control (Main ToolStrip) as a reusable component
 * so that it can drive record selection on screens and is dynamic After
 * Main ToolStrip is selected to open Find Control
 * =============================================================================
 *
 * Author: Raja/Kunal
 *
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { FindControl } from "conduent-component-library";
import { Constants } from "../../constants/VIEW_TYPES";
import ACTION_TYPES from '../../constants/ACTION_TYPES';
import CONFIG from "./../../constants/CONFIG";
const paramsData = require("../../data/ParamsData.json");

class FindControlView extends Component {
  _isMounted = false;
  _isPageUpdate = true;
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      centerLoader: false,
      recentData: {},
      activeProfile: "",
      screenName: props.providerType,
      profileSettings: [],
      currentProfileId : 0
    };

    this.comboDefaultOption = {
      ReimbursementID: "<Any>",
      ResultCount: "<All>",
      AdditionalService: "<All>",
      Language: "<None>",
      State: "<Any>",
      County: "<Any>",
      Region: "<Any>"
    };

    this.controlValidations = {
      Distance: {
        validation: {
          max: 30
        },
        messages: {
          max: "Numeric Value is too Large.Please try a smaller value"
        }
      }
    };
    const paramsObj = {
      ...paramsData.getControlsParam,
      findCommand: this.props.providerType
    };
    this.props.fetchControls(paramsObj, this.onFetchControl);
  }

  shouldComponentUpdate(newProps, nextState) {
    console.log(this.props.findCommandsData)
    if (newProps.providerType !== this.props.providerType) {
      this.setState({ loading: true });
      const paramsObj = {
        findCommand: newProps.providerType,
        findProfileId: this.state.currentProfileId,
        usage: CONFIG.USAGE_OBJ.BY_FIND_COMMAND_USAGE
      };
      this.props.fetchControls(paramsObj, this.onFetchControl);

      let params = {
        "listviewProfileId": this.state.currentProfileId,
        "usage": CONFIG.USAGE_OBJ.FOR_LISTVIEW_USAGE,
        "listviewName": newProps.providerType
      }
      this.props.fetchRefCodes(params);

      this.callFunctionOnProvider(newProps.providerType!==""?newProps.providerType:this.props.providerType);
    }else if (this._isMounted===false){
      this.callFunctionOnProvider(this.props.providerType);
    }
    return true;
  }

  onGetRecentSearch = data => {
    if (data) {
      const recentData = JSON.parse(data.itemValue);
      this.setState({ recentData });
      this.handleFind(
        { ...recentData, ...this.state.params },
        this.props.providerType,
        ""
      );
    }
  };

  getRecentSearch = params => {
    this._isPageUpdate = false;
    params.itemType =
      this.props.providerType === Constants.CONTRACT
        ? Constants.CONTRACT_SEARCH
        : Constants.OFFICE_SEARCH;
    this.props.fetchRecentSearch(params, this.onGetRecentSearch);
    this.props.clearIndex("true");
  };

  setSearchList = params => {
    params.itemType =
      this.props.providerType === Constants.CONTRACT
        ? Constants.CONTRACT_SEARCH
        : Constants.OFFICE_SEARCH;
    this.props.setSearchList({
      itemType:
        this.props.providerType === Constants.CONTRACT
          ? Constants.CONTRACT_SEARCH
          : Constants.OFFICE_SEARCH,
      ...params
    });
    this.props.searchPerformed(true)


    
  };

  setProfileSettings = (profileId, findParams, screenName, profileName) => {
    const params = {
      ...paramsData.getProfileSettings,
      listviewProfileId: profileId
    };
    if (screenName === Constants.OFFICE) {
      params.listviewName = Constants.OFFICE;
    }
    this.props.fetchProfileSettings(params);
    if (this.props.profileSettings) {
      // this.handleFind(
      //   findParams,
      //   screenName,
      //   this.props.profileSettings.listviewProfileSettingsDTO
      // );
      this.setState({
        //profileSettings: this.props.profileSettings.listviewProfileSettingsDTO,
        activeProfile: profileName,
        currentProfileId: profileId
      });

      let params = {
        "listviewProfileId": profileId,
        "usage": CONFIG.USAGE_OBJ.FOR_LISTVIEW_USAGE,
        "listviewName": this.props.providerType
      }
      this.props.fetchRefCodes(params);

      const paramsObj = {
        findCommand: screenName,
        findProfileId: profileId,
        usage: CONFIG.USAGE_OBJ.BY_FIND_COMMAND_USAGE
      };
      this.props.fetchControls(paramsObj, this.onFetchControl);

      this.props.clearIndex("false");

    }
    //this.props.clearIndex("true");
  };

  callFunctionOnProvider(screenName) {
    console.log("screenName",screenName )
    if (screenName === Constants.CONTRACT) {
      this.props.fetchProfiles(paramsData.contractFetchProfile);
      this.setState({
        params: {
          ...paramsData.contractSearch
        },
        activeProfile: ""
      });
      if (localStorage.getItem(Constants.KEY_VIEW_DETAILS) === Constants.CONTRACT_SEARCH) {
        localStorage.setItem(Constants.KEY_VIEW_DETAILS, "");
        this.getRecentSearch(CONFIG.ITEM_TYPE.CONTRACT);
      }
      this._isMounted = true;
    } else if (screenName === Constants.OFFICE) {
      this.props.fetchProfiles(paramsData.officeFetchProfile);
      this.setState({
        params: {
          ...paramsData.officeSearch
        },
        activeProfile: ""
      });
      if (localStorage.getItem(Constants.KEY_VIEW_DETAILS) === Constants.OFFICE_SEARCH) {
        localStorage.setItem(Constants.KEY_VIEW_DETAILS, "");
        this.getRecentSearch(CONFIG.ITEM_TYPE.OFFICE);
      }
      this._isMounted = true;
    }
    
  }

  componentDidMount() {
    let params = {
      "listviewProfileId": this.state.currentProfileId,
      "usage": CONFIG.USAGE_OBJ.FOR_LISTVIEW_USAGE,
      "listviewName": this.props.providerType
    }
    this.props.fetchRefCodes(params);
    this.callFunctionOnProvider("");
  }

  casecadeDropdownValue = (value, ddlName, id, linkTo) => {
    if (linkTo && id.toLowerCase() === "demographic") {
      const paramsObj = {
        ...paramsData.sessionIdParams,
        usage: `|${linkTo.toUpperCase()}|`,
        [ddlName]: value
      };
      this.props.fetchDemoGraphics(paramsObj, linkTo);
    }
  };

  onFetchSuccess = data => {
    this.setState({ centerLoader: false });
  };

  onFetchControl = data => {
    this.setState({ loading: false });
  };

  handleFind = (findParams, screenName, columns = "") => {
    this.setState({ centerLoader: true });
    const params = {
      ...paramsData.findParams,
      ...findParams
    };
    if (screenName === Constants.OFFICE) {
      params.usage = Constants.SEARCH_USAGE;
      this.props.fetchOffice(params, this.onFetchSuccess);
    } else {
      this.props.fetchContract(params, this.onFetchSuccess);
    }
    if (this._isPageUpdate) {
      if (this.props.providerType === Constants.CONTRACT) {
        localStorage.setItem(Constants.KEY_CONTRACT_PAGENO, 0);
      } else {
        localStorage.setItem(Constants.KEY_OFFICE_PAGENO, 0);
      }
    }
    this._isPageUpdate = true;
  };

  componentWillUnmount() {
    this._isMounted = false;
    this.setState = (state, callback) => {};
  }

  DisplayListView(lvStat) {
    // alert(lvStat);
    debugger;
    this.props.clearIndex(lvStat);
  }

  render() {
    
    const loader = () => {
      return (
        <div className="spinner-border m-5" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    };
    const centerLoader = () => {
      if (!this.state.loading) {
        return (
          <div className="spinner-border loader" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        );
      } else return "";
    };
    if (
      this.props.controls &&
      this.props.controls.findControls &&
      this.props.controls.findControls.length
    ) {
      return (
        <React.Fragment>
          {this.state.centerLoader && centerLoader()}
          {!this.state.loading && (
            <FindControl
              screenName={this.props.providerType}
              findControls={this.props.controls.findControls}
              findProfiles={this.props.profiles}
              referenceCodes_V2DTOs={this.props.refcodes}
              params={this.state.params}
              onRecentSearch={params => this.getRecentSearch(params)}
              recentData={this.state.recentData}
              onFind={params => this.setSearchList(params)}
              onProfile={(profileId, findParams, screenName, profileName) =>
                this.setProfileSettings(profileId, findParams, screenName, profileName)
              }
              profileSettings={this.state.profileSettings}
              cascadeDropdownsListData={this.props.cascadeData}
              casecadeDropdownValue={this.casecadeDropdownValue}
              handleFind={this.handleFind}
              controlValidations={this.controlValidations}
              comboDefaultOption={this.comboDefaultOption}
              toggleExpandNav={this.props.toggleExpandNav}
              clearIndex={this.props.clearIndex}
              activeProfile={this.state.activeProfile}
            />
          )}
          {this.state.loading && loader()}
        </React.Fragment>
      );
    } else {
      return loader();
    }
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchProfiles: paramsObj => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_PROFILE,
        paramsObj
      });
    },
    fetchRefCodes: paramsObj => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_REFCODES,
        paramsObj
      });
    },
    fetchProfileSettings: paramsObj => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_PROFILESETTINGS,
        paramsObj
      });
    },
    fetchControls: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_CONTROLS,
        paramsObj,
        callback
      });
    },
    fetchDemoGraphics: (paramsObj, linkTo) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_DEMOGRAPHICS,
        paramsObj,
        linkTo
      });
    },
    fetchOffice: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_OFFICE,
        paramsObj,
        callback
      });
    },
    fetchContract: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_CONTRACT,
        paramsObj,
        callback
      });
    },
    fetchRecentSearch: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_RECENTSEARCH,
        paramsObj,
        callback
      });
    },
    setSearchList: paramsObj => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.FETCH_SEARCHLIST,
        paramsObj
      });
    }
  };
};

const mapStateToProps = state => {
  return {
    controls: state.controlsActions.controlsList,
    profiles: state.profileActions.profileList || {
      findProfile: [],
      findProfile2: []
    },
    refcodes: state.refCodesActions.refCodesList || [],
    profileSettings: state.profileSettingsActions.profileSettingsList,
    recentSearch: state.recentSearchActions.recentSearchList,
    cascadeData: state.controlsActions.cascadeDropdownsList || []
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FindControlView);
