/**
 * User_story: US6_Provider_Contract_Find_Results
 *
 * Description:
 * ============================================================================
 * As a User, I want to create Web UI for Find Contracts with Results
 *  (with all the relevant Contract details on the List View) After Search
 *  is made on selection of Find Button or Press Enter on any of the parameters
 *  in the parameter area, Find Contracts with Results Page to be displayed
 * =============================================================================
 *
 * Author: Raja,Kannan
 *
 */

import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { ListView } from "conduent-component-library";
import { Constants } from "../../constants/VIEW_TYPES";
import ACTION_TYPES from '../../constants/ACTION_TYPES';

class ContractListView extends Component {
  constructor(props) {
    super(props);
    this.colOrderListView = [];
    this.state = {
      listViewData: [],
      showListView: false,
      isLoadedSettings: false,
      page: 0,
      settings: {
        resizer: [],
        reorder: [],
        type: Constants.SET_COLS_CONTRACT
      }
    };
  }

  onSuccess = data => {
    const settings = {
      resizer: [],
      reorder: [],
      type: Constants.SET_COLS_CONTRACT
    };
    if (data && data.settings) {
      settings.resizer = data.settings.resizer;
      settings.reorder = data.settings.reorder;
    }
    localStorage.setItem(Constants.SET_COLS_CONTRACT, JSON.stringify(settings));
    this.setState({ isLoadedSettings: true, settings: settings });
  };

  componentWillMount() {
    this.props.getListViewSettingsContract(
      { type: Constants.CONTRACT },
      this.onSuccess
    );
  }

  setUserRestoreOption(params) {
    this.props.saveListViewSettingsContract(params);
  }

  SaveColumns = settings => {
    const jsonData = {
      type: Constants.CONTRACT,
      settings: settings
    };

    this.setUserRestoreOption(jsonData);
  };

  OBJtoXML(obj) {
    let xml = "";
    obj.map((index, key) => {
      if (index.accessor) {
        xml =
          xml +
          "&lt;Column&gt;&lt;Name&gt;" +
          index.accessor +
          "&lt;/Name&gt;&lt;Location&gt;" +
          key +
          "&lt;/Location&gt;&lt;Width&gt;" +
          index.width +
          "&lt;/Width&gt;&lt;/Column&gt;";
      }
    });
    const rtn =
      "&lt;?xml version:1.0?&gt;&lt;Settings&gt;&lt;Columns&gt;" +
      xml +
      "&lt;/Columns&gt;&lt;/Settings&gt;";
    return rtn;
  }

  SelectedRow = (e, rowInfo) => {
    var config = {
      pathname: `/contractsubview/${rowInfo.original.contractId}/details`,
      state: { detail: rowInfo.original }
    };
    this.props.history.push(config);
  };

  columnOrderData = cols => {
    // Re Order Column Detail
    this.colOrderListView = cols || this.state.columns;
  };

  componentDidUpdate(prevProps) {
    if (this.props.contracts && this.props.contracts !== prevProps.contracts) {
      const data = this.props.contracts;
      this.setState({
        page: parseInt(localStorage.getItem(Constants.KEY_CONTRACT_PAGENO)) || 0,
        listViewData: data[Constants.CONTRACTS],
        showListView: true
      });
    }
  }

  setPageNumber(page) {
    localStorage.setItem(Constants.KEY_CONTRACT_PAGENO, page);
    this.setState({ page: page });
  }

  render() {
    let columns = []
    if(this.props.profileSettings!==undefined && this.props.profileSettings!==undefined!==null){
      columns = JSON.parse(this.props.profileSettings.listviewProfileSettingsDTO[0].settings).ListviewSettings.VisibleColumns.Column
    }
    const { listViewData } = this.state;
    const isSortable = listViewData && Array.isArray(listViewData) && listViewData.length > 1 ? true: false;
    return (
      <React.Fragment>
        {this.state.isLoadedSettings && (
          <ListView
            rows={this.state.listViewData}
            searchPerformed = {this.props.searchPerformed}
            page={this.state.page}
            onPageChange={page => this.setPageNumber(page)}
            columns={columns}
            defaultPageSize={10}
            minRows={1}
            FindControllId=""
            Name=""
            FindType=""
            SortKey=""
            className="-striped -highlight"
            selectedIndex={this.SelectedRow}
            ListViewTitle={this.props.listViewTitle}
            SerialNoRequired
            SaveColumns={this.SaveColumns}
            settings={this.state.settings}
            sortable={isSortable}
          />
        )}
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveListViewSettingsContract: paramsObj => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.SAVE_LISTVIEW_SETTING,
        paramsObj
      });
    },
    getListViewSettingsContract: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.GET_LISTVIEW_SETTING,
        paramsObj,
        callback
      });
    }
  };
};

const mapStateToProps = state => {
  return {
    contracts: state.contractActions.contractList,
    profileSettings: state.profileSettingsActions.profileSettingsList
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ContractListView)
);
