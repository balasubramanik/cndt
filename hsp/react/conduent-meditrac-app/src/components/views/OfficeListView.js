/**
 * User_story: US2_Provider_Office_Find With Results
 *
 * Description:
 * ============================================================================
 * As a User, I want to create Web UI for Find Office with Results
 * (with all the relevant office details on the List View) After Search
 * is made on selection of Find Button or Press Enter on any of the parameters
 *  in the parameter area, Find Office with Results Page to be displayed
 * =============================================================================
 *
 * Author: Raja
 *
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { ListView } from "conduent-component-library";
import { withRouter } from "react-router-dom";
import { Constants } from "../../constants/VIEW_TYPES";
import LoadXMLBrowser from "./../containers/provider/loadXMLBrowser";
import APILIST from "./../../api/apiList";
import ACTION_TYPES from '../../constants/ACTION_TYPES';

class OfficeListView extends Component {
  constructor(props) {
    super(props);
    this.colOrderListView = [];
    this.state = {
      listViewData: [],
      xmlbrowser: null,
      isListView: true,
      showListView: false,
      isLoadedSettings: false,
      page: 0,
      settings: {
        resizer: [],
        reorder: [],
        type: Constants.SET_COLS_OFFICE
      }
    };
  }

  onSuccess = data => {
    const settings = {
      resizer: [],
      reorder: [],
      type: Constants.SET_COLS_OFFICE
    };
    if (data && data.settings) {
      settings.resizer = data.settings.resizer;
      settings.reorder = data.settings.reorder;
    }
    localStorage.setItem(Constants.SET_COLS_OFFICE, JSON.stringify(settings));
    this.setState({ isLoadedSettings: true, settings: settings });
  };

  componentWillMount() {
    this.props.getListViewSettings({ type: Constants.OFFICE }, this.onSuccess);
  }

  setUserRestoreOption(params) {
    this.props.saveListViewSetting(params);
  }

  SaveColumns = settings => {
    const jsonData = {
      type: Constants.OFFICE,
      settings: settings
    };
    this.setUserRestoreOption(jsonData);
  };

  OBJtoXML(obj) {
    let xml = "";
    obj.map((index, key) => {
      if (index.accessor) {
        xml =
          xml +
          "&lt;Column&gt;&lt;Name&gt;" +
          index.accessor +
          "&lt;/Name&gt;&lt;Location&gt;" +
          key +
          "&lt;/Location&gt;&lt;Width&gt;" +
          index.width +
          "&lt;/Width&gt;&lt;/Column&gt;";
      }
    });
    const rtn =
      "&lt;?xml version:1.0?&gt;&lt;Settings&gt;&lt;Columns&gt;" +
      xml +
      "&lt;/Columns&gt;&lt;/Settings&gt;";
    return rtn;
  }

  closeStylesheet() {
    this.setState({ isListView: true });
  }

  SelectedRow = (e, row) => {
    var config = {
      pathname: `/officesubview/${row.original.officeID}/demographics`,
      state: { detail: row.original }
    };
    this.props.history.push(config);
  };

  OnStyleSheetClick = (e, row) => {
    if (e === "SW") {
      this.setState({ isListView: false, xmlbrowser: row });
    }
  };

  OnOpenInNewWindowClick = (e, row) => {
    localStorage.setItem(Constants.KEY_CURRENTOFFICE, JSON.stringify(row));
    const pathname =
      APILIST.baseURL + `/officesubview/${row.officeID}/demographics`;
    window.open(pathname, "_blank", "toolbar=0,location=0,menubar=0");
  };

  OnOpenInNewTabClick = (e, row) => {
    localStorage.setItem(Constants.KEY_CURRENTOFFICE, JSON.stringify(row));
    const pathname =
      APILIST.baseURL + `/officesubview/${row.officeID}/demographics`;
    window.open(pathname, "_blank");
  };

  columnOrderData = cols => {
    // Column ReOrder Details Details
    this.colOrderListView = cols || this.state.columns;
  };

  componentDidUpdate(prevProps) {
    if (this.props.offices && this.props.offices !== prevProps.offices) {
      const data = this.props.offices;
      this.setState({
        page: parseInt(localStorage.getItem(Constants.KEY_OFFICE_PAGENO)) || 0,
        listViewData: data[Constants.OFFICES_V2S],
        showListView: true
      });
    }
  }

  setPageNumber(page) {
    localStorage.setItem(Constants.KEY_OFFICE_PAGENO, page);
    this.setState({ page: page });
  }

  render() {
    let columns = []
    if(this.props.profileSettings!==undefined && this.props.profileSettings!==undefined!==null ){
      columns = JSON.parse(this.props.profileSettings.listviewProfileSettingsDTO[0].settings).ListviewSettings.VisibleColumns.Column
    }
    const { isListView, xmlbrowser, listViewData } = this.state;
    const isSortable = listViewData && Array.isArray(listViewData) && listViewData.length > 1 ? true: false;
    return (
      <React.Fragment>
        <div>
          <div style={{ display: isListView ? "block" : "none" }}>
            {this.state.isLoadedSettings && (
              <ListView
                page={this.state.page}
                onPageChange={page => this.setPageNumber(page)}
                searchPerformed = {this.props.searchPerformed}
                rows={this.state.listViewData}
                columns={columns}
                defaultPageSize={10}
                minRows={1}
                FindControllId=""
                Name=""
                FindType=""
                SortKey=""
                className="-striped -highlight"
                selectedIndex={this.SelectedRow}
                columnOrder={this.columnOrderData}
                ListViewTitle={this.props.listViewTitle}
                DisplayIconRequired
                OnStyleSheetClick={this.OnStyleSheetClick}
                OnOpenInNewWindowClick={this.OnOpenInNewWindowClick}
                OnOpenInNewTabClick={this.OnOpenInNewTabClick}
                SerialNoRequired
                SaveColumns={this.SaveColumns}
                settings={this.state.settings}
                sortable={isSortable}
              />
            )}
          </div>
          {!isListView && (
            <div className="loadXmlBrowserListView">
              <a
                title="Close Stylesheet"
                onClick={() => {
                  this.closeStylesheet();
                }}
                className="closeStylesheetBtn"
              >
                <img
                  src={process.env.PUBLIC_URL + "/assets/close.png"}
                  alt="Close Stylesheet"
                />
              </a>
              <LoadXMLBrowser content={xmlbrowser} />
            </div>
          )}
        </div>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    saveListViewSetting: paramsObj => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.SAVE_LISTVIEW_SETTING,
        paramsObj
      });
    },
    getListViewSettings: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.GET_LISTVIEW_SETTING,
        paramsObj,
        callback
      });
    }
  };
};

const mapStateToProps = state => {
  return {
    offices: state.officeActions.officeList,
    profileSettings: state.profileSettingsActions.profileSettingsList
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(OfficeListView)
);
