/**
 * Page level toolbar to show search
 * and create contract/office buttons in right corner
 * of Find Office/Contract Screen
 * User_story: US1_Provider_Office_Find
 * 
 */
import React, { Component } from "react";
import { LastTenSearch } from "conduent-component-library";
import { withRouter, Link } from "react-router-dom";
import { connect } from "react-redux";
import { Constants } from "../constants/VIEW_TYPES";
import ACTION_TYPES from "../constants/ACTION_TYPES";
class PageLevelToolbar extends Component {
  constructor() {
    super();
    this.child = React.createRef();
    this.state = {
      popupVisible: false,
      list: []
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this.recentSearch = this.recentSearch.bind(this);
  }

  recentSearch() {
    this.child.current.toggleLast10();
  }

  handleClick() {
    if (!this.state.popupVisible) {
      // attach/remove event handler
      document.addEventListener("click", this.handleOutsideClick, false);
    } else {
      document.removeEventListener("click", this.handleOutsideClick, false);
    }

    this.setState(prevState => ({
      popupVisible: !prevState.popupVisible
    }));
  }

  handleOutsideClick(e) {
    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }

    this.handleClick();
  }

  onFetchSuccess = data => {
    this.setState({ list: data });
  };

  gotoFind() {
    let backURL;
    if (this.props.type === Constants.OFFICE_SEARCH) {
        backURL = "/provider/Office";
        this.props.history.push(backURL);
    } else if (this.props.type === Constants.CONTRACT_SEARCH) {
        backURL = "/provider/Contract";
        this.props.history.push(backURL);
    }

}

  componentDidMount() {
    this.props.getLastTenSearchData(
      {
        userID: 1,
        itemType: this.props.type,
        lastTen: true
      },
      this.onFetchSuccess
    );
  }

  shouldComponentUpdate(newProps, nextState) {
    if (newProps.type !== this.props.type) {
      this.props.getLastTenSearchData(
        {
          userID: 1,
          itemType: newProps.type,
          lastTen: true
        },
        this.onFetchSuccess
      );
    }
    return true;
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.setState = (state, callback) => { };
  }

  render() {
    const { list } = this.state;
    var newData = [];
    if (this.props.type === Constants.CONTRACT_SEARCH) {
      newData = list.map(item => ({
        ...item,
        text:
          item.itemValue.contractNumber + " - " + item.itemValue.contractName,
        time: item.addedTime
      }));
    }
    if (this.props.type === Constants.OFFICE_SEARCH) {
      newData = list.map(item => ({
        ...item,
        text: item.itemValue.officeNumber + " - " + item.itemValue.officeName,
        time: item.addedTime
      }));
    }

    const setRecord = (clickedObj, e) => {
      let config;
      if (this.props.type === Constants.CONTRACT_SEARCH) {
        config = {
          pathname: `/contractsubview/${clickedObj.itemValue.contractId}/details`,
          state: { detail: clickedObj.itemValue }
        };
      }
      if (this.props.type === Constants.OFFICE_SEARCH) {
        config = {
          pathname: `/officesubview/${clickedObj.itemValue.officeID}/demographics`,
          state: { detail: clickedObj.itemValue }
        };
      }

      this.props.history.push(config);
    };
    const { params } = this.props.match;
    return (
      <nav className="navbar navbar-expand-lg   navbar-light bg-white providerNavbar" style={{ zIndex: Object.keys(params).length === 0 ? 0 : 1 }}>
        <div className="collapse navbar-collapse">
          <div className="navbar-nav mr-auto mt-2 mt-lg-0">
            <div className="nav-item">
              <Link to="/" className="nav-link Bread">Provider</Link>
              <span className="pageTitle">{this.props.providerType}s</span>
            </div>
          </div>
          <div className="actions-part">
            <a className="hand">
              <LastTenSearch
                newSearchRequired
                data={newData}
                gotoFind={this.gotoFind}
                setRecord={setRecord}
                ref={this.child}
                position="right"
              />
            </a>
            <button type="button" className="createBtn">
              Create {this.props.providerType}
            </button>
          </div>
        </div>
      </nav>
    );
  }
}
const mapStateToProps = state => {
  return {
    list: state.LastTen?.lastTen
  };
};
const mapDispatchToProps = dispatch => {
  return {
    getLastTenSearchData: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_GET_LAST_TEN_SEARCH,
        paramsObj,
        callback
      });
    }
  };
};
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(PageLevelToolbar)
);
