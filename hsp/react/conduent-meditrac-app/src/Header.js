/**
 * Header Files with Menu of Provider(Contranct/Office)
 */
import React, { Component } from "react";
import { Link, Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import api from './api/apiList'
import io from 'socket.io-client';
import IdleTimer from 'react-idle-timer';
import CONFIG from './constants/CONFIG';
import { submenu } from './data/Icons';
import ACTION_TYPES from "./constants/ACTION_TYPES";
let socket;
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logoutSuccess: false,
      logoutConfirm: false,
      timeout: 1000 * (api.SESSION_TIMEOUT),//Milliseconds(1000) * seconds(300)
      showModal: false,
      userLoggedIn: false,
      isTimedOut: false,
      hideSubMenu: false
    }
    const user = JSON.parse(localStorage.getItem("GetData"));
    if (user) {
      socket = io.connect(api.apiURLOrigin, { path: api.socketPath, query: "sessionId=" + user.sessionId + "&userID=" + user.userId });
      socket.on("REMOVE_OLD_SESSION", (userSession) => {
        if (user.sessionId === userSession.sessionId) {
          props.history.push("/disconnected");
          socket.disconnect();
        }
      });
    }
    this.idleTimer = null
    this.onAction = this._onAction.bind(this)
    this.onActive = this._onActive.bind(this)
    this.onIdle = this._onIdle.bind(this)
    this.setActive = this.setActive.bind(this)
    this.getMainNavigation = this.getMainNavigation.bind(this)
    this.createSubmenu = this.createSubmenu.bind(this)
    this.getURL = this.getURL.bind(this)
    this.hideSubMenu = this.hideSubMenu.bind(this)
  }
  componentDidMount() {
    this.props.getPermission({});
  }
  componentWillUnmount() {
    socket.disconnect();
  }
  logout() {
    const userInfo = JSON.parse(localStorage.getItem("GetData"));
    const payload = {
      sessionID: userInfo?.sessionId,
    };
    socket.emit("logout", { userID: userInfo?.userId, sessionId: userInfo?.sessionId });
    socket.disconnect();
    this.props.disconnectUser(payload);
    localStorage.removeItem("GetData");
  }

  componentWillReceiveProps(nextProps) {
    const userDisconnect = nextProps.userDisconnect
    if (userDisconnect && userDisconnect.countMetadata && userDisconnect.countMetadata.status === "0") {
      this.setState({ logoutSuccess: true });
      nextProps.clearDisconnectInfo();
    }
  }

  logoutRedirect() {
    const userInfo = localStorage.getItem("GetData")
    if (this.state.logoutSuccess || !userInfo)
      return <Redirect to="/login" />
  }

  closePopup() {
    this.setState({ logoutConfirm: false });

  }

  setActive(e) {
    if (document.querySelector(".active")) {
      document.querySelector(".active").classList.remove("active");
    }
    e.currentTarget.classList.add("active");
  }
  closeSessionTimeOutPoup() {
    this.props.history.push("/login");
    this.setState({ showModal: false });
  }
  _onAction(e) {

    this.setState({ isTimedOut: false });
  }

  _onActive(e) {
    this.setState({ isTimedOut: false });
  }

  _onIdle(e) {

    const isTimedOut = this.state.isTimedOut
    if (isTimedOut) {
      this.logout();
      this.setState({ showModal: true });

    } else {
      this.idleTimer.reset();
      this.setState({ isTimedOut: true });
    }

  }

  hideSubMenu() {
    this.setState({ hideSubMenu: true }, () => {
      setTimeout(() => {
        this.setState({ hideSubMenu: false });
      }, 200);
    });
  }

  getMainNavigation(getURL) {
    return CONFIG.main_menu.list.map((li, i) => (
      <li key={i} className="nav-item dropdown" onClick={this.setActive}>
        <a className="nav-link">
          {li.text}
          <span className="underline" />
        </a>
        <div className={this.state.hideSubMenu ? "dropdown-wrapper hidemenu" : "dropdown-wrapper"}>
          <ul className="dropdown-menu">
            {
              li.list.map((subli, idx) => {
                return <li key={idx} className="dropdown" onClick={this.hideSubMenu}><Link to={getURL(subli.type)} className={getURL(subli.type) === "#" && (subli.type === 'CONTRACTS' || subli.type === 'OFFICES') ? "dropdown-item disabledMenuItem" : "dropdown-item"}>{subli.text}{subli.has_submenu && submenu}</Link>{subli.has_submenu && this.createSubmenu(subli, getURL)}</li>
              })
            }
          </ul>
        </div>
      </li>
    ))
  }

  createSubmenu(li, getURL) {
    const subListItems = li.list.map((list, i) => {
      return <li key={i} className="dropdown" onClick={this.hideSubMenu}><Link to={getURL(list.type)} className={getURL(list.type) === "#" && (list.type === 'CONTRACTS' || list.type === 'OFFICES') ? "dropdown-item disabledMenuItem" : "dropdown-item"}>{list.text}</Link></li>
    });
    return (
      <div className={this.state.hideSubMenu ? "dropdown-wrapper sub-dropdown hidemenu" : "dropdown-wrapper sub-dropdown"}>
        <ul className="dropdown-menu">
          {subListItems}
        </ul>
      </div>
    );
  }

  getURL(TYPE) {
    let url = "#";
    if (TYPE === 'CONTRACTS' && this.props.permissions.contract) {
      url = "/provider/Contract"
    }
    else if (TYPE === 'OFFICES' && this.props.permissions.office) {
      url = "/provider/Office";
    }
    return url
  }

  render() {
    let state = {};
    if (this.props.location && this.props.location.state) {
      state = this.props.location.state;
    }
    return (
      <nav className="navbar navbar-expand-sm header navbar-dark topheader" style={{ zIndex: Object.keys(state).length === 0 ? "inherit" : 9 }}>
        {!this.state.showModal ? this.logoutRedirect() : ''}
        <Link to="/" className="navbar-brand">
          <img
            src={process.env.PUBLIC_URL + "/assets/logo_blue_hsp.png"}
            alt=" HSP logo"
            style={{ height: "45px" }}
          />
        </Link>

        <div className="container-fluid">
          <ul className="navbar-nav">
            {this.getMainNavigation(this.getURL)}
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li className="nav-item">
              <a className="nav-link" onClick={() => this.setState({ logoutConfirm: true })}>Log Out</a>
            </li>
          </ul>
        </div>
        <Modal backdrop="static" isOpen={this.state.logoutConfirm} toggle={() => this.closePopup()} centered>
          <ModalHeader toggle={() => this.closePopup()}></ModalHeader>
          <ModalBody>
            You will be returned to log in screen ?
          </ModalBody>
          <ModalFooter>
            <Button className="btn cancel-session-btn" onClick={() => this.closePopup()}>Cancel</Button>
            <Button color="primary" className="disconnect-btn" onClick={() => this.logout()}>Continue</Button>
          </ModalFooter>
        </Modal>
        <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          element={document}
          onActive={this.onActive}
          onIdle={this.onIdle}
          onAction={this.onAction}
          debounce={250}
          timeout={this.state.timeout} />
        <Modal
          backdrop="static"
          isOpen={this.state.showModal}
          toggle={() => this.closeSessionTimeOutPoup()}
          className="common-popup-size1 common-popup-position"
        >
          <ModalBody>

            <div className="d-inline common-popup-body">
              Session timed out after {CONFIG.SESSION_TIMEOUT} seconds due to inactivity, you will be redirected to login page
            </div>

          </ModalBody>
          <ModalFooter>
            <Button className="common-popup-button" onClick={() => this.closeSessionTimeOutPoup()}>
              Ok
          </Button>
          </ModalFooter>
        </Modal>
      </nav>
    );
  }
}
const mapStateToProps = state => {
  return {
    permissions: state.auth.permissions,
    userDisconnect: state.loginAction && state.loginAction.userDisconnect
  }
};

const mapDispatchToProps = dispatch => {
  return {
    disconnectUser: (data) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_USER_DISCONNECT,
        payload: data
      });
    },
    getPermission: (paramsObj, callback) => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.REQUEST_PERMISSION,
        paramsObj,
        callback
      });
    },
    clearDisconnectInfo: () => {
      dispatch({
        type: ACTION_TYPES.UI_ACTION.CLEAR_DISCONNECT_INFO
      });
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
