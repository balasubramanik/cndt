/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from './../constants/ACTION_TYPES';
const initialState = {
  styles: null,
  xmlbrowser: null
};
export default (state = initialState, { type, data }) => {
  switch (type) {
    case ACTION_TYPES.UI_ACTION.RECEIVE_API_DATA_STYLES:
      return {
        ...state,
        styles: data
      };
    case ACTION_TYPES.UI_ACTION.RECEIVE_API_DATA_XMLBROWSER:
      return {
        ...state,
        xmlbrowser: data
      };
    default:
      return state;
  }
};
