/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  recentSearchList: null
};

export default function recentSearchReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_RECENTSEARCH_DATA:
      return Object.assign({}, state, { recentSearchList: action.data });
    default:
      return state;
  }
}
