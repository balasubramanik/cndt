/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  controlsList: [],
  cascadeDropdownsList: []
};

export default function findControlsReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_CONTROLS_DATA:
      return { ...state, controlsList: action.data };
    case ACTION_TYPES.UI_ACTION.SET_PARENT_DROPDOWN_DATA:
      return { ...state, cascadeDropdownsList: action.data };
    default:
      return state;
  }
}
