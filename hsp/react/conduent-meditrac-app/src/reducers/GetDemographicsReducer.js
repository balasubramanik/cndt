/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  demoGraphicsList: []
};

export default function getDemographics(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_DEMOGRAPHICS_DATA:
      return { ...state, demoGraphicsList: action.data };
    default:
      return state;
  }
}
