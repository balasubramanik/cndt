import ACTION_TYPES from "../constants/ACTION_TYPES";
const initialStateAuth = {
    isAuth: true,
    permissions: {
        office: true,
        contract: true
    }
};
export default function AuthReducer(state = initialStateAuth, action) {
    switch (action.type) {
        case ACTION_TYPES.UI_ACTION.IS_AUTH:
            return {
                ...state,
                isAuth: action.data
            };
        case ACTION_TYPES.UI_ACTION.RECEIVE_PERMISSION:
            return {
                ...state,
                permissions: action.data.permissions
            };
        case ACTION_TYPES.UI_ACTION.REMOVE_PERMISSION:
            return {
                ...state,
                permissions: {
                    office: false,
                    contract: false
                }
            };
        case ACTION_TYPES.UI_ACTION.LOGOUT:
            return {
                ...state,
                isAuth: false,
                permissions: {
                    office: false,
                    contract: false
                }
            };
        default:
            return state;
    }
}
