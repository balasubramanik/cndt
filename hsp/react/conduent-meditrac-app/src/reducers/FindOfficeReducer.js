/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  officeList: null
};

export default function findOfficeReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_OFFICE_DATA:
      return Object.assign({}, state, { officeList: action.data });
    case ACTION_TYPES.UI_ACTION.RECEIVE_CLEAR_OFFICE:
      return Object.assign({}, state, { officeList: [] });
    default:
      return state;
  }
}
