/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  profileList: null
};

export default function userLoginReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.USER_LOGIN:
      return Object.assign({}, state, { userData: action.data });

    case ACTION_TYPES.UI_ACTION.USER_DISCONNECT:
      return Object.assign({}, state, { userDisconnect: action.data, userData: {} });

    case ACTION_TYPES.UI_ACTION.CLEAR_DISCONNECT_INFO:
      return Object.assign({}, state, { userDisconnect: {} });

    default:
      return state;
  }
}

