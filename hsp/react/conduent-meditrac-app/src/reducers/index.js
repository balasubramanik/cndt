/**
 * Combine all reducers which are used to maintain the state of the response object using redux
 */
import { combineReducers } from "redux";
import findOfficeReducer from "./FindOfficeReducer";
import findContractReducer from "./FindContractReducer";
import findControlsReducer from "./FindControlsReducer";
import findProfileReducer from "./FindProfileReducer";
import getRefCodesReducer from "./GetRefCodesReducer";
import getProfileSettingsReducer from "./GetProfileSettingsReducer";
import getDemographicsReducer from "./GetDemographicsReducer";
import recentSearchReducer from "./GetRecentSearchReducer";
import setSearchListReducer from "./SetSearchListReducer";
import ProviderSubViewReducer from "./ProviderSubViewReducer";
import OfficeSubViewReducer from "./OfficeSubViewReducer";
import LastTenSearchData from "./lastTenSearchReducer";
import saveListViewSettingReducer from "./saveListViewSettingReducer";
import getListViewSettingReducer from "./getListViewSettingReducer";
import userLoginReducer from "./UserLoginReducer";
import AuthReducer from "./AuthReducer";
import FindCommandsReducer from "./FindCommandsReducer";

export default combineReducers({
  loginAction: userLoginReducer,
  officeActions: findOfficeReducer,
  controlsActions: findControlsReducer,
  contractActions: findContractReducer,
  profileActions: findProfileReducer,
  refCodesActions: getRefCodesReducer,
  profileSettingsActions: getProfileSettingsReducer,
  demographicsActions: getDemographicsReducer,
  recentSearchActions: recentSearchReducer,
  setSearchListActions: setSearchListReducer,
  providerSubViewResult: ProviderSubViewReducer,
  officeSubViewResult: OfficeSubViewReducer,
  LastTen: LastTenSearchData,
  saveListViewSettingAction: saveListViewSettingReducer,
  getListViewSettingReducer: getListViewSettingReducer,
  auth: AuthReducer,
  findCommandsAction: FindCommandsReducer
});
