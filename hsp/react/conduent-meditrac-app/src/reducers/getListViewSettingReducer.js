import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  getListView: null
};

export default function getListViewSettingReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_GET_LISTVIEW_SETTING:
      return Object.assign({}, state, { getListView: action.data });
    default:
      return state;
  }
}
