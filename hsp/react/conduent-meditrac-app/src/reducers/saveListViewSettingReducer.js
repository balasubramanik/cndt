import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  saveListView: null
};

export default function saveListViewSettingReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_SAVE_LISTVIEW_SETTING:
      return Object.assign({}, state, { saveListView: action.data });
    default:
      return state;
  }
}
