/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  findCommands: []
};

export default function FindCommandsReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_FIND_COMMANDS:
      return { ...state, findCommands: action.data };

    default:
      return state;
  }
}

