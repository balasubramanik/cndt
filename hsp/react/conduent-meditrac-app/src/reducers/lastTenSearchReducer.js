/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  lastTen: null
};

export default function setLastTenSearchData(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.REQUEST_SET_LAST_TEN_SEARCH:
      return {
        ...state
      };
    case ACTION_TYPES.UI_ACTION.RECEIVE_LAST_TEN_SEARCH:
      return {
        ...state,
        lastTen: action.data
      };
    default:
      return state;
  }
}
