/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  setSearchList: null
};

export default function setSearchListReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_SEARCHLIST_DATA:
      return Object.assign({}, state, { setSearchList: action.data });
    default:
      return state;
  }
}
