/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  profileList: null
};

export default function findProfileReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_PROFILE_DATA:
      return Object.assign({}, state, { profileList: action.data });
    default:
      return state;
  }
}
