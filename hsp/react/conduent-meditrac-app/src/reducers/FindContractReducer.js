/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  contractList: null
};

export default function findContractReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_CONTRACT_DATA:
      return Object.assign({}, state, { contractList: action.data });
    case ACTION_TYPES.UI_ACTION.RECEIVE_CLEAR_CONTRACT:
      return Object.assign({}, state, { contractList: [] });
    default:
      return state;
  }
}
