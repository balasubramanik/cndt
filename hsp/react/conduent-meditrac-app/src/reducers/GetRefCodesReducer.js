/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  refCodesList: []
};

export default function getRefCodesReducer(state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_REFCODES_DATA:
      return { ...state, refCodesList: action.data.referenceCodes_V2DTOs };
    default:
      return state;
  }
}
