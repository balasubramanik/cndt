/**
 * Reducer to maintain the state of the response object
 */
import ACTION_TYPES from "../constants/ACTION_TYPES";

const initialState = {
  profileSettingsList: null
};

export default function getProfileSettingsReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case ACTION_TYPES.UI_ACTION.ON_PROFILESETTINGS_DATA:
      return Object.assign({}, state, { profileSettingsList: action.data });
    default:
      return state;
  }
}
