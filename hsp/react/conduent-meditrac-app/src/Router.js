import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import FindContainer from "./components/containers/FindContainer";
import LoginContainer from "./components/containers/LoginContainer";
import ProviderSubContainerOffice from "./components/containers/ProviderSubContainerOffice";
import ProviderSubContainerContract from "./components/containers/ProviderSubContainerContract";
import Home from "./components/Home";
import LoadXMLBrowser from "./components/containers/provider/loadXMLBrowser";
import Header from "./Header";
import NoPermissionPopup from './NoPermissionPopup';
import PopupDisconnected from './PopupDisconnected';
import store from './store/store';
import axios from "axios";
const Router = (props) => (
  <Switch>
    <Route path="/login" component={LoginContainer} />
    <Route path="/disconnected" component={PopupDisconnected} />
    <PrivateRoute protect="isAuth" path="/unauthorized" component={NoPermissionPopup} />
    <PrivateRoute protect="isAuth" exact path="/" component={Home} />
    <PrivateRoute protect="all" path="/provider/:providerType" component={FindContainer} />
    <PrivateRoute protect="office" path="/officesubview/:id/:ViewType" component={ProviderSubContainerOffice} />
    <PrivateRoute protect="contract" path="/contractsubview/:id/:ViewType" component={ProviderSubContainerContract} />
    <PrivateRoute protect="office" path="/xmlbrowser/:id+" render={(props) => {
      let content = JSON.parse(localStorage.getItem("loadXmlBrowser"));
      return <LoadXMLBrowser content={content} />
    }} />
  </Switch>
)

const PrivateRoute = ({ component: Component, ...rest }) => {
  const userSession = JSON.parse(localStorage.getItem("GetData"));
  if (!userSession) {
    return (<Redirect
      to={{
        pathname: "/login"
      }}
    />)
  }
  axios.defaults.headers.common.sessionid = userSession.sessionId;
  axios.defaults.headers.common.userid = userSession.userId;
  return (
    <main>
      <Header />
      <div className="mainContainer">
        <Route
          {...rest}
          render={props => {
            const authObj = store.getState().auth;
            const protect = rest.protect;
            if (!authObj.isAuth) {
              return (

                <Redirect
                  to={{
                    pathname: "/login"
                  }}
                />)
            }
            else if (protect === 'isAuth') {

              if (authObj[protect]) {

                return (<Component {...props} />)

              }

            }

            if (protect === 'all') {

              if (Object.keys(props.match.params).length > 0 && props.match.params.providerType) {
                const providerType = props.match.params.providerType.toLowerCase();
                if ((providerType === 'contract' || providerType === 'office')) {

                  if (authObj.permissions[providerType]) {
                    return (<Component {...props} />)

                  }
                  else {

                    return (<Redirect
                      to={{
                        pathname: "/unauthorized"
                      }}
                    />)
                  }
                }
                else {
                  return (<Redirect
                    to={{
                      pathname: "/"
                    }}
                  />)
                }


              }
              else {
                return (<Redirect
                  to={{
                    pathname: "/"
                  }}
                />)
              }


            }
            else if (authObj.permissions[protect]) {

              return (<Component {...props} />)
            }

            return <NoPermissionPopup />;
          }}
        />
      </div>
    </main>
  )
};

export default Router;