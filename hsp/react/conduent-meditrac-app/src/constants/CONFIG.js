/** common config constants */
export default {
  SESSION_TIMEOUT: 300,
  STYLE: {
    DEFAULT_WIDTH: 280,
    PADDING_LEFT_WITH_ICON: 20
  },
  ITEM_TYPE: {
    OFFICE: "OfficeSearch",
    CONTRACT: "ContractSearch",
    TYPE: "RESULTCOUNT"
  },
  ALLOWCHECKOUT: {
    NO: "N",
    YES: "Y"
  },
  LOGIN:{
    PASSWORD_BOX: "password",
    VALIDATION: {
      MAXLENGTH: 30,
      MSG: "Please enter Username",
      DATE_RANGE: {
        START: "01/01/1900",
        END: "12/31/9999",
        DATE_RANGE_MSG: "As Of Date: Valid dates are between 1/1/1900 and 12/31/9999"
      },
      MESSAGES:{
        APP_NAME: "Please select Application",
        USER_NAME: "Please enter Username",
        PASSWORD: "Please enter Password"
      },
      USER_LOGINERROR_MSGS:{
        INVALID: {
          RESP_ERRROR_MSG: "Could not connect to database because the login is invalid.;",
          ERROR_MSG: "Login failed due to invalid role permissions"
        },
        LOCKED: {
          RESP_ERRROR_MSG: "Could not connect to database because the user account has been locked due to repeated invalid login attempts.;",
          ERROR_MSG: "Login failed attempts exceeded. Please try again in 15 minutes"
        }
      }
    }
  },
  DETAILS:{
    GET_REF_CODES:{
      CONTRACT_TYPE: "ContractType",
      PRICING_MODE: "PRICINGMODE",
      FEE_SCHEDULE_SEARCH_CRITERIA: "feeScheduleSearchCriteria"
    },
    TABLE_VIEW_1:{
      FIELDS:{
        CONTRACT_NAME:{
          NAME: "Contract Name",
          FIELD: "contractName"
        },
        CONTRACT_TYPE:{
          REF_NAME: "ContractType",
          NAME: "Contract Type",
          FIELD: "contractType",
          EMPTY: "None"
        },
        CONTRACT_NUMBER:{
          NAME: "Contract Number",
          FIELD: "contractNumber"
        }
      }
    },
    TABLE_VIEW_2:{
      FIELDS:{
        EFFECTIVE_DATE:{
          NAME: "Effective Date",
          FIELD: "effectiveDate",
          DATE: true
        },
        EXP_DATE:{
          NAME: "Expiration Date",
          FIELD: "contractType",
          DATE: true
        },
        CONTRACT_DESC:{
          NAME: "Contract Description",
          FIELD: "contractDescription"
        }
      }
    },
    TABLE_VIEW_3:{
      FIELDS:{
        PRICING_MODE:{
          REF_NAME: "PRICINGMODE",
          NAME: "Pricing Mode",
          FIELD: "pricingMode"
        },
        FEE_SCHEDULE_SEARCH_CRITERIA:{
          REF_NAME: "feeScheduleSearchCriteria",
          NAME: "Fee Schedule Search Criteria",
          FIELD: "feeScheduleSearchCriteria"
        }
      }
    },
    TABLE_VIEW_4:{
      FIELDS:{
        CONTRACT_NOTES:{
          NAME: "Contract Notes",
          FIELD: "contractNotes"
        }
      }
    }
  },
  CODE:{
    CATEGORY_CODE:{
      CONTRACT: "COP",
      OFFICE: "OP"
    }
  },
  USAGE_OBJ: {
    BY_FIND_COMMAND_USAGE: "|ByFindCommand|",
    FOR_FIND_COMMAND_USAGE: "|ForFindCommand|",
    FOR_LISTVIEW_USAGE: "|ForListview|",
    USAGE1: "|USAGE1|",
    USAGE2: "|USAGE2|",
    USAGE3: "|USAGE3|",
    ALL_BY_CATEGORYCODE: "|ALLBYCATEGORYCODE|",
    FOR_COMBOBOX: "|ForCombobox|"
  },
  LIST_VIEW_SETTINGS:{
    CONTRACT:{
      COL_TYPE: "setColsContract",
      LIST_TYPE_CONTRACT: "Contract",
      LIST_PAGE_NUMBER_CONTRACT: "ContractPageNumber"
    },
    OFFICE:{
      COL_TYPE: "setColsOffice",
      LIST_TYPE_OFFICE: "Office",
      CURRENT_OFFICE: "CurrentOffice",
      LIST_PAGE_NUMBER_OFFICE: "OfficePageNumber"
    }
  },
  OFFICE_MODULE:{
    ADDITIONAL_DETAILS:{
      GET_REF_CODES: {
        YES_NO: "YesNo",
        AVAILABLE_AFTER_HRS: "AvailableAfterHours",
        WHEELCHAIR_ACCESS: "wheelchairaccess",
        AFTER_HRS_CONTACT_METHOD: "AfterHoursContactMethod",
        ALLOW_RA_ACCESS_ON_WEBOFFICE: "AllowRAAccessOnWebOffice"
      },
      TABLE_VIEW_1: {
        FIELDS: {
          FACILITY_OP_NUM: {
            NAME: "Facility Operating Number",
            FIELD: "facilityOperatingNumber"
          },
          PERM_FACILITY_ID: {
            NAME: "Permanent Facility Identifier",
            FIELD: "permanentFacilityID"
          },
          NPI: {
            NAME: "National Provider Identifier",
            FIELD: "npi"
          }
        }
      },
      TABLE_VIEW_2: {
        FIELDS: {
          NUM_OF_PHYSICIANS: {
            NAME: "Number Of Physicians",
            FIELD: "numberOfPhysicians"
          },
          CONTACTING_PROVIDER: {
            NAME: "Contacting Provider",
            EMPTY:"None",
            FIELD: "contractingProviderName"
          },
          ACCESS_CODE: {
            NAME: "Access Code",
            FIELD: "accessCode"
          }
        }
      },
      TABLE_VIEW_3: {
        FIELDS: {
          TOT_OFFICE_HRS: {
            NAME: "Total Office Hours",
            FIELD: "totalOfficeHours"
          },
          AFTER_HRS_CONTACT_METHOD: {
            REF_NAME: "AfterHoursContactMethod",
            NAME: "After Hours Contact Method",
            FIELD: "afterHoursContactMethod"
          },
          WHEELCHAIR_ACCESS: {
            REF_NAME: "wheelchairaccess",
            NAME: "Wheelchair Access",
            FIELD: "wheelchairAccess"
          }
        }
      },
      TABLE_VIEW_4: {
        FIELDS: {
          ALLOW_RA_ACCESS_ON_WEBOFFICE: {
            REF_NAME: "AllowRAAccessOnWebOffice",
            NAME: "Allow RA Access On Web",
            FIELD: "AllowRAAccessOnWebOffice"
          },
          SHOW_IN_WEB_DIR: {
            REF_NAME: "YesNo",
            NAME: "Show In Web Directory",
            FIELD: "showInWebDirectory"
          },
          ALLOW_INVALID_NPI: {
            REF_NAME: "YesNo",
            NAME: "Allow Invalid NPI Format",
            FIELD: "allowInvalidNPI"
          }
        }
      },
      TABLE_VIEW_5: {
        FIELDS: {
          AVAILABLE_AFTER_HRS: {
            REF_NAME: "YesNo",
            NAME: "Available After Hours",
            FIELD: "availableAfterHours"
          },
          OVERRIDE_DUP_ADDRESS: {
            REF_NAME: "YesNo",
            NAME: "Override Duplicate Address",
            FIELD: "overrideDuplicateAddress"
          }
        }
      },
      TABLE_VIEW_6: {
        FIELDS: {
          MONDAY: {
            NAME: "Monday",
            FIELD: "mondayStart",
            SUBFIELD: "mondayEnd"
          },
          TUESDAY: {
            NAME: "Tuesday",
            FIELD: "tuesdayStart",
            SUBFIELD: "tuesdayEnd"
          },
          WEDNESDAY: {
            NAME: "Wednesday",
            FIELD: "wednesdayStart",
            SUBFIELD: "wednesdayEnd"
          },
          THURSDAY: {
            NAME: "Thursday",
            FIELD: "thursdayStart",
            SUBFIELD: "thursdayEnd"
          },
          FRIDAY: {
            NAME: "Friday",
            FIELD: "fridayStart",
            SUBFIELD: "fridayEnd"
          }
        }
      },
      TABLE_VIEW_7: {
        FIELDS: {
          SATURDAY: {
            NAME: "Saturday",
            FIELD: "saturdayStart",
            SUBFIELD: "saturdayEnd"
          },
          SUNDAY: {
            NAME: "Sunday",
            FIELD: "sundayStart",
            SUBFIELD: "sundayEnd"
          }
        }
      }
    },
    DEMOGRAPHICS: {
      GET_REF_CODES: {
        STATE: "State"
      },
      TABLE_VIEW_1: {
        FIELDS: {
          OFFICE_NUM: {
            NAME: "Office Number",
            FIELD: "officeNumber"
          },
          OFFICE_NAME: {
            NAME: "Office Name",
            FIELD: "officeName"
          }
        }
      },
      TABLE_VIEW_2: {
        FIELDS: {
          STREET_ADD_1: {
            NAME: "Street Address 1",
            FIELD: "address1"
          },
          STREET_ADD_2: {
            NAME: "Street Address 2",
            FIELD: "address2"
          }
        }
      },
      TABLE_VIEW_3: {
        FIELDS: {
          CITY: {
            NAME: "City",
            FIELD: "city"
          },
          STATE: {
            REF_NAME: "State",
            NAME: "State",
            FIELD: "state"
          },
          ZIP: {
            NAME: "Zip Code",
            FIELD: "zip"
          }
        }
      },
      TABLE_VIEW_4: {
        FIELDS: {
          COUNTY: {
            NAME: "County",
            FIELD: "county"
          },
          COUNTRIES: {
            REF_NAME: "Countries",
            VALUE_FIELD: "countryCode",
            NAME: "Country",
            LABEL_FIELD: "countryName",
            FIELD: "countryCode"
          }
        }
      },
      TABLE_VIEW_5: {
        FIELDS: {
          CONTACT_NAME: {
            NAME: "Contact Name",
            FIELD: "contactName"
          },
          CONTACT_PHONE: {
            NAME: "Contact Phone",
            FIELD: "contactPhone"
          },
          Ext: {
            NAME: "Ext",
            FIELD: "contactExt"
          },
          FAX: {
            NAME: "Contact Fax",
            FIELD: "contactFax"
          }
        }
      },
      TABLE_VIEW_6: {
        FIELDS: {
          CONTACT_EMAIL: {
            NAME: "Contact Email",
            FIELD: "contactEmail"
          }
        }
      }
    },
    OFFICE:{
      XMLBROWSER_RETURN_STATUS: "N"
    }
  },
  ADDITIONAL_DETAILS:{
    PAYMENT_CLASS:"PaymentClass",
    YES_NO: "YesNo",
    PROVIDER_INHERIT_MEMBER_PAYMENT: "providerInheritsMemberPayment",
    TO_PAY: "ToPay",
    UNIT_TYPE: "unitType",
    APPLY_COPAY_PER_SCHEDULE: "applyCopayPerSchedule",
    NEGOTIATED: "Negotiated",
    MONEY_LIMIT_PROCESSING_ORDER: "MoneyLimitProcessingOrder",
    PANEL_SIZE_ENFORCEMENT: "PanelSizeEnforcement",
    PANEL_SIZE_CALC_METHOD: "PanelSizeCalculationMethod",
    PANEL_OVERRIDE_ALLOWANCE: "PanelOverrideAllowance",
    TABLE_VIEW_1: {
      FIELDS: {
        PAYMENT_CLASS: {
          REF_NAME: "PaymentClass",
          NAME: "Payment Class",
          FIELD: "paymentClass"
        },
        SUPPRESS_PAYMENT: {
          REF_NAME: "YesNo",
          NAME: "Suppress Payment",
          FIELD: "suppressPayment"
        },
        PROVIDER_INHERITS_MEMBER_PAYMENT: {
          REF_NAME: "YesNo",
          NAME: "Provider Inherit's Member's Mispayment",
          WIDTH: 270,
          FIELD: "providerInheritsMemberPayment"
        }
      }
    },
    TABLE_VIEW_2: {
      FIELDS: {
        CAPITATION_RATE: {
          NAME: "Capitation Rate",
          FIELD: "capitationRateName"
        },
        LAST_POSTED: {
          NAME: "Last Posted",
          FIELD: ""
        },
        POSTING_CUTOFF_DAY: {
          NAME: "Posting Cutoff Day",
          FIELD: "postingCutoffDay"
        }
      }
    },
    TABLE_VIEW_3: {
      FIELDS: {
        RETRO_TERM_LIMIT: {
          NAME: "Retro Term Limit (Months)",
          FIELD: "retroactiveTermLimit"
        },
        RETRO_ADD_LIMIT: {
          NAME: "Retro Add Limit (Months)",
          FIELD: "retroactiveAddLimit"
        }
      }
    },
    TABLE_VIEW_4: {
      FIELDS: {
        TO_PAY: {
          REF_NAME: "ToPay",
          NAME: "Who To Pay",
          FIELD: "toPay"
        },
        PRE_ESTIMATE_EXP: {
          REF_NAME: "unitType",
          NAME: "Pre Estimate Expiration",
          APPEND_FIELD: "preEstExpUnits",
          FIELD: "preEstExpUnitType"
        },
        APPLY_COPAY_PER_SCHEDULE: {
          REF_NAME: "applyCopayPerSchedule",
          NAME: "Apply Copay Per Schedule",
          FIELD: "applyCopayPerSchedule"
        }
      }
    },
    TABLE_VIEW_5: {
      FIELDS: {
        NEGOTIATED: {
          REF_NAME: "Negotiated",
          NAME: "Negotiated",
          FIELD: "negotiated"
        },
        MONEY_LIMIT_PROCESSING_ORDER: {
          REF_NAME: "MoneyLimitProcessingOrder",
          NAME: "Money Limit Processing Order",
          FIELD: "moneyLimitProcessingOrder"
        },
        USE_HCPCS_FEE_SCHEDULE: {
          REF_NAME: "YesNo",
          NAME: "Use HCPCS When Adjudication Finds Fee Schedule",
          WIDTH: 340,
          FIELD: "useHCPCSWhenFindFeeSchedule"
        }
      }
    },
    TABLE_VIEW_6: {
      FIELDS: {
        PANEL_SIZE_ENFORCEMENT: {
          REF_NAME: "PanelSizeEnforcement",
          NAME: "Panel Size Enforcement",
          FIELD: "panelSizeEnforcement"
        },
        PANEL_SIZE_CALC_METHOD: {
          REF_NAME: "PanelSizeCalculationMethod",
          NAME: "Panel Size Calculation Method",
          FIELD: "panelSizeCalculationMethod"
        },
        FUTURE_PCP_LOOKBACK_PERIOD: {
          NAME: "Future PCP Lookback Period",
          FIELD: "futurePCPLookbackPeriod"
        }
      }
    },
    TABLE_VIEW_7: {
      FIELDS: {
        PRIMARY_PCP_LOOKBACK_PERIOD: {
          NAME: "Primary PCP Lookback Period",
          FIELD: "primaryPCPLookbackPeriod"
        },
        PANEL_OVERRIDE_ALLOWANCE: {
          REF_NAME: "PanelOverrideAllowance",
          NAME: "Panel Override Allowance",
          FIELD: "panelOverrideAllowance"
        }
      }
    }
  },
  main_menu: {
    list: [
      {
        text: "Claims",
        has_submenu: true,
        type: "CLAIMS",
        list: [
          { text: "Find Claim", type: 'FIND_CLAIM', has_submenu: false, list: [] },
          { text: "Dental", type: 'DENTAL', has_submenu: false, list: [] },
          { text: "Dental Estimate", type: 'DENTAL_ESTIMATE', has_submenu: false, list: [] },
          { text: "Institutional (UB)", type: 'INSTITUTIONAL', has_submenu: false, list: [] },
          { text: "Non-Medical", type: 'NON_MEDICAL', has_submenu: false, list: [] },
          { text: "Pharmacy (Rx)", type: 'PHARMACY_RX', has_submenu: false, list: [] },
          { text: "Professional (HCFA)", type: 'PROFESSIONAL_HCFA', has_submenu: false, list: [] },
          {
            text: "Payment Rules",
            has_submenu: true,
            type: "PAYMENT_RULES",
            list: [
              { text: "Bundling Rules", type: 'BUNDLING_RULES', has_submenu: false, list: [] },
              { text: "Modifier Reduction Schedules", type: 'MODIFIER_REDUCTION_SCHEDULES', has_submenu: false, list: [] },
              { text: "Network Reimbursement Contracts", type: 'NETWORK_REIMBURSEMENT_CONTRACTS', has_submenu: false, list: [] },
              { text: "Related Claim Rules", type: 'RELATED_CLAIM_RULES', has_submenu: false, list: [] },
              { text: "Restriction Classes", type: 'RESTRICTION_CLASSES', has_submenu: false, list: [] },
              { text: "Service Restriction Packages", type: 'SERVICE_RESTRICTION_PACKAGES', has_submenu: false, list: [] },
              { text: "Timely Filing", type: 'TIMELY_FILING', has_submenu: false, list: [] },
              { text: "UB Place Of Service Rules", type: 'UB_PLACE_OF_SERVICE_RULES', has_submenu: false, list: [] }
            ]
          },
          {
            text: "Utilities",
            has_submenu: true,
            type: "UTILITIES",
            list: [
              { text: "Auditing Packages", type: 'AUDITING_PACKAGES', has_submenu: false, list: [] },
              { text: "Auditing Wizard", type: 'AUDITING_WIZARD', has_submenu: false, list: [] },
              { text: "Batch Manager", type: 'BATCH_MANAGER', has_submenu: false, list: [] },
              { text: "Claims Resubmission", type: 'CLAIMS_RESUBMISSION', has_submenu: false, list: [] },
              { text: "Duplicate Claim Wizard", type: 'DUPLICATE_CLAIMS_WIZARD', has_submenu: false, list: [] },
              { text: "Open Work Management-Claims", type: 'OPEN_WORK_MANAGEMENT_CLAIMS', has_submenu: false, list: [] },
              { text: "Provider Picking Statistics", type: 'PROVIDER_PICKING_STATISTICS', has_submenu: false, list: [] },
              { text: "Re-Adjudication Wizard", type: 'READJUDICATION_WIZARD', has_submenu: false, list: [] },
              { text: "Repricing Wizard", type: 'REPRICING_WIZARD', has_submenu: false, list: [] }
            ]
          }
        ]
      },
      {
        text: "Enrollment",
        has_submenu: true,
        type: "ENROLLMENT",
        list: [
          { text: "Assign Providers For Reimbursement", type: 'ASSIGN_PROVIDERS_FOR_REIMBURSEMENT', has_submenu: false, list: [] },
          { text: "Brokers", type: 'BROKERS', has_submenu: false, list: [] },
          { text: "Groups", type: 'GROUPS', has_submenu: false, list: [] },
          { text: "ID Card Packages", type: 'ID_CARD_PACKAGES', has_submenu: false, list: [] },
          { text: "ID Card Wizard", type: 'ID_CARD_WIZARD', has_submenu: false, list: [] },
          { text: "Members", type: 'MEMBERS', has_submenu: false, list: [] },
          { text: "Payers", type: 'PAYERS', has_submenu: false, list: [] },
          { text: "Reinsurance Policies", type: 'REINSURANCE_POLICIES', has_submenu: false, list: [] }
        ]
      },
      {
        text: "Benefits",
        has_submenu: true,
        type: "BENEFITS",
        list: [
          { text: "Aid Codes", type: 'AID_CODES', has_submenu: false, list: [] },
          { text: "Benefit Plans", type: 'BENEFIT_PLANS', has_submenu: false, list: [] },
          { text: "Benefits Toolbox", type: 'BENEFIT_TOOLBOX', has_submenu: false, list: [] },
          { text: "Diagnosis Codes", type: 'DIAGNOSIS_CODES', has_submenu: false, list: [] },
          { text: "Procedure Codes", type: 'PROCEDURE_CODES', has_submenu: false, list: [] },
          { text: "Product Codes", type: 'PRODUCT_CODES', has_submenu: false, list: [] },
          { text: "Service Categories", type: 'SERVICE_CATEGORIES', has_submenu: false, list: [] }
        ]
      },
      {
        text: "Provider",
        has_submenu: true,
        type: "PROVIDER",
        list: [
          { text: "Contracts", type: 'CONTRACTS', has_submenu: false, list: [] },
          { text: "Covering Provider Groups", type: 'COVERING_PROVIDER_GROUPS', has_submenu: false, list: [] },
          { text: "Fee Schedules", type: 'FEE_SCHEDULES', has_submenu: false, list: [] },
          { text: "Networks", type: 'NETWORKS', has_submenu: false, list: [] },
          { text: "Offices", type: 'OFFICES', has_submenu: false, list: [] },
          { text: "Provider Specialty Categories", type: 'PROVIDER_SPECIALITY_CATEGORIES', has_submenu: false, list: [] },
          { text: "Provider Taxonomy Codes", type: 'PROVIDER_TAXONOMY_CODES', has_submenu: false, list: [] },
          { text: "Provider Wizard", type: 'PROVIDER_WIZARD', has_submenu: false, list: [] },
          { text: "Providers", type: 'PROVIDERS', has_submenu: false, list: [] },
          { text: "Risk Groups", type: 'RISK_GROUPS', has_submenu: false, list: [] }
        ]
      },
      {
        text: "Finance",
        has_submenu: true,
        type: "FINANCE",
        list: [
          { text: "Companies", type: 'COMPANIES', has_submenu: false, list: [] },
          { text: "Corporations", type: 'CORPORATIONS', has_submenu: false, list: [] },
          { text: "Vendors", type: 'VENDORS', has_submenu: false, list: [] },
          {
            text: "Billing", type: 'BILLING', has_submenu: true, list: [
              { text: "Billing Entities", type: 'BILLING_ENTITIES', has_submenu: false, list: [] },
              { text: "Billing Wizard", type: 'BILLING_WIZARD', has_submenu: false, list: [] },
              { text: "Check Deposit", type: 'CHECK_DEPOSIT', has_submenu: false, list: [] }
            ]
          },
          {
            text: "Payment", type: 'PAYMENT', has_submenu: true, list: [
              { text: "Broker Posting", type: 'BROKER_POSTING', has_submenu: false, list: [] },
              { text: "Capitation Posting Wizard", type: 'CAPITATION_POSTING_WIZARD', has_submenu: false, list: [] },
              { text: "Capitation Rates", type: 'CAPITATION_RATES', has_submenu: false, list: [] },
              { text: "Check Packages", type: 'CHECK_PACKAGES', has_submenu: false, list: [] },
              { text: "Claim Posting", type: 'CLAIM_POSTING', has_submenu: false, list: [] },
              { text: "Interest Packages", type: 'INTEREST_PACKAGES', has_submenu: false, list: [] },
              { text: "Payment Packages", type: 'PAYMENT_PACKAGES', has_submenu: false, list: [] },
              { text: "Payment Wizard", type: 'PAYMENT_WIZARD', has_submenu: false, list: [] },
              { text: "Sales Tax Rates", type: 'SALES_TAX_RATES', has_submenu: false, list: [] },
            ]
          }
        ]
      },
      {
        text: "UM",
        has_submenu: true,
        type: "UM",
        list: [
          { text: "Authorizations", type: 'AUTHORIZATIONS', has_submenu: false, list: [] },
          { text: "Open Work Management – Authorizations", type: 'OPEN_WORK_MANAGEMENT_AUTHORIZATIONS', has_submenu: false, list: [] },
          { text: "Quick Authorizations", type: 'QUICK_AUTHORIZATIONS', has_submenu: false, list: [] },
          { text: "Cases-Funded", type: 'CASES_FUNDED', has_submenu: false, list: [] },
          { text: "Cases-Medical", type: 'CASES_MEDICAL', has_submenu: false, list: [] },
          { text: "Cases-Workers Compensations", type: 'CASES_WORKERS_COMPENSATIONS', has_submenu: false, list: [] },
          { text: "Referrals", type: 'REFERRALS', has_submenu: false, list: [] },
          { text: "Referrals Output", type: 'REFERRALS_OUTPUT', has_submenu: false, list: [] },
          { text: "Condition Categories", type: 'CONDITION_CATEGORIES', has_submenu: false, list: [] }
        ]
      },
      {
        text: "WorkFlow",
        has_submenu: true,
        type: "WORKFLOW",
        list: [
          { text: "Document Viewer", type: 'DOCUMENT_VIEWER', has_submenu: false, list: [] },
          { text: "Event Viewer", type: 'EVENT_VIEWER', has_submenu: false, list: [] },
          { text: "Find My Work", type: 'FIND_MY_WORK', has_submenu: false, list: [] },
          { text: "Form Letters", type: 'FORM_LETTERS', has_submenu: false, list: [] },
          { text: "Queue Viewer", type: 'QUEUE_VIEWER', has_submenu: false, list: [] },
          { text: "Time Items", type: 'TIME_ITEMS', has_submenu: false, list: [] },
          { text: "Time Manager", type: 'TIME_MANAGER', has_submenu: false, list: [] },
          {
            text: "Individuals", type: 'INDIVIDUALS', has_submenu: true, list: [
              { text: "Prospective Broker", type: 'PROSPECTIVE_BROKER', has_submenu: false, list: [] },
              { text: "Prospective  Group", type: 'PROSPECTIVE_GROUP', has_submenu: false, list: [] },
              { text: "Prospective Member", type: 'PROSPECTIVE_MEMBER', has_submenu: false, list: [] },
              { text: "Prospective Provider", type: 'PROSPECTIVE_PROVIDER', has_submenu: false, list: [] },
              { text: "Prospective Office", type: 'PROSPECTIVE_OFFICE', has_submenu: false, list: [] },
              { text: "Prospective (General)", type: 'PROSPECTIVE_GENERAL', has_submenu: false, list: [] },
              { text: "Customer", type: 'CUSTOMER', has_submenu: false, list: [] },
            ]
          }
        ]
      },
      {
        text: "Tools",
        has_submenu: true,
        type: "TOOLS",
        list: [
          { text: "Authorization Load Wizard", type: 'AUTHORIZATION_LOAD_WIZARD', has_submenu: false, list: [] },
          { text: "Claim Load Profile", type: 'CLAIM_LOAD_PROFILE', has_submenu: false, list: [] },
          { text: "Claim Load Wizard", type: 'CLAIM_LOAD_WIZARD', has_submenu: false, list: [] },
          { text: "Member Load Wizard", type: 'MEMBER_LOAD_WIZARD', has_submenu: false, list: [] },
          { text: "Operational Reports", type: 'OPERATIONAL_REPORTS', has_submenu: false, list: [] }
        ]
      },
      {
        text: "Help",
        has_submenu: true,
        type: "HELP",
        list: [
          { text: "MediTrac Contents and Index", type: 'MEDITRAC_CONTENTS_AND_INDEX', has_submenu: false, list: [] },
          { text: "MediTrac Shortcuts", type: 'MEDITRAC_SHORTCUTS', has_submenu: false, list: [] },
          { text: "i- Transact Contents and Index", type: 'I_TRANSACT_CONTENTS_AND_INDEX', has_submenu: false, list: [] },
          { text: "HSP Automation User Manual", type: 'HSP_AUTOMATION_USER_MANUAL', has_submenu: false, list: [] },
          { text: "HSP Help", type: 'HSP_HELP', has_submenu: false, list: [] },
          { text: "About Meditrac...", type: 'ABOUT_MEDITRAC', has_submenu: false, list: [] }
        ]
      }
    ]
  }
};
