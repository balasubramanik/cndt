/** Constants for common usage in app */
export const Constants = {
  OFFICE: "Office",
  CONTRACT: "Contract",
  SEARCH_USAGE: "|SEARCH|",
  CONTRACTS: "contracts",
  OFFICES_V2S: "offices_V2s",
  OFFICE_SEARCH: "OfficeSearch",
  CONTRACT_SEARCH: "ContractSearch",
  SET_COLS_CONTRACT: "setColsContract",
  SET_COLS_OFFICE: "setColsOffice",
  KEY_CONTRACT_PAGENO: "ContractPageNumber",
  KEY_OFFICE_PAGENO: "OfficePageNumber",
  KEY_VIEW_DETAILS: "viewDetails",
  KEY_CURRENTOFFICE: "CurrentOffice"
};
