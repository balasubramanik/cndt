/* Sample popup for no permission provider */
import React, { Component } from "react";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
class NoPermissionPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      visible: !prevState.visible
    }));
    this.props.history.push("/")
  }

  render() {
    return (
      <Modal
        backdrop="static"
        isOpen={this.state.visible}
        toggle={this.toggle}
        className="common-popup-size common-popup-position"
      >
        <ModalHeader className="common-popup-header">Meditrac</ModalHeader>
        <ModalBody>
          <div className="d-inline"><img src={process.env.PUBLIC_URL + "/assets/caution_icon.png"} alt="No Permission Caution" /></div>
          <div className="d-inline common-popup-body">Sorry you are not authorized to access this page</div>
          <small className="d-block text-center">Please contact Adminstrator</small>
        </ModalBody>
        <ModalFooter>
          <Button className="common-popup-button" onClick={this.toggle}>
            Ok
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default withRouter(NoPermissionPopup);
