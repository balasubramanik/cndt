/* Sample popup for no permission provider */
import React, { Component } from "react";
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from "reactstrap";
import { withRouter } from "react-router-dom";
class PopupDisconnected extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: true
    };
    this.toggle = this.toggle.bind(this);
    localStorage.removeItem("GetData");
  }

  toggle() {
    this.setState(prevState => ({
      visible: !prevState.visible
    }));
    this.props.history.push("/login");

  }

  render() {
    return (
      <Modal
        backdrop="static"
        isOpen={this.state.visible}
        toggle={this.toggle}
        className="common-popup-size common-popup-position"
      >
        <ModalHeader className="common-popup-header pb-0 mt-3">You have Been Disconnected</ModalHeader>
        <ModalBody>
          <div className="d-inline common-popup-body">
            You have been disconnected from the application by
            a different user logged with the same credentials.
            Your information and changes were saved automatically
            </div>
        </ModalBody>
        <ModalFooter>
          <Button className="common-popup-button" onClick={this.toggle}>
            Go to Login
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default withRouter(PopupDisconnected);
