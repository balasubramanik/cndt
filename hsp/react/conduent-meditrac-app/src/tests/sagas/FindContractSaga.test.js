import sagaHelper from "redux-saga-testing";
import {
  fetchContractData,
  watchContractData
} from "../../saga/FindContractSaga";

describe("Office saga testing", () => {
  const paramsObj = {
    SessionId: "4287",
    OfficeNumber: null,
    OfficeName: "c",
    Address1: null,
    City: null,
    Region: null,
    Zip: null,
    Distance: null,
    State: null,
    County: null,
    Language: null,
    ContactPhone: null,
    AdditionalService: null,
    NPI: null,
    ResultCount: null,
    Usage: "|SEARCH|"
  };
  const action = {
    ...paramsObj,
    callback: () => {}
  };
  const it = sagaHelper(fetchContractData(action));

  it("Test get Contracts data response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchContractData());
  watch("watch Contract Data", () => {});
});
