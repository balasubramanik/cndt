import sagaHelper from "redux-saga-testing";
import {
  fetchSessionIDData,
  watchSessionIDData
} from "../../saga/GetSessionIDSaga";

describe("Fetch Session ID saga testing", () => {
  const paramsObj = {
    username: "admin",
    productName: "Meditrac",
    applicationVersion: "10.07.0.HSP.0.0.173"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(fetchSessionIDData(action));

  it("Test get session ID response", response => {
    expect(response).toBe(response);
  });
});
