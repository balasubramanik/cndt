import sagaHelper from "redux-saga-testing";
import {
  getUserReports,
  watchUserStylesReport
} from "../../saga/OfficeSubViewSaga";

describe("searchlist saga testing", () => {
  const paramsObj = {
    sessionID: "4284",
    usage: "|BYCATEGORYCODE|",
    categoryCode: "OP"
  };
  const action = {
    ...paramsObj,
    callback: () => {}
  };
  const it = sagaHelper(getUserReports(action));

  it("Test set searchlist response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchUserStylesReport());
  watch("watch searchlist", () => {});
});
