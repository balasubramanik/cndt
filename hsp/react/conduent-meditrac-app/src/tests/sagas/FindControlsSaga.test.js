import sagaHelper from "redux-saga-testing";
import {
  fetchControlsData,
  watchControlsData
} from "../../saga/FindControlsSaga";

describe("controls saga testing", () => {
  const paramsObj = {
    sessionId: "4287",
    findCommand: "Contract",
    findProfileId: "0",
    usage: "|ByFindCommand|"
  };
  const action = {
    ...paramsObj,
    callback: () => {}
  };
  const it = sagaHelper(fetchControlsData(action));

  it("Test get conrols response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchControlsData());
  watch("watch Controls Data", () => {});
});
