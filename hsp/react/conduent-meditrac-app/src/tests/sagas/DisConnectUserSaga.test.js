import sagaHelper from "redux-saga-testing";
import {
  fetchDisConnectionData,
  watchDisConnectionData
} from "../../saga/DisConnectUserSaga";

describe("connect user saga testing", () => {
  const paramsObj = {
    sessionID: "4287"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(fetchDisConnectionData(action));

  it("Test disconnect user response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchDisConnectionData());
  watch("watch disconnect user", () => {});
});
