import sagaHelper from "redux-saga-testing";
import {
  saveListViewSettings,
  watchSaveListView
} from "../../saga/SaveListViewSettingSaga";

describe("save listview settings saga testing", () => {
  const paramsObj = {
    FormName: "frmMemberLoadWizard",
    ListView: "lvwUploadedFiles",
    StoredProcedure: "ee_GetMembershipFiles",
    ColumnSettings:
      "&lt;?xml version:1.0?&gt;&lt;Settings&gt;&lt;Columns&gt;&lt;Column&gt;&lt;Name&gt;FileName&lt;/Name&gt;&lt;Location&gt;0&lt;/Location&gt;&lt;Width&gt;59&lt;/Width&gt;&lt;/Column&gt;&lt;Column&gt;&lt;Name&gt;TotalMembers&lt;/Name&gt;&lt;Location&gt;1&lt;/Location&gt;&lt;Width&gt;82&lt;/Width&gt;&lt;/Column&gt;&lt;Column&gt;&lt;Name&gt;InputType&lt;/Name&gt;&lt;Location&gt;2&lt;/Location&gt;&lt;Width&gt;63&lt;/Width&gt;&lt;/Column&gt;&lt;Column&gt;&lt;Name&gt;DateInput&lt;/Name&gt;&lt;Location&gt;3&lt;/Location&gt;&lt;Width&gt;62&lt;/Width&gt;&lt;/Column&gt;&lt;Column&gt;&lt;Name&gt;LastUpdatedBy&lt;/Name&gt;&lt;Location&gt;4&lt;/Location&gt;&lt;Width&gt;91&lt;/Width&gt;&lt;/Column&gt;&lt;Column&gt;&lt;Name&gt;LastUpdatedAt&lt;/Name&gt;&lt;Location&gt;5&lt;/Location&gt;&lt;Width&gt;903&lt;/Width&gt;&lt;/Column&gt;&lt;/Columns&gt;&lt;/Settings&gt;"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(saveListViewSettings(action));

  it("Test save listview settings", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchSaveListView());
  watch("watch save listview settings", () => {});
});
