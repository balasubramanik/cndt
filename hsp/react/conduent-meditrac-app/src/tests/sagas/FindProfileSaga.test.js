import sagaHelper from "redux-saga-testing";
import { fetchProfileData, watchProfileData } from "../../saga/FindProfileSaga";

describe("controls saga testing", () => {
  const paramsObj = {
    findCommandId: "45",
    usage: "|ForFindCommand|"
  };
  const action = {
    ...paramsObj,
    callback: () => {}
  };
  const it = sagaHelper(fetchProfileData(action));

  it("Test get conrols response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchProfileData());
  watch("watch controls", () => {});
});
