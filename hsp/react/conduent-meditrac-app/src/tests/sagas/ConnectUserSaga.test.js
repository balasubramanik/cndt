import sagaHelper from "redux-saga-testing";
import {
  fetchConnectionData,
  watchConnectionData
} from "../../saga/ConnectUserSaga";

describe("connect user saga testing", () => {
  const paramsObj = {
    username: "admin",
    password: "",
    roleName: "",
    appName: "Meditrac",
    version: "10.07.0.HSP.0.0.173",
    usage: "|usage1|"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(fetchConnectionData(action));

  it("Test connect user response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchConnectionData());
  watch("watch connect user", () => {});
});
