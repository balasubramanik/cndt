import sagaHelper from "redux-saga-testing";
import {
  fetchRefCodesData,
  watchRefCodesData
} from "../../saga/GetRefCodesSaga";

describe("Fetch Reference Code saga testing", () => {
  const paramsObj = {
    usage: "|USAGE2|",
    allowCheckout: "N",
    type: "RESULTCOUNT"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(fetchRefCodesData(action));

  it("Test to get resultcount response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchRefCodesData());
  watch("watch resultcount request", () => {});
});
