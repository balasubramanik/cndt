import sagaHelper from "redux-saga-testing";
import {
  fetchRecentSearchData,
  watchRecentSearchData
} from "../../saga/GetRecentSearchSaga";

describe("searchlist saga testing", () => {
  const paramsObj = {
    userID: "1",
    itemType: "OfficeSearch",
    sessionId: "4287"
  };
  const action = {
    ...paramsObj,
    callback: () => {}
  };
  const it = sagaHelper(fetchRecentSearchData(action));

  it("Test set searchlist response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchRecentSearchData());
  watch("watch searchlist", () => {});
});
