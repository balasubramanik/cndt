import sagaHelper from "redux-saga-testing";
import {
  fetchDemographicsData,
  watchDemographicsData
} from "../../saga/DemoGraphicsSaga";

describe("connect user saga testing", () => {
  const paramsObj = {
    sessionID: "4287",
    usage: "|COUNTY|",
    state: "NY"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(fetchDemographicsData(action));

  it("Test disconnect user response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchDemographicsData());
  watch("watch disconnect user", () => {});
});
