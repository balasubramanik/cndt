import sagaHelper from "redux-saga-testing";
import {
  fetchProfileSettingsData,
  watchProfileSettingssData
} from "../../saga/GetProfileSettingsSaga";

describe("Fetch Profile Settings saga testing", () => {
  const paramsObj = {
    sessionId: "4287",
    listviewProfileId: "0",
    listviewName: "Contract",
    usage: "|ForListview|"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(fetchProfileSettingsData(action));

  it("Test get profilesettings response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchProfileSettingssData());
  watch("watch profile settings", () => {});
});
