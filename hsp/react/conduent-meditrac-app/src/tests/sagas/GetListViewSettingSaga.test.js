import sagaHelper from "redux-saga-testing";
import {
  getListViewSettings,
  watchGetListView
} from "../../saga/GetListViewSettingSaga";

describe("Fetch Profile Settings saga testing", () => {
  const paramsObj = {
    sessionId: "4287",
    formName: "OfficeForm",
    listView: "lvContractFind",
    storedProcedure: "ee_GetOffices_v2"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(getListViewSettings(action));

  it("Test get profilesettings response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchGetListView());
  watch("watch profile settings", () => {});
});
