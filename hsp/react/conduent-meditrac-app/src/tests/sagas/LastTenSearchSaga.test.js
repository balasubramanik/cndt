import sagaHelper from "redux-saga-testing";
import {
  setLastTenSearchData,
  getLastTenSearchData,
  setLastData,
  getLastData
} from "../../saga/LastTenSearchSaga";

describe("LastTen search saga testing", () => {
  const paramsObj = {
    userID: "1",
    itemType: "OfficeSearch",
    itemValue: '{"OfficeNumber":"c"}',
    sessionId: "4287"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(setLastTenSearchData(action));

  it("Test set last ten search response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(setLastData());
  watch("watch last ten search list", () => {});
});
