import sagaHelper from "redux-saga-testing";
import { setSearchData, watchSearchData } from "../../saga/SetSearchListSaga";

describe("searchlist saga testing", () => {
  const paramsObj = {
    userID: "1",
    itemType: "OfficeSearch",
    itemValue: '{"OfficeNumber":"c"}',
    sessionId: "4287"
  };
  const action = {
    ...paramsObj
  };
  const it = sagaHelper(setSearchData(action));

  it("Test set searchlist response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchSearchData());
  watch("watch searchlist", () => {});
});
