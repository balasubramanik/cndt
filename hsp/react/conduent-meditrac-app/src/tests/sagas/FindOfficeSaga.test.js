import sagaHelper from "redux-saga-testing";
import { fetchOfficeData, watchOfficeData } from "../../saga/FindOfficeSaga";

describe("Office saga testing", () => {
  const paramsObj = {
    SessionId: "4287",
    OfficeNumber: null,
    OfficeName: "c",
    Address1: null,
    City: null,
    Region: null,
    Zip: null,
    Distance: null,
    State: null,
    County: null,
    Language: null,
    ContactPhone: null,
    AdditionalService: null,
    NPI: null,
    ResultCount: null,
    Usage: "|SEARCH|"
  };
  const action = {
    ...paramsObj,
    callback: () => {}
  };
  const it = sagaHelper(fetchOfficeData(action));

  it("Test get Offices data response", response => {
    expect(response).toBe(response);
  });

  const watch = sagaHelper(watchOfficeData());
  watch("watch Office Data", () => {});
});
