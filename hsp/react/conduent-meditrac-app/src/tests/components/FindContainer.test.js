/**
 * User Story ID: 48837 US1_Provider_Office_Find
 * User Story ID: 49738 US30_Provider_Contract_Find
 * 
 * Description (48837): As a User, I want to go to Find Office After selection on the Providers from the Menu bar, where the Find office is displayed
 * Description (49738): As a User, I want to go to Find Contract After selection on the Providers from the Menu bar, where the Find Contract is displayed.
 *
 * Author: Jai
 */
import React from "react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import { MemoryRouter } from "react-router-dom";
import { mount } from "enzyme";
import { initialState } from "./mock/state";
import FindContainer from "./../../components/containers/FindContainer";

describe("<FindContainer />", () => {
  const match = { params: { providerType: "Contract" } };

  it("should exists", () => {
    const mockStore = configureMockStore([]);
    const store = mockStore(initialState);
    const component = mount(
      <Provider store={store}>
        <MemoryRouter>
          <FindContainer match={match} />
        </MemoryRouter>
      </Provider>
    );
    expect(component.exists()).toBeTruthy();
  });
});
