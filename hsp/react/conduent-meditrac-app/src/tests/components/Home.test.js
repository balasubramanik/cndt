import React from "react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import { mount } from "enzyme";
import { MemoryRouter } from "react-router-dom";
import Home from "../../components/Home";
import { initialState } from "./mock/state";

describe("<Home />", () => {
  it("should mount and exits", () => {
    const mockStore = configureMockStore([]);
    const store = mockStore(initialState);
    const component = mount(
      <Provider store={store}>
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      </Provider>
    );
    expect(component.exists()).toBeTruthy();
  });

  it("should render div", () => {
    const mockStore = configureMockStore([]);
    const store = mockStore(initialState);
    const component = mount(
      <Provider store={store}>
        <MemoryRouter>
          <Home />
        </MemoryRouter>
      </Provider>
    );
    expect(component.find("div").length).toBe(1);
  });
});
