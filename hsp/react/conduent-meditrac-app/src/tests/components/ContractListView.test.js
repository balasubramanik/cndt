/**
 * User Story ID: 48918 US16_Provider_Re-usable Components_List_View_(With Paging)
 * Description: As a User, I want a List View (with paging) as a reusable component so that it display lists, create base list component first and then add features
 * Author: Jai
 */
import React from "react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import { MemoryRouter } from "react-router-dom";
import { mount } from "enzyme";
import { state } from "./mock/ContractListView.helper";
import ContractListView from "../../components/views/ContractListView";

describe("<ContractListView />", () => {
  it("should exists", () => {
    const mockStore = configureStore([]);
    const store = mockStore(state);
    const component = mount(
      <Provider store={store}>
        <MemoryRouter>
          <ContractListView contracts={state.listViewData} />
        </MemoryRouter>
      </Provider>
    );

    expect(component.exists()).toBeTruthy();
  });
});
