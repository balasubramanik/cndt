/**
 * User Story ID: 49738 US30_Provider_Contract_Find
 * 
 * Description (49738): As a User, I want to go to Find Contract After selection on the Providers from the Menu bar, where the Find Contract is displayed.
 *
 * Author: Jai
 */
import React from "react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import { MemoryRouter } from "react-router-dom";
import { mount } from "enzyme";
import { props } from "./mock/ProviderSubContainerContract.helper";
import ProviderSubContainerContract from "./../../components/containers/ProviderSubContainerContract";

describe("<ProviderSubContainerContract />", () => {
  it("should exists", () => {
    const mockStore = configureMockStore([]);
    const store = mockStore(props);

    const component = mount(
      <Provider store={store}>
        <MemoryRouter>
          <ProviderSubContainerContract location={props.location} />
        </MemoryRouter>
      </Provider>
    );
    component.setProps({ location: props.location });
    expect(component.exists()).toBeTruthy();
  });
});
