/**
 * User Story ID: 48911 US9_Provider_Re-usable Components_Stylesheet_Toolbar
 * Description: As a User, I want to display Stylesheet Expanded View, After User makes a selection of record on the List View of office/contracts page
 * Author: Jai
 */
import React from "react";
import configureStore from "redux-mock-store";
import { MemoryRouter } from "react-router-dom";
import { mount, shallow } from "enzyme";
import PageLevelToolbar from "../../components/PageLevelToolbar";
import { props, state } from "./mock/PageLevelToolbar.helper";

describe("<PageLevelToolbar />", () => {
  it("should exists", () => {
    const component = shallow(<PageLevelToolbar />);
    expect(component.exists()).toBeTruthy();
  });
  it("should have list in the state", () => {
    const mockStore = configureStore();
    const store = mockStore(state);
    const component = mount(
      <MemoryRouter>
        <PageLevelToolbar type="OfficeSearch" {...props} store={store} />
      </MemoryRouter>
    );
    component.instance().setState({ list: state.LastTen.lastTen });
    expect(component.instance().state.list.length).toBe(1);
  });
  it("should show last ten search on click on button", () => {
    const mockStore = configureStore();
    const store = mockStore(state);
    const component = mount(
      <MemoryRouter>
        <PageLevelToolbar type="OfficeSearch" {...props} store={store} />
      </MemoryRouter>
    );
    component.instance().setState({ list: state.LastTen.lastTen });
    const lastTenBtn = component.find("button").at(0);
    lastTenBtn.simulate("click");
    expect(component.find(".ccl-recent-search-wrapper").props().hidden).toBe(
      false
    );
  });
  it("should show last ten search on click on button", () => {
    const mockStore = configureStore();
    const store = mockStore(state);
    const component = mount(
      <MemoryRouter>
        <PageLevelToolbar {...props} store={store} />
      </MemoryRouter>
    );
    const lastTenBtn = component.find("button").at(0);
    lastTenBtn.simulate("click");
    component.instance().setState({ list: state.LastTen.lastTen });
    component.setProps({ type: "OfficeSearch" });
    component
      .find("li")
      .at(0)
      .simulate("click", { clickedObj: state.LastTen.lastTen[0] });
    expect(component.props().type).toBe("OfficeSearch");
  });
});
