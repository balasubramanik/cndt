export const props = {
  history: {
    length: 10,
    action: "PUSH",
    location: {
      pathname: "/contractsubview/79/details",
      search: "",
      hash: "",
      key: "44wmwu"
    }
  },
  location: {
    pathname: "/contractsubview/79/details",
    search: "",
    hash: "",
    key: "44wmwu"
  },
  match: {
    path: "/contractsubview/:id+",
    url: "/contractsubview/79/details",
    isExact: true,
    params: { id: "79/details" }
  }
};

export const state = {
  content: {
    contractId: 79,
    contractNumber: "CA001990",
    contractName: "California Healthy Kids Contract",
    capitationOnly: null,
    contractDescription: "California Healthy Kids Contract",
    contractNotes: null,
    effectiveDate: "2015-01-01T00:00:00",
    expirationDate: "9999-12-31T00:00:00",
    negotiated: "ADA",
    pricingMode: "IP",
    toPay: null,
    preEstExpUnits: 12,
    preEstExpUnitType: "MTH",
    rvsMultiplier: 0,
    capitationRateId: null,
    capitationRateName: null,
    lastPostedDate: "0001-01-01T00:00:00",
    retroactiveAddLimit: null,
    retroactiveTermLimit: null,
    postingCutoffDay: null,
    capitationCurrentPeriod: "0001-01-01T00:00:00",
    useHCPCSWhenFindFeeSchedule: "N",
    feeScheduleSearchCriteria: "SRV",
    applyCopayPerSchedule: "CLC",
    paymentClass: "REG",
    suppressPayment: "N",
    providerInheritsMemberPayment: "Y",
    contractType: "DEF",
    contractTypeName: "Default",
    lastUpdatedAt: "2018-04-19T16:37:12.58",
    lastUpdatedBy: "Einolf, Jill",
    asOfDate: "0001-01-01T00:00:00",
    reimbursementID: null,
    panelSizeEnforcement: "DAA",
    panelSizeCalculationMethod: "PPO",
    futurePCPLookbackPeriod: null,
    primaryPCPLookbackPeriod: null,
    moneyLimitProcessingOrder: "ALI"
  },
  modalMessage: false,
  selected: "01"
};
