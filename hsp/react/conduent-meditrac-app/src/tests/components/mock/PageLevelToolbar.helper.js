export const props = {
  history: {
    length: 5,
    action: "PUSH",
    location: {
      pathname: "/contractsubview/31/details",
      state: {
        detail: '{applyCopayPerSchedule: "CLC", asOfDate: "0001-01-0…}'
      },
      search: "",
      hash: "",
      key: "0enujr"
    },
    createHref: "ƒ M() {}",
    push: "ƒ push() {}",
    replace: "ƒ replace() {}",
    go: "ƒ A() {}",
    goBack: "ƒ goBack() {}",
    goForward: "ƒ goForward() {}",
    block: "ƒ block() {}",
    listen: "ƒ listen() {}"
  },
  location: {
    pathname: "/contractsubview/31/details",
    state: "{detail: {…}}",
    search: "",
    hash: "",
    key: "0enujr"
  },
  match: {
    path: "/contractsubview/:id+",
    url: "/contractsubview/31/details",
    isExact: true,
    params: '{id: "31/details"}'
  }
};

export const state = {
  LastTen: {
    sessionId: "4287",
    userID: "1",
    lastTen: [
      {
        addedTime: "2020-05-04T05:59:05.781+00:00",
        itemType: "OfficeSearch",
        itemValue: {
          applyCopayPerSchedule: "CLC",
          asOfDate: "0001-01-01T00:00:00",
          capitationCurrentPeriod: "2016-01-01T00:00:00",
          capitationOnly: null,
          capitationRateId: 17,
          capitationRateName: "Test Capitation Rate - Standard",
          contractDescription: "Capitation Contract 2",
          contractId: 70,
          contractName: "Capitation Contract 2",
          contractNotes: null,
          contractNumber: "CAPCONT2",
          contractType: "DEF",
          contractTypeName: "Default",
          effectiveDate: "1900-01-01T00:00:00",
          expirationDate: "9999-12-31T00:00:00",
          feeScheduleSearchCriteria: "SRV",
          futurePCPLookbackPeriod: null,
          lastPostedDate: "2015-12-01T00:00:00",
          lastUpdatedAt: "2017-07-11T13:00:32.307",
          lastUpdatedBy: "Fonte, Angela",
          moneyLimitProcessingOrder: "ALI",
          negotiated: "ADA",
          panelSizeCalculationMethod: "PPO",
          panelSizeEnforcement: "DAA",
          paymentClass: "REG",
          postingCutoffDay: 1,
          preEstExpUnitType: "MTH",
          preEstExpUnits: 12,
          pricingMode: "IP",
          primaryPCPLookbackPeriod: null,
          providerInheritsMemberPayment: "Y",
          reimbursementID: null,
          retroactiveAddLimit: 3,
          retroactiveTermLimit: 6,
          rvsMultiplier: 0,
          suppressPayment: "N",
          toPay: null,
          useHCPCSWhenFindFeeSchedule: "N"
        }
      }
    ]
  }
};
