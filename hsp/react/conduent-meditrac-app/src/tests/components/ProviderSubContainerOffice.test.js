/**
 * User Story ID: 49738 US30_Provider_Contract_Find
 *
 * Description (49738): As a User, I want to go to Find Contract After selection on the Providers from the Menu bar, where the Find Contract is displayed.
 *
 * Author: Jai
 */
import React from "react";
import configureMockStore from "redux-mock-store";
import { MemoryRouter } from "react-router-dom";
import { mount } from "enzyme";
import { props } from "./mock/ProviderSubContainerOffice.helper";
import ProviderSubContainerOffice from "./../../components/containers/ProviderSubContainerOffice";

describe("<ProviderSubContainerOffice />", () => {
  it("should exists", () => {
    const mockStore = configureMockStore([]);
    const store = mockStore({
      getState: () => ({
        history: props.history,
        location: props.location,
        match: props.match
      }),
      dispatch: () => {},
      subscribe: () => {}
    });
    const component = mount(
      <MemoryRouter>
        <ProviderSubContainerOffice store={store} match={props.match} />
      </MemoryRouter>
    );
    component.children().update();
    // component.props().children.props().match = props.match;
    // component.props().children.history.push(props.match.url);
    console.log(component.props().children);
    console.log(component.props().children.props);
    expect(component.exists()).toBeTruthy();
  });
});
