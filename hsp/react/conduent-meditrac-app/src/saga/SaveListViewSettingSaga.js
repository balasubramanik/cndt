import { put, call, takeLatest } from "redux-saga/effects";

import * as saveListViewSettingApi from "../api/saveListViewSettingApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* saveListViewSettings(action) {
  try {
    const response = yield call(
      saveListViewSettingApi.saveListViewSetting,
      action.paramsObj
    );

    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_SAVE_LISTVIEW_SETTING,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_SAVE_LISTVIEW_SETTING,
      error: e
    });
  }
}

export function* watchSaveListView() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.SAVE_LISTVIEW_SETTING,
    saveListViewSettings
  );
}
