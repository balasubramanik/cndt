/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as GetRefCodesApi from "../api/GetRefCodesApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchRefCodesData(action) {
  try {
    const response = yield call(GetRefCodesApi.getRefCodes, action.paramsObj);
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_REFCODES_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_REFCODES_DATA,
      error: e
    });
  }
}

export function* watchRefCodesData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_REFCODES, fetchRefCodesData);
}
