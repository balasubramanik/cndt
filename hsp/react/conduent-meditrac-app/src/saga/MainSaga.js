/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 * This is to combine all saga files used and to export
 */
import { all } from "redux-saga/effects";
import * as FindCommandsSaga from "./FindCommandsSaga";
import * as FindOfficeSaga from "./FindOfficeSaga";
import * as FindContractSaga from "./FindContractSaga";
import * as FindControlsSaga from "./FindControlsSaga";
import * as GetRefCodesSaga from "./GetRefCodesSaga";
import * as ConnectUserSaga from "./ConnectUserSaga";
import * as DisConnectUserSaga from "./DisConnectUserSaga";
import * as FindProfileSaga from "./FindProfileSaga";
import * as GetProfileSettingsSaga from "./GetProfileSettingsSaga";
import * as GetDemographicsSaga from "./DemoGraphicsSaga";
import * as GetRecentSearchSaga from "./GetRecentSearchSaga";
import * as SetSearchListSaga from "./SetSearchListSaga";
import * as ProviderSubView from "./ProviderSubViewSaga";
import * as OfcSubviewSaga from "./OfficeSubViewSaga";
import * as LastTenSearch from "./LastTenSearchSaga";
import * as SaveListViewSetting from "./SaveListViewSettingSaga";
import * as GetListViewSetting from "./GetListViewSettingSaga";
import * as UserLoginSaga from "./UserLoginSaga";
import * as GetPermissionSaga from "./GetPermissionSaga";
import * as GetCountriesSaga from "./GetCountriesSaga";
export function* appMainSaga() {
  yield all([
    FindCommandsSaga.watchAllCommandsData(),
    UserLoginSaga.watchUserLogin(),
    FindOfficeSaga.watchOfficeData(),
    FindOfficeSaga.watchClearOfficeData(),
    FindContractSaga.watchContractData(),
    FindContractSaga.watchClearContractData(),
    FindControlsSaga.watchControlsData(),
    GetRefCodesSaga.watchRefCodesData(),
    FindProfileSaga.watchProfileData(),
    ConnectUserSaga.watchConnectionData(),
    DisConnectUserSaga.watchDisConnectionData(),
    GetProfileSettingsSaga.watchProfileSettingssData(),
    GetDemographicsSaga.watchDemographicsData(),
    GetRecentSearchSaga.watchRecentSearchData(),
    SetSearchListSaga.watchSearchData(),
    ProviderSubView.watchUserStylesReport(),
    ProviderSubView.watchXMLBROWSER(),
    OfcSubviewSaga.watchUserStylesReport(),
    OfcSubviewSaga.watchXMLBROWSER(),
    OfcSubviewSaga.watchRefCode(),
    LastTenSearch.setLastData(),
    LastTenSearch.getLastData(),
    SaveListViewSetting.watchSaveListView(),
    GetListViewSetting.watchGetListView(),
    GetPermissionSaga.watchPermission(),
    GetCountriesSaga.watchGetCountriesData()
  ]);
}
