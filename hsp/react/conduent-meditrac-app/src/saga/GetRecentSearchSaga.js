/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 *
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as GetRecentSearchApi from "../api/GetRecentSearchApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchRecentSearchData(action) {
  try {
    const response = yield call(
      GetRecentSearchApi.getRecentSearch,
      action.paramsObj
    );
    if (action.callback) {
      action.callback(response.data);
    }
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_RECENTSEARCH_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_RECENTSEARCH_DATA,
      error: e
    });
  }
}

export function* watchRecentSearchData() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.FETCH_RECENTSEARCH,
    fetchRecentSearchData
  );
}
