/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as contractsApi from "../api/contractsApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchContractData(action) {
  try {
    const response = yield call(contractsApi.findContracts, action.paramsObj);
    if (action.callback) {
      action.callback(response.data);
    }
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_CONTRACT_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_CONTRACT_DATA,
      error: e
    });
  }
}
export function* clearContractData(action) {
  try {
    yield put({
      type: ACTION_TYPES.UI_ACTION.RECEIVE_CLEAR_CONTRACT,
      data: []
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.RECEIVE_CLEAR_CONTRACT,
      error: e
    });
  }
}
export function* watchContractData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_CONTRACT, fetchContractData);
}
export function* watchClearContractData() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.REQUEST_CLEAR_CONTRACT,
    clearContractData
  );
}
