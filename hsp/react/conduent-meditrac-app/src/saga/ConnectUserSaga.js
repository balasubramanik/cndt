/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as ConnectUserApi from "../api/ConnectUserApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchConnectionData(action) {
  try {
    const response = yield call(
      ConnectUserApi.getConnectUser,
      action.paramsObj
    );
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_CONNECT_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_CONNECT_DATA,
      error: e
    });
  }
}

export function* watchConnectionData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_CONNECT, fetchConnectionData);
}
