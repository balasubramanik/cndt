/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";
import {
  receiveLastTenSearch
} from "./../actions/LastTenSearchAction";
import * as SetSearchListApi from "../api/lastTenSearchAPI";
import ACTION_TYPES from './../constants/ACTION_TYPES';

export function* setLastTenSearchData(action) {
  try {
    yield call(SetSearchListApi.setLastTensearch, action.paramsObj);
    if (action.callback) {
      action.callback();
    }
  } catch (e) {}
}
export function* getLastTenSearchData(action) {
  try {
    const data = yield call(
      SetSearchListApi.getLastTensearch,
      action.paramsObj
    );
    if (action.callback) {
      action.callback(data.data);
    }
    yield put(receiveLastTenSearch(data.data));
  } catch (e) {}
}
export function* setLastData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_SET_LAST_TEN_SEARCH, setLastTenSearchData);
}
export function* getLastData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_GET_LAST_TEN_SEARCH, getLastTenSearchData);
}
