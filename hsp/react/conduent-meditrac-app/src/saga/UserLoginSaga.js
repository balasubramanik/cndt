/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 *
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as userLogin from "../api/LoginApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* getUserLogin(action) {
  try {
    const response = yield call(userLogin.userLogin, action.payload);
    yield put({
      type: ACTION_TYPES.UI_ACTION.USER_LOGIN,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.USER_LOGIN,
      error: e
    });
  }
}

export function* disconnectUser(action) {
  try {
    const response = yield call(userLogin.userDisconnect, action.payload);
    yield put({
      type: ACTION_TYPES.UI_ACTION.USER_DISCONNECT,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.USER_DISCONNECT,
      error: e
    });
  }
}

export function* watchUserLogin() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_USER_LOGIN, getUserLogin);
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_USER_DISCONNECT, disconnectUser);
}
