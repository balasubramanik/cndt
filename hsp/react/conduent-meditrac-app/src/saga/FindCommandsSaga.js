/**
 * Middleware usage 'redux-saga' to get all the Commands for data fetching 
 * from the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";
import * as GetFindCommandsApi from "../api/GetFindCommandsApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchAllCommands() {
  try {
    const response = yield call(GetFindCommandsApi.getFindCommands);
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_FIND_COMMANDS,
      data: response.data.findCommands.findCommands
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_FIND_COMMANDS,
      error: e
    });
  }
}

export function* watchAllCommandsData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FIND_COMMANDS, fetchAllCommands);
}
