/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as getSessionIDApi from "../api/GetSessionIDApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchSessionIDData(action) {
  try {
    const response = yield call(getSessionIDApi.getSessionID, action.paramsObj);
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_SESSIONID_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_SESSIONID_DATA,
      error: e
    });
  }
}

export function* watchSessionIDData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_SESSIONID, fetchSessionIDData);
}
