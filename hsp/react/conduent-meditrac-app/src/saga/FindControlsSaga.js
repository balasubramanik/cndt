/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 *
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as controlsApi from "../api/controlsApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchControlsData(action) {
  try {
    const response = yield call(controlsApi.getControls, action.paramsObj);
    if (action.callback) {
      action.callback(response.data);
    }
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_CONTROLS_DATA,
      data: response.data
    });
    if (action.paramsObj.findCommand === "Office") {
      const parentCascadings = response.data.findControls.filter(
        obj => obj.linkTo
      );
      const parentObject = {};
      if (parentCascadings) {
        parentObject[parentCascadings[0].name] = parentCascadings[0].comboData;
        yield put({
          type: ACTION_TYPES.UI_ACTION.SET_PARENT_DROPDOWN_DATA,
          data: [parentObject]
        });
      }
    }
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_CONTROLS_DATA,
      error: e
    });
  }
}

export function* watchControlsData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_CONTROLS, fetchControlsData);
}
