/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as getCountriesApi from "../api/getCountriesApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchCountriesData(action) {
    try {
        const response = yield call(getCountriesApi.getCountries, action.paramsObj);
        if (action.callback) {
            action.callback(response.data);
        }
    } catch (e) {

    }
}

export function* watchGetCountriesData() {
    yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_COUNTRIES, fetchCountriesData);
}
