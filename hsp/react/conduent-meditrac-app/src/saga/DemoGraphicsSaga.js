/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest, select } from "redux-saga/effects";
import * as GetDemographicsApi from "../api/GetDemographicsApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchDemographicsData(action) {
  try {
    const response = yield call(
      GetDemographicsApi.getDemographics,
      action.paramsObj
    );
    const cascadeList = state => {
      return state.controlsActions.cascadeDropdownsList;
    };
    const list = yield select(cascadeList);
    if (
      response.data.demographicsFromZipDTOs &&
      response.data.demographicsFromZipDTOs.length
    ) {
      const demographicsArray = list.filter(
        obj => Object.keys(obj)[0] !== action.linkTo
      );
      demographicsArray.push({
        [action.linkTo]: response.data.demographicsFromZipDTOs
      });
      yield put({
        type: ACTION_TYPES.UI_ACTION.SET_PARENT_DROPDOWN_DATA,
        data: demographicsArray
      });
    }
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.SET_PARENT_DROPDOWN_DATA,
      error: e
    });
  }
}

export function* watchDemographicsData() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.FETCH_DEMOGRAPHICS,
    fetchDemographicsData
  );
}
