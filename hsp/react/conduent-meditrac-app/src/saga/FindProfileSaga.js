/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 *
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as ProfileApi from "../api/ProfileApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchProfileData(action) {
  try {
    const response = yield call(ProfileApi.findProfile, action.paramsObj);
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_PROFILE_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_PROFILE_DATA,
      error: e
    });
  }
}

export function* watchProfileData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_PROFILE, fetchProfileData);
}
