/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 *
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as OfficeApi from "../api/OfficeApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchOfficeData(action) {
  try {
    const response = yield call(OfficeApi.findOffice, action.paramsObj);
    if (action.callback) {
      action.callback(response.data);
    }
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_OFFICE_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_OFFICE_DATA,
      error: e
    });
  }
}
export function* clearOfficeData(action) {
  try {
    yield put({
      type: ACTION_TYPES.UI_ACTION.RECEIVE_CLEAR_OFFICE,
      data: []
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.RECEIVE_CLEAR_OFFICE,
      error: e
    });
  }
}
export function* watchOfficeData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_OFFICE, fetchOfficeData);
}
export function* watchClearOfficeData() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.REQUEST_CLEAR_OFFICE,
    clearOfficeData
  );
}
