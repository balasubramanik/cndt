/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as DisConnectUserApi from "../api/DisConnectUserApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchDisConnectionData(action) {
  try {
    const response = yield call(
      DisConnectUserApi.getDisConnectUser,
      action.paramsObj
    );
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_DISCONNECT_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_DISCONNECT_DATA,
      error: e
    });
  }
}

export function* watchDisConnectionData() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.FETCH_DISCONNECT,
    fetchDisConnectionData
  );
}
