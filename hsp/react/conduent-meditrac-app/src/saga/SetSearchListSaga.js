/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as SetSearchListApi from "../api/SetSearchListApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* setSearchData(action) {
  try {
    const response = yield call(
      SetSearchListApi.setSearchData,
      action.paramsObj
    );
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_SEARCHLIST_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_SEARCHLIST_DATA,
      error: e
    });
  }
}

export function* watchSearchData() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.FETCH_SEARCHLIST, setSearchData);
}
