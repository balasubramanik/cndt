/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { call, put, takeLatest } from "redux-saga/effects";

import {
  receiveApiDataStylesOfc,
  receiveApiXMLBROWSEROfc
} from "../actions/OfficeSubView";
import ACTION_TYPES from './../constants/ACTION_TYPES';
import {
  fetchUserReportsStyles,
  fetchXMLBrowser,
  fetchRefcodes
} from "../api/ProviderSubViewApi";

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
export function* getUserReports(action) {
  try {
    // do api call
    const data = yield call(fetchUserReportsStyles, action.paramsObj);
    if (action.callback) {
      action.callback(data.data);
    }
    yield put(receiveApiDataStylesOfc(data.data));
  } catch (e) {
    console.log(e);
  }
}
export function* getXMLBrowser(action) {
  try {
    // do api call
    const data = yield call(fetchXMLBrowser, action.paramsObj);
    if (action.callback) {
      action.callback(data.data);
    }
    yield put(receiveApiXMLBROWSEROfc(data.data));
  } catch (e) {
    console.log(e);
  }
}
export function* getRefCodes(action) {
  try {
    // do api call
    const data = yield call(fetchRefcodes, action.paramsObj);
    if (action.callback) {
      action.callback(data.data);
    }
  } catch (e) {
    console.log(e);
  }
}
export function* watchUserStylesReport() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES_OFC, getUserReports);
}
export function* watchXMLBROWSER() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER_OFC, getXMLBrowser);
}
export function* watchRefCode() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_REF_CODES, getRefCodes);
}
