/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 *
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as ProfileSettingsApi from "../api/ProfileSettingsApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* fetchProfileSettingsData(action) {
  try {
    const response = yield call(
      ProfileSettingsApi.getProfileSettings,
      action.paramsObj
    );
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_PROFILESETTINGS_DATA,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_PROFILESETTINGS_DATA,
      error: e
    });
  }
}

export function* watchProfileSettingssData() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.FETCH_REFCODES,
    fetchProfileSettingsData
  );
}
