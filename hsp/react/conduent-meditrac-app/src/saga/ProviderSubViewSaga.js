/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { call, put, takeLatest } from "redux-saga/effects";

import {
  receiveApiDataStyles,
  receiveApiXMLBROWSER
} from "./../actions/ProviderSubViewAction";
import ACTION_TYPES from './../constants/ACTION_TYPES';
import {
  fetchUserReportsStyles,
  fetchXMLBrowser
} from "./../api/ProviderSubViewApi";

// worker Saga: will be fired on USER_FETCH_REQUESTED actions
export function* getUserReports(action) {
  try {
    // do api call
    const data = yield call(fetchUserReportsStyles, action.paramsObj);
    //  console.log(data)
    if (action.callback) {
      action.callback(data.data);
    }
    yield put(receiveApiDataStyles(data.data));
  } catch (e) {
    console.log(e);
  }
}
export function* getXMLBrowser(action) {
  try {
    // do api call
    const data = yield call(fetchXMLBrowser, action.paramsObj);
    if (action.callback) {
      action.callback(data.data);
    }
    yield put(receiveApiXMLBROWSER(data.data));
  } catch (e) {
    console.log(e);
  }
}
export function* watchUserStylesReport() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES, getUserReports);
}
export function* watchXMLBROWSER() {
  yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER, getXMLBrowser);
}
// export default function* watchUserStylesReport() {
//   yield takeLatest(REQUEST_API_DATA_STYLES, getUserReports);
// }
