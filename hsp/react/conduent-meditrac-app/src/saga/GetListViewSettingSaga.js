import { put, call, takeLatest } from "redux-saga/effects";

import * as getListViewSettingApi from "../api/getListViewSettingApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* getListViewSettings(action) {
  try {
    const response = yield call(
      getListViewSettingApi.getListViewSetting,
      action.paramsObj
    );
    if (action.callback) {
      action.callback(response.data);
    }
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_GET_LISTVIEW_SETTING,
      data: response.data
    });
  } catch (e) {
    yield put({
      type: ACTION_TYPES.UI_ACTION.ON_GET_LISTVIEW_SETTING,
      error: e
    });
  }
}

export function* watchGetListView() {
  yield takeLatest(
    ACTION_TYPES.UI_ACTION.GET_LISTVIEW_SETTING,
    getListViewSettings
  );
}
