/**
 * Middleware usage 'redux-saga' to connect to the Orchestration layer
 */
import { put, call, takeLatest } from "redux-saga/effects";

import * as getPermissionAPI from "../api/getPermissionApi";
import ACTION_TYPES from "../constants/ACTION_TYPES";

export function* getPermission(action) {
    try {
        const response = yield call(
            getPermissionAPI.getPermission,
            action.paramsObj
        );
        yield put({
            type: ACTION_TYPES.UI_ACTION.RECEIVE_PERMISSION,
            data: response.data
        });
    } catch (e) {
        yield put({
            type: ACTION_TYPES.UI_ACTION.RECEIVE_PERMISSION,
            data: {
                permissions: {
                    office: false,
                    contract: false
                }
            },
            error: e
        });
    }
}

export function* watchPermission() {
    yield takeLatest(ACTION_TYPES.UI_ACTION.REQUEST_PERMISSION, getPermission);
}