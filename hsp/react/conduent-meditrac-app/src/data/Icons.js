import React from 'react';

export const submenu = (<svg className="submenu-arrow"  width="8" height="8" viewBox="0 0 8 14"><path fill="#000" fillRule="nonzero" d="M-4.91 0L-6 1.002 1.046 8 8 1 6.81 0 1.046 5.952z"/></svg>);