/**
 * Application file to route the respective page while hitting HSP URL
 *  and on navigation of Office/ Contract  Find or View details pages
 */
import React, { Component } from "react";
import { Switch, BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store/store";
import Router from "./Router";
import APILIST from "./api/apiList";
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter basename={APILIST.baseURL}>
          <Switch>
            <Router />
          </Switch>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
