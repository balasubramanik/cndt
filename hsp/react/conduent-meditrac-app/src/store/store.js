/**
 * Create Middleware using 'redux-saga' to connect to the Orchestration layer
 * and create store to maintain state of props
 */
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import reducers from "./../reducers";
import { appMainSaga } from "../saga/MainSaga";
// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
const loadState = key => {
  try {
    const serializedStage = localStorage.getItem(key);
    if (serializedStage === null) {
      return undefined;
    }
    return JSON.parse(serializedStage);
  } catch (err) {
    return undefined;
  }
};

const saveState = (key, state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(key, serializedState);
  } catch (e) {}
};
const persistedState = loadState("state");
// mount it on the Store
const store = createStore(
  reducers,
  persistedState,
  applyMiddleware(sagaMiddleware)
);
store.subscribe(() => {
  saveState("state", store.getState());
});
window.store_ref = store;
// then run the saga
sagaMiddleware.run(appMainSaga);
export default store;
