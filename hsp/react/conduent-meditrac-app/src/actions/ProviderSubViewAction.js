/**
 * User_story: US31_Provider_Contract_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of Contract After a particular
 * contract is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
/* Actions items for Office in  View Details Page */
import ACTION_TYPES from './../constants/ACTION_TYPES';

export const requestApiDataStyles = () => ({ 
  type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES 
});
export const receiveApiDataStyles = data => ({
  type: ACTION_TYPES.UI_ACTION.RECEIVE_API_DATA_STYLES,
  data
});

export const requestApiXMLBROWSER = () => ({
  type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER
});
export const receiveApiXMLBROWSER = data => ({
  type: ACTION_TYPES.UI_ACTION.RECEIVE_API_DATA_XMLBROWSER,
  data
});
