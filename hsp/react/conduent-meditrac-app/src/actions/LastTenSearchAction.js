/* List of Actions items for Last Ten Search */
import ACTION_TYPES from './../constants/ACTION_TYPES';

export const requestSetLastTenSearch = () => ({
  type: ACTION_TYPES.UI_ACTION.REQUEST_SET_LAST_TEN_SEARCH
});
export const requestGetLastTenSearch = () => ({
  type: ACTION_TYPES.UI_ACTION.REQUEST_GET_LAST_TEN_SEARCH
});
export const receiveLastTenSearch = data => ({
  type: ACTION_TYPES.UI_ACTION.RECEIVE_LAST_TEN_SEARCH,
  data
});
