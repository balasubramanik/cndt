/**
 * User_story: US3_Provider_Office_Details
 *
 * Description:
 * ================================================================
 * As a User, I want to View Details of a record After a particular
 * office is selected in the Find Control
 * ================================================================
 *
 * Author: Birendranath
 *
 */
/* Actions items for Styles in  View Details Page */
import ACTION_TYPES from './../constants/ACTION_TYPES';

export const requestApiDataStylesOfc = () => ({
  type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_STYLES_OFC
});
export const receiveApiDataStylesOfc = data => ({
  type: ACTION_TYPES.UI_ACTION.RECEIVE_API_DATA_STYLES_OFC,
  data
});

export const requestApiXMLBROWSEROfc = () => ({
  type: ACTION_TYPES.UI_ACTION.REQUEST_API_DATA_XMLBROWSER_OFC
});
export const receiveApiXMLBROWSEROfc = data => ({
  type: ACTION_TYPES.UI_ACTION.RECEIVE_API_DATA_XMLBROWSER_OFC,
  data
});
