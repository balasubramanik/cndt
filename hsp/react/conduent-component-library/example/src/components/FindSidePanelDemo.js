import React from "react";
import { FindSidePanel } from "conduent-component-library";

class FindSidePanelDemo extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      findProfiles: []
    }
  }
  componentDidMount(){

    /* const findCommandId = result.findCommands[0].findCommandId;  // 45 Contract
    const paramsFindProfiles = {
      sessionId: "4342",
      Usage: "|ForFindCommand|",
      findCommandId: findCommandId
    }; */
      const { url, usage, findCommandId  } = this.props.data;
      fetch(url)
      .then(res => res.json())
      .then(
        (result) => {
          
          this.setState({findProfiles: result})
        },
        (error) => {
          console.log(error);
        }
      )
    
  }
  render(){
    return(
      <React.Fragment>
        <h3>Additional Side Panel</h3>
        <div>
        <FindSidePanel findProfiles={this.state.findProfiles} />
        </div>
      </React.Fragment>
    );
  }
}
export default FindSidePanelDemo;