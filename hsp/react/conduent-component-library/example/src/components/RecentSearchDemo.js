import React from "react";
import { RecentSearch } from "conduent-component-library";

export const RecentSearchDemo = () => {
  
  return(
    <React.Fragment>
        <h3>Recent Search</h3>
        <div>
        <RecentSearch />
        </div>
    </React.Fragment>
  );
}