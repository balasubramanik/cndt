import React from 'react';
import { ListView } from "conduent-component-library";

class ListViewDemo extends React.Component {
    constructor(props) {
        super(props);
        this.colOrder = []
        this.reSizer = []
        this.state = {
            jsondata: [],
            columns: [],
            columnOrder: [],
            setCount: 0,
        };
    }
    componentDidMount() {
        let col = [
            { "Header": "Contract Id", "accessor": "contractId" },
            { "width": 350, "format": "string", "Header": "Contract Number", "accessor": "contractNumber", "noDrag": "true" },
            { "Header": "contractName", "accessor": "contractName", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "capitationOnly", "accessor": "capitationOnly", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "contractDescription", "accessor": "contractDescription", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "contractNotes", "accessor": "contractNotes", "noDrag": "true" },
            { "width": 150, "format": "Date", "dataformat": "MM/DD/YYYY", "Header": "effectiveDate", "accessor": "effectiveDate", "noDrag": "true" },
            { "width": 150, "format": "Date", "dataformat": "MM/DD/YYYY", "Header": "expirationDate", "accessor": "expirationDate", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "negotiated", "accessor": "negotiated", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "pricingMode", "accessor": "pricingMode", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "toPay", "accessor": "toPay", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "preEstExpUnits", "accessor": "preEstExpUnits", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "preEstExpUnitType", "accessor": "preEstExpUnitType", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "rvsMultiplier", "accessor": "rvsMultiplier", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "capitationRateId", "accessor": "capitationRateId", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "capitationRateName", "accessor": "capitationRateName", "noDrag": "true" },
            { "width": 150, "format": "Date", "dataformat": "MM/DD/YYYY", "Header": "lastPostedDate", "accessor": "lastPostedDate", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "retroactiveAddLimit", "accessor": "retroactiveAddLimit", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "retroactiveTermLimit", "accessor": "retroactiveTermLimit", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "postingCutoffDay", "accessor": "postingCutoffDay", "noDrag": "true" },
            { "width": 150, "format": "Date", "dataformat": "MM/DD/YYYY", "Header": "capitationCurrentPeriod", "accessor": "capitationCurrentPeriod", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "useHCPCSWhenFindFeeSchedule", "accessor": "useHCPCSWhenFindFeeSchedule", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "feeScheduleSearchCriteria", "accessor": "feeScheduleSearchCriteria", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "applyCopayPerSchedule", "accessor": "applyCopayPerSchedule", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "paymentClass", "accessor": "paymentClass", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "suppressPayment", "accessor": "suppressPayment", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "providerInheritsMemberPayment", "accessor": "providerInheritsMemberPayment", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "contractType", "accessor": "contractType", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "contractTypeName", "accessor": "contractTypeName", "noDrag": "true" },
            { "width": 150, "format": "DateTime", "dataformat": "MM/DD/YYYY HH:mm:ss", "Header": "lastUpdatedAt", "accessor": "lastUpdatedAt", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "lastUpdatedBy", "accessor": "lastUpdatedBy", "noDrag": "true" },
            { "width": 150, "format": "Date", "dataformat": "MM/DD/YYYY", "Header": "asOfDate", "accessor": "asOfDate", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "reimbursementID", "accessor": "reimbursementID", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "panelSizeEnforcement", "accessor": "panelSizeEnforcement", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "panelSizeCalculationMethod", "accessor": "panelSizeCalculationMethod", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "futurePCPLookbackPeriod", "accessor": "futurePCPLookbackPeriod", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "primaryPCPLookbackPeriod", "accessor": "primaryPCPLookbackPeriod", "noDrag": "true" },
            { "width": 150, "format": "string", "Header": "moneyLimitProcessingOrder", "accessor": "moneyLimitProcessingOrder", "noDrag": "true" }
        ]
        fetch('http://localhost:3000/json/contracts.json')
            .then(res => res.json())
            .then((data) => {
                let arr = [];
                arr = data["contracts"];
                this.setState({ jsondata: arr, columns: col })
            })
            .catch(error => console.log(error))
    }

    async postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

    setUserRestoreOption(params) {
        this.postData('http://localhost:5000/api/setUserListViewSetting', params)
            .then((data) => {
                if (!data.status) {
                    let userOption = data.status;
                    alert("Add User Listview Setting Id :" + data.addUserListviewSetting[0].userListviewSettingId);
                }
            });
    }

    SaveColumns = (reSizer) => {
        let SaveColDetails = this.colOrder;
        if (reSizer.length > 0) {
            reSizer[0].map((index, key) => {
                const _accessor = index.id;
                SaveColDetails.find(obj => obj.accessor === _accessor).width = index.value;
            })
        }
        let jsonData = {
            "sessionID": "124",
            "FormName": "contract_new",
            "ListView": "lvwUploadedFiles",
            "StoredProcedure": "ee_GetMembershipFiles",
            "ColumnSettings": this.OBJtoXML(this.colOrder)
        }
        this.setUserRestoreOption(jsonData)
    }

    OBJtoXML(obj) {
        let xml = ""
        obj.map((index, key) => {
            xml = xml + "&lt;Column&gt;&lt;Name&gt;" + index.accessor + "&lt;/Name&gt;&lt;Location&gt;" + key + "&lt;/Location&gt;&lt;Width&gt;" + index.width + "&lt;/Width&gt;&lt;/Column&gt;";
        })
        let rtn = "&lt;?xml version:1.0?&gt;&lt;Settings&gt;&lt;Columns&gt;" + xml + "&lt;/Columns&gt;&lt;/Settings&gt;"
        return (rtn)
    }

    SelectedRow = (e, rowInfo) => {
        //Selected Row Details 
    }

    columnOrderData = (cols) => {
        //Column ReOrder Details Details
        this.colOrder = cols || this.state.columns;
    }

    OnStyleSheetClick = (e,rowInfo) => {
        //Style Sheet Click Event
    }
    OnOpenInNewWindowClick = (e,rowInfo) => {
        //New Window Click Event
    }
    OnOpenInNewTabClick = (e,rowInfo) => {
        //New Tab Click Event
    }

    render() {
        return (
            <div>
                {this.state.jsondata.length ?
                    <React.Fragment>
                        <ListView
                            ListViewTitle="Contract Name"
                            rows={this.state.jsondata}
                            columns={this.state.columns}
                            defaultPageSize={10}
                            minRows={1}
                            FindControllId=""
                            Name=""
                            FindType=""
                            SortKey=""
                            className="-striped -highlight"
                            selectedIndex={this.SelectedRow}
                            columnOrder={this.columnOrderData}
                            SaveColumns={this.SaveColumns}
                            SerialNoRequired={true}
                            DisplayIconRequired={true}
                            OnStyleSheetClick={this.OnStyleSheetClick}
                            OnOpenInNewWindowClick={this.OnOpenInNewWindowClick}
                            OnOpenInNewTabClick={this.OnOpenInNewTabClick}
                        />
                    </React.Fragment> : ''}
            </div>
        );
    }
}
export default ListViewDemo;