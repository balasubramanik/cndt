import React, { Component } from "react";
import { FindControl } from "conduent-component-library";

class FindControlDemo extends Component {

  constructor(props){
    super(props);
    this.state = {
      findControls: []
    }
  }

  componentDidMount(){
    fetch('http://localhost:3000/json/ContractFindControls.json')
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({findControls: result.findControls});
      },
      (error) => {
        console.log(error);
      }
    )
  }

  render(){
    return(
      <React.Fragment>
        <div className="sidebar">
          <FindControl findControls={this.state.findControls} />
        </div>
        <div className="main-panel"> 

        </div>
      </React.Fragment>
    );
  }
}
export default FindControlDemo;


