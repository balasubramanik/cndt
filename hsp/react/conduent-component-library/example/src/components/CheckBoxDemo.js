import React, {useState} from "react";
import { CheckBox } from "conduent-component-library";

export const CheckBoxDemo = () => {
    const [checked, setChecked] = useState(true)
    return(
        <React.Fragment>
            <h3>Check Box</h3>
            <div>
                <CheckBox type="checkbox" checked={checked} onChange={()=>{debugger;setChecked(!checked)}} label="Hello" />
            </div>
        </React.Fragment>
    );
}