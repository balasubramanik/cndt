import React from "react";
import { ButtonBar } from "conduent-component-library";

const routes = [
    {
        "name": "ButtonBar",
        "parent": "null",
        "children": [
            {
                "TabPanel": "PROPERTIES",
                "children": [
                    {
                        "id": "01",
                        "name": "Details",
                        "desc": ""
                    },
                    {
                        "id": "02",
                        "name": "Additional Details",
                        "desc": "Additional Details of content"
                    }
                ]
            },
            {
                "TabPanel": "MAPPING",
                "children": [
                    {
                        "id": "011",
                        "name": "Contracts Content",
                        "desc": "Contracts Content",
                    },
                    {
                        "id": "021",
                        "name": "Languages ",
                        "desc": "Languages  content"
                    },
                    {
                        "id": "031",
                        "name": "Fulfilment",
                        "desc": "Fulfilment content"
                    },
                    {
                        "id": "041",
                        "name": "Events",
                        "desc": "Events content"
                    }
                ]
            },
            {
                "TabPanel": "RELATED ITEMS",
                "children": [
                    {
                        "id": "012",
                        "name": "Claims",
                        "desc": "Claims content"
                    },
                    {
                        "id": "022",
                        "name": "Providers",
                        "desc": "Providers"
                    },
                    {
                        "id": "032",
                        "name": "History",
                        "desc": "History content"
                    }
                ]
            }
        ]
    }
]

  const ButtonBarDemo = () => {
    return(
        <React.Fragment>
            <div>
            <ButtonBar onSelected={(s) => console.log(s)} container={true} selected={"01"} routes={routes} />
            </div>
        </React.Fragment>
    );
}
export default ButtonBarDemo;