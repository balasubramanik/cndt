import React from 'react'
import { CascadingDropDown } from "conduent-component-library"


const dropdownsList = [{ name: "State", linkfrom: "Country", linkTo: "City", ddlValueField: "demographic", ddlLabelField: "demographic" },
{ name: "City", linkfrom: "State", linkTo: "null", ddlValueField: "demographic", ddlLabelField: "demographic" }];

class CascadingDropDownDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            defalutSelect: 0,
            dropdownsListData: []
        }
    }
    componentDidMount() {
        let StateData = this.fetchState('http://localhost:3000/json/state.json');
    }

    async fetchState(url) {
        await fetch(url)
            .then(res => res.json())
            .then((data) => {
                let arr = [];
                arr.push({ "State": data["demographicsFromZipDTOs"] });
                this.setState({ dropdownsListData: arr })
            })
            .catch(error => console.log(error))
    }

    async fetchCity(url) {
        await fetch(url)
            .then(res => res.json())
            .then((data) => {
                let arr = this.state.dropdownsListData;
                arr.splice(1, 2)
                arr.push({ "City": data["demographicsFromZipDTOs"] });
                this.setState({ dropdownsListData: arr })
            })
            .catch(error => console.log(error))
    }

    selectedValue = (e) => {
        let StateId = e.filter((el) => el.ddlName === "State")
        let CountyData = this.fetchCity('http://localhost:3000/json/county.json', StateId[0].selectedVal);
        let CityId = e.filter((el) => el.ddlName === "City")
        e.map((index) => console.log(index.ddlName, index.selectedVal, index.crrentChange))
    }

    render() {
        return (
            <div>
                {this.state.dropdownsListData.length > 0 ?
                    <CascadingDropDown dropdownsList={dropdownsList} title={"cascading ddl"} onValue={this.selectedValue} dropdownsListData={this.state.dropdownsListData} /> : ""
                }
            </div>
        );
    }
}
export default CascadingDropDownDemo;