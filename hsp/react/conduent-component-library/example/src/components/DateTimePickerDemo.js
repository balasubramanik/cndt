import React, { useState } from "react";
import { DateTimePicker } from "conduent-component-library";

export const DateTimePickerDemo = () => {
    
    const [date, setDate] = useState(null);
    const [time, setTime] = useState(new Date());
    
    return(
        <React.Fragment>
            <h3>Date/Time Picker Demo</h3>
            <div>
                <h4>Date Picker</h4>
                <DateTimePicker placeHolder="MM/DD/YYYY" selectedDate = {date} handleChange={(date)=>setDate(date)}/>
            </div>
            <div>
                <h4>Time Picker</h4>
                <DateTimePicker showTimeOnly={true} selectedDate = {time} handleChange={(time)=>setTime(time)}/>
            </div>
        </React.Fragment>
    );
}