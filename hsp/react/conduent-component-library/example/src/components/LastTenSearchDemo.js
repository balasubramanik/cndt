import React from "react";
import { LastTenSearch } from 'conduent-component-library';

export class LastTenSearchDemo extends React.Component {
  constructor(props) {
    super(props)
    this.child = React.createRef();
    this.state = {
       searches: [
        {
          "searchID": "1",
          "itemType": "ProviderProfile",
          "text" :'Provider profile 4545454',
          "itemValue": {"OfficeName": "Office Name 1"},
          "addedTime": new Date('2020-03-23T20:19:16.774Z').getTime()
        },
        {
          "searchID": "2",
          "itemType": "ProviderProfile",
          "text": "Provider profile 123456",
          "itemValue": {OfficeName: "Office Name 2"},
          "addedTime": new Date('2020-03-23T20:19:44.999Z').getTime()
        },
        {
          "searchID": "3",
          "itemType": "ProviderProfile",
           "text": "Provider profile 676767",
          "itemValue": {"OfficeName": "Office Name 3"},
          "addedTime": new Date('2020-03-23T20:19:01.999Z').getTime()
        },
        {
          "searchID": "4",
          "itemType": "ProviderProfile",
          "text" : "Provider profile 98765",
          "itemValue": { "OfficeName": "Office Name 4"},
          "addedTime": new Date('2020-03-23T20:19:22.999Z').getTime()
        }
        ]
    }
    this.recentSearch = this.recentSearch.bind(this);
  }


  gotoFind(){
    alert("go to new search");
  }
  
  setRecord(clickedObj) {
    console.log(clickedObj);
  };

  recentSearch(){
    this.child.current.toggleLast10();
  }

  render() {
    return (
        <div style={{ position: "relative", float: "right"}}>
          <LastTenSearch 
            hideNewSearch={false}
            newSearchText="New Search"
            data={this.state.searches}
            gotoFind={this.gotoFind}
            setRecord={this.setRecord}
            position="right"
          />
        </div>
    );
  }
}

export default LastTenSearchDemo;