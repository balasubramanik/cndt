import React from "react";
import { ComboBoxCache } from "conduent-component-library"

const ddlList = [
  {
    name: 'sroske',
    value: 'sroske',
  },
  {
    name: 'lee',
    value: 'lee',
  },
  {
    name: 'jeffsmith',
    value: 'jeffsmith',
  },
  {
    name: 'kadalkannan',
    value: 'kadalkannan',
  },
]

export const ComboBoxCacheDemo = () => {  
    return(
        <React.Fragment>
            <h3>ComboBox Cache Demo</h3>
            <div>
                <ComboBoxCache
                  ddlList={ddlList}
                  resultAPI={'https://api.github.com/users/'}
                  size={200}
                  DefaultValue=''
                  DisplayFieldName=''
                  DisplayIDName=''
                  FindDisplayName=''
                  placeholder=" -- Select User -- "
                  styleClass="custom-select" />
            </div>
        </React.Fragment>
    );
}