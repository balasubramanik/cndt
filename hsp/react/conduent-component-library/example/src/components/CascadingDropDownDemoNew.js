import React from 'react'
import CascadingDropDownDemo from './CascadingDropDownDemo'

export const CascadingDropDownDemoNew = () => {
    return (
        <React.Fragment>
            <h3>CascadingDropDownDemo</h3>
            <div>
               <CascadingDropDownDemo />
                <br />
            </div>
        </React.Fragment>
    );
}

