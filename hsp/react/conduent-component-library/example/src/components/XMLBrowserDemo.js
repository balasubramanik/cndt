import React, { Component } from "react";
import { XMLBrowser } from 'conduent-component-library';
import 'conduent-component-library/dist/index.css';

class XMLBrowserDemo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      html: {},
      printRef: {},
      zoom: 1,
      styles: [],
      values: []
    }
    this.myRef = React.createRef();
  }
  async getReport() {
    const reqBody = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        "sessionID": "4287",
        "usage": "|ALLBYCATEGORYCODE|",
        "categoryCode": "OP"
      }
      )
    };
    const req = await fetch("http://localhost:5000/api/getuserreports", reqBody);
    const data = await req.json();
    this.setState({ styles: data.userReportsDTO, values: [data.userReportsDTO[0]] });
    this.getXMLData(data.userReportsDTO[0])
  }
  async getXMLData(config) {
    const reqBody = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        xmlreqbody: {
          "SPName": config.storedProcedureName || "ee_getofficeprofile_xml",
          "XSLTPath": config.reportPath || "OfficeProfile.xsl",
          "sessionId": "4287",
          "officeid": "1",
          "returnStatus": "N"
        }
      })
    };
    const req = await fetch("http://localhost:5000/api/getxmlstylesheet", reqBody);
    const data = await req.json();
    this.setState({ html: data });

  }
  componentDidMount() {
    this.getReport();
  }
  onRefresh(v) {
    this.getXMLData(v[0])
  }
  render() {
    return (
          <div className="ccl--xml-wrapper">
            <div className="ccl--toolbar">
              Placeholder for &lt;Toolbar /&gt;  
            </div>

            <div style={{ zoom: this.state.zoom }} >
              <XMLBrowser ref={el => (this.myRef = el)} data={this.state.html} />
            </div>
          </div>
    );
  }

}


export default XMLBrowserDemo;
