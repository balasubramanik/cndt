import React, { Component } from 'react';
import { FindControl, ListView } from "conduent-component-library";

class FindControlDemo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      findControls: [],
      findProfiles: [],
      referenceCodes_V2DTOs: [],
      recentData: {},
      userOption: {},
      screenName: "",
      isNavExpended: true,
      profileSettings: [],
      cascadeDropdownsListData: [],
      listViewData: [],
      listViewColData: []
    }

    this.comboDefaultOption = {
      "ReimbursementID": "<All>",
      "ResultCount": "<All>",
      "AdditionalService": "<All>",
      "Language": "<None>",
      "State": "<Any>",
      "County": "<Any>",
      "Region": "<Any>",
    }
    this.controlValidations = {
      Distance: {
        validation: {
          max: 30
        },
        messages: {
          max: "Numeric Value is too Large.Please try a smaller value"
        }
      },
      AsOfDate: {
        validation: {
          dateRange: {
            start: "01/01/1900",
            end: "12/31/9999"
          }
        },
        messages: {
          dateRange: "As Of Date: Valid dates are between 1/1/1900 and 12/31/9999"
        }
      }
    }

    this.getRecentSearch = this.getRecentSearch.bind(this);
    this.casecadeDropdownValue = this.casecadeDropdownValue.bind(this);
    this.handleFind = this.handleFind.bind(this);
    this.toggleExpendNav = this.toggleExpendNav.bind(this)
  }
  // Example POST method implementation:
  async postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return response.json(); // parses JSON response into native JavaScript objects
  }

  getRecentSearch(params) {

    this.postData('http://localhost:5000/api/getUserOptions', params)
      .then((data) => {
        if (!data.status) {
          let recentData = JSON.parse(data.itemValue);
          this.setState({ recentData });
        }
      });

  }
  setUserOptions(params) {

    this.postData('http://localhost:5000/api/setUserOptions', params)
      .then((data) => {
        if (!data.status) {
          let userOption = data.status;
          this.setState({ userOption });
        }
      });

  }
  camelCase = (str) => {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
      return index == 0 ? word.toLowerCase() : word.toUpperCase();
    }).replace(/\s+/g, '');
  }
  setProfile(profileId, findParams, screenName) {
    /*send below params object while calling actual API*/
    /*const paramsObj = {
      sessionId: "4342",
      listviewName: screenName,
      listviewProfileId: profileId,
      usage: "|ForListView|"
    }*/
  
    fetch('http://localhost:3000/json/ListViewSettings.json')
      .then(res => res.json())
      .then(
        (result) => {
          this.handleFind(findParams, screenName, result.listviewProfileSettingsDTO);
          this.setState({
            profileSettings: result.listviewProfileSettingsDTO
          });

        },
        (error) => {

        }
      )
  }

  toggleExpendNav() {
    this.setState({
      isNavExpended: !this.state.isNavExpended
    })
  }

  onButtonFindClick(name) {

    if (name === 'Contract') {
      Promise.all([
        fetch('http://localhost:3000/json/ContractFindControls.json'),
        fetch('http://localhost:3000/json/GetFindProfiles.json'),
        fetch('http://localhost:3000/json/ResultCount.json')
      ])
        .then(async ([res1, res2, res3]) => {
          const a = await res1.json();
          const b = await res2.json();
          const c = await res3.json();

          this.setState({
            findProfiles: b,
            findControls: a.findControls,
            referenceCodes_V2DTOs: c.referenceCodes_V2DTOs,
            params: {
              userID: 1,
              itemType: "ContractSearch"
            }
          })

        })
        .catch(error => {
          console.log(error);
        });
    }
    else if (name === 'Office') {
      Promise.all([
        fetch('http://localhost:3000/json/OfficeFindControls.json'),
        fetch('http://localhost:3000/json/OfficeFindProfiles.json'),
        fetch('http://localhost:3000/json/ResultCount.json')
      ])
        .then(async ([res1, res2, res3]) => {
          const a = await res1.json();
          const b = await res2.json();
          const c = await res3.json();

          const parentCascadings = a.findControls.filter(obj => obj.linkTo);
          let parentObject = {};

          parentObject[parentCascadings[0].name] = parentCascadings[0].comboData;
          this.setState({
            findProfiles: b,
            findControls: a.findControls,
            cascadeDropdownsListData: [parentObject],
            referenceCodes_V2DTOs: c.referenceCodes_V2DTOs,
            params: {
              userID: 1,
              itemType: "OfficeSearch"
            }
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
    this.setState({ screenName: name });
  }

  casecadeDropdownValue(value, ddlName, id, linkTo) {
    fetch('http://localhost:3000/json/County.json')// Todo pass params to api to get child dropdown data in actual app
      .then(res => res.json())
      .then(
        (result) => {
          const values = Object.values(result);
          const array = this.state.cascadeDropdownsListData.filter((obj) => Object.keys(obj)[0] !== linkTo);
          array.push({ [linkTo]: values[1] });
          this.setState({
            cascadeDropdownsListData: array
          });
        },
        (error) => {

        }
      )
  }

  SelectedRow = (rowInfo) => {
    //select Row Details
    //alert(JSON.stringify(rowInfo.original))
  }

  columnOrderData = (cols) => {
    //ReOrder Column Details
    let colOrder = [];
    cols.map((index) => colOrder.push(index.accessor));
  }

  handleFind(findParams, screenName, columns = '') {
    let url = 'http://localhost:5000/api/contract';
    let objName = 'contracts';

    let cols = [
      { "Header": "Contract Name", "accessor": "contractName" },
      { "Header": "Contract Number", "accessor": "contractNumber" },
      { "Header": "Contract Description", "accessor": "contractDescription" },
      { "Header": "Capitation Rate Name", "accessor": "capitationRateName" },
      { "Header": "Effective Date", "accessor": "effectiveDate", "format": "Date", "dataformat": "MM/DD/YYYY" },
      { "Header": "Expiration Date", "accessor": "expirationDate", "format": "Date", "dataformat": "MM/DD/YYYY" },
      { "Header": "Last Posted Date", "accessor": "lastPostedDate", "format": "Date", "dataformat": "MM/DD/YYYY" }
    ];
    if (columns) {
      let column = JSON.parse(columns[0].settings).ListviewSettings.VisibleColumns.Column;
      let obj = [];
      column.forEach((value) => {

        obj.push({ Header: value['@Name'], accessor: this.camelCase(value['@Name']) });
      });
      cols = obj;
    }
    //[{"Header":"contractId","accessor":"contractId"}]
    const params = {
      "sessionID": "4287",
      "usage": "|USAGE2|",
      ...findParams
    }
    if (screenName === "Office") {
      url = 'http://localhost:5000/api/office';
      objName = "offices_V2s";
      params.usage = "|SEARCH|";
      cols = [
        { "Header": "Office Number", "accessor": "officeNumber" },
        { "Header": "Office Name", "accessor": "officeName" },
        { "Header": "Address1", "accessor": "address1" },
        { "Header": "Address2", "accessor": "address2" },
        { "Header": "City", "accessor": "city" },
        { "Header": "Preferred City", "accessor": "preferredCity" },
        { "Header": "State", "accessor": "state" },
        { "Header": "Zip", "accessor": "zip" },
        { "Header": "Country", "accessor": "country" },
        { "Header": "Region", "accessor": "region" },
        { "Header": "Contract Name", "accessor": "contractName" }
      ];
    }

    this.postData(url, params)
      .then((data) => {
        this.setState({ listViewData: data[objName], listViewColData: cols });
      });
  }

  render() {
    return (
      <div className="find-control-section">
        <div className="row">
          <div className="col-sm-3">
            <button className="btn-primary tabs" onClick={() => this.onButtonFindClick('Contract')}>Contract</button>
            <button className="btn-primary tabs" onClick={() => this.onButtonFindClick('Office')}>Office</button>
          </div>
        </div>
        <div className="row">
          <div className={(this.state.isNavExpended ? "col-md-3" : "col-md-1") + " find-control-area"}>

            <FindControl
              screenName={this.state.screenName}
              findControls={this.state.findControls}
              findProfiles={this.state.findProfiles}
              controlValidations={this.controlValidations}
              comboDefaultOption={this.comboDefaultOption}
              toggleExpendNav={() => this.toggleExpendNav()}
              referenceCodes_V2DTOs={this.state.referenceCodes_V2DTOs}
              params={this.state.params}
              onRecentSearch={(params) => this.getRecentSearch(params)}
              recentData={this.state.recentData}
              clearIndex = {() => {}}
              onFind={(params) => this.setUserOptions(params)}
              userOption={this.state.userOption}
              onProfile={(profileId, findParams, screenName) => this.setProfile(profileId, findParams, screenName)}
              profileSettings={this.state.profileSettings}
              cascadeDropdownsListData={this.state.cascadeDropdownsListData}
              casecadeDropdownValue={this.casecadeDropdownValue}
              handleFind={this.handleFind}
            />
          </div>
          <div className={(this.state.isNavExpended ? "col-md-9" : "col-md-11")}>
            {
              <ListView
              rows={this.state.listViewData}
              columns={this.state.listViewColData}
              resetColumns={this.state.listViewColData}
              defaultPageSize={10}
              minRows={1}
              FindControllId=""
              Name=""
              FindType=""
              SortKey=""
              className="-striped -highlight"
              selectedIndex={this.SelectedRow}
              columnOrder={this.columnOrderData}
            />
            }
          </div>
        </div>
      </div>
    )
  }
}

export default FindControlDemo;