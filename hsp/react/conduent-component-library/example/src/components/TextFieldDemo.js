import React, { useState } from "react";
import { TextField } from "conduent-component-library";

export const TextFieldDemo = () => {

    const [value, setValue] = useState("");
    return(
        <React.Fragment>
            <h3>Text Box</h3>
            <div>
            <TextField 
                type="text"
                id="contractName"
                label="Contract Name"
                required="Y"
                defaultValue=""
                value={value}
                minLength="0"
                maxLength="60"
                title="Contract Name"
                placeholder="Contract Name"
                autoComplete="off"
                onKeyPress={(event)=>{debugger}}
                onChange={(e) => { setValue(e.target.value);} }
              />
            </div>
        </React.Fragment>
    );
}