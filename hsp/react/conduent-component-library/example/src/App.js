import React, { Component } from "react";
import ReactDOM from "react-dom";

import { CheckBoxDemo } from "./components/CheckBoxDemo";
import { TextFieldDemo } from './components/TextFieldDemo';
import { DateTimePickerDemo } from "./components/DateTimePickerDemo";
import { ComboBoxDemo } from "./components/ComboBoxDemo";
import FindSidePanelDemo from "./components/FindSidePanelDemo";
import FindControlDemo from "./components/FindControlDemo";
import ButtonBarDemo from "./components/ButtonBarDemo";
import { ComboBoxCacheDemo } from "./components/ComboBoxCacheDemo";
import ListViewDemo from "./components/ListViewDemo";
import { CascadingDropDownDemoNew }  from "./components/CascadingDropDownDemoNew";
import LastTenSearchDemo from './components/LastTenSearchDemo';
import XMLBrowserDemo from './components/XMLBrowserDemo';

const components = require("./components.json");

class App extends Component {
  getComponent = (event) => {
    const name = event.target.name;
    switch (name) {
      case "CheckBoxDemo":
        ReactDOM.render(<CheckBoxDemo />, document.getElementById('content'))
        break;
      case "TextFieldDemo":
        ReactDOM.render(<TextFieldDemo />, document.getElementById('content'))
        break;
      case "DateTimePickerDemo":
        ReactDOM.render(<DateTimePickerDemo />, document.getElementById('content'))
        break;
      case "ComboBoxDemo":
          ReactDOM.render(<ComboBoxDemo />, document.getElementById('content'))
      break;
      case "FindControlDemo":
        ReactDOM.render(<FindControlDemo />, document.getElementById('content'))
        break;
      case "FindSidePanelDemo":
        ReactDOM.render(<FindSidePanelDemo 
          data={{
            "findCommandId": 45, 
            "usage": "|ForFindCommand|",
            "url": "http://localhost:3000/json/GetFindProfiles.json"
          }} />, document.getElementById('content'))
        break;
        case "ButtonBarDemo":
          ReactDOM.render(<ButtonBarDemo />, document.getElementById('content'))
          break;
        case "ComboBoxCacheDemo":
          ReactDOM.render(<ComboBoxCacheDemo />, document.getElementById('content'))
          break;
         case "ListViewDemo":
           ReactDOM.render(<ListViewDemo />, document.getElementById('content'))
         break;
         case "CascadingDropDownDemoNew":
           ReactDOM.render(<CascadingDropDownDemoNew />, document.getElementById('content'))
         break;
         case "LastTenSearchDemo":
          ReactDOM.render(<LastTenSearchDemo />, document.getElementById('content'))
        break;
         case "XMLBrowserDemo":
          ReactDOM.render(<XMLBrowserDemo />, document.getElementById('content'))
        break;
      default:
        break;
    }
  }
  CompList = () => {
    let menuDom = document.createElement('div');
    Object.entries(components).map(comp => {
      const name = comp[1].name;
      const title = comp[1].title;
      const list = document.createElement("a");
      list.appendChild(document.createTextNode(title));
      list.addEventListener("click", this.getComponent);
      list.setAttribute("name", name);
      menuDom.appendChild(list);
    });
    const menuContainer = document.getElementById('menu');
    menuContainer.appendChild(menuDom);
  }

  componentDidMount() {
    this.CompList();
  }
  render() {
    return (
      <React.Fragment>
        <div className="header" id="ConduentHeader">
          <img src="conduent_logo.PNG" alt="logo" />
           <h2>Conduent Component Library</h2>
        </div>
        <div className="main" >
          <div className="row">
            <div className="col-md-2">
              <div className="sidenav" id="menu"></div>
            </div>
            <div className="col-md-10">
              <div id="content" className="main-content"></div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default App
