import './index.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'conduent-component-library/dist/index.css';
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

ReactDOM.render(<App />, document.getElementById('root'))
