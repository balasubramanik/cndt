# conduent-component-library

> Conduent components library for HSP


## Install

```bash
npm install --save conduent-component-library
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'conduent-component-library'
import 'conduent-component-library/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```
