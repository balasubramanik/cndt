export const Constants = {
  PREVIOUS_SEARCH_TITLE:
    "Perform a search using previously selected parameters",
  PROFILE_SETTINGS_TITLE: "User Profiles",
  DEFAULT_COLUMN_WIDTH: 150
};
