import React from "react";
import ReactDOM from "react-dom";
import { shallow, mount } from "enzyme";
import CascadingDropDown from "../../components/CascadingDropDown/CascadingDropDown";


const DdlData = [{ name: "State", linkfrom: "Country", linkTo: "City", displayIDName: "demographic", displayFieldName: "demographic" }];

const dropdownsListData=[{"countrycode":"IN","countryName":"India"}]

const comboSelValue=[{"countrycode":"IN","countryName":"India"}];

describe('CascadingDropDown component', () => {
    test('should shallow correctly', () => {
        expect(shallow(
          <CascadingDropDown dropdownsList={DdlData} dropdownsListData={dropdownsListData} comboSelValues={comboSelValue} />
        )).toMatchSnapshot() 
    })
    test('should mount correctly', () => {
        expect(mount(
          <CascadingDropDown dropdownsList={DdlData} dropdownsListData={dropdownsListData} comboSelValues={comboSelValue} />
        )).toMatchSnapshot() 
    })
    it("should select the record on click on the list", () => {
      const ChangeDdl = jest.fn();
      const component = mount(
        <CascadingDropDown dropdownsList={DdlData} dropdownsListData={dropdownsListData} comboSelValues={comboSelValue} />
      );
      const btn = component.find(".react-dropdown-select-input").at(0);
      btn.simulate("click");
      const li = component.find(".react-dropdown-select-dropdown").at(0);
      expect(li.length).toBe(1);
    });

    it("should select the record on click on the list", () => {
      const ChangeDdl = jest.fn();
      const component = mount(
        <CascadingDropDown dropdownsList={DdlData} dropdownsListData={dropdownsListData} comboSelValues={comboSelValue} />
      );
      const btn = component.find(".react-dropdown-select-input").at(0);
      btn.simulate("click");
      const li = component.find(".react-dropdown-select-dropdown").at(0);
      const span = li.find(".react-dropdown-select-item").at(0);
      // span.simulate("click");
      // expect(btn.getDOMNode().value).toBe('India');
      //console.log(btn.getDOMNode());
    });
  
  })

describe('Testing CascadingDropDown Node', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<CascadingDropDown  dropdownsList={DdlData}  dropdownsListData={dropdownsListData}  comboSelValues={comboSelValue} />, div);
        ReactDOM.unmountComponentAtNode(div);
    })
});

