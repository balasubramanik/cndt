import React from "react";
import { shallow, mount } from "enzyme";
import ReactDOM from "react-dom";
// import { create } from "react-test-renderer";

import Combobox from "../../components/combobox/ComboBox";

describe("<Combobox/>", () => {
  const ddlList = [{ "countrycode": "IN", "countryName": "India" },{ "countrycode": "SG", "countryName": "Singapore" }];
  const selvalues =  [{ "countrycode": "IN", "countryName": "India" }];

  describe('ComboboxCache component', () => {
    test('should shallow correctly', () => {
      expect(shallow(
        <Combobox options={ddlList} values={selvalues} />
      )).toMatchSnapshot()
    })
    test('should mount correctly', () => {
      expect(mount(
        <Combobox options={ddlList} values={selvalues}  />
      )).toMatchSnapshot()
    })
   });
  
  describe('Testing ComboboxCache Node', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');
      <Combobox options={ddlList}  values={selvalues} />
      ReactDOM.unmountComponentAtNode(div);
    })
  });
});
  

  // it("should have a size prop", () => {
  //   const component = mount(<Combobox size="lg" />);
  //   expect(component.props().size).toEqual("lg");
  // });

  // it("should have a round prop", () => {
  //   const component = mount(<Combobox round={2} />);
  //   expect(component.props().round).toEqual(2);
  // });

  // it("should have a label prop", () => {
  //   const component = mount(<Combobox label="Label for combobox" />);
  //   expect(component.props().label).toEqual("Label for combobox");
  // });

  // it("should have a multi prop for multicolumn combobox", () => {
  //   const component = mount(<Combobox multi />);
  //   expect(component.props().multi).toBeTruthy();
  // });

  // it("should have state object", () => {
  //   const component = mount(<Combobox />);
  //   expect(component.instance().state).toBeTruthy();
  // });

  // it("should have users in the state object", () => {
  //   const component = mount(<Combobox />);
  //   component.instance().setState(state);
  //   expect(component.instance().state.users.length).toEqual(2);
  // });

  // it("should have fire click event on button", () => {
  //   const component = mount(<Combobox />);
  //   const btn = component.find("button");
  //   btn.simulate("click");
  //   expect(component.find("div").length).toEqual(1);
  // });
  
  // it("should have fire onFocus event on input", () => {
  //   const component = mount(<Combobox />);
  //   const input = component.find("input");
  //   input.simulate("focus");
  //   expect(component.find("ul").length).toEqual(1);
  // });

  // it("should have fire onChange event on input", () => {
  //   const testUserList = [{id:1, username:'Bret_Lee', name:'Bret'}];
  //   const component = mount(<Combobox multi />);
  //   const input = component.find("input");

  //   component.instance().setState({users:testUserList, usersCopy: testUserList, selectedValue:'Bret'});
  //   input.getDOMNode().value = component.state().selectedValue;
  //   input.simulate("change");
  //   expect(component.find("ul").length).toEqual(1);
  // });

  // it("should have fire onKeyDown event on input", () => {
    
  //   const multiColComponent = mount(<Combobox multi />);
  //   const inputMulti = multiColComponent.find("input");
    
  //   multiColComponent.instance().setState(state);
  //   inputMulti.getDOMNode().value = multiColComponent.state().selectedUserName;
  //   inputMulti.simulate("keydown");
  //   expect(inputMulti.getDOMNode().value).toEqual('bret');
    
  //   const singleColComponent = mount(<Combobox />);
  //   const inputSingle = singleColComponent.find("input");
    
  //   singleColComponent.instance().setState(state);
  //   inputSingle.getDOMNode().value = singleColComponent.state().selectedUserName;
  //   inputSingle.simulate("keydown");
  //   expect(inputSingle.getDOMNode().value).toEqual('bret');
  // });

  // it("should set users in state object input's onFocus and button's onClick", () => {
  //   const component = mount(<Combobox />);
  //   const input = component.find("input");
  //   const button = component.find("button");

  //   component.instance().setState(state);
  //   input.getDOMNode().value = component.state().selectedUserName;
  //   input.simulate("change");
  //   expect(component.find("ul").length).toEqual(1);
    
  //   button.simulate('click');
  //   expect(component.find("ul").length).toEqual(1);

  // });

  // it("should call getUsers() input's onFocus and button's onClick", () => {
  //   const component = mount(<Combobox />);
  //   const input = component.find("input");
  //   const button = component.find("button");

  //   component.instance().setState(state);
  //   component.instance().setState({users:[]});
  //   input.simulate("change");
  //   expect(component.find("ul").length).toEqual(1);
    
  //   button.simulate('click');
  //   expect(component.find("ul").length).toEqual(1);

  // });

  // it("should close the dropdown on keydown event", () => {
  //   const component = mount(<Combobox />);
  //   const input = component.find("input");

  //   component.instance().setState(state);
  //   input.simulate("keydown", {key:'Tab'});
  //   expect(component.instance().state.isOpen).toEqual(false);

  // });

  // it("should close the dropdown on button focus", () => {
  //   const component = mount(<Combobox />);
  //   const button = component.find("button");

  //   component.instance().setState(state);
  //   button.simulate("focus");
  //   expect(component.instance().state.isOpen).toEqual(false);

  // });

  // it("should close the dropdown on input blur", () => {
  //   const component = mount(<Combobox />);
  //   const input = component.find("input");

  //   component.instance().setState(state);
  //   input.simulate("blur");
  //   expect(component.instance().state.isOpen).toEqual(false);

  // });

  // it("should fire click event on dropdown option list", () => {
  //   const component = mount(<Combobox />);
  //   component.instance().setState(state);
  //   component.find('button').simulate('click');
  //   component.instance().setState({selectedUser: state.users[0]});
  //   const li = component.find("li").at(0);
  //   li.simulate("click");
  //   expect(component.instance().state.selectedUser.username).toEqual('Bret');
  // });
  
  // it("should click on document to close combobox list", () => {
  //   const component = mount(<Combobox />);
  //   const btn = component.find('button');
  //   btn.simulate('click');
  //   var evt = document.createEvent("HTMLEvents");
  //   evt.initEvent("focus", false, true);
  //   document.body.dispatchEvent(evt);
  //   expect(component.instance().state.isOpen).toEqual(true);
  // });

//});
