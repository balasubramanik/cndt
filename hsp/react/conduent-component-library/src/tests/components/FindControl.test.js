import React from "react";
import { mount } from "enzyme";
import { create } from "react-test-renderer";

import FindControl from "../../components/FindControl/FindControl";

describe("<FindControl/>", () => {
 const props = {
    screenName :"Contract",
    findControls : [],
    findProfiles: {},
    referenceCodes_V2DTOs : [],
    params :{},
    onRecentSearch : () => {},
    onFind: () => {},
    onProfile : () => {},
    handleFind: () => {}

 }
    it('should render a button element', () => {
        const component = mount(<FindControl {...props} />);
        expect(component.exists()).toBe(true);
    });

    
   /* it('should render without a crash', () => {
        const component = create(<FindControl/>).toJSON();
        expect(component).toMatchSnapshot();
    });
    
    it('should render a button element', () => {
        const component = mount(<FindControl type="button">click</FindControl>);
        expect(component.exists()).toBe(true);
    });*/

    it('should render children', () => {
        const component = mount(<FindControl {...props} />);
        console.log("FINDCONTROL >>", component);
        expect(component.find('button').at(0).text()).toEqual('Clear');
        
    });

   /* it('should have a type prop', () => {
        const component = mount(<FindControl type="button">click</FindControl>);
        expect(component.props().type).toBe('button');
    });

    it('should have a onClick prop', () => {
        const doSomething = jest.fn();
        const component = mount(<FindControl type="button" onClick={doSomething}>click</FindControl>);
        expect(component.props().onClick).toBeTruthy();
    });

    it('should have other props', () => {
        const doSomething = jest.fn();
        const component = mount(<FindControl type="button" test="test" onClick={doSomething}>click</FindControl>);
        expect(component.props().test).toBe('test');
    });*/

});
