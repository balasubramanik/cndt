import React from "react";
import { mount, shallow } from "enzyme";
import ReactDOM from "react-dom";
import ListView from "../../components/ListView/ListView";
import ReactTablePagination from "../../components/ListView/Pagination";

const columns = [
    {
        Header: "Header1",
        accessor: "accessor1",
        width: 100
    },
    {
        Header: "Header2",
        accessor: "accessor2",
        width: 100
    }
];
const rowsData =
    [
        {
            "Header1": "row1",
            "Header2": "row1"
        },
        {
            "Header1": "row2",
            "Header2": "row2"
        }
    ]


const titleLv = "HSP"

describe('ListView component', () => {
    test('should shallow correctly', () => {
        expect(shallow(
            <ListView rows={rowsData} columns={columns} title={titleLv} defaultPageSize={10} minRows={1} />
        )).toMatchSnapshot()
    })
    test('should mount correctly', () => {
        expect(mount(
            <ListView rows={rowsData} columns={columns} title={titleLv} defaultPageSize={10} minRows={1} />
        )).toMatchSnapshot()
    })

    test('should shallow correctly', () => {
        expect(shallow(
            <ReactTablePagination page={100}
                pageSize={10}
                currentCount={20} />
        )).toMatchSnapshot()
    })

    test('should mount correctly', () => {
        expect(mount(
            <ReactTablePagination page={100}
                pageSize={10}
                currentCount={20} />
        )).toMatchSnapshot()
    })

    it('check getFirstRecord is instanceof moment', () => {
        const wrapper = mount(<ReactTablePagination page={100}
            pageSize={10}
            currentCount={20} />);
        const fun = wrapper.instance().getFirstRecord(1, 1, 0);
        expect(fun).toBe(0); // no
    });

    it('check getLastRecord is instanceof moment', () => {
        const wrapper = mount(<ReactTablePagination page={100}
            pageSize={10}
            currentCount={20} />);
        const fun = wrapper.instance().getLastRecord(1, 1, 0, 1);
        expect(fun).toBe(0); // no
    });


    it('check prop title by default', () => {
        const ListViewComponent = mount(<ListView rows={rowsData} columns={columns} title={"TestTitle"} defaultPageSize={10} minRows={1} />);
        expect(ListViewComponent.find('.ListViewTitle')).toBeDefined();
    });

    it('check handleClickEvent is instanceof moment', () => {
        const wrapper = mount(<ListView rows={rowsData} columns={columns} title={titleLv} defaultPageSize={10} minRows={1} />);
        const fun = wrapper.find('.dropbtnStyle').simulate('change',{target:{id:"NW",indfo:"10",value:{value:'NW'});
        expect(fun).toBeDefined(); // no
    });

    it('check changeDateFormat is instanceof moment', () => {
        const wrapper = mount(<ListView rows={rowsData} columns={columns} title={titleLv} defaultPageSize={10} minRows={1} />);
        const fun = wrapper.instance().changeDateFormat("Tue Mar 24 2015 17:00:00", "MM/DD/YYYY");
        expect(fun).toBe("03/24/2015"); // no
    });

    it('check changeDateFormat is instanceof moment', () => {
        const wrapper = mount(<ListView rows={rowsData} columns={columns} title={titleLv} defaultPageSize={10} minRows={1} />);
        const fun = wrapper.instance().changeDateTimeFormat("Tue Mar 24 2015 17:00:00", "MM/DD/YYYY HH");
        expect(fun).toBe("03/24/2015 17"); // no
    });
});

describe('Testing ListView Node', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<ListView rows={rowsData} columns={columns} title={titleLv} DisplayIconRequired="true" />, div);
        ReactDOM.unmountComponentAtNode(div);
    })
});

describe('Testing ListView Node', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<ReactTablePagination page={100}
            pageSize={10}
            currentCount={20} canPrevious={true} canNext={true} />, div);
        ReactDOM.unmountComponentAtNode(div);
    })
});