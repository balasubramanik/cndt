import React from "react";
import { mount } from "enzyme";

import ComboboxOption from "../../components/combobox/ComboboxOption";

describe("<ComboboxOption />", () => {
  it("should have active prop", () => {
    const component = mount(
      <ComboboxOption
        key={1}
        user={{ id: 1, username: "username", name: "name" }}
        selectUser={jest.fn()}
        multi
        row={1}
        index={1}
      />
    );
    expect(component.props().index).toBe(1);
    expect(component.props().multi).toBe(true);
  });
});
