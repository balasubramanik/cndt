import React from "react";
import { mount } from "enzyme";

import ComboboxList from "../../components/combobox/ComboboxList";

describe("<ComboboxList/>", () => {
  it("should have active prop", () => {
    const component = mount(
      <ComboboxList
        users={[{ id: 1, username: "username", name: "name" }]}
        selectUser={jest.fn()}
        hidden={false}
        round={2}
        multi
        active
        className="hover"
        isUserFound={false}
      />
    );
    expect(component.props().active).toBe(true);
    expect(component.props().isUserFound).toBe(false);
    expect(component.props().className).toBe("hover");
  });
});
