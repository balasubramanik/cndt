import React from "react";
import { mount } from "enzyme";
import LastTenSearch from "../../components/LastTenSearch/LastTenSearch";
describe("<LastTenSearch/>", () => {
  const state = {
    searches: [
      {
        searchID: "1",
        itemType: "ProviderProfile",
        text: "Provider profile 4545454",
        itemValue: { OfficeName: "Office Name 1" },
        addedTime: new Date("2020-03-23T20:19:16.774Z").getTime()
      },
      {
        searchID: "2",
        itemType: "ProviderProfile",
        text: "Provider profile 123456",
        itemValue: { OfficeName: "Office Name 2" },
        addedTime: new Date("2020-03-23T20:19:44.999Z").getTime()
      },
      {
        searchID: "3",
        itemType: "ProviderProfile",
        text: "Provider profile 676767",
        itemValue: { OfficeName: "Office Name 3" },
        addedTime: new Date("2020-03-23T20:19:01.999Z").getTime()
      },
      {
        searchID: "4",
        itemType: "ProviderProfile",
        text: "Provider profile 98765",
        itemValue: { OfficeName: "Office Name 4" },
        addedTime: new Date("2020-03-23T20:19:22.999Z").getTime()
      }
    ]
  };

  it("should mount and exists in dom", () => {
    const component = mount(<LastTenSearch data={state.searches} />);
    expect(component.exists()).toBeTruthy();
  });

  it("should contain one button element for last ten search", () => {
    const component = mount(<LastTenSearch data={state.searches} />);
    const btn = component.find("button");
    expect(btn.length).toBe(1);
  });

  it("should bind event on window when hidden prop in state changes", () => {
    const component = mount(<LastTenSearch data={state.searches} />);
    component.instance().setState({ hidden: false });
    expect(component.instance().state.hidden).toBe(false);
  });

  it("should show last ten search list on button click", () => {
    const component = mount(<LastTenSearch data={state.searches} />);
    const btn = component.find("button");
    btn.simulate("click");
    expect(component.find(".ccl-recent-search-wrapper").length).toBe(1);
  });

  it("should select the record on click on the list", () => {
    const setRecord = jest.fn();
    const component = mount(
      <LastTenSearch setRecord={setRecord} data={state.searches} index={0} />
    );
    const btn = component.find("button");
    btn.simulate("click");
    const li = component.find("li").at(0);
    li.simulate("click");
    expect(setRecord).toHaveBeenCalledTimes(1);
  });

  it("should select the list item on ArrowDown key", () => {
    const setRecord = jest.fn();
    const event = new KeyboardEvent("keydown", {
      key: "ArrowDown",
      bubbles: true
    });
    const component = mount(
      <LastTenSearch setRecord={setRecord} data={state.searches} />
    );
    const btn = component.find("button");
    btn.simulate("click");
    const li = component.find("li");
    document.dispatchEvent(event);
    expect(
      li
        .at(1)
        .getDOMNode()
        .classList.contains("hover")
    ).toBeTruthy();
  });

  it("should select the last list item on ArrowUp key", () => {
    const setRecord = jest.fn();
    const event = new KeyboardEvent("keydown", {
      key: "ArrowUp",
      bubbles: true
    });
    const component = mount(
      <LastTenSearch setRecord={setRecord} data={state.searches} />
    );
    const btn = component.find("button");
    btn.simulate("click");
    const li = component.find("li");
    document.dispatchEvent(event);
    expect(
      li
        .last()
        .getDOMNode()
        .classList.contains("hover")
    ).toBeTruthy();
  });

  it("should focus list item", () => {
    const setRecord = jest.fn();
    const component = mount(
      <LastTenSearch index={0} setRecord={setRecord} data={state.searches} />
    );
    const btn = component.find("button");
    btn.simulate("click");
    const li = component.find("li");
    li.at(0).simulate("mouseenter");
    expect(document.activeElement.nodeName).toBe("LI");
    li.at(1).simulate("mouseenter");
    expect(
      li
        .at(1)
        .getDOMNode()
        .classList.contains("hover")
    ).toBe(true);
    expect(document.activeElement.nodeName).toBe("LI");
  });

  it("should setRecord on Enter key", () => {
    const setRecord = jest.fn();
    const component = mount(
      <LastTenSearch index={0} setRecord={setRecord} data={state.searches} />
    );
    const btn = component.find("button");
    btn.simulate("click");
    const li = component.find("li");
    li.at(0).simulate("keydown", { key: "Enter" });
    expect(setRecord).toHaveBeenCalledTimes(1);
  });
});
