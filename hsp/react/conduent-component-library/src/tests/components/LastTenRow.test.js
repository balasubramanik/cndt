/* eslint-disable prettier/prettier */
import React from "react";
import { mount } from "enzyme";
import LastTenRow from "../../components/LastTenSearch/LastTenRow";
describe("<LastTenRow />", () => {
  const state = {
    searches: [
      {
        searchID: "1",
        itemType: "ProviderProfile",
        text: "Provider profile 4545454",
        itemValue: { OfficeName: "Office Name 1" },
        addedTime: new Date("2020-03-23T20:19:16.774Z").getTime()
      },
      {
        searchID: "2",
        itemType: "ProviderProfile",
        text: "Provider profile 123456",
        itemValue: { OfficeName: "Office Name 2" },
        addedTime: new Date("2020-03-23T20:19:44.999Z").getTime()
      },
      {
        searchID: "3",
        itemType: "ProviderProfile",
        text: "Provider profile 676767",
        itemValue: { OfficeName: "Office Name 3" },
        addedTime: new Date("2020-03-23T20:19:01.999Z").getTime()
      },
      {
        searchID: "4",
        itemType: "ProviderProfile",
        text: "Provider profile 98765",
        itemValue: { OfficeName: "Office Name 4" },
        addedTime: new Date("2020-03-23T20:19:22.999Z").getTime()
      }
    ]
  };

  it("should mount and exists in dom", () => {
    const component = mount(<LastTenRow data={state.searches} />);
    expect(component.exists()).toBeTruthy();
  });

  it('should get the index 0 for the first list item', ()=>{
    const setRecord = jest.fn();
    const component = mount(<LastTenRow selectRecord={setRecord} index={0} data={state.searches} />);
    expect(component.at(0).props().index).toBe(0);
  });
  
});
