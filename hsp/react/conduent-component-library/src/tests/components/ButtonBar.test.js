import React from "react";
import { mount, shallow } from "enzyme";
import ButtonBar from "../../components/buttonbar/ButtonBar";

describe("<CheckBox />", () => {
  const routes = [
    {
      name: "ButtonBar",
      parent: "null",
      children: [
        {
          TabPanel: "PROPERTIES",
          children: [
            {
              id: "01",
              name: "Details",
              desc: ""
            },
            {
              id: "02",
              name: "Additional Details",
              desc: "Additional Details of content"
            }
          ]
        },
        {
          TabPanel: "MAPPING",
          children: [
            {
              id: "011",
              name: "Contracts Content",
              desc: "Contracts Content"
            },
            {
              id: "021",
              name: "Languages ",
              desc: "Languages  content"
            },
            {
              id: "031",
              name: "Fulfilment",
              desc: "Fulfilment content"
            },
            {
              id: "041",
              name: "Events",
              desc: "Events content"
            }
          ]
        },
        {
          TabPanel: "RELATED ITEMS",
          children: [
            {
              id: "012",
              name: "Claims",
              desc: "Claims content"
            },
            {
              id: "022",
              name: "Providers",
              desc: "Providers"
            },
            {
              id: "032",
              name: "History",
              desc: "History content"
            }
          ]
        }
      ]
    }
  ];

  it("should have mounted", () => {
    const component = mount(
      <ButtonBar container selected="01" routes={routes} />
    );
    expect(component.exists()).toBeTruthy();
  });

  it("should have anchor element's in the list", () => {
    const component = mount(
      <ButtonBar container selected="01" routes={routes} />
    );
    const anchors = component.find("ul").find("a");
    expect(anchors.at(0).exists()).toBeTruthy();
  });
});
