import React from "react";
import { mount } from "enzyme";
import Stylesheet from "../../components/XMLBrowser/Stylesheet";
describe("<Stylesheet/>", () => {
  const data = {
    customErrorMessage: null,
    htmlResult: `<html>
      <head>
        <META http-equiv="Content-Type" content="text/html; charset=utf-16">
        
        <link rel="stylesheet" type="text/css" href="http://localhost:5000/stylesheets/styles.css">
    
            
    
            <script language="javascript">
    
    
    
              function hideAllGroup()
    		         {
          			
    		            locl = document.getElementsByTagName('tbody');
    
                    
    
    		            for (i=0;i < locl.length;i++)
    		            {
    			            if (locl[i].id.indexOf('Group_') > -1)
    				      {
                    
    					      locl[i].style.display = 'none';
    					      toggleCaption(locl[i].id + '_caption','+')
    				      }
    		            }
    
    		            return false;
    		         }
    
              function showAllGroup()
    		         {
          			
    		            locl = document.getElementsByTagName('tbody');
    
    		            for (i=0;i<locl.length;i++)
    		            {
    			            if (locl[i].id.indexOf('Group_') > -1)
    				      {
    					      locl[i].style.display = '';
    					      toggleCaption(locl[i].id + '_caption','-')
    				      }
    		            }
    
    		            return false;
    		         }
    
             
    		      function hideAll()
    		         {
          			
    		            locl = document.getElementsByTagName('tbody');
    
    		            for (i=0;i < locl.length;i++)
    		            {
    			            if (locl[i].id.indexOf('_nav') > -1)
    				      {
    					      locl[i].style.display = 'none';
    					      toggleCaption(locl[i].id + '_caption','+')
    				      }
    		            }
    
    		            return false;
    		         }
    
    		         function showAll()
    		         {
          			
    		            locl = document.getElementsByTagName('tbody');
    
    		            for (i=0;i<locl.length;i++)
    		            {
    			            if (locl[i].id.indexOf('_nav') > -1)
    				      {
    					      locl[i].style.display = '';
    					      toggleCaption(locl[i].id + '_caption','-')
    				      }
    		            }
    
    		            return false;
    		         }
    
    		          function getItem(id)
    		          {
    			      var itm = false;
    			      if(document.getElementById)
    			          itm = document.getElementById(id);
    			      else if(document.all)
    			          itm = document.all[id];
    			      else if(document.layers)
    			          itm = document.layers[id];
    
    			      return itm;
    		          }
    
    		          function toggleItem(id)
    		          {
    			      itm = getItem(id);
    
    			      if(!itm)
    			          return false;
    
    			      if(itm.style.display == 'none')
    			      {
    			          itm.style.display = '';
    			          toggleCaption(id + '_caption','-')
    			      }
    			      else
    			      {
    			          itm.style.display = 'none';
    			          toggleCaption(id + '_caption','+')
    			      }
    
    			      return false;
    		          }
    
    		          function toggleCaption(id, v)
    		          {
    			      itm = getItem(id);
    
    			      if(!itm)
    			          return false;
          			
    			      itm.innerHTML = v;
    
    			      return false;
    		          }
    
                  
          </script>
      
          
    
          </head>
      <title>
            Office Information
          </title>
      <body onload="hideAll();">
        <table width="100%" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
          <tr>
            <td width="100px"><img src="http://localhost:5000/assets/HSP-logo.png"></td>
            <td valign="bottom">
              <h4>HSP Standard Embedded Dashboard</h4>
            </td>
          </tr>
        </table>
        <table width="100%" cellpadding="2">
          <table width="100%">
            <td width="70%" class="singleborder">
              <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="bottomdotted" colspan="2"><b class="header">Office Information</b></td>
                </tr>
                <tr>
                  <td style="vertical-align:top" width="30%">
                    <table cellpadding="0" cellspacing="0">
                      <tr>
                        <td colspan="2">
                          <tr><b>Dr Taviloglu 
                                    (70418)
                                  </b></tr>
                          <tr>
                            <td>31 Main Street</td>
                          </tr>
                          <tr>
                            <td colspan="2"></td>
                          </tr>
                          <tr>
                            <td>STONY BROOK 
                                    NY, 
                                    11790 
                                    US 
                                  </td>
                          </tr>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td class="leftdotted">
                    <table cellpadding="0" cellspacing="0" align="top">
                      <tr class="alternating1">
                        <td align="right" width="10%" nowrap><b>Contact Name:
            </b></td>
                        <td>Vanessa Anderson</td>
                      </tr>
                      <tr class="alternating2">
                        <td align="right" width="10%" nowrap><b>Contact Email:
            </b></td>
                        <td>vanderson@gmail.com</td>
                      </tr>
                      <tr class="alternating1">
                        <td align="right" width="10%" nowrap><b>Contact Phone:
            </b></td>
                        <td>(631) 751-2456 x </td>
                      </tr>
                      <tr class="alternating2">
                        <td align="right" width="10%" nowrap><b>Contact Fax:
            </b></td>
                        <td>
                        </td>
                      </tr>
                      <tr class="alternating1">
                        <td align="right" width="10%" nowrap><b>Number Of Physicians:
            </b></td>
                        <td>1</td>
                      </tr>
                      <tr class="alternating2">
                        <td align="right" width="10%" nowrap><b>Access Code:
            </b></td>
                        <td>74195296</td>
                      </tr>
                      <tr class="alternating1">
                        <td align="right" width="10%" nowrap><b>Wheelchair Access:
            </b></td>
                        <td>Y</td>
                      </tr>
                      <tr class="alternating2">
                        <td align="right" width="10%" nowrap><b>Available After Hours:
            </b></td>
                        <td>Y</td>
                      </tr>
                      <tr class="alternating1">
                        <td align="right" width="10%" nowrap><b>Number Of Physicians Extendors:
            </b></td>
                        <td>1</td>
                      </tr>
                      <tr class="alternating2">
                        <td align="right" width="10%" nowrap><b>Facility Operating Number:
            </b></td>
                        <td>
                        </td>
                      </tr>
                    </table>
                  </td>
                  <td width="30%" class="singleborder">
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="bottomdotted"><b class="header">Office Hours</b></td>
                      </tr>
                      <tr>
                        <td><b> Monday: </b>9:00:00 AM - 6:00:00 PM</td>
                      </tr>
                      <tr>
                        <td><b> Tuesday: </b>9:00:00 AM - 6:00:00 PM</td>
                      </tr>
                      <tr>
                        <td><b> Wednesday: </b>9:00:00 AM - 6:00:00 PM</td>
                      </tr>
                      <tr>
                        <td><b> Thursday: </b>9:00:00 AM - 6:00:00 PM</td>
                      </tr>
                      <tr>
                        <td><b> Friday: </b>9:00:00 AM - 6:00:00 PM</td>
                      </tr>
                      <tr>
                        <td><b> Saturday: </b> - </td>
                      </tr>
                      <tr>
                        <td><b> Sunday: </b> - </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </table>
          <table align="center">
            <tr class="navigationborder">
              <td><a href="# " class="hover" title="Collapse All" onclick="hideAll();return false;"> [ Collapse All ]</a></td>
              <td><a href="#" class="hover" title="Expand All" onclick="showAll();return false;"> [ Expand All ]</a></td>
            </tr>
          </table><br><table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td colspan="3" style="padding-bottom: 3px"><b class="detailsheader"><a id="MappedProvidersBody__nav_caption" class="navigate" title="Collapse/Expand Mapped Providers Info" onclick="toggleItem('MappedProvidersBody__nav')">-</a>
                          Mapped Providers
                        </b></td>
            </tr>
            <tbody id="MappedProvidersBody__nav">
              <tr style="text-align: center;">
                <td class="bottomdouble">
                          Last Name
                        </td>
                <td class="bottomdouble">
                          First Name
                        </td>
                <td class="bottomdouble">
                          Number
                        </td>
                <td class="bottomdouble">
                          NPI
                        </td>
                <td class="bottomdouble">
                          Monday 
                        </td>
                <td class="bottomdouble">
                          Tuesday 
                        </td>
                <td class="bottomdouble">
                          Wednesday 
                        </td>
                <td class="bottomdouble">
                          Thursday 
                        </td>
                <td class="bottomdouble">
                          Friday 
                        </td>
                <td class="bottomdouble">
                          Saturday 
                        </td>
                <td class="bottomdouble">
                          Sunday 
                        </td>
                <td class="bottomdouble">
                          Total Office Hours
    					</td>
                <td class="bottomdouble">
                          Primary Language
              </td>
              </tr>
              <tr class="alternating1" style="text-align:center">
                <td>Pratt</td>
                <td class="leftdotted">Jack</td>
                <td class="leftdotted">4512784</td>
                <td class="leftdotted">1542015482</td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted">
    					     -
    					    </td>
                <td class="leftdotted">English</td>
              </tr>
              <tr class="alternating2" style="text-align:center">
                <td>Taviloglu</td>
                <td class="leftdotted">Servet</td>
                <td class="leftdotted">2790292</td>
                <td class="leftdotted">1811919889</td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted"> - </td>
                <td class="leftdotted">
    					     -
    					    </td>
                <td class="leftdotted">English</td>
              </tr>
            </tbody>
          </table><br><table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td colspan="2" style="padding-bottom: 3px"><b class="detailsheader"><a id="LanguagesBody__nav_caption" class="navigate" title="Collapse/Expand Language Info" onclick="toggleItem('LanguagesBody__nav')">-</a>
                          Languages
                        </b></td>
            </tr>
            <tbody id="LanguagesBody__nav">
              <tr style="text-align: center;">
                <td class="bottomdouble">
                          Name
                        </td>
                <td class="bottomdouble">
                          Use
                        </td>
                <td class="bottomdouble">
                          Spoken
                        </td>
                <td class="bottomdouble">
                          Written
                        </td>
                <td class="bottomdouble">
                          Level Of Proficiency
                        </td>
              </tr>
              <tr class="alternating1" style="text-align:center">
                <td>Turkish, Ottoman (1500-1928)</td>
                <td>Primary</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
              </tr>
              <tr class="alternating2" style="text-align:center">
                <td>English</td>
                <td>Secondary</td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
              </tr>
            </tbody>
          </table><br><table width="99%" class="singleborder4" cellpadding="0" cellspacing="0" align="center">
            <tr>
              <td colspan="2" style="padding-bottom: 3px"><b class="detailsheader"><a id="AdditionalServicesBody__nav_caption" class="navigate" title="Collapse/Expand Language Info" onclick="toggleItem('AdditionalServicesBody__nav')">-</a>
                          Additional Services
                        </b></td>
            </tr>
            <tbody id="AdditionalServicesBody__nav">
              <tr style="text-align: center;">
                <td class="bottomdouble">
    						Service Name
                        </td>
                <td class="bottomdouble">
    						Effective From - To
                        </td>
              </tr>
              <tr class="alternating1" style="text-align:center">
                <td>Lab</td>
                <td nowrap class="leftdotted">01/01/1900 - 12/31/9999</td>
              </tr>
              <tr class="alternating2" style="text-align:center">
                <td>X-Ray</td>
                <td nowrap class="leftdotted">01/01/1900 - 12/31/9999</td>
              </tr>
            </tbody>
          </table>
        </table>
        <table width="100%" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:user="urn:my-scripts">
          <tr>
            <td><br><br><hr>
            </td>
          </tr>
          <tr>
            <td>
              <center>
                <h6>Health Solutions Plus, Inc.</h6>
              </center>
            </td>
          </tr>
        </table>
      </body>
    </html>`,
    outputMessages: null,
    sqlExceptioMessages: null,
    statusMetadata: null
  };
  it("should mount and exists in dom", () => {
    const component = mount(<Stylesheet data={data} />);
    expect(component.exists()).toBeTruthy();
  });

  it("should show loader when request is sent", () => {
    const component = mount(<Stylesheet data={{}} />);
    expect(component.find(".spinner-border").length).toBe(1);
  });

  it("should render html from the response", () => {
    const component = mount(<Stylesheet data={data} />);
    expect(component.getDOMNode()).toBeTruthy();
  });
});
