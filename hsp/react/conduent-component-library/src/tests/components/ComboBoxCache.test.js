import React from "react";
import { shallow, mount } from "enzyme";
import ReactDOM from "react-dom";
import ComboboxCache from "../../components/ComboCache/ComboBoxCache";
import CachedSearch from "../../components/ComboCache/CachedSearch";


const ddlList = [{ "countrycode": "IN", "countryName": "India" }];

describe('ComboboxCache component', () => {
  test('should shallow correctly', () => {
    expect(shallow(
      <ComboboxCache ddlList={ddlList} />
    )).toMatchSnapshot()
  })
  test('should mount correctly', () => {
    expect(mount(
      <ComboboxCache ddlList={ddlList} />
    )).toMatchSnapshot()
  })
 });

describe('Testing ComboboxCache Node', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    <ComboboxCache ddlList={ddlList} />
    ReactDOM.unmountComponentAtNode(div);
  })
});
