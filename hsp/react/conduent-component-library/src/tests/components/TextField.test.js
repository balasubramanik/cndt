import React from "react";
import { mount } from "enzyme";
import TextField from "../../components/textbox/TextField";

describe("<TextField />", () => {
  it("should have mounted", () => {
    const onChange = jest.fn();
    const component = mount(
      <TextField type="text" dataType="SYSTEM.INT32" onChange={onChange} />
    );
    expect(component.exists()).toBeTruthy();
  });

  it("should have a text type", () => {
    const onChange = jest.fn();
    const component = mount(<TextField type="text" onChange={onChange} />);
    expect(component.find('input[type="text"]').length).toEqual(1);
  });

  it("should have a number type", () => {
    const onChange = jest.fn();
    const component = mount(<TextField type="number" onChange={onChange} />);
    expect(component.find('input[type="number"]').length).toEqual(1);
  });

  it("should have a password type", () => {
    const onChange = jest.fn();
    const component = mount(<TextField type="password" onChange={onChange} />);
    expect(component.find('input[type="password"]').length).toEqual(1);
  });

  it("should allow numbers when dataType is SYSTEM.INT32", () => {
    const onChange = jest.fn();
    const component = mount(
      <TextField
        type="text"
        title="Text"
        defaultValue={1}
        dataType="SYSTEM.INT32"
        onChange={onChange}
      />
    );
    component.find("input").simulate("keypress", { keyCode: 49 });
    expect(component.props().defaultValue).toBe(1);
  });

  it("should fire blur event on input", () => {
    const onChange = jest.fn();
    const component = mount(
      <TextField
        type="text"
        required="Y"
        title="Text"
        defaultValue={1}
        dataType="SYSTEM.INT32"
        onChange={onChange}
      />
    );
    component.find("input").simulate("blur");
    expect(component.instance().state.validated).toBe(true);
  });

  it("should validate type of email", () => {
    const onChange = jest.fn();
    const component = mount(
      <TextField
        type="email"
        required="Y"
        title="Text"
        defaultValue="test@test.com"
        dataType="SYSTEM.INT32"
        onChange={onChange}
      />
    );
    component.find("input").simulate("blur");
    expect(component.instance().state.validated).toBe(true);
  });
});
