import React from "react";
import { mount, shallow } from "enzyme";
import CheckBox from "../../components/checkbox/CheckBox";

describe("<CheckBox />", () => {
  it("should have mounted", () => {
    const component = mount(
      <CheckBox id="isChecked" type="checkbox" label="Default checkbox 1" />
    );
    expect(component.exists()).toBeTruthy();
  });

  it("should have a checkbox type", () => {
    const component = mount(
      <CheckBox id="isChecked" type="checkbox" label="Default checkbox 1" />
    );
    expect(component.find('input[type="checkbox"]').length).toEqual(1);
  });

  it("change value after after blur", () => {
    const component = mount(
      <CheckBox id="isChecked" type="checkbox" label="Default checkbox 1" />
    );
    console.log(component);
    const input = component.find("input");
  });
});
