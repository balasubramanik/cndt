import React from "react";
import ReactDOM from "react-dom";
import renderer from "react-test-renderer";
import { mount, shallow } from "enzyme";
import DateTimePicker from "../../components/datepicker/DateTimePicker";

describe("Testing DateTime Node", () => {
  it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<DateTimePicker />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});
describe("Testing DateTime component snapshot match", () => {
  it("renders as expected", () => {
    const wrapper = shallow(<DateTimePicker />);
    expect(wrapper).toMatchSnapshot();
  });
});
const defaultProps = {
  date: new Date()
};
const DateInput = props => <DateTimePicker {...defaultProps} {...props} />;

describe("Render DateInput", () => {
  it("render correctly date component", () => {
    const DateInputComponent = renderer.create(<DateInput />);
    expect(DateInputComponent).toMatchSnapshot();
  });

  it("render date input correctly with empty value", () => {
    const props = {
      date: null
    };
    const DateInputComponent = mount(<DateInput {...props} />);
    expect(DateInputComponent.prop("date")).toEqual(null);
  });

  it('check the onChange callback', () => {
    const onChange = jest.fn(),
      props = {
        value: new Date('01/20/2020'),
        onChange
      },
      DateInputComponent = mount(<DateInput {...props} />).find('input');
    DateInputComponent.simulate('change', { target: { value: new Date('01/22/2020') } });
    expect(onChange).toBeCalled();
  });

  it("check the type of value", () => {
    const props = {
      date: new Date("03/10/2020")
    };
    const DateInputComponent = mount(<DateInput {...props} />);
    expect(DateInputComponent.prop("date")).toBeTruthy();
  });

  it("check DatePicker popup open", () => {
    const DateComponent = mount(<DateInput />);
    const dateInput = DateComponent.find("input[type='text']");
    dateInput.simulate("click");
    expect(DateComponent.find(".react-datepicker")).toHaveLength(2);
  });

  /* it('check month and years dropdowns displayed', () => {
    const props = {
      showMonthYearsDropdowns: true
    },
      DateInputComponent = mount(<DateInput {...props} />).find('.react-datepicker');
    expect(DateInputComponent.hasClass('react-datepicker')).toEqual(true);
  }); */
});
