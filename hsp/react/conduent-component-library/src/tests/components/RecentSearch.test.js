import React from "react";
import { mount } from "enzyme";
import RecentSearch from "../../components/lastsearch/RecentSearch";

describe("<RecentSearch />", () => {
  it("should have mounted", () => {
    const component = mount(<RecentSearch itemType="OfficeHistory" />);
    expect(component.exists()).toBeTruthy();
  });
});
