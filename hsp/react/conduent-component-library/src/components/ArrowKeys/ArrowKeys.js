/**
 * User_story: 48912
 *
 * Description:
 * ============================================================
 * As a User, I want to interact with Keyboard on all the web pages,
 * After User pushes Tab, Shift+Tab,
 * Enter and Space Buttons,
 * Up and Down Arrows
 * =============================================================
 *
 * Author: Jai Prakash
 *
 */

import React, { Component } from "react";

const ArrowKeys = (WrappedComponent, selector) => {
  if (!selector) {
    throw new Error("Selector is required for arrow keys.");
  }
  return class ArrowKeyEvent extends Component {
    constructor(props) {
      super(props);
      this.state = { index: this.props.index || 0 };
      this.time = null;
      this.keyHandler = this.keyHandler.bind(this);
    }

    keyHandler(e) {
      const { key } = e;
      this.setState(
        prevState => {
          let newIndex;
          const isDataLength = this.props.data && this.props.data.length - 1;
          if (key === "ArrowUp") {
            newIndex =
              prevState.index <= 0 ? isDataLength : prevState.index - 1;
          } else if (key === "ArrowDown") {
            newIndex =
              prevState.index >= isDataLength ? 0 : prevState.index + 1;
          }
          return { index: isNaN(newIndex) || !newIndex ? 0 : newIndex };
        },
        () => {
          if (this.time) {
            clearTimeout(this.time);
          }
          this.time = setTimeout(() => {
            if (document.querySelector(`${selector} li.hover`)) {
              document.querySelector(`${selector} li.hover`).focus();
            }
          }, 100);
        }
      );
    }

    componentDidMount() {
      document.addEventListener("keydown", this.keyHandler);
    }

    componentWillUnmount() {
      document.removeEventListener("keydown", this.keyHandler);
    }

    render() {
      return <WrappedComponent {...this.props} index={this.state.index} />;
    }
  };
};

export default ArrowKeys;
