/*
 * User Story ID: US15_Provider_RC_Date/Time_Picker
 * Date and Time picker to show on selection from calendar/time
 */
import React, { Component, useState } from "react";
import PropTypes from "prop-types";
import DatePicker from "./DatePicker";
import Config from "../config";
import "./react-datepicker.css";
import "../../assets/css/index.css";
import errorIcon from "../../assets/img/error-icon.png";
import moment from 'moment'

const DateComponent = props => {
  const [startDate, setStartDate] = useState(props.selectedDate || null);
  return (
    <DatePicker
      showMonthDropdown
      showYearDropdown
      scrollableYearDropdown
      selected={props.selectedDate || ""}
      yearDropdownItemNumber={15}
      placeholderText={props.placeHolder}
      dateFormat={Config.defaultDateFormat}
      onChange={date => {
        props.handleChange(date);
      }}
      onBlur={event => {
        const value = event.target.value;
        if (!moment(value).isValid() && isNaN(parseInt(value))) {    
          props.handleChange(new Date())
        } else if (parseInt(value) === 0) {
          props.handleChange(new Date(Config.minDate))
        }
      }}
      isClearable
      {...props}
    />
  );
};
class DateTimePicker extends Component {
  constructor(props) {
    super(props);
  }

  Date = DateComponent;

  Time = () => {
    return (
      <DatePicker
        selected={this.props.selectedDate}
        onChange={this.props.handleChange}
        showTimeSelect
        showTimeSelectOnly
        timeIntervals={1}
        timeCaption="Time"
        dateFormat={Config.defaultTimeFormat}
        placeholderText={this.props.placeHolder}
        strictParsing
        {...this.props}
      />
    );
  };

  render() {
    const picker = this.props.showTimeOnly ? (
      <this.Time {...this.props} />
    ) : (
        <this.Date {...this.props} />
      );
    const { validationErrors, controlName } = this.props;
    return (
      <React.Fragment>
        {function () {
          return <div>{picker}</div>;
        }.call(this)}
        {function () {
          if (validationErrors && validationErrors[controlName]) {
            return (
              <div className="error-message">
                <img src={errorIcon} alt="error-icon" />
                {validationErrors[controlName]}
              </div>
            );
          }
        }.call(this)}
      </React.Fragment>
    );
  }
}
DateTimePicker.defaultProps = {
  date: new Date(),
  placeHolder: "",
  showTimeOnly: false,
  minDate: new Date(Config.minDate),
  maxDate: new Date(Config.maxDate)
};
DateTimePicker.propTypes = {
  date: PropTypes.instanceOf(Date),
  minDate: PropTypes.instanceOf(Date),
  maxDate: PropTypes.instanceOf(Date),
  placeHolder: PropTypes.string,
  showTimeOnly: PropTypes.bool
};
export default DateTimePicker;
