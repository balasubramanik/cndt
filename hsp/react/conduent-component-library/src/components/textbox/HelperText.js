import React from "react";

function HelperText(props) {
  return (
    <small id={props.id} className="form-text text-muted">
      {props.children}
    </small>
  );
}

export default HelperText;
