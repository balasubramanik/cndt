/**
 * User Story Id : US12_Provider_Text_Box:
 * Description:  As a User, I want to create a Text Box as a reusable component so that it can display or edit information
 **/
import React, { Component } from "react";
import propTypes from "prop-types";
import classNames from "classnames";
import { validateEmail } from "../../common/Utils";
import "../../assets/css/index.css";
import errorIcon from "../../assets/img/error-icon.png";
import eyeIcon from "../../assets/img/eye-icon.png";
import eyeDisableIcon from "../../assets/img/eye-disable-icon.png";

class TextField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validated: false,
      showPasswod: false
    };
    this.onBlur = this.onBlur.bind(this);
    this.onKeyPress = this.onKeyPress.bind(this);
  }

  togglePassword() {
    this.setState({
      showPasswod: !this.state.showPasswod
    })
  }

  onBlur(event) {
    const validationStatus = [];
    const { value, required = false, type } = event.target;

    if (required) {
      validationStatus.push(!!value.length);
      if (type === "email") {
        validationStatus.push(validateEmail(value));
      }
    }
    const validated = validationStatus.every(value => value === true);
    this.setState({ validated: validated });
  }

  onKeyPress(event) {
    const { dataType } = this.props;
    if (
      dataType === "SYSTEM.INT32" ||
      dataType === "SYSTEM.SINGLE" ||
      dataType === "SYSTEM.INT64"
    ) {
      const keyCode = event.keyCode || event.which;
      const keyValue = String.fromCharCode(keyCode);

      if (!/[0-9\.]/.test(keyValue)) {
        event.preventDefault();
      }
    }
    if(typeof this.props.onKeyPress === "function") {
      this.props.onKeyPress(event)
    }
  }

  render() {
    const { validated, showPasswod } = this.state;
    const {
      type,
      autoComplete,
      label,
      id,
      title,
      value,
      passwordInput,
      controlName,
      validationErrors,
      defaultValue,
      areaDescribedBy,
      areaLabel,
      placeholder,
      min,
      max,
      minLength,
      maxLength,
      required,
      onChange
    } = this.props;

    const classes = classNames({
      // "is-valid": validated,
      // "is-invalid": !validated
    });
    const finalValue = value || defaultValue || "";
    const labelStar = required == "Y" && <span className="input-required">*</span>;

    return (
      <div
        className={
          (validationErrors && validationErrors[controlName]
            ? "invalid-control"
            : "") + " form-group"
        }
      >
        {label && (
          <label htmlFor={id}>
            {label} {labelStar}
          </label>
        )}
        {function () {
          const pwdIcon = (!showPasswod)?eyeIcon:eyeDisableIcon
          return (
            <div className={(passwordInput)?"password-input":""}>
              <input
                type={(passwordInput && showPasswod)?"text":type}
                className={`form-control TextField ${
                  required === "Y" ? classes : ""
                  }`}
                autoComplete={autoComplete}
                id={id}
                aria-describedby={areaDescribedBy}
                aria-label={areaLabel}
                placeholder={placeholder}
                minLength={minLength}
                min={min}
                max={max}
                maxLength={maxLength !== 0 ? maxLength : ""}
                onChange={onChange}
                onBlur={this.onBlur}
                value={finalValue}
                required={required === "Y" && required}
                onKeyPress={this.onKeyPress}
              />
              {
                (passwordInput)?<div className="show-password">
                  <img src={pwdIcon} alt="eye-icon"
                    onClick={(data)=>this.togglePassword()}
                  />
                </div>:""
              }
            </div>
          );
        }.call(this)}
        {function () {
          if (validationErrors && validationErrors[controlName]) {
            return (
              <div className="error-message">
                <img src={errorIcon} alt="error-icon" />
                {validationErrors[controlName]}
              </div>
            );
          }
        }.call(this)}
      </div>
    );
  }
}

TextField.propTypes = {
  type: propTypes.string,
  id: propTypes.string,
  value: propTypes.string
};

export default TextField;
