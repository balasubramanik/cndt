import React from 'react'
import NoRecIcon from '../../assets/img/no-record-found.png'
import "./ListView.css"

const NoRecordFound = (props) => {
    return (
        <div className="no-record-container">
            <div className="inner-container">
                <div className="no-record-icon">
                    <img src={NoRecIcon} alt="No Record Found" />
                </div>
                <div className="no-record-text">
                    No Items Found Matching Your Request
                    <p className="new-criteria"> Select new criteria and click Find.</p>
                </div>
            </div>
        </div>
    )
}

export default NoRecordFound;