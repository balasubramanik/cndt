/*
 * User Story ID: US16_List View
 * Description: List View(Grid) to display after Find search with given data
 * Author: Kannan
 */
import React, { Component } from "react";
import ReactTable, { ReactTableDefaults } from "react-table";
import "react-table/react-table.css";
import Pagination from "./Pagination";
import NoRecordFound from "./NoRecordFound"
import classNames from "classnames";
import moment from "moment";
// import cloneDeep from 'lodash/cloneDeep';
// import PrintWithWindow from './PrintWithWindow';
import "./ListView.css";
import { Constants } from "../../common/Constants";

Object.assign(ReactTableDefaults, {
  PaginationComponent: Pagination
});

class ListView extends Component {
  defaultSettings = {
    resizer: [],
    reorder: []
  };

  constructor(props) {
    super(props);
    this.dragged = null;

    this.resizer = [];
    this.reorder = [];
    this.state = {
      rows: this.props.rows,
      columns: this.props.columns,
      trigger: 0,
      selected: 0,
      reset: false,
      hovered: false
    };
    this.myRef = React.createRef();
  }

  onRowSelect = e => {
    e.persist();
    const { key } = e;
    if (
      key === "Alt" ||
      key === "Shift" ||
      key === "Control" ||
      key === "ArrowRight" ||
      key === "ArrowLeft"
    ) {
      return;
    }
    const rows = Array.from(document.querySelectorAll(".rt-tr-group"));
    this.setState(
      state => {
        let newIndex;
        const isRowLength = rows && rows.length - 1;
        if (key !== "Enter") {
          if (key === "ArrowUp") {
            newIndex = state.selected <= 0 ? isRowLength : state.selected - 1;
          } else if (key === "ArrowDown") {
            newIndex = state.selected >= isRowLength ? 0 : state.selected + 1;
          }
        } else {
          newIndex = state.selected;
        }
        return { selected: newIndex };
      },
      () => {
        if (key === "Enter") {
          rows[this.state.selected].click();
        }
        this.onRowHighlight(e, rows);
      }
    );
  };

  onRowHighlight = (e, rows) => {
    const activeEl = document.querySelector(".active");
    activeEl.classList.remove("active");
    if (rows?.length) {
      rows[this.state.selected].classList.add("active");
      rows[this.state.selected].tabIndex = 1;
      rows[this.state.selected].focus();
    } else {
      e.target.classList.add("active");
    }
    activeEl.scrollIntoView();
  };

  mountEvents() {
    const headers = Array.prototype.slice.call(
      document.querySelectorAll(".draggable-header")
    );

    headers.forEach((header, i) => {
      header.setAttribute("draggable", true);
      // the dragged header
      header.ondragstart = e => {
        e.stopPropagation();
        this.dragged = i;
      };

      header.ondrag = e => e.stopPropagation;

      header.ondragend = e => {
        e.stopPropagation();
        setTimeout(() => (this.dragged = null), 100);
      };

      // the dropped header
      header.ondragover = e => {
        e.preventDefault();
      };

      header.ondrop = e => {
        e.preventDefault();
        this.reorder.push({ a: i, b: this.dragged });
        this.setState({ trigger: Math.random() });
      };
    });
  }

  componentDidMount() {
    this.mountEvents();
    if (this.props.rows && this.props.rows.length > 0) {
      const tr = document.querySelector(".rt-tr-group");
      tr.tabIndex = 1;
      tr.focus();
    }
  }

  componentDidUpdate() {
    this.mountEvents();
  }

  componentWillMount() {
    if (this.props.settings && Object.keys(this.props.settings).length > 0) {
      this.resizer = this.props.settings.resizer;
      this.reorder = this.props.settings.reorder;
    }
  }

  changeDateFormat = (realDate, inputFormat) => {
    const formattedDate = moment(moment(realDate).toDate()).format(inputFormat);
    return formattedDate;
  };

  changeDateTimeFormat = (realDate, inputFormat) => {
    const formattedDate = moment(moment(realDate).toDate()).format(inputFormat);
    return formattedDate;
  };

  changeTimeFormat = (time, inputFormat) => {
    if (!time) {
      return;
    }
    const formattedTime = moment(time, ["h:mm A"]).format(inputFormat);
    return formattedTime;
  };

  resetColumn() {
    if (this.props.settings && Object.keys(this.props.settings).length > 0) {
      const settings = JSON.parse(
        localStorage.getItem(this.props.settings.type)
      );

      this.resizer = settings.resizer;
      this.reorder = settings.reorder;
    } else {
      this.resizer = [];
      this.reorder = [];
    }

    this.setState({ trigger: Math.random() });
  }

  handleSortChange = newSort => {
    // Ascending
    if (!this.state.isSorted) {
      // this.state.isSorted is initially false
      this.setState({
        sort: newSort,
        isSorted: true
      });
      // None
    } else if (!newSort[0].desc) {
      this.setState({
        sort: [],
        isSorted: false
      });
      // Descending
    } else {
      this.setState({ sort: newSort });
    }
  };

  handleClickEvent(id, info, event) {
    event.stopPropagation();
    if (id === "NW") {
      this.props.OnOpenInNewWindowClick("NW", info);
    }
    if (id === "TW") {
      this.props.OnOpenInNewTabClick("TW", info);
    }
    if (id === "SW") {
      this.props.OnStyleSheetClick("SW", info);
    }
    if (id === "ROW") {
      this.props.selectedIndex("ROW", info);
    }
  }

  SaveSettings() {
    if (this.props.SaveColumns) {
      this.props.SaveColumns({
        resizer: this.resizer,
        reorder: this.reorder
      });
    }
  }

  setResize(cols) {
    cols.map((col, key) => {
      const id = col.id.split("#")[0];
      this.resizer.map((obj, i) => {
        if (obj.id === id) {
          this.resizer[i].value = col.value;
        }
      });
      const isExt = this.resizer.filter(el => el.id === id);
      if (isExt.length === 0) {
        this.resizer.push({ id: id, value: col.value });
      }
    });
  }

  firstMouseOver() {
    console.log("---------------")
    this.setState({ hovered: true })
  }

  render() {
    const rows = this.props.rows || [];
    const columns = this.props.columns;
    let cols = [];
    const getColWidth = obj => {
      const ColObj =
        this.resizer.length > 0 &&
        this.resizer.filter(el => el.id === obj.accessor);

      if (ColObj && ColObj.length > 0) {
        return ColObj[0].value;
      } else if (obj.width) {
        return obj.width;
      }
      return Constants.DEFAULT_COLUMN_WIDTH;
    };

    columns.map((obj, key) => {
      if (this.props.SerialNoRequired === true && key === 0) {
        cols.push({
          Header: "#",
          id: "row",
          width: 40,
          className: "addIcon",
          resizable: false,
          Cell: row => {
            return (
              <span className="float-left" draggable="false">
                {row.index + 1}
              </span>
            );
          }
        });
      }

      cols.push({
        width: getColWidth(obj),
        Header: obj['@Name'],
        accessor: obj["@Name"].charAt(0).toLowerCase() + obj["@Name"].slice(1),
        className: "addIcon",
        Cell: row => {
          return obj.format === "Date" ? (
            <span className="trimText" draggable="true" title={this.changeDateFormat(row.value, obj.dataformat)}>
              {this.changeDateFormat(row.value, obj.dataformat)}
            </span>
          ) : obj.format === "DateTime" ? (
            <span className="trimText" draggable="true" title={this.changeDateTimeFormat(row.value, obj.dataformat)}>
              {this.changeDateTimeFormat(row.value, obj.dataformat)}
            </span>
          ) : obj.format === "Time" ? (
            <span className="float-left" draggable="true">
              {this.changeTimeFormat(row.value, obj.dataformat)}
            </span>
          ) : (
                  <span className="trimText" draggable="true" title={row.value}>
                    {row.value}
                  </span>
                );
        }
      });
      if (
        this.props.DisplayIconRequired === true &&
        key === columns.length - 1
      ) {
        cols.push({
          width: -1,
          Header: "",
          accessor: "",
          Cell: row => {
            return (
              <span className="float-left LastRow" draggable="false">
                <React.Fragment>
                  <div className="dropdown_Main">
                    <button
                      className="dropbtnStyle"
                      style={{ visibility: "hidden" }}
                    >
                      <img
                        alt="Show Stylesheet"
                        src={process.env.PUBLIC_URL + "/assets/stylesheet.png"}
                        onClick={e =>
                          this.handleClickEvent("SW", row.original, e)
                        }
                      />
                    </button>
                    <div className="dropdown">
                      <button
                        className="dropbtn"
                        style={{ visibility: "hidden" }}
                      >
                        <img
                          alt="Show Details in new window/tab"
                          src={process.env.PUBLIC_URL + "/assets/pwdShow.png"}
                        />
                      </button>
                      <div className="dropdown-content">
                        <a
                          onClick={e =>
                            this.handleClickEvent("NW", row.original, e)
                          }
                        >
                          Open in New Window
                        </a>
                        <a
                          onClick={e =>
                            this.handleClickEvent("TW", row.original, e)
                          }
                        >
                          Open New Tab
                        </a>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              </span>
            );
          }
        });
      }
    });
    cols = cols.map(col => ({
      ...col,
      id: col.accessor + "#" + Math.random(),
      Header: (
        <span className="draggable-header" draggable="true" title={col.Header}>
          {col.Header}
        </span>
      )
    }));
    this.reorder.forEach(o => {
      cols.splice(o.a, 0, cols.splice(o.b, 1)[0]);
      // return this.props.columnOrder(cols);
    });

    return (
      <div className="esr-table">
        <div className="row pl-2 pr-2 mb-0 align-items-end">
          <div className="col-6 text-left ListViewTitle pl-3">
            {this.props.ListViewTitle}
          </div>
          <div className="col-6 text-right">
            <img
              className="ClickIcon"
              src={process.env.PUBLIC_URL + "/assets/restore.png"}
              alt="restore"
              title="Restore default column settings"
              onClick={() => this.resetColumn()}
            />
            <img
              className="ClickIcon"
              src={process.env.PUBLIC_URL + "/assets/floppy-save.png"}
              alt="Save"
              title="Save column settings"
              onClick={() => this.SaveSettings()}
            />
            <img
              className="ClickIcon"
              src={process.env.PUBLIC_URL + "/assets/printer.png"}
              alt="Out of Scope-1"
              title="Out of Scope"
            />
            {/* Enable below if print option required */}
            {/* <PrintWithWindow
              print={true}
              trigger={() => <img className="ClickIcon" src={process.env.PUBLIC_URL + "/assets/printer.png"} alt="Out of Scope" title="Out of Scope" />}
              content={() => this.myRef} /> */}
          </div>
        </div>
        <div className="col-12 pl-2 pr-2">
          {
            function () {
              if (this.props.searchPerformed && !rows.length) {
                return (<NoRecordFound />)
              } else {
                return <ReactTable
                  {...this.props}
                  data={rows}
                  columns={cols}
                  getTrGroupProps={(state, rowInfo, column, instance) => {
                    if (
                      typeof rowInfo !== "undefined" &&
                      this.props.rows &&
                      this.props.rows.length > 0
                    ) {
                      return {
                        onKeyDown: this.onRowSelect,
                        className:
                          (rowInfo.index === 0 && this.state.hovered === false)
                            ? (state.className += " active")
                            : instance.state.className,
                        onClick: (e, handleOriginal) => {
                          this.onRowSelect(e);
                          this.setState({ selected: rowInfo.index });
                          if (handleOriginal) {
                            handleOriginal();
                          }
                        },
                        onMouseOver: (e, handleOriginal) => {
                          if (this.state.hovered === false) {
                            this.firstMouseOver();
                          }
                        }
                      };
                    }
                  }}
                  getTrProps={(state, rowInfo, column, instance) => {
                    if (typeof rowInfo !== "undefined") {
                      return {
                        onClick: (e, handleOriginal) => {
                          this.setState({
                            selected: rowInfo.index
                          });
                          this.handleClickEvent("ROW", rowInfo, e);
                          // this.props.selectedIndex(rowInfo);
                          if (handleOriginal) {
                            handleOriginal();
                          }
                        },
                        style: {
                          background:
                            rowInfo.index === this.state.selected
                              ? "#e5ecf8"
                              : "white",
                          color:
                            rowInfo.index === this.state.selected ? "black" : "black"
                        }
                      };
                    } else {
                      return {
                        onClick: (e, handleOriginal) => {
                          // this.props.selectedIndex(rowInfo);
                          this.handleClickEvent("ROW", rowInfo, e);
                          if (handleOriginal) {
                            handleOriginal();
                          }
                        },
                        style: {
                          background: "white",
                          color: "black"
                        }
                      };
                    }
                  }}
                  onResizedChange={e => {
                    this.setResize(e);
                  }}
                  getPaginationProps={() => {
                    return {
                      totalSize: rows ? rows.length : 0,
                      currentCount: this.props.defaultPageSize,
                      pageRangeDisplayed: 4,
                      marginPagesDisplayed: 1
                    };
                  }}
                  ref={el => (this.myRef = el)}
                  className={classNames({ "-striped": true, "-highlight": true })}
                />
              }

            }.call(this)
          }
        </div>
      </div>
    );
  }
}
export default ListView;
