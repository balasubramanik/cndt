import React, { Component } from "react";
import ComboBox from "../combobox";
import PrintWithWindow from "./PrintWithWindow";
class ToolBar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      values: this.props.selectedStyle || [],
      zoom: this.props.selectedZoom || [{ label: "100%", value: 1 }]
    };
  }

  refreshStyleXML(v) {
    if (this.props.onRefresh) {
      this.props.onRefresh(v);
    }
  }

  setSelectedStyle(v) {
    if (this.props.onSelectStyle) {
      this.props.onSelectStyle(v);
    }
    this.setState({ values: v });
  }

  render() {
    const zoomData = [
      {
        label: "400%",
        value: 4
      },
      {
        label: "200%",
        value: 2
      },
      {
        label: "150%",
        value: 1.5
      },
      {
        label: "125%",
        value: 1.25
      },
      {
        label: "100%",
        value: 1
      },
      {
        label: "75%",
        value: 0.75
      },
      {
        label: "50%",
        value: 0.5
      }
    ];

    return (
      <div>
        <div className="row">
          <div className="col-lg-3">
            <div className="bg-white m-2 styleCombobox">
            <label htmlFor="userreports" className="p-0 d-inline visuallyhidden">User Reports</label>
              {this.props.styles && (
                <ComboBox
                  id="userreports"
                  columns={[
                    { name: "Report Name", field: "reportName" },
                    { name: "Report Path", field: "reportPath" }
                  ]}
                  onChange={v => {
                    this.setSelectedStyle(v);
                  }}
                  values={this.props.selectedStyle}
                  labelField="reportName"
                  valueField="userReportId"
                  options={this.props.styles}
                  searchable={false}
                />
              )}
            </div>
          </div>

          <div className="col-lg-9">
            <ul className="list-group bg-transparent list-group-horizontal float-right ">
              <li
                title="Preview style sheet"
                className="list-group-item  bg-transparent border-0"
                onClick={() => this.refreshStyleXML(this.state.values)}
              >
                <img
                  alt=""
                  src={process.env.PUBLIC_URL + "/assets/refresh-new.png"}
                />
              </li>
              <li
                title="Print style sheet"
                className="list-group-item bg-transparent border-0"
              >
                <PrintWithWindow
                  print
                  trigger={() => (
                    <img
                      alt=""
                      src={process.env.PUBLIC_URL + "/assets/printer.png"}
                    />
                  )}
                  content={() => this.props.printRef}
                />
              </li>
              <li
                title="display stylesheet in a separate window"
                className="list-group-item bg-transparent border-0"
              >
                <PrintWithWindow
                  popup
                  trigger={() => (
                    <img
                      alt="New Window"
                      src={process.env.PUBLIC_URL + "/assets/new-window.png"}
                    />
                  )}
                  content={() => this.props.printRef}
                />
              </li>
              <li
                title="Set the browser's zoom level "
                className="list-group-item bg-transparent border-0"
              >
                <label htmlFor="zoom" className="p-0 d-inline visuallyhidden">Zoom level</label>
                <div className="zoom">
                  <ComboBox
                    id="zoom"
                    values={this.state.zoom}
                    onChange={v => {
                      this.props.onZoom(v[0].value);
                    }}
                    labelField="label"
                    valueField="value"
                    options={zoomData}
                    searchable={false}
                  />
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

ToolBar.propTypes = {};

export default ToolBar;
