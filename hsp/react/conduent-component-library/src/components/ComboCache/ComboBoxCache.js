/**
 * User Story Id : US14_Provider_Combo_Box_Caching
 * Description : As a User, I want to create Combo Box Caching so that it can manage drop down lists for combos
 **/
import React, { useMemo, useState } from "react";
import CachedSearch from "./CachedSearch";
import PropTypes from "prop-types";

const ComboBoxCache = props => {
  const [query, setQuery] = useState("");
  const [results, setResults] = useState([]);
  const [value, setValue] = useState();

  const ddlList = props.ddlList;

  const searchAPI = query =>
    new Promise((resolve, reject) => {
      const JsonResults = fetch(props.resultAPI + query)
        .then(res => res.json())
        .then(data => {
          return data;
        });
      resolve(JsonResults);
    });

  const cachedSearch = useMemo(
    () => new CachedSearch(searchAPI, setResults),
    []
  );

  const handleQueryChange = query => {
    if (query != "") {
      setQuery(query);
      cachedSearch.changeQuery(query);
    }
  };

  const optionItems = ddlList.map(item => (
    <option key={item.name} value={item.value}>
      {item.name}
    </option>
  ));

  return (
    <div>
      <select
        onChange={e => handleQueryChange(e.currentTarget.value)}
        value={value}
        className={props.styleClass}
        style={{ width: "200px" }}
      >
        <option value="">{props.placeholder} </option>
        {optionItems}
      </select>
      <br />
      <br />
      <br />
      GitHub Created on : {results.created_at}
    </div>
  );
};

// Set Prop Types for the ComboBox
ComboBoxCache.propTypes = {
  DefaultValue: PropTypes.string,
  placeholder: PropTypes.string,
  ddlList: PropTypes.array,
  resultAPI: PropTypes.string,
  styleClass: PropTypes.string,
  DisplayFieldName: PropTypes.string,
  DisplayIDName: PropTypes.string,
  FindDisplayName: PropTypes.string
};

// Set default Prop Types for the ComboBox
ComboBoxCache.defaultProps = {
  DefaultValue: "",
  styleClass: "",
  placeholder: "",
  ddlList: [],
  DisplayFieldName: "",
  DisplayIDName: "",
  FindDisplayName: ""
};

export default ComboBoxCache;
