/**
 * User Story Id : US18_Provider_Check_Box:
 * Description: As a User, I want to create a Check Box as a reusable component so that it can display or edit information
 **/
import React, { Component } from "react";
import propTypes from "prop-types";
import classNames from "classnames";
import "./CheckBox.css";

class CheckBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validated: false
    };
    this.handleValidation = this.handleValidation.bind(this);
  }

  handleValidation(event) {
    const { checked, required } = event.target;

    if (required) {
      this.setState({ validated: checked });
    }
  }

  render() {
    const { validated } = this.state;
    const { type, required, id, label, checked, onChange } = this.props;
    const classes = classNames({
      "is-valid": validated,
      "is-invalid": !validated
    });

    return (
      <div className="custom-checkbox form-group">
        <input
          type={type}
          className={`form-check-input ${required ? classes : ""}`}
          id={id}
          value={label}
          checked={checked}
          onBlur={this.handleValidation}
          {...this.props}
        />
        <label onClick={(event)=>onChange(event)} className="form-check-label" htmlFor={id}>
          {label}
        </label>
      </div>
    );
  }
}

CheckBox.propTypes = {
  type: propTypes.string,
  id: propTypes.string,
  value: propTypes.string,
  checked: propTypes.bool
};
export default CheckBox;
