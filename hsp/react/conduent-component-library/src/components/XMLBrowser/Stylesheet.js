/*
 * User Story ID: US26_Provider_RC_XML_Browse
 * Description: As a User, I want to create XML Browser as a reusable component so that it can display stylesheet on screens and can be customized
 * Author: Bala
 */
import React, { Component } from "react";
import PropTypes from "prop-types";
class Stylesheet extends Component {
  constructor(props) {
    super(props);

    this.state = {
      data: []
    };

    this.script = null;

    this.getHtmlContent = this.getHtmlContent.bind(this);
    this.extractScripts = this.extractScripts.bind(this);
  }

  extractScripts(htmlResponse) {
    let extractedScript;
    if (htmlResponse) {
      extractedScript = htmlResponse.match(
        /<script language="javascript">[\s\S]*<\/script>/gi
      )[0];
      extractedScript = extractedScript.split("\n");
      extractedScript.pop();
      extractedScript.shift();
      extractedScript = extractedScript.join("\n");
    }
    this.script = extractedScript;
    return { extractedScript, htmlResponse };
  }

  getHtmlContent(objWithHtmlAndScript) {
    window.eval(this.script);
    return { __html: objWithHtmlAndScript.htmlResponse };
  }

  render() {
    const htmlObj = this.extractScripts(this.props.data.htmlResult);
    if (!htmlObj.htmlResponse) {
      return (
        <div className="spinner-border spinner-border-sm" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    } else {
      return <div dangerouslySetInnerHTML={this.getHtmlContent(htmlObj)} />;
    }
  }
}

Stylesheet.propTypes = {
  data: PropTypes.object.isRequired
};

export default Stylesheet;
