/**
 * User_story: 48915 & 48919
 *
 * Description:
 * =======================================================
 * As a User, I want a Combo Box as a reusable component
 * so that it can display and select list
 * =======================================================
 */

import React from "react";
import PropTypes from "prop-types";

import ComboboxOption from "./ComboboxOption";
import { UnorderedList } from "../styled";

const ComboboxList = React.forwardRef((props, ref) => {
  const ulRef = ref;
  const errorOption = !props.isItemFound && (
    <ComboboxOption
      error="Not found"
      selectItem={props.selectItem}
      isItemFound={props.isItemFound}
    />
  );
  const comboboxOptionList =
    props.config.data && props.isItemFound
      ? props.config.data.map((item, i) => (
          <ComboboxOption
            optionRef={props.optionRef}
            key={i}
            item={item}
            config={props.config}
            onEnter={props.selectItem}
            selectItem={props.selectItem}
            multi={props.config.multi}
            row={i}
            index={props.index}
            isItemFound={props.isItemFound}
            selectedOption={props.selectedOption}
          />
        ))
      : errorOption;

  const sortFilter = e => {
    props.sortFilter(e.target.dataset.sortby, props.index);
  };

  return (
    <UnorderedList
      ref={props.ulRef || ulRef}
      hidden={props.hidden}
      round={props.round}
      multi={props.config.multi}
    >
      <li className="header">
        {props.config.multi &&
          props.config.columns.map((item, i) => (
            <button
              onClick={sortFilter.bind(this)}
              key={i}
              data-sortby={item.column}
            >
              {item.columnTitle}
            </button>
          ))}
      </li>
      {comboboxOptionList || "Loading..."}
    </UnorderedList>
  );
});

ComboboxList.propTypes = {
  size: PropTypes.string,
  users: PropTypes.array,
  selectItem: PropTypes.func,
  hidden: PropTypes.bool,
  round: PropTypes.number,
  multi: PropTypes.bool
};

export default ComboboxList;
