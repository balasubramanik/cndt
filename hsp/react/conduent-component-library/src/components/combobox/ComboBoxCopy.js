/**
 * User_story: 48915 & 48919
 *
 * Description:
 * =======================================================
 * As a User, I want a Combo Box as a reusable component
 * so that it can display and select list
 * =======================================================
 */

import React, { Component, createRef } from "react";
import PropTypes from "prop-types";

import { ComboboxWrapper, Input, Button } from "../styled";
import KeyboardEvents from "../HOC";
import Label from "../Label";
import ComboboxList from "./ComboboxList";

class ComboBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      data: this.props.data || [],
      config: this.props.config || {},
      dataCopy: this.props.data || [],
      isItemFound: true,
      selectedUser: null,
      selectedUserName: "",
      selectedOption: null,
      index: this.props.index || 0,
      required: this.props.required,
      visible: this.props.visible,
      errorClass: null
    };

    this.inputRef = createRef();
    this.comboListRef = createRef();
    this.optionRef = createRef();
    this.required = !!this.props.required;
    this.size = this.props.size;
    this.round = this.props.round;
    this.label = this.props.label;
    this.multi = this.props.multi;

    this.filterList = this.filterList.bind(this);
    this.sortFilter = this.sortFilter.bind(this);
    this.setSelectedObj = this.setSelectedObj.bind(this);
    this.selectItem = this.selectItem.bind(this);
    this.isRequiredElem = this.isRequiredElem.bind(this);
    this.toggleList = this.toggleList.bind(this);
    this.toggleScroll = this.toggleScroll.bind(this);
  }

  setSelectedObj(e) {
    this.setState({ selectedUserName: e.target.value, isOpen: true }, () => {
      this.filterList();
    });
  }

  filterList() {
    let filtered;
    const inputValue = this.state.selectedUserName;
    if (inputValue.length && this.multi) {
      filtered = this.state.dataCopy.filter(
        obj =>
          obj.name.toLowerCase().includes(inputValue) ||
          obj.username.toLowerCase().includes(inputValue)
      );
    } else if (inputValue.length && !this.multi) {
      filtered = this.state.dataCopy.filter(obj =>
        obj.username.toLowerCase().includes(inputValue)
      );
    } else {
      filtered = this.state.dataCopy;
    }
    this.setState(state => ({ data: filtered, isItemFound: filtered.length }));
  }

  sortFilter(sortby, index) {
    const sortedList = this.state.config.data.sort((a, b) => {
      const sortByA = a[sortby].toUpperCase();
      const sortByB = b[sortby].toUpperCase();
      if (sortByA < sortByB) {
        return -1;
      } else if (sortByA > sortByB) {
        return 1;
      } else {
        return 0;
      }
    });
    this.setState(state => {
      return {
        data: sortedList,
        index: this.state.selectedOption,
        isOpen: true
      };
    });
  }

  toggleList(e) {
    this.setState(state => {
      return {
        isOpen: !state.isOpen,
        data: this.props.data,
        dataCopy: this.props.data
      };
    });
    this.toggleScroll(e);
  }

  selectItem(optionObj, selectedOption) {
    this.setState(
      {
        selectedUserName: this.state.config.multi
          ? `${optionObj[this.state.config.columns[0].column] +
              " " +
              optionObj[this.state.config.columns[1].column]}`
          : `${optionObj.text}`,
        selectedUser: optionObj,
        selectedOption
      },
      () => {
        this.props.onValue(optionObj);
      }
    );
    this.toggleList();
  }

  toggleScroll(e) {
    document.body.classList.toggle("no-scroll", !this.state.isOpen);
    this.setState({
      required: this.state.required === "Y",
      errorClass: this.state.required === "Y" ? "isRequired" : ""
    });
  }

  isRequiredElem(e) {
    this.setState(
      {
        isOpen: this.state.isOpen,
        required: this.state.required === "N",
        errorClass: this.state.required === "N" && "isRequired"
      },
      () => {
        this.toggleScroll();
      }
    );
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      config: nextProps.config,
      data: this.props.data,
      visible: this.props.visible,
      required: this.props.required
      // selectedUserName : this.props.defaultValue || ""
    });
  }

  componentDidMount() {
    document.addEventListener("click", e => {
      if (document.activeElement === document.body && this.state.isOpen) {
        this.setState({ isOpen: !this.state.isOpen }, () => {
          document.body.classList.toggle("no-scroll", this.state.isOpen);
        });
      }
    });
  }

  render() {
    const label = this.label && (
      <Label htmlFor="display_value">{this.label}</Label>
    );
    const ComboBoxWrapperEl = (
      <ComboboxWrapper
        hidden={this.props.visible !== "Y"}
        className={this.state.errorClass}
        size={this.size}
        round={this.round}
        multi={this.multi}
      >
        <Input
          ref={this.inputRef}
          type="text"
          name={this.props.name}
          autoComplete="off"
          placeholder={this.props.placeholder}
          value={this.state.selectedUserName}
          onClick={this.toggleList}
          onBlur={this.isRequiredElem}
          title={this.props.tooltip}
          readOnly
        />
        <Button tabIndex="-1" onClick={this.toggleList}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
          >
            <path fill="#000" fillRule="evenodd" d="M8 10l4 5 4-5z" />
          </svg>
        </Button>
        <ComboboxList
          ulRef={this.comboListRef}
          optionRef={this.optionRef}
          size={this.size}
          data={this.state.data}
          config={this.state.config}
          hidden={!this.state.isOpen}
          round={this.round}
          multi={this.multi}
          isItemFound={this.state.isItemFound}
          selectedOption={this.state.selectedOption}
          index={this.props.index}
          selectItem={this.selectItem}
          sortFilter={this.sortFilter}
        />
      </ComboboxWrapper>
    );
    return (
      <React.Fragment>
        {label}
        {ComboBoxWrapperEl}
      </React.Fragment>
    );
  }
}

ComboBox.propTypes = {
  size: PropTypes.string,
  round: PropTypes.number,
  label: PropTypes.string,
  multi: PropTypes.bool,
  config: PropTypes.object.isRequired,
  required: PropTypes.string.isRequired
};

export default KeyboardEvents(ComboBox);
