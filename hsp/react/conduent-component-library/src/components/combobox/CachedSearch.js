export default class CachedSearch {
  constructor(searchFunction, resultsHandler) {
    this.searchFunction = searchFunction;
    this.resultsHandler = resultsHandler;
    this.query = "";
    this.cache = {};
    this.cacheHitsHistory = [];
  }

  changeQuery(query) {
    if (query.length < 0) {
      this.resultsHandler([]);
      return;
    }
    if (this.cache[query]) {
      this.cacheHitsHistory.concat(query);
      this.resultsHandler(this.cache[query]);
    } else {
      this.searchFunction(query).then(results => {
        this.cache[query] = results;
        this.resultsHandler(results);
      });
    }
  }
}
