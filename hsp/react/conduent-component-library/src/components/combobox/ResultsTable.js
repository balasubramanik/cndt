import React from "react";

export default ({ results }) => (
  <table border="1" style={{ marginTop: "10px" }}>
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>location</th>
        <th>created_at</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{results.id}</td>
        <td>{results.login}</td>
        <td>{results.location}</td>
        <td>{results.created_at}</td>
      </tr>
    </tbody>
  </table>
);
