import React, { useMemo, useState } from "react";
import CachedSearch from "./CachedSearch";
import ResultsTable from "./ResultsTable";
import PropTypes from "prop-types";

const ComboBoxCahe = props => {
  const [query, setQuery] = useState("");
  const [results, setResults] = useState([]);
  const [value, setValue] = useState();

  const searchAPI = query =>
    new Promise((resolve, reject) => {
      const results = fetch(props.resultAPI + query)
        .then(res => res.json())
        .then(data => {
          return data;
        });
      resolve(results);
    });

  const cachedSearch = useMemo(
    () => new CachedSearch(searchAPI, setResults),
    []
  );
  const handleQueryChange = query => {
    setQuery(query);
    cachedSearch.changeQuery(query);
  };

  return (
    <div>
      <div style={{ width: "200px" }}>
        <select
          onChange={e => handleQueryChange(e.currentTarget.value)}
          value={value}
          className={props.styleClass}
        >
          <option value="">{props.placeholder} </option>
          {props.ddlList.map(item => (
            <option key={item.name} value={item.value}>
              {item.name}
            </option>
          ))}
        </select>
      </div>
      <br />
      <div>
        <ResultsTable results={results} />
      </div>
    </div>
  );
};

// ComboBoxCahe.propTypes = {
//   value: PropTypes.string,
//   placeholder: PropTypes.string,
//   ddlList: PropTypes.array.isRequired,
//   resultAPI: PropTypes.array.isRequired,
//   styleClass: PropTypes.string,
//   onChange: PropTypes.func.isRequired
// };

// ComboBoxCahe.defaultProps = {
//   value: '',
//   styleClass: '',
//   placeholder: '',
//   ddlList: []
// };

export default ComboBoxCahe;
