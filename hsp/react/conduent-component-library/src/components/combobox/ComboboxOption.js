/**
 * User_story: 48915 & 48919
 *
 * Description:
 * =======================================================
 * As a User, I want a Combo Box as a reusable component
 * so that it can display and select list
 * =======================================================
 */

import React from "react";
import PropTypes from "prop-types";

const ComboboxOption = React.forwardRef((props, ref) => {
  let cssClass = props.index === props.row ? "hover" : "";
  if (!cssClass) {
    cssClass = !props.isItemFound && "error";
  }
  const tbIndex = props.index === props.row ? 1 : 0;
  const selectItem = e => {
    props.selectItem(props?.item, props.row);
  };
  const onEnter = e => {
    if (e.key === "Enter") {
      props.selectItem(props?.item, props.row);
    }
  };

  const onSelect = e => {
    e.currentTarget.focus();
    e.currentTarget.tabIndex = 1;
    e.currentTarget.classList.toggle("hover", e.type === "mouseenter");
  };

  const options = props.multi ? (
    <li
      ref={props.optionRef}
      tabIndex={tbIndex}
      onClick={selectItem.bind(this)}
      onKeyDown={onEnter.bind(this)}
      onMouseEnter={onSelect}
      onMouseLeave={onSelect}
      className={cssClass.toString()}
    >
      <div>{props?.item[props.config.columns[0].column]}</div>
      <div>{props?.item[props.config.columns[1].column]}</div>
    </li>
  ) : (
    <li
      ref={props.optionRef}
      tabIndex={tbIndex}
      onClick={selectItem.bind(this)}
      onKeyDown={onEnter.bind(this)}
      onMouseEnter={onSelect}
      onMouseLeave={onSelect}
      className={cssClass.toString()}
    >
      {props.isItemFound ? props.item.text : props.error}
    </li>
  );
  return options;
});

ComboboxOption.propTypes = {
  item: PropTypes.object,
  selectItem: PropTypes.func.isRequired,
  multi: PropTypes.bool,
  row: PropTypes.number,
  index: PropTypes.number
};

export default ComboboxOption;
