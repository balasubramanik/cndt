import React, { Component } from "react";
import { Select } from "react-dropdown-select";
import "./ComboBox.css";
import errorIcon from "../../assets/img/error-icon.png";
import "../../assets/css/index.css";

class ComboBox extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortColumn: "",
      sortOrder: ""
    };
    this.toggleSort = this.toggleSort.bind(this);
    this.compare = this.compare.bind(this);

    this.comboOptions = {
      dropdownPosition: "auto",
      dropdownHeight: "180px"
    };
  }

  toggleSort(colName, sortOrder) {
    if (colName !== "name") {
      return false;
    }
    this.setState({
      sortColumn: colName,
      sortOrder: sortOrder === "" ? "asc" : sortOrder
    });
  }

  compare(a, b) {
    const { sortColumn, sortOrder } = this.state;

    if (sortOrder === "") return 0;
    // Use toUpperCase() to ignore character casing
    const bandA = a[sortColumn].toUpperCase();
    const bandB = b[sortColumn].toUpperCase();

    let comparison = 0;
    if (bandA > bandB) {
      comparison = sortOrder === "asc" ? 1 : -1;
    } else if (bandA < bandB) {
      comparison = sortOrder === "asc" ? -1 : 1;
    }
    return comparison;
  }

  componentDidMount() {
    if (this.props.id) {
      const comboEle = document.getElementById(`${this.props.id}_Combo`);
      comboEle.querySelector(".react-dropdown-select-input").id = this.props.id;
      comboEle.querySelector(".react-dropdown-select").children[1].setAttribute("aria-labelledby", this.props.id);
    }
  }
  render() {
    const { columns, values, title } = this.props;
    const comboProps = { ...this.comboOptions, ...this.props };

    let options = this.props.options;
    const selectedValues = values || [];
    if (this.props.defaultOption) {
    }
    const referenceCodeType = comboProps.referenceCodeType || "";
    if (referenceCodeType !== "" && referenceCodeType !== null) {
      if (comboProps.required === "N") {
        const comboObject = {};
        comboObject[this.props.valueField] = "Any";
        if (comboProps.controlType === "ToolkitMultiColumnCombobox") {
          comboObject[this.props.labelField] = "None";
        } else if (comboProps.controlType === "ToolkitCombobox") {
          comboObject[this.props.labelField] = "Any";
          if(comboProps.isCascading) {
            comboObject[this.props.labelField] = "None";
          }
        }
        options = [comboObject].concat(options);
      }
    }
    let temp = [];
    const customDropdownRenderer = ({ props, state, methods }) => {
      const regexp = new RegExp(state.search, "i");
      // const myState = {...state}

      function onMouseEnter(e, option) {
        const rows = document.querySelectorAll(".combotable tr");
        [].forEach.call(rows, function (el, key) {
          el.classList.remove("focusme");
          if (e.target.parentElement === el) state.cursor = key;
        });
        rows[state.cursor].classList.add("focusme");
        rows[state.cursor].focus();
        // document.querySelector("tr").focus()
        document.onkeydown = checkKey;

        temp.push(option);
      }
      function onMouseLeave(e, option) {
        temp = temp.filter(
          el => el[props.valueField] !== option[props.valueField]
        );
      }

      function checkKey(event) {
        const element = document.querySelector(".combotable tr.focusme");
        element &&
          element.scrollIntoView({
            block: "end",
            behavior: "smooth"
          });
      }

      document.onkeypress = function (event) {
        if (state.dropdown && event.keyCode === 13) {
          // event.stopPropagation()
          event.preventDefault();
          if (temp.length > 0) {
            methods.addItem(temp[0]);
          }
          if (temp.length === 0 && state.cursor > 0) {
            methods.addItem(props.options[state.cursor - 1]);
          }
        }
      };

      if (state.cursor) {
        var rows = document.querySelectorAll(".combotable tr");

        [].forEach.call(rows, function (el) {
          el.classList.remove("focusme");

          rows[state.cursor].setAttribute("class", "focusme");
        });
      }

      const { sortColumn, sortOrder } = this.state;
      const filterOptons = props.options.filter(item =>
        regexp.test(item[props.searchBy] || item[props.labelField])
      );
      filterOptons.sort(this.compare);
      return (
        <div>
          <table
            id="combotable"
            className="table table-sortable combotable table-bordered"
          >
            <thead>
              <tr>
                {props.columns.map((col, i) => {
                  let toggleOrder = "";
                  let toggleClass = "";
                  if (sortColumn === col.field) {
                    toggleOrder = sortOrder === "asc" ? "desc" : "asc";
                    toggleClass = sortOrder;
                  }
                  return (
                    <th
                      className={toggleClass}
                      key={i}
                      onClick={() => {
                        this.toggleSort(col.field, toggleOrder);
                      }}
                    >
                      {col.name}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody>
              {filterOptons.map((option, i) => {
                if (!props.keepSelectedInList && methods.isSelected(option)) {
                  return null;
                }
                return (
                  <tr
                    tabIndex={i}
                    className="trCombo"
                    onMouseLeave={e => {
                      onMouseLeave(e, option);
                    }}
                    onMouseEnter={e => {
                      onMouseEnter(e, option);
                    }}
                    // onKeyUp={(e) => { onKeyPress(e, option) }}
                    key={option[props.valueField]}
                    onClick={
                      option.disabled ? null : () => methods.addItem(option)
                    }
                  >
                    {props.columns.map((col, j) => {
                      return (
                        <td key={j} title={option[col.field]}>
                          <a href="" />
                          {option[col.field]}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      );
    };

    if (columns && columns.length > 0) {
      comboProps.dropdownRenderer = customDropdownRenderer;
    }
    const labelStar = comboProps.required == "Y" && <span className="input-required">*</span>;
    return (
      <div id={`${this.props.id}_Combo`} className={
        (comboProps.validationErrors && comboProps.validationErrors[comboProps.controlName]
          ? "invalid-control"
          : "") + ""
      }>
        {comboProps.label && (
          <label htmlFor={comboProps.id}>
            {comboProps.label} {labelStar}
          </label>
        )}
        <Select {...comboProps} onChange= {(values)=>{
          if(values.length) {
            for (let i = 0; i < values.length; i++) {
              const element = values[i]
              if(element[comboProps.valueField] === "Any" || element[comboProps.valueField] === "None") {
                values[i][comboProps.valueField] = ""
              }
            }
          }
          if(typeof comboProps.onChange === "function") {
            comboProps.onChange(values)
          }
        }} values={selectedValues} options={options} />
        {function () {
          if (comboProps.validationErrors && comboProps.validationErrors[comboProps.controlName]) {
            return (
              <div className="error-message">
                <img src={errorIcon} alt="error-icon" />
                {comboProps.validationErrors[comboProps.controlName]}
              </div>
            );
          }
        }.call(this)}
      </div>
    );
  }
}

export default ComboBox;
