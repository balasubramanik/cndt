/**
 * User Story Id: US28_Provider_Button_Bar:
 * Description:  As a User, I want to create Button Bar as a reusable component so that it user can navigate sections on screens
 * Author: Kanan
 * Last Change: Biren : Changes required for integration with screen.
 *
 **/

import React from "react";
import "./buttonbar.css";

class ButtonBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: this.props.selected,
      btnList: this.props.routes,
      DisplayPanel: ""
    };
    this.onClickBtn = this.onClickBtn.bind(this);
  }

  onClickBtn(desc) {
    this.setState({ DisplayPanel: desc });
  }

  activeCls(tab) {
    if (this.state.selected === tab.id) {
      return "activeMenu";
    }
  }

  componentDidMount() {
    const list = this.props.routes[0].children;
    for (var i = 0; i < list.length; i++) {
      for (var j = 0; j < list[i].children.length; j++) {
        if (list[i].children[j].id === this.props.selected) {
          this.setState({ DisplayPanel: list[i].children[j].desc });
        }
      }
    }
  }

  rtnBtn() {
    const tempVal = this.state.btnList[0].children;
    return (
      <React.Fragment>
        {tempVal.map((tp, i) => (
          <div key={i}>
            <span style={{ fontWeight: "600" }}>{tp.TabPanel}</span>{" "}
            <ul>
              {" "}
              {tp.children.map((tab, i) => (
                <li key={tab.name}>
                  <a
                    className={this.activeCls(tab)}
                    onClick={() => {
                      this.setState({
                        selected: tab.id,
                        DisplayPanel: tab.desc
                      });
                      this.setSelected(tab.id);
                    }}
                  >
                    {tab.name}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </React.Fragment>
    );
  }

  setSelected(id) {
    if (this.props.onSelected) {
      this.props.onSelected(id);
    }
  }

  render() {
    return (
      <div>
        <div style={{ width: "100 vw" }}>
          <table>
            <tbody>
              <tr>
                <td>
                  {" "}
                  <div
                    className="sideHeader"
                    style={{ width: "25 vw", verticalAlign: "top" }}
                  >
                    {this.rtnBtn()}
                  </div>
                </td>
                <td
                  style={{
                    width: "75 vw",
                    verticalAlign: "top",
                    display: this.props.container ? "block" : "none"
                  }}
                >
                  {" "}
                  <div id="tab_desc"> {this.state.DisplayPanel} </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ButtonBar;
