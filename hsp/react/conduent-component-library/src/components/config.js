/* Configuration file to include all common properties to use */
export default {
  defaultDateFormat: "MM/dd/yyyy",
  defaultTimeFormat: "h:mm aa",
  minDate: "01/01/1900",
  maxDate: "12/31/9999"
};
