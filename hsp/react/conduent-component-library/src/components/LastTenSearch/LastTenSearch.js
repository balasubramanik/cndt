/**
 * User_story: 48922
 *
 * Description:
 * ============================================================
 * As a User, I want a Last 10 Search as a reusable component
 * so that it remembers last 10 records selected and pick them
 * =============================================================
 *
 * Author: Jai Prakash
 *
 */
import React from "react";
import ArrowKeys from "./../ArrowKeys/ArrowKeys";
import LastTenRow from "./LastTenRow";
import "./LastTenSearch.style.css";

export class LastTenSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hidden: true
    };
    this.toggleLast10 = this.toggleLast10.bind(this);
    this.selectRecord = this.selectRecord.bind(this);
  }

  componentDidUpdate() {
    setTimeout(() => {
      if (!this.state.hidden) {
        window.addEventListener("click", this.toggleLast10);
      } else {
        window.removeEventListener("click", this.toggleLast10);
      }
    }, 0);
  }

  toggleLast10() {
    this.setState({ hidden: !this.state.hidden });
  }

  selectRecord(obj, e) {
    e.stopPropagation();
    this.props.setRecord(obj);
    this.toggleLast10();
  }

  render() {
    const position = this.props.position || "left";
    return (
      <React.Fragment>
        <button
          type="button"
          name="recent search"
          title="Recent Search"
          onClick={this.toggleLast10}
        >
          <svg width="24" height="24" viewBox="0 0 24 24">
            <title>Last Ten Search</title>
            <path
              fill="#1B1F26"
              fillRule="evenodd"
              d="M10 2.75c3.996 0 7.25 3.133 7.25 6.99 0 1.79-.704 3.474-1.943 4.757l4.777 4.783a.563.563 0 0 1-.017.814.59.59 0 0 1-.817-.016l-4.81-4.816A7.387 7.387 0 0 1 10 16.73c-3.996 0-7.25-3.133-7.25-6.99S6.004 2.75 10 2.75zm0 1.142c-3.356 0-6.083 2.626-6.083 5.848 0 3.223 2.727 5.848 6.083 5.848 3.356 0 6.083-2.625 6.083-5.848 0-3.222-2.727-5.848-6.083-5.848z"
            />
          </svg>
        </button>
        <div
          className="ccl-recent-search-wrapper noselect"
          style={{
            left: position === "right" ? "inherit" : 0,
            right: position === "right" ? 0 : "inherit"
          }}
          hidden={this.state.hidden}
        >
          <div
            hidden={this.props.newSearchRequired}
            className="ccl--new-search"
            onClick={this.props.gotoFind}
          >
            {this.props.newSearchText}
          </div>
          <div className="ccl--recent-search">Recent Search</div>
          <ul className="ccl--last-ten-list-wrapper">
            <LastTenRow
              data={this.props.data}
              selectRecord={this.selectRecord}
              index={this.props.index || 0}
            />
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default ArrowKeys(LastTenSearch, ".ccl--last-ten-list-wrapper");
