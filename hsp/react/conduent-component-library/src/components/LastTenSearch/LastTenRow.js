import React from "react";

const LastTenRow = props => {
  const { selectRecord, data, index } = props;

  const clickedRecord = (obj, e) => {
    selectRecord(obj, e);
  };

  const onEnter = (item, e) => {
    if (e.key === "Enter") {
      selectRecord(item, e);
    }
  };

  const onSelect = e => {
    const { currentTarget: target } = e;
    target.focus();
    target.tabIndex = 1;
    if (document.querySelector("li.hover")) {
      document.querySelector("li.hover").classList.remove("hover");
    }
    target.classList.add("hover");
  };

  const lastTenListItems = data.length ? (
    data.map((listItem, i) => {
      const hover =
        index === i && !listItem.error
          ? "hover"
          : listItem.error
          ? "error"
          : "";
      return (
        <li
          tabIndex={index?.toString()}
          key={i}
          className={hover.toString()}
          onClick={clickedRecord.bind(this, listItem)}
          onKeyDown={onEnter.bind(this, listItem)}
          onMouseEnter={onSelect}
          index={index}
        >
          {listItem.text}
        </li>
      );
    })
  ) : (
    <li className="loading">No record available</li>
  );
  return lastTenListItems;
};

export default LastTenRow;
