import React from "react";
import { LastTenList } from "./../../styled";

const LastTen = ({ hidden, findRecord, round }) => {
  return (
    <LastTenList hidden={hidden} round={round}>
      <li onClick={findRecord}>Last 10 find/s</li>
    </LastTenList>
  );
};

export default LastTen;
