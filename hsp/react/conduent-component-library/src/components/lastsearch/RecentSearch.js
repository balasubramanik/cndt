/*
 * User Story: Find Control tool strip
 * Description: Last Search done to find either Contract or Office
 * Author: Bala
 */
import React, { Component } from "react";
import RecentSearchIcon from "../../assets/img/recent-search.png";
import { Constants } from "../../common/Constants";

class RecentSearch extends Component {
  render() {
    const { onClick } = this.props;
    return (
      <a href="#" role="button" title={Constants.PREVIOUS_SEARCH_TITLE}>
        <img
          src={RecentSearchIcon}
          className="settings dropdown-toggle"
          className="recent-search"
          alt="Recent Search"
          onClick={onClick}
        />
      </a>
    );
  }
}
export default RecentSearch;
