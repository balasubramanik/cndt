/**
 * User_story: US25
 *
 * Description:
 * =======================================================
 * As a User, I want a Cascading dropdown as a reusable component
 * so that it can display and select list
 * =======================================================
 * Author: Kanan
 */

import React from "react";
import ComboBox from "../combobox/ComboBox";
import { camelCase } from "../../common/Utils";

class CascadingDropDown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: [],
      comboSelValues: {},
      option: 1,
      ddlDataArr: []
    };
  }

  componentDidMount() {
    this.defaultSelectState();
  }
  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.isClear != this.props.isClear && nextProps.isClear) {
      this.defaultSelectState();
      this.props.handleIsClear();
      return true;
    }
    return true;
  }
  defaultSelectState() {
    const ddlListName = [];
    this.props.dropdownsList.map(index =>
      ddlListName.push({
        ddlName: index.name,
        selectedVal: 0,
        crrentChange: "N"
      })
    );
    this.setState({ selectedValue: ddlListName });
  }

  ChangeDdl(e, ddlName, value, label, linkTo) {
    if (!e.length) {
      return;
    }
    const values = this.state.comboSelValues;
    let temp = 0;
    const comboObject = {};

    comboObject[value] = e[0][value];
    comboObject[label] = e[0][label];
    values[name] = [].concat(comboObject);

    const newData = this.state.selectedValue.map(el => {
      if (el.ddlName == ddlName) {
        temp = 1;
        return Object.assign({}, el, { val: e, selectedVal: e[0][value] });
      }
      if (temp === 1) {
        return Object.assign({}, el, { val: [], selectedVal: 0 });
      }
      return el;
    });
    this.setState({ selectedValue: newData, comboSelValues: values });
    this.props.onValue(e[0][value], ddlName, value, linkTo, e);
    this.props.handleIsClear();
  }
  getPrevValue = (index, key) => {
    let selectedValue = [];
    const { findParameters, findParamsPrev, controlsName } = this.props;
    if (findParameters[index.name] === "" || findParameters[index.name]) {
      selectedValue = findParamsPrev[index.name]
    } else {
      selectedValue = index.name === this.state.selectedValue[key].ddlName &&
      this.state.selectedValue[key].selectedVal === "0"
        ? []
        : this.state.selectedValue[key].val
    }
    return selectedValue;
  }
  renderDropdown() {
    if (this.state.selectedValue.length) {
      return this.props.dropdownsList.map((index, key) => {

        const label = camelCase(index.displayFieldName);
        const value = camelCase(index.displayIDName);
        return (
          <div className="form-group">
            <label htmlFor={index.name}>{index.name}</label>
            <ComboBox
              id={index.name}
              isCascading={true}
              key={
                this.state.comboSelValues[index.name] &&
                this.state.comboSelValues[index.name].length
              }
              referenceCodeType={index.referenceCodeType}
              required={index.required}                          
              controlType={index.controlType}
              name={index.name}
              defaultOption={this.props.defaultOption}
              title={index.tooltip}
              onChange={values =>
                this.ChangeDdl(values, index.name, value, label, index.linkTo)
              }
              values={ this.getPrevValue(index, key) }
              options={
                this.props.dropdownsListData.length >= key + 1
                  ? this.props.dropdownsListData[key][index.name]
                  : ""
              }
              valueField={value}
              labelField={label}
              clearOnSelect
              clearable
            />
          </div>
        );
      });
    }
  }

  render() {
    return (
      <div>
        <div className="row" className="hdr">
          <div>{this.renderDropdown()}</div>
        </div>
      </div>
    );
  }
}
export default CascadingDropDown;
