/*
 * User Story ID: US19_Provider_Find_Control_(Main ToolStrip)
 * Description: Find Control
 * Author: Bala
 */
import React, { Component } from "react";
import TextField from "../textbox/TextField";
import DateTimePicker from "../datepicker/DateTimePicker";
import Collapse from "../FindControl/Collapse";
import FindSidePanel from "../FindControl/FindSidePanel";
import RecentSearch from "../lastsearch/RecentSearch";
import ComboBox from "../combobox/ComboBox";
import ResultCount from "./ResultCount";
import { camelCase } from "../../common/Utils";
import moment from "moment";
import CascadingDropDown from "../CascadingDropDown/CascadingDropDown";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import warningIcon from "../../assets/img/warning-icon.png";
import "../../assets/css/index.css";

class FindControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      findParameters: {},
      comboSelValues: {},
      profile: "",
      userStatus: null,
      profileSettings: null,
      isFilterWarning: false,
      validationErrors: {},
      isClear: false,
      findParamsPrev: {},
      isPrevious: false,
      findParamsPrevOnFind: {}
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.onComboChange = this.onComboChange.bind(this);
    this.handleResultCount = this.handleResultCount.bind(this);
    this.enterHandler = this.enterHandler.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onClearClick = this.onClearClick.bind(this);
    this.setProfile = this.setProfile.bind(this);
    this.casecadeDropdownValue = this.casecadeDropdownValue.bind(this);
  }

  componentWillReceiveProps(props) {
    let params = {};
    let status = "";
    let activeProfile = "";
    let profile = "";

    if (props.screenName === "Office") {
      const findParameters = this.state.findParameters;
      findParameters.Language = (findParameters.Language === undefined || findParameters.Language === null)?"ENG":findParameters.Language;

      //if switching from other screen
      if(props.screenName !== this.props.screenName && props.screenName === "Office") {
        findParameters.Language = "ENG"
      }
      this.setState({ findParameters });
    }

    if (props) {
      if (props.findControls) {
        if (props.findControls.length) {
          props.findControls.map(obj => {
            params[obj.name] = obj.defaultValue ? obj.defaultValue : "";
          });
        }
      }
      if (props.findProfiles) {
        const { findProfile, findProfile2 } = props.findProfiles;
       
        // if (findProfile2) {
        //   if (findProfile2.length) {
        //     findProfile2.map(profile => {
        //       if (profile.status === "Active") {
        //         activeProfile = profile.name;
        //       }
        //     });
        //   }
        // }
        // if (findProfile) {
        //   if (findProfile.length) {
        //     findProfile.map(profile => {
        //       if (profile.status === "Active") {
        //         activeProfile = profile.name;
        //       }
        //     });
        //   }
        // }
      }
      if (props.recentData) {
        params = props.recentData;
      }
      if (props.userOption) {
        status = props.userOption;
      }
      if (props.profileSettings) {
        profile = props.profileSettings;
      }
    }

    this.setState({
      findParameters: params,
      userStatus: status,
      profile: activeProfile,
      profileSettings: profile
    });
  }

  componentDidMount() {
    document.addEventListener("keydown", this.enterHandler, false);
    const t1 = localStorage.getItem("previous");
    const t2 = t1 ? JSON.parse(t1) : {}
    this.setState({
      findParamsPrevOnFind: {...t2}
    });
  }

  setProfile(profile, profileId) {
    const obj = this.state.findParameters;
    let key;
    const keys = Object.keys(obj);
    let n = keys.length;
    const newobj = {};
    while (n--) {
      key = keys[n];
      newobj[camelCase(key)] = obj[key];
    }

    this.setState(
      {
        profile: profile
      },
      () => {
        this.props.onProfile(profileId, newobj, this.props.screenName, profile);
      }
    );
  }

  handleResultCount(resultCount, name) {
    const params = this.state.findParameters;
    params[name] = resultCount;
    this.setState({ findParameters: params, isClear: false, isPrevious: false });
  }

  onClearClick() {
    const params = this.state.findParameters;
    const comboValues = this.state.comboSelValues;
    const paramsPrev = this.state.findParamsPrev;
    this.props.findControls.map(obj => {
      if (params.hasOwnProperty(obj.name)) {
        comboValues[obj.name] = [];
        if (obj.defaultValue) {
          params[obj.name] = obj.defaultValue;
          if (obj.name === "Language") {
            const comboObject = {};
            comboObject[camelCase(obj.displayIDName)] = "ENG";
            comboObject[camelCase(obj.displayFieldName)] = "English";
            comboValues[obj.name] = [comboObject];
            paramsPrev[obj.name] = [comboObject];
          }
        } else {
          delete params[obj.name];
        }
      }
    });
    this.props.clearIndex("false");
    this.setState({ findParameters: params, comboSelValues: comboValues, isClear: true,
      findParamsPrev: paramsPrev,
      isClear: true, isPrevious: false });
  }

  handleInputChange(e) {
    const params = this.state.findParameters;
    params[e.target.id] = e.target.value;
    this.setState({ findParameters: params, isClear: false, isPrevious: false });
  }

  handleSubmit() {
    if (!this.validateControls()) {
      return false;
    }
    const params = this.state.findParameters;

    if (!Object.keys(params).length && this.props.screenName === "Office") {
      this.setState({
        isFilterWarning: true
      });
      this.props.clearIndex("false");
      return false;
    } else if (
      Object.keys(params).length &&
      this.props.screenName === "Office"
    ) {
      const allControls = Object.keys(params).filter(
        element => params[element] !== "" && params[element] !== null
      );
      if (!allControls.length) {
        this.setState({
          isFilterWarning: true
        });
        this.props.clearIndex("false");
        return false;
      }
    }

    const finalData = this.props.params;
    const itemValue = {};
    this.props.findControls.map(obj => {
      if (params.hasOwnProperty(obj.name)) {
        itemValue[obj.name] = params[obj.name];
      }
    });

    const obj = this.state.findParameters;
    let key;
    const keys = Object.keys(obj);
    let n = keys.length;
    const newobj = {};
    while (n--) {
      key = keys[n];
      newobj[camelCase(key)] = obj[key];
    }

    finalData.itemValue = JSON.stringify(itemValue);
    if (newobj.resultCount && newobj.resultCount === "0") {
      delete newobj.resultCount;
    }
    this.props.onFind(finalData);
    this.props.handleFind(newobj, this.props.screenName);
    this.props.clearIndex("true");
    const temp = JSON.parse(JSON.stringify(this.state.findParamsPrev));
    this.setState({
      findParamsPrevOnFind: {...this.state.findParamsPrevOnFind, ...temp}
    });
  }

  validateControls() {
    let isValid = true;
    const params = this.state.findParameters;
    const validators = this.props.controlValidations;
    const errorObj = {};
    this.props.findControls.map(obj => {
      if (params.hasOwnProperty(obj.name)) {
        const control = validators[obj.name];
        if (!control) {
          return;
        }
        Object.keys(control.validation).forEach(key => {
          switch (key) {
            case "max": {
              if (params[obj.name].length > control.validation[key]) {
                isValid = false;
                errorObj[obj.name] =
                  (control.messages && control.messages[key]) ||
                  "Invalid Value";
              }
              break;
            }
            case "dateRange": {
              const dateObj = moment(params[obj.name]);
              const start = moment(control.validation[key].start);
              const end = moment(control.validation[key].end);
              if (!dateObj.isValid()) {
                isValid = false;
                errorObj[obj.name] =
                  (control.messages && control.messages[key]) || "Invalid Date";
              } else if (!start.isValid() || !end.isValid()) {
                isValid = false;
                errorObj[obj.name] =
                  (control.messages && control.messages[key]) ||
                  "No Valid Range Given";
              } else if (!(dateObj >= start && dateObj <= end)) {
                isValid = false;
                errorObj[obj.name] =
                  (control.messages && control.messages[key]) ||
                  "Invalid Value";
              }
              break;
            }
          }
        });
      }
    });
    this.setState({ validationErrors: errorObj });
    return isValid;
  }

  onComboChange(obj, name, key, fieldName) {
    const params = this.state.findParameters;
    const paramsPrev = this.state.findParamsPrev;
    if (obj.length) {
      const values = this.state.comboSelValues;

      params[name] = obj[0][key];
      // values[name]

      const comboObject = {};
      comboObject[key] = obj[0][key];
      comboObject[fieldName] = obj[0][fieldName];
      values[name] = [].concat(comboObject);
      paramsPrev[name] = [].concat(comboObject);
      this.setState({ findParameters: params, comboSelValues: values, findParamsPrev: paramsPrev });
    } else {
      delete params[name];
    }
    this.setState({ findParameters: params, isClear: false, isPrevious: false });
  }

  enterHandler(e) {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  }

  toggleWarning() {
    this.setState({
      isFilterWarning: false
    });
  }

  handleDateChange(selectedDate, name) {
    const selData = selectedDate || null;
    const params = this.state.findParameters;

    params[name] =
    selData !== null
        ? moment(moment(selData)).format("MM/DD/YYYY")
        : null;
    this.setState({
      findParameters: params,
      isClear: false,
      isPrevious: false
    });
  }

  componentWillUnmount() {
    document.removeEventListener("keydown", this.enterHandler, false);
  }

  casecadeDropdownValue(value, ddlName, id, linkTo, e) {
    const params = this.state.findParameters;
    const paramsPrev = this.state.findParamsPrev;
    params[ddlName] = value === this.props.comboDefaultOption[ddlName] ? "" : value;
    
    paramsPrev[ddlName] = e;
    this.setState({ findParameters: params, isClear: false, findParamsPrev: paramsPrev, isPrevious: false });
    this.props.casecadeDropdownValue(value, ddlName, id, linkTo);
  }
  handleIsClear = () => {
    this.setState({ isClear: false });
  }
  onRecentSearch = () => {
    const temp = JSON.parse(JSON.stringify(this.state.findParamsPrevOnFind));
    this.setState({
      comboSelValues: {...temp},
      isPrevious: true
    });
    this.props.onRecentSearch(this.props.params);
  }
  getPrevValue = (controls) => {
    let defValue = [];
    console.log("inside getPrevValue " + this.state.isPrevious);
    if(this.state.isPrevious) {
      defValue = this.props.recentData[controls.name] === "" || this.props.recentData[controls.name] ? this.state.findParamsPrevOnFind[controls.name] : [];
    } else {
      defValue = (this.state.findParameters[controls.name] === "" || this.state.findParameters[controls.name]) ? this.state.findParamsPrev[controls.name] : [];
    }
    return defValue;
  }

  getPrevValueCasFindParams = () => {
    let findParams = {...this.state.findParameters};
    if(this.state.isPrevious) {
      const prevValues = this.props.params && this.props.params.itemValue ? JSON.parse(this.props.params.itemValue) : {};
      return prevValues;
    } 
    return findParams;
  }
  
  getPrevValueCasFindParamsPrev = () => {
    if(this.state.isPrevious) {
      return this.state.findParamsPrevOnFind;
    } else {
      return this.state.findParamsPrev;
    }
  }

  componentWillUnmount() {
    localStorage.setItem("previous", JSON.stringify(this.state.findParamsPrevOnFind));
  }

  getOptions = (list=[], controls) => {
    const valueField=camelCase(controls.displayIDName);
    const labelField=camelCase(controls.displayFieldName);
    const options = [];
      list.map((l) => {
        const obj = {};
        obj[valueField] = l[valueField];
        obj[labelField] = l[labelField];
        options.push(obj);
      })
      return options;
  }
  render() {
    const prevValues = this.props.params && this.props.params.itemValue ? JSON.parse(this.props.params.itemValue) : {};
    
    const self = this;
    const cascadeDropdownListData = this.props.findControls.filter(
      obj => obj.linkTo || obj.linkFrom
    );
    let profile = "";
    if(this.props.activeProfile===""){
      profile = "No Profile";
    }else{
      profile = this.props.activeProfile;
    }
    const control = this.props.findControls.length ? (
      <React.Fragment>
        <form>
          <div className="sidebar">
            <ul className="bcg">
              <li className="collapse-list">
                <Collapse click={() => this.props.toggleExpandNav()} />
              </li>
              <li className="recent-list">
                <RecentSearch
                  onClick={e => this.onRecentSearch()}
                />
              </li>
              <li className="find-button-list">
                <FindSidePanel
                  findProfiles={this.props.findProfiles}
                  screenName={this.props.screenName}
                  activeProfile={this.props.activeProfile}
                  onProfile={(profile, profileId) =>
                    this.setProfile(profile, profileId)
                  }
                />
              </li>
            </ul>

            <div className="sidebar-wrapper ps" ref="sidebarWrapper">
              {this.props.findControls.map((controls, index) => {
                switch (controls.controlType) {
                  case "ToolkitTextbox": {
                    return (
                      <TextField
                        key={index}
                        type="text"
                        id={controls.name}
                        controlName={controls.name}
                        label={controls.label}
                        required={controls.required}
                        value={this.state.findParameters[controls.name]}
                        minLength={controls.minLength}
                        maxLength={controls.maxLength}
                        validationErrors={this.state.validationErrors}
                        title={controls.tooltip}
                        placeholder={controls.label}
                        dataType={controls.dataType}
                        autoComplete="off"
                        onChange={this.handleInputChange}
                      />
                    );
                  }
                  case "ToolkitDateTimePicker": {
                    const date =
                      typeof self.state.findParameters[controls.name] ===
                      "string"
                        ? new Date(self.state.findParameters[controls.name])
                        : self.state.findParameters[controls.name];
                    return (
                      <div
                        className={
                          (self.state.validationErrors &&
                          self.state.validationErrors[controls.name]
                            ? "invalid-control"
                            : "") + " form-group"
                        }
                      >
                        <label htmlFor={controls.name}>{controls.label}</label>
                        <DateTimePicker
                          // key={date}
                          id={controls.name}
                          placeHolder={controls.label}
                          label={controls.label}
                          controlName={controls.name}
                          validationErrors={self.state.validationErrors}
                          selectedDate={date || null}
                          handleChange={date =>
                            self.handleDateChange(date, controls.name)
                          }
                          className="form-control"
                        />
                      </div>
                    );
                  }
                  case "ToolkitMultiColumnCombobox": {
                    const comboObject = {};
                    comboObject[camelCase(controls.displayIDName)] = "ENG";
                    comboObject[camelCase(controls.displayFieldName)] =
                      "English";

                    const colName = this.state.comboSelValues[controls.name];
                    const columnKey =
                      colName &&
                      colName.length &&
                      colName[0][camelCase(controls.displayFieldName)];
                    return (
                      <div className="form-group">
                        <label htmlFor={controls.name} >{controls.label}</label>
                        <ComboBox
                          referenceCodeType={controls.referenceCodeType}
                          required={controls.required}
                          controlType={controls.controlType}
                          id = {controls.name}
                          key={
                            columnKey ||
                            comboObject[camelCase(controls.displayFieldName)]
                          }
                          columns={[
                            { name: "Name", field: "name" },
                            { name: "Description", field: "description" }
                          ]}
                          options={controls.comboData}
                          defaultOption={
                            this.props.comboDefaultOption[controls.name]
                          }
                          className="dropdown-overlay"
                          valueField={camelCase(controls.displayIDName)}
                          labelField={camelCase(controls.displayFieldName)}
                          onChange={values =>
                            this.onComboChange(
                              values,
                              controls.name,
                              camelCase(controls.displayIDName),
                              camelCase(controls.displayFieldName)
                            )
                          }
                          title={controls.tooltip}
                          values={
                            this.state.comboSelValues[controls.name] || [
                              comboObject
                            ]
                          }
                          separator
                          closeOnSelect
                        />
                      </div>
                    );
                  }
                  case "ToolkitCombobox": {
                    const value = controls.defaultValue
                      ? controls.defaultValue
                      : "";
                      const defValue = this.getPrevValue(controls);
                    const combobox = (
                      <div className="form-group">
                        <label htmlFor={controls.name}>{controls.label}</label>
                        <ComboBox
                          id={controls.name}
                          key={
                            this.state.comboSelValues[controls.name] &&
                            this.state.comboSelValues[controls.name].length
                          }
                          referenceCodeType={controls.referenceCodeType}
                          required={controls.required}                          
                          controlType={controls.controlType}
                          options={this.getOptions(controls.comboData, controls)}
                          defaultOption={
                            this.props.comboDefaultOption[controls.name]
                          }
                          valueField={camelCase(controls.displayIDName)}
                          labelField={camelCase(controls.displayFieldName)}
                          searchBy={camelCase(controls.displayFieldName)}
                          onChange={values =>
                            this.onComboChange(
                              values,
                              controls.name,
                              camelCase(controls.displayIDName),
                              camelCase(controls.displayFieldName)
                            )
                          }
                          className="form-control"
                          values={defValue}
                          title={controls.tooltip}
                          separator
                          Searchable
                          clearable
                          Autofocus={false}
                          name={controls.name}
                        />
                      </div>
                    );

                    const resultCount = (
                      <ResultCount
                        key={index}
                        id={controls.name}
                        label={controls.label}
                        defaultValue={controls.defaultValue}
                        value={this.state.findParameters[controls.name]}
                        required={controls.required}
                        onValue={result =>
                          this.handleResultCount(result, controls.name)
                        }
                        referenceCodes_V2DTOs={this.props.referenceCodes_V2DTOs}
                        screenName={this.props.screenName}
                      />
                    );
                    const cascadeDropdown = (
                      <CascadingDropDown
                        key={
                          this.state.comboSelValues[controls.name] &&
                          this.state.comboSelValues[controls.name].length
                        }
                        dropdownsList={cascadeDropdownListData}
                        defaultOption={
                          this.props.comboDefaultOption[controls.name]
                        }
                        onValue={(value, ddlName, id, linkTo, e) =>
                          this.casecadeDropdownValue(value, ddlName, id, linkTo, e)
                        }
                        dropdownsListData={this.props.cascadeDropdownsListData}
                        isClear={this.state.isClear}
                        handleIsClear={this.handleIsClear}
                        findParameters={this.getPrevValueCasFindParams()}
                        findParamsPrev={this.getPrevValueCasFindParamsPrev()}
                      />
                    );
                    let ctrlToolkitCombo = null;
                    if (controls.referenceCodeType === "RESULTCOUNT") {
                      ctrlToolkitCombo = resultCount;
                    } else if (controls.linkTo && !controls.linkFrom) {
                      ctrlToolkitCombo = cascadeDropdown;
                    } else if (controls.linkFrom) {
                      ctrlToolkitCombo = null;
                    } else {
                      ctrlToolkitCombo = combobox;
                    }
                    return ctrlToolkitCombo;
                  }
                  default: {
                    return "";
                  }
                }
              })}
            </div>
            <div className="findButton">
              <div className="form-row">
                <div className="col-6">
                  <button
                    type="reset"
                    className="btn btn-light clear-btn"
                    onClick={this.onClearClick}
                  >
                    Clear
                  </button>
                </div>
                <div className="col-6">
                  <button
                    type="button"
                    className="btn btn-primary find-btn"
                    onClick={this.handleSubmit}
                  >
                    Find
                  </button>
                </div>
              </div>
              <div className="form-row">
                <div className="col">
                  <span className="profile">Find {this.props.screenName}</span>
                  <span className="profile">&nbsp;({profile})</span>
                </div>
              </div>
            </div>
          </div>
          <div className="main-panel" />
        </form>
        <div>
          <Modal className="warning-popup" isOpen={this.state.isFilterWarning}>
            <ModalHeader>Meditrac</ModalHeader>
            <ModalBody>
              <div className="warning-icon">
                <img src={warningIcon} alt="Warning Icon" />
              </div>
              <div className="warning-text">
                Could not find Offices because no criteria were specified
              </div>
            </ModalBody>
            <ModalFooter>
              <Button
                color="primary"
                onClick={() => {
                  this.toggleWarning();
                }}
              >
                Ok
              </Button>
            </ModalFooter>
          </Modal>
        </div>
      </React.Fragment>
    ) : (
      ""
    );
    return <React.Fragment>{control}</React.Fragment>;
  }
}

export default FindControl;
