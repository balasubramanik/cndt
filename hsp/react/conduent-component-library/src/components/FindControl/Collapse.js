import React, { useState } from "react";
import CollapseIcon from "../../assets/img/collapse.png";
import ShowIcon from "../../assets/img/show.png";
function Collapse(props) {
  const [mini, setMini] = useState(false);

  const toggleSidebar = () => {
    setMini(!mini);
    document.body.classList.toggle("sidebar-mini");
    props.click();
  };
  return (
    <React.Fragment>
      <a href="#" role="button">
        <img
          src={mini ? ShowIcon : CollapseIcon}
          className="collapse-icon dropdown-toggle"
          alt="Toggle Sidebar"
          onClick={toggleSidebar}
          title={mini ? "Show Search Criteria" : "Hide Search Criteria"}
        />
      </a>
      <p className="parameters">Parameters</p>
    </React.Fragment>
  );
}
export default Collapse;
