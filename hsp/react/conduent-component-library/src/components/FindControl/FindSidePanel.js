/**
 * User Story: US27_Provider_Re-usable Components_Additional_Find_Side_Panel_Buttons
 * Description: As a User, I want Additional Find Side Panel Buttons so that it can display user profile maintenance
 * Author: Bala
 */

import React, { Component } from "react";
import SettingsIcon from "../../assets/img/settings.png";
import classNames from "classnames";
import propTypes from "prop-types";
import { Constants } from "../../common/Constants";

class FindSidePanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
      submenuToggle: false
    };
    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.toggleSubMenu = this.toggleSubMenu.bind(this);
  }

  componentDidMount() {
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({ toggle: false });
    }
  }

  toggleSubMenu() {
    if(this.state.submenuToggle===false){
      this.setState({ submenuToggle: !this.state.submenuToggle});
    }else{
      this.setState({ submenuToggle: !this.state.submenuToggle,  toggle: false });
    }
  }

  render() {
    const { toggle, submenuToggle } = this.state;
    const classes = classNames({
      show: toggle
    });
    const subMenuClass = classNames({
      show: submenuToggle
    });

    const { findProfile, findProfile2 } = this.props.findProfiles;

    return (
      <React.Fragment>
        <div className={`dropdown ${classes}`} ref={this.setWrapperRef}>
          <a href="#" role="button" title={Constants.PROFILE_SETTINGS_TITLE}>
            <img
              src={SettingsIcon}
              className="settings dropdown-toggle"
              alt="Find Side Panel"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded={toggle}
              onClick={e => this.setState({ toggle: !this.state.toggle })}
            />
          </a>
          <ul
            className={`dropdown-menu ${classes}`}
          >
            <li className="dropdown-submenu dropright">
              <a
                className="dropdown-item dropdown-toggle"
                href="#"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded={this.state.submenuToggle}
                onClick={e => this.toggleSubMenu()}
                title="Available User Profile"
              >
                Available Profiles
              </a>
              <ul
                className={`dropdown-menu ${subMenuClass}`}
              >
                
                {findProfile &&
                  findProfile.map(profile => (
                    <li key={profile.findProfileId}>
                      <a
                        className={`dropdown-item ${
                          profile.status === "1" ? "active" : ""
                        }`}
                        href="#"
                        onClick={e => {
                          this.props.onProfile(
                            profile.name,
                            profile.findProfileId
                          );
                          this.toggleSubMenu();
                        }}
                        title={`${profile.name} Profile`}
                      >
                        {profile.name}
                      </a>
                    </li>
                  ))}
                {findProfile2 &&
                  findProfile2.map(profile => (
                    <li key={profile.findProfileId}>
                      <a
                        className={`dropdown-item ${
                          profile.status === "1" ? "active" : ""
                        }`}
                        href="#"
                        onClick={e => {
                          this.props.onProfile(
                            profile.name,
                            profile.findProfileId
                          );
                          this.toggleSubMenu();
                        }}
                        title={`${profile.name} Profile`}
                      >
                        {profile.name}
                      </a>
                    </li>
                  ))}
                  <li key="noProfile">
                    <a
                      className={`dropdown-item`}
                      href="#"
                      onClick={e => {
                        this.props.onProfile(
                          "No Profile",
                          0
                        );
                        this.toggleSubMenu();
                      }}
                      title={`No Profile`}
                    >
                      No Profile
                    </a>
                  </li>
              </ul>
            </li>
            <li>
              <a
                className="dropdown-item"
                href="#"
                onClick={e => this.setState({ submenuToggle: false })}
                title="Manage Profiles Settings"
              >
                Manage Profiles
              </a>
            </li>
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

FindSidePanel.propTypes = {
  findCommand: propTypes.string
};
export default FindSidePanel;
