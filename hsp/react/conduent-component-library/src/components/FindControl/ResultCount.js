/*
 * To show Show Result option in find control
 */
import React, { Component } from "react";

class ResultCount extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      id,
      label,
      value,
      required,
      referenceCodes_V2DTOs,
      onValue,
      screenName
    } = this.props;
    const finalValue = value===undefined && screenName==="Contract"?100: value===undefined && screenName!=="Contract"?0:value;

    const labelStar = required === "Y" && (
      <span className="text-danger">*</span>
    );
    return (
      <div className="form-group">
        {label && <label htmlFor={id}>Show Results {labelStar}</label>}
        <ul className="pagination">
          <li
            onClick={e => onValue("0")}
            className={`page-item ${finalValue == "0" ? "active" : ""}`}
          >
            <a href="#" className="page-link">
              All
            </a>
          </li>
          {referenceCodes_V2DTOs.map((resultCount, index) => (
            <li
              key={index}
              onClick={e => onValue(resultCount.name)}
              className={`page-item ${
                finalValue == resultCount.name ? "active" : ""
              }`}
            >
              <a href="#" className="page-link">
                {resultCount.name}
              </a>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default ResultCount;
