import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-my-burger-b41d6.firebaseio.com/'
});

export default instance;
