Workforce Management, Leave management, Recruitment management, Project portfolio management, Policy awareness system and Learning management
23-Jul-2018
14-Feb-2020
2
Recruitment management:
* Recruitment request and approval.
* Candidate registration
* Updating status of Interview.
Workforce Management:
* On-boarding process
* Employee's Performance management
* Exit process
Leave management:
* Applying and Approving Leave
* Updating on-duty and week-off.
* Unplanned leave management
Project portfolio management:
* Project creation
* Time utilization report
* Revenue report
* Cost configuration and Settings
Policy awareness system:
* Showing policies to Employee and put them to attend the quiz.
* Reports.
Learning management
* Confirmed employees can take any course and got approval.
* once it is approved they need to attach the receipt for reimbursement.
* Once the recipt verified Amount will be credited on the Next month salary cycle.


MERIT SOFTWARE SERVICES PVT LTD

SOFTWARE ENGINEER


HTML 5, CSS 3, Javascript, Angular JS, React JS, PHP, MySQL and MS SQL Server.

Developing and Maintaining Web Applications.

Senior Software Engineer
1.	4+ yrs of experience in application development for web based software applications
2.	Good knowledge of software development life cycle that includes requirement analysis, development, testing, implementation of Web Applications
3.	Strong programming skills in HTML 5, CSS 3, Angular JS, React JS, PHP.
4.	Good hands-on experience in relational databases like MySQL and MS SQL 
5.	Efficient in using MVC Architecture
6.	Excellent analytical, problem solving and presentation skills